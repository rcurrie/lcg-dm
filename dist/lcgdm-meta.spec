Name:		lcgdm-meta
Version:	1.8.11
Release:	1%{?dist}
Summary:	LHC Computing Grid Data Management

Group:		Applications/Internet
License:	ASL 2.0
URL:		http://glite.web.cern.ch/glite/

%description
The lcgdm meta package provides all meta packages for both DPM and
LFC components.

%package -n emi-lfc_mysql
Summary:	EMI LFC (MySQL)
Group:		Applications/Internet
Requires:	bdii
Requires:	dmlite-plugins-adapter >= 0.4.0
Requires:	dmlite-plugins-mysql >= 0.4.0
Requires:	edg-mkgridmap
Requires:       emi-version
Requires:	fetch-crl
Requires:	finger
Requires:	lcgdm-dav
Requires:	lcgdm-dav-server
Requires:	lfc%{?_isa} = %{version}
Requires:	lfc-devel%{?_isa} = %{version}
Requires:	lfc-perl%{?_isa} = %{version}
Requires:	lfc-python%{?_isa} = %{version}
Requires:	lfc-server-mysql%{?_isa} = %{version}
Requires:	lfc-yaim

%description -n emi-lfc_mysql
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package contains the runtime LFC client library.
This is a virtual package providing all required daemons for the LFC.

%package -n emi-dpm_mysql
Summary:	EMI DPM Head Node (MySQL)
Group:		Applications/Internet
Requires:	bdii
Requires:	dpm%{?_isa} = %{version}
Requires:	dpm-copy-server-mysql%{?_isa} = %{version}
Requires:	dpm-devel%{?_isa} = %{version}
Requires:	dpm-dsi%{?_isa}
Requires:	dpm-name-server-mysql%{?_isa} = %{version}
Requires:	dpm-perl%{?_isa} = %{version}
Requires:	dpm-python%{?_isa} = %{version}
Requires:	dpm-rfio-server%{?_isa} = %{version}
Requires:	dpm-server-mysql%{?_isa} = %{version}
Requires:	dpm-srm-server-mysql%{?_isa} = %{version}
Requires:	dpm-yaim >= 4.2.20
Requires:	dmlite-plugins-adapter >= 0.4.0
Requires:	dmlite-plugins-mysql >= 0.4.0
Requires:	edg-mkgridmap
Requires:       emi-version
Requires:	fetch-crl
Requires:	finger
Requires:	lcgdm-dav
Requires:	lcgdm-dav-server
Requires:	lcg-expiregridmapdir

%description -n emi-dpm_mysql
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This is a virtual package providing all required daemons for a DPM
Head Node. 

%package -n emi-dpm_disk
Summary:	EMI DPM Disk Node
Group:		Applications/Internet
Requires:	dpm%{?_isa} = %{version}
Requires:	dpm-devel%{?_isa} = %{version}
Requires:	dpm-dsi%{?_isa}
Requires:	dpm-perl%{?_isa} = %{version}
Requires:	dpm-python%{?_isa} = %{version}
Requires:	dpm-rfio-server%{?_isa} = %{version}
Requires:	dpm-yaim >= 4.2.20
Requires:	dmlite-plugins-adapter >= 0.4.0
Requires:	edg-mkgridmap
Requires:       emi-version
Requires:	fetch-crl
Requires:	finger
Requires:	lcgdm-dav
Requires:	lcgdm-dav-server
Requires:	lcg-expiregridmapdir

%description -n emi-dpm_disk
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This is a virtual package providing all required daemons for a DPM
Disk Node. 

%prep

%build

%install

%files -n emi-lfc_mysql

%files -n emi-dpm_mysql

%files -n emi-dpm_disk

%changelog
* Fri Feb 01 2013 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.6-2
- Drop dependency on lfc-dli from the emi lfc metapackage
- Dropped dependencies on the release number for the metapackages

* Mon Dec 10 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.6-1
- Update for new upstream release

* Thu Nov 01 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.5-1
- Update for new upstream release

* Mon Sep 03 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.4-1
- Update for new upstream release

* Mon Jun 11 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3.1-2
- Follow up with main lcgdm release

* Thu Mar 22 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3-3
- Added oracle metapackages

* Thu Feb 09 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3-2
- Added missing dependencies on the metapackages

* Tue Feb 07 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3-1
- Updated metapackage dependencies to lcgdm 1.8.3

* Tue Dec 06 2011 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.2-1
- Initial build
