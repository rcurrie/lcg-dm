# Temporary (we need to manually remove the unwanted files instead)
%define _unpackaged_files_terminate_build 0

Name:		lcgdm-oracle
Version:	1.8.10
Release:	1%{?dist}
Summary:	LHC Computing Grid Data Management

Group:		Applications/Internet
License:	ASL 2.0
URL:		http://glite.web.cern.ch/glite/
Source0:	%{name}-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{?fedora}%{!?fedora:0} >= 5 || %{?rhel}%{!?rhel:0} >= 5
BuildRequires:	imake
%else
%if %{?fedora}%{!?fedora:0} >= 2 || %{?rhel}%{!?rhel:0} >= 4
BuildRequires:	xorg-x11-devel
%else
BuildRequires:	XFree86-devel
%endif
%endif
BuildRequires:	globus-gssapi-gsi-devel%{?_isa}
BuildRequires:	globus-gss-assist-devel%{?_isa}
BuildRequires:	globus-gsi-credential-devel%{?_isa}
BuildRequires:	globus-gsi-callback-devel%{?_isa}
BuildRequires:	globus-gass-copy-devel%{?_isa}
BuildRequires:	globus-ftp-client-devel%{?_isa}
BuildRequires:	globus-common-devel%{?_isa}
BuildRequires:	voms-devel%{?_isa}
BuildRequires:	gsoap-devel%{?_isa}
BuildRequires:	CGSI-gSOAP-devel%{?_isa} >= 1.3.4.0
%if %{?fedora}%{!?fedora:0} >= 12 || %{?rhel}%{!?rhel:0} >= 6
BuildRequires:	libuuid-devel%{?_isa}
%else
BuildRequires:	e2fsprogs-devel%{?_isa}
%endif
# Required packages in addition to the default ones (meaning mysql ones)
BuildRequires:	wget
BuildRequires:  libaio-devel%{?_isa}
BuildRequires:  oracle-instantclient-precomp
BuildRequires:	groff

%description
Packaging of oracle servers for LCGDM.

%package -n lfc-server-oracle
Summary:	LCG File Catalog (LFC) server with Oracle database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Provides:	lfcdaemon = %{version}-%{release}
Requires:	lfc-libs%{?_isa} = %{version}
Provides:	LFC-server-oracle = %{version}-%{release}
Obsoletes:	LFC-server-oracle <= 1.8.2

Requires(pre):		shadow-utils
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts

%description -n lfc-server-oracle
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package provides an LFC server that uses Oracle as its database
back-end.

%package -n dpm-server-oracle
Summary:	Disk Pool Manager (DPM) server with Oracle database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-libs%{?_isa} = %{version}
Provides:	DPM-server-oracle = %{version}-%{release}
Obsoletes:	DPM-server-oracle <= 1.8.2

Requires(pre):		shadow-utils
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts

%description -n dpm-server-oracle
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM server that uses Oracle as its database
back-end.

%package -n dpm-name-server-oracle
Summary:	DPM name server with Oracle database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-server-oracle%{?_isa} = %{version}
Provides:	DPM-name-server-oracle = %{version}-%{release}
Obsoletes:	DPM-name-server-oracle <= 1.8.2

Requires(pre):		dpm-server-oracle
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts

%description -n dpm-name-server-oracle
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM name server that uses Oracle as its database
back-end.

%package -n dpm-copy-server-oracle
Summary:	DPM copy server with Oracle database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-server-oracle%{?_isa} = %{version}
Provides:	DPM-copy-server-oracle = %{version}-%{release}
Obsoletes:	DPM-copy-server-oracle <= 1.8.2

Requires(pre):		dpm-server-oracle
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts

%description -n dpm-copy-server-oracle
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM copy server that uses Oracle as its
database back-end.

%package -n dpm-srm-server-oracle
Summary:	DPM SRM server with Oracle database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-server-oracle%{?_isa} = %{version}
Provides:	DPM-srm-server-oracle = %{version}-%{release}
Obsoletes:	DPM-srm-server-oracle <= 1.8.2

Requires(pre):		dpm-server-oracle
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts

%description -n dpm-srm-server-oracle
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM SRM server that uses Oracle as its
database back-end.

%prep
%setup -T -q -c
%setup -q -c -n %{name}-%{version}/lfc-oracle
%setup -q -c -n %{name}-%{version}/dpm-oracle
%setup -D -T -q

for d in lfc-oracle dpm-oracle ; do

pushd $d/%{name}-%{version}

chmod 644 security/globus_gsi_gss_constants.h \
	  security/globus_i_gsi_credential.h \
	  security/gssapi_openssl.h
chmod 644 doc/lfc/INSTALL-*

# The code violates the strict aliasing rules all over the place...
# Need to use -fnostrict-aliasing so that the -O2 optimization in
# optflags doesn't try to use them.
sed 's/^CC +=/& %{optflags} -fno-strict-aliasing -fno-tree-sink/' -i config/linux.cf
sed "s/i386/'i386'/" -i config/linux.cf

popd

done

%build
gsoapversion=`soapcpp2 -v 2>&1 | grep C++ | sed 's/.* //'`

# setup proc env
oversion=`oracle-instantclient-config.%{_arch} --version`
export ORACLE_HOME=%{_libdir}/oracle/$oversion/client
export PATH=$PATH:$ORACLE_HOME/bin
oidir=%{_includedir}/oracle/$oversion/client
osdir=%{_datadir}/oracle/$oversion/client
oldir=$ORACLE_HOME/%{_lib}

pushd lfc-oracle/%{name}-%{version}

./configure lfc --with-oracle \
	--libdir=%{_lib} \
	--with-gsoap-version=$gsoapversion \
	--with-id-map-file=%{_sysconfdir}/lcgdm-mapfile \
	--with-ns-config-file=%{_sysconfdir}/NSCONFIG \
	--with-etc-dir='$(prefix)/../etc' \
	--with-emi \
	--without-argus

make -f Makefile.ini Makefiles

make %{?_smp_mflags} SOAPFLG="`pkg-config --cflags gsoap`" ORACLE_INCLUDE=$oidir ORACLE_SHARE=$osdir ORACLE_LIB=$oldir

popd

pushd dpm-oracle/%{name}-%{version}

./configure dpm --with-oracle \
	--libdir=%{_lib} \
	--with-gsoap-version=$gsoapversion \
	--with-dpm-config-file=%{_sysconfdir}/DPMCONFIG \
	--with-id-map-file=%{_sysconfdir}/lcgdm-mapfile \
	--with-ns-config-file=%{_sysconfdir}/NSCONFIG \
	--with-etc-dir='$(prefix)/../etc' \
	--with-emi \
	--without-argus

make -f Makefile.ini Makefiles

make %{?_smp_mflags} SOAPFLG="`pkg-config --cflags gsoap`" ORACLE_INCLUDE=$oidir ORACLE_SHARE=$osdir ORACLE_LIB=$oldir

popd

%install
rm -rf ${RPM_BUILD_ROOT}

pushd lfc-oracle/%{name}-%{version}

make SOAPFLG="`pkg-config --cflags gsoap`" \
     prefix=${RPM_BUILD_ROOT}%{_prefix} install install.man

touch ${RPM_BUILD_ROOT}%{_initrddir}/lfcdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/lfcdaemon
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/lfcdaemon
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/lfcdaemon
touch ${RPM_BUILD_ROOT}%{_sbindir}/lfcdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/lfcdaemon
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/lfcdaemon.8.gz
touch ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/NSCONFIG.templ
touch ${RPM_BUILD_ROOT}%{_sbindir}/lfc-shutdown
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/lfc-shutdown
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/lfc-shutdown.8.gz

# Remove static libraries
rm ${RPM_BUILD_ROOT}%{_libdir}/liblfc.a
rm ${RPM_BUILD_ROOT}%{_libdir}/liblcgdm.a

popd

pushd dpm-oracle/%{name}-%{version}

make SOAPFLG="`pkg-config --cflags gsoap`" \
     prefix=${RPM_BUILD_ROOT}%{_prefix} install install.man

touch ${RPM_BUILD_ROOT}%{_initrddir}/dpm
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/dpm
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/dpm
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/dpm
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpm
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpm
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpm.8.gz
touch ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/DPMCONFIG.templ
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpm-shutdown
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpm-shutdown
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpm-shutdown.8.gz
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpm-buildfsv
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpm-buildfsv
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpm-buildfsv.8.gz
touch ${RPM_BUILD_ROOT}%{_initrddir}/dpnsdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/dpnsdaemon
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/dpnsdaemon
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/dpnsdaemon
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpnsdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpnsdaemon
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpnsdaemon.8.gz
touch ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/DPNSCONFIG.templ
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpns-shutdown
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpns-shutdown
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpns-shutdown.8.gz

touch ${RPM_BUILD_ROOT}%{_initrddir}/dpmcopyd
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/dpmcopyd
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/dpmcopyd
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/dpmcopyd
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpmcopyd
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpmcopyd
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpmcopyd.8.gz

for svc in srmv1 srmv2 srmv2.2 ; do
  touch ${RPM_BUILD_ROOT}%{_initrddir}/${svc}
  chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/${svc}
  touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/${svc}
  touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/${svc}
  touch ${RPM_BUILD_ROOT}%{_sbindir}/${svc}
  chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/${svc}
  touch ${RPM_BUILD_ROOT}%{_mandir}/man8/${svc}.8.gz
done

# remove static library
rm ${RPM_BUILD_ROOT}%{_libdir}/libdpm.a
rm ${RPM_BUILD_ROOT}%{_libdir}/liblcgdm.a

popd

# remove the log man page (already exists in the system and not needed anyway)
rm ${RPM_BUILD_ROOT}%{_mandir}/man3/log.3*

# Add the upgrade scripts 
mkdir ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades
install -m 755 lfc-oracle/%{name}-%{version}/scripts/upgrades/*.pm ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades
install -m 755 lfc-oracle/%{name}-%{version}/scripts/upgrades/cns-db* ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades
install -m 755 lfc-oracle/%{name}-%{version}/scripts/upgrades/dpm-db* ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades

# Add the LFC info provider script
mkdir ${RPM_BUILD_ROOT}%{_libexecdir}
install -m 755 lfc-oracle/%{name}-%{version}/scripts/lcg-info-provider-lfc ${RPM_BUILD_ROOT}%{_libexecdir}

%clean
rm -rf ${RPM_BUILD_ROOT}

%pre -n lfc-server-oracle
getent group lfcmgr > /dev/null || groupadd -r lfcmgr
getent passwd lfcmgr > /dev/null || useradd -r -g lfcmgr \
    -d %{_localstatedir}/lib/lfc -s /bin/bash -c "LFC Manager" lfcmgr
exit 0

%post -n lfc-server-oracle
if [ -e /etc/init.d/lfcdaemon -a ! -h /etc/init.d/lfcdaemon ]; then
	rm /etc/init.d/lfcdaemon
fi
%{_sbindir}/update-alternatives --install %{_sbindir}/lfcdaemon lfcdaemon \
	  %{_libdir}/lfc-oracle/lfcdaemon 20 \
  --slave %{_mandir}/man8/lfcdaemon.8.gz lfcdaemon.8.gz \
	  %{_libdir}/lfc-oracle/lfcdaemon.8.gz \
  --slave %{_datadir}/lcgdm/NSCONFIG.templ NSCONFIG.templ \
	  %{_libdir}/lfc-oracle/NSCONFIG.templ \
  --slave %{_initrddir}/lfcdaemon lfcdaemon.init \
	  %{_sysconfdir}/lfc-oracle/lfcdaemon.init \
  --slave %{_sysconfdir}/sysconfig/lfcdaemon lfcdaemon.conf \
	  %{_sysconfdir}/lfc-oracle/lfcdaemon.conf \
  --slave %{_sysconfdir}/logrotate.d/lfcdaemon lfcdaemon.logrotate \
	  %{_sysconfdir}/lfc-oracle/lfcdaemon.logrotate \
  --slave %{_sbindir}/lfc-shutdown lfc-shutdown \
	  %{_libdir}/lfc-oracle/lfc-shutdown \
  --slave %{_mandir}/man8/lfc-shutdown.8.gz lfc-shutdown.8.gz \
	  %{_libdir}/lfc-oracle/lfc-shutdown.8.gz

if [ $1 = 1 ]; then
    if [ -r %{_initrddir}/lfcdaemon ]; then
	/sbin/chkconfig --add lfcdaemon
    fi
fi

%preun -n lfc-server-oracle
export LANG=C

if [ $1 = 0 ]; then
    %{_sbindir}/update-alternatives --display lfcdaemon | \
	grep currently | grep -q lfc-oracle && \
	/sbin/service lfcdaemon stop > /dev/null 2>&1 || :
    %{_sbindir}/update-alternatives --remove lfcdaemon \
	%{_libdir}/lfc-oracle/lfcdaemon
    %{_sbindir}/update-alternatives --display lfcdaemon > /dev/null || \
	/sbin/chkconfig --del lfcdaemon > /dev/null 2>&1 || :
fi

%postun -n lfc-server-oracle
export LANG=C

if [ $1 -ge 1 ]; then
    %{_sbindir}/update-alternatives --display lfcdaemon | \
	grep currently | grep -q lfc-oracle && \
	/sbin/service lfcdaemon condrestart > /dev/null 2>&1 || :
fi

%post -n dpm-server-oracle
if [ -e /etc/init.d/dpm -a ! -h /etc/init.d/dpm ]; then
	rm /etc/init.d/dpm
fi
%{_sbindir}/update-alternatives --install %{_sbindir}/dpm dpm \
	  %{_libdir}/dpm-oracle/dpm 20 \
  --slave %{_mandir}/man8/dpm.8.gz dpm.8.gz \
	  %{_libdir}/dpm-oracle/dpm.8.gz \
  --slave %{_datadir}/lcgdm/DPMCONFIG.templ DPMCONFIG.templ \
	  %{_libdir}/dpm-oracle/DPMCONFIG.templ \
  --slave %{_initrddir}/dpm dpm.init \
	  %{_sysconfdir}/dpm-oracle/dpm.init \
  --slave %{_sysconfdir}/sysconfig/dpm dpm.conf \
	  %{_sysconfdir}/dpm-oracle/dpm.conf \
  --slave %{_sysconfdir}/logrotate.d/dpm dpm.logrotate \
	  %{_sysconfdir}/dpm-oracle/dpm.logrotate \
  --slave %{_sbindir}/dpm-buildfsv dpm-buildfsv \
	  %{_libdir}/dpm-oracle/dpm-buildfsv \
  --slave %{_mandir}/man8/dpm-buildfsv.8.gz dpm-buildfsv.8.gz \
	  %{_libdir}/dpm-oracle/dpm-buildfsv.8.gz \
  --slave %{_sbindir}/dpm-shutdown dpm-shutdown \
	  %{_libdir}/dpm-oracle/dpm-shutdown \
  --slave %{_mandir}/man8/dpm-shutdown.8.gz dpm-shutdown.8.gz \
	  %{_libdir}/dpm-oracle/dpm-shutdown.8.gz

if [ $1 = 1 ]; then
    if [ -r %{_initrddir}/dpm ]; then
	/sbin/chkconfig --add dpm
    fi
fi

%preun -n dpm-server-oracle
export LANG=C

if [ $1 = 0 ]; then
    %{_sbindir}/update-alternatives --display dpm | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service dpm stop > /dev/null 2>&1 || :
    %{_sbindir}/update-alternatives --remove dpm \
	%{_libdir}/dpm-oracle/dpm
    %{_sbindir}/update-alternatives --display dpm > /dev/null || \
	/sbin/chkconfig --del dpm > /dev/null 2>&1 || :
fi

%postun -n dpm-server-oracle
export LANG=C

if [ $1 -ge 1 ]; then
    %{_sbindir}/update-alternatives --display dpm | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service dpm condrestart > /dev/null 2>&1 || :
fi

%post -n dpm-name-server-oracle
if [ -e /etc/init.d/dpnsdaemon -a ! -h /etc/init.d/dpnsdaemon ]; then
	rm /etc/init.d/dpnsdaemon
fi
%{_sbindir}/update-alternatives --install %{_sbindir}/dpnsdaemon dpnsdaemon \
	  %{_libdir}/dpm-oracle/dpnsdaemon 20 \
  --slave %{_mandir}/man8/dpnsdaemon.8.gz dpnsdaemon.8.gz \
	  %{_libdir}/dpm-oracle/dpnsdaemon.8.gz \
  --slave %{_datadir}/lcgdm/DPNSCONFIG.templ DPNSCONFIG.templ \
	  %{_libdir}/dpm-oracle/DPNSCONFIG.templ \
  --slave %{_initrddir}/dpnsdaemon dpnsdaemon.init \
	  %{_sysconfdir}/dpm-oracle/dpnsdaemon.init \
  --slave %{_sysconfdir}/sysconfig/dpnsdaemon dpnsdaemon.conf \
	  %{_sysconfdir}/dpm-oracle/dpnsdaemon.conf \
  --slave %{_sysconfdir}/logrotate.d/dpnsdaemon dpnsdaemon.logrotate \
	  %{_sysconfdir}/dpm-oracle/dpnsdaemon.logrotate \
  --slave %{_sbindir}/dpns-shutdown dpns-shutdown \
	  %{_libdir}/dpm-oracle/dpns-shutdown \
  --slave %{_mandir}/man8/dpns-shutdown.8.gz dpns-shutdown.8.gz \
	  %{_libdir}/dpm-oracle/dpns-shutdown.8.gz

if [ $1 = 1 ]; then
    if [ -r %{_initrddir}/dpnsdaemon ]; then
	/sbin/chkconfig --add dpnsdaemon
    fi
fi

%preun -n dpm-name-server-oracle
export LANG=C

if [ $1 = 0 ]; then
    %{_sbindir}/update-alternatives --display dpnsdaemon | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service dpnsdaemon stop > /dev/null 2>&1 || :
    %{_sbindir}/update-alternatives --remove dpnsdaemon \
	%{_libdir}/dpm-oracle/dpnsdaemon
    %{_sbindir}/update-alternatives --display dpnsdaemon > /dev/null || \
	/sbin/chkconfig --del dpnsdaemon > /dev/null 2>&1 || :
fi

%postun -n dpm-name-server-oracle
export LANG=C

if [ $1 -ge 1 ]; then
    %{_sbindir}/update-alternatives --display dpnsdaemon | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service dpnsdaemon condrestart > /dev/null 2>&1 || :
fi

%post -n dpm-copy-server-oracle
if [ -e /etc/init.d/dpmcopyd -a ! -h /etc/init.d/dpmcopyd ]; then
	rm /etc/init.d/dpmcopyd
fi
%{_sbindir}/update-alternatives --install %{_sbindir}/dpmcopyd dpmcopyd \
	  %{_libdir}/dpm-oracle/dpmcopyd 20 \
  --slave %{_mandir}/man8/dpmcopyd.8.gz dpmcopyd.8.gz \
	  %{_libdir}/dpm-oracle/dpmcopyd.8.gz \
  --slave %{_initrddir}/dpmcopyd dpmcopyd.init \
	  %{_sysconfdir}/dpm-oracle/dpmcopyd.init \
  --slave %{_sysconfdir}/sysconfig/dpmcopyd dpmcopyd.conf \
	  %{_sysconfdir}/dpm-oracle/dpmcopyd.conf \
  --slave %{_sysconfdir}/logrotate.d/dpmcopyd dpmcopyd.logrotate \
	  %{_sysconfdir}/dpm-oracle/dpmcopyd.logrotate

if [ $1 = 1 ]; then
    if [ -r %{_initrddir}/dpmcopyd ]; then
	/sbin/chkconfig --add dpmcopyd
    fi
fi

%preun -n dpm-copy-server-oracle
export LANG=C

if [ $1 = 0 ]; then
    %{_sbindir}/update-alternatives --display dpmcopyd | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service dpmcopyd stop > /dev/null 2>&1 || :
    %{_sbindir}/update-alternatives --remove dpmcopyd \
	%{_libdir}/dpm-oracle/dpmcopyd
    %{_sbindir}/update-alternatives --display dpmcopyd > /dev/null || \
	/sbin/chkconfig --del dpmcopyd > /dev/null 2>&1 || :
fi

%postun -n dpm-copy-server-oracle
export LANG=C

if [ $1 -ge 1 ]; then
    %{_sbindir}/update-alternatives --display dpmcopyd | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service dpmcopyd condrestart > /dev/null 2>&1 || :
fi

%post -n dpm-srm-server-oracle
if [ -e /etc/init.d/srmv1 -a ! -h /etc/init.d/srmv1 ]; then
    rm /etc/init.d/srmv1;
fi
if [ -e /etc/init.d/srmv2 -a ! -h /etc/init.d/srmv2 ]; then
    rm /etc/init.d/srmv2
fi
if [ -e /etc/init.d/srmv2.2 -a ! -h /etc/init.d/srmv2.2 ]; then
    rm /etc/init.d/srmv2.2
fi
%{_sbindir}/update-alternatives --install %{_sbindir}/srmv1 srmv1 \
	  %{_libdir}/dpm-oracle/srmv1 20 \
  --slave %{_mandir}/man8/srmv1.8.gz srmv1.8.gz \
	  %{_libdir}/dpm-oracle/srmv1.8.gz \
  --slave %{_initrddir}/srmv1 srmv1.init \
	  %{_sysconfdir}/dpm-oracle/srmv1.init \
  --slave %{_sysconfdir}/sysconfig/srmv1 srmv1.conf \
	  %{_sysconfdir}/dpm-oracle/srmv1.conf \
  --slave %{_sysconfdir}/logrotate.d/srmv1 srmv1.logrotate \
	  %{_sysconfdir}/dpm-oracle/srmv1.logrotate

%{_sbindir}/update-alternatives --install %{_sbindir}/srmv2 srmv2 \
	  %{_libdir}/dpm-oracle/srmv2 20 \
  --slave %{_mandir}/man8/srmv2.8.gz srmv2.8.gz \
	  %{_libdir}/dpm-oracle/srmv2.8.gz \
  --slave %{_initrddir}/srmv2 srmv2.init \
	  %{_sysconfdir}/dpm-oracle/srmv2.init \
  --slave %{_sysconfdir}/sysconfig/srmv2 srmv2.conf \
	  %{_sysconfdir}/dpm-oracle/srmv2.conf \
  --slave %{_sysconfdir}/logrotate.d/srmv2 srmv2.logrotate \
	  %{_sysconfdir}/dpm-oracle/srmv2.logrotate

%{_sbindir}/update-alternatives --install %{_sbindir}/srmv2.2 srmv2.2 \
	  %{_libdir}/dpm-oracle/srmv2.2 20 \
  --slave %{_mandir}/man8/srmv2.2.8.gz srmv2.2.8.gz \
	  %{_libdir}/dpm-oracle/srmv2.2.8.gz \
  --slave %{_initrddir}/srmv2.2 srmv2.2.init \
	  %{_sysconfdir}/dpm-oracle/srmv2.2.init \
  --slave %{_sysconfdir}/sysconfig/srmv2.2 srmv2.2.conf \
	  %{_sysconfdir}/dpm-oracle/srmv2.2.conf \
  --slave %{_sysconfdir}/logrotate.d/srmv2.2 srmv2.2.logrotate \
	  %{_sysconfdir}/dpm-oracle/srmv2.2.logrotate

if [ $1 = 1 ]; then
    if [ -r %{_initrddir}/srmv1 ]; then
	/sbin/chkconfig --add srmv1;
    fi
    if [ -r %{_initrddir}/srmv2 ]; then
	/sbin/chkconfig --add srmv2
    fi
    if [ -r %{_initrddir}/srmv2.2 ]; then
	/sbin/chkconfig --add srmv2.2
    fi
fi

%preun -n dpm-srm-server-oracle
export LANG=C

if [ $1 = 0 ]; then
    %{_sbindir}/update-alternatives --display srmv1 | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service srmv1 stop > /dev/null 2>&1 || :
    %{_sbindir}/update-alternatives --remove srmv1 \
	%{_libdir}/dpm-oracle/srmv1
    %{_sbindir}/update-alternatives --display srmv1 > /dev/null || \
	/sbin/chkconfig --del srmv1 > /dev/null 2>&1 || :

    %{_sbindir}/update-alternatives --display srmv2 | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service srmv2 stop > /dev/null 2>&1 || :
    %{_sbindir}/update-alternatives --remove srmv2 \
	%{_libdir}/dpm-oracle/srmv2
    %{_sbindir}/update-alternatives --display srmv2 > /dev/null || \
	/sbin/chkconfig --del srmv2 > /dev/null 2>&1 || :

    %{_sbindir}/update-alternatives --display srmv2.2 | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service srmv2.2 stop > /dev/null 2>&1 || :
    %{_sbindir}/update-alternatives --remove srmv2.2 \
	%{_libdir}/dpm-oracle/srmv2.2
    %{_sbindir}/update-alternatives --display srmv2.2 > /dev/null || \
	/sbin/chkconfig --del srmv2.2 > /dev/null 2>&1 || :
fi

%postun -n dpm-srm-server-oracle
export LANG=C

if [ $1 -ge 1 ]; then
    %{_sbindir}/update-alternatives --display srmv1 | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service srmv1 condrestart > /dev/null 2>&1 || :

    %{_sbindir}/update-alternatives --display srmv2 | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service srmv2 condrestart > /dev/null 2>&1 || :

    %{_sbindir}/update-alternatives --display srmv2.2 | \
	grep currently | grep -q dpm-oracle && \
	/sbin/service srmv2.2 condrestart > /dev/null 2>&1 || :
fi

%files -n lfc-server-oracle
%defattr(-,root,root,-)
%dir %{_libdir}/lfc-oracle
%{_libdir}/lfc-oracle/lfcdaemon
%ghost %{_sbindir}/lfcdaemon
%{_libdir}/lfc-oracle/lfc-shutdown
%ghost %{_sbindir}/lfc-shutdown
%{_libdir}/lfc-oracle/NSCONFIG.templ
%ghost %{_datadir}/lcgdm/NSCONFIG.templ
%doc %{_libdir}/lfc-oracle/lfcdaemon.8*
%ghost %{_mandir}/man8/lfcdaemon.8*
%doc %{_libdir}/lfc-oracle/lfc-shutdown.8*
%ghost %{_mandir}/man8/lfc-shutdown.8*
%dir %{_sysconfdir}/lfc-oracle
%{_sysconfdir}/lfc-oracle/lfcdaemon.init
%ghost %{_initrddir}/lfcdaemon
%config(noreplace) %{_sysconfdir}/lfc-oracle/lfcdaemon.conf
%ghost %{_sysconfdir}/sysconfig/lfcdaemon
%config(noreplace) %{_sysconfdir}/lfc-oracle/lfcdaemon.logrotate
%ghost %{_sysconfdir}/logrotate.d/lfcdaemon
%doc %{_datadir}/lcgdm/create_lfc_tables_oracle.sql
%{_datadir}/lcgdm/upgrades/cns-db-*

%files -n dpm-server-oracle
%defattr(-,root,root,-)
%dir %{_libdir}/dpm-oracle
%{_libdir}/dpm-oracle/dpm
%ghost %{_sbindir}/dpm
%{_libdir}/dpm-oracle/dpm-shutdown
%ghost %{_sbindir}/dpm-shutdown
%{_libdir}/dpm-oracle/dpm-buildfsv
%ghost %{_sbindir}/dpm-buildfsv
%doc %{_libdir}/dpm-oracle/dpm.8*
%ghost %{_mandir}/man8/dpm.8*
%doc %{_libdir}/dpm-oracle/dpm-shutdown.8*
%ghost %{_mandir}/man8/dpm-shutdown.8*
%doc %{_libdir}/dpm-oracle/dpm-buildfsv.8*
%ghost %{_mandir}/man8/dpm-buildfsv.8*
%{_libdir}/dpm-oracle/DPMCONFIG.templ
%ghost %{_datadir}/lcgdm/DPMCONFIG.templ
%dir %{_sysconfdir}/dpm-oracle
%{_sysconfdir}/dpm-oracle/dpm.init
%ghost %{_initrddir}/dpm
%config(noreplace) %{_sysconfdir}/dpm-oracle/dpm.conf
%ghost %{_sysconfdir}/sysconfig/dpm
%config(noreplace) %{_sysconfdir}/dpm-oracle/dpm.logrotate
%ghost %{_sysconfdir}/logrotate.d/dpm
%{_datadir}/lcgdm/create_dpm_tables_oracle.sql
%{_datadir}/lcgdm/upgrades/dpm-db-*

%files -n dpm-name-server-oracle
%defattr(-,root,root,-)
%{_libdir}/dpm-oracle/dpnsdaemon
%ghost %{_sbindir}/dpnsdaemon
%{_libdir}/dpm-oracle/dpns-shutdown
%ghost %{_sbindir}/dpns-shutdown
%doc %{_libdir}/dpm-oracle/dpnsdaemon.8*
%ghost %{_mandir}/man8/dpnsdaemon.8*
%doc %{_libdir}/dpm-oracle/dpns-shutdown.8*
%ghost %{_mandir}/man8/dpns-shutdown.8*
%{_libdir}/dpm-oracle/NSCONFIG.templ
%ghost %{_datadir}/lcgdm/DPNSCONFIG.templ
%{_sysconfdir}/dpm-oracle/dpnsdaemon.init
%ghost %{_initrddir}/dpnsdaemon
%config(noreplace) %{_sysconfdir}/dpm-oracle/dpnsdaemon.conf
%ghost %{_sysconfdir}/sysconfig/dpnsdaemon
%config(noreplace) %{_sysconfdir}/dpm-oracle/dpnsdaemon.logrotate
%ghost %{_sysconfdir}/logrotate.d/dpnsdaemon
%{_datadir}/lcgdm/create_dpns_tables_oracle.sql
%{_datadir}/lcgdm/upgrades/cns-db-*

%files -n dpm-copy-server-oracle
%defattr(-,root,root,-)
%{_libdir}/dpm-oracle/dpmcopyd
%ghost %{_sbindir}/dpmcopyd
%doc %{_libdir}/dpm-oracle/dpmcopyd.8*
%ghost %{_mandir}/man8/dpmcopyd.8*
%{_sysconfdir}/dpm-oracle/dpmcopyd.init
%ghost %{_initrddir}/dpmcopyd
%config(noreplace) %{_sysconfdir}/dpm-oracle/dpmcopyd.conf
%ghost %{_sysconfdir}/sysconfig/dpmcopyd
%config(noreplace) %{_sysconfdir}/dpm-oracle/dpmcopyd.logrotate
%ghost %{_sysconfdir}/logrotate.d/dpmcopyd

%files -n dpm-srm-server-oracle
%defattr(-,root,root,-)
%{_libdir}/dpm-oracle/srmv1
%{_libdir}/dpm-oracle/srmv2
%{_libdir}/dpm-oracle/srmv2.2
%ghost %{_sbindir}/srmv1
%ghost %{_sbindir}/srmv2
%ghost %{_sbindir}/srmv2.2
%doc %{_libdir}/dpm-oracle/srmv1.8*
%doc %{_libdir}/dpm-oracle/srmv2.8*
%doc %{_libdir}/dpm-oracle/srmv2.2.8*
%ghost %{_mandir}/man8/srmv1.8*
%ghost %{_mandir}/man8/srmv2.8*
%ghost %{_mandir}/man8/srmv2.2.8*
%{_sysconfdir}/dpm-oracle/srmv1.init
%{_sysconfdir}/dpm-oracle/srmv2.init
%{_sysconfdir}/dpm-oracle/srmv2.2.init
%ghost %{_initrddir}/srmv1
%ghost %{_initrddir}/srmv2
%ghost %{_initrddir}/srmv2.2
%config(noreplace) %{_sysconfdir}/dpm-oracle/srmv1.conf
%config(noreplace) %{_sysconfdir}/dpm-oracle/srmv2.conf
%config(noreplace) %{_sysconfdir}/dpm-oracle/srmv2.2.conf
%ghost %{_sysconfdir}/sysconfig/srmv1
%ghost %{_sysconfdir}/sysconfig/srmv2
%ghost %{_sysconfdir}/sysconfig/srmv2.2
%config(noreplace) %{_sysconfdir}/dpm-oracle/srmv1.logrotate
%config(noreplace) %{_sysconfdir}/dpm-oracle/srmv2.logrotate
%config(noreplace) %{_sysconfdir}/dpm-oracle/srmv2.2.logrotate
%ghost %{_sysconfdir}/logrotate.d/srmv1
%ghost %{_sysconfdir}/logrotate.d/srmv2
%ghost %{_sysconfdir}/logrotate.d/srmv2.2

%changelog
* Thu Aug 08 2013 Oliver Keeble <oliver.keeble@cern.ch> - 1.8.7-4
- Allow building against rpm-packaged oracle precompiler

* Wed Jan 30 2013 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.6-3
- Removed dependency on release number to base packages

* Tue Jan 29 2013 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.6-2
- Add patch for issue with gss authz and oracle 11

* Mon Dec 10 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.6-1
- Update for new upstream release

* Thu Nov 01 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.5-1
- Update for new upstream release

* Mon Sep 03 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.4-1
- Update for new upstream release

* Mon Jun 11 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3-2
- Follow up with main lcgdm release

* Wed Mar 21 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3-1
- First packaging of oracle servers
