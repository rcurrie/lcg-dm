/*
 * Copyright (C) 2004-2008 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_permreq.c,v $ $Revision: 1.15 $ $Date: 2008/09/24 11:25:01 $ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <sys/types.h>
#include <Cgrp.h>
#include "Cnetdb.h"
#include <Cpwd.h>
#include "dpm.h"
#include "dpm_server.h"
#include "dpm_util.h"
#include "dpns_api.h"
#include "serrno.h"
#include "srm_server.h"
#include "srmv2H.h"
static enum ns1__TPermissionMode f_modes[] = {None, X, W, WX, R, RX, RW, RWX};
static int cvt_entries(struct ns1__srmSetPermissionRequest *, struct Cns_acl *);
static int na_key = -1;

/*			Permission Functions				*/

int
ns1__srmSetPermission (struct soap *soap, struct ns1__srmSetPermissionRequest *req, struct ns1__srmSetPermissionResponse_ *rep)
{
	struct Cns_acl *acl;
	struct Cns_acl *aclp;
	char clientdn[256];
	const char *clienthost;
	int found;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	struct Cns_acl inpacl[CA_MAXACLENTRIES];
	int j;
	int k;
	int nb_inp_entries;
	int nb_tmp_entries;
	int nbfqans;
	int nbgids;
	int nentries;
	int nocheckid;
	int ng = 0;
	int nm = 0;
	int nu = 0;
	struct ns1__srmSetPermissionResponse *repp;
	char *sfn;
	struct Cns_acl *taclp;
	struct srm_srv_thread_info *thip = soap->user;
	struct Cns_acl tmpacl[CA_MAXACLENTRIES];
	uid_t uid;
	char *voname;

	strcpy (func, "SetPermission");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmSetPermissionResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmSetPermissionResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}

	if (! req->path || ! req->path->SURLOrStFN ||
	    ! req->path->SURLOrStFN->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "path is required");
		RETURN (SOAP_OK);
	}
	if ((sfn = sfnfromsurl (req->path->SURLOrStFN->value)) == NULL) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREPATH;
		RETURN (SOAP_OK);
	}
	if ((nb_inp_entries = cvt_entries (req, inpacl)) <= 0) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		RETURN (SOAP_OK);
	}

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	if (req->permissionType == ADD || req->permissionType == REMOVE ||
	    req->permissionType == CHANGE) {
		if ((nb_tmp_entries = Cns_getacl (sfn, CA_MAXACLENTRIES, tmpacl)) < 0) {
			repp->returnStatus->explanation = soap_strdup (soap, sstrerror (serrno));
			repp->returnStatus->statusCode = serrno2statuscode (serrno);
			RETURN (SOAP_OK);
		}
		if (req->permissionType == REMOVE) {
			for (aclp = inpacl, j = 0; j < nb_inp_entries; aclp++, j++) {
				nocheckid = aclp->a_type == CNS_ACL_USER_OBJ ||
					    aclp->a_type == CNS_ACL_GROUP_OBJ ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ) ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP_OBJ);
				for (taclp = tmpacl, k = 0; k < nb_tmp_entries; taclp++, k++) {
					if (aclp->a_type == taclp->a_type &&
					    (aclp->a_id == taclp->a_id ||
					    nocheckid)) {
						nb_tmp_entries--;
						if (k < nb_tmp_entries)
							memcpy (taclp, taclp + 1,
							    (nb_tmp_entries - k) * sizeof (struct Cns_acl));
						break;
					}
				}
			}
		} else {
			for (aclp = inpacl, j = 0; j < nb_inp_entries; aclp++, j++) {
				found = 0;
				nocheckid = aclp->a_type == CNS_ACL_USER_OBJ ||
					    aclp->a_type == CNS_ACL_GROUP_OBJ ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ) ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP_OBJ);
				for (taclp = tmpacl, k = 0; k < nb_tmp_entries; taclp++, k++) {
					if ((aclp->a_type == taclp->a_type ||
					    (aclp->a_type == CNS_ACL_GROUP &&
					     taclp->a_type == CNS_ACL_GROUP_OBJ)) &&
					    (aclp->a_id == taclp->a_id ||
					    nocheckid)) {
						found++;
						taclp->a_perm = aclp->a_perm;
						break;
					}
				}
				if (! found) {
					if (req->permissionType != ADD) {
						repp->returnStatus->statusCode =
						    SRM_USCOREINVALID_USCOREREQUEST;
						RETURN (SOAP_OK);
					}
					if (nb_tmp_entries >= CA_MAXACLENTRIES) {
						repp->returnStatus->explanation =
						    soap_strdup (soap, "Too many ACL entries");
						repp->returnStatus->statusCode =
						    SRM_USCOREFAILURE;
						RETURN (SOAP_OK);
					}
					memcpy (taclp, aclp, sizeof(struct Cns_acl));
					nb_tmp_entries++;
				}
			}
		}
		acl = tmpacl;
		nentries = nb_tmp_entries;
	} else {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		RETURN (SOAP_OK);
	}

	/* If there is any USER or GROUP entry, there must be a MASK entry.
	 * If there is no mask entry, generate one */

	for (aclp = acl, j = 0; j < nentries; aclp++, j++) {
		if (aclp->a_type == CNS_ACL_USER) nu++;
		else if (aclp->a_type == CNS_ACL_GROUP) ng++;
		else if (aclp->a_type == CNS_ACL_MASK) nm++;
	}
	if ((nu || ng) && nm != 1) {
		aclp->a_type = CNS_ACL_MASK;
		aclp->a_id = 0;
		aclp->a_perm = 07;
		nentries++;
	}

	if (Cns_setacl (sfn, nentries, acl)) {
		repp->returnStatus->explanation = soap_strdup (soap, sstrerror (serrno));
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

static int
cvt_entries (struct ns1__srmSetPermissionRequest *req, struct Cns_acl *acl)
{
	int i;
	int nentries = 0;

	if (req->ownerPermission) {
		acl->a_type = CNS_ACL_USER_OBJ;
		acl->a_id = 0;
		acl->a_perm = req->ownerPermission->mode;
		acl++;
		nentries++;
	}
	if (req->userPermission) {
		for (i = 0; i < req->userPermission->__sizeuserPermissionArray; i++) {
			if (! req->userPermission->userPermissionArray[i])
				continue;
			acl->a_type = CNS_ACL_USER;
			if (! req->userPermission->userPermissionArray[i]->userID ||
			    ! req->userPermission->userPermissionArray[i]->userID->value ||
			    (acl->a_id = cvt_user (req->userPermission->userPermissionArray[i]->userID->value)) < 0)
				return (-1);
			acl->a_perm = req->userPermission->userPermissionArray[i]->mode;
			acl++;
			nentries++;
		}
	}
	if (req->groupPermission) {
		for (i = 0; i < req->groupPermission->__sizegroupPermissionArray; i++) {
			if (! req->groupPermission->groupPermissionArray[i])
				continue;
			acl->a_type = CNS_ACL_GROUP;
			if (! req->groupPermission->groupPermissionArray[i]->groupID ||
			    ! req->groupPermission->groupPermissionArray[i]->groupID->value ||
			    (acl->a_id = cvt_group (req->groupPermission->groupPermissionArray[i]->groupID->value)) < 0)
				return (-1);
			acl->a_perm = req->groupPermission->groupPermissionArray[i]->mode;
			acl++;
			nentries++;
		}
	}
	if (req->otherPermission) {
		acl->a_type = CNS_ACL_OTHER;
		acl->a_id = 0;
		acl->a_perm = req->otherPermission->mode;
		acl++;
		nentries++;
	}
	return (nentries);
}

cvt_group (char *p)
{
	gid_t gid;
	struct group *gr;

#ifdef VIRTUAL_ID
	if (strcmp (p, "root") == 0)
		gid = 0;
	else if (Cns_getgrpbynam (p, &gid) < 0) {
#else
	if ((gr = Cgetgrnam (p)))
		gid = gr->gr_gid;
	else {
#endif
		return (-1);
	}
	return (gid);
}

cvt_user (char *p)
{
	struct passwd *pwd;
	uid_t uid;

#ifdef VIRTUAL_ID
	if (strcmp (p, "root") == 0)
		uid = 0;
	else if (Cns_getusrbynam (p, &uid) < 0) {
#else
	if ((pwd = Cgetpwnam (p)))
		uid = pwd->pw_uid;
	else {
#endif
		return (-1);
	}
	return (uid);
}

int
ns1__srmReassignToUser (struct soap *soap, struct ns1__srmReassignToUserRequest *req, struct ns1__srmReassignToUserResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[16];
	struct ns1__srmReassignToUserResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "ReassignToUser");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmReassignToUserResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	rep->srmReassignToUserResponse = repp;
	RETURN (SOAP_OK);
}

int
ns1__srmCheckPermission (struct soap *soap, struct ns1__srmCheckPermissionRequest *req, struct ns1__srmCheckPermissionResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int mode;
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbsurls;
	struct ns1__TSURLPermissionReturn *repfilep;
	struct ns1__srmCheckPermissionResponse *repp;
	char *sfn;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "CheckPermission");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmCheckPermissionResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfPermissions = NULL;
	rep->srmCheckPermissionResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}

	if (! req->arrayOfSiteURLs) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSiteURLs is required");
		RETURN (SOAP_OK);
	}

	nbsurls = req->arrayOfSiteURLs->__sizesurlInfoArray;

	/* Allocate the array of file permissions */

	if ((repp->arrayOfPermissions =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLPermissionReturn))) == NULL ||
	    (repp->arrayOfPermissions->surlPermissionArray =
	    soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLPermissionReturn *))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->arrayOfPermissions->__sizesurlPermissionArray = nbsurls;

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	for (i = 0; i < nbsurls; i++) {
		if ((repp->arrayOfPermissions->surlPermissionArray[i] = repfilep =
		    soap_malloc (soap, sizeof(struct ns1__TSURLPermissionReturn))) == NULL)
			RETURN (SOAP_EOM);
		memset (repfilep, 0, sizeof(struct ns1__TSURLPermissionReturn));
		if ((repp->arrayOfPermissions->surlPermissionArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repfilep->status, 0, sizeof(struct ns1__TReturnStatus));
		repfilep->surl = req->arrayOfSiteURLs->surlInfoArray[i]->SURLOrStFN;
		if (! repfilep->surl->value) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			repfilep->status->explanation = soap_strdup (soap, "Pointer to SURL is NULL");
			nb_file_err++;
			continue;
		}
		if (strlen (repfilep->surl->value) >= CA_MAXSFNLEN ||
		    (sfn = sfnfromsurl (repfilep->surl->value)) == NULL) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		mode = 0;
		if (Cns_access (sfn, R_OK) < 0) {
			if (serrno != EACCES) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				continue;
			}
		} else
			mode |= 4;
		if (Cns_access (sfn, W_OK) < 0) {
			if (serrno != EACCES) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				continue;
			}
		} else
			mode |= 2;
		if (Cns_access (sfn, X_OK) < 0) {
			if (serrno != EACCES) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				continue;
			}
		} else
			mode |= 1;
		repfilep->userPermission = &f_modes[mode];
	}
	repp->returnStatus->statusCode = (nb_file_err == nbsurls) ?
		SRM_USCOREFAILURE : SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}
