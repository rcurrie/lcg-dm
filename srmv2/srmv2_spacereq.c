/*
 * Copyright (C) 2004-2011 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_spacereq.c,v $ $Revision$ $Date$ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <sys/types.h>
#include "Cnetdb.h"
#include "dpm.h"
#include "dpm_api.h"
#include "dpm_server.h"
#include "serrno.h"
#include "srm_server.h"
#include "srmv2H.h"
extern int Cgroupmatch (gid_t, int, gid_t *);
extern char *decode_group(gid_t, gid_t *, char *);
extern char *decode_user(uid_t, uid_t *, char *);
static enum ns1__TSpaceType s_types[] = {Volatile, Durable, Permanent};
static int na_key = -1;

/*			Space Management Functions			*/

int
ns1__srmReserveSpace (struct soap *soap, struct ns1__srmReserveSpaceRequest *req, struct ns1__srmReserveSpaceResponse_ *rep)
{
	u_signed64 actual_g_space;
	time_t actual_lifetime;
	char actual_s_type;
	u_signed64 actual_t_space;
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int nbfqans;
	int nbgids;
	struct ns1__srmReserveSpaceResponse *repp;
	u_signed64 req_g_space;
	time_t req_lifetime;
	u_signed64 req_t_space;
	char s_token[CA_MAXDPMTOKENLEN+1];
	char s_type;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "ReserveSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmReserveSpaceResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmReserveSpaceResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmReserveSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (req->typeOfSpace == Volatile)
		s_type = 'V';
	else if (req->typeOfSpace == Durable)
		s_type = 'D';
	else if (req->typeOfSpace == Permanent)
		s_type = 'P';
	else {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid type of space");
		RETURN (SOAP_OK);
	}
	if (req->userSpaceTokenDescription &&
	    strlen (req->userSpaceTokenDescription) >= 256) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation =
		    soap_strdup (soap, "Invalid user space token description");
		RETURN (SOAP_OK);
	}
	if (req->sizeOfTotalSpaceDesired)
		req_t_space = req->sizeOfTotalSpaceDesired->value;
	else
		req_t_space = 0;
	if (req->sizeOfGuaranteedSpaceDesired)
		req_g_space = req->sizeOfGuaranteedSpaceDesired->value;
	else
		req_g_space = 0;
	if (req->lifetimeOfSpaceToReserve)
		req_lifetime = req->lifetimeOfSpaceToReserve->value;
	else
		req_lifetime = 0;

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_reservespace (s_type, req->userSpaceTokenDescription, '\0', 'O',
	    req_t_space, req_g_space, req_lifetime, 0, NULL, NULL,
	    &actual_s_type, &actual_t_space, &actual_g_space, &actual_lifetime,
	    s_token) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURN (SOAP_OK);
	}
	if (actual_s_type == 'V')
		repp->typeOfReservedSpace = &s_types[Volatile];
	else if (actual_s_type == 'D')
		repp->typeOfReservedSpace = &s_types[Durable];
	else
		repp->typeOfReservedSpace = &s_types[Permanent];
	if ((repp->sizeOfTotalReservedSpace = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repp->sizeOfTotalReservedSpace->value = actual_t_space;
	if ((repp->sizeOfGuaranteedReservedSpace = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repp->sizeOfGuaranteedReservedSpace->value = actual_g_space;
	if ((repp->lifetimeOfReservedSpace = soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))))
		repp->lifetimeOfReservedSpace->value = actual_lifetime;
	if ((repp->referenceHandleOfReservedSpace = soap_malloc (soap, sizeof(struct ns1__TSpaceToken))))
		repp->referenceHandleOfReservedSpace->value = soap_strdup (soap, s_token);
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmReleaseSpace (struct soap *soap, struct ns1__srmReleaseSpaceRequest *req, struct ns1__srmReleaseSpaceResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	int force;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXDPMTOKENLEN+14];
	int nbfqans;
	int nbgids;
	struct ns1__srmReleaseSpaceResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "ReleaseSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmReleaseSpaceResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmReleaseSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->spaceToken || ! req->spaceToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "spaceToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->spaceToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid space token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "ReleaseSpace %s", req->spaceToken->value);
	srm_logreq (func, logbuf);

	if (req->forceFileRelease)
		force = *req->forceFileRelease;
	else
		force = 0;

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_releasespace (req->spaceToken->value, force) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURN (SOAP_OK);
	}
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmUpdateSpace (struct soap *soap, struct ns1__srmUpdateSpaceRequest *req, struct ns1__srmUpdateSpaceResponse_ *rep)
{
	u_signed64 actual_g_space;
	time_t actual_lifetime;
	u_signed64 actual_t_space;
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXDPMTOKENLEN+13];
	int nbfqans;
	int nbgids;
	struct ns1__srmUpdateSpaceResponse *repp;
	u_signed64 req_g_space;
	time_t req_lifetime;
	u_signed64 req_t_space;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "UpdateSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmUpdateSpaceResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmUpdateSpaceResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmUpdateSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->spaceToken || ! req->spaceToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "spaceToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->spaceToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid space token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "UpdateSpace %s", req->spaceToken->value);
	srm_logreq (func, logbuf);

	if (req->newSizeOfTotalSpaceDesired)
		req_t_space = req->newSizeOfTotalSpaceDesired->value;
	else
		req_t_space = -1;
	if (req->newSizeOfGuaranteedSpaceDesired)
		req_g_space = req->newSizeOfGuaranteedSpaceDesired->value;
	else
		req_g_space = -1;
	if (req->newLifeTimeFromCallingTime)
		req_lifetime = req->newLifeTimeFromCallingTime->value;
	else
		req_lifetime = -1;

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_updatespace (req->spaceToken->value,
	    req_t_space, req_g_space, req_lifetime, -1, NULL,
	    &actual_t_space, &actual_g_space, &actual_lifetime) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURN (SOAP_OK);
	}
	if ((repp->sizeOfTotalSpace = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repp->sizeOfTotalSpace->value = actual_t_space;
	if ((repp->sizeOfGuaranteedSpace = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repp->sizeOfGuaranteedSpace->value = actual_g_space;
	if ((repp->lifetimeGranted = soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))))
		repp->lifetimeGranted->value = actual_lifetime;
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmCompactSpace (struct soap *soap, struct ns1__srmCompactSpaceRequest *req, struct ns1__srmCompactSpaceResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[16];
	struct ns1__srmCompactSpaceResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "CompactSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmCompactSpaceResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmCompactSpaceResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	rep->srmCompactSpaceResponse = repp;
	RETURN (SOAP_OK);
}

int
ns1__srmGetSpaceMetaData (struct soap *soap, struct ns1__srmGetSpaceMetaDataRequest *req, struct ns1__srmGetSpaceMetaDataResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	time_t current_time;
	time_t curtime;
	struct dpm_space_reserv dpm_spcmd;
	char **fqan;
	char func[17];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	int nb_errors = 0;
	int nbfqans;
	int nbgids;
	int nbtokens;
	struct ns1__TMetaDataSpace *repmdp;
	struct ns1__srmGetSpaceMetaDataResponse *repp;
	char *s_token;
	gid_t sav_gid = -1;
	char sav_gidstr[256];
	uid_t sav_uid = -1;
	char sav_uidstr[256];
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "GetSpaceMetaData");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetSpaceMetaDataResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmGetSpaceMetaDataResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmGetSpaceMetaDataResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->arrayOfSpaceTokens) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSpaceTokens is required");
		RETURN (SOAP_OK);
	}
	nbtokens = req->arrayOfSpaceTokens->__sizetokenArray;

	/* Allocate the array of space details */

	if ((repp->arrayOfSpaceDetails =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTMetaDataSpace))) == NULL ||
	    (repp->arrayOfSpaceDetails->spaceDetailArray =
	    soap_malloc (soap, nbtokens * sizeof(struct ns1__TMetaDataSpace *))) == NULL)
		RETURN (SOAP_EOM);
	repp->arrayOfSpaceDetails->__sizespaceDetailArray = nbtokens;
	for (i = 0; i < nbtokens; i++) {
		if ((repp->arrayOfSpaceDetails->spaceDetailArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TMetaDataSpace))) == NULL)
			RETURN (SOAP_EOM);
	}

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->arrayOfSpaceDetails = NULL;
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	j = 0;
	for (i = 0; i < nbtokens; i++) {
		s_token = req->arrayOfSpaceTokens->tokenArray[i]->value;
		if (strlen (s_token) > CA_MAXDPMTOKENLEN) {
			nb_errors++;
			continue;
		}
		if (dpm_get_spcmd_by_token (&thip->dbfd, s_token, &dpm_spcmd,
		    0, NULL) < 0) {
			nb_errors++;
			continue;
		}
		if ((dpm_spcmd.s_gid && ! Cgroupmatch (dpm_spcmd.s_gid, nbgids, gids)) ||
		    (dpm_spcmd.s_uid && uid != dpm_spcmd.s_uid)) {
			nb_errors++;
			continue;
		}
		current_time = time (0);
		repmdp = repp->arrayOfSpaceDetails->spaceDetailArray[j];
		if (dpm_spcmd.s_type == 'V')
			repmdp->type = &s_types[Volatile];
		else if (dpm_spcmd.s_type == 'D')
			repmdp->type = &s_types[Durable];
		else
			repmdp->type = &s_types[Permanent];
		repmdp->spaceToken = req->arrayOfSpaceTokens->tokenArray[i];
		repmdp->isValid = true_;
		if ((repmdp->owner = soap_malloc (soap, sizeof(struct ns1__TUserID))))
			if (dpm_spcmd.s_gid)
				repmdp->owner->value =
				    soap_strdup (soap, decode_group (dpm_spcmd.s_gid, &sav_gid, sav_gidstr));
			else
				repmdp->owner->value =
				    soap_strdup (soap, decode_user (dpm_spcmd.s_uid, &sav_uid, sav_uidstr));
		if ((repmdp->totalSize = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
			repmdp->totalSize->value = dpm_spcmd.t_space;
		if ((repmdp->guaranteedSize = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
			repmdp->guaranteedSize->value = dpm_spcmd.g_space;
		if ((repmdp->unusedSize = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
			repmdp->unusedSize->value = dpm_spcmd.u_space > 0 ? dpm_spcmd.u_space : 0;
		if ((repmdp->lifetimeAssigned = soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))))
			repmdp->lifetimeAssigned->value = current_time - dpm_spcmd.assign_time;
		if ((repmdp->lifetimeLeft = soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))))
			repmdp->lifetimeLeft->value = (dpm_spcmd.expire_time > current_time) ?
			    dpm_spcmd.expire_time - current_time : 0;
		j++;
	}
	repp->arrayOfSpaceDetails->__sizespaceDetailArray = nbtokens - nb_errors;
	if (nb_errors == nbtokens) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid/unknown space token(s)");
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmChangeFileStorageType (struct soap *soap, struct ns1__srmChangeFileStorageTypeRequest *req, struct ns1__srmChangeFileStorageTypeResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[22];
	struct ns1__srmChangeFileStorageTypeResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "ChangeFileStorageType");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmChangeFileStorageTypeResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	repp->arrayOfFileStatuses = NULL;
	rep->srmChangeFileStorageTypeResponse = repp;
	RETURN (SOAP_OK);
}

int
ns1__srmGetSpaceToken (struct soap *soap, struct ns1__srmGetSpaceTokenRequest *req, struct ns1__srmGetSpaceTokenResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int nbfqans;
	int nbgids;
	int nbreplies = 0;
	struct ns1__srmGetSpaceTokenResponse *repp;
	char **s_tokens = NULL;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "GetSpaceToken");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetSpaceTokenResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfPossibleSpaceTokens = NULL;
	rep->srmGetSpaceTokenResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->userSpaceTokenDescription) { 
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "userSpaceTokenDescription is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->userSpaceTokenDescription) > 255) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid user space token description");
		RETURN (SOAP_OK);
	}

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_getspacetoken (req->userSpaceTokenDescription, &nbreplies, &s_tokens) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		for (i = 0; i < nbreplies; i++)
			free (s_tokens[i]);
		free (s_tokens);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURN (SOAP_OK);
	}

	/* Allocate the array of space tokens */

	if ((repp->arrayOfPossibleSpaceTokens =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTSpaceToken))) == NULL ||
	    (repp->arrayOfPossibleSpaceTokens->tokenArray =
	    soap_malloc (soap, nbreplies * sizeof(struct ns1__TSpaceToken *))) == NULL) {
		for (i = 0; i < nbreplies; i++)
			free (s_tokens[i]);
		free (s_tokens);
		RETURN (SOAP_EOM);
	}

	repp->arrayOfPossibleSpaceTokens->__sizetokenArray = nbreplies;
	for (i = 0; i < nbreplies; i++) {
		if ((repp->arrayOfPossibleSpaceTokens->tokenArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSpaceToken))) == NULL ||
		    (repp->arrayOfPossibleSpaceTokens->tokenArray[i]->value =
		    soap_strdup (soap, s_tokens[i])) == NULL) {
			for ( ; i < nbreplies; i++)
				free (s_tokens[i]);
			free (s_tokens);
			RETURN (SOAP_EOM);
		}
		free (s_tokens[i]);
	}
	free (s_tokens);
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}
