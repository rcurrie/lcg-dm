.\" @(#)$RCSfile: dpm2_python.man,v $ $Revision$ $Date$ CERN IT-DM/SMD Remi Mollon
.\" Copyright (C) 2004-2010 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH dpm2_python 3 "$Date$" DPM "Python Reference"

.SH NAME
dpm \- Python interface to the DPM
.br

.SH SYNOPSIS
.B import dpm2 as dpm

.SH DESCRIPTION
The dpm module permits you to access the DPM client interface from python
programs. The dpm module is a swig wrapping of the standard C interface.
For detailed descriptions of each function see the individual man page of
each function.
 
.SH FUNCTIONS
.B  dpns_aborttrans ( )

.BI "dpns_access ( string " path ,
.BI "int " amode
.BI  ") -> bool " access

.BI "dpns_accessr ( string " sfn ,
.BI "int " amode
.BI ") -> bool " access

.BI "dpns_addreplica ( string " guid ,
.BI "struct dpns_fileid *" file_uniqueid ,
.BI "string " server ,
.BI "string " sfn ,
.BI "char " status ,
.BI "char " f_type ,
.BI "string " poolname ,
.BI "string " fs
.B  )

.BI "dpns_addreplicax ( string " guid ,
.BI "struct dpns_fileid *" file_uniqueid ,
.BI "string " server ,
.BI "string " sfn ,
.BI "char " status ,
.BI "char " f_type ,
.BI "string " poolname ,
.BI "string " fs ,
.BI "char " r_type ,
.BI "string " setname
.B  )

.BI "dpns_chdir ( string " path
.B  )

.BI "dpns_chmod ( string " path ,
.BI "mode_t " mode
.B  )

.BI "dpns_chown ( string "path ,
.BI "uid_t " new_uid ,
.BI "gid_t " new_gid
.B  )

.BI "dpns_client_setAuthorizationId ( uid_t " uid ,
.BI "gid_t " gid ,
.BI "string " mech ,
.BI "string " id
.B  )

.BI "dpns_client_setVOMS_data ( string " voname ,
.BI "stringlist " vomsdata
.B  )

.BI "dpns_closedir ( dpns_DIR " dirp
.B  )

.BI "dpns_creat ( string " path ,
.BI "mode_t " mode
.B  )

.BI "dpns_delcomment ( string " path
.B  )

.BI "dpns_delete ( string " path
.B  )

.BI "dpns_delreplica ( string " guid ,
.BI "struct dpns_fileid *" file_uniqueid ,
.BI "string " sfn
.B  )

.BI "dpns_delreplicasbysfn ( ListOfString " sfns ,
.BI "ListOfString " guids
.BI ") -> ListOfInt " results

.B  dpns_endsess ( )

.B  dpns_endtrans ( )

.BI "dpns_getacl ( string " path ,
.BI ") -> ListOf struct dpns_acl " acls

.BI "dpns_getcomment ( string " path
.BI ") -> string " comment

.BI "dpns_getcwd ( ) -> string " dir

.BI "dpns_getifcevers ( ) -> string " version

.BI "dpns_getlinks ( string " path ,
.BI "string " guid
.BI ") -> ListOf struct dpns_linkinfo " links

.BI "dpns_getreplica ( string " path ,
.BI "string " guid ,
.BI "string " se
.BI ") -> ListOf struct dpns_filereplica " replicas

.BI "dpns_getreplicax ( string " path ,
.BI "string " guid ,
.BI "string " se
.BI ") -> ListOf struct dpns_filereplicax " replicas

.BI "dpns_lchown ( string " path ,
.BI "uid_t " new_uid ,
.BI "gid_t "new_gid
.B  )

.BI "dpns_listreplicax ( string " poolname ,
.BI "string " server ,
.BI "string " fs ,
.BI "int " flags ,
.BI "dpns_list * listp
.BI ") -> struct dpns_filereplica " replica

.BI "dpns_listrepset ( string " setname ,
.BI "int " flags ,
.BI "dpns_list *" listp
.BI ") -> struct dpns_filereplicax " replica

.BI "dpns_lstat ( string " path
.BI ") -> struct dpns_filestat " statbuf

.BI "dpns_mkdir ( string " path ,
.BI "mode_t " mode
.B  )

.BI "dpns_modreplica ( string " sfn ,
.BI "string " setname ,
.BI "string " poolname ,
.BI "string " server
.B  )

.BI "dpns_modreplicax ( string " sfn ,
.BI "string " setname ,
.BI "string " poolname ,
.BI "string " server ,
.BI "string " fs ,
.BI "char " r_type
.B  )

.BI "dpns_opendir ( string " path
.BI ") -> dpns_DIR " dir

.BI "dpns_opendirxg ( string " server ,
.BI "string " path ,
.BI "string " guid
.BI ") -> dpns_DIR " dir

.BI "dpns_ping ( string " server
.BI ") -> string " info

.BI "dpns_readdir ( dpns_DIR " dirp
.BI ") -> struct dirent " dirent

.BI "dpns_readdirc ( dpns_DIR " dirp
.BI ") -> struct dpns_direncomm " dirent

.BI "dpns_readdirg ( dpns_DIR " dirp
.BI ") -> struct dpns_direnstatg " dirent

.BI "dpns_readdirx ( dpns_DIR " dirp
.BI ") -> struct dpns_direnstat " dirent

.BI "dpns_readdirxc ( dpns_DIR " dirp
.BI ") -> struct dpns_direnstatc " dirent

.BI "dpns_readdirxp ( dpns_DIR " dirp ,
.BI "string " pattern ,
.BI "string " se
.BI "= None ) -> struct dpns_direnrep " dirent

.BI "dpns_readdirxr ( dpns_DIR " dirp ,
.BI "string " se
.BI "= None ) -> struct dpns_direnrep " dirent

.BI "dpns_readlink ( string " path
.BI ") -> string " link

.BI "dpns_rename ( string " oldpath ,
.BI "string " newpath
.B  )

.BI "dpns_rewinddir ( dpns_DIR " dirp
.B  )

.BI "dpns_rmdir ( string " path
.B  )

.BI "dpns_setacl ( string " path ,
.BI "ListOf struct dpns_acl " acls
.B  )

.BI "dpns_setatime ( string " path ,
.BI "struct dpns_fileid " file_uniqueid
.B  )

.BI "dpns_setcomment ( string " path ,
.BI "string " comment
.B  )

.BI "dpns_setfsize ( string " path ,
.BI "struct dpns_fileid " file_uniqueid ,
.BI "long " filesize
.B  )

.BI "dpns_setfsizec ( string " path ,
.BI "struct dpns_fileid " file_uniqueid ,
.BI "long " filesize ,
.BI "string " csumtype ,
.BI "string " csumvalue
.B  )

.BI "dpns_setptime ( string " sfn ,
.BI "long " ptime
.B  )

.BI "dpns_setratime ( string " sfn
.B  )

.BI "dpns_setrltime ( string " sfn ,
.BI "long " ltime
.B  )

.BI "dpns_setrstatus ( string " sfn ,
.BI "char " status
.B  )

.BI "dpns_setrtype ( string " sfn ,
.BI "char " type
.B  )

.BI "dpns_startsess ( string " server ,
.BI "string " comment
.BI )

.BI "dpns_starttrans ( string " server ,
.BI "string " comment
.BI )

.BI "dpns_stat ( string " path
.BI ") -> struct dpns_filestat " statbuf

.BI "dpns_statg ( string " path ,
.BI "string " guid
.BI ") -> struct dpns_filestatg " statbuf

.BI "dpns_statr ( string " sfn)
.BI ") -> struct dpns_filestatg " statbuf

.BI "dpns_symlink ( string " target ,
.BI "string " linkname
.B  )

.BI "dpns_umask ( mode_t " cmask
.BI ") -> mode_t " oldmask

.BI "dpns_undelete ( string " path
.B  )

.BI "dpns_unlink ( string " path
.B  )

.BI "dpns_utime ( string " path ,
.BI "struct utimbuf " times
.B  )

.BI "dpns_entergrpmap ( gid_t " gid ,
.BI "string " groupname
.B  )

.BI "dpns_enterusrmap ( uid_t " uid ,
.BI "string " username
.B  )

.BI "dpns_getgrpbygid ( gid_t " gid
.BI ") -> string " group

.BI "dpns_getgrpbygids ( ListOfgid_t " gids
.BI ") -> ListOfString " groups

.BI "dpns_getgrpbynam ( string " groupname
.BI ") -> gid_t " gid

.BI "dpns_getgrpmap ( ) -> ListOf struct dpns_groupinfo " infos

.BI "dpns_getusrbynam ( string " username
.BI ") -> uid_t " uid

.BI "dpns_getusrbyuid ( uid_t " uid
.BI ") -> string " user

.BI "dpns_getusrmap ( ) -> ListOf struct dpns_userinfo " userinfo

.BI "dpns_modifygrpmap ( gid_t " gid ,
.BI "string " newname
.B  )

.BI "dpns_modifyusrmap ( uid_t " uid ,
.BI "string " newname
.B  )

.BI "dpns_rmgrpmap ( gid_t " gid ,
.BI "string " groupname
.B  )

.BI "dpns_rmusrmap ( uid_t " uid ,
.BI "string " username
.B  )

.BI "dpm_getpoolfs ( string " poolname
.BI ") -> ListOf struct dpm_fs " dpm_fs

.BI "dpm_getpools ( ) -> ListOf struct dpm_pool " dpm_pools

.BI "dpm_getprotocols ( ) -> ListOfString " protocols

.BI "dpm_getspacemd ( ListOfString " s_tokens
.BI ") -> ListOf struct dpm_space_metadata " spacemd

.BI "dpm_getspacetoken ( string " u_token
.BI ") -> ListOfString " s_tokens

.BI "dpm_addfs ( string " poolname ,
.BI "string " server ,
.BI "string " fs ,
.BI "int " status
.B  )

.BI "dpm_addpool ( struct dpm_pool " dpm_pool
.B  )

.BI "dpm_delreplica ( string " pfn
.B  )

.BI "dpm_modifyfs ( string " server ,
.BI "string " fs ,
.BI "int " status
.B  )

.BI "dpm_modifypool ( struct dpm_pool " dpm_pool
.B  )

.BI "dpm_ping ( string " host
.BI ") -> string " info

.BI "dpm_releasespace ( string " s_token ,
.BI "int " force
.B  )

.BI "dpm_reservespace ( char " s_type ,
.BI "string " u_token ,
.BI "char " ret_policy ,
.BI "char " ac_latency ,
.BI "long " req_t_space ,
.BI "long " req_g_space ,
.BI "long " req_lifetime ,
.BI "ListOfInt " gids ,
.BI "string " poolname
.BI ") -> char " actual_s_type ,
.BI "long " actual_t_space ,
.BI "long " actual_g_space ,
.BI "long " actual_lifetime ,
.BI "string " s_token

.BI "dpm_rmfs ( string " server ,
.BI "string " fs
.B  )

.BI "dpm_rmpool ( string " poolname
.B  )

.BI "dpm_updatespace ( string " s_token ,
.BI "long " req_t_space ,
.BI "long " req_g_space ,
.BI "long " req_lifetime ,
.BI "ListOfInt " gids
.BI ") -> long " actual_t_space ,
.BI "long " actual_g_space ,
.BI "long " actual_lifetime

.SH STRUCTURES
Sometimes you need to specify a structure as an input argument (eg.
.IR "struct dpns_acl" ,
.IR "struct dpns_list" ,
.IR "struct dpm_pool" ", etc.)."
For that purpose, the module includes structure constructors, named exactly as the structure.

Thus, to create
.IR "struct dpns_acl" ,
.IR "struct dpns_list " or
.IR "struct dpmm_pool" ,
you have to do the following:

.nf
    dpnsacl = dpm.dpns_acl()
    dpnslist = dpm.dpns_list()
    dpmpool = dpm.dpm_pool()
.fi

If you want to see how to use it in a real case, please have a look at examples.

.SH ERRORS
As you can see, no error code is returned. When an error occurs, an exception is raised
with a description of the error.

.SH EXAMPLE
.nf
#!/usr/bin/python

"""
# Using the dpns_readdirxr method
"""

import sys
import traceback
import dpm2 as dpm

name = "/dpm/cern.ch/home/dteam/";

try:
   dir = dpm.dpns_opendir(name)

   while 1:
      entry = dpm.dpns_readdirxr(dir)
      if entry == None:
         break
      print entry.d_name
      for i in range(entry.nbreplicas):
         print " ==> %s" % entry.rep[i].sfn

   dpm.dpns_closedir(dir)
except TypeError, x:
   print " ==> None"
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpns_getreplica method
"""

file = "/dpm/cern.ch/home/dteam/file.test"

try:
   list = dpm.dpns_getreplica(file, "", "")
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in list:
   print i.host
   print i.sfn

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpns_getacl and dpns_setacl methods to add a user ACL
"""

file = "/dpm/cern.ch/home/dteam/file.test"

try:
   acls_list = dpm.dpns_getacl(file)
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

# When adding a first ACL for a given user, you also need to add the mask
# When adding the second user ACL, it is not necessary anymore

acl_user = dpm.dpns_acl()
acl_mask = dpm.dpns_acl()

acl_user.a_type = lfc.CNS_ACL_USER
acl_user.a_id = 18701		# user id
acl_user.a_perm = 5

acl_mask.a_type = lfc.CNS_ACL_MASK
acl_mask.a_id = 0			# no user id specified
acl_mask.a_perm = 5

acls_list.append(acl_user)
acls_list.append(acl_mask)

try:
   dpm.dpns_setacl(file, acls_list)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpns_getacl and dpns_setacl methods to remove a user ACL
"""

file = "/dpm/cern.ch/home/dteam/file.test"

try:
   acls_list = dpm.dpns_getacl(file)
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

del acls_list[1]	# delete a given user ACL from the list of ACLs

try:
   dpm.dpns_setacl(file, acls_list)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpns_getusrmap method
"""

try:
   list = dpm.dpns_getusrmap()
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in list:
   print i.userid + " " + i.username

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpns_getgrpmap method
"""

try:
   list = dpm.dpns_getgrpmap()
except Exception:
   traceback.print_exc()
   sys.exit(1)
    
for i in list:
   print i.gid + " " + i.groupname

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_addfs method
"""

try:
   dpm.dpm_addfs("mypool", "mydiskserver.domain.com", "/mountpoint", \\
			dpm.FS_READONLY)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_modifyfs method
"""

try:
   dpm.dpm_modifyfs("mydiskserver.domain.com", "/mountpoint", \\
			dpm.FS_READONLY)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_rmfs method
"""

try:
   dpm.dpm_rmfs("mypool", "mydiskserver.domain.com", "/mountpoint")
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_addpool method
"""

dpmpool = dpm.dpm_pool()
dpmpool.poolname = "mypool"
dpmpool.defsize = 209715200
dpmpool.def_lifetime = 604800
dpmpool.defpintime = 604800
dpmpool.max_lifetime = 604800
dpmpool.max_pintime = 604800
dpmpool.nbgids = 1
dpmpool.gids = [0]
dpmpool.ret_policy = 'R'
dpmpool.s_type = 'D'

try:
   dpm.dpm_addpool(dpmpool)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_modifypool method
"""

dpmpool = dpm.dpm_pool()
dpmpool.poolname = "mypool"
dpmpool.defsize = 209715200
dpmpool.def_lifetime = 604800
dpmpool.defpintime = 604800
dpmpool.max_lifetime = 604800
dpmpool.max_pintime = 604800
dpmpool.nbgids = 1
dpmpool.gids = [0]
dpmpool.ret_policy = 'R'
dpmpool.s_type = 'D'

try:
   dpm.dpm_modifypool(dpmpool)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_rmpool method
"""

try:
   dpm.dpm_rmpool("mypool")
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_getpoolfs method
"""

try:
   list = dpm.dpm_getpoolfs("mypool")
except Exception:
   traceback.print_exc()
   sys.exit(1)

print len(list)
for i in list:
   print "POOL " + i.poolname + " SERVER " + i.server + " FS " + i.fs \\
      + " CAPACITY " + i.capacity + " FREE " + i.free

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_getpools method
"""

try:
   list = dpm.dpm_getpools()
except Exception:
   traceback.print_exc()
   sys.exit(1)

print len(list)
for i in list:
   print "POOL " + i.poolname + " CAPACITY " + i.capacity + " FREE " + i.free

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_getprotocols method
"""

try:
   list = dpm.dpm_getprotocols()
except Exception:
   traceback.print_exc()
   sys.exit(1)

print len(list)
for i in list:
   print i

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_getspacemd method
"""

try:
   list = dpm.dpm_getspacemd(["myspacetoken"])
except Exception:
   traceback.print_exc()
   sys.exit(1)

print len(list)
for i in list:
   print "TYPE " + i.s_type + " SPACETOKEN " i.s_token + " USERTOKEN " \\
      + i.u_token + " TOTAL " + i.t_space + " GUARANTUEED " + i.g_space \\
      + " UNUSED " + i.u_space + " POOL " + i.poolname

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_getspacetoken method
"""

try:
   list = dpm.dpm_getspacetoken("myspacetokendesc")
except Exception:
   traceback.print_exc()
   sys.exit(1)

print len(list)
for i in list:
   print i

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_reservespace method
"""

try:
   actual_s_type,actual_t_space,actual_g_space,actual_lifetime,s_token = \\
		dpm.dpm_reservespace('D', "myspacetokendesc", 'R', 'O', 209715200, \\
		209715200, 2592000, 0, "mypoolname")
except Exception:
   traceback.print_exc()
   sys.exit(1)

print "TYPE " + actual_s_type + " TOTAL " + actual_t_space + " GUARANTEED " \\
   + actual_g_space + " LIFETIME " + actual_lifetime + " TOKEN " + s_token

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_updatespace method
"""

try:
   actual_t_space,actual_g_space,actual_lifetime = \\
		dpm.dpm_updatespace("myspacetoken", 209715200, 209715200, 2592000)
except Exception:
   traceback.print_exc()
   sys.exit(1)

print " TOTAL " + actual_t_space + " GUARANTEED " + actual_g_space \\
   + " LIFETIME " + actual_lifetime

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_releasespace method
"""

try:
   dpm.dpm_releasespace("myspacetoken", 0)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import dpm2 as dpm

"""
# Using the dpm_ping method
"""

try:
   info = dpm.dpm_ping("mydpmserver.domain.com")
except Exception:
   traceback.print_exc()
   sys.exit(1)

print info

.SH SEE ALSO
.B DPM C interface man pages
