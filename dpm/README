
			/* Disk Pool Manager client structures */

struct dpm_pool {
	char		poolname[CA_MAXPOOLNAMELEN+1];
	u_signed64	defsize;
	int		defpintime;
	int		put_retenp;
	char		fss_policy[CA_MAXPOLICYLEN+1];
	char		gc_policy[CA_MAXPOLICYLEN+1];
	char		rs_policy[CA_MAXPOLICYLEN+1];
	gid_t		gid;
	char		s_type;
	u_signed64	capacity;
	u_signed64	free;
	int		nbelem;
};

struct dpm_fs {
	char		poolname[CA_MAXPOOLNAMELEN+1];
	char		server[CA_MAXHOSTNAMELEN+1];
	char		fs[80];
	u_signed64	capacity;
	u_signed64	free;
	int		status;
};

struct dpm_copyfilereq {
	char		*from_surl;
	char		*to_surl;
	time_t		lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	int		flags;
};

struct dpm_copyfilestatus {
	char		*from_surl;
	char		*to_surl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t		pintime;
};

struct dpm_filestatus {
	char		*surl;
	int		status;
	char		*errstring;
};

struct dpm_getfilereq {
	char		*from_surl;
	time_t		lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	int		flags;
};

struct dpm_getfilestatus {
	char		*from_surl;
	char		*turl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t		pintime;
};

struct dpm_putfilereq {
	char		*to_surl;
	time_t		lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	u_signed64	requested_size;
};

struct dpm_putfilestatus {
	char		*to_surl;
	char		*turl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t		pintime;
};

struct dpm_reqsummary {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	char		r_type;
	int		nb_reqfiles;
	int		nb_queued;
	int		nb_finished;
	int		nb_progress;
};

struct dpm_space_metadata {
	char		s_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		client_dn[256];
	u_signed64	t_space;	/* Total space */
	u_signed64	g_space;	/* Guaranteed space */
	u_signed64	u_space;	/* Unused space */
	time_t		a_lifetime;	/* Lifetime assigned */
	time_t		r_lifetime;	/* Remaining lifetime */
};

struct dpm_tokeninfo {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	time_t		c_time;
};

			/* function prototypes */

int dpm_reservespace (
	const char s_type,
	const char *u_token,
	u_signed64 req_t_space,
	u_signed64 req_g_space,
	time_t req_lifetime,
	char *actual_s_type,
	u_signed64 *actual_t_space,
	u_signed64 *actual_g_space,
	time_t *actual_lifetime,
	char *s_token);

int dpm_releasespace (
	char *s_token,
	int force);

int dpm_updatespace (
	char *s_token,
	u_signed64 req_t_space,
	u_signed64 req_g_space,
	time_t req_lifetime,
	u_signed64 *actual_t_space,
	u_signed64 *actual_g_space,
	time_t *actual_lifetime);

int dpm_getspacemd (
	int nbtokens,
	char **s_tokens,
	int *nbreplies,
	struct dpm_space_metadata **spacemd);

int dpm_getspacetoken (
	const char *u_token,
	int *nbreplies,
	char ***s_tokens);

int dpm_rm (
	int nbpaths,
	char **paths,
	int *nbreplies,
	struct dpm_filestatus **filestatuses);

int dpm_get (
	int nbreqfiles,
	struct dpm_getfilereq *reqfiles,
	int nbprotocols,
	char **protocols,
	char *u_token,
	time_t retrytime,
	char *r_token,
	int *nbreplies,
	struct dpm_getfilestatus **filestatuses);

int dpm_put (
	int nbreqfiles,
	struct dpm_putfilereq *reqfiles,
	int nbprotocols,
	char **protocols,
	char *u_token,
	int overwrite,
	time_t retrytime,
	char *r_token,
	int *nbreplies,
	struct dpm_putfilestatus **filestatuses);

int dpm_copy (
	int nbreqfiles,
	struct dpm_copyfilereq *reqfiles,
	char *u_token,
	int flags,
	time_t retrytime,
	char *r_token,
	int *nbreplies,
	struct dpm_copyfilestatus **filestatuses);

int dpm_relfiles (
	char *r_token,
	int nbsurls,
	char **surls,
	int keepspace,
	int *nbreplies,
	struct dpm_filestatus **filestatuses);

int dpm_putdone (
	char *r_token,
	int nbsurls,
	char **surls,
	int *nbreplies,
	struct dpm_filestatus **filestatuses);

int dpm_abortreq (
	char *r_token);

int dpm_getstatus_getreq (
	char *r_token,
	int nbfromsurls,
	char **fromsurls,
	int *nbreplies,
	struct dpm_getfilestatus **filestatuses);

int dpm_getstatus_putreq (
	char *r_token,
	int nbtosurls,
	char **tosurls,
	int *nbreplies,
	struct dpm_putfilestatus **filestatuses);

int dpm_getstatus_copyreq (
	char *r_token,
	int nbsurls,
	char **fromsurls,
	char **tosurls,
	int *nbreplies,
	struct dpm_copyfilestatus **filestatuses);

int dpm_getreqsummary (
	int nbtokens;
	char **r_tokens,
	int *nbreplies,
	struct dpm_reqsummary **summaries);

int dpm_extendfilelife (
	char *r_token,
	char *surl,
	time_t lifetime,
	time_t *actual_lifetime);

int dpm_getreqid (
	const char *u_token,
	int *nbreplies,
	struct dpm_tokeninfo **tokeninfos);

int dpm_addpool (
	struct dpm_pool *dpm_pool);

int dpm_getpools (
	int *nbpools,
	struct dpm_pool **dpm_pool);

int dpm_modifypool (
	struct dpm_pool *dpm_pool);

int dpm_rmpool (
	char *poolname);

int dpm_addfs (
	char *poolname,
	char *server,
	char *fs,
	int status);

int dpm_getpoolfs (
	char *poolname,
	int *nbfs,
	struct dpm_fs **dpm_fs);

int dpm_modifyfs (
	char *server,
	char *fs,
	int status);

int dpm_rmfs (
	char *server,
	char *fs);

int dpm_getprotocols (
	int *nb_supported_protocols,
	char ***supported_protocols);
