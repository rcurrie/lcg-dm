/*
 * @(#)$RCSfile: dpm2.i,v $ $Revision$ $Date$ CERN IT-DM/SMD Remi Mollon
 */

/*********************************************
    SWIG input file for DPM
 (including typemaps for non-trivial functions
*********************************************/

%module dpm2

%{
#include "dpns_api.h"
%}

%include "typemaps.i"
%include "dpm-python_typemaps.i"

#define SWIGNOPROTO

%{
#define SWIGNOPROTO
%}

%include "dpns_api.h"
%include "Cns.i"

%{
static char serrbuf[ERRORLEN_MAX] = "";
%}

%init %{
    dpns_seterrbuf (serrbuf, ERRORLEN_MAX);
%}

%{
#include "dpm_api.h"
%}

%include "dpm_api.h"
%include "dpm_constants.h"
%include "dpm_struct.h"

typedef long long signed64;



extern RETURNCODE  dpm_accessr(const char *pfn, int amode);
extern RETURNCODE  dpm_getpoolfs(char *poolname, int *LENGTH, struct dpm_fs **OUTPUT);
extern RETURNCODE  dpm_getpools(int *LENGTH, struct dpm_pool **OUTPUT);
extern RETURNCODE  dpm_getprotocols (int *LENGTH, char ***OUTPUT);
extern RETURNCODE  dpm_getspacemd (int LENGTH, char **TUPLE, int *LENGTH, struct dpm_space_metadata **OUTPUT);
extern RETURNCODE  dpm_getspacetoken (const char *u_token, int *LENGTH, char ***OUTPUT);
extern RETURNCODE  dpm_addfs(char *poolname, char *server, char *fs, int status, int weight);
extern RETURNCODE  dpm_addpool(struct dpm_pool *POOL);
extern RETURNCODE  dpm_delreplica(char *pfn);
extern RETURNCODE  dpm_modifyfs(char *server, char *fs, int status, int weight);
extern RETURNCODE  dpm_modifypool(struct dpm_pool *POOL);
extern RETURNCODE  dpm_ping(char *host, char *STRING256OUT);
extern RETURNCODE  dpm_releasespace(char *s_token, int force);
extern RETURNCODE  dpm_reservespace(const char s_type, const char *u_token, const char ret_policy,
        const char ac_latency, u_signed64 req_t_space, u_signed64 req_g_space,
        time_t req_lifetime, int LENGTH, gid_t *INPUT, const char *poolname,
        char *OUTPUT, u_signed64 *OUTPUT, u_signed64 *OUTPUT,
        time_t *OUTPUT, char *TOKEN);
extern RETURNCODE  dpm_rmfs(char *server, char *fs);
extern RETURNCODE  dpm_rmpool(char *poolname);
extern RETURNCODE  dpm_updatespace(char *s_token, u_signed64 req_t_space, u_signed64 req_g_space, time_t req_lifetime,
        int LENGTH, gid_t *INPUT, u_signed64 *OUTPUT, u_signed64 *OUTPUT, time_t *OUTPUT);
