/*
 * Copyright (C) 2013 by CERN/IT/SDC/ID
 * All rights reserved
 */

/*      dpm_accessr - check accessibility of a file replica in the pools */

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h> 
#endif
#include "dpm_api.h"
#include "dpns_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_accessr(const char *pfn, int amode)
{
	int c;
	static char func[] = "dpm_accessr";
	gid_t gid;
	int msglen;
	char *q;
	char *rbp;
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct dpm_api_thread_info *thip;
	uid_t uid;

	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! pfn) {
		serrno = EFAULT;
		return (-1);
	}

	if (strlen (pfn) > CA_MAXSFNLEN) {
		serrno = ENAMETOOLONG;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC);
	marshall_LONG (sbp, DPM_ACCESSR);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_STRING (sbp, pfn);
	marshall_LONG (sbp, amode);

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2dpm (NULL, sendbuf, msglen, NULL, 0, NULL, NULL);

	return (c);
}
