#
#  Copyright (C) 2004 by CERN/IT/GD/CT
#  All rights reserved
#
#       @(#)$RCSfile: oralink.mk,v $ $Revision: 1.2 $ $Date: 2005/03/08 11:31:38 $ CERN IT-GD/CT Jean-Philippe Baud
 
#    Link dpm with Oracle libraries.

ifdef ORACLE_SHARE
include $(ORACLE_SHARE)/demo_proc_ic.mk
else
include $(ORACLE_HOME)/precomp/lib/env_precomp.mk
endif

ifdef ORACLE_LIB
LIBHOME=$(ORACLE_LIB)
endif

PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL) $(STATICTTLIBS)
PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL)

dpm: $(SRV_OBJS)
	$(CC) -o dpm $(CLDFLAGS) $(SRV_OBJS) $(LIBS) -L$(LIBHOME) $(PROLDLIBS)
