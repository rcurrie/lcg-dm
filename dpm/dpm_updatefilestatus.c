/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_updatefilestatus.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*      dpm_updatefilestatus - update file status from a DPM backend */

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_updatefilestatus (const char r_type, char *r_token, char *surl, char *server, char *pfn, int status, char *errmsg)
{
	int c;
	char func[21];
	gid_t gid;
	int msglen;
	char *q;
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_updatefilestatus");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! r_token || ! surl) {
		serrno = EFAULT;
		return (-1);
	}
	if (! r_type) {
		serrno = EINVAL;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC);
	marshall_LONG (sbp, DPM_UPDFILSTS);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_BYTE (sbp, r_type);
	marshall_STRING (sbp, r_token);
	marshall_STRING (sbp, surl);
	if (server) {
		marshall_STRING (sbp, server);
	} else {
		marshall_STRING (sbp, "");
	}
	if (pfn) {
		marshall_STRING (sbp, pfn);
	} else {
		marshall_STRING (sbp, "");
	}
	marshall_LONG (sbp, status);
	if (errmsg) {
		marshall_STRING (sbp, errmsg);
	} else {
		marshall_STRING (sbp, "");
	}

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2dpm (NULL, sendbuf, msglen, NULL, 0, NULL, NULL);
	return (c);
}
