.\" @(#)$RCSfile: dpm_getstatus_getreq.man,v $ $Revision: 1.2 $ $Date: 2006/03/23 06:12:41 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GETSTATUS_GETREQ 3 "$Date: 2006/03/23 06:12:41 $" LCG "DPM Library Functions"
.SH NAME
dpm_getstatus_getreq \- get status for a dpm_get request
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getstatus_getreq (char *" r_token ,
.BI "int " nbfromsurls ,
.BI "char **" fromsurls ,
.BI "int *" nbreplies ,
.BI "struct dpm_getfilestatus **" filestatuses )
.SH DESCRIPTION
.B dpm_getstatus_getreq
gets status for a dpm_get request.
.LP
The input arguments are:
.TP
.I r_token
specifies the token returned by a previous get request.
.TP
.I nbfromsurls
specifies the number of files for which the status is requested. If zero,
the status of all files in the get request is returned.
.TP
.I fromsurls
specifies the array of file names.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of file statuses.
.TP
.I filestatuses
will be set to the address of an array of dpm_getfilestatus structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_getfilestatus {
	char		*from_surl;
	char		*turl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t	pintime;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I nbfromsurls
is strictly positive and
.I fromsurls
is NULL or
.IR r_token ,
.I nbreplies
or
.I filestatuses
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request.
.TP
.B EINVAL
.I nbfromsurls
is not positive, the token is invalid/unknown or all file requests have errors.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
