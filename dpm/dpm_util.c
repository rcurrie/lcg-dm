/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_util.c,v $ $Revision: 1.11 $ $Date: 2009/11/13 10:03:12 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Castor_limits.h"
#include "dpm_constants.h"
#include "dpm.h"
#include "dpm_server.h"
#include "dpm_util.h"
#include "serrno.h"

#ifndef _WIN32
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
#define strtok(X,Y) strtok_r(X,Y,&last)
#endif /* _REENTRANT || _THREAD_SAFE */
#endif /* _WIN32 */

extern char *getconfent();
extern char localdomain[CA_MAXHOSTNAMELEN+1];
extern char localhost[CA_MAXHOSTNAMELEN+1];
static char *default_protocols[] = {"rfio", "gsiftp", NULL};

static char gridftphead[CA_MAXHOSTNAMELEN+1];
static char httphead[CA_MAXHOSTNAMELEN+1];
static char httpshead[CA_MAXHOSTNAMELEN+1];
static char xroothead[CA_MAXHOSTNAMELEN+1];

build_proxy_filename(char *proxy_filename, char *r_token)
{
	strcpy (proxy_filename, P_tmpdir);
	if (*(P_tmpdir+sizeof(P_tmpdir)-2) != '/')
		strcat (proxy_filename, "/");
	strcat (proxy_filename, "dpm");
	strcat (proxy_filename, r_token);
	return (0);
}

get_req_retention_time(tp)
time_t *tp;
{
	int errflag;
	char *p;
	char *p2;
	time_t req_retention;

	p = getconfent ("DPM", "REQCLEAN", 0);
	req_retention = 0x7FFFFFFF;
	if (p && strcmp (p, "Inf") != 0) {
		errflag = 0;
		if ((req_retention = strtol (p, &p2, 10)) < 0 ||
		    (*p2 != '\0' && *(p2+1) != '\0')) {
			errflag++;
		} else {
			switch (*p2) {
				case 'y':
					req_retention *= 365 * 86400;
					break;

				case 'm':
					req_retention *= 30 * 86400;
					break;

				case 'd':
					req_retention *= 86400;
					break;
                                                
				case 'h':
					req_retention *= 3600;
					break;

				case '\0':
					break;

				default:
					errflag++;
					break;
			}
		}
		if (errflag)
			return (-1);
	}
	*tp = req_retention;
	return (0);
}

static int add_proto(char ***sup_proto, const char *proto)
{
	int nb=0,dup=0;
	while((*sup_proto)[nb]) {
		if (strcmp ((*sup_proto)[nb], proto) == 0)
			dup++;
		nb++;
	}
	if (dup) { return (nb); }
	*sup_proto = realloc (*sup_proto, sizeof(char *)*(nb + 2));
	(*sup_proto)[nb] = strdup (proto);
	(*sup_proto)[nb+1] = NULL;
	return (nb+1);
}

static int is_xroot(const char *proto)
{
	if (strcmp(proto,"xroot") == 0 ||
	    strcmp(proto,"root") == 0)
		return (1);
	return (0);
}

static int is_dav(const char *proto)
{
	if (strcmp(proto,"https") == 0 ||
	    strcmp(proto,"http") == 0)
		return (1);
	return (0);
}

static int is_gsiftp(const char *proto)
{
	if (strcmp(proto,"gsiftp") == 0)
		return (1);
	return (0);
}

int is_redirect_to_headnode_with_sfn(const char *proto)
{
	if (is_xroot (proto) || is_dav (proto))
		return (1);
	return (0);
}

char *get_redirect_target(const char *proto)
{
	if (*xroothead && is_xroot (proto))
		return (xroothead);
	if (*httphead && strcmp (proto, "http") == 0)
		return (httphead);
	if (*httpshead && strcmp (proto, "https") == 0)
		return (httpshead);
	if (*gridftphead && is_gsiftp (proto))
		return (gridftphead);
	return (NULL);
}

get_supported_protocols(char ***sup_proto)
{
#ifndef _WIN32
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
	char *last = NULL;
#endif
#endif
	int has_xroot = 0;
	int has_dav = 0;
	int has_gsiftp = 0;
	int i;
	int nb_supported_protocols = 0;
	char *p;
	char *protocols;
	char **supported_protocols;

	supported_protocols = (char **)calloc (1, sizeof(char *));

	if ((protocols = getconfent ("DPM", "PROTOCOLS", 1))) {
		p = strtok (protocols, " \t");
		while (p) {
			nb_supported_protocols = add_proto (&supported_protocols, p);
			p = strtok (NULL, " \t");
		}
	} else {
		for(i=0;default_protocols[i];i++) {
			nb_supported_protocols = add_proto (&supported_protocols, default_protocols[i]);
		}
	}
	for(i=0;i<nb_supported_protocols;i++) {
		if (is_dav (supported_protocols[i]))
			has_dav++;
		if (is_xroot (supported_protocols[i]))
			has_xroot++;
		if (is_gsiftp (supported_protocols[i]))
			has_gsiftp++;
	}
	xroothead[0] = '\0';
	if (has_xroot) {
		nb_supported_protocols = add_proto (&supported_protocols, "root");
		nb_supported_protocols = add_proto (&supported_protocols, "xroot");
		if ((p = getconfent ("DPM", "XROOTHEAD", 0))) {
			strncat (xroothead, p, CA_MAXHOSTNAMELEN);
		} else {
			strncat (xroothead, localhost, CA_MAXHOSTNAMELEN);
			strncat (xroothead, ":1094", CA_MAXHOSTNAMELEN - strlen (xroothead));
		}
	}
	httphead[0] = '\0';
	httpshead[0] = '\0';
	if (has_dav) {
		if ((p = getconfent ("DPM", "HTTPHEAD", 0)))
			strncat (httphead, p, CA_MAXHOSTNAMELEN);
		if ((p = getconfent ("DPM", "HTTPSHEAD", 0)))
			strncat (httpshead, p, CA_MAXHOSTNAMELEN);
		if (! *httphead && ! *httpshead) {
			strncat (httphead, localhost, CA_MAXHOSTNAMELEN);
			strncat (httpshead, localhost, CA_MAXHOSTNAMELEN);
		} else if (*httphead && ! *httpshead) {
			strcpy (httpshead, httphead);
		} else if (*httpshead && ! *httphead) {
			strcpy (httphead, httpshead);
		}
	}
	gridftphead[0] = '\0';
	if (has_gsiftp) {
		if ((p = getconfent ("DPM", "FTPHEAD", 0))) {
			strncat (gridftphead, p, CA_MAXHOSTNAMELEN);
		}
	}
	*sup_proto = supported_protocols;
	return (nb_supported_protocols);
}

is_surl_local (char *surl)
{
	int len;
	char *p;
	char *q;
	char server[CA_MAXHOSTNAMELEN+1];

	if (strncmp (surl, "srm://", 6)) {
		errno = EINVAL;
		return (-1);
	}
	if ((p = strchr (surl + 6, '/')) == NULL) {
		errno = EINVAL;
		return (-1);
	}
	if (memchr (surl + 6, '[', p - surl - 6) != NULL) {
		/* the surl host is an IP address: it is not supported */
		errno = EINVAL;
		return (-1);
	}
	if ((q = memchr (surl + 6, ':', p - surl - 6)) != NULL)
		p = q;
	if ((len = p - surl - 6) >= sizeof(server)) {
		errno = EINVAL;
		return (-1);
	}
	memcpy (server, surl + 6, len);
	server[len] = '\0';
	if (strcmp (server, localhost) == 0)
		return (1);
	if (strchr (server, '.'))
		return (0);
	if (len + strlen (localdomain) >= sizeof(server)) {
		errno = EINVAL;
		return (-1);
	}
	strcat (server, localdomain);
	if (strcmp (server, localhost) == 0)
		return (1);
	return (0);
}

char *
sfnfromsurl (char *surl)
{
	char *p;

	if (strncmp (surl, "srm://", 6)) {
		errno = EINVAL;
		return (NULL);
	}
	if ((p = strstr (surl + 6, "?SFN=")))
		return (p + 5);
	if ((p = strchr (surl + 6, '/')) == NULL) {
		errno = EINVAL;
		return (NULL);
	}
	return (p);
}

char *
status2str (int status, char *buf)
{
	static char *s_status[] = {
		"DPM_SUCCESS", "DPM_QUEUED", "DPM_ACTIVE", "DPM_READY",
		"DPM_RUNNING", "DPM_DONE", "DPM_FAILED", "DPM_ABORTED",
		"DPM_EXPIRED", "DPM_RELEASED"};
	if ((status & 0xF000) == DPM_FAILED)
		sprintf (buf, "DPM_FAILED (%s)", sstrerror (status - DPM_FAILED));
	else if (status >= DPM_SUCCESS && status <= DPM_RELEASED)
		strcpy (buf, s_status[status >> 12]);
	else
		sprintf (buf, "%d", status);
	return (buf);
}

const char *
csumtype2srmname (const char *csumtype)
{
	const char *name = csumtype;
	if (!strcmp (csumtype, "AD"))
		name = "ADLER32";
	else if (!strcmp (csumtype, "CS"))
		name = "CRC32";
	else if (!strcmp (csumtype, "MD"))
		name = "MD5";
	return name;
}

char *pfr_to_turl(struct dpm_dbfd *dbfd, struct dpm_put_filereq *pfr_entry, char *turl, size_t bufsize) {
	char *p;
	*turl = '\0';

	if (pfr_entry->status != DPM_READY && pfr_entry->status != DPM_DONE)
		return turl;

	if (*pfr_entry->pfn) {
		if (strcmp (pfr_entry->protocol, "rfio") == 0 ||
		    strcmp (pfr_entry->protocol, "file") == 0) {
			p = strchr (pfr_entry->pfn, ':');
			if (p == NULL) { return (NULL); }
			p++;
		} else
			p = pfr_entry->pfn;
		snprintf (turl, bufsize, "%s://%s/%s", pfr_entry->protocol, pfr_entry->server, p);
	} else {
		char cgi[2048];
		char *sfn;
		if (strncmp (pfr_entry->to_surl, "srm://", 6) == 0) {
			if ((sfn = sfnfromsurl (pfr_entry->to_surl)) == NULL) {
				return (turl);
			}
		} else
			sfn = pfr_entry->to_surl;
		cgi[0] = '\0';
		if (is_xroot (pfr_entry->protocol)) {
			snprintf (&cgi[strlen (cgi)], sizeof(cgi)-strlen (cgi),
			    "&dpm.ftype=%c", pfr_entry->f_type);
			if (*pfr_entry->s_token) {
				snprintf (&cgi[strlen (cgi)], sizeof(cgi)-strlen (cgi),
				    "&dpm.stoken=%s", pfr_entry->s_token);
			}
			if (pfr_entry->requested_size != 0) {
				snprintf (&cgi[strlen (cgi)], sizeof(cgi)-strlen (cgi),
				    "&dpm.reqsize=%lld",
				    (long long)pfr_entry->requested_size);
			}
			if (*cgi) {
				snprintf (turl, bufsize,
				    "root://%s/%s?%s", pfr_entry->server, sfn, cgi);
			} else {
				snprintf (turl, bufsize,
				    "root://%s/%s", pfr_entry->server, sfn);
			}
		} else if (is_dav (pfr_entry->protocol)) {
			if (*pfr_entry->s_token) {
				struct dpm_space_reserv dpm_spcmd;
				snprintf (&cgi[strlen (cgi)], sizeof(cgi)-strlen (cgi),
				    "SpaceToken=%s", pfr_entry->s_token);
				if (dpm_get_spcmd_by_token (dbfd, pfr_entry->s_token,
				    &dpm_spcmd, 0, NULL) == 0 && *dpm_spcmd.u_token) {
					snprintf (&cgi[strlen (cgi)], sizeof(cgi)-strlen (cgi),
					    "&spacetoken=%s", dpm_spcmd.u_token);
				}
			}
			if (*cgi) {
				snprintf (turl, bufsize,
				    "%s://%s/%s?%s", pfr_entry->protocol, pfr_entry->server, sfn, cgi);
			} else {
				snprintf (turl, bufsize,
				    "%s://%s/%s", pfr_entry->protocol, pfr_entry->server, sfn);
			}
		}
	}

	return (turl);
}

char *gfr_to_turl(struct dpm_dbfd *dbfd, struct dpm_get_filereq *gfr_entry, char *turl, size_t bufsize) {
	char *p;
	*turl = '\0';

	if (gfr_entry->status != DPM_READY && gfr_entry->status != DPM_RELEASED)
		return turl;

	if (*gfr_entry->pfn) {
		if (strcmp (gfr_entry->protocol, "rfio") == 0 ||
		    strcmp (gfr_entry->protocol, "file") == 0) {
			p = strchr (gfr_entry->pfn, ':');
			if (p == NULL) { return (NULL); }
			p++;
		} else
			p = gfr_entry->pfn;
		snprintf (turl, bufsize, "%s://%s/%s", gfr_entry->protocol, gfr_entry->server, p);
	} else {
		char *sfn;
		if (strncmp (gfr_entry->from_surl, "srm://", 6) == 0) {
			if ((sfn = sfnfromsurl (gfr_entry->from_surl)) == NULL) {
				return (turl);
			}
		} else
			sfn = gfr_entry->from_surl;
		if (is_xroot (gfr_entry->protocol)) {
			snprintf (turl, bufsize,
			    "root://%s/%s", gfr_entry->server, sfn);
		} else if (is_dav (gfr_entry->protocol)) {
			snprintf (turl, bufsize,
			    "%s://%s/%s", gfr_entry->protocol, gfr_entry->server, sfn);
		}
	}

	return (turl);
}
