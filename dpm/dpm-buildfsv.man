.\" @(#)$RCSfile: dpm-buildfsv.man $ $Revision$ $Date$ CERN IT Maarten Litmaath/Jean-Philippe Baud
.\" Copyright (C) 2011 by CERN/IT
.\" All rights reserved
.\"
.TH DPM-BUILDFSV 1 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpm-buildfsv \- build Disk Pool Manager filesystem selection vector
.SH SYNOPSIS
.B dpm-buildfsv
.I input_file
.SH DESCRIPTION
.B dpm-buildfsv
builds the Disk Pool Manager filesystem selection vector for preview purpose.
The new algorithm allows to assign weights to filesystems and tries
to avoid overloading servers or filesystems.
To obtain the file system "vector" the code will first try to avoid
selecting the same file server twice in a row and then try to avoid
selecting the same file system twice in a row.
At any time it will try to select the server and the file system
that have the highest remaining weight and are compatible with
the previous rule.

A file system with a weight of N will appear N times in the vector.
At any time the next file system to use is the next entry in the vector.
When the last entry has been used, the DPM starts again with the first.
.LP
The
.I input_file
contains one line per filesystem.
Each line must contain the triplet server fs weight and an optional status.
.SH EXAMPLE
If the input file contains:
.LP
.nf
.ft CW
dskserver1	fs0	8
dskserver1	fs1	3
dskserver1	fs2	5
dskserver2	fs0	5
dskserver2	fs1	0
dskserver2	fs2	4
dskserver3	fs0	5
dskserver3	fs1	5
.ft
.fi
.LP
The program output would be:
.LP
.nf
.ft CW
fs_vector_len = 35
fs_vector[0] = dskserver1 fs0
fs_vector[1] = dskserver3 fs0
fs_vector[2] = dskserver1 fs0
fs_vector[3] = dskserver2 fs0
fs_vector[4] = dskserver1 fs0
fs_vector[5] = dskserver3 fs1
fs_vector[6] = dskserver1 fs0
fs_vector[7] = dskserver2 fs0
fs_vector[8] = dskserver1 fs2
fs_vector[9] = dskserver3 fs0
fs_vector[10] = dskserver1 fs0
fs_vector[11] = dskserver2 fs2
fs_vector[12] = dskserver1 fs2
fs_vector[13] = dskserver3 fs1
fs_vector[14] = dskserver1 fs0
fs_vector[15] = dskserver2 fs0
fs_vector[16] = dskserver1 fs1
fs_vector[17] = dskserver3 fs0
fs_vector[18] = dskserver1 fs2
fs_vector[19] = dskserver2 fs2
fs_vector[20] = dskserver1 fs0
fs_vector[21] = dskserver3 fs1
fs_vector[22] = dskserver1 fs1
fs_vector[23] = dskserver2 fs0
fs_vector[24] = dskserver1 fs2
fs_vector[25] = dskserver3 fs0
fs_vector[26] = dskserver1 fs0
fs_vector[27] = dskserver2 fs2
fs_vector[28] = dskserver3 fs1
fs_vector[29] = dskserver1 fs1
fs_vector[30] = dskserver2 fs0
fs_vector[31] = dskserver3 fs0
fs_vector[32] = dskserver1 fs2
fs_vector[33] = dskserver2 fs2
fs_vector[34] = dskserver3 fs1
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
