/*
 * Copyright (C) 2005-2011 by CERN/IT/GD/SC
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-drain.c,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

/*      dpm-drain - drain a component of the Light Weight Disk Pool Manager */

#include <grp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include "Cgetopt.h"
#include "Cglobals.h"
#include "Cpool_api.h"
#include "Csnprintf.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "rfio_api.h"
#include "serrno.h"
#include "u64subr.h"
#define LOGBUFSZ 1024
int condvar;
u_signed64 current_size;
gid_t *gid_list;	
int help_flag;
int jid;
u_signed64 min_size;
int nbfree;
int nbgids;
int pass_good;
int pass_rc;
int rc;
volatile sig_atomic_t sig_int_received;

void sig_handler(signum)
int signum;
{
	const char notice[34] = {"Interrupt received, finishing up.\n"};
	const char warn[50] = {"Interrupt received. One more to exit immediately.\n"};

	switch (++sig_int_received) {
		case 1:
			write (STDERR_FILENO, notice, sizeof (notice));
			break;
		case 2:
			write (STDERR_FILENO, warn, sizeof (warn));
			break;
		case 3:
			_exit (USERR);
			break;
	}
}

drainlogit(char *msg, ...)
{
	va_list args;
	time_t current_time;
	char prtbuf[LOGBUFSZ];
	int save_errno;
	int Tid = 0;
	struct tm *tm;
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
	struct tm tmstruc;
#endif

	save_errno = errno;
	va_start (args, msg);
	current_time = time (0);
#if (defined(_REENTRANT) || defined(_THREAD_SAFE)) && !defined(_WIN32)
	(void) localtime_r (&current_time, &tmstruc);
	tm = &tmstruc;
#else
	tm = localtime (&current_time);
#endif
	Cglobals_getTid (&Tid);
	if (Tid < 0)	/* main thread */
		Csnprintf (prtbuf, LOGBUFSZ, "%02d/%02d %02d:%02d:%02d %5d: ",
		    tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
		    jid);
	else
		Csnprintf (prtbuf, LOGBUFSZ, "%02d/%02d %02d:%02d:%02d %5d,%d: ",
		    tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
		    jid, Tid);
	Cvsnprintf (prtbuf+strlen(prtbuf), LOGBUFSZ-strlen(prtbuf), msg, args);
	if (prtbuf[LOGBUFSZ-2] != '\n') {
		prtbuf[LOGBUFSZ-2] = '\n';
		prtbuf[LOGBUFSZ-1] = '\0';
	}
	va_end (args);
	printf ("%s", prtbuf);
	fflush (stdout);
	errno = save_errno;
	return (0);
}

int main(argc, argv)
int argc;
char **argv;
{
	int c;
	void *doit(void *);
	char *dp;
	struct dpm_fs *dpm_fs;
	struct dpm_pool *dpm_pools;
	int errflg = 0;
	int flags;
	int found_fs = 0;
	char *fs = NULL;
	struct fs_list_s {
		char server[CA_MAXHOSTNAMELEN+1];
		char fs[80];
	} *fs_list = NULL;
	int fs_status;
#ifndef VIRTUAL_ID
	struct group *gr;
#endif
	char groupname[256];
	int i = 0;
	int ipool;
	int j = 0;
	Cns_list list;
	static struct Coptions longopts[] = {
		{"fs", REQUIRED_ARGUMENT, 0, OPT_FS},
		{"gid",REQUIRED_ARGUMENT,0,OPT_POOL_GID},
		{"group",REQUIRED_ARGUMENT,0,OPT_POOL_GROUP},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"size", REQUIRED_ARGUMENT, 0, OPT_DRAIN_SZ},
		{"poolname", REQUIRED_ARGUMENT, 0, OPT_POOL_NAME},
		{"server", REQUIRED_ARGUMENT, 0, OPT_FS_SERVER},
		{"threads", REQUIRED_ARGUMENT, 0, OPT_NBTHREADS},
		{0, 0, 0, 0}
	};
	struct Cns_filereplica *lp;
	int nbfs;	
	int nbpools;	
	int nbthreads = 1;	
	char *p = NULL;
	char *poolname = NULL;
	struct Cns_filereplica *rep_entries;
	int save_serrno;
	struct sigaction sigact;
	char *server = NULL;
	int target_fs_status;
	int thread_index;
	char u64buf[21];

	/* fix for bug LCGDM-1332 */
	setenv("GLOBUS_THREAD_MODEL", "pthread", 1);

	Copterr = 1;
	Coptind = 1;
	jid = getpid();

	Cns_set_selectsrvr (CNS_SSRV_NOTPATH);

	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_FS:
			if (strlen (Coptarg) > 79) {
				fprintf (stderr,
					"filesystem name too long: %s\n", Coptarg);
				errflg++;
			} else
				fs = Coptarg;
			break;
		case OPT_FS_SERVER:
			if (strlen (Coptarg) > CA_MAXHOSTNAMELEN) {
				fprintf (stderr,
					"server name too long: %s\n", Coptarg);
				errflg++;
			} else
				server = Coptarg;
			break;
		case OPT_DRAIN_SZ:
			p = Coptarg;
			while (*p >= '0' && *p <= '9') p++;
			if (! (*p == '\0' || ((*p == 'k' || *p == 'M' ||
			    *p == 'G' || *p == 'T' || *p == 'P') && *(p+1) == '\0'))) {
				fprintf (stderr,
				    "invalid minimum amount to drain %s\n", Coptarg);
				errflg++;
			} else
				min_size = strutou64 (Coptarg);
			break;
		case OPT_NBTHREADS:
			p = Coptarg;
			while (*p >= '0' && *p <= '9') p++;
			if (*p != '\0' || (nbthreads = atoi (Coptarg)) <= 0) {
				fprintf (stderr,
				    "invalid number of threads %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_POOL_NAME:
			if (strlen (Coptarg) > CA_MAXPOOLNAMELEN) {
				fprintf (stderr,
					"pool name too long: %s\n", Coptarg);
				errflg++;
			} else
				poolname = Coptarg;
			break;
		case OPT_POOL_GID:
			p = Coptarg;
			//check that the user didn't provide already a list of gid
			if (nbgids == 0) i = 0;
			while (*p) {
				if (*p == ',') nbgids++;
				p++;
			}
			nbgids++;
			if ((gid_list = (gid_t *) realloc (gid_list, nbgids * sizeof(gid_t))) == NULL) {
				fprintf (stderr, "Could not allocate memory for gids\n");
				exit (USERR);
			}
			p = strtok (Coptarg, ",");
			while (p) {
				if ((gid_list[i] = strtol (p, &dp, 10)) < 0 || *dp != '\0') {
					fprintf (stderr, "Invalid gid %s \n",p);
					errflg++;
				} else {
						
#ifdef VIRTUAL_ID
					if (gid_list[i] > 0 && Cns_getgrpbygid (gid_list[i], groupname) < 0) {
#else
					if (gid_list[i] > 0 && ! getgrgid (gid_list[i])) {
#endif
						fprintf (stderr, "Invalid gid %s \n", p);
						errflg++;
					}
				}
				i++;
				if ((p = strtok (NULL, ",")))
					*(p - 1) = ',';
			}
			break;
		case OPT_POOL_GROUP:
			/* if it contains ALL, it is like a normal dpm-drain without filtering */
			if (strstr (Coptarg, "ALL") != NULL)
				break;
			/* check that the user didn't provide already a list of gid */
			if (nbgids == 0) i = 0;
			p = Coptarg;
		  	while (*p) {
				if (*p == ',') nbgids++;
				p++;
			}
			nbgids++;
			if ((gid_list = (gid_t *) realloc (gid_list, nbgids * sizeof(gid_t))) == NULL) {
				fprintf (stderr, "Could not allocate memory for gids\n");
				exit (USERR);
			}
			p = strtok (Coptarg, ",");
			while (p) {
#ifdef VIRTUAL_ID
				if (strcmp (p, "root") == 0)
					gid_list[i] = 0;
				else if (Cns_getgrpbynam (p, &gid_list[i]) < 0) {
#else
				if ((gr = getgrnam (p)))
					gid_list[i] = gr->gr_gid;
				else {
#endif
					fprintf (stderr, "Invalid group : %s\n", p);
					errflg++;
				}
				i++;
				if ((p = strtok (NULL, ",")))
					*(p - 1) = ',';
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || (poolname == NULL && server == NULL && fs == NULL))
		errflg++;
	if (fs && server == NULL)
		errflg++;
	if (fs && poolname)
		errflg++;
	if (errflg || help_flag) {
		fprintf (stderr, "usage:\n%s\n%s\n%s\n%s",
		    "dpm-drain --poolname pool_name [--server fs_server] [--gid gid(s)]\n"
		    "\t[--group group_name(s)] [--size amount_to_drain] [--threads nbthreads]\n",
		    "dpm-drain --server fs_server [--gid gid(s)] [--group group_name(s)]\n"
		    "\t[--size amount_to_drain] [--threads nbthreads]\n",
		    "dpm-drain --server fs_server --fs fs_name [--gid gid(s)]\n"
		    "\t[--group group_name(s)] [--size amount_to_drain] [--threads nbthreads]\n",
		    "dpm-drain --help\n");
		exit (help_flag ? 0 : USERR);
	}

	/* set status to FS_RDONLY unless a specific server & fs is specified along with a limit */
	target_fs_status = ((nbgids == 0 && min_size == 0) || !(server && fs)) ? FS_RDONLY : -1;

	if (dpm_getpools (&nbpools, &dpm_pools) < 0) {
		fprintf (stderr, "dpm_getpools: %s\n", sstrerror (serrno));
		exit (USERR);
	}
	for (i = 0; i < nbpools; i++) {
		if (poolname && strcmp ((dpm_pools + i)->poolname, poolname))
			continue;
		if (dpm_getpoolfs ((dpm_pools + i)->poolname, &nbfs, &dpm_fs) < 0) {
			fprintf (stderr, 
				"dpm_getpoolfs %s: %s\n", (dpm_pools + i)->poolname, 
				sstrerror (serrno));
			exit (USERR);
		}
		for (j = 0; j < nbfs; j++) {
			if (server && strcmp ((dpm_fs + j)->server, server))
				continue;
			if (fs && strcmp (dpm_fs[j].fs, fs))
				continue;
			fs_status = target_fs_status;
			if (dpm_fs[j].status & FS_DISABLED)
				fs_status = -1;
			if (dpm_modifyfs ((dpm_fs + j)->server, (dpm_fs + j)->fs, fs_status, -1) < 0) {
				fprintf (stderr, 
					"dpm_modifyfs %s %s: %s\n", (dpm_fs + j)->server, 
					(dpm_fs + j)->fs, sstrerror (serrno));
				exit (USERR);
			}
			p = realloc (fs_list, sizeof(struct fs_list_s)*(found_fs+1));
			fs_list = (struct fs_list_s *)p;
			strcpy (fs_list[found_fs].server,(dpm_fs + j)->server);
			strcpy (fs_list[found_fs].fs,(dpm_fs + j)->fs);
			found_fs++;
		}
	}
	if (!found_fs) {
		fprintf (stderr, "No file systems matching specification\n");
		exit (USERR);
	}

	/* install a signal handler for SIGINT */

	memset(&sigact, '\0', sizeof(sigact));
	sigemptyset(&sigact.sa_mask);
	sigact.sa_handler = &sig_handler;
#ifdef SA_RESTART
	sigact.sa_flags |= SA_RESTART;
#endif
	sigaction(SIGINT, &sigact, (struct sigaction *)NULL);

	/* create a pool of threads */

	if ((ipool = Cpool_create (nbthreads, NULL)) < 0) {
		fprintf (stderr, "Cpool_create: %s\n", sstrerror(serrno));
		exit (SYERR);
	}
	if ((rep_entries =
	    calloc (nbthreads, sizeof(struct Cns_filereplica))) == NULL) {
		fprintf (stderr, "Could not allocate memory for thread_info\n");
		exit (SYERR);
	}
	nbfree = nbthreads;

	if (Cthread_mutex_lock (&condvar) < 0) {
		fprintf (stderr, "Cthread_mutex_lock: %s\n", sstrerror (serrno));
		exit (SYERR);
	}

	drainlogit ("Starting to drain with %d threads\n", nbthreads);

	while (1) {
		pass_good = 0;
		pass_rc = 0;
		flags = CNS_LIST_BEGIN;
		while (!sig_int_received && (serrno = 0, lp = Cns_listreplicax (poolname, server, fs, flags, &list)) != NULL) {
			if (flags != CNS_LIST_CONTINUE)
				flags = CNS_LIST_CONTINUE;

			for (i=0; i < found_fs; i++) {
				if (!strcmp (fs_list[i].server, lp->host) &&
				    !strcmp (fs_list[i].fs, lp->fs))
					break;
			}
			if (i >= found_fs) {
				if (!fs && !poolname && !strcmp (server, lp->host)) {
					drainlogit ("Filesystem %s on %s is not managed by DPM, ignoring file %s\n",
					    lp->fs, lp->host, lp->sfn);
				}
				continue;
			}

			while (! nbfree) {
				if (Cthread_cond_wait (&condvar) < 0) {
					drainlogit ("Cthread_cond_wait: %s\n",
					    sstrerror (serrno));
					exit (SYERR);
				}
			}

			if (min_size != 0 && current_size >= min_size)
				break;

			if (sig_int_received)
				break;

			if ((thread_index = Cpool_next_index (ipool)) < 0) {
				drainlogit ("Cpool_next_index: %s\n",
					sstrerror (serrno));
				rc = SYERR;
				break;
			}
			memcpy (rep_entries+thread_index, lp, sizeof(struct Cns_filereplica));
			if (Cpool_assign (ipool, &doit, rep_entries+thread_index, 1) < 0) {
				drainlogit ("Cpool_assign: %s\n", sstrerror (serrno));
				rc = SYERR;
				break;
			}
			nbfree--;
		}
		save_serrno = serrno;
		(void) Cns_listreplicax (poolname, server, fs, CNS_LIST_END, &list);
		while (nbfree < nbthreads) {
			if (Cthread_cond_wait (&condvar) < 0) {
				drainlogit ("Cthread_cond_wait: %s\n",
				    sstrerror (serrno));
				exit (SYERR);
			}
		}

		if (sig_int_received || lp != NULL || rc || save_serrno != SETIMEDOUT || pass_good == 0) {
			if (pass_rc)
				rc = 1;
			if (!sig_int_received && lp == NULL && save_serrno)
				drainlogit ("Cns_listreplicax: %s\n", sstrerror (save_serrno));
			break;
		}
	}
	if (Cthread_mutex_unlock (&condvar) < 0)
		drainlogit ("Cthread_mutex_unlock: %s\n", sstrerror (serrno));
	if (sig_int_received) {
		drainlogit ("\nFinishing after interrupt\n");
		rc = 1;
	} else if (rc)
		drainlogit ("\nThere were some errors which prevented dpm-drain from completing fully\n");
	else
		drainlogit ("\nFinished draining\n");
	drainlogit ("\nnumber of bytes drained %s\n", u64tostru (current_size, u64buf, 0));
	if (min_size != 0) {
		if (current_size < min_size)
			rc = 1;
	}
	exit (rc);
}

void *
doit(arg)
void *arg;
{
	char buf[256];
	time_t current_time;
	time_t f_lifetime;
	int good = 0;
	int hardrc = 0;	/* delreplica errors are more important because they mean space loss */
	int i;
	int j;
	struct Cns_filereplica *lp = (struct Cns_filereplica *) arg;
	int nbentries;
	char *p;
	char path[CA_MAXPATHLEN+1];
	char pfn[CA_MAXSFNLEN+1];
	struct dpns_filereplicax *rep_entries = NULL;
	struct dpm_space_metadata *spacemd = NULL;
	struct Cns_filestatg statbuf;
	int thread_rc = 0;
	char u64buf[21];

	if (Cns_statr (lp->sfn, &statbuf) < 0) {
		drainlogit ("Cns_statr %s: %s\n", lp->sfn, sstrerror (serrno));
		thread_rc = 1;
		goto end_proc;
	}
	if (nbgids != 0) {
		for (i = 0; i < nbgids; i++) {
			if (statbuf.gid == gid_list[i]) 
				break;
		}
		if (i >= nbgids)
			goto end_proc;
	}
	if (lp->status != '-') {	/* file is being populated/deleted */
		if (lp->status == 'P') {
			drainlogit ("The file %s is in the process of being uploaded, ignoring\n", lp->sfn);
		} else {
			drainlogit ("The file %s is recorded as being in the process of being deleted, ignoring it during drain\n", lp->sfn);
		}
		if (min_size == 0)
			thread_rc = 1;
		goto end_proc;
	}
	current_time = time (0);
	if (lp->ptime > current_time) {	/* file is pinned */
		drainlogit ("%s pinned until %s", lp->sfn, ctime_r (&lp->ptime, buf));
		if (min_size == 0)
			thread_rc = 1;
		goto end_proc;
	}

	if (dpns_getpath (NULL, lp->fileid, path) < 0) {
		drainlogit (
			"dpns_getpath: %s (%s): %s\n", lp->sfn,
			u64tostr (lp->fileid, u64buf, 0), sstrerror (serrno));
		thread_rc = 1;
		goto end_proc;
	}

	if (dpns_getreplicax (path, NULL, NULL, &nbentries, &rep_entries) < 0) {
		drainlogit ("dpns_getreplicax: %s: %s\n",
			path, sstrerror (serrno));
		thread_rc = 1;
		goto end_proc;
	}
	for (i = 0; i < nbentries; i++) {
		if (!strcmp (lp->sfn, rep_entries[i].sfn))
			break;
	}
	if (i >= nbentries) {
		drainlogit ("could not find replica of %s with pfn %s\n", path, lp->sfn);
		thread_rc = 1;
		goto end_proc;
	}
	drainlogit ("File:\t\t%s\n", path);
	drainlogit ("pfn:\t\t%s (of %d)\n", lp->sfn, nbentries);
	drainlogit ("replica type:\t%s\n",
	    (rep_entries[i].r_type == 'P') ? "primary" : "secondary");
	switch(lp->f_type) {
		case 'V':
			strcpy (buf, "volatile");
			break;
		case 'D':
			strcpy (buf, "durable");
			break;
		case 'P':
			strcpy (buf, "permanent");
			break;
		default:
			sprintf (buf, "'%c'",lp->f_type);
			break;
	}
	if (lp->f_type == 'P')
		strcat (buf, " (does not expire)");
	else {
		char buf2[26];
		ctime_r (&rep_entries[i].ltime, buf2);
		if ((p = strchr (buf2, '\n')))
			*p = '\0';
		sprintf (&buf[strlen (buf)], ", %s at %s",
		    (rep_entries[i].ltime <= current_time) ? "expired" : "expires", buf2);
	}
	drainlogit ("file type:\t%s\n", buf);
	drainlogit ("size:\t\t%s\n", u64tostru (statbuf.filesize, u64buf, 0));
	if (rep_entries[i].setname == NULL || *rep_entries[i].setname == '\0')
		drainlogit ("space:\t\tnot in any space\n");
	else {
		p = rep_entries[i].setname;
		drainlogit ("space:\t\t%s", p);
		j = 0;
		if (dpm_getspacemd (1, &p, &j, &spacemd) < 0) {
			if (serrno == EINVAL) {
				drainlogit (" (invalid space)\n");
				*p = '\0';
			} else
				drainlogit ("\n");
		} else if (j == 1 && spacemd)
			drainlogit (" (%s)\n", spacemd[0].u_token);
		else
			drainlogit ("\n");
		free (spacemd);
		spacemd = NULL;
	}
	if (lp->f_type != 'V' || rep_entries[i].ltime > current_time) {
		drainlogit ("replicating...\n");
		f_lifetime = rep_entries[i].ltime;
		rfio_errno = 0;
		if (dpm_replicatex (lp->sfn, lp->f_type, rep_entries[i].setname, f_lifetime, pfn) < 0) {
			drainlogit ("failed\n");
			drainlogit ("dpm_replicatex %s: %s\n", lp->sfn, rfio_serror());
			thread_rc = 1;
			goto end_proc;
		}
		if (lp->f_type != 'P') {
			free (rep_entries);
			rep_entries = NULL;
			if (dpns_getreplicax (path, NULL, NULL, &nbentries, &rep_entries) < 0) {
				drainlogit ("failed\n");
				drainlogit ("dpns_getreplicax: %s: %s\n",
				    path, sstrerror (serrno));
				if (dpm_delreplica (pfn) < 0) {
					drainlogit ("dpm_delreplica: %s: %s\n",
					    pfn, sstrerror (serrno));
					hardrc = 1;
				}
				thread_rc = 1;
				goto end_proc;
			}
			for (i = 0; i < nbentries; i++) {
				if (!strcmp (pfn, rep_entries[i].sfn))
					break;
			}
			if (i >= nbentries) {
				drainlogit ("failed\n");
				drainlogit ("could not find new replica\n");
				if (dpm_delreplica (pfn) < 0) {
					drainlogit ("dpm_delreplica: %s: %s\n",
					    pfn, sstrerror (serrno));
					hardrc = 1;
				}
				thread_rc = 1;
				goto end_proc;
			}
			if (rep_entries[i].ltime < f_lifetime) {
				drainlogit ("failed\n");
				drainlogit ("could not replicate to a new file with sufficient lifetime\n");
				if (dpm_delreplica (pfn) < 0) {
					drainlogit ("dpm_delreplica: %s: %s\n",
					    pfn, sstrerror (serrno));
					hardrc = 1;
				}
				thread_rc = 1;
				goto end_proc;
			}
		}
	}
	drainlogit ("deleting %s\n", lp->sfn);
	if (dpm_delreplica (lp->sfn) < 0) {
		drainlogit ("dpm_delreplica %s: %s\n", lp->sfn, sstrerror (serrno));
		hardrc = 1;
		thread_rc = 1;
		goto end_proc;
	}
	good = 1;
end_proc:
	free (rep_entries);
	if (Cthread_mutex_lock (&condvar) < 0) {
		drainlogit ("Cthread_mutex_lock: %s\n", sstrerror (serrno));
		exit (SYERR);
	}
	if (good) {
		current_size += statbuf.filesize;
		pass_good++;
	} else if (thread_rc) {
		if (hardrc)
			rc = hardrc;
		pass_rc = thread_rc;
	}
	nbfree++;
	if (Cthread_cond_signal (&condvar) < 0) {
		drainlogit ("Cthread_cond_signal: %s\n", sstrerror (serrno));
		exit (SYERR);
	}
	if (Cthread_mutex_unlock (&condvar) < 0) {
		drainlogit ("Cthread_mutex_unlock: %s\n", sstrerror (serrno));
		exit (SYERR);
	}
	return (NULL);
}
