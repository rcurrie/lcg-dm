/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-qryconf.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-qryconf - display the Disk Pool Manager configuration */
#include <errno.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
static char *decode_group(gid_t);
static char *decode_time(int, char *);
int group_flag;
int help_flag;
int proto_flag;
int si_flag;
int weight_flag;

main(argc, argv)
int argc;
char **argv;
{
	int c;
	struct dpm_fs *dpm_fs;
	struct dpm_pool *dpm_pools;
	int errflag = 0;
	int i;
	int j;
	static struct Coptions longopts[] = {
		{"groups", NO_ARGUMENT, &group_flag, 1},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"proto", NO_ARGUMENT, &proto_flag, 1},
		{"si", NO_ARGUMENT, &si_flag, 1},
		{"weight", NO_ARGUMENT, &weight_flag, 1},
		{0, 0, 0, 0}
	};
	int nb_supported_protocols;
	int nbfs;
	int nbpools = 0;
	char p_stat;
	char **supported_protocols;
	char tmpbuf[21];
	char tmpbuf2[21];
	char tmpbuf3[6];
	char tmpbuf4[6];
	char tmpbuf5[6];
	char tmpbuf6[6];

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case '?':
			errflag++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc)
		errflag++;
	if (errflag || help_flag) {
		fprintf (stderr, "usage: %s [--groups] [--help] [--proto] [--si] [--weight]\n", argv[0]);
		exit (errflag ? USERR : 0);
	}

	if (dpm_getpools (&nbpools, &dpm_pools) < 0) {
		fprintf (stderr, "dpm-qryconf: %s\n", sstrerror(serrno));
		errflag++;
	}
	for (i = 0; i < nbpools; i++) {
		printf ("POOL %s DEFSIZE %s GC_START_THRESH %d GC_STOP_THRESH %d DEF_LIFETIME %s DEFPINTIME %s MAX_LIFETIME %s MAXPINTIME %s FSS_POLICY %s GC_POLICY %s RS_POLICY %s ",
		    (dpm_pools + i)->poolname,
		    si_flag ?
		        u64tostrsi ((dpm_pools + i)->defsize, tmpbuf, 0) :
		        u64tostru ((dpm_pools + i)->defsize, tmpbuf, 0),
		    (dpm_pools + i)->gc_start_thresh, (dpm_pools + i)->gc_stop_thresh,
		    decode_time ((dpm_pools + i)->def_lifetime, tmpbuf3),
		    decode_time ((dpm_pools + i)->defpintime, tmpbuf4),
		    decode_time ((dpm_pools + i)->max_lifetime, tmpbuf5),
		    decode_time ((dpm_pools + i)->maxpintime, tmpbuf6),
		    (dpm_pools + i)->fss_policy, (dpm_pools + i)->gc_policy,
		    (dpm_pools + i)->rs_policy);
		for (j = 0; j < (dpm_pools + i)->nbgids; j++) {
			if (group_flag)
				printf ("%s%s", (j == 0) ? "GROUPS " : ",",
				    (dpm_pools + i)->gids[j] == 0 ? "ALL" :
				    decode_group ((dpm_pools + i)->gids[j]));
			else
				printf ("%s%d", (j == 0) ? "GIDS " : ",", (dpm_pools + i)->gids[j]);
		}
		printf (" S_TYPE %c MIG_POLICY %s RET_POLICY %c\n",
		    (dpm_pools + i)->s_type,
		    (dpm_pools + i)->mig_policy, (dpm_pools + i)->ret_policy);
		free ((dpm_pools + i)->gids);
		printf ("                              CAPACITY %s FREE %s (%5.1f%%)\n",
		    si_flag ?
		        u64tostrsi ((dpm_pools + i)->capacity, tmpbuf, 0) :
		        u64tostru ((dpm_pools + i)->capacity, tmpbuf, 0),
		    si_flag ?
		        u64tostrsi ((dpm_pools + i)->free >= 0 ? (dpm_pools + i)->free : 0, tmpbuf2, 0) :
		        u64tostru ((dpm_pools + i)->free >= 0 ? (dpm_pools + i)->free : 0, tmpbuf2, 0),
		    ((dpm_pools + i)->capacity && (dpm_pools + i)->free > 0) ?
		    (((double) (dpm_pools + i)->free * 100.) / (double) (dpm_pools + i)->capacity) : 0);

		if (dpm_getpoolfs ((dpm_pools + i)->poolname, &nbfs, &dpm_fs) < 0) {
			fprintf (stderr, "dpm-qryconf: %s\n", sstrerror(serrno));
			errflag++;
		}
		for (j = 0; j < nbfs; j++) {
			p_stat = '\0';
			printf ("  %s %s CAPACITY %s FREE %s (%5.1f%%) ", 
			    (dpm_fs + j)->server, (dpm_fs + j)->fs,
			    si_flag ?
			        u64tostrsi ((dpm_fs + j)->capacity, tmpbuf, 0) :
			        u64tostru ((dpm_fs + j)->capacity, tmpbuf, 0),
			    si_flag ?
			        u64tostrsi ((dpm_fs + j)->free >= 0 ? (dpm_fs + j)->free : 0, tmpbuf2, 0) :
			        u64tostru ((dpm_fs + j)->free >= 0 ? (dpm_fs + j)->free : 0, tmpbuf2, 0),
			    ((dpm_fs + j)->capacity && (dpm_fs + j)->free > 0) ?
			    (((double) (dpm_fs + j)->free * 100.) / (double) (dpm_fs + j)->capacity) : 0);
			if ((dpm_fs + j)->status  & FS_RDONLY) {
				printf (p_stat ? "|RDONLY" : "RDONLY");
				p_stat = '|';
			}
			if ((dpm_fs + j)->status  & FS_DISABLED) {
				printf (p_stat ? "|DISABLED" : "DISABLED");
				p_stat = '|';
			}
			if (weight_flag) {
				if (p_stat == '\0')
					printf ("ONLINE");
				printf (" %d", (dpm_fs + j)->weight);
			}
			printf ("\n");
		}
		if (nbfs)
			free (dpm_fs);
	}
	if (nbpools)
		free (dpm_pools);
	if (proto_flag) {
		if (dpm_getprotocols (&nb_supported_protocols, &supported_protocols) < 0) {
			fprintf (stderr, "dpm-qryconf: %s\n", sstrerror(serrno));
			errflag++;
		} else {
			printf ("\nSupported protocols are:");
			for (i = 0; i < nb_supported_protocols; i++) {
				printf (" %s", supported_protocols[i]);
				free (supported_protocols[i]);
			}
			printf ("\n");
			free (supported_protocols);
		}
	}
	exit (errflag ? 2 : 0);
}

static char *
decode_group(gid_t gid)
{
	struct group *gr;
	static char gidstr[256];

#ifdef VIRTUAL_ID
	if (gid == 0)
		return ("root");
	if (Cns_getgrpbygid (gid, gidstr) < 0)
#else
	if (gr = getgrgid (gid)) {
		strcpy (gidstr, gr->gr_name);
	} else
#endif
		sprintf (gidstr, "%d", gid);
	return (gidstr);
}

static char *
decode_time(int t, char *buf)
{
	float fnum;
	char unit;

	if (t == 0x7FFFFFFF) {
		strcpy (buf, "Inf");
		return (buf);
	}
	if (t >= 365 * 86400) {
		fnum = (float) t / (float) (365 * 86400);
		unit = 'y';
	} else if (t >= 30 * 86400) {
		fnum = (float) t / (float) (30 * 86400);
		unit = 'm';
	} else if (t >= 86400) {
		fnum = (float) t / 86400.;
		unit = 'd';
	} else if (t >= 3600) {
		fnum = (float) t / 3600.;
		unit = 'h';
	} else
		unit = ' ';
	if (unit != ' ')
		sprintf (buf, "%.1f%c", fnum, unit);
	else
		sprintf (buf, "%d", t);
	return (buf);
}
