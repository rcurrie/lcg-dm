/***********************************************
    SWIG input file for DPM python interface
 (including typemaps for non-trivial functions)
***********************************************/


#ifdef SWIGPYTHON



/***************************************
    Input (in) Typemaps 
***************************************/



%typemap(in) (gid_t *gids) {
    int size;

    if ($input == Py_None || (size = PyList_Size($input)) < 0)
        $1 = NULL;
    else {
        int i;

        $1 = (gid_t *) calloc (size + 1, sizeof (gid_t));

        for (i = 0; i < size; ++i) {
            $1[i] = PyInt_AsLong (PyList_GetItem($input, i));
        }
    }
}


%typemap(in, numinputs=0) (int *LENGTH, struct dpm_fs **OUTPUT){
   int tmp_int;
   struct dpm_fs *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(in, numinputs=0) (int *LENGTH, struct dpm_pool **OUTPUT){
   int tmp_int;
   struct dpm_pool *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(in, numinputs=0) (int *LENGTH, char ***OUTPUT){
   int tmp_int;
   char **tmp_struct = 0;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(in) (int LENGTH, char **TUPLE){

  /* Check the input list */
  if(!PyList_Check($input)){
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else {
    /* Length of the python list (how many space tokens) */
    $1 = PyList_Size($input);

    /* Reserve space to store all the space tokens in an array (after converting them) */
    $2 = (char **) calloc ($1, sizeof(char *)) ;

    int i;
    for (i=0; i<$1; ++i)
      $2[i] = PyString_AsString (PyList_GetItem($input, i));

  }/* end of: No error */

}/* end of: typemap(in) */



/* Free the temporary space tokens array passed to the C function after the dpm_getspacemd function has returned */
%typemap(freearg) (int LENGTH, char **TUPLE) {
  if($2){
     free($2);
   }
}


%typemap(in, numinputs=0) (int *LENGTH, struct dpm_space_metadata **OUTPUT){
   int tmp_int;
   struct dpm_space_metadata *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(in) (struct dpm_pool *POOL){

  /* Temp pointer */
  struct dpm_pool * aux_p;

  /* This moves aux_p to point to a newly allocated dpns_acl struct representing the item in the python list */
  SWIG_ConvertPtr($input, (void**) &aux_p,
          $descriptor(struct dpm_pool *), SWIG_POINTER_EXCEPTION);

  /* Copy the value to the array */
  $1 = aux_p;

}/* end of: typemap(in) */


%typemap(in) (int LENGTH, gid_t *INPUT){

  /* Empty list */
  if ($input == Py_None) {
      $1 = 0;
      $2 = NULL;
  }

  /* Check the input list */
  else if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else {
    int i;

    /* Length of the python list */
    $1 = PyList_Size($input);

    $2 = (gid_t *) calloc ($1, sizeof (gid_t));

    for (i = 0; i < $1; ++i) {
        $2[i] = PyInt_AsLong (PyList_GetItem($input, i));
    }
  }/* end of: No error */

}/* end of: typemap(in) */


/* Free the temporary array passed to the C function */
%typemap(freearg) (int LENGTH, gid_t *INPUT) {
  if ($2) {
     free ($2);
  }
}


%typemap(in, numinputs=0) (char *OUTPUT) {
   char tmp[2];
   $1 = tmp;
}


%typemap(in, numinputs=0) (char *TOKEN) {
   char tmp[CA_MAXDPMTOKENLEN+1];
   $1 = tmp;
}


%typemap(in, numinputs=0) (u_signed64 *OUTPUT) {
   u_signed64 tmp;
   $1 = &tmp;
}


%typemap(in, numinputs=0) (time_t *OUTPUT) {
   time_t tmp;
   $1 = &tmp;
}


%typemap(in, numinputs=0) (gid_t *OUTPUT) {
   gid_t tmp;
   $1 = &tmp;
}



/***************************************
    Output (out/argout) Typemaps 
***************************************/



%typemap(out) gid_t * {
  PyObject *myresult;

  /* Error, return None */
  if ($1 == 0) {
      myresult = Py_None;
      Py_INCREF(Py_None);
  }
  /* No error */
  else {
    int i;
    /* the size of the C table is the previous field (int type) of the same structure */
    int len = (arg1)->nbgids;
    myresult = PyList_New(len);

    for (i = 0; i < len; ++i)
      PyList_SetItem($result, i, PyInt_FromLong ($1[i]));
  }

  $result = my_t_output_helper(NULL, myresult);  
}


%typemap(argout) (int *LENGTH, struct dpm_fs **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if(result < 0){
      Py_INCREF(Py_None);
      myresult = Py_None;
  }
  /* No error */
  else{
    int i;
    myresult = PyList_New(*$1);

    for (i = 0; i < *$1; ++i) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct dpm_fs * aux = (struct dpm_fs *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpm_fs *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem(myresult, i, aux_obj);

    }
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (int *LENGTH, struct dpm_pool **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF(Py_None);
      myresult = Py_None;
  }
  /* No error */
  else {
    int i;
    myresult = PyList_New(*$1);

    for (i = 0; i < *$1; ++i) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct dpm_pool * aux = (struct dpm_pool *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpm_pool *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem(myresult, i, aux_obj);
    }
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (int *LENGTH, char ***OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if(result < 0) {
      Py_INCREF(Py_None);
      myresult = Py_None;
  }
  /* No error */
  else{
    int i;
    myresult = PyList_New(*$1);

    for (i = 0; i < *$1; ++i) {
      PyList_SetItem(myresult, i, PyString_FromString ((*$2)[i]));
      free((*$2)[i]);
    }
    free(*$2);
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (int *LENGTH, struct dpm_space_metadata **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result <  0) {
      Py_INCREF(Py_None);
      myresult = Py_None;
  }
  /* No error */
  else{
    int i;
    myresult = PyList_New(*$1);

    for (i = 0; i < *$1; ++i){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct dpm_space_metadata * aux = (struct dpm_space_metadata *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpm_space_metadata *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem(myresult, i, aux_obj);
    }
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (char *OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
    Py_INCREF(Py_None);
    myresult = Py_None;
  }
  /* No error */
  else {
    $1[1] = 0;
    myresult = PyString_FromString ($1);
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (char *TOKEN) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
    Py_INCREF(Py_None);
    myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyString_FromString ($1);
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (u_signed64 *OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
    Py_INCREF(Py_None);
    myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyLong_FromLongLong (*$1);
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (time_t *OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
    Py_INCREF(Py_None);
    myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyLong_FromLong (*$1);
  }

  $result = my_t_output_helper($result, myresult);  
}


%typemap(argout) (gid_t *OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
    Py_INCREF(Py_None);
    myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyLong_FromLong (*$1);
  }

  $result = my_t_output_helper($result, myresult);  
}


#endif /* SWIGPYTHON */
