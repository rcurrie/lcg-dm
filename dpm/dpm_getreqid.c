/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_getreqid.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_getreqid - get request id for a set of requests */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_getreqid (const char *u_token, int *nbreplies, struct dpm_tokeninfo **tokeninfos)
{
	int c;
	char errstring[256];
	char func[16];
	gid_t gid;
	int i;
	int msglen;
	char *rbp;
	char repbuf[4+256];
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_getreqid");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! nbreplies || ! tokeninfos) {
		serrno = EFAULT;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 5 * LONGSIZE;
	if (u_token)
		msglen += strlen (u_token) + 1;
	else
		msglen++;

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC);
	marshall_LONG (sbp, DPM_GETREQID);
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	if (u_token) {
		marshall_STRING (sbp, u_token);
	} else {
		marshall_STRING (sbp, "");
	}

	c = send2dpm (NULL, sendbuf, msglen, repbuf, sizeof(repbuf),
	    (void **)tokeninfos, nbreplies);
	if (c == 0) { 
		rbp = repbuf;
		unmarshall_LONG (rbp, c);
		if (c) {
			serrno = c - DPM_FAILED;
			c = -1;
		}
		unmarshall_STRING (rbp, errstring);
		if (*errstring)
			dpm_errmsg (func, "%s\n", errstring);
	}
	return (c);
}
