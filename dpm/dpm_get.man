.\" @(#)$RCSfile: dpm_get.man,v $ $Revision: 1.2 $ $Date: 2006/12/20 15:21:57 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GET 3 "$Date: 2006/12/20 15:21:57 $" LCG "DPM Library Functions"
.SH NAME
dpm_get \- make a set of existing files available for I/O
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_get (int " nbreqfiles ,
.BI "struct dpm_getfilereq *" reqfiles ,
.BI "int " nbprotocols ,
.BI "char **" protocols ,
.BI "char *" u_token ,
.BI "time_t " retrytime ,
.BI "char *" r_token ,
.BI "int *" nbreplies ,
.BI "struct dpm_getfilestatus **" filestatuses )
.SH DESCRIPTION
.B dpm_get
makes a set of existing files available for I/O.
.LP
The input arguments are:
.TP
.I nbreqfiles
specifies the number of files belonging to the request.
.TP
.I reqfiles
specifies the array of file requests (dpm_getfilereq structures).
.PP
.nf
.ft CW
struct dpm_getfilereq {
	char		*from_surl;
	time_t	lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	int		flags;
};
.ft
.fi
.TP
.I nbprotocols
specifies the number of protocols.
.TP
.I protocols
specifies the array of protocols.
.TP
.I u_token
specifies the user provided description associated with the request.
.TP
.I retrytime
This field is currently ignored.
.LP
The output arguments are:
.TP
.I r_token
Address of a buffer to receive the system allocated token.
The buffer must be at least CA_MAXDPMTOKENLEN+1 characters long.
.TP
.I nbreplies
will be set to the number of replies in the array of file statuses.
.TP
.I filestatuses
will be set to the address of an array of dpm_getfilestatus structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_getfilestatus {
	char		*from_surl;
	char		*turl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t	pintime;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.IR reqfiles ,
.IR protocols ,
.IR r_token ,
.I nbreplies
or
.I filestatuses
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request.
.TP
.B EINVAL
.I nbreqfiles
or
.I nbprotocols
is not strictly positive, the protocols are not supported, the length of the
user request description is greater than 255 or all file requests have errors.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
