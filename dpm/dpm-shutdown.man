.\" @(#)$RCSfile: dpm-shutdown.man,v $ $Revision: 1.1.1.1 $ $Date: 2004/12/14 07:38:18 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM-SHUTDOWN 8 "$Date: 2004/12/14 07:38:18 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-shutdown \- shutdown the Disk Pool Manager
.SH SYNOPSIS
.B dpm-shutdown
.RB [ -f ]
.B -h
.I server
.SH DESCRIPTION
.B dpm-shutdown
shuts the Disk Pool Manager down.
It waits for outstanding requests to complete. New requests will be rejected
with
.B serrno
set to
.BR EDPMNACT .
Connections to the database server are closed one by one when they are not
used anymore and when all connections are closed, the Disk Pool Manager server
exits.
.SH OPTIONS
.TP
.B -f
force shutdown, i.e. long operations like copy can be suspended.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
