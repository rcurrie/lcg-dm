.\" @(#)$RCSfile: dpm_seterrbuf.man,v $ $Revision: 1.2 $ $Date: 2007/05/14 05:53:52 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2007 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_SETERRBUF 3 "$Date: 2007/05/14 05:53:52 $" LCG "DPM Library Functions"
.SH NAME
dpm_seterrbuf \- set receiving buffer for error messages
.SH SYNOPSIS
.BI "int dpm_seterrbuf (char *" buffer ,
.BI "int " buflen )
.SH DESCRIPTION
.B dpm_seterrbuf
tells the Disk Pool Manager client API the address and the size of the buffer
to be used for error messages. If this routine is not called or if
.I buffer
is NULL, the messages are printed on
.BR stderr .
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
memory could not be allocated for the thread specific information.
