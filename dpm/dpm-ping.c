/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-ping.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-ping - check server alive and return version number */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#if !defined(_WIN32)
#include <unistd.h>
#endif
#include "dpm_api.h"
#include "serrno.h"
extern	char	*optarg;
extern	int	optind;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	char info[256];
	char *server = NULL;

        while ((c = getopt (argc, argv, "h:")) != EOF) {
                switch (c) {
		case 'h':
			server = optarg;
			break;
                case '?':
                        errflg++;
                        break;
                default:
                        break;
                }
        }
        if (optind < argc) {
                errflg++;
        }
        if (errflg) {
                fprintf (stderr, "usage: %s [-h server]\n", argv[0]);
                exit (USERR);
        }

	putenv ("DPM_CONRETRY=0");
	if (dpm_ping (server, info) < 0) {
		fprintf (stderr, "dpm-ping: %s\n", sstrerror(serrno));
		exit (USERR);
	}
	printf ("%s\n", info);
	exit (0);
}
