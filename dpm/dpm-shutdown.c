/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-shutdown.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-shutdown - shutdown the Disk Pool Manager server */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"
extern	char	*optarg;
extern	int	optind;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	int force = 0;
	gid_t gid;
	int msglen;
	char *q;
	char *sbp;
	char sendbuf[REQBUFSZ];
	char *server = NULL;
	uid_t uid;

	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		fprintf (stderr, DP053);
		exit (USERR);
	}
#endif
	while ((c = getopt (argc, argv, "fh:")) != EOF) {
		switch (c) {
		case 'f':
			force++;
			break;
		case 'h':
			server = optarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (optind < argc || server == NULL) {
		errflg++;
	}
	if (errflg) {
		fprintf (stderr, "usage: %s [-f] -h server\n", argv[0]);
		exit (USERR);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC);
	marshall_LONG (sbp, DPM_SHUTDOWN);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_WORD (sbp, force);

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	putenv ("DPM_CONRETRY=0");
	if (send2dpm (server, sendbuf, msglen, NULL, 0) < 0) {
		fprintf (stderr, "dpm-shutdown: %s\n", sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
