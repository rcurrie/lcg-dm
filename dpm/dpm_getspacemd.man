.\" @(#)$RCSfile: dpm_getspacemd.man,v $ $Revision: 1.4 $ $Date: 2008/09/25 10:17:46 $ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2008 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH DPM_GETSPACEMD 3 "$Date: 2008/09/25 10:17:46 $" LCG "DPM Library Functions"
.SH NAME
dpm_getspacemd \- get space metadata
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getspacemd (int " nbtokens ,
.BI "char **" s_tokens ,
.BI "int *" nbreplies ,
.BI "struct dpm_space_metadata **" spacemd )
.SH DESCRIPTION
.B dpm_getspacemd
gets space metadata.
.LP
The input arguments are:
.TP
.I nbtokens
specifies the number of tokens in the request.
.TP
.I s_tokens
specifies an array of tokens returned by previous reservespace requests.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of space metadata.
.TP
.I spacemd
will be set to the address of an array of dpm_space_metadata structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_space_metadata {
	char		s_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	uid_t		s_uid;
	gid_t		s_gid;
	char		ret_policy;
	char		ac_latency;
	char		u_token[256];
	char		client_dn[256];
	u_signed64	t_space;	/* Total space */
	u_signed64	g_space;	/* Guaranteed space */
	signed64	u_space;	/* Unused space */
	char		poolname[CA_MAXPOOLNAMELEN+1];
	time_t		a_lifetime;	/* Lifetime assigned */
	time_t		r_lifetime;	/* Remaining lifetime */
	int		nbgids;
	gid_t		*gids;		/* restrict the space to given group(s) */
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I s_tokens ,
.I nbreplies
or
.I spacemd
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for storing the reply.
.TP
.B EINVAL
.I nbtokens
is not strictly positive or the specified tokens are invalid/unknown.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
