/*
 * Copyright (C) 2011 by CERN/IT/GT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-buildfsv.c $ $Revision$ $Date$ CERN IT-GT Jean-Philippe Baud";
#endif /* not lint */

/*      dpm-buildfsv - build filesystem selection vector */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include "dpm.h"
#include "dpm_server.h"
#include "serrno.h"

main(int argc, char **argv)
{
	char buf[256];
	char *dp;
	FILE *fp;
	int nbfs = 0;
	struct dpm_fs *dpm_fs;
	struct dpm_fs *elemp;
	int *fs_vector;
	int fs_vector_len;
	int i;
	int j;
	int n;
	char *p;
	char *q;
	int status;
	char statusa[50];

	if (argc != 2) {
		fprintf (stderr, "usage: %s input_file\n%s", argv[0],
		    "\teach line must contain the triplet server fs weight and an optional status\n");
		exit (1);
	}
	if ((fp = fopen (argv[1], "r")) == NULL) {
		perror ("fopen");
		exit (1);
	}
	while (fgets (buf, sizeof(buf), fp) != NULL) {
		n = strlen (buf);
		if (buf[n - 1] != '\n') {
			fprintf (stderr, "line %d too long\n", nbfs);
			exit (1);
		} else
			buf[n - 1] = '\0';
		nbfs++;
	}
	if ((dpm_fs = calloc (nbfs, sizeof(struct dpm_fs))) == NULL) {
		perror ("dpm_fs alloc");
		exit (1);
	}
	rewind (fp);
	elemp = dpm_fs;
	i = 0;
	while (fgets (buf, sizeof(buf), fp) != NULL) {
		n = strlen (buf);
		buf[n - 1] = '\0';
		p = strtok (buf, " \t");
		if (! p) {
			fprintf (stderr, "syntax error at line %d\n", i);
			exit (1);
		}
		strcpy (elemp->server, p);
		p = strtok (NULL, " \t");
		if (! p) {
			fprintf (stderr, "syntax error at line %d\n", i);
			exit (1);
		}
		strcpy (elemp->fs, p);
		p = strtok (NULL, " \t\n");
		if (! p) {
			fprintf (stderr, "syntax error at line %d\n", i);
			exit (1);
		}
		elemp->weight = atoi (p);
		p = strtok (NULL, " \t\n");
		if (p) {
			if (isdigit (*p)) {
				if ((status = strtol (p, &dp, 10)) < 0 ||
				    *dp != '\0') {
					fprintf (stderr,
					    "invalid status %s at line %d\n", p, i);
					exit (1);
				}
			} else {
				if (strlen (p) >= sizeof (statusa)) {
					fprintf (stderr,
					    "invalid status %s at line %d\n", p, i);
					exit (1);
					break;
				}
				status = 0;
				strcpy (statusa, p);
				q = strtok (statusa, "|");
				while (q) {
					if (strcmp (q, "DISABLED") == 0)
						status |= FS_DISABLED;
					else if (strcmp (q, "RDONLY") == 0)
						status |= FS_RDONLY;
					else {
						fprintf (stderr,
						    "invalid status %s at line %d\n", p, i);
						exit (1);
						break;
					}
					q = strtok (NULL, "|");
				}
			}
			elemp->status = status;
		}
		elemp++;
		i++;
	}
	fclose (fp);

	if ((fs_vector_len = dpm_build_fs_vector (nbfs, dpm_fs, &fs_vector)) < 0) {
		sperror ("dpm_build_fs_vector");
		exit (1);
	}
	printf ("fs_vector_len = %d\n", fs_vector_len);
	for (i = 0; i < fs_vector_len; i++) {
		j = *(fs_vector + i);
		printf ("fs_vector[%d] = %s %s\n", i, (dpm_fs + j)->server,
		    (dpm_fs + j)->fs);
	}
	exit (0);
}
