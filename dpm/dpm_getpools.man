.\" @(#)$RCSfile: dpm_getpools.man,v $ $Revision: 1.4 $ $Date: 2007/04/30 06:39:28 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2007 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GETPOOLS 3 "$Date: 2007/04/30 06:39:28 $" LCG "DPM Library Functions"
.SH NAME
dpm_getpools \- get list of pools
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getpools (int *" nbpools ,
.BI "struct dpm_pool **" dpm_pools )
.SH DESCRIPTION
.B dpm_getpools
gets list of pools.
.LP
The output arguments are:
.TP
.I nbpools
will be set to the number of replies in the array of pools.
.TP
.I dpm_pools
will be set to the address of an array of dpm_pool structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore. This is also necessary for the gids entry of each dpm_pool.
.PP
.nf
.ft CW
struct dpm_pool {
	char		poolname[CA_MAXPOOLNAMELEN+1];
	u_signed64	defsize;
	int		gc_start_thresh;
	int		gc_stop_thresh;
	int		def_lifetime;
	int		defpintime;
	int		max_lifetime;
	int		maxpintime;
	char		fss_policy[CA_MAXPOLICYLEN+1];
	char		gc_policy[CA_MAXPOLICYLEN+1];
	char		mig_policy[CA_MAXPOLICYLEN+1];
	char		rs_policy[CA_MAXPOLICYLEN+1];
	int		nbgids
	gid_t		*gids;		/* restrict the pool to given group(s) */
	char		ret_policy;	/* retention policy: 'R', 'O' or 'C' */
	char		s_type;		/* space type: 'V', 'D' or 'P' */
	u_signed64	capacity;
	u_signed64	free;
	struct dpm_fs	*elemp;
	int		nbelem;
	int		next_elem;	/* next pool element to be used */
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I nbpools
or
.I dpm_pools
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for storing the reply.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SECOMERR
Communication error.
