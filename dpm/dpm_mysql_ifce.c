/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_mysql_ifce.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <errmsg.h>
#include <mysqld_error.h>
#include "dpm.h"
#include "dpm_server.h"
#include "serrno.h"
#include "u64subr.h"
gid_t *Cdecode_groups (char *, int *);
char *Cencode_groups (int, gid_t *, char *, size_t);

static char db_name[33];
static char db_pwd[33];
static char db_srvr[33];
static char db_user[33];

static inline unsigned long
dm_escape_string(mysql, to, from, length, max)
MYSQL *mysql;
char *to;
const char *from;
unsigned long length;
unsigned long max;
{
	unsigned long num = 0;
	if (max == 0)
		return 0;
	num = (max-1)/2;
	if (length < num)
		num = length;
	return mysql_real_escape_string (mysql, to, from, num);
}

int dm_mysql_query(MYSQL *mysql, const char *stmt_str) {
	static char func[] = "dm_mysql_query";
	int i,ret;
	for(i=0;i<=MAXRETRY;i++) {
		if (i>0) { dpmlogit (func,
			"Restarting query after lock wait timeout\n"); }
		ret = mysql_query (mysql, stmt_str);
		if (ret && mysql_errno (mysql) == ER_LOCK_WAIT_TIMEOUT) {
			continue;
		}
		break;
	}
	return ret;
}

static int
dm_snprintf(char *str, size_t size, const char *format, ...)
{  
	va_list args;
	int ret;

	va_start (args, format);

	ret = vsnprintf(str, size, format, args);
	if (size && (ret < 0 || (size_t)ret >= size))
		*str = '\0';

	va_end (args);
	return ret;   
}

int
dpm_get_dbconf(dpmconfig)
char *dpmconfig;
{
	FILE *cf;
	char cfbuf[80];
	char func[16];
	char dpmconfigfile[CA_MAXPATHLEN+1];
	char *p;
	char *p_n, *p_p, *p_s, *p_u;
	int rc = 0;

	strcpy (func, "dpm_get_dbconf");

	/* get DB login info from the Disk Pool Manager server config file */

	if (! dpmconfig) {
		if (strncmp (DPMCONFIG, "%SystemRoot%\\", 13) == 0 &&
		    (p = getenv ("SystemRoot")))
			sprintf (dpmconfigfile, "%s%s", p, strchr (DPMCONFIG, '\\'));
		else
			strcpy (dpmconfigfile, DPMCONFIG);
	} else if (strlen (dpmconfig) > CA_MAXPATHLEN) {
		serrno = ENAMETOOLONG;
		return (-1);
	} else
		strcpy (dpmconfigfile, dpmconfig);

	if ((cf = fopen (dpmconfigfile, "r")) == NULL) {
		dpmlogit (func, DP023, dpmconfigfile);
		serrno = SENOCONFIG;
		return (-1);
	}
	if (fgets (cfbuf, sizeof(cfbuf), cf) &&
	    strlen (cfbuf) >= 5 && (p_u = strtok (cfbuf, "/\n")) &&
	    (p_p = strtok (NULL, "@\n")) && (p_s = strtok (NULL, "/\n"))) {
		p_n = strtok (NULL, "\n");
		if (strlen (p_u) >= sizeof(db_user) ||
		    strlen (p_p) >= sizeof(db_pwd) ||
		    strlen (p_s) >= sizeof(db_srvr) ||
		    (p_n && strlen (p_n) >= sizeof(db_name))) {
			dpmlogit (func, DP009, dpmconfigfile, "incorrect");
			serrno = EINVAL;
			rc = -1;
		} else {
			strcpy (db_user, p_u);
			strcpy (db_pwd, p_p);
			strcpy (db_srvr, p_s);
			if (p_n)
				strcpy (db_name, p_n);
			else
				strcpy (db_name, "dpm_db");
		}
	} else {
		dpmlogit (func, DP009, dpmconfigfile, "incorrect");
		serrno = EINVAL;
		rc = -1;
	}
	(void) fclose (cf);
	return (rc);
}

dpm_init_dbpkg()
{
	return (0);
}

void
dpm_mysql_error(func, sql_method, dbfd)
char *func;
char *sql_method;
struct dpm_dbfd *dbfd;
{
	int error = mysql_errno (&dbfd->mysql);

	dpmlogit (func, "%s error: %s\n", sql_method, mysql_error (&dbfd->mysql));
	if (error == CR_SERVER_GONE_ERROR || error == CR_SERVER_LOST) {
		dpmlogit (func, "Trying to reconnect\n");
		dpm_closedb (dbfd);
		if (dpm_opendb (dbfd) < 0)
			dpmlogit (func, "Reconnect failed\n");
	}
	serrno = SEINTERNAL;
}

dpm_abort_tr(dbfd)
struct dpm_dbfd *dbfd;
{
	(void) dm_mysql_query (&dbfd->mysql, "ROLLBACK");
	if (!dbfd->tr_started) {
		dpmlogit ("dpm_abort_tr", "Aborted transaction when none had been started\n");
	}
	dbfd->tr_started = 0;
	return (0);
}

dpm_closedb(dbfd)
struct dpm_dbfd *dbfd;
{
	mysql_close (&dbfd->mysql);
	return (0);
}

dpm_decode_cpr_entry(row, lock, rec_addr, cpr_entry)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_copy_filereq *cpr_entry;
{
	int i = 0;
	char *p;
	memset (cpr_entry, 0, sizeof(struct dpm_copy_filereq));

	if (lock) {
		memset (rec_addr, 0, sizeof(dpm_dbrec_addr));
		if ((p=row[i++]))
			strncat (*rec_addr, p, sizeof(dpm_dbrec_addr)-1);
	}
	if ((p=row[i++]))
		strncat (cpr_entry->r_token, p, sizeof(cpr_entry->r_token)-1);
	if ((p=row[i++]))
		cpr_entry->f_ordinal = atoi (p);
	if ((p=row[i++]))
		strncat (cpr_entry->from_surl, p, sizeof(cpr_entry->from_surl)-1);
	if ((p=row[i++]))
		strncat (cpr_entry->to_surl, p, sizeof(cpr_entry->to_surl)-1);
	if ((p=row[i++]))
		cpr_entry->f_lifetime = atoi (p);
	if ((p=row[i++]))
		cpr_entry->f_type = *p;
	if ((p=row[i++]))
		strncat (cpr_entry->s_token, p, sizeof(cpr_entry->s_token)-1);
	if ((p=row[i++]))
		cpr_entry->ret_policy = *p;
	if ((p=row[i++]))
		cpr_entry->ac_latency = *p;
	if ((p=row[i++]))
		cpr_entry->flags = atoi (p);
	if ((p=row[i++]))
		cpr_entry->actual_size = strtou64 (p);
	if ((p=row[i++]))
		cpr_entry->status = atoi (p);
	if ((p=row[i]))
		strncat (cpr_entry->errstring, p, sizeof(cpr_entry->errstring)-1);
}

dpm_decode_fs_entry(row, lock, rec_addr, fs_entry)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_fs *fs_entry;
{
	int i = 0;
	char *p;
	memset (fs_entry, 0, sizeof(struct dpm_fs));

	if (lock) {
		memset (rec_addr, 0, sizeof(dpm_dbrec_addr));
		if ((p=row[i++]))
			strncat (*rec_addr, p, sizeof(dpm_dbrec_addr)-1);
	}
	if ((p=row[i++]))
		strncat (fs_entry->poolname, p, sizeof(fs_entry->poolname)-1);
	if ((p=row[i++]))
		strncat (fs_entry->server, p, sizeof(fs_entry->server)-1);
	if ((p=row[i++]))
		strncat (fs_entry->fs, p, sizeof(fs_entry->fs)-1);
	if ((p=row[i++]))
		fs_entry->status = atoi (p);
	if ((p=row[i]))
		fs_entry->weight = atoi (p);
}

dpm_decode_gfr_entry(row, lock, rec_addr, gfr_entry)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_get_filereq *gfr_entry;
{
	int i = 0;
	char *p;
	memset (gfr_entry, 0, sizeof(struct dpm_get_filereq));

	if (lock) {
		memset (rec_addr, 0, sizeof(dpm_dbrec_addr));
		if ((p=row[i++]))
			strncat (*rec_addr, p, sizeof(dpm_dbrec_addr)-1);
	}
	if ((p=row[i++]))
		strncat (gfr_entry->r_token, p, sizeof(gfr_entry->r_token)-1);
	if ((p=row[i++]))
		gfr_entry->f_ordinal = atoi (p);
	if ((p=row[i++]))
		gfr_entry->r_uid = atoi (p);
	if ((p=row[i++]))
		strncat (gfr_entry->from_surl, p, sizeof(gfr_entry->from_surl)-1);
	if ((p=row[i++]))
		strncat (gfr_entry->protocol, p, sizeof(gfr_entry->protocol)-1);
	if ((p=row[i++]))
		gfr_entry->lifetime = atoi (p);
	if ((p=row[i++]))
		gfr_entry->f_type = *p;
	if ((p=row[i++]))
		strncat (gfr_entry->s_token, p, sizeof(gfr_entry->s_token)-1);
	if ((p=row[i++]))
		gfr_entry->ret_policy = *p;
	if ((p=row[i++]))
		gfr_entry->flags = atoi (p);
	if ((p=row[i++]))
		strncat (gfr_entry->server, p, sizeof(gfr_entry->server)-1);
	if ((p=row[i++]))
		strncat (gfr_entry->pfn, p, sizeof(gfr_entry->pfn)-1);
	if ((p=row[i++]))
		gfr_entry->actual_size = strtou64 (p);
	if ((p=row[i++]))
		gfr_entry->status = atoi (p);
	if ((p=row[i]))
		strncat (gfr_entry->errstring, p, sizeof(gfr_entry->errstring)-1);
}

dpm_decode_pfr_entry(row, lock, rec_addr, pfr_entry)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_put_filereq *pfr_entry;
{
	int i = 0;
	char *p;
	memset (pfr_entry, 0, sizeof(struct dpm_put_filereq));

	if (lock) {
		memset (rec_addr, 0, sizeof(dpm_dbrec_addr));
		if ((p=row[i++]))
			strncat (*rec_addr, p, sizeof(dpm_dbrec_addr)-1);
	}
	if ((p=row[i++]))
		strncat (pfr_entry->r_token, p, sizeof(pfr_entry->r_token)-1);
	if ((p=row[i++]))
		pfr_entry->f_ordinal = atoi (p);
	if ((p=row[i++]))
		strncat (pfr_entry->to_surl, p, sizeof(pfr_entry->to_surl)-1);
	if ((p=row[i++]))
		strncat (pfr_entry->protocol, p, sizeof(pfr_entry->protocol)-1);
	if ((p=row[i++]))
		pfr_entry->lifetime = atoi (p);
	if ((p=row[i++]))
		pfr_entry->f_lifetime = atoi (p);
	if ((p=row[i++]))
		pfr_entry->f_type = *p;
	if ((p=row[i++]))
		strncat (pfr_entry->s_token, p, sizeof(pfr_entry->s_token)-1);
	if ((p=row[i++]))
		pfr_entry->ret_policy = *p;
	if ((p=row[i++]))
		pfr_entry->ac_latency = *p;
	if ((p=row[i++]))
		pfr_entry->requested_size = strtou64 (p);
	if ((p=row[i++]))
		strncat (pfr_entry->server, p, sizeof(pfr_entry->server)-1);
	if ((p=row[i++]))
		strncat (pfr_entry->pfn, p, sizeof(pfr_entry->pfn)-1);
	if ((p=row[i++]))
		pfr_entry->actual_size = strtou64 (p);
	if ((p=row[i++]))
		pfr_entry->status = atoi (p);
	if ((p=row[i]))
		strncat (pfr_entry->errstring, p, sizeof(pfr_entry->errstring)-1);
}

dpm_decode_pool_entry(row, lock, rec_addr, pool_entry)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_pool *pool_entry;
{
	int i = 0;
	char *p;
	memset (pool_entry, 0, sizeof(struct dpm_pool));

	if (lock) {
		memset (rec_addr, 0, sizeof(dpm_dbrec_addr));
		if ((p=row[i++]))
			strncat (*rec_addr, p, sizeof(dpm_dbrec_addr)-1);
	}
	if ((p=row[i++]))
		strncat (pool_entry->poolname, p, sizeof(pool_entry->poolname)-1);
	if ((p=row[i++]))
		pool_entry->defsize = strtou64 (p);
	if ((p=row[i++]))
		pool_entry->gc_start_thresh = atoi (p);
	if ((p=row[i++]))
		pool_entry->gc_stop_thresh = atoi (p);
	if ((p=row[i++]))
		pool_entry->def_lifetime = atoi (p);
	if ((p=row[i++]))
		pool_entry->defpintime = atoi (p);
	if ((p=row[i++]))
		pool_entry->max_lifetime = atoi (p);
	if ((p=row[i++]))
		pool_entry->maxpintime = atoi (p);
	if ((p=row[i++]))
		strncat (pool_entry->fss_policy, p, sizeof(pool_entry->fss_policy)-1);
	if ((p=row[i++]))
		strncat (pool_entry->gc_policy, p, sizeof(pool_entry->gc_policy)-1);
	if ((p=row[i++]))
		strncat (pool_entry->mig_policy, p, sizeof(pool_entry->mig_policy)-1);
	if ((p=row[i++]))
		strncat (pool_entry->rs_policy, p, sizeof(pool_entry->rs_policy)-1);
	pool_entry->gids = Cdecode_groups (row[i] ? row[i] : "",
	    &pool_entry->nbgids);
	i++;
	if ((p=row[i++]))
		pool_entry->ret_policy = *p;
	if ((p=row[i]))
		pool_entry->s_type = *p;
}

dpm_decode_spcmd_entry(row, lock, rec_addr, dpm_spcmd)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_space_reserv *dpm_spcmd;
{
	int i = 0;
	char *p;
	memset (dpm_spcmd, 0, sizeof(struct dpm_space_reserv));

	if (lock) {
		memset (rec_addr, 0, sizeof(dpm_dbrec_addr));
		if ((p=row[i++]))
			strncat (*rec_addr, p, sizeof(dpm_dbrec_addr)-1);
	}
	if ((p=row[i++]))
		strncat (dpm_spcmd->s_token, p, sizeof(dpm_spcmd->s_token)-1);
	if ((p=row[i++]))
		strncat (dpm_spcmd->client_dn, p, sizeof(dpm_spcmd->client_dn)-1);
	if ((p=row[i++]))
		dpm_spcmd->s_uid = atoi (p);
	if ((p=row[i++]))
		dpm_spcmd->s_gid = atoi (p);
	if ((p=row[i++]))
		dpm_spcmd->ret_policy = *p;
	if ((p=row[i++]))
		dpm_spcmd->ac_latency = *p;
	if ((p=row[i++]))
		dpm_spcmd->s_type = *p;
	if ((p=row[i++]))
		strncat (dpm_spcmd->u_token, p, sizeof(dpm_spcmd->u_token)-1);
	if ((p=row[i++]))
		dpm_spcmd->t_space = strtou64 (p);
	if ((p=row[i++]))
		dpm_spcmd->g_space = strtou64 (p);
	if ((p=row[i++]))
		dpm_spcmd->u_space = strtoi64 (p);
	if ((p=row[i++]))
		strncat (dpm_spcmd->poolname, p, sizeof(dpm_spcmd->poolname)-1);
	if ((p=row[i++]))
		dpm_spcmd->assign_time = atoi (p);
	if ((p=row[i++]))
		dpm_spcmd->expire_time = atoi (p);
	if ((p=row[i]))
		strncat (dpm_spcmd->groups, p, sizeof(dpm_spcmd->groups)-1);
}

dpm_decode_xferreq_entry(row, lock, rec_addr, dpm_req)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_req *dpm_req;
{
	int i = 0;
	char *p;
	memset (dpm_req, 0, sizeof(struct dpm_req));

	if (lock) {
		memset (rec_addr, 0, sizeof(dpm_dbrec_addr));
		if ((p=row[i++]))
			strncat (*rec_addr, p, sizeof(dpm_dbrec_addr)-1);
	}
	if ((p=row[i++]))
		dpm_req->r_ordinal = atoi (p);
	if ((p=row[i++]))
		strncat (dpm_req->r_token, p, sizeof(dpm_req->r_token)-1);
	if ((p=row[i++]))
		dpm_req->r_uid = atoi (p);
	if ((p=row[i++]))
		dpm_req->r_gid = atoi (p);
	if ((p=row[i++]))
		strncat (dpm_req->client_dn, p, sizeof(dpm_req->client_dn)-1);
	if ((p=row[i++]))
		strncat (dpm_req->clienthost, p, sizeof(dpm_req->clienthost)-1);
	if ((p=row[i++]))
		dpm_req->r_type = *p;
	if ((p=row[i++]))
		strncat (dpm_req->u_token, p, sizeof(dpm_req->u_token)-1);
	if ((p=row[i++]))
		dpm_req->flags = atoi (p);
	if ((p=row[i++]))
		dpm_req->retrytime = atoi (p);
	if ((p=row[i++]))
		dpm_req->nbreqfiles = atoi (p);
	if ((p=row[i++]))
		dpm_req->ctime = atoi (p);
	if ((p=row[i++]))
		dpm_req->stime = atoi (p);
	if ((p=row[i++]))
		dpm_req->etime = atoi (p);
	if ((p=row[i++]))
		dpm_req->status = atoi (p);
	if ((p=row[i++]))
		strncat (dpm_req->errstring, p, sizeof(dpm_req->errstring)-1);
	if ((p=row[i]))
		strncat (dpm_req->groups, p, sizeof(dpm_req->groups)-1);
}

dpm_delete_cprs_by_token(dbfd, r_token)
struct dpm_dbfd *dbfd;
char *r_token;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_copy_filereq WHERE r_token = '%s'";
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[25];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_cprs_by_token");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, escaped_token);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_fs_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_fs WHERE ROWID = %s";
	char func[20];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_fs_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, *rec_addr);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_gfrs_by_token(dbfd, r_token)
struct dpm_dbfd *dbfd;
char *r_token;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_get_filereq WHERE r_token = '%s'";
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[25];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_gfrs_by_token");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, escaped_token);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_pending_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_pending_req WHERE ROWID = %s";
	char func[25];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_pending_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, *rec_addr);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_pfrs_by_token(dbfd, r_token)
struct dpm_dbfd *dbfd;
char *r_token;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_put_filereq WHERE r_token = '%s'";
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[25];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_pfrs_by_token");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, escaped_token);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_pool_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_pool WHERE ROWID = %s";
	char func[22];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_pool_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, *rec_addr);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_req_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_req WHERE ROWID = %s";
	char func[21];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_req_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, *rec_addr);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_spcmd_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_space_reserv WHERE ROWID = %s";
	char func[23];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_spcmd_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, *rec_addr);
	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_end_tr(dbfd)
struct dpm_dbfd *dbfd;
{
	(void) dm_mysql_query (&dbfd->mysql, "COMMIT");
	if (!dbfd->tr_started) {
		dpmlogit ("dpm_end_tr", "Ended transaction when none had been started\n");
	}
	dbfd->tr_started = 0;
	return (0);
}

dpm_exec_query(func, dbfd, sql_stmt, res)
char *func;
struct dpm_dbfd *dbfd;
char *sql_stmt;
MYSQL_RES **res;
{
	int i;
	for(i=0;i<=MAXRETRY;i++) {
		if (i>0) { dpmlogit (func,
			"Restarting query after lock wait timeout\n"); }
		if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
			*res = NULL;
			dpm_mysql_error (func, "mysql_query", dbfd);
			return (-1);
		}
		if ((*res = mysql_store_result (&dbfd->mysql)) == NULL) {
			if (mysql_errno (&dbfd->mysql) == ER_LOCK_WAIT_TIMEOUT) {
				continue;
			}
			dpm_mysql_error (func, "mysql_store_res", dbfd);
			return (-1);
		}
		break;
	}
	return (*res == NULL) ? -1 : 0;
}

dpm_get_cpr_by_fullid(dbfd, r_token, f_ordinal, cpr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
int f_ordinal;
struct dpm_copy_filereq *cpr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_cpr_by_fullid");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token, f_ordinal);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_cpr_entry (row, lock, rec_addr, cpr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_cpr_by_surl(dbfd, r_token, surl, cpr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *surl;
struct dpm_copy_filereq *cpr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[20];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND (from_surl = '%s' OR to_surl = '%s')";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND (from_surl = '%s' OR to_surl = '%s') \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[8192];

	strcpy (func, "dpm_get_cpr_by_surl");
	dm_escape_string (&dbfd->mysql, escaped_name, surl,
	    strlen (surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token, escaped_name, escaped_name);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_cpr_entry (row, lock, rec_addr, cpr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_cpr_by_surls(dbfd, r_token, from_surl, to_surl, cpr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *from_surl;
char *to_surl;
struct dpm_copy_filereq *cpr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[21];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND from_surl = '%s' AND to_surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND from_surl = '%s' AND to_surl = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[8192];

	strcpy (func, "dpm_get_cpr_by_surls");
	dm_escape_string (&dbfd->mysql, escaped_name, from_surl,
	    strlen (from_surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_name2, to_surl,
	    strlen (to_surl), sizeof(escaped_name2));
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token, escaped_name, escaped_name2);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_cpr_entry (row, lock, rec_addr, cpr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_fs_entry(dbfd, server, fs, fs_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *server;
char *fs;
struct dpm_fs *fs_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_fs[159];
	char escaped_server[CA_MAXHOSTNAMELEN*2+1];
	char func[17];
	static char query[] =
		"SELECT \
		 POOLNAME, SERVER, FS, STATUS, WEIGHT \
		FROM dpm_fs \
		WHERE server = '%s' AND fs = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 POOLNAME, SERVER, FS, STATUS, WEIGHT \
		FROM dpm_fs \
		WHERE server = '%s' AND fs = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_fs_entry");
	dm_escape_string (&dbfd->mysql, escaped_fs, fs,
	    strlen (fs), sizeof(escaped_fs));
	dm_escape_string (&dbfd->mysql, escaped_server, server,
	    strlen (server), sizeof(escaped_server));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_server, escaped_fs);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_fs_entry (row, lock, rec_addr, fs_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_gfr_by_fullid(dbfd, r_token, f_ordinal, gfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
int f_ordinal;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_gfr_by_fullid");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token, f_ordinal);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_gfr_entry (row, lock, rec_addr, gfr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_gfr_by_pfn(dbfd, bol, pfn, gfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol; 
char *pfn;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE pfn = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE pfn = '%s' \
		FOR UPDATE";
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_get_gfr_by_pfn");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_escape_string (&dbfd->mysql, escaped_name, pfn,
		    strlen (pfn), sizeof(escaped_name));
		dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
		    escaped_name);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_gfr_entry (row, lock, rec_addr, gfr_entry);
	return (0);
}

dpm_get_gfr_by_surl(dbfd, r_token, from_surl, gfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *from_surl;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[20];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND from_surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND from_surl = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_get_gfr_by_surl");
	dm_escape_string (&dbfd->mysql, escaped_name, from_surl,
	    strlen (from_surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token, escaped_name);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_gfr_entry (row, lock, rec_addr, gfr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_gfr_by_surl_uid(dbfd, bol, from_surl, uid, gfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol; 
char *from_surl;
uid_t uid;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[24];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE from_surl = '%s' AND R_UID = %d AND STATUS < %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE from_surl = '%s' AND R_UID = %d AND STATUS < %d \
		FOR UPDATE";
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_get_gfr_by_surl_uid");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_escape_string (&dbfd->mysql, escaped_name, from_surl,
		    strlen (from_surl), sizeof(escaped_name));
		dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
		    escaped_name, uid, DPM_FAILED);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_gfr_entry (row, lock, rec_addr, gfr_entry);
	return (0);
}

dpm_get_max_get_lifetime(dbfd, pfn, lifetime)
struct dpm_dbfd *dbfd;
char *pfn;
time_t *lifetime;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[25];
	static char query[] =
		"SELECT MAX(lifetime) \
		FROM dpm_get_filereq \
		WHERE pfn = '%s'";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_get_max_get_lifetime");
	dm_escape_string (&dbfd->mysql, escaped_name, pfn,
	    strlen (pfn), sizeof(escaped_name));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), query, escaped_name);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	if (row[0] == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	*lifetime = atoi (row[0]);
	mysql_free_result (res);
	return (0);
}

dpm_get_next_req(dbfd, status, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
int status;
struct dpm_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[17];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE status = %d \
		ORDER BY ctime, r_ordinal \
		LIMIT 1 \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_next_req");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), query4upd, status);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_xferreq_entry (row, lock, rec_addr, dpm_req);
	mysql_free_result (res);
	return (0);
}

dpm_get_pending_req_by_token(dbfd, r_token, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
struct dpm_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[29];
	static char query[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE r_token = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE r_token = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pending_req_by_token");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_xferreq_entry (row, lock, rec_addr, dpm_req);
	mysql_free_result (res);
	return (0);
}

dpm_get_pending_reqs_by_u_desc(dbfd, bol, u_token, uid, dpm_req, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *u_token;
uid_t uid;
struct dpm_req *dpm_req; 
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_utoken[255*2+1];
	char func[31];
	static char queryo[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE r_uid = %d";
	static char queryto[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE u_token = '%s' AND r_uid = %d";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pending_reqs_by_u_desc");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		if (*u_token) {
			dm_escape_string (&dbfd->mysql, escaped_utoken,
			    u_token, strlen (u_token), sizeof(escaped_utoken));
			dm_snprintf (sql_stmt, sizeof(sql_stmt), queryto,
			    escaped_utoken, uid);
		} else
			dm_snprintf (sql_stmt, sizeof(sql_stmt), queryo, uid);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_xferreq_entry (row, 0, NULL, dpm_req);
	return (0);
}

dpm_get_pfr_by_fullid(dbfd, r_token, f_ordinal, pfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
int f_ordinal;
struct dpm_put_filereq *pfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pfr_by_fullid");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token, f_ordinal);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pfr_entry (row, lock, rec_addr, pfr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_pfr_by_pfn(dbfd, pfn, pfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *pfn;
struct dpm_put_filereq *pfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE pfn = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE pfn = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_get_pfr_by_pfn");
	dm_escape_string (&dbfd->mysql, escaped_name, pfn,
	    strlen (pfn), sizeof(escaped_name));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_name);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pfr_entry (row, lock, rec_addr, pfr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_pfr_by_surl(dbfd, r_token, to_surl, pfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *to_surl;
struct dpm_put_filereq *pfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[20];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND to_surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND to_surl = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_get_pfr_by_surl");
	dm_escape_string (&dbfd->mysql, escaped_name, to_surl,
	    strlen (to_surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token, escaped_name);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pfr_entry (row, lock, rec_addr, pfr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_pool_entry(dbfd, poolname, pool_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *poolname;
struct dpm_pool *pool_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_poolname[CA_MAXPOOLNAMELEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE \
		FROM dpm_pool \
		WHERE poolname = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE \
		FROM dpm_pool \
		WHERE poolname = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pool_entry");
	dm_escape_string (&dbfd->mysql, escaped_poolname, poolname,
	    strlen (poolname), sizeof(escaped_poolname));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_poolname);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pool_entry (row, lock, rec_addr, pool_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_req_by_token(dbfd, r_token, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
struct dpm_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[21];
	static char query[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE r_token = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE r_token = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_req_by_token");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_xferreq_entry (row, lock, rec_addr, dpm_req);
	mysql_free_result (res);
	return (0);
}

dpm_get_reqs4del(dbfd, bol, time_lower, time_upper, max_recs, dpm_req, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
time_t time_lower;
time_t time_upper;
int max_recs;
struct dpm_req *dpm_req;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[17];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE ctime = %d \
		AND (status = %d OR status >= %d)";
	static char query4updlim1[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE ctime <= %d \
		AND (status = %d OR status >= %d) \
		LIMIT %d";
	static char query4updlim2[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE ctime BETWEEN %d AND %d \
		AND (status = %d OR status >= %d) \
		LIMIT %d";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_reqs4del");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		*dblistptr = NULL;
		if (max_recs) {
			if (time_lower != 0) {
				dm_snprintf (sql_stmt, sizeof(sql_stmt),
				    query4updlim2, time_lower, time_upper,
				    DPM_SUCCESS, DPM_DONE, max_recs);
			} else {
				dm_snprintf (sql_stmt, sizeof(sql_stmt),
				    query4updlim1, time_upper, DPM_SUCCESS,
				    DPM_DONE, max_recs);
			}
		} else {
			if (time_lower != time_upper) {
				serrno = EINVAL;
				return (-1);
			}
			dm_snprintf (sql_stmt, sizeof(sql_stmt), query4upd,
			    time_lower, DPM_SUCCESS, DPM_DONE);
		}
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL) {
		return (1);
	}
	dpm_decode_xferreq_entry (row, 1, rec_addr, dpm_req);
	return (0);
}

dpm_get_reqs_by_u_desc(dbfd, bol, u_token, uid, dpm_req, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *u_token;
uid_t uid;
struct dpm_req *dpm_req; 
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_utoken[255*2+1];
	char func[23];
	static char queryo[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE r_uid = %d";
	static char queryto[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE u_token = '%s' AND r_uid = %d";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_reqs_by_u_desc");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		if (*u_token) {
			dm_escape_string (&dbfd->mysql, escaped_utoken,
			    u_token, strlen (u_token), sizeof(escaped_utoken));
			dm_snprintf (sql_stmt, sizeof(sql_stmt), queryto,
			    escaped_utoken, uid);
		} else
			dm_snprintf (sql_stmt, sizeof(sql_stmt), queryo, uid);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_xferreq_entry (row, 0, NULL, dpm_req);
	return (0);
}

dpm_get_spcmd_by_token(dbfd, s_token, dpm_spcmd, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *s_token;
struct dpm_space_reserv *dpm_spcmd;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[23];
	static char query[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE s_token = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE s_token = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_spcmd_by_token");
	dm_escape_string (&dbfd->mysql, escaped_token, s_token,
	    strlen (s_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
	    escaped_token);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_spcmd_entry (row, lock, rec_addr, dpm_spcmd);
	mysql_free_result (res);
	return (0);
}

dpm_get_spcmd_by_u_desc(dbfd, bol, u_token, uid, dpm_spcmd, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *u_token;
uid_t uid;
struct dpm_space_reserv *dpm_spcmd; 
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_utoken[255*2+1];
	char func[24];
	static char querya[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv";
	static char queryo[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE (s_uid = 0 OR s_uid = %d)";
	static char queryto[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE u_token = '%s' AND (s_uid = 0 OR s_uid = %d)";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_spcmd_by_u_desc");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		if (*u_token) {
			dm_escape_string (&dbfd->mysql, escaped_utoken,
			    u_token, strlen (u_token), sizeof(escaped_utoken));
			dm_snprintf (sql_stmt, sizeof(sql_stmt), queryto,
			    escaped_utoken, uid);
		} else if (uid)
			dm_snprintf (sql_stmt, sizeof(sql_stmt), queryo, uid);
		else
			dm_snprintf (sql_stmt, sizeof(sql_stmt), "%s", querya);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_spcmd_entry (row, 0, NULL, dpm_spcmd);
	return (0);
}

dpm_get_spcmd_by_pathpfx(dbfd, path, dpm_spcmd, lock, dblistptr)
struct dpm_dbfd *dbfd;
char *path;
struct dpm_space_reserv *dpm_spcmd;
int lock;
DBLISTPTR *dblistptr;
{
  char func[32];
  static char query[] =
  "SELECT \
  S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
  AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
  U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
  FROM dpm_space_reserv \
  WHERE locate( concat(path, '/'), '%s/' )=1 order by length(path) desc limit 1";

    
    MYSQL_RES *res;
    MYSQL_ROW row;
    char sql_stmt[1024];
    strcpy (func, "dpm_get_spcmd_by_pathpfx");

    
    dm_snprintf (sql_stmt, sizeof(sql_stmt), query, path);
    if (dpm_exec_query (func, dbfd, sql_stmt, &res))
      return (-1);
    if ((row = mysql_fetch_row (res)) == NULL) {
      mysql_free_result (res);
      serrno = ENOENT;
      return (-1);
    }
    dpm_decode_spcmd_entry (row, 0, NULL, dpm_spcmd);
    mysql_free_result (res);
    return (0);
}

dpm_insert_cpr_entry(dbfd, cpr_entry)
struct dpm_dbfd *dbfd;
struct dpm_copy_filereq *cpr_entry;
{
	char escaped_aclatency[3];
	char escaped_ftype[3];
	char escaped_msg[255*2+1];
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char escaped_retpolicy[3];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_token2[CA_MAXDPMTOKENLEN*2+1];
	char filesize_str[21];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_copy_filereq \
		(R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING) \
		VALUES \
		('%s', %d, '%s', '%s', %d, '%s', '%s', '%s', '%s', %d, %s, %d, '%s')";
	char sql_stmt[8192];

	strcpy (func, "dpm_insert_cpr_entry");
	dm_escape_string (&dbfd->mysql, escaped_aclatency,
	    &cpr_entry->ac_latency, 1, sizeof(escaped_aclatency));
	dm_escape_string (&dbfd->mysql, escaped_msg, cpr_entry->errstring,
	    strlen (cpr_entry->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_ftype,
	    &cpr_entry->f_type, 1, sizeof(escaped_ftype));
	dm_escape_string (&dbfd->mysql, escaped_name, cpr_entry->from_surl,
	    strlen (cpr_entry->from_surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_name2, cpr_entry->to_surl,
	    strlen (cpr_entry->to_surl), sizeof(escaped_name2));
	dm_escape_string (&dbfd->mysql, escaped_retpolicy,
	    &cpr_entry->ret_policy, 1, sizeof(escaped_retpolicy));
	dm_escape_string (&dbfd->mysql, escaped_token, cpr_entry->r_token,
	    strlen (cpr_entry->r_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_token2, cpr_entry->s_token,
	    strlen (cpr_entry->s_token), sizeof(escaped_token2));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    escaped_token, cpr_entry->f_ordinal, escaped_name,
	    escaped_name2, cpr_entry->f_lifetime, escaped_ftype,
	    escaped_token2, escaped_retpolicy, escaped_aclatency,
	    cpr_entry->flags,
	    u64tostr (cpr_entry->actual_size, filesize_str, -1),
	    cpr_entry->status, escaped_msg);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_fs_entry(dbfd, fs_entry)
struct dpm_dbfd *dbfd;
struct dpm_fs *fs_entry;
{
	char escaped_fs[159];
	char escaped_poolname[CA_MAXPOOLNAMELEN*2+1];
	char escaped_server[CA_MAXHOSTNAMELEN*2+1];
	char func[20];
	static char insert_stmt[] =
		"INSERT INTO dpm_fs \
		(POOLNAME, SERVER, FS, STATUS, WEIGHT) \
		VALUES \
		('%s', '%s', '%s', %d, %d)";
	char sql_stmt[1024];

	strcpy (func, "dpm_insert_fs_entry");
	dm_escape_string (&dbfd->mysql, escaped_fs, fs_entry->fs,
	    strlen (fs_entry->fs), sizeof(escaped_fs));
	dm_escape_string (&dbfd->mysql, escaped_poolname, fs_entry->poolname,
	    strlen (fs_entry->poolname), sizeof(escaped_poolname));
	dm_escape_string (&dbfd->mysql, escaped_server, fs_entry->server,
	    strlen (fs_entry->server), sizeof(escaped_server));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    escaped_poolname, escaped_server, escaped_fs,
	    fs_entry->status, fs_entry->weight);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_gfr_entry(dbfd, gfr_entry)
struct dpm_dbfd *dbfd;
struct dpm_get_filereq *gfr_entry;
{
	char escaped_ftype[3];
	char escaped_msg[255*2+1];
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char escaped_protocol[CA_MAXPROTOLEN*2+1];
	char escaped_retpolicy[3];
	char escaped_server[CA_MAXHOSTNAMELEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_token2[CA_MAXDPMTOKENLEN*2+1];
	char filesize_str[21];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_get_filereq \
		(R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING) \
		VALUES \
		('%s', %d, %d, '%s', '%s', %d, '%s', '%s', '%s', %d, '%s', '%s', %s, %d, '%s')";
	char sql_stmt[8192];

	strcpy (func, "dpm_insert_gfr_entry");
	dm_escape_string (&dbfd->mysql, escaped_msg, gfr_entry->errstring,
	    strlen (gfr_entry->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_ftype,
	    &gfr_entry->f_type, 1, sizeof(escaped_ftype));
	dm_escape_string (&dbfd->mysql, escaped_name, gfr_entry->from_surl,
	    strlen (gfr_entry->from_surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_name2, gfr_entry->pfn,
	    strlen (gfr_entry->pfn), sizeof(escaped_name2));
	dm_escape_string (&dbfd->mysql, escaped_protocol, gfr_entry->protocol,
	    strlen (gfr_entry->protocol), sizeof(escaped_protocol));
	dm_escape_string (&dbfd->mysql, escaped_retpolicy,
	    &gfr_entry->ret_policy, 1, sizeof(escaped_retpolicy));
	dm_escape_string (&dbfd->mysql, escaped_token, gfr_entry->r_token,
	    strlen (gfr_entry->r_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_token2, gfr_entry->s_token,
	    strlen (gfr_entry->s_token), sizeof(escaped_token2));
	dm_escape_string (&dbfd->mysql, escaped_server, gfr_entry->server,
	    strlen (gfr_entry->server), sizeof(escaped_server));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    escaped_token, gfr_entry->f_ordinal, gfr_entry->r_uid,
	    escaped_name, escaped_protocol, gfr_entry->lifetime,
	    escaped_ftype, escaped_token2, escaped_retpolicy,
	    gfr_entry->flags, escaped_server, escaped_name2,
	    u64tostr (gfr_entry->actual_size, filesize_str, -1),
	    gfr_entry->status, escaped_msg);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_pending_entry(dbfd, dpm_req)
struct dpm_dbfd *dbfd;
struct dpm_req *dpm_req;
{
	char escaped_groups[255*2+1];
	char escaped_host[CA_MAXHOSTNAMELEN*2+1];
	char escaped_msg[255*2+1];
	char escaped_name[255*2+1];
	char escaped_rtype[3];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_utoken[255*2+1];
	char func[25];
	static char insert_stmt[] =
		"INSERT INTO dpm_pending_req \
		(R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS) \
		VALUES \
		(%d, '%s', %d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %d, %d, %d, %d, '%s', '%s')";
	char sql_stmt[4096];

	strcpy (func, "dpm_insert_pending_entry");
	dm_escape_string (&dbfd->mysql, escaped_msg, dpm_req->errstring,
	    strlen (dpm_req->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_groups,
	    dpm_req->groups, strlen (dpm_req->groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_host, dpm_req->clienthost,
	    strlen (dpm_req->clienthost), sizeof(escaped_host));
	dm_escape_string (&dbfd->mysql, escaped_name, dpm_req->client_dn,
	    strlen (dpm_req->client_dn), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_rtype,
	    &dpm_req->r_type, 1, sizeof(escaped_rtype));
	dm_escape_string (&dbfd->mysql, escaped_token, dpm_req->r_token,
	    strlen (dpm_req->r_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_utoken, dpm_req->u_token,
	    strlen (dpm_req->u_token), sizeof(escaped_utoken));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    dpm_req->r_ordinal, escaped_token, dpm_req->r_uid,
	    dpm_req->r_gid, escaped_name, escaped_host,
	    escaped_rtype, escaped_utoken,
	    dpm_req->flags, dpm_req->retrytime, dpm_req->nbreqfiles,
	    dpm_req->ctime, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, escaped_msg, escaped_groups);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_pfr_entry(dbfd, pfr_entry)
struct dpm_dbfd *dbfd;
struct dpm_put_filereq *pfr_entry;
{
	char escaped_aclatency[3];
	char escaped_ftype[3];
	char escaped_msg[255*2+1];
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char escaped_protocol[CA_MAXPROTOLEN*2+1];
	char escaped_retpolicy[3];
	char escaped_server[CA_MAXHOSTNAMELEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_token2[CA_MAXDPMTOKENLEN*2+1];
	char filesize_str[21];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_put_filereq \
		(R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING) \
		VALUES \
		('%s', %d, '%s', '%s', %d, %d, '%s', '%s', '%s', '%s', %s, '%s', '%s', %s, %d, '%s')";
	char reqsize_str[21];
	char sql_stmt[8192];

	strcpy (func, "dpm_insert_pfr_entry");
	dm_escape_string (&dbfd->mysql, escaped_aclatency,
	    &pfr_entry->ac_latency, 1, sizeof(escaped_aclatency));
	dm_escape_string (&dbfd->mysql, escaped_msg, pfr_entry->errstring,
	    strlen (pfr_entry->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_ftype,
	    &pfr_entry->f_type, 1, sizeof(escaped_ftype));
	dm_escape_string (&dbfd->mysql, escaped_name, pfr_entry->to_surl,
	    strlen (pfr_entry->to_surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_name2, pfr_entry->pfn,
	    strlen (pfr_entry->pfn), sizeof(escaped_name2));
	dm_escape_string (&dbfd->mysql, escaped_retpolicy,
	    &pfr_entry->ret_policy, 1, sizeof(escaped_retpolicy));
	dm_escape_string (&dbfd->mysql, escaped_protocol, pfr_entry->protocol,
	    strlen (pfr_entry->protocol), sizeof(escaped_protocol));
	dm_escape_string (&dbfd->mysql, escaped_server, pfr_entry->server,
	    strlen (pfr_entry->server), sizeof(escaped_server));
	dm_escape_string (&dbfd->mysql, escaped_token, pfr_entry->r_token,
	    strlen (pfr_entry->r_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_token2, pfr_entry->s_token,
	    strlen (pfr_entry->s_token), sizeof(escaped_token2));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    escaped_token, pfr_entry->f_ordinal, escaped_name,
	    escaped_protocol, pfr_entry->lifetime, pfr_entry->f_lifetime,
	    escaped_ftype, escaped_token2, escaped_retpolicy,
	    escaped_aclatency,
	    u64tostr (pfr_entry->requested_size, reqsize_str, -1),
	    escaped_server, escaped_name2,
	    u64tostr (pfr_entry->actual_size, filesize_str, -1),
	    pfr_entry->status, escaped_msg);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_pool_entry(dbfd, pool_entry)
struct dpm_dbfd *dbfd;
struct dpm_pool *pool_entry;
{
	char escaped_fss_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_gc_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_groups[255*2+1];
	char escaped_mig_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_poolname[CA_MAXPOOLNAMELEN*2+1];
	char escaped_retpolicy[3];
	char escaped_rs_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_stype[3];
	char defsize_str[21];
	char func[22];
	char groups[256];
	static char insert_stmt[] =
		"INSERT INTO dpm_pool \
		(POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE) \
		VALUES \
		('%s', %s, %d, %d, %d, %d, %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
	char sql_stmt[2048];

	strcpy (func, "dpm_insert_pool_entry");
	Cencode_groups (pool_entry->nbgids, pool_entry->gids, groups,
	    sizeof(groups));
	dm_escape_string (&dbfd->mysql, escaped_groups, 
	    groups, strlen (groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_fss_policy,
	    pool_entry->fss_policy, strlen (pool_entry->fss_policy),
	    sizeof(escaped_fss_policy));
	dm_escape_string (&dbfd->mysql, escaped_gc_policy,
	    pool_entry->gc_policy, strlen (pool_entry->gc_policy),
	    sizeof(escaped_gc_policy));
	dm_escape_string (&dbfd->mysql, escaped_mig_policy,
	    pool_entry->mig_policy, strlen (pool_entry->mig_policy),
	    sizeof(escaped_mig_policy));
	dm_escape_string (&dbfd->mysql, escaped_rs_policy,
	    pool_entry->rs_policy, strlen (pool_entry->rs_policy),
	    sizeof(escaped_rs_policy));
	dm_escape_string (&dbfd->mysql, escaped_poolname, pool_entry->poolname,
	    strlen (pool_entry->poolname), sizeof(escaped_poolname));
	dm_escape_string (&dbfd->mysql, escaped_retpolicy,
	    &pool_entry->ret_policy, 1, sizeof(escaped_retpolicy));
	dm_escape_string (&dbfd->mysql, escaped_stype,
	    &pool_entry->s_type, 1, sizeof(escaped_stype));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    escaped_poolname, u64tostr (pool_entry->defsize, defsize_str, -1),
	    pool_entry->gc_start_thresh, pool_entry->gc_stop_thresh,
	    pool_entry->def_lifetime, pool_entry->defpintime,
	    pool_entry->max_lifetime, pool_entry->maxpintime,
	    escaped_fss_policy, escaped_gc_policy,
	    escaped_mig_policy, escaped_rs_policy,
	    escaped_groups,
	    escaped_retpolicy, escaped_stype);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_spcmd_entry(dbfd, dpm_spcmd)
struct dpm_dbfd *dbfd;
struct dpm_space_reserv *dpm_spcmd;
{
	char escaped_aclatency[3];
	char escaped_groups[255*2+1];
	char escaped_name[255*2+1];
	char escaped_poolname[CA_MAXPOOLNAMELEN*2+1];
	char escaped_retpolicy[3];
	char escaped_stype[3];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_utoken[255*2+1];
	char func[23];
	char g_space_str[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_space_reserv \
		(S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS) \
		VALUES \
		('%s', '%s', %d, %d, '%s', '%s', '%s', '%s', %s, %s, %s, '%s', %d, %d, '%s')";
	char sql_stmt[2048];
	char t_space_str[21];
	char u_space_str[21];

	strcpy (func, "dpm_insert_spcmd_entry");
	dm_escape_string (&dbfd->mysql, escaped_retpolicy,
	    &dpm_spcmd->ret_policy, 1, sizeof(escaped_retpolicy));
	dm_escape_string (&dbfd->mysql, escaped_aclatency,
	    &dpm_spcmd->ac_latency, 1, sizeof(escaped_aclatency));
	dm_escape_string (&dbfd->mysql, escaped_stype,
	    &dpm_spcmd->s_type, 1, sizeof(escaped_stype));
	dm_escape_string (&dbfd->mysql, escaped_groups, dpm_spcmd->groups,
	    strlen (dpm_spcmd->groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_name, dpm_spcmd->client_dn,
	    strlen (dpm_spcmd->client_dn), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_poolname, dpm_spcmd->poolname,
	    strlen (dpm_spcmd->poolname), sizeof(escaped_poolname));
	dm_escape_string (&dbfd->mysql, escaped_token, dpm_spcmd->s_token,
	    strlen (dpm_spcmd->s_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_utoken, dpm_spcmd->u_token,
	    strlen (dpm_spcmd->u_token), sizeof(escaped_utoken));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    escaped_token, escaped_name,
	    dpm_spcmd->s_uid, dpm_spcmd->s_gid, escaped_retpolicy,
	    escaped_aclatency, escaped_stype, escaped_utoken,
	    u64tostr (dpm_spcmd->t_space, t_space_str, -1),
	    u64tostr (dpm_spcmd->g_space, g_space_str, -1),
	    i64tostr (dpm_spcmd->u_space, u_space_str, -1),
	    escaped_poolname, dpm_spcmd->assign_time, dpm_spcmd->expire_time,
	    escaped_groups);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_xferreq_entry(dbfd, dpm_req)
struct dpm_dbfd *dbfd;
struct dpm_req *dpm_req;
{
	char escaped_groups[255*2+1];
	char escaped_host[CA_MAXHOSTNAMELEN*2+1];
	char escaped_msg[255*2+1];
	char escaped_name[255*2+1];
	char escaped_rtype[3];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_utoken[255*2+1];
	char func[25];
	static char insert_stmt[] =
		"INSERT INTO dpm_req \
		(R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS) \
		VALUES \
		(%d, '%s', %d, %d, '%s', '%s', '%s', '%s', %d, %d, %d, %d, %d, %d, %d, '%s', '%s')";
	char sql_stmt[4096];

	strcpy (func, "dpm_insert_xferreq_entry");
	dm_escape_string (&dbfd->mysql, escaped_rtype,
	    &dpm_req->r_type, 1, sizeof(escaped_rtype));
	dm_escape_string (&dbfd->mysql, escaped_msg, dpm_req->errstring,
	    strlen (dpm_req->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_groups, dpm_req->groups,
	    strlen (dpm_req->groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_host, dpm_req->clienthost,
	    strlen (dpm_req->clienthost), sizeof(escaped_host));
	dm_escape_string (&dbfd->mysql, escaped_name, dpm_req->client_dn,
	    strlen (dpm_req->client_dn), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_token, dpm_req->r_token,
	    strlen (dpm_req->r_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_utoken, dpm_req->u_token,
	    strlen (dpm_req->u_token), sizeof(escaped_utoken));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    dpm_req->r_ordinal, escaped_token, dpm_req->r_uid,
	    dpm_req->r_gid, escaped_name, escaped_host,
	    escaped_rtype, escaped_utoken,
	    dpm_req->flags, dpm_req->retrytime, dpm_req->nbreqfiles,
	    dpm_req->ctime, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, escaped_msg, escaped_groups);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_list_cpr_entry(dbfd, bol, r_token, cpr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *r_token;
struct dpm_copy_filereq *cpr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal \
		FOR UPDATE";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_cpr_entry");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_escape_string (&dbfd->mysql, escaped_token, r_token,
		    strlen (r_token), sizeof(escaped_token));
		dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
		    escaped_token);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_cpr_entry (row, lock, rec_addr, cpr_entry);
	return (0);
}

dpm_list_expired_puts(dbfd, bol, pfr_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_put_filereq *pfr_entry; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE (status = %d OR status = %d) AND lifetime <= %d";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_expired_puts");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_snprintf (sql_stmt, sizeof(sql_stmt), query, DPM_READY,
		    DPM_RUNNING, time (0));
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_pfr_entry (row, 0, NULL, pfr_entry);
	return (0);
}

dpm_list_expired_spaces(dbfd, bol, dpm_spcmd, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_space_reserv *dpm_spcmd; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[24];
	static char query[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE expire_time <= %d";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_expired_spaces");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_snprintf (sql_stmt, sizeof(sql_stmt), query, time (0));
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_spcmd_entry (row, 0, NULL, dpm_spcmd);
	return (0);
}

dpm_list_rr_puts(dbfd, bol, pfr_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_put_filereq *pfr_entry; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[17];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE status = %d OR status = %d";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_rr_puts");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_snprintf (sql_stmt, sizeof(sql_stmt), query, DPM_READY,
		    DPM_RUNNING);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_pfr_entry (row, 0, NULL, pfr_entry);
	return (0);
}

dpm_list_fs_entry(dbfd, bol, poolname, fs_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *poolname;
struct dpm_fs *fs_entry;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_poolname[CA_MAXPOOLNAMELEN*2+1];
	char func[18];
	static char query[] =
		"SELECT \
		 POOLNAME, SERVER, FS, STATUS, WEIGHT \
		FROM dpm_fs \
		WHERE poolname = '%s' \
		ORDER BY server, fs";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_fs_entry");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_escape_string (&dbfd->mysql, escaped_poolname,
		    poolname, strlen (poolname), sizeof(escaped_poolname));
		dm_snprintf (sql_stmt, sizeof(sql_stmt), query, escaped_poolname);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_fs_entry (row, 0, NULL, fs_entry);
	return (0);
}

dpm_list_gfr_entry(dbfd, bol, r_token, gfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *r_token;
struct dpm_get_filereq *gfr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal \
		FOR UPDATE";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_gfr_entry");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_escape_string (&dbfd->mysql, escaped_token, r_token,
		    strlen (r_token), sizeof(escaped_token));
		dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
		    escaped_token);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_gfr_entry (row, lock, rec_addr, gfr_entry);
	return (0);
}

dpm_list_pending_req(dbfd, bol, dpm_req, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_req *dpm_req; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[21];
	static char query[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE,  U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		FOR UPDATE";
	MYSQL_ROW row;

	strcpy (func, "dpm_list_pending_req");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		if (dpm_exec_query (func, dbfd, lock ? query4upd : query, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_xferreq_entry (row, lock, rec_addr, dpm_req);
	return (0);
}

dpm_list_pfr_entry(dbfd, bol, r_token, pfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *r_token;
struct dpm_put_filereq *pfr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal \
		FOR UPDATE";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_pfr_entry");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_escape_string (&dbfd->mysql, escaped_token, r_token,
		    strlen (r_token), sizeof(escaped_token));
		dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
		    escaped_token);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_pfr_entry (row, lock, rec_addr, pfr_entry);
	return (0);
}

dpm_list_pool_entry(dbfd, bol, pool_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_pool *pool_entry;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[20];
	static char query[] =
		"SELECT \
		 POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE \
		FROM dpm_pool \
		ORDER BY poolname";
	MYSQL_ROW row;

	strcpy (func, "dpm_list_pool_entry");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		if (dpm_exec_query (func, dbfd, query, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_pool_entry (row, 0, NULL, pool_entry);
	return (0);
}

dpm_list_spcmd_entry(dbfd, bol, dpm_spcmd, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_space_reserv *dpm_spcmd;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[21];
	static char query[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv";
	MYSQL_ROW row;

	strcpy (func, "dpm_list_spcmd_entry");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		if (dpm_exec_query (func, dbfd, query, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_spcmd_entry (row, 0, NULL, dpm_spcmd);
	return (0);
}

dpm_opendb(dbfd)
struct dpm_dbfd *dbfd; 
{               
	char func[16];
	int ntries;
		
	strcpy (func, "dpm_opendb");
	(void) mysql_init (&dbfd->mysql);
	ntries = 0;
	while (1) {
		if (mysql_real_connect (&dbfd->mysql, db_srvr, db_user, db_pwd,
		    db_name, 0, NULL, 0)) return (0);
		if (ntries++ >= MAXRETRY) break;
		sleep (RETRYI);
	}
	dpm_mysql_error (func, "CONNECT", dbfd);
	return (-1);
}

dpm_pingdb(dbfd)
struct dpm_dbfd *dbfd;
{
	char func[16];
	static char query_stmt[] = "SELECT 1 FROM DUAL";
	MYSQL_RES *res;

	strcpy (func, "dpm_pingdb");
	if (dpm_exec_query (func, dbfd, query_stmt, &res))
		return (-1);
	mysql_free_result (res);
	return (0);
}

dpm_start_tr(s, dbfd)
int s;
struct dpm_dbfd *dbfd;
{
	(void) dm_mysql_query (&dbfd->mysql, "BEGIN");
	dbfd->tr_started = 1;
	return (0);
}

dpm_unique_id(dbfd, unique_id)
struct dpm_dbfd *dbfd;
u_signed64 *unique_id;
{
	my_ulonglong arows;
	char func[16];
	static char insert_stmt[] =
		"INSERT INTO dpm_unique_id (ID) SELECT 0 FROM DUAL WHERE NOT \
		 EXISTS (SELECT * FROM dpm_unique_id)";
	static char update_stmt[] =
		"UPDATE dpm_unique_id SET ID = LAST_INSERT_ID(ID + 1)";

	strcpy (func, "dpm_unique_id");

	if (dm_mysql_query (&dbfd->mysql, update_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}

	arows = mysql_affected_rows (&dbfd->mysql);
	if (!arows) {
		if (dm_mysql_query (&dbfd->mysql, insert_stmt)) {
			dpm_mysql_error (func, "INSERT", dbfd);
			return (-1);
		}
		if (dm_mysql_query (&dbfd->mysql, update_stmt)) {
			dpm_mysql_error (func, "UPDATE", dbfd);
			return (-1);
		}
		arows = mysql_affected_rows (&dbfd->mysql);
		if (!arows) {
			serrno = SEINTERNAL;
			return (-1);
		}
	}

	*unique_id = mysql_insert_id (&dbfd->mysql);
	return (0);
}

dpm_update_cpr_entry (dbfd, rec_addr, cpr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_copy_filereq *cpr_entry;
{
	char escaped_ftype[3];
	char escaped_msg[255*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char filesize_str[21];
	char func[21];
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_copy_filereq SET \
		F_LIFETIME = %d, F_TYPE = '%s', S_TOKEN = '%s', \
		ACTUAL_SIZE = %s, STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_cpr_entry");
	dm_escape_string (&dbfd->mysql, escaped_ftype,
	    &cpr_entry->f_type, 1, sizeof(escaped_ftype));
	dm_escape_string (&dbfd->mysql, escaped_msg, cpr_entry->errstring,
	    strlen (cpr_entry->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_token, cpr_entry->s_token,
	    strlen (cpr_entry->s_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    cpr_entry->f_lifetime, escaped_ftype, escaped_token,
	    u64tostr (cpr_entry->actual_size, filesize_str, -1),
	    cpr_entry->status, escaped_msg, *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_update_fs_entry (dbfd, rec_addr, fs_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_fs *fs_entry;
{
	char func[20];
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_fs SET \
		STATUS = %d, WEIGHT = %d \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_fs_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    fs_entry->status, fs_entry->weight, *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_update_gfr_entry (dbfd, rec_addr, gfr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_get_filereq *gfr_entry;
{
	char escaped_ftype[3];
	char escaped_msg[255*2+1];
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_server[CA_MAXHOSTNAMELEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char filesize_str[21];
	char func[21];
	char sql_stmt[4096];
	static char update_stmt[] =
		"UPDATE dpm_get_filereq SET \
		LIFETIME = %d, F_TYPE = '%s', S_TOKEN = '%s', \
		SERVER = '%s', PFN = '%s', ACTUAL_SIZE = %s, \
		STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_gfr_entry");
	dm_escape_string (&dbfd->mysql, escaped_ftype,
	    &gfr_entry->f_type, 1, sizeof(escaped_ftype));
	dm_escape_string (&dbfd->mysql, escaped_msg, gfr_entry->errstring,
	    strlen (gfr_entry->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_name, gfr_entry->pfn,
	    strlen (gfr_entry->pfn), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_server, gfr_entry->server,
	    strlen (gfr_entry->server), sizeof(escaped_server));
	dm_escape_string (&dbfd->mysql, escaped_token, gfr_entry->s_token,
	    strlen (gfr_entry->s_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    gfr_entry->lifetime, escaped_ftype, escaped_token,
	    escaped_server, escaped_name,
	    u64tostr (gfr_entry->actual_size, filesize_str, -1),
	    gfr_entry->status, escaped_msg, *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_update_pending_entry (dbfd, rec_addr, dpm_req)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_req *dpm_req;
{
	char escaped_msg[255*2+1];
	char func[25];
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_pending_req SET \
		NBREQFILES = %d, STIME = %d, ETIME = %d, \
		STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_pending_entry");
	dm_escape_string (&dbfd->mysql, escaped_msg, dpm_req->errstring,
	    strlen (dpm_req->errstring), sizeof(escaped_msg));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    dpm_req->nbreqfiles, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, escaped_msg,
	    *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_update_pfr_entry (dbfd, rec_addr, pfr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_put_filereq *pfr_entry;
{
	char escaped_ftype[3];
	char escaped_msg[255*2+1];
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_server[CA_MAXHOSTNAMELEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char filesize_str[21];
	char func[21];
	char reqsize_str[21];
	char sql_stmt[4096];
	static char update_stmt[] =
		"UPDATE dpm_put_filereq SET \
		LIFETIME = %d, F_LIFETIME = %d, F_TYPE = '%s', S_TOKEN = '%s', \
		REQUESTED_SIZE = %s, SERVER = '%s', PFN = '%s', \
		ACTUAL_SIZE = %s, STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_pfr_entry");
	dm_escape_string (&dbfd->mysql, escaped_ftype,
	    &pfr_entry->f_type, 1, sizeof(escaped_ftype));
	dm_escape_string (&dbfd->mysql, escaped_msg, pfr_entry->errstring,
	    strlen (pfr_entry->errstring), sizeof(escaped_msg));
	dm_escape_string (&dbfd->mysql, escaped_name, pfr_entry->pfn,
	    strlen (pfr_entry->pfn), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_server, pfr_entry->server,
	    strlen (pfr_entry->server), sizeof(escaped_server));
	dm_escape_string (&dbfd->mysql, escaped_token, pfr_entry->s_token,
	    strlen (pfr_entry->s_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt, pfr_entry->lifetime,
	    pfr_entry->f_lifetime, escaped_ftype, escaped_token,
	    u64tostr (pfr_entry->requested_size, reqsize_str, -1),
	    escaped_server, escaped_name,
	    u64tostr (pfr_entry->actual_size, filesize_str, -1),
	    pfr_entry->status, escaped_msg, *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_update_pool_entry (dbfd, rec_addr, pool_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_pool *pool_entry;
{
	char escaped_fss_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_gc_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_groups[255*2+1];
	char escaped_mig_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_retpolicy[3];
	char escaped_rs_policy[CA_MAXPOLICYLEN*2+1];
	char escaped_stype[3];
	char defsize_str[21];
	char func[22];
	char groups[256];
	char sql_stmt[2048];
	static char update_stmt[] =
		"UPDATE dpm_pool SET \
		DEFSIZE = %s, GC_START_THRESH = %d, GC_STOP_THRESH = %d, \
		DEF_LIFETIME = %d, DEFPINTIME = %d, MAX_LIFETIME = %d, \
		MAXPINTIME = %d, \
		FSS_POLICY = '%s', GC_POLICY = '%s', MIG_POLICY = '%s', \
		RS_POLICY = '%s', GROUPS = '%s', RET_POLICY = '%s', S_TYPE = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_pool_entry");
	dm_escape_string (&dbfd->mysql, escaped_retpolicy,
	    &pool_entry->ret_policy, 1, sizeof(escaped_retpolicy));
	dm_escape_string (&dbfd->mysql, escaped_stype,
	    &pool_entry->s_type, 1, sizeof(escaped_stype));
	dm_escape_string (&dbfd->mysql, escaped_fss_policy,
	    pool_entry->fss_policy, strlen (pool_entry->fss_policy),
	    sizeof(escaped_fss_policy));
	dm_escape_string (&dbfd->mysql, escaped_gc_policy,
	    pool_entry->gc_policy, strlen (pool_entry->gc_policy),
	    sizeof(escaped_gc_policy));
	Cencode_groups (pool_entry->nbgids, pool_entry->gids, groups, sizeof(groups));
	dm_escape_string (&dbfd->mysql, escaped_groups, groups,
	    strlen (groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_mig_policy,
	    pool_entry->mig_policy, strlen (pool_entry->mig_policy),
	    sizeof(escaped_mig_policy));
	dm_escape_string (&dbfd->mysql, escaped_rs_policy,
	    pool_entry->rs_policy, strlen (pool_entry->rs_policy),
	    sizeof(escaped_rs_policy));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    u64tostr (pool_entry->defsize, defsize_str, -1),
	    pool_entry->gc_start_thresh, pool_entry->gc_stop_thresh,
	    pool_entry->def_lifetime, pool_entry->defpintime,
	    pool_entry->max_lifetime, pool_entry->maxpintime,
	    escaped_fss_policy, escaped_gc_policy,
	    escaped_mig_policy, escaped_rs_policy,
	    escaped_groups,
	    escaped_retpolicy, escaped_stype, *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_update_spcmd_entry (dbfd, rec_addr, dpm_spcmd)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_space_reserv *dpm_spcmd;
{
	char escaped_groups[255*2+1];
	char escaped_poolname[CA_MAXPOOLNAMELEN*2+1];
	char func[23];
	char g_space_str[21];
	char sql_stmt[1024];
	char t_space_str[21];
	char u_space_str[21];
	static char update_stmt[] =
		"UPDATE dpm_space_reserv SET \
		S_GID = %d, T_SPACE = %s, G_SPACE = %s, U_SPACE = %s, \
		POOLNAME = '%s', ASSIGN_TIME = %d, EXPIRE_TIME = %d, GROUPS = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_spcmd_entry");
	dm_escape_string (&dbfd->mysql, escaped_groups, dpm_spcmd->groups,
	    strlen (dpm_spcmd->groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_poolname, dpm_spcmd->poolname,
	    strlen (dpm_spcmd->poolname), sizeof(escaped_poolname));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    dpm_spcmd->s_gid,
	    u64tostr (dpm_spcmd->t_space, t_space_str, -1),
	    u64tostr (dpm_spcmd->g_space, g_space_str, -1),
	    i64tostr (dpm_spcmd->u_space, u_space_str, -1),
	    escaped_poolname,
	    dpm_spcmd->assign_time, dpm_spcmd->expire_time,
	    escaped_groups, *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}


dpm_update_uspcincr_spcmd_entry (dbfd, rec_addr, dpm_spcmd, uspaceincr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_space_reserv *dpm_spcmd;
long long int uspaceincr;
{
	char escaped_groups[255*2+1];
	char escaped_poolname[CA_MAXPOOLNAMELEN*2+1];
	char func[23];
	char g_space_str[21];
	char sql_stmt[1024];
	char t_space_str[21];
	char u_space_str[21];
	static char update_stmt[] =
		"UPDATE dpm_space_reserv SET \
		S_GID = %d, T_SPACE = %s, G_SPACE = %s, U_SPACE = U_SPACE + (%s), \
		POOLNAME = '%s', ASSIGN_TIME = %d, EXPIRE_TIME = %d, GROUPS = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_spcmd_entry");
	
	dpm_spcmd->u_space += uspaceincr;
	
	dm_escape_string (&dbfd->mysql, escaped_groups, dpm_spcmd->groups,
	    strlen (dpm_spcmd->groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_poolname, dpm_spcmd->poolname,
	    strlen (dpm_spcmd->poolname), sizeof(escaped_poolname));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    dpm_spcmd->s_gid,
	    u64tostr (dpm_spcmd->t_space, t_space_str, -1),
	    u64tostr (dpm_spcmd->g_space, g_space_str, -1),
	    i64tostr (uspaceincr, u_space_str, -1),
	    escaped_poolname,
	    dpm_spcmd->assign_time, dpm_spcmd->expire_time,
	    escaped_groups, *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}



dpm_update_xferreq_entry (dbfd, rec_addr, dpm_req)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_req *dpm_req;
{
	char escaped_msg[255*2+1];
	char func[25];
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_req SET \
		NBREQFILES = %d, STIME = %d, ETIME = %d, \
		STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_xferreq_entry");
	dm_escape_string (&dbfd->mysql, escaped_msg, dpm_req->errstring,
	    strlen (dpm_req->errstring), sizeof(escaped_msg));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt,
	    dpm_req->nbreqfiles, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, escaped_msg,
	    *rec_addr);

	if (dm_mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}
