/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_put.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_put - reserve space for a set of files */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_put (int nbreqfiles, struct dpm_putfilereq *reqfiles, int nbprotocols, char **protocols, char *u_token, int overwrite, time_t retrytime, char *r_token, int *nbreplies, struct dpm_putfilestatus **filestatuses)
{
	int i;
	int ret;
	struct dpm_putfilereqx *r;

	if (nbreqfiles <= 0) {
		serrno = EINVAL;
		return (-1);
	}

	r = calloc (nbreqfiles, sizeof(struct dpm_putfilereqx));
	for(i = 0; i < nbreqfiles; i++) {
		r[i].to_surl = reqfiles[i].to_surl;
		r[i].lifetime = reqfiles[i].lifetime;
		r[i].f_lifetime = reqfiles[i].f_lifetime;
		r[i].f_type = reqfiles[i].f_type;
		strcpy (r[i].s_token, reqfiles[i].s_token);
		r[i].ret_policy = reqfiles[i].ret_policy;
		r[i].ac_latency = reqfiles[i].ac_latency;
		r[i].requested_size = reqfiles[i].requested_size;
	}

	ret = dpm_putx (nbreqfiles, r, nbprotocols, protocols, u_token,
	    overwrite, retrytime, r_token, nbreplies, filestatuses);

	free (r);
	return ret;
}

int DLL_DECL
dpm_putx (int nbreqfiles, struct dpm_putfilereqx *reqfiles, int nbprotocols, char **protocols, char *u_token, int overwrite, time_t retrytime, char *r_token, int *nbreplies, struct dpm_putfilestatus **filestatuses)
{
	int c;
	char errstring[256];
	char func[16];
	gid_t gid;
	int i;
	int msglen;
	int needx = 0;
	char *rbp;
	char repbuf[4+256+CA_MAXDPMTOKENLEN+1];
	char *sbp;
	char *sendbuf;
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_putx");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbreqfiles <= 0 || nbprotocols <= 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (! reqfiles || ! protocols || ! r_token || ! nbreplies || ! filestatuses) {
		serrno = EFAULT;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 6 * LONGSIZE;
	for (i = 0; i < nbprotocols; i++)
		msglen += strlen (protocols[i]) + 1;
	if (u_token)
		msglen += strlen (u_token) + 1;
	else
		msglen++;
	msglen += LONGSIZE;
	msglen += TIME_TSIZE;
	msglen += LONGSIZE;
	for (i = 0; i < nbreqfiles; i++) {
		if (reqfiles[i].reserved != 0) {
			serrno = EINVAL;
			return (-1);
		}
		msglen += strlen (reqfiles[i].to_surl) + 1;
		msglen += TIME_TSIZE;
		msglen++;
		msglen += strlen (reqfiles[i].s_token) + 1;
		msglen += HYPERSIZE;
		msglen += TIME_TSIZE;
		msglen++;
		msglen++;
		if (*reqfiles[i].server)
			needx = 1;
		if (*reqfiles[i].pfnhint)
			needx = 1;
	}
	if (needx) {
		msglen++;
		for (i = 0; i < nbreqfiles; i++) {
			msglen += strlen (reqfiles[i].server) + 1;
			msglen += strlen (reqfiles[i].pfnhint) + 1;
		}
	}

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, (needx ? DPM_MAGIC : DPM_MAGIC2));
	marshall_LONG (sbp, (needx ? DPM_PUTX : DPM_PUT));
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_LONG (sbp, nbprotocols);
	for (i = 0; i < nbprotocols; i++)
		marshall_STRING (sbp, protocols[i]);
	if (u_token) {
		marshall_STRING (sbp, u_token);
	} else {
		marshall_STRING (sbp, "");
	}
	marshall_LONG (sbp, overwrite);
	marshall_TIME_T (sbp, retrytime);

	if (needx) {
		/* putx_version number */
		marshall_BYTE (sbp, 0);
	}

	marshall_LONG (sbp, nbreqfiles);
	for (i = 0; i < nbreqfiles; i++) {
		marshall_STRING (sbp, reqfiles[i].to_surl);
		marshall_TIME_T (sbp, reqfiles[i].lifetime);
		marshall_BYTE (sbp, reqfiles[i].f_type);
		marshall_STRING (sbp, reqfiles[i].s_token);
		marshall_HYPER (sbp, reqfiles[i].requested_size);
		marshall_TIME_T (sbp, reqfiles[i].f_lifetime);
		marshall_BYTE (sbp, reqfiles[i].ret_policy);
		marshall_BYTE (sbp, reqfiles[i].ac_latency);
		if (needx) {
			marshall_STRING (sbp, reqfiles[i].server);
			marshall_STRING (sbp, reqfiles[i].pfnhint);
		}
	}
	c = send2dpm (NULL, sendbuf, msglen, repbuf, sizeof(repbuf),
	    (void **)filestatuses, nbreplies);
	free (sendbuf);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, c);
		if ((c & 0xF000) == DPM_FAILED) {
			serrno = c - DPM_FAILED;
			c = -1;
		}
		unmarshall_STRING (rbp, errstring);
		if (*errstring)
			dpm_errmsg (func, "%s\n", errstring);
		unmarshall_STRING (rbp, r_token);
	}
	return (c);
}
