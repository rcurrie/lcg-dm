.\" @(#)$RCSfile: dpm-qryconf.man,v $ $Revision$ $Date$ CERN Jean-Philippe Baud
.\" Copyright (C) 2004-2011 by CERN
.\" All rights reserved
.\"
.TH DPM-QRYCONF 1 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpm-qryconf \- display the Disk Pool Manager configuration
.SH SYNOPSIS
.B dpm-qryconf
[
.B --groups
] [
.B --help
] [
.B --proto
] [
.B --si
] [
.B --weight
]
.SH DESCRIPTION
.B dpm-qryconf
displays the Disk Pool Manager configuration.
For each pool, it displays two lines. The first one gives the pool name,
the default amount of space reserved for a file,
the threshold (in %) at which the garbage collector is started and stopped,
the default lifetime, the default pin time,
the maximum lifetime, the maximum pin time,
the "File System Selection" policy name, the "Garbage Collection" policy name,
the "Request Selection" policy name, the group id if the pool is restricted or
0, the space type:
.BR V " (for Volatile),"
.BR D " (for Durable),"
.BR P " (for Permanent)"
or
.BR - " (for any type),"
the "Migration Policy" name and the retention policy:
.BR R " (for Replica),"
.BR O " (for Output)"
or
.BR C " (for Custodial)."
All time values are either "Inf" (for infinite) or expressed in years, months,
days, hours or seconds.
.LP
The second one gives the capacity of the disk pool and the amount of free space.
.LP
After each pool description, the filesystems belonging to the pool are listed
giving the server name, the filesystem name, the capacity, the amount of free
space and the status.
.SH OPTIONS
.TP
.B --groups
list group names instead of gids.
.TP
.B --proto
list also the supported access protocols.
.TP
.B --si
use powers of 1000 not 1024 for sizes.
.TP
.B --weight
list weight associated with each filesystem.
.SH EXAMPLE
.nf
.ft CW
setenv DPM_HOST dpmhost
.sp
dpm-qryconf
.sp
POOL Volatile DEFSIZE 100.00M GC_START_THRESH 0 GC_STOP_THRESH 0 \\
	DEF_LIFETIME 7.0d DEFPINTIME 2.0h \\
	MAX_LIFETIME 1.0m MAXPINTIME 12.0h \\
	FSS_POLICY maxfreespace GC_POLICY lru RS_POLICY fifo GIDS 0 S_TYPE - \\
	MIG_POLICY none RET_POLICY R
                              CAPACITY 3.85G FREE 3.43G ( 89.1%)
  lxb0722.cern.ch /data CAPACITY 3.85G FREE 3.43G ( 89.1%)
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.BR dpm-addpool(1) ,
.B dpm-addfs(1)
