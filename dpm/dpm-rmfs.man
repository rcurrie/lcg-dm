.\" @(#)$RCSfile: dpm-rmfs.man,v $ $Revision: 1.3 $ $Date: 2007/01/09 10:15:04 $ CERN Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN
.\" All rights reserved
.\"
.TH DPM-RMFS 1 "$Date: 2007/01/09 10:15:04 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-rmfs \- remove a filesystem from a disk pool definition
.SH SYNOPSIS
.B dpm-rmfs
.BI --server " fs_server"
.BI --fs " fs_name"
[
.B --help
]
.SH DESCRIPTION
.B dpm-rmfs
removes a filesystem from a disk pool definition.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.I server
specifies the host name of the disk server where this filesystem is mounted.
.TP 
.I fs
specifies the mount point of the dedicated filesystem.
.SH EXAMPLE
.nf
.ft CW
	dpm-rmfs --server sehost --fs /data
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.B dpm_rmfs(3)
