/*
 * Copyright (C) 2007-2011 by CERN/IT/GD/ITR
 * All rights updated
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-updatespace.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-updatespace - update space */
#include <errno.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
int help_flag;
int si_flag;
main(argc, argv)
int argc;
char **argv;
{
	u_signed64 actual_g_space;
	time_t actual_lifetime;
	u_signed64 actual_t_space;
	char *arg_gid = NULL;
	char *arg_group = NULL;
	int c;
	char *dp;
	int errflg = 0;
	gid_t gid;
	char gid_str[256];
	char *g_space_str = NULL;
	int i;
	static struct Coptions longopts[] = {
		{"gid", REQUIRED_ARGUMENT, 0, OPT_POOL_GID},
		{"group", REQUIRED_ARGUMENT, 0, OPT_POOL_GROUP},
		{"gspace", REQUIRED_ARGUMENT, 0, OPT_GSPACE_SZ},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"lifetime", REQUIRED_ARGUMENT, 0, OPT_LIFETIME},
		{"ret_policy", REQUIRED_ARGUMENT, 0, OPT_RET_POLICY},
		{"si", NO_ARGUMENT, &si_flag, 1},
		{"space_token", REQUIRED_ARGUMENT, 0, OPT_S_TOKEN},
		{"token_desc", REQUIRED_ARGUMENT, 0, OPT_U_DESC},
		{0, 0, 0, 0}
	};
	int nbgids = -1;
	int nbreplies = 0;
	char ops = '\0';
	char *p;
	gid_t *q;
	u_signed64 req_g_space = -1;
	time_t req_lifetime = -1;
	gid_t *s_gids = NULL;
	char *s_token = NULL;
	char **s_tokens = NULL;
	struct dpm_space_metadata *spacemd = NULL;
	char *u_token = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_POOL_GID:
			arg_gid = Coptarg;
			break;
		case OPT_POOL_GROUP:
			arg_group = Coptarg;
			break;
		case OPT_GSPACE_SZ:
			p = Coptarg;
			while (*p >= '0' && *p <= '9') p++;
			if (! (*p == '\0' || ((*p == 'k' || *p == 'M' ||
			    *p == 'G' || *p == 'T' || *p == 'P') && *(p+1) == '\0'))) {
				fprintf (stderr,
				    "invalid size of guaranteed space %s\n", Coptarg);
				errflg++;
			} else
				g_space_str = Coptarg;
			break;
		case OPT_LIFETIME:
			if (strcmp (Coptarg, "Inf") == 0) {
				req_lifetime = 0x7FFFFFFF;
				break;
			}
			if ((req_lifetime = strtol (Coptarg, &dp, 10)) < 0 ||
			    (*dp != '\0' && *(dp+1) != '\0')) {
				fprintf (stderr,
				    "invalid lifetime %s\n", Coptarg);
				errflg++;
				break;
			}
			switch (*dp) {
			case 'y':
				req_lifetime *= 365 * 86400;
				break;
			case 'm':
				req_lifetime *= 30 * 86400;
				break;
			case 'd':
				req_lifetime *= 86400;
				break;
			case 'h':
				req_lifetime *= 3600;
				break;
			case '\0':
				break;
			default:
				fprintf (stderr,
				    "invalid lifetime %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_S_TOKEN:
			s_token = Coptarg;
			break;
		case OPT_U_DESC:
			u_token = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || (! s_token && ! u_token)) {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s\t%s\t%s\t%s %s\t%s\t%s",
		    argv[0], "--space_token space_token [--gspace size_guaranteed_space]\n",
		    "\t[--gid group_id(s)] [--group group_name(s)] [--help]\n",
		    "\t[--lifetime space_lifetime] [--si]\n",
		    argv[0], "--token_desc u_token [--gspace size_guaranteed_space]\n",
		    "\t[--gid group_id(s)] [--group group_name(s)] [--help]\n",
		    "\t[--lifetime space_lifetime] [--si]\n");
		exit (errflg ? USERR : 0);
	}

	if (g_space_str) {
		if (si_flag)
			req_g_space = strsitou64 (g_space_str);
		else
			req_g_space = strutou64 (g_space_str);
	}
	if (! s_token) {
		if (dpm_getspacetoken (u_token, &nbreplies, &s_tokens) < 0) {
			fprintf (stderr, "dpm-updatespace: %s\n", sstrerror(serrno));
			exit (serrno == EINVAL ? USERR : SYERR);
		}
		if (nbreplies > 1) {
			fprintf (stderr,
			    "dpm-updatespace: more than one space token associated with this description\n");
			exit (USERR);
		}
		s_token = s_tokens[0];
	}

	if ((arg_gid && (*arg_gid == '+' || *arg_gid == '-')) ||
	    (arg_group && (*arg_group == '+' || *arg_group == '-'))) {
		if (dpm_getspacemd (1, &s_token, &nbreplies, &spacemd) < 0) {
			fprintf (stderr, "dpm_getspacemd: %s\n", sstrerror(serrno));
			exit (serrno == EINVAL ? USERR : SYERR);
		}
		nbgids = spacemd->nbgids;
		s_gids = spacemd->gids;
	} else if (arg_gid || arg_group)
		nbgids = 0;

	if (arg_gid) {
		p = strtok (arg_gid, ",");
		while (p) {
			if (*p == '+' || *p == '-') {
				ops = *p;
				p++;
			}
			if ((gid = strtol (p, &dp, 10)) < 0 ||
			    *dp != '\0') {
				fprintf (stderr, "invalid gid %s\n", p);
				errflg++;
				goto next_gid;
			}
#ifdef VIRTUAL_ID
			if (gid > 0 && Cns_getgrpbygid (gid, gid_str) < 0) {
#else
			if (gid > 0 && ! getgrgid (gid)) {
#endif
				fprintf (stderr, "invalid gid: %s\n", p);
				errflg++;
				goto next_gid;
			}
			for (i = 0; i < nbgids; i++) {
				if (gid == s_gids[i]) break;
			}
			if (ops == '\0' || ops == '+') {
				if (i < nbgids) {
					fprintf (stderr,
					    "%s is already in the list of gids\n", p);
					errflg++;
					goto next_gid;
				}
				nbgids++;
				if ((q = realloc (s_gids,
				    nbgids * sizeof(gid_t))) == NULL) {
					fprintf (stderr, "Could not allocate memory for gids\n");
					exit (USERR);
				}
				s_gids = q;
				s_gids[nbgids - 1] = gid;
			} else {
				if (i >= nbgids) {
					fprintf (stderr,
					    "%s is not in the list of gids\n", p);
					errflg++;
					goto next_gid;
				}
				nbgids--;
				if (nbgids == 0) {
					free (s_gids);
					s_gids = NULL;
				} else {
					if (i < nbgids)
						memmove (&s_gids[i], &s_gids[i + 1],
						    (nbgids - i) * sizeof(gid_t));
					if ((q = realloc (s_gids,
					    nbgids * sizeof(gid_t))))
						s_gids = q;
				}
			}
next_gid:
			if (p = strtok (NULL, ",")) *(p - 1) = ',';
		}
	} else if (arg_group) {
		p = strtok (arg_group, ",");
		while (p) {
			if (*p == '+' || *p == '-') {
				ops = *p;
				p++;
			}
#ifdef VIRTUAL_ID
			if (strcmp (p, "root") == 0)
				gid = 0;
			else if (Cns_getgrpbynam (p, &gid) < 0) {
#else
			else if ((gr = getgrnam (p)))
				gid = gr->gr_gid;
			else {
#endif
				fprintf (stderr, "invalid group: %s\n", p);
				errflg++;
				goto next_group;
			}
			for (i = 0; i < nbgids; i++) {
				if (gid == s_gids[i]) break;
			}
			if (ops == '\0' || ops == '+') {
				if (i < nbgids) {
					fprintf (stderr,
					    "%s is already in the list of gids\n", p);
					errflg++;
					goto next_group;
				}
				nbgids++;
				if ((q = realloc (s_gids,
				    nbgids * sizeof(gid_t))) == NULL) {
					fprintf (stderr, "Could not allocate memory for gids\n");
					exit (USERR);
				}
				s_gids = q;
				s_gids[nbgids - 1] = gid;
			} else {
				if (i >= nbgids) {
					fprintf (stderr,
					    "%s is not in the list of gids\n", p);
					errflg++;
					goto next_group;
				}
				nbgids--;
				if (nbgids == 0) {
					free (s_gids);
					s_gids = NULL;
				} else {
					if (i < nbgids)
						memmove (&s_gids[i], &s_gids[i + 1],
						    (nbgids - i) * sizeof(gid_t));
					if ((q = realloc (s_gids,
					    nbgids * sizeof(gid_t))))
						s_gids = q;
				}
			}
next_group:
			if (p = strtok (NULL, ",")) *(p - 1) = ',';
		}
	}
	if (errflg)
		exit (USERR);

	if (dpm_updatespace (s_token, req_g_space, req_g_space, req_lifetime,
	    nbgids, s_gids, &actual_t_space, &actual_g_space, &actual_lifetime) < 0) {
		fprintf (stderr, "dpm-updatespace: %s\n", sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
