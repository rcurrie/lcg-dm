/*
 * Copyright (C) 2005-2008 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_copyfile.c,v $ $Revision$ $Date$ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "globus_gass_copy.h"
#include "Castor_limits.h"
#include "Cnetdb.h"
#include "serrno.h"

typedef struct {
	globus_mutex_t mutex;
	globus_cond_t cond;
	volatile globus_bool_t done;
	volatile globus_bool_t errflag;
	globus_object_t *error;
} monitor_t;

typedef struct {
	monitor_t *mt;
	volatile globus_bool_t done;
}  check_t;

static int ggc_interrupted;
static int ggc_interrupt_processed;
static int istimeout;

static volatile int globus_activated = 0;

static int
do_globus_activates(void)
{
	/* not thread safe */
	if (!globus_activated) {
		globus_activated = 1;
		(void) globus_module_activate (GLOBUS_GASS_COPY_MODULE);
		(void) globus_module_activate (GLOBUS_FTP_CLIENT_MODULE);
	}

	return GLOBUS_SUCCESS;
}

static int
do_globus_deactivates(void)
{
	return GLOBUS_SUCCESS;
}

static int
is_url_ipv6(const char *url) {
	struct addrinfo *aitop;
	int gaierrno;
	struct addrinfo hints;
	char hostname[CA_MAXHOSTNAMELEN+1];
	const char *p, *p2, *p3;
	int ret;

	p = strstr (url, "://");
	if (!p)
		return (0);

	p += 3;
	while (*p == '/') ++p;

	if (!*p)
		return (0);

	p2 = strchr (p, '/');
	p3 = strchr (p, ']');

	if (p2 && p3 && p3>p2)
		p3 = NULL;

	if (*p == '[' && p3) {
		if (p3 - p >= CA_MAXHOSTNAMELEN)
			return (0);
		strncpy (hostname, p, p3 - p + 1);
		hostname[p3-p+1] = '\0';
	} else if (p2) {
		if (p2 - p > CA_MAXHOSTNAMELEN)
			return (0);
		strncpy (hostname, p, p2 - p);
		hostname[p2-p] = '\0';
	} else {
		if (strlen(p) > CA_MAXHOSTNAMELEN)
			return (0);
		strcpy (hostname, p);
	}

	p2 = strrchr (hostname, ':');
	p3 = strchr (hostname, ']');

	if (p2 && p3 && p2<p3)
		p2 = NULL;

	if (p2)
		hostname[p2-hostname] = '\0';

	memset (&hints, '\0', sizeof(hints));
	hints.ai_family = PF_INET6;
	gaierrno = Cgetaddrinfo (hostname, NULL, &hints, &aitop);
	if (gaierrno != 0)
		aitop = NULL;

	ret = (aitop != NULL) ? 1 : 0;

	if (aitop)
		freeaddrinfo (aitop);

	return (ret);
}

static int
scan_errstring(const char *p) {
	int ret = EINVAL;

	if (p == NULL) return ret;

	if (strstr (p, "o such file"))
		ret = ENOENT;
	else if (strstr (p, "ermission denied") || strstr (p, "credential"))
		ret = EACCES;
	else if (strstr (p, "exists"))
		ret = EEXIST;

	return ret;
}

static void
ggc_callback (void *callback_arg, globus_gass_copy_handle_t *handle, globus_object_t *error)
{
	monitor_t *monitor = (monitor_t *) callback_arg;
	globus_mutex_lock (&monitor->mutex);
	if (error != GLOBUS_SUCCESS) {
		monitor->errflag = GLOBUS_TRUE;
		monitor->error = globus_object_copy (error);
	}
	monitor->done = GLOBUS_TRUE;
	globus_cond_signal (&monitor->cond);
	globus_mutex_unlock (&monitor->mutex);
}

static void
ggc_cancel_callback (void *callback_arg, globus_gass_copy_handle_t *handle, globus_object_t *error)
{
	monitor_t *monitor = (monitor_t *) callback_arg;
	globus_mutex_lock (&monitor->mutex);
	if (error != GLOBUS_SUCCESS) {
		monitor->errflag = GLOBUS_TRUE;
		monitor->error = globus_object_copy (error);
	}
	monitor->done = GLOBUS_TRUE;
	globus_cond_signal (&monitor->cond);
	globus_mutex_unlock (&monitor->mutex);
}

static void 
timeout_callback(void * user_arg)
{
	check_t * check;
	monitor_t *  monitor;
	globus_reltime_t   delay;
	check = (check_t *)user_arg;
	monitor = (monitor_t *)check->mt;
	globus_mutex_lock (&monitor->mutex);
	{
		GlobusTimeReltimeSet (delay, 1, 0);
		if (!monitor->done && !istimeout && !ggc_interrupted){
			globus_callback_register_oneshot (
				NULL,
				&delay,
				timeout_callback,
				check);
		} else
			check->done = GLOBUS_TRUE;
	}
	globus_cond_signal (&monitor->cond);
	globus_mutex_unlock (&monitor->mutex);
}

static void
copyfile_sigint_handler(int dummy)
{
	ggc_interrupted = 1;
}

int
dpm_copyfile(char *src_file, char *dest_file, int nbstreams, int timeout)
{
	globus_ftp_control_dcau_t dcau;
	globus_ftp_client_operationattr_t *src_ftp_attr,*dest_ftp_attr;
	globus_url_t dest_url_struct;
	int enable_ipv6 = 0;
	globus_gass_copy_attr_t ggc_destattr;
	globus_gass_copy_attr_t ggc_srcattr;
	globus_gass_copy_handle_t ggc_handle;
	globus_result_t gresult;
	monitor_t monitor;
	int nretries = 0;
	char *p;
	globus_ftp_control_parallelism_t parallelism;
	int rc;
	struct sigaction sa,oldsa;
	struct timespec ts;
	check_t check;
	int save_errno = 0;

	ggc_interrupted = 0;
	ggc_interrupt_processed = 0;
	istimeout = 0;
	rc = do_globus_activates();
	globus_mutex_init (&monitor.mutex, NULL);
	globus_cond_init (&monitor.cond, NULL);
	globus_gass_copy_attr_init (&ggc_srcattr);
	globus_gass_copy_attr_init (&ggc_destattr);
	globus_gass_copy_handle_init (&ggc_handle, GLOBUS_NULL);
	if ((src_ftp_attr = globus_libc_malloc (sizeof(globus_ftp_client_operationattr_t))) == NULL)
		return (-1);
	if ((dest_ftp_attr = globus_libc_malloc (sizeof(globus_ftp_client_operationattr_t))) == NULL)
		return (-1);
	globus_ftp_client_operationattr_init (src_ftp_attr);
	globus_ftp_client_operationattr_init (dest_ftp_attr);

#if defined (GLOBUS_IO_OVER_XIO)
	enable_ipv6 = -1;
	if (strncmp (src_file, "gsiftp:", 7) == 0 && strncmp (dest_file, "gsiftp:", 7) == 0) {
		if (is_url_ipv6 (src_file) && is_url_ipv6 (dest_file)) {
			enable_ipv6 = 1;
		} else {
			enable_ipv6 = 0;
		}
	}
#endif

	if (strncmp (src_file, "gsiftp:", 7) == 0) {
		if (nbstreams > 1) {
			globus_ftp_client_operationattr_set_mode (src_ftp_attr,
			    GLOBUS_FTP_CONTROL_MODE_EXTENDED_BLOCK);
			parallelism.mode = GLOBUS_FTP_CONTROL_PARALLELISM_FIXED;
			parallelism.fixed.size = nbstreams;
			globus_ftp_client_operationattr_set_parallelism (src_ftp_attr,
			    &parallelism);
		}
		/* disable data channel authentication */
		dcau.mode = GLOBUS_FTP_CONTROL_DCAU_NONE;
		globus_ftp_client_operationattr_set_dcau (src_ftp_attr, &dcau);
#if defined (GLOBUS_IO_OVER_XIO)
		if (enable_ipv6==1 || (enable_ipv6<0 && is_url_ipv6 (src_file))) {
			globus_ftp_client_operationattr_set_allow_ipv6 (
			    src_ftp_attr, GLOBUS_TRUE);
		}
#endif
		globus_gass_copy_attr_set_ftp (&ggc_srcattr, src_ftp_attr);
	}
	if (strncmp (dest_file, "gsiftp:", 7) == 0) {
		if (nbstreams > 1) {
			globus_ftp_client_operationattr_set_mode (dest_ftp_attr,
			    GLOBUS_FTP_CONTROL_MODE_EXTENDED_BLOCK);
			parallelism.mode = GLOBUS_FTP_CONTROL_PARALLELISM_FIXED;
			parallelism.fixed.size = nbstreams;
			globus_ftp_client_operationattr_set_parallelism (dest_ftp_attr,
			    &parallelism);
		}
		/* disable data channel authentication */
		dcau.mode = GLOBUS_FTP_CONTROL_DCAU_NONE;
		globus_ftp_client_operationattr_set_dcau (dest_ftp_attr, &dcau);
#if defined (GLOBUS_IO_OVER_XIO)
		if (enable_ipv6==1 || (enable_ipv6<0 && is_url_ipv6 (dest_file))) {
			globus_ftp_client_operationattr_set_allow_ipv6 (
			    dest_ftp_attr, GLOBUS_TRUE);
		}
#endif
		globus_gass_copy_attr_set_ftp (&ggc_destattr, dest_ftp_attr);
	}
	sigaction (SIGINT, NULL, &oldsa);
	if (oldsa.sa_handler == SIG_DFL) {
		sa.sa_handler = &copyfile_sigint_handler;
		sigemptyset (&sa.sa_mask);
		sa.sa_flags = 0;
		sigaction (SIGINT, &sa, NULL);
	}

	monitor.done = GLOBUS_FALSE;
	monitor.errflag = GLOBUS_FALSE;
	gresult = globus_gass_copy_register_url_to_url (&ggc_handle,
	    src_file, &ggc_srcattr, dest_file, &ggc_destattr,
	    &ggc_callback, &monitor);

	/* check the state immdiately */	
	if (gresult != GLOBUS_SUCCESS ) {
		globus_object_t *errobj;
		monitor.errflag = GLOBUS_TRUE;
		errobj = globus_error_get (gresult);
#if defined (GLOBUS_ERROR_TYPE_MULTIPLE)
		p = globus_error_print_friendly (errobj);
#else
		p = globus_object_printable_to_string (errobj);
#endif
		globus_object_free (errobj);
		goto err;
	}
		
	globus_mutex_lock (&monitor.mutex);
	check.mt = &monitor;
	check.done = GLOBUS_FALSE;
	globus_callback_register_oneshot (
			NULL,
			NULL,
			timeout_callback,
			&check);
	if (timeout) {
		ts.tv_sec = time (0) + timeout;
		ts.tv_nsec = 0;
		while ( (! monitor.done) && (!istimeout) && (!save_errno)) {
			if (ggc_interrupted && ! ggc_interrupt_processed) {
				globus_gass_copy_cancel (&ggc_handle,
				    &ggc_cancel_callback, &monitor);
				ggc_interrupt_processed = 1;
			}
			save_errno = globus_cond_timedwait (&monitor.cond, &monitor.mutex, &ts);
			if (save_errno == EINTR)
				save_errno = 0;
		}
	} else
		while ( (! monitor.done) && (!istimeout) && (!save_errno)) {
			if (ggc_interrupted && ! ggc_interrupt_processed) {
				globus_gass_copy_cancel (&ggc_handle,
				    &ggc_cancel_callback, &monitor);
				ggc_interrupt_processed = 1;
			}
			save_errno = globus_cond_wait (&monitor.cond, &monitor.mutex);
			if (save_errno == EINTR)
				save_errno = 0;
		}
	if (save_errno == ETIMEDOUT)
		istimeout = 1;
	if (!monitor.done){
		int ret;
		globus_gass_copy_cancel (&ggc_handle, &ggc_cancel_callback, &monitor);
		/* wait for the cancel to complete before returning to user */
		do {
			ret = globus_cond_wait (&monitor.cond, &monitor.mutex);
			if (ret == EINTR)
				ret = 0;
		} while (!monitor.done && !ret);
		if (ret && !save_errno)
			save_errno = ret;
	}

	/* wait timeout_callback to complete */

	if (!check.done) {
		int ret;
		do {
			ret = globus_cond_wait (&monitor.cond, &monitor.mutex);
			if (ret == EINTR)
				ret = 0;
		} while (!check.done && !ret);
		if (ret && !save_errno)
			save_errno = ret;
	}
	globus_mutex_unlock (&monitor.mutex);

	if (ggc_interrupt_processed == 1) {
		save_errno = ECANCELED;
	} else if (istimeout == 1) {
		save_errno = ETIMEDOUT;
	}

	if (monitor.errflag) {
		int err;
#if defined (GLOBUS_ERROR_TYPE_MULTIPLE)
		p = globus_error_print_friendly (monitor.error);
#else
		p = globus_object_printable_to_string (monitor.error);
#endif
		globus_object_free (monitor.error);
err:                
			err = scan_errstring (p);
			if (p != NULL && !save_errno && err != ENOENT && err != EEXIST)
		                dpm_errmsg ("dpm_copyfile", "%s", p);

		if (p != NULL)
			globus_libc_free (p);

		if (!save_errno)
			save_errno = err;
	}

	sigaction (SIGINT, &oldsa, NULL);
	
	(void) globus_mutex_destroy (&monitor.mutex);
	(void) globus_cond_destroy (&monitor.cond);
	(void) globus_gass_copy_handle_destroy (&ggc_handle);
	(void) globus_ftp_client_operationattr_destroy (src_ftp_attr);
	(void) globus_ftp_client_operationattr_destroy (dest_ftp_attr);
	(void) do_globus_deactivates();

	if (( monitor.errflag == 0 ) && (!save_errno))
		return 0;

	serrno = save_errno;
	return -1;
}
