.\" @(#)$RCSfile: dpm_getreqid.man,v $ $Revision: 1.2 $ $Date: 2006/02/17 06:47:32 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GETREQID 3 "$Date: 2006/02/17 06:47:32 $" LCG "DPM Library Functions"
.SH NAME
dpm_getreqid \- get request id for a set of requests
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getreqid (char *" u_token ,
.BI "int *" nbreplies ,
.BI "struct dpm_tokeninfo **" tokeninfos )
.SH DESCRIPTION
.B dpm_getreqid
gets request id for a set of requests.
.LP
The input argument is:
.TP
.I u_token
specifies the user provided description associated with a previous get/put/copy
request.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of token infos.
.TP
.I tokeninfos
will be set to the address of an array of dpm_tokeninfo structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_tokeninfo {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	time_t	c_time;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I nbreplies
or
.I tokeninfos
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for storing the reply.
.TP
.B EINVAL
The length of the user request description is greater than 255 or
the user request description is unknown.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
