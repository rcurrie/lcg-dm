/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights released
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-releasespace.c,v $ $Revision: 1.1 $ $Date: 2007/02/12 14:10:06 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-releasespace - release space */
#include <errno.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
int help_flag;
int force;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	static struct Coptions longopts[] = {
		{"force", NO_ARGUMENT, &force, 1},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"space_token", REQUIRED_ARGUMENT, 0, OPT_S_TOKEN},
		{"token_desc", REQUIRED_ARGUMENT, 0, OPT_U_DESC},
		{0, 0, 0, 0}
	};
	int nbreplies = 0;
	char *s_token = NULL;
	char **s_tokens = NULL;
	char *u_token = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_S_TOKEN:
			s_token = Coptarg;
			break;
		case OPT_U_DESC:
			u_token = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || (! s_token && ! u_token)) {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s\t%s %s",
		    argv[0], "--space_token space_token [--force] [--help]\n",
		    argv[0], "--token_desc u_token [--force] [--help]\n");
		exit (errflg ? USERR : 0);
	}

	if (! s_token) {
		if (dpm_getspacetoken (u_token, &nbreplies, &s_tokens) < 0) {
			fprintf (stderr, "dpm-releasespace: %s\n", sstrerror(serrno));
			exit (serrno == EINVAL ? USERR : SYERR);
		}
		if (nbreplies > 1) {
			fprintf (stderr,
			    "dpm-releasespace: more than one space token associated with this description\n");
			exit (USERR);
		}
		s_token = s_tokens[0];
	}

	if (dpm_releasespace (s_token, force) < 0) {
		fprintf (stderr, "dpm-releasespace: %s\n", sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
