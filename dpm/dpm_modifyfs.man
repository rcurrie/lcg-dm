.\" @(#)$RCSfile: dpm_modifyfs.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2011 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_MODIFYFS 3 "$Date$" LCG "DPM Library Functions"
.SH NAME
dpm_modifyfs \- modify the parameters of a disk pool filesystem
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_modifyfs (char *" server ,
.BI "char *" fs ,
.BI "int " status ,
.BI "int " weight )
.SH DESCRIPTION
.B dpm_modifyfs
modifies the parameters of a disk pool filesystem.
.TP
.I server
specifies the host name of the disk server where this filesystem is mounted.
.TP
.I fs
specifies the mount point of the dedicated filesystem.
.TP
.I status
Initial status of this filesystem. It can be set to 0 or
.B FS_DISABLED
or
.BR FS_RDONLY .
(-1 will tell the server to not change the current value).
.TP
.I weight
specifies the weight of the filesystem. This is used during the filesystem
selection. The value must be positive (-1 will tell the server to not change
the current value).
It is recommended to use a value lower than 10.
.LP
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
Filesystem does not exist.
.TP
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B EFAULT
.I server
or
.I fs
is a NULL pointer.
.TP
.B EINVAL
The length of
.I server
exceeds
.B CA_MAXHOSTNAMELEN
or the length of
.I fs
exceeds 79.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
