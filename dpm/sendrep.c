/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: sendrep.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <sys/types.h>
#include <string.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <netinet/in.h>
#endif
#include <stdio.h>
#include <stdarg.h>
#include "marshall.h"
#include "net.h"
#include "dpm.h"
sendrep(int rpfd, int rep_type, ...)
{
	va_list args;
	char func[16];
	char *msg;
	int n;
	char *p;
	char prtbuf[PRTBUFSZ];
	char *q;
	char *rbp;
	int rc;
	char repbuf[REPBUFSZ+12];
	int repsize;

	strcpy (func, "sendrep");
	rbp = repbuf;
	marshall_LONG (rbp, DPM_MAGIC3);
	va_start (args, rep_type);
	marshall_LONG (rbp, rep_type);
	switch (rep_type) {
	case MSG_ERR:
		msg = va_arg (args, char *);
		vsprintf (prtbuf, msg, args);
		marshall_LONG (rbp, strlen (prtbuf) + 1);
		marshall_STRING (rbp, prtbuf);
		dpmlogit (func, "%s", prtbuf);
		break;
	case MSG_DATA:
	case MSG_GET:
	case MSG_PUT:
	case MSG_COPY:
	case MSG_SURLST:
	case MSG_FS:
	case MSG_POOL:
	case MSG_SUMMARY:
	case MSG_REQIDS:
	case MSG_SPCMD:
	case MSG_SPCTKN:
		n = va_arg (args, int);
		marshall_LONG (rbp, n);
		msg = va_arg (args, char *);
		memcpy (rbp, msg, n);	/* marshalling already done */
		rbp += n;
		break;
	case DPM_IRC:
	case DPM_RC:
		rc = va_arg (args, int);
		marshall_LONG (rbp, rc);
		break;
	}
	va_end (args);
	repsize = rbp - repbuf;
	if (netwrite (rpfd, repbuf, repsize) != repsize) {
		dpmlogit (func, DP002, "send", neterror());
		if (rep_type == DPM_RC)
			netclose (rpfd);
		return (-1);
	}
	if (rep_type == DPM_RC)
		netclose (rpfd);
	return (0);
}
