/*
 * Copyright (C) 2004-2008 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-addpool.c,v $ $Revision: 1.11 $ $Date: 2008/02/11 10:35:48 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-addpool - define a new disk pool */
#include <errno.h>
#include <sys/types.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
int help_flag;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	struct dpm_pool dpm_pool;
	int errflg = 0;
	char gid_str[256];
	struct group *gr;
	int i;
	static struct Coptions longopts[] = {
		{"def_filesize", REQUIRED_ARGUMENT, 0, OPT_DEF_FSZ},
		{"def_lifetime", REQUIRED_ARGUMENT, 0, OPT_DEF_LTIME},
		{"def_pintime", REQUIRED_ARGUMENT, 0, OPT_DEF_PTIME},
		{"fss_policy", REQUIRED_ARGUMENT, 0, OPT_FSS_POLICY},
		{"gc_policy", REQUIRED_ARGUMENT, 0, OPT_GC_POLICY},
		{"gc_start_thresh", REQUIRED_ARGUMENT, 0, OPT_GC_START},
		{"gc_stop_thresh", REQUIRED_ARGUMENT, 0, OPT_GC_STOP},
		{"gid", REQUIRED_ARGUMENT, 0, OPT_POOL_GID},
		{"group", REQUIRED_ARGUMENT, 0, OPT_POOL_GROUP},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"max_lifetime", REQUIRED_ARGUMENT, 0, OPT_MAX_LTIME},
		{"max_pintime", REQUIRED_ARGUMENT, 0, OPT_MAX_PTIME},
		{"mig_policy", REQUIRED_ARGUMENT, 0, OPT_MIG_POLICY},
		{"poolname", REQUIRED_ARGUMENT, 0, OPT_POOL_NAME},
		{"ret_policy", REQUIRED_ARGUMENT, 0, OPT_RET_POLICY},
		{"rs_policy", REQUIRED_ARGUMENT, 0, OPT_RS_POLICY},
		{"s_type", REQUIRED_ARGUMENT, 0, OPT_S_TYPE},
		{0, 0, 0, 0}
	};
	char *p;

	memset (&dpm_pool, 0, sizeof(struct dpm_pool));
	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_POOL_NAME:
			if (strlen (Coptarg) > CA_MAXPOOLNAMELEN) {
				fprintf (stderr,
				    "pool name too long: %s\n", Coptarg);
				errflg++;
			} else
				strcpy (dpm_pool.poolname, Coptarg);
			break;
		case OPT_DEF_FSZ:
			p = Coptarg;
			while (*p >= '0' && *p <= '9') p++;
			if (! (*p == '\0' || ((*p == 'k' || *p == 'M' ||
			    *p == 'G' || *p == 'T' || *p == 'P') && *(p+1) == '\0'))) {
				fprintf (stderr,
				    "invalid default filesize %s\n", Coptarg);
				errflg++;
			} else
				dpm_pool.defsize = strutou64 (Coptarg);
			break;
		case OPT_GC_START:
			if ((dpm_pool.gc_start_thresh = strtol (Coptarg, &dp, 10)) < 0 ||
			    *dp != '\0') {
				fprintf (stderr,
				    "invalid gc_start_thresh %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_GC_STOP:
			if ((dpm_pool.gc_stop_thresh = strtol (Coptarg, &dp, 10)) < 0 ||
			    *dp != '\0') {
				fprintf (stderr,
				    "invalid gc_stop_thresh %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_DEF_LTIME:
			if ((dpm_pool.def_lifetime = strtol (Coptarg, &dp, 10)) < 0 ||
			    *dp != '\0') {
				fprintf (stderr,
				    "invalid def_lifetime %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_DEF_PTIME:
			if ((dpm_pool.defpintime = strtol (Coptarg, &dp, 10)) < 0 ||
			    *dp != '\0') {
				fprintf (stderr,
				    "invalid defpintime %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_MAX_LTIME:
			if ((dpm_pool.max_lifetime = strtol (Coptarg, &dp, 10)) < 0 ||
			    *dp != '\0') {
				fprintf (stderr,
				    "invalid max_lifetime %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_MAX_PTIME:
			if ((dpm_pool.maxpintime = strtol (Coptarg, &dp, 10)) < 0 ||
			    *dp != '\0') {
				fprintf (stderr,
				    "invalid maxpintime %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_FSS_POLICY:
			if (strlen (Coptarg) > CA_MAXPOLICYLEN) {
				fprintf (stderr,
				    "policy name too long: %s\n", Coptarg);
				errflg++;
			} else
				strcpy (dpm_pool.fss_policy, Coptarg);
			break;
		case OPT_GC_POLICY:
			if (strlen (Coptarg) > CA_MAXPOLICYLEN) {
				fprintf (stderr,
				    "policy name too long: %s\n", Coptarg);
				errflg++;
			} else
				strcpy (dpm_pool.gc_policy, Coptarg);
			break;
		case OPT_MIG_POLICY:
			if (strlen (Coptarg) > CA_MAXPOLICYLEN) {
				fprintf (stderr,
				    "policy name too long: %s\n", Coptarg);
				errflg++;
			} else
				strcpy (dpm_pool.mig_policy, Coptarg);
			break;
		case OPT_RS_POLICY:
			if (strlen (Coptarg) > CA_MAXPOLICYLEN) {
				fprintf (stderr,
				    "policy name too long: %s\n", Coptarg);
				errflg++;
			} else
				strcpy (dpm_pool.rs_policy, Coptarg);
			break;
		case OPT_POOL_GID:
			p = Coptarg;
			while (*p) {
				if (*p == ',') dpm_pool.nbgids++;
				p++;
			}
			dpm_pool.nbgids++;
			if ((dpm_pool.gids = malloc (dpm_pool.nbgids * sizeof(gid_t))) == NULL) {
				fprintf (stderr, "Could not allocate memory for gids\n");
				exit (USERR);
			}
			i = 0;
			p = strtok (Coptarg, ",");
			while (p) {
				if ((dpm_pool.gids[i] = strtol (p, &dp, 10)) < 0 ||
				    *dp != '\0') {
					fprintf (stderr,
					    "invalid pool_gid %s\n", p);
					errflg++;
					goto next_gid;
				}
#ifdef VIRTUAL_ID
				if (dpm_pool.gids[i] > 0 &&
				    Cns_getgrpbygid (dpm_pool.gids[i], gid_str) < 0) {
#else
				if (dpm_pool.gids[i] > 0 &&
				    ! getgrgid (dpm_pool.gids[i])) {
#endif
					fprintf (stderr,
					    "invalid pool_gid: %s\n", p);
					errflg++;
				}
next_gid:
				if (p = strtok (NULL, ",")) *(p - 1) = ',';
				i++;
			}
			break;
		case OPT_POOL_GROUP:
			p = Coptarg;
			while (*p) {
				if (*p == ',') dpm_pool.nbgids++;
				p++;
			}
			dpm_pool.nbgids++;
			if ((dpm_pool.gids = malloc (dpm_pool.nbgids * sizeof(gid_t))) == NULL) {
				fprintf (stderr, "Could not allocate memory for gids\n");
				exit (USERR);
			}
			i = 0;
			p = strtok (Coptarg, ",");
			while (p) {
				if (strcmp (p, "ALL") == 0)
					dpm_pool.gids[i] = 0;
#ifdef VIRTUAL_ID
				else if (strcmp (p, "root") == 0)
					dpm_pool.gids[i] = 0;
				else if (Cns_getgrpbynam (p, &dpm_pool.gids[i]) < 0) {
#else
				else if ((gr = getgrnam (p)))
					dpm_pool.gids[i] = gr->gr_gid;
				else {
#endif
					fprintf (stderr,
					    "invalid pool_group: %s\n", p);
					errflg++;
				}
				if (p = strtok (NULL, ",")) *(p - 1) = ',';
				i++;
			}
			break;
		case OPT_RET_POLICY:
			dpm_pool.ret_policy = *Coptarg;
			break;
		case OPT_S_TYPE:
			dpm_pool.s_type = *Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || *dpm_pool.poolname == '\0') {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s%s%s%s%s%s%s", argv[0],
		    "--poolname pool_name --def_filesize defsize\n",
		    "\t[--def_lifetime def_lifetime] [--def_pintime defpintime]\n",
		    "\t[--gc_start_thresh perc] [--gc_stop_thresh perc]\n",
		    "\t[--gid pool_gid(s)] [--group pool_group(s)] [--help]\n",
		    "\t[--max_lifetime max_lifetime] [--max_pintime maxpintime]\n",
		    "\t[--fss_policy name] [--gc_policy name] [--rs_policy name]\n",
		    "\t[--mig_policy name] [--ret_policy name] [--s_type space_type]\n");
		exit (errflg ? USERR : 0);
	}

	if (dpm_addpool (&dpm_pool) < 0) {
		fprintf (stderr, "dpm-addpool %s: %s\n", dpm_pool.poolname,
		    (serrno == EEXIST) ? "Pool exists already" : sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
