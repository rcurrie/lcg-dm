.\" @(#)$RCSfile: dpm-register.man,v $ $Revision: 1.3 $ $Date: 2008/01/07 10:38:38 $ CERN Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN
.\" All rights reserved
.\"
.TH DPM-REGISTER 1 "$Date: 2008/01/07 10:38:38 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-register \- register external files in DPNS
.SH SYNOPSIS
.B dpm-register
.BI --filesize " filesize"
[
.BI --user " owner_user"
] [
.BI --group " owner_group"
] [
.BI --mode " filemode"
] 
.BI --pfn " pfn"
.BI --server " hostname"
[
.BI --st " status"
] [
.B --help
]
.I filename
.SH DESCRIPTION
.B dpm-register
registers a given filename with the specified permissions into DPNS:
   - create the file metadata entry in DPNS (dpns_creat)
   - set the ownership (dpns_chown) to
.I owner_user
:
.I owner_group
(by default the requester's DN and group)
   - set the filemode (dpns_chmod) to
.I filemode
   - set the size of the file (dpns_setfsize) to
.I filesize
   - add the replica entry (dpns_addreplicax) setting
     * sfn = 
.I pfn
     * status to
.I status
('O' for Online, 'N' for Nearline)
     * f_type = 'P' (Permanent)
     * r_type = 'P' (Primary)
.SH EXAMPLE
.nf
.ft CW
	dpm-register --filesize 525424 --server `hostname --fqdn` \\
		--group biomed --pfn dicom:///1/2/3 \\
		/dpm/cern.ch/home/biomed/mdm/1/2/3
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpns_creat(3) ,
.BR dpns_chown(3) ,
.BR dpns_chmod(3) , 
.BR dpns_setfsize(3) ,
.B dpns_addreplicax(3)
