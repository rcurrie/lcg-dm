.\" @(#)$RCSfile: dpm_releasespace.man,v $ $Revision: 1.1 $ $Date: 2005/11/09 09:18:53 $ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH DPM_RELEASESPACE 3 "$Date: 2005/11/09 09:18:53 $" LCG "DPM Library Functions"
.SH NAME
dpm_releasespace \- release space
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_releasespace (char *" s_token ,
.BI "int " force )
.SH DESCRIPTION
.B dpm_releasespace
releases space.
.LP
The input arguments are:
.TP
.I s_token
specifies the token returned by a previous reservespace request.
.TP
.I force
force file release.
If force is false (default), the space will not be released if it has
files that are still pinned in the space.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EINVAL
.I s_token
is invalid (too long) or unknown.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
