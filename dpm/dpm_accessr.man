.\" Copyright (C) 2013 by CERN/IT/SDC/ID
.\" All rights reserved
.\"
.TH DPM_ACCESSR 3 "$Date: 2013/06/12 14:00:00 $" WLCG "DPM Library Functions"
.SH NAME
dpm_accessr \- check existence/accessibility of a file replica in the pools
.SH SYNOPSIS
.br
.B #include <sys/types.h>
.br
.B #include <unistd.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_accessr (const char *" pfn ,
.BI "int " amode );
.SH DESCRIPTION
.B dpm_accessr
checks the existence or the accessibility of the file replica according to the dpm. The
name server entry for the replica is taken into account and that of the associated pool
and, if relevant, the status of an ongoing put request.
The physical file name
.I pfn
is checked according to the bit pattern in
.I amode
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file does not exist.
.TP
.B EACCES
Search permission is denied on a component of the parent file prefix,
the specified access to the file itself is denied or W_OK is requested but
the replica status is not 'P' (cannot modify an existing file).
.TP
.B EFAULT
.I pfn
is a NULL pointer.
.TP
.B EINVAL
.I amode
is invalid.
.TP
.B ENAMETOOLONG
The length of
.I pfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EDPMNACT
Disk Pool Manager is not running or is being shutdown.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.B Cns_accessr(3)
