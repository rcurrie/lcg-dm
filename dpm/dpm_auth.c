/*
 * Copyright (C) 2005 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_auth.c,v $ $Revision: 1.3 $ $Date: 2009/09/03 13:38:46 $ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include "Castor_limits.h"
#include "dpm_api.h"
#include "serrno.h"

/*      dpm_client_getAuthorizationId - get the authorization id from the thread-specific structure */

int DLL_DECL
dpm_client_getAuthorizationId(uid_t *uid, gid_t *gid, char **mech, char **id)
{
	struct dpm_api_thread_info *thip;

#ifdef CSEC
	if (dpm_apiinit (&thip))
		return (-1);
	if (thip->use_authorization_id == 0)
		return (0);
	if (uid)
		*uid = thip->Csec_uid;
	if (gid)
		*gid = thip->Csec_gid;
	if (mech)
		*mech = thip->Csec_mech;
	if (id)
		*id = thip->Csec_auth_id;
#endif
	return (0);
}

/*	dpm_client_resetAuthorizationId - reset the authorization id in the thread-specific structure */

int DLL_DECL
dpm_client_resetAuthorizationId()
{
        char func[32];
        struct dpm_api_thread_info *thip;

#ifdef CSEC
	strcpy (func, "dpm_client_resetAuthorizationId");
        if (dpm_apiinit (&thip))
                return (-1);
        thip->use_authorization_id = 0;
#endif
      	return (0);
}

/*      dpm_client_setAuthorizationId - set the authorization id in the thread-specific structure */

int DLL_DECL
dpm_client_setAuthorizationId(uid_t uid, gid_t gid, const char *mech, char *id)
{
	char func[30];
	struct dpm_api_thread_info *thip;

#ifdef CSEC
	strcpy (func, "dpm_client_setAuthorizationId");
	if (dpm_apiinit (&thip))
		return (-1);
	thip->Csec_uid = uid;
	thip->Csec_gid = gid;
	if (strlen (mech) > CA_MAXCSECPROTOLEN) {
		dpm_errmsg (func, "Supplied Csec protocol is too long\n");
		serrno = EINVAL;
		return (-1);
	}
	strcpy (thip->Csec_mech, mech);
	if (strlen (id) > CA_MAXCSECNAMELEN) {
		dpm_errmsg (func, "Supplied authorization id is too long\n");
		serrno = EINVAL;
		return (-1);
	}
	strcpy (thip->Csec_auth_id, id);
	thip->voname = NULL;
	thip->nbfqan = 0;
	thip->fqan = NULL;
	thip->use_authorization_id = 1;
#endif
	return (0);
}

/*      dpm_client_setSecurityOpts - set the security options in the thread-specific structure */

int DLL_DECL
dpm_client_setSecurityOpts(int opt)
{
	struct dpm_api_thread_info *thip;

#ifdef CSEC
	if (dpm_apiinit (&thip))
		return (-1);
	thip->Csec_opt = opt;
#endif
	return (0);
}

/*      dpm_client_setVOMS_data - set the VOMS data in the thread-specific structure */

int DLL_DECL
dpm_client_setVOMS_data(char *voname, char **fqan, int nbfqan)
{
	struct dpm_api_thread_info *thip;

#ifdef CSEC
	if (dpm_apiinit (&thip))
		return (-1);
	thip->voname = voname;
	thip->nbfqan = nbfqan;
	thip->fqan = fqan;
#endif
	return (0);
}
