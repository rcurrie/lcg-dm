/*
 * Copyright (C) 2006-2011 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-replicate.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*      dpm-replicate - replicate a file given the SURL or the PFN */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "rfio_api.h"
#include "serrno.h"
int help_flag=0;

main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	int errflg = 0;
	static struct Coptions longopts[] = {
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"f_type", REQUIRED_ARGUMENT, 0, OPT_F_TYPE},
		{"space_token", REQUIRED_ARGUMENT, 0, OPT_S_TOKEN},
		{"lifetime", REQUIRED_ARGUMENT, 0, OPT_LIFETIME},
		{0, 0, 0, 0}
	};
	time_t req_lifetime = 0;
	char *s_token = NULL;
	char f_type = '\0';

	Cns_set_selectsrvr (CNS_SSRV_NOTPATH);

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_F_TYPE:
			f_type = *Coptarg;
			break;
		case OPT_LIFETIME:
			if (strcmp (Coptarg, "Inf") == 0) {
				req_lifetime = 0x7FFFFFFF;
				break;
			}
			if ((req_lifetime = strtol (Coptarg, &dp, 10)) < 0 ||
			    (*dp != '\0' && *(dp+1) != '\0')) {
				fprintf (stderr,
				    "invalid lifetime %s\n", Coptarg);
				errflg++;
				break;
			}
			switch (*dp) {
				case 'y':
					req_lifetime *= 365 * 86400;
					break;
				case 'm':
					req_lifetime *= 30 * 86400;
					break;
				case 'd':
					req_lifetime *= 86400;
					break;
				case 'h':
					req_lifetime *= 3600;
					break;
				case '\0':
					break;
                        	default:
					fprintf (stderr,
					    "invalid lifetime %s\n", Coptarg);
	                                errflg++;
			}
			break;
		case OPT_S_TOKEN:
			s_token = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind >= argc)
		errflg++;
	if (req_lifetime == 0x7FFFFFFF) {
		if (f_type && f_type != 'P')
			errflg++;
		f_type = 'P';
	}
	if (req_lifetime && req_lifetime != 0x7FFFFFFF) {
		if (f_type == 'P')
			errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s", argv[0],
		"[--help] [--f_type file_type] [--space_token s_token] [--lifetime file_lifetime] file\n");
		exit (errflg ? USERR : 0);
	}
	if (req_lifetime && req_lifetime != 0x7FFFFFFF)
		req_lifetime += time (0);
	rfio_errno = 0;
	if (dpm_replicatex (argv[Coptind], f_type, s_token, req_lifetime, NULL) < 0) {
		fprintf (stderr, "dpm-replicate %s: %s\n", argv[Coptind],
		    rfio_serror());
		exit (USERR);
	}
	exit (0);
}
