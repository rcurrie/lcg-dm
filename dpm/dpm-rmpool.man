.\" @(#)$RCSfile: dpm-rmpool.man,v $ $Revision: 1.3 $ $Date: 2007/01/09 10:15:04 $ CERN Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN
.\" All rights reserved
.\"
.TH DPM-RMPOOL 1 "$Date: 2007/01/09 10:15:04 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-rmpool \- remove a disk pool definition
.SH SYNOPSIS
.B dpm-rmpool
.BI --poolname " pool_name"
[
.B --help
]
.SH DESCRIPTION
.B dpm-rmpool
removes a disk pool definition.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.I pool_name
specifies the disk pool name.
It must be at most CA_MAXPOOLNAMELEN characters long.
.SH EXAMPLE
.nf
.ft CW
	dpm-rmpool --poolname Volatile
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.B dpm_rmpool(3)
