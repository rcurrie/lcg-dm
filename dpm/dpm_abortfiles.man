.\" @(#)$RCSfile: dpm_abortfiles.man,v $ $Revision: 1.2 $ $Date: 2006/08/01 07:37:06 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH DPM_ABORTFILES 3 "$Date: 2006/08/01 07:37:06 $" LCG "DPM Library Functions"
.SH NAME
dpm_abortfiles \- abort a set of file requests
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_abortfiles (char *" r_token ,
.BI "int " nbsurls ,
.BI "char **" surls ,
.BI "int *" nbreplies ,
.BI "struct dpm_filestatus **" filestatuses )
.SH DESCRIPTION
.B dpm_abortfiles
aborts a set of file requests.
File requests in status DPM_QUEUED are removed from the queue of pending
requests, the ones in status DPM_READY or DPM_DONE are released (unpinned).
.LP
The input arguments are:
.TP
.I r_token
specifies the token returned by a previous get/put/copy request.
.TP
.I nbsurls
specifies the number of files to be aborted.
.TP
.I surls
specifies the array of file names.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of file statuses.
.TP
.I filestatuses
will be set to the address of an array of dpm_filestatus structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_filestatus {
	char		*surl;
	int		status;
	char		*errstring;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
File does not exist.
.TP
.B EACCES
Permission denied.
.TP
.B EFAULT
.IR r_token ,
.IR surls ,
.I nbreplies
or
.I filestatuses
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request.
.TP
.B EINVAL
.I nbsurls
is not strictly positive, the token is not known or all file requests have
errors.
.TP
.B ENAMETOOLONG
The length of the surl exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
