.\" @(#)$RCSfile: dpm-replicate.man,v $ $Revision: 1.3 $ $Date: 2008/08/27 12:43:28 $ CERN Jean-Philippe Baud
.\" Copyright (C) 2006 by CERN
.\" All rights reserved
.\"
.TH DPM-REPLICATE 1 "$Date: 2008/08/27 12:43:28 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-replicate \- replicate a file given the SURL or the PFN
.SH SYNOPSIS
.B dpm-replicate
[
.BI --f_type " file_type"
] [
.BI --space_token " s_token"
] [
.BI --lifetime " file_lifetime"
] [
.B --help
]
file
.SH DESCRIPTION
.B dpm-replicate
replicates a file given the SURL or the PFN.
.TP
.I file_type
indicates the type of file desired for the new replica. It can be
.BR V " (for Volatile),"
.BR D " (for Durable)"
or
.BR P " (for Permanent)."
.TP
.I file_lifetime
specifies the requested file lifetime of the new replica relative to the current time.
It can be "Inf" (for infinite) or expressed in years (suffix 'y'), months
(suffix 'm'), days (suffix 'd'), hours (suffix 'h') or seconds (no suffix).
If specified the
.BR file_type
and
.BR file_lifetime
must be compatible.
.TP
.I s_token
specifies that the new replica should be allocated within a space identified by the
given token.
.SH EXAMPLE
.nf
.ft CW
	dpm-replicate --f_type P /dpm/cern.ch/home/dteam/afile
.sp
	dpm-replicate --f_type V lxb1921.cern.ch:/storage/dteam/2005-12-14/afile.38215.0
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.BR dpm_get(3) ,
.B dpm_put(3)
