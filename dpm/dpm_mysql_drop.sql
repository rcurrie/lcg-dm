--
--  Copyright (C) 2004-2008 by CERN/IT/GD/CT
--  All rights reserved
--
--       @(#)$RCSfile: dpm_mysql_drop.sql,v $ $Revision: 1.3 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/CT Jean-Philippe Baud
 
--     Drop Disk Pool Manager MySQL tables and constraints.

USE dpm_db;
ALTER TABLE dpm_space_reserv
       DROP INDEX s_token;
ALTER TABLE dpm_copy_filereq
       DROP INDEX pk_c_fullid;
ALTER TABLE dpm_put_filereq
       DROP INDEX pk_p_fullid;
ALTER TABLE dpm_get_filereq
       DROP INDEX pk_g_fullid;
ALTER TABLE dpm_req
       DROP INDEX r_token;
ALTER TABLE dpm_pending_req
       DROP INDEX r_token;
ALTER TABLE dpm_fs
       DROP INDEX pk_fs,
       DROP FOREIGN KEY fk_fs;
ALTER TABLE dpm_pool
       DROP INDEX poolname;

DROP TABLE dpm_unique_id;

DROP TABLE dpm_space_reserv;
DROP TABLE dpm_copy_filereq;
DROP TABLE dpm_put_filereq;
DROP TABLE dpm_get_filereq;
DROP TABLE dpm_req;
DROP TABLE dpm_pending_req;
DROP TABLE dpm_fs;
DROP TABLE dpm_pool;
DROP TABLE schema_version_dpm;
