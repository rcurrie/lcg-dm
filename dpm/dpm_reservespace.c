/*
 * Copyright (C) 2005-2010 by CERN/IT/GD/SC
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_reservespace.c,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_reservespace - reserve space */

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_reservespace(const char s_type, const char *u_token, const char ret_policy, const char ac_latency, u_signed64 req_t_space, u_signed64 req_g_space, time_t req_lifetime, int nbgids, gid_t *req_gids, const char *poolname, char *actual_s_type, u_signed64 *actual_t_space, u_signed64 *actual_g_space, time_t *actual_lifetime, char *s_token)
{
	time_t actual_life;
	u_signed64 actual_space;
	char actual_type;
	int c;
	char errstring[256];
	char func[17];
	gid_t gid;
	int i;
	int msglen;
	char *p;
	char *q;
	char *rbp;
	char repbuf[4+256+25+CA_MAXDPMTOKENLEN+1];
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_reservespace");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbgids > 0 && ! req_gids) {
		serrno = EFAULT;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC3);
	marshall_LONG (sbp, DPM_RSVSPACE);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_BYTE (sbp, s_type);
	if (u_token) {
		marshall_STRING (sbp, u_token);
	} else {
		marshall_STRING (sbp, "");
	}
	marshall_HYPER (sbp, req_t_space);
	marshall_HYPER (sbp, req_g_space);
	marshall_TIME_T (sbp, req_lifetime);
	marshall_BYTE (sbp, ret_policy);
	marshall_BYTE (sbp, ac_latency);
	marshall_LONG (sbp, nbgids > 0 ? req_gids[0] : 0);
	if (poolname) {
		marshall_STRING (sbp, poolname);
	} else {
		marshall_STRING (sbp, "");
	}
	marshall_LONG (sbp, nbgids);
	for (i = 0; i < nbgids; i++)
		marshall_LONG (sbp, req_gids[i]);

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2dpm (NULL, sendbuf, msglen, repbuf, sizeof(repbuf), NULL, NULL);
	if (c == 0) { 
		rbp = repbuf;
		unmarshall_LONG (rbp, c);
		if (c) {
			serrno = c - DPM_FAILED;
			c = -1;
		}
		unmarshall_STRING (rbp, errstring);
		if (*errstring)
			dpm_errmsg (func, "%s\n", errstring);
		if (c == 0) {
			unmarshall_BYTE (rbp, actual_type);
		       	if (actual_s_type)
				*actual_s_type = actual_type;
			unmarshall_HYPER (rbp, actual_space);
		       	if (actual_t_space)
				*actual_t_space = actual_space;
			unmarshall_HYPER (rbp, actual_space);
		       	if (actual_g_space)
				*actual_g_space = actual_space;
			unmarshall_TIME_T (rbp, actual_life);
		       	if (actual_lifetime)
				*actual_lifetime = actual_life;
			if (s_token)
				unmarshall_STRING (rbp, s_token);
		}
	}
	return (c);
}
