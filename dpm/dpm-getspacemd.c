/*
 * Copyright (C) 2007-2008 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-getspacemd.c,v $ $Revision: 1.5 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*      dpm-getspacemd - get space metadata */
#include <errno.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
int help_flag;
int si_flag;

static char *
decode_group(gid_t gid)
{
	struct group *gr;
	static gid_t sav_gid = -1;
	static char sav_gidstr[256];

	if (gid != sav_gid) {
#ifdef VIRTUAL_ID
		if (gid == 0)
			return ("root");
		sav_gid = gid;
		if (Cns_getgrpbygid (sav_gid, sav_gidstr) < 0)
#else
		sav_gid = gid;
		if (gr = getgrgid (sav_gid)) {
			strcpy (sav_gidstr, gr->gr_name);
		} else
#endif
			sprintf (sav_gidstr, "%d", sav_gid);
	}
	return (sav_gidstr);
}

static char *
decode_time(int t, char *buf)
{
	float fnum;
	char unit;

	if (t == 0x7FFFFFFF) {
		strcpy (buf, "Inf");
		return (buf);
	}
	if (t >= 365 * 86400) {
		fnum = (float) t / (float) (365 * 86400);
		unit = 'y';
	} else if (t >= 30 * 86400) {
		fnum = (float) t / (float) (30 * 86400);
		unit = 'm';
	} else if (t >= 86400) {
		fnum = (float) t / 86400.;
		unit = 'd';
	} else if (t >= 3600) {
		fnum = (float) t / 3600.;
		unit = 'h';
	} else
		unit = ' ';
	if (unit != ' ')
		sprintf (buf, "%.1f%c", fnum, unit);
	else
		sprintf (buf, "%d", t);
	return (buf);
}

static char *
decode_user(uid_t uid)
{
	struct passwd *pw;
	static uid_t sav_uid = -1;
	static char sav_uidstr[256];

	if (uid != sav_uid) {
#ifdef VIRTUAL_ID
		if (uid == 0)
			return ("root");
		sav_uid = uid;
		if (Cns_getusrbyuid (sav_uid, sav_uidstr) < 0)
#else
		sav_uid = uid;
		if (pw = getpwuid (sav_uid)) {
			strcpy (sav_uidstr, pw->pw_name);
		} else
#endif
			sprintf (sav_uidstr, "%d", sav_uid);
	}
	return (sav_uidstr);
}

listentry(spacemd)
struct dpm_space_metadata *spacemd;
{
	int i;
	char tmpbuf[21];
	char tmpbuf1[21];
	char tmpbuf2[6];
	printf ("%s %s %s\n", spacemd->s_token, spacemd->u_token, spacemd->poolname);
	if (spacemd->s_gid) {
		for (i = 0; i < spacemd->nbgids; i++)
			printf ("%c%s", (i == 0) ? '\t' : ',',
			    decode_group (spacemd->gids[i]));
		printf ("\n");
	} else
		printf ("\t%s\n", decode_user (spacemd->s_uid));
	printf ("\t%s %s %s %s %s\n",
	    si_flag ? u64tostrsi (spacemd->g_space, tmpbuf, 0): u64tostru (spacemd->g_space, tmpbuf, 0),
	    si_flag ? u64tostrsi (spacemd->u_space, tmpbuf1, 0): u64tostru (spacemd->u_space, tmpbuf1, 0),
	    decode_time (spacemd->r_lifetime, tmpbuf2),
	    spacemd->ret_policy == 'C' ? "CUSTODIAL" :
	    spacemd->ret_policy == 'O' ? "OUTPUT" : "REPLICA",
	    spacemd->ac_latency == 'N' ? "NEARLINE" : "ONLINE");
}

main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	int i;
	static struct Coptions longopts[] = {
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"space_token", REQUIRED_ARGUMENT, 0, OPT_S_TOKEN},
		{"token_desc", REQUIRED_ARGUMENT, 0, OPT_U_DESC},
		{"si", NO_ARGUMENT, &si_flag, 1},
		{0, 0, 0, 0}
	};
	int nbreplies = 0;
	int nbtokens = 0;
	char *s_token = NULL;
	char **s_tokens = NULL;
	struct dpm_space_metadata *spacemd = NULL;
	char *u_token = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_S_TOKEN:
			s_token = Coptarg;
			break;
		case OPT_U_DESC:
			u_token = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc) {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage:\t%s %s\t%s %s",
		    argv[0], "--space_token space_token [--si] [--help]\n",
		    argv[0], "--token_desc user_space_token_description [--si] [--help]\n");
		exit (errflg ? USERR : 0);
	}

	if (s_token) {
		if (dpm_getspacemd (1, &s_token, &nbreplies, &spacemd) < 0) {
			fprintf (stderr, "dpm-getspacemd: %s\n", sstrerror(serrno));
			exit (serrno == EINVAL ? USERR : SYERR);
		}
		listentry (spacemd);
		exit (0);
	}
	if (dpm_getspacetoken (u_token, &nbtokens, &s_tokens) < 0) {
		fprintf (stderr, "dpm-getspacemd: %s\n", sstrerror(serrno));
		for (i = 0; i < nbtokens; i++)
			free (s_tokens[i]);
		free (s_tokens);
		exit (serrno == EINVAL ? USERR : SYERR);
	}
	if (dpm_getspacemd (nbtokens, s_tokens, &nbreplies, &spacemd) < 0) {
		fprintf (stderr, "dpm-getspacemd: %s\n", sstrerror(serrno));
		free (spacemd);
		exit (serrno == EINVAL ? USERR : SYERR);
	}
	for (i = 0; i < nbtokens; i++) {
		free (s_tokens[i]);
	}
	free (s_tokens);
	for (i = 0; i < nbreplies; i++) {
		listentry (spacemd + i);
	}
	free (spacemd);
	exit (0);
}
