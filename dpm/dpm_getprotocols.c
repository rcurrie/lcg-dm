/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_getprotocols.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_getprotocols - to get the list of supported protocols */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_getprotocols (int *nb_supported_protocols, char ***supported_protocols)
{
	int c;
	char func[17];
	gid_t gid;
	int i;
	int j;
	int msglen;
	char **p;
	char protocol[CA_MAXPROTOLEN+1];
	char *rbp;
	char repbuf[256];
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_getprotocols");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! nb_supported_protocols || ! supported_protocols) {
		serrno = EFAULT;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC);
	marshall_LONG (sbp, DPM_GETPROTO);
	msglen = 5 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);

	c = send2dpm (NULL, sendbuf, msglen, repbuf, sizeof(repbuf), NULL, NULL);
	if (c == 0) { 
		rbp = repbuf;
		unmarshall_WORD (rbp, *nb_supported_protocols);
		if ((p = calloc (*nb_supported_protocols, sizeof(char *))) == NULL) {
			serrno = ENOMEM;
			return (-1);
		}
		for (i = 0; i < *nb_supported_protocols; i++) {
			unmarshall_STRING (rbp, protocol);
			if((p[i] = strdup (protocol)) == NULL) {
				for (j = 0; j < i; j++)
					free (p[j]);
				free (p);
				serrno = ENOMEM;
				return (-1);
			}
		}
		*supported_protocols = p;
	}
	return (c);
}
