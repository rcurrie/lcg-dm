.\" Copyright (C) 2007 by CNRS/IN2P3/LAL
.\" All rights reserved
.\"
.TH DPM-LISTSPACES 1 "$Date: 2009/04/20 15:08:04 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-listspaces \- Display Disk Pool Manager pools and space reservations 
.SH SYNOPSIS
.B dpm-listspaces
[
.BI --pools poolname,poolname2...
.BI --reservations token,token2...
.BI --children
.BI --parents
.BI --long
.BI --domain dpm-domain
.BI --basedir dpns-basedir
]
.SH DESCRIPTION
.B dpm-listspaces
Display Disk Pool Manager pools and space reservations and their detailed configuration. The most up-to-date
documentation can be obtained with option --help.
.SH OPTIONS
.TP
.BI --pools poolname,poolname2...
Display information only about listed pools or --all for all pools.
.TP
.BI --reservations token,token2...
Display information only about listed space reservations or --all for all reservations.
.TP
.BI --children
Display information about reservations contained in selected pools.
.TP
.BI --parents
Display information about pools containing selected reservations.
.TP
.BI --long
Detailed information (e.g. file systems in pools).
.TP
.BI --domain domain
Name of DPM domain, if not current host domain name.
.TP
.BI --basedir directory
Name of DPNS base directory, defaults to "home".
.TP
.BI --help
Online help about options
.TP
.BI --version
Program version and detailed information about this tool

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
