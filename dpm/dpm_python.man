.\" @(#)$RCSfile: dpm_python.man,v $ $Revision$ $Date$ CERN IT-GD/CT David Smith
.\" Copyright (C) 2004-2009 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH dpm_python 3 "$Date$" DPM "Python Reference"
.SH NAME
dpm \- Python interface to the DPM
.br
.SH SYNOPSIS
.B import dpm

.SH DESCRIPTION
The dpm module permits you to access the DPM client interface from python
programs. The dpm module is a swig wrapping of the standard C interface.
For detailed descriptions of each function see the individual man page of
each function.

There follows a series of examples of how to use selected functions and how
to retrieve the information returned by them: Examples are listing the
replicas of a given entry, reading the content of a directory, getting and
setting ACLs. etc.

.SH EXAMPLE
.nf
#!/usr/bin/python

"""
# Using the dpns_readdirxr method
"""

import sys
import dpm

name = "/dpm/cern.ch/home/dteam/";

dir = dpm.dpns_opendirg(name,"")
if (dir == None) or (dir == 0):
        err_num = dpm.cvar.serrno
        err_string = dpm.sstrerror(err_num)
        print "Error while looking for " + name + ": Error " + str(err_num) \\
			+ " (" + err_string + ")"
        sys.exit(1)

while 1:
        read_pt = dpm.dpns_readdirxr(dir,"")
        if (read_pt == None) or (read_pt == 0):
                break
        entry, list = read_pt
        print entry.d_name
        try:
                for i in range(len(list)):
                        print " ==> %s" % list[i].sfn
        except TypeError, x:
                print " ==> None"

dpm.dpns_closedir(dir)
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpns_getlinks method
"""

result, list = dpm.dpns_getlinks("/dpm/cern.ch/home/dteam/file.test", "")
print result
print len(list)
if (result == 0):
	for i in list:
		print i.path
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpns_getreplica method
"""

result, list = dpm.dpns_getreplica("/dpm/cern.ch/home/dteam/file.test", "", "")
print result
print len(list)
if (result == 0):
	for i in list:
		print i.host
		print i.sfn
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpns_getacl and dpns_setacl methods to add a user ACL
"""

nentries, acls_list = dpm.dpns_getacl("/dpm/cern.ch/home/dteam/file.test", \\
					dpm.CA_MAXACLENTRIES)

print nentries
print len(acls_list)

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

# When adding a first ACL for a given user, you also need to add the mask
# When adding the second user ACL, it is not necessary anymore

acl_user = dpm.dpns_acl()
acl_mask = dpm.dpns_acl()

acl_user.a_type=2		# 2 corresponds to CNS_ACL_USER
acl_user.a_id=18701		# user id
acl_user.a_perm=5

acl_mask.a_type=5		# 5 corresponds to CNS_ACL_MASK
acl_mask.a_id=0			# no user id specified
acl_mask.a_perm=5

acls_list.append(acl_user)
acls_list.append(acl_mask)

res = dpm.dpns_setacl("/dpm/cern.ch/home/dteam/file.test", acls_list)

if res == 0:
        print "OK"
else:
        err_num = dpm.cvar.serrno
        err_string = dpm.sstrerror(err_num)
        print "There was an error : Error " + str(err_num) + " (" + err_string + ")"
        sys.exit(1)
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpns_getacl and dpns_setacl methods to remove a user ACL
"""

nentries, acls_list = dpm.dpns_getacl("/dpm/cern.ch/home/dteam/file.test", \\
						dpm.CA_MAXACLENTRIES)

# Note : you cannot remove the owner ACL (i.e. for CNS_ACL_USER_OBJ type) if
# ====== ACLs for other users exist. If all the other user ACLs are deleted,
# ====== the owner ACL is automatically removed.

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

del acls_list[1]	# delete a given user ACL from the list of ACLs

res = dpm.dpns_setacl("/dpm/cern.ch/home/dteam/file.test", acls_list)

if res == 0:
        print "OK"
else:
        err_num = dpm.cvar.serrno
        err_string = dpm.sstrerror(err_num)
        print "There was an error : Error " + str(err_num) + " (" + err_string + ")"
        sys.exit(1)
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpns_getusrmap method
"""

result, list = dpm.dpns_getusrmap()
print result
print len(list)
if (result == 0):
	for i in list:
		print i.userid + " " + i.username
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpns_getgrpmap method
"""

result, list = dpm.dpns_getgrpmap()
print result
print len(list)
if (result == 0):
	for i in list:
		print i.gid + " " + i.groupname
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_addfs method
"""

result = dpm.dpm_addfs("mypool", "mydiskserver.domain.com", "/mountpoint", \\
			dpm.FS_READONLY)
print result

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_modifyfs method
"""

result = dpm.dpm_modifyfs("mydiskserver.domain.com", "/mountpoint", \\
			dpm.FS_READONLY)
print result

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_rmfs method
"""

result = dpm.dpm_rmfs("mypool", "mydiskserver.domain.com", "/mountpoint")
print result

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_addpool method
"""

dpmpool = dpm.dpm_pool()
dpmpool.poolname = "mypool"
dpmpool.defsize = 209715200
dpmpool.def_lifetime = 604800
dpmpool.defpintime = 604800
dpmpool.max_lifetime = 604800
dpmpool.max_pintime = 604800
dpmpool.nbgids = 1
dpmpool.gids = [0]
dpmpool.ret_policy = 'R'
dpmpool.s_type = 'D'

result = dpm.dpm_addpool(dpmpool)
print result

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_modifypool method
"""

dpmpool = dpm.dpm_pool()
dpmpool.poolname = "mypool"
dpmpool.defsize = 209715200
dpmpool.def_lifetime = 604800
dpmpool.defpintime = 604800
dpmpool.max_lifetime = 604800
dpmpool.max_pintime = 604800
dpmpool.nbgids = 1
dpmpool.gids = [0]
dpmpool.ret_policy = 'R'
dpmpool.s_type = 'D'

result = dpm.dpm_modifypool(dpmpool)
print result

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_rmpool method
"""

result = dpm.dpm_rmpool("mypool")
print result

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_getpoolfs method
"""

result,list = dpm.dpm_getpoolfs("mypool")
print result
print len(list)
if (result == 0):
	for i in list:
		print "POOL " + i.poolname + " SERVER " + i.server + " FS " + i.fs \\
			+ " CAPACITY " + i.capacity + " FREE " + i.free
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_getpools method
"""

result,list = dpm.dpm_getpools()
print result
print len(list)
if (result == 0):
	for i in list:
		print "POOL " + i.poolname + " CAPACITY " + i.capacity + " FREE " + i.free
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_getprotocols method
"""

result,list = dpm.dpm_getprotocols()
print result
print len(list)
if (result == 0):
	for i in list:
		print i
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_getspacemd method
"""

result, list = dpm.dpm_getspacemd(["myspacetoken"])
print result
print len(list)
if (result == 0):
	for i in list:
		print "TYPE " + i.s_type + " SPACETOKEN " i.s_token + " USERTOKEN " \\
			+ i.u_token + " TOTAL " + i.t_space + " GUARANTUEED " + i.g_space \\
			+ " UNUSED " + i.u_space + " POOL " + i.poolname
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_getspacetoken method
"""

result, list = dpm.dpm_getspacetoken("myspacetokendesc")
print result
print len(list)
if (result == 0):
	for i in list:
		print i
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_reservespace method
"""

result,actual_s_type,actual_t_space,actual_g_space,actual_lifetime,s_token = \\
		dpm.dpm_reservespace('D', "myspacetokendesc", 'R', 'O', 209715200, \\
		209715200, 2592000, 0, "mypoolname")
print result
if (result == 0):
	print "TYPE " + actual_s_type + " TOTAL " + actual_t_space + " GUARANTEED " \\
		+ actual_g_space + " LIFETIME " + actual_lifetime + " TOKEN " + s_token
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_updatespace method
"""

result,actual_t_space,actual_g_space,actual_lifetime = \\
		dpm.dpm_updatespace("myspacetoken", 209715200, 209715200, 2592000)
print result
if (result == 0):
	print " TOTAL " + actual_t_space + " GUARANTEED " + actual_g_space \\
		+ " LIFETIME " + actual_lifetime
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_releasespace method
"""

result = dpm.dpm_releasespace("myspacetoken", 0)
print result

.SH EXAMPLE
.nf
#!/usr/bin/python

import dpm

"""
# Using the dpm_ping method
"""

result,info = dpm.dpm_ping("mydpmserver.domain.com")
print result
if (result == 0):
	print info
.fi

.SH KNOWN BUGS
The current interface to the \fBdpns_getcwd(3)\fP,
\fBdpns_readlink(3)\fP, \fBdpns_seterrbuf(3)\fP requires the passing of str object which is
modified to contain the result (in a similar way to the C functions, which accept a buffer).
However this breaks the immutability of python str. This will be changed in the future.

.SH SEE ALSO
.B DPM C interface man pages
