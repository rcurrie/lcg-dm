/*
 * Copyright (C) 2005 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_freefilestatuses.c,v $ $Revision: 1.2 $ $Date: 2005/11/24 07:20:25 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_freefilestatuses - free file statuses returned by DPM APIs */

#include <stdlib.h>
#include <sys/types.h>
#include "dpm_api.h"

void DLL_DECL
dpm_free_cfilest(int nbreplies, struct dpm_copyfilestatus *filestatuses)
{
	int i;

	if (filestatuses) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->from_surl)
				free ((filestatuses+i)->from_surl);
			if ((filestatuses+i)->to_surl)
				free ((filestatuses+i)->to_surl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
	}
}

void DLL_DECL
dpm_free_filest(int nbreplies, struct dpm_filestatus *filestatuses)
{
	int i;

	if (filestatuses) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->surl)
				free ((filestatuses+i)->surl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
	}
}

void DLL_DECL
dpm_free_gfilest(int nbreplies, struct dpm_getfilestatus *filestatuses)
{
	int i;

	if (filestatuses) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->from_surl)
				free ((filestatuses+i)->from_surl);
			if ((filestatuses+i)->turl)
				free ((filestatuses+i)->turl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
	}
}

void DLL_DECL
dpm_free_pfilest(int nbreplies, struct dpm_putfilestatus *filestatuses)
{
	int i;

	if (filestatuses) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->to_surl)
				free ((filestatuses+i)->to_surl);
			if ((filestatuses+i)->turl)
				free ((filestatuses+i)->turl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
	}
}
