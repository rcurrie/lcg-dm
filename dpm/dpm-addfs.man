.\" @(#)$RCSfile: dpm-addfs.man,v $ $Revision$ $Date$ CERN Jean-Philippe Baud
.\" Copyright (C) 2004-2011 by CERN
.\" All rights reserved
.\"
.TH DPM-ADDFS 1 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpm-addfs \- add a filesystem to a disk pool
.SH SYNOPSIS
.B dpm-addfs
.BI --poolname " pool_name"
.BI --server " fs_server"
.BI --fs " fs_name"
[
.BI --st " status"
] [
.BI --weight " weight"
] [
.B --help
]
.SH DESCRIPTION
.B dpm-addfs
adds a filesystem to a disk pool.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.I pool_name
specifies the disk pool name previously defined using
.BR dpm-addpool .
.TP
.I server
specifies the host name of the disk server where this filesystem is mounted.
.TP 
.I fs
specifies the mount point of the dedicated filesystem.
.TP
.I status
Initial status of this filesystem. It can be set to 0 or
.B DISABLED
or
.BR RDONLY .
This can be either alphanumeric or the corresponding numeric value.
.TP
.I weight
specifies the weight of the filesystem. This is used during the filesystem
selection. The value must be positive.
It is recommended to use a value lower than 10. Default is 1.
.SH EXAMPLE
.nf
.ft CW
	dpm-addfs --poolname Volatile --server sehost --fs /data
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.BR dpm_addfs(3) ,
.B dpm-addpool(1)
