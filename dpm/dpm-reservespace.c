/*
 * Copyright (C) 2006-2011 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-reservespace.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-reservespace - reserve space */
#include <errno.h>
#include <sys/types.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
int help_flag;
int si_flag;
main(argc, argv)
int argc;
char **argv;
{
	char ac_latency = '\0';
	u_signed64 actual_g_space;
	time_t actual_lifetime;
	char actual_s_type;
	u_signed64 actual_t_space;
	int c;
	char *dp;
	int errflg = 0;
	gid_t gid = 0;
	char gid_str[256];
	struct group *gr;
	char *g_space_str = NULL;
	int i;
	static struct Coptions longopts[] = {
		{"ac_latency", REQUIRED_ARGUMENT, 0, OPT_AC_LATENCY},
		{"gid", REQUIRED_ARGUMENT, 0, OPT_POOL_GID},
		{"group", REQUIRED_ARGUMENT, 0, OPT_POOL_GROUP},
		{"gspace", REQUIRED_ARGUMENT, 0, OPT_GSPACE_SZ},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"lifetime", REQUIRED_ARGUMENT, 0, OPT_LIFETIME},
		{"poolname", REQUIRED_ARGUMENT, 0, OPT_POOL_NAME},
		{"ret_policy", REQUIRED_ARGUMENT, 0, OPT_RET_POLICY},
		{"si", NO_ARGUMENT, &si_flag, 1},
		{"s_type", REQUIRED_ARGUMENT, 0, OPT_S_TYPE},
		{"token_desc", REQUIRED_ARGUMENT, 0, OPT_U_DESC},
		{0, 0, 0, 0}
	};
	int nbgids = 0;
	char *p;
	char *poolname = NULL;
	u_signed64 req_g_space = 0;
	time_t req_lifetime = 0;
	char ret_policy = '\0';
	gid_t *s_gids = NULL;
	char s_token[CA_MAXDPMTOKENLEN+1];
	char s_type = '\0';
	char *u_token = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_GSPACE_SZ:
			p = Coptarg;
			while (*p >= '0' && *p <= '9') p++;
			if (! (*p == '\0' || ((*p == 'k' || *p == 'M' ||
			    *p == 'G' || *p == 'T' || *p == 'P') && *(p+1) == '\0'))) {
				fprintf (stderr,
				    "invalid size of guaranteed space %s\n", Coptarg);
				errflg++;
			} else
				g_space_str = Coptarg;
			break;
		case OPT_AC_LATENCY:
			ac_latency = *Coptarg;
			break;
		case OPT_POOL_GID:
			p = Coptarg;
			while (*p) {
				if (*p == ',') nbgids++;
				p++;
			}
			nbgids++;
			if ((s_gids = malloc (nbgids * sizeof(gid_t))) == NULL) {
				fprintf (stderr, "Could not allocate memory for gids\n");
				exit (USERR);
			}
			i = 0;
			p = strtok (Coptarg, ",");
			while (p) {
				if ((s_gids[i] = strtol (p, &dp, 10)) < 0 ||
				    *dp != '\0') {
					fprintf (stderr, "invalid gid %s\n", p);
					errflg++;
					goto next_gid;
				}
#ifdef VIRTUAL_ID
				if (s_gids[i] > 0 &&
				    Cns_getgrpbygid (s_gids[i], gid_str) < 0) {
#else
				if (s_gids[i] > 0 && ! getgrgid (s_gids[i])) {
#endif
					fprintf (stderr, "invalid gid %s\n", p);
					errflg++;
				}
next_gid:
				if (p = strtok (NULL, ",")) *(p - 1) = ',';
				i++;
			}
			break;
		case OPT_POOL_GROUP:
			p = Coptarg;
			while (*p) {
				if (*p == ',') nbgids++;
				p++;
			}
			nbgids++;
			if ((s_gids = malloc (nbgids * sizeof(gid_t))) == NULL) {
				fprintf (stderr, "Could not allocate memory for gids\n");
				exit (USERR);
			}
			i = 0;
			p = strtok (Coptarg, ",");
			while (p) {
#ifdef VIRTUAL_ID
				if (strcmp (p, "root") == 0)
					s_gids[i] = 0;
				else if (Cns_getgrpbynam (p, &s_gids[i]) < 0) {
#else
				if ((gr = getgrnam (p)))
					s_gids[i] = gr->gr_gid;
				else {
#endif
					fprintf (stderr,
					    "invalid group: %s\n", p);
					errflg++;
				}
				if (p = strtok (NULL, ",")) *(p - 1) = ',';
				i++;
			}
			break;
		case OPT_POOL_NAME:
			if (strlen (Coptarg) > CA_MAXPOOLNAMELEN) {
				fprintf (stderr,
				    "pool name too long: %s\n", Coptarg);
				errflg++;
			} else
				poolname = Coptarg;
			break;
		case OPT_LIFETIME:
			if (strcmp (Coptarg, "Inf") == 0) {
				req_lifetime = 0x7FFFFFFF;
				break;
			}
			if ((req_lifetime = strtol (Coptarg, &dp, 10)) < 0 ||
			    (*dp != '\0' && *(dp+1) != '\0')) {
				fprintf (stderr,
				    "invalid lifetime %s\n", Coptarg);
				errflg++;
				break;
			}
			switch (*dp) {
			case 'y':
				req_lifetime *= 365 * 86400;
				break;
			case 'm':
				req_lifetime *= 30 * 86400;
				break;
			case 'd':
				req_lifetime *= 86400;
				break;
			case 'h':
				req_lifetime *= 3600;
				break;
			case '\0':
				break;
			default:
				fprintf (stderr,
				    "invalid lifetime %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_RET_POLICY:
			ret_policy = *Coptarg;
			break;
		case OPT_S_TYPE:
			s_type = *Coptarg;
			break;
		case OPT_U_DESC:
			u_token = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || g_space_str == NULL) {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s%s%s%s", argv[0],
		    "--gspace size_guaranteed_space [--ac_latency name]\n",
		    "\t[--gid group_id(s)] [--group group_name(s)] [--help]\n",
		    "\t[--lifetime space_lifetime] [--poolname pool_name] [--ret_policy name]\n",
		    "\t[--s_type space_type] [--si] [--token_desc user_space_token_description]\n");
		exit (errflg ? USERR : 0);
	}
	if (si_flag)
		req_g_space = strsitou64 (g_space_str);
	else
		req_g_space = strutou64 (g_space_str);

	if (dpm_reservespace (s_type, u_token, ret_policy, ac_latency,
	    req_g_space, req_g_space, req_lifetime, nbgids, s_gids, poolname,
	    &actual_s_type, &actual_t_space, &actual_g_space, &actual_lifetime, s_token) < 0) {
		fprintf (stderr, "dpm-reservespace: %s\n", sstrerror(serrno));
		exit (USERR);
	}
	printf ("%s\n", s_token);
	exit (0);
}
