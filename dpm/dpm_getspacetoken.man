.\" @(#)$RCSfile: dpm_getspacetoken.man,v $ $Revision: 1.3 $ $Date: 2006/12/20 15:47:54 $ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2006 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH DPM_GETSPACETOKEN 3 "$Date: 2006/12/20 15:47:54 $" LCG "DPM Library Functions"
.SH NAME
dpm_getspacetoken \- get space token
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getspacetoken (const char *" u_token ,
.BI "int *" nbreplies ,
.BI "char ***" s_tokens )
.SH DESCRIPTION
.B dpm_getspacetoken
gets the list of space tokens associated with this user description.
.LP
The input argument is:
.TP
.I u_token
specifies the user provided description associated with a previous reservespace
request. If the argument is NULL, all the space tokens accessible by the user
will be listed.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of space tokens.
.TP
.I s_tokens
will be set to the address of an array of space tokens allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I nbreplies
or
.I s_tokens
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for storing the reply.
.TP
.B EINVAL
The length of the user space token description is greater than 255 or
the user space token description is unknown.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
