.\" @(#)$RCSfile: dpm-ping.man,v $ $Revision: 1.1 $ $Date: 2007/05/09 06:38:31 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH DPM-PING 1 "$Date: 2007/05/09 06:38:31 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-ping \- check if Disk Pool Manager is alive and print its version number
.SH SYNOPSIS
.B dpm-ping
[
.BI -h " server"
]
.SH DESCRIPTION
.B dpm-ping
checks if the Disk Pool Manager is alive and prints its version number.
.SH OPTIONS
.TP
.BI -h " server"
specify the Disk Pool Manager hostname.
This can also be set thru the DPM_HOST environment variable.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
