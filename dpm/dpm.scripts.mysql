#!/bin/sh
#
# dpm         Start/Stop dpm
#
# chkconfig: - 93 17
# description: DPM server daemon

# $Id$
#
# @(#)$RCSfile: dpm.scripts.mysql,v $ $Revision$ $Date$ CERN/IT/ADC/CA Jean-Damien Durand
#

#
## PLEASE USE sysconfig FILE TO CHANGE THESE ENVIRONMENT VARIABLES
#
PREFIX=/opt/lcg
GRIDMAPDIR=/etc/grid-security/gridmapdir
export GRIDMAPDIR
GRIDMAP=/etc/grid-security/grid-mapfile
export GRIDMAP
LD_LIBRARY_PATH=/opt/glite/lib:/opt/globus/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
CSEC_MECH=ID
export CSEC_MECH
DPMDAEMONLOGFILE=/var/log/dpm/log
DPMCONFIGFILE=/opt/lcg/etc/DPMCONFIG
DPMUSER=dpmmgr
DPMGROUP=dpmmgr
NB_FTHREADS=20
NB_STHREADS=20

sysname=`uname -s`

# source function library
if [ -r /etc/rc.d/init.d/functions ]; then
    . /etc/rc.d/init.d/functions
    DAEMON="daemon --check dpm"
    FAILURE=failure
    ECHO_FAILURE=echo_failure
    SUCCESS=success
    ECHO_SUCCESS=echo_success
    ECHO_END=echo
    if [ $sysname = "Linux" ]; then
        ECHOOPT=-n
    fi
else
    DAEMON=eval
    FAILURE=
    ECHO_FAILURE=
    SUCCESS=
    ECHO_SUCCESS=
    ECHOOPT=
    ECHO_END=
fi

RETVAL=0
prog="dpm"
PIDFILE=/var/run/dpm.pid
PIDDIR=/var/run
SUBSYS=/var/lock/subsys/dpm
SUBSYSDIR=/var/lock/subsys
[ -z "$SILENTSTOP" ] && SILENTSTOP=0
DPMDAEMON=$PREFIX/sbin/dpm
DPMSHUTDOWN=$PREFIX/sbin/dpm-shutdown
if [ -r $PREFIX/etc/dpm.conf ]; then
    SYSCONFIGDPMDAEMON=$PREFIX/etc/dpm.conf
elif [ -r /etc/sysconfig/dpm ]; then
    SYSCONFIGDPMDAEMON=/etc/sysconfig/dpm
elif [ -r /etc/default/dpm ]; then
    SYSCONFIGDPMDAEMON=/etc/default/dpm
fi

#
## Blindly attempt to create useful directories
#
# [ ! -d $PIDDIR ] && mkdir -p $PIDDIR > /dev/null 2>&1
# [ ! -d $SUBSYSDIR ] && mkdir -p $SUBSYSDIR > /dev/null 2>&1

if [ $sysname = "HP-UX" ]; then
    export UNIX95=1
fi
if [ $sysname = "SunOS" ]; then
    format4comm="fname"
elif [ $sysname = "Darwin" ]; then
    format4comm="ucomm"
else
    format4comm="comm"
fi

start() {
    if [ -n "$SYSCONFIGDPMDAEMON" ]; then
        #
        ## Source the configuration
        #
        . $SYSCONFIGDPMDAEMON
        if [ "${RUN_DPMDAEMON}" != "yes" ]; then
            echo $ECHOOPT "$SYSCONFIGDPMDAEMON says NO: "
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "$SYSCONFIGDPMDAEMON says NO: "
            RETVAL=0
            $ECHO_END
            exit $RETVAL
        fi
        export DPNS_HOST
        if [ -n "${ULIMIT_N}" ]; then
            ulimit -n ${ULIMIT_N}
        fi
    fi
    if [ ! -s "$DPMCONFIGFILE" ]; then
        echo $ECHOOPT "config file $DPMCONFIGFILE empty: "
        [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "config file $DPMCONFIGFILE empty: "
        RETVAL=1
    fi
    if [ $RETVAL -eq 0 ]; then
        echo $ECHOOPT "Starting $prog: "
        cd /
        mkdir -p `dirname $DPMDAEMONLOGFILE`
        chown $DPMUSER:$DPMGROUP `dirname $DPMDAEMONLOGFILE`
        ## For the DPM, umask must be set to 0
        umask 0
        if [ "${ALLOW_COREDUMP}" != "yes" ]; then
            $DAEMON "su $DPMUSER -c \"LD_LIBRARY_PATH=$LD_LIBRARY_PATH $DPMDAEMON -t $NB_FTHREADS -T $NB_STHREADS -c $DPMCONFIGFILE -l $DPMDAEMONLOGFILE\""
        else
            $DAEMON "su $DPMUSER -c \"mkdir -p $DPMUSERHOME/dpm; cd $DPMUSERHOME/dpm; hostname -f >> logstart; date >> logstart; umask 0; umask >> logstart; ulimit -c unlimited; pwd >> logstart; ulimit -c >> logstart; echo DAEMON $DPMDAEMON \`hostname -f\` >> logstart; LD_LIBRARY_PATH=$LD_LIBRARY_PATH $DPMDAEMON -t $NB_FTHREADS -T $NB_STHREADS -c $DPMCONFIGFILE -l $DPMDAEMONLOGFILE\""
        fi
        if [ $? -eq 0 ]; then
            if [ -d $SUBSYSDIR ]; then
                touch $SUBSYS
                chmod 0644 $SUBSYS
            fi    
            if [ -d $PIDDIR ]; then
                pid=`ps -eo pid,ppid,$format4comm | grep " 1 dpm$" | awk '{print $1}'`
                # The point of $PIDFILE is that it kills only
                # the master daemon.
                rm -f $PIDFILE
                echo $pid > $PIDFILE
                chmod 0644 $PIDFILE
            fi
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Starting $prog: "
            RETVAL=0
        else
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Starting $prog: "
            RETVAL=1
        fi
    fi
}

case "$1" in
  start)
    DPMUSERHOME=`finger $DPMUSER | grep -i directory: | cut -f 2 -d' '`
    X509_USER_CERT=/etc/grid-security/$DPMUSER/dpmcert.pem
    export X509_USER_CERT
    X509_USER_KEY=/etc/grid-security/$DPMUSER/dpmkey.pem
    export X509_USER_KEY

    pid=`ps -eo pid,ppid,$format4comm | grep " 1 dpm$" | awk '{print $1}'`
    if [ -n "$pid" ]; then
        echo $ECHOOPT "dpm (pid $pid) already started:"
        $ECHO_SUCCESS
        RETVAL=0
    else
        netstat -an | egrep '[:.]5015 ' | egrep 'LISTEN *$' > /dev/null
        port_in_use="$?" # 0 is true, > 0 is false

        if [ -f $PIDFILE ]; then
            if [ $port_in_use -eq 0 ]; then 
                echo $ECHOOPT "dpm port in use:"
                $ECHO_FAILURE
                RETVAL=1
            else
                start
            fi
        else
            if [ $port_in_use -eq 0 ]; then 
                echo $ECHOOPT "dpm port in use:"
                $ECHO_FAILURE
                RETVAL=1
            else
                start
            fi
        fi
    fi
    $ECHO_END
    ;;

  stop)
    if [ -f $PIDFILE ]; then
        echo $ECHOOPT "Stopping $prog: "
        if [ -x $DPMSHUTDOWN ]; then
            $DPMSHUTDOWN -f -h `hostname` > /dev/null 2>&1
            RETVAL=$?
            if [ $RETVAL -ne 0 ]; then
                kill -2 -`cat $PIDFILE` > /dev/null 2>&1
                RETVAL=$?
            fi
        else
            kill -2 -`cat $PIDFILE` > /dev/null 2>&1
            RETVAL=$?
        fi
        if [ $RETVAL -eq 0 ]; then
            rm -f $PIDFILE
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
        else
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
        fi
    else
        # dpm might have been started by hand
        pid=`ps -eo pid,ppid,$format4comm | grep " 1 dpm$" | awk '{print $1}'`
        if [ -n "$pid" ]; then
            echo $ECHOOPT "Stopping $prog: "
            if [ -x $DPMSHUTDOWN ]; then
                $DPMSHUTDOWN -f -h `hostname` > /dev/null 2>&1
                RETVAL=$?
                if [ $RETVAL -ne 0 ]; then
                    kill -2 -$pid > /dev/null 2>&1
                    RETVAL=$?
                fi
            else
                kill -2 -$pid > /dev/null 2>&1
                RETVAL=$?
            fi
            if [ $RETVAL -eq 0 ]; then
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
            else
                [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
            fi
        else
            echo $ECHOOPT "dpm already stopped: "
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "dpm already stopped: "
            [ $SILENTSTOP -eq 0 ] && RETVAL=0 || RETVAL=1
        fi
    fi

    lpid="X"
    while [ "x$lpid" != "x" ]; do
        sleep 1
        #Does not seem to work on SunOS ;-)
        lpid=`ps -eo pid,ppid,$format4comm | grep " 1 dpm$" | awk '{print $1}'`
    done

    [ -d $SUBSYSDIR ] && rm -f $SUBSYS
    $ECHO_END
    ;;
  restart | force-reload)
    $0 stop
    if [ $? -eq 0 -o $SILENTSTOP -eq 0 ]; then
        sleep 1
        $0 start
        RETVAL=$?
    else
        RETVAL=0
    fi
    ;;
  condrestart | try-restart)
    SILENTSTOP=1
    export SILENTSTOP
    $0 restart
    RETVAL=$?
    ;;
  reload)
    ;;
  status)
    pid=`ps -eo pid,ppid,$format4comm | grep " 1 dpm$" | awk '{print $1}'`
    if [ -n "$pid" ]; then
        echo $ECHOOPT "dpm (pid $pid) is running..."
        $ECHO_SUCCESS
        $ECHO_END
        RETVAL=0
    else
        if [ -f $PIDFILE ]; then
            pid=`head -1 $PIDFILE`
            if [ "$pid" != "" ] ; then
                echo $ECHOOPT "dpm dead but pid file exists"
                $ECHO_FAILURE
                $ECHO_END
                RETVAL=1
            else
                echo $ECHOOPT "dpm dead"
                $ECHO_FAILURE
                $ECHO_END
                RETVAL=1
            fi
        else
            if [ -f $SUBSYS ]; then
                echo $ECHOOPT "dpm dead but subsys ($SUBSYS) locked"
                RETVAL=2
            else
                echo $ECHOOPT "dpm is stopped"
                RETVAL=3
            fi
            $ECHO_FAILURE
            $ECHO_END
        fi
    fi
    ;;
  *)
    echo "Usage: $0 {start|stop|status|restart|condrestart}"
    RETVAL=1
    ;;
esac

exit $RETVAL
