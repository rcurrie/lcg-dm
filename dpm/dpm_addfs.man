.\" @(#)$RCSfile: dpm_addfs.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2011 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_ADDFS 3 "$Date$" LCG "DPM Library Functions"
.SH NAME
dpm_addfs \- add a filesystem to a disk pool
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_addfs (char *" poolname ,
.BI "char *" server ,
.BI "char *" fs ,
.BI "int " status ,
.BI "int " weight )
.SH DESCRIPTION
.B dpm_addfs
adds a filesystem to a disk pool.
.TP
.I poolname
specifies the disk pool name previously defined using
.BR dpm_addpool .
.TP
.I server
specifies the host name of the disk server where this filesystem is mounted.
.TP
.I fs
specifies the mount point of the dedicated filesystem.
.TP
.I status
Initial status of this filesystem. It can be set to 0 or
.B FS_DISABLED
or
.BR FS_RDONLY .
.TP
.I weight
specifies the weight of the filesystem. This is used during the filesystem
selection. The value must be positive. A negative value will tell the server to
allocate the default weight value (1).
It is recommended to use a value lower than 10.
.LP
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
Filesystem does not exist.
.TP
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B EFAULT
.IR poolname ,
.I server
or
.I fs
is a NULL pointer.
.TP
.B EEXIST
this filesystem is already part of a pool.
.TP
.B ENOMEM
Memory could not be allocated for storing the filesystem definition.
.TP
.B EINVAL
The pool is unknown or the length of
.I poolname
exceeds
.B CA_MAXPOOLNAMELEN
or the length of
.I server
exceeds
.B CA_MAXHOSTNAMELEN
or the length of
.I fs
exceeds 79.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
