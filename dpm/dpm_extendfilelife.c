/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_extendfilelife.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_extendfilelife - extend file lifetime */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_extendfilelife (char *r_token, char *surl, time_t lifetime, time_t *actual_lifetime)
{
	int c;
	char errstring[256];
	char func[19];
	gid_t gid;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[4+256+8];
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_extendfilelife");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! surl) {
		serrno = EFAULT;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC);
	marshall_LONG (sbp, DPM_EXTENDLIFE);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	if (r_token) {
		marshall_STRING (sbp, r_token);
	} else {
		marshall_STRING (sbp, "");
	}
	marshall_STRING (sbp, surl);
	marshall_TIME_T (sbp, lifetime);

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2dpm (NULL, sendbuf, msglen, repbuf, sizeof(repbuf), NULL, NULL);
	if (c && serrno == SENAMETOOLONG) serrno = ENAMETOOLONG;
	if (c == 0) { 
		rbp = repbuf;
		unmarshall_LONG (rbp, c);
		if ((c & 0xF000) == DPM_FAILED) {
			serrno = c - DPM_FAILED;
			c = -1;
		}
		unmarshall_STRING (rbp, errstring);
		if (*errstring)
			dpm_errmsg (func, "%s\n", errstring);
		if (c == 0 && actual_lifetime)
			unmarshall_TIME_T (rbp, *actual_lifetime);
	}
	return (c);
}
