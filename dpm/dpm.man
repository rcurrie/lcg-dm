.\" @(#)$RCSfile: dpm.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 1999-2011 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM 8 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpm \- start the Disk Pool Manager
.SH SYNOPSIS
.B dpm
[
.BI -c " config_file"
] [
.BI -l " log_file"
] [
.BI -t " nbfthreads"
] [
.BI -T " nbsthreads"
] [
.BI -4
|
.BI -6
]
.SH DESCRIPTION
.LP
The
.B dpm
command starts the Disk Pool Manager.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will read the Disk Pool Manager "request" database configuration file,
create a pool of threads and look for requests.
Each of them is processed in a thread which opens a connection to the
database server if necessary.
When a request has been completed, the thread becomes idle until it is allocated
to another request.
The connection to the database server is kept open between 2 requests.
If the Disk Pool Manager is being shutdown or not active, the requests are
automatically retried by the client API.
.LP
All error messages and statistical information are kept in a log.
.LP
The Disk Pool Manager listen port number can be defined on client hosts and
on the server itself in either of the following ways:
.RS
.LP
setting an environment variable DPM_PORT
.RS
.HP
setenv DPM_PORT 5015
.RE
.LP
an entry in
.B /etc/shift.conf
like:
.RS
.HP
DPM	PORT	5015
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of DPM_PORT in dpm_constants.h.
.LP
The Disk Pool Manager host name can be defined on client hosts
in either of the following ways:
.RS
.LP
setting an environment variable DPM_HOST, for example:
.RS
.HP
setenv DPM_HOST sehost
.RE
.LP
an entry in
.B /etc/shift.conf
for example:
.RS
.HP
DPM	HOST	sehost
.RE
.RE
.LP
If none of these methods is used, the default host is taken from the
definition of DpmHost in site.def.
.LP
The Disk Pool Manager "request" database keeps the requests and their status
even after completion.
By default, the requests are kept for ever, but an entry in
.B /etc/shift.conf
can set the retention period for the requests:
.RS
.HP
DPM	REQCLEAN	req_retention
.RE
.sp
where req_retention can be "Inf" (for infinite) or expressed in years (suffix 'y'),
months (suffix 'm'), days (suffix 'd'), hours (suffix 'h') or seconds (no suffix).
.LP
The Disk Pool Manager configuration file contains password information for the
database and must be readable/writable only by root.
It contains a single line in the format:
.HP
.RS
username/password@server
.RE
or
.RS
username/password@server/dbname
.RE
.sp
where 'username' and 'password' are the credentials to login to the database
instance identified by 'server'. If 'dbname' is not specified, "dpm_db" is used.
.LP
In the log each entry has a timestamp.
All entries corresponding to one request have the same request id.
For each user command there is one message DP092 giving information about
the requestor (hostname, uid, gid) and one message DP098 giving the command
itself.
The completion code of the command is also logged.
.SH OPTIONS
.TP
.BI -c " config_file"
Specifies a different path for the Disk Pool Manager configuration file.
.TP
.BI -l " log_file"
Specifies a different path for the Disk Pool Manager log file.
The special value
.B syslog
will send the log messages to the system logger syslogd.
.TP
.BI -t " nbfthreads"
specifies the number of fast threads. The number of fast threads + the number of
slow threads + the number of disk pools + 2 must be less than DPM_MAXNBTHREADS
(currently 100). Default is 20.
.TP
.BI -T " nbsthreads"
specifies the number of slow threads. The number of fast threads + the number of
slow threads + the number of disk pools + 2 must be less than DPM_MAXNBTHREADS
(currently 100). Default is 20.
.TP
.BI -4
only try to listen on IPv4 addresses
.TP
.BI -6
only try to listen on IPv6 addresses
.SH FILES
.TP 1.5i
.B /etc/DPMCONFIG
configuration file
.TP
.B /var/log/dpm/log
.SH EXAMPLES
.TP
Here is a small log:
.nf
11/23 07:06:17 24395 dpm_serv: started
11/23 07:06:17 24395 dpm_serv: Supported protocols are: rfio gsiftp
11/23 07:06:17 24395 dpm_getpoolconf: getting existing disk pool configuration from the DB
12/06 07:09:11 18163,1 dpm_srv_put: DP092 - put request by 395,1028 from lxb0722.cern.ch
12/06 07:09:15 18163,1 dpm_srv_put: DP098 - put 2 59d0946e-1063-4a78-92f3-687a11efc1f6
12/06 07:09:15 18163,1 dpm_srv_put: DP098 - put 0 /dpm/dteam/testjpb22
.fi
.SH SEE ALSO
.BR Clogit(3) ,
.B dpm-shutdown(1)
