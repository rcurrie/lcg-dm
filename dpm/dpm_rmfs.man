.\" @(#)$RCSfile: dpm_rmfs.man,v $ $Revision: 1.2 $ $Date: 2006/09/11 05:40:30 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_RMFS 3 "$Date: 2006/09/11 05:40:30 $" LCG "DPM Library Functions"
.SH NAME
dpm_rmfs \- remove a filesystem from a disk pool definition
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_rmfs (char *" server ,
.BI "char *" fs )
.SH DESCRIPTION
.B dpm_rmfs
removes a filesystem from a disk pool definition.
.TP
.I server
specifies the host name of the disk server where this filesystem is mounted.
.TP
.I fs
specifies the mount point of the dedicated filesystem.
.LP
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
Filesystem does not exist.
.TP
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B EFAULT
.I server
or
.I fs
is a NULL pointer.
.TP
.B EINVAL
The length of
.I server
exceeds
.B CA_MAXHOSTNAMELEN
or the length of
.I fs
exceeds 79.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
