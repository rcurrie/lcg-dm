.\" @(#)$RCSfile: dpm_rm.man,v $ $Revision: 1.1.1.1 $ $Date: 2004/12/15 08:31:49 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_RM 3 "$Date: 2004/12/15 08:31:49 $" LCG "DPM Library Functions"
.SH NAME
dpm_rm \- delete a set of files
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_rm (int " nbsurls ,
.BI "char **" surls ,
.BI "int *" nbreplies ,
.BI "struct dpm_filestatus **" filestatuses )
.SH DESCRIPTION
.B dpm_rm
deletes a set of files from disk and from the name space.
.LP
The input arguments are:
.TP
.I nbsurls
specifies the number of files belonging to the request.
.TP
.I surls
specifies the array of file names.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of file statuses.
.TP
.I filestatuses
will be set to the address of an array of dpm_filestatus structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_filestatus {
	char		*surl;
	int		status;
	char		*errstring;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.IR surls ,
.I nbreplies
or
.I filestatuses
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request.
.TP
.B EINVAL
.I nbsurls
is not strictly positive or all file requests have errors.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
