.\" @(#)$RCSfile: dpm-drain.man,v $ $Revision$ $Date$ CERN Jean-Philippe Baud
.\" Copyright (C) 2006-2010 by CERN
.\" All rights reserved
.\"
.TH DPM-DRAIN 1 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpm-drain \- drain a component of the Light Weight Disk Pool Manager
.SH SYNOPSIS
.B dpm-drain
.BI --poolname " pool_name"
[
.BI --server " fs_server"
] [
.BI --gid " gids"
] [
.BI --group " groups"
] [
.BI --size " amount_to_drain"
]
.LP
.B dpm-drain
.BI --server " fs_server"
[
.BI --gid " gids"
] [
.BI --group " groups"
] [
.BI --size " amount_to_drain"
]
.LP
.B dpm-drain
.BI --server " fs_server"
.BI --fs " fs_name"
[
.BI --gid " gids"
] [
.BI --group " groups"
] [
.BI --size " amount_to_drain"
]
.LP
.B dpm-drain
.B --help
.SH DESCRIPTION
.B dpm-drain
drains a component of the Light Weight Disk Pool Manager: a filesystem, a disk
server or a pool, optionally filtered by a list of gids/groups. It replicates
files which belong to the given list of gids/groups (if specified) to another
filesystem and removes the original file. It gives an error message for
files still pinned. The pool name, the disk server name or the
filesystem name can be specified.
.LP
.BR Volatile
files which have expired
will not be copied and will be 
.BR deleted
during the draining process.
.LP
If no specfic file server and file system is specified, or no limit is placed
on the files to drain, then the involved active file system(s) will have their
status changed to
.B RDONLY.
If any of the involved file systems are marked as
.B DISABLED
they will not have their status changed, however an attempt will still be made
to replicate the files from them and, if successful, delete the original.
.LP
Files which are to be moved and belong to a space will be moved to a different location
(e.g. file system or disk server) but remain in the same space. If there is no
alternate location within the same space an error will be reported for the file
and it will not be drained.
.LP
Replicas of files with a lifetime will also have lifetime at least as long
as the original. If this is not possible (for example available pools have
a maximum lifetime too short) then an error is given and the file will not
be drained.
.LP
This command requires ADMIN privilege.
.TP
.I pool_name
specifies the disk pool name previously defined using
.BR dpm-addpool .
.TP
.I server
specifies the host name of the disk server to be drained.
.TP
.I fs
specifies the mount point of the dedicated filesystem.
.TP
.I gid
specifies the comma separated list of gids to which the files must belong.
.TP
.I group
specifies the comma separated list of groups to which the files must belong.
.TP
.I size
specifies the minimum number of bytes to drain. The number may be post-fixed
with 'k', 'M', 'G' or 'T' for kilobyte, Megabyte, Gigabyte and Terabyte respectively.
Draining is halted once the desired target has been reached or passed.
If size is not specified or is given as zero dpm-drain will drain all eligible files.
.SH EXAMPLE
.nf
.ft CW
	setenv DPM_HOST dpmhost
	setenv DPNS_HOST dpnshost
.sp
	dpm-drain --poolname Permanent
.sp
	dpm-drain --server lxb1921.cern.ch
.sp
	dpm-drain --server lxb1921.cern.ch --fs /storage

.sp
	dpm-drain --poolname Permanent --gid 105
.sp
	dpm-drain --server lxb1921.cern.ch --group dteam
.sp
	dpm-drain --server lxb1921.cern.ch --gid 105,104
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.B dpm-qryconf(1)
