/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-register.c,v $ $Revision: 1.2 $ $Date: 2008/01/07 10:38:38 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-register - register external files in DPNS */

#include <sys/types.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Cgetopt.h"
#include "dpm_constants.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
int help_flag;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	int errflg = 0;
	struct group *gr;
	static struct Coptions longopts[] = {
		{"filesize", REQUIRED_ARGUMENT, 0, 'S'},
		{"group", REQUIRED_ARGUMENT, 0, 'g'},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"mode", REQUIRED_ARGUMENT, 0, 'm'},
		{"pfn", REQUIRED_ARGUMENT, 0, 'p'},
		{"server", REQUIRED_ARGUMENT, 0, 'h'},
		{"status", REQUIRED_ARGUMENT, 0, OPT_STATUS},
		{"user", REQUIRED_ARGUMENT, 0, 'u'},
		{0, 0, 0, 0}
	};
	mode_t filemode = 0664;
	u_signed64 filesize = -1;
	gid_t ownergid = -1;
	uid_t owneruid = -1;
	char *p;
	char *pfn = NULL;
	struct passwd *pwd;
	char *server = NULL;
	char status = '-';

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "m:", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_STATUS:
			status = *Coptarg;
			break;
		case 'g':
#ifdef VIRTUAL_ID
			if (strcmp (Coptarg, "root") == 0)
				ownergid = 0;
			else if (Cns_getgrpbynam (Coptarg, &ownergid) < 0) {
#else
			if ((gr = getgrnam (Coptarg)))
				ownergid = gr->gr_gid;
			else {
#endif
				fprintf (stderr, "invalid group: %s\n", Coptarg);
				errflg++;
			}
			break;
		case 'h':
			if (strlen (Coptarg) > CA_MAXHOSTNAMELEN) {
				fprintf (stderr, "invalid server: %s\n", Coptarg);
				errflg++;
			} else
				server = Coptarg;
			break;
		case 'm':
			filemode = strtol (Coptarg, &dp, 8);
			if (*dp != '\0') {
				fprintf (stderr, "invalid value for option -m\n");
				errflg++;
			}
			break;
		case 'p':
			if (strlen (Coptarg) > CA_MAXSFNLEN) {
				fprintf (stderr, "invalid pfn: %s\n", Coptarg);
				errflg++;
			} else
				pfn = Coptarg;
			break;
		case 'S':
			p = Coptarg;
			while (*p >= '0' && *p <= '9') p++;
			if (*p != '\0') {
				fprintf (stderr, "invalid filesize: %s\n", Coptarg);
				errflg++;
			} else
				filesize = strtou64 (Coptarg);
			break;
		case 'u':
#ifdef VIRTUAL_ID
			if (strcmp (Coptarg, "root") == 0)
				owneruid = 0;
			else if (Cns_getusrbynam (Coptarg, &owneruid) < 0) {
#else
			if ((pwd = getpwnam (Coptarg)))
				owneruid = pwd->pw_uid;
			else {
#endif
				fprintf (stderr, "invalid user: %s\n", Coptarg);
				errflg++;
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind != argc - 1 || filesize < 0 || ! server || ! pfn)
		errflg++;
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s%s%s", argv[0],
		    "--filesize filesize [--group group_name] [--help]\n",
		    "\t[--mode filemode] --pfn pfn --server hostname [--st status]\n",
		    "\t[--user file_owner] filename\n");
		exit (errflg ? USERR : 0);
	}
	if (strlen (argv[Coptind]) > CA_MAXPATHLEN) {
		fprintf (stderr, "invalid filename: %s\n", argv[Coptind]);
		exit (USERR);
	}

	if (dpm_register (argv[Coptind], filemode, owneruid, ownergid, filesize,
	    server, pfn, status) < 0) {
		fprintf (stderr, "dpm-register %s: %s\n", argv[Coptind], sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
