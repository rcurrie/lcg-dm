.\" @(#)$RCSfile: dpm_abortreq.man,v $ $Revision: 1.2 $ $Date: 2006/03/23 06:12:41 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_ABORTREQ 3 "$Date: 2006/03/23 06:12:41 $" LCG "DPM Library Functions"
.SH NAME
dpm_abortreq \- abort a given get, put or copy request
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_abortreq (char *" r_token )
.SH DESCRIPTION
.B dpm_abortreq
aborts a given get, put or copy request.
The request status and the status of every file belonging to this request and
still in the queue is set to
.BR DPM_ABORTED .
.TP
.I r_token
specifies the token returned by a previous get, put or copy request.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EACCES
Permission denied.
.TP
.B EFAULT
.I r_token
is a NULL pointer.
.TP
.B EINVAL
The length of
.I r_token
exceeds
.B CA_MAXDPMTOKENLEN
or the token is unknown.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
