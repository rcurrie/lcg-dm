/*
 * Copyright (C) 2004-2008 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_procsubr.c,v $ $Revision: 1.8 $ $Date: 2008/10/27 11:16:13 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <sys/types.h>
#include "dpm.h"
#include "dpm_server.h"
#include "dpm_util.h"
#include "dpns_api.h"
#include "serrno.h"

dpm_getonereqsummary (thip, r_token, r_type, r_status, nbreqfiles, nb_queued, nb_progress, nb_failed)
struct dpm_srv_thread_info *thip;
char *r_token;
char *r_type;
int *r_status;
int *nbreqfiles;
int *nb_queued;
int *nb_progress;
int *nb_failed;
{
	int bol;
	int c;
	struct dpm_copy_filereq cpr_entry;
	DBLISTPTR dblistptr;
	struct dpm_req dpm_req;
	struct dpm_get_filereq gfr_entry;
	struct dpm_put_filereq pfr_entry;

	if (dpm_get_pending_req_by_token (&thip->dbfd, r_token, &dpm_req, 0, NULL) < 0 &&
	    dpm_get_req_by_token (&thip->dbfd, r_token, &dpm_req, 0, NULL) < 0)
		return (-1);
	bol = 1;
	*r_type = dpm_req.r_type;
	*r_status = dpm_req.status;
	*nbreqfiles = dpm_req.nbreqfiles;
	*nb_queued = 0;
	*nb_progress = 0;
	*nb_failed = 0;
	switch (dpm_req.r_type) {
	case 'C':
		while ((c = dpm_list_cpr_entry (&thip->dbfd, bol, r_token,
		    &cpr_entry, 0, NULL, 0, &dblistptr)) == 0) {
			bol = 0;
			if (cpr_entry.status == DPM_QUEUED)
				(*nb_queued)++;
			else if (cpr_entry.status == DPM_ACTIVE)
				(*nb_progress)++;
			else if (((cpr_entry.status & 0xF000) == DPM_FAILED) ||
			    cpr_entry.status == DPM_ABORTED)
				(*nb_failed)++;
		}
		(void) dpm_list_cpr_entry (&thip->dbfd, bol, r_token,
		    &cpr_entry, 0, NULL, 1, &dblistptr);
		break;
	case 'B':
	case 'G':
		while ((c = dpm_list_gfr_entry (&thip->dbfd, bol, r_token,
		    &gfr_entry, 0, NULL, 0, &dblistptr)) == 0) {
			bol = 0;
			if (gfr_entry.status == DPM_QUEUED)
				(*nb_queued)++;
			else if (gfr_entry.status == DPM_ACTIVE)
				(*nb_progress)++;
			else if (((gfr_entry.status & 0xF000) == DPM_FAILED) ||
			    gfr_entry.status == DPM_ABORTED)
				(*nb_failed)++;
		}
		(void) dpm_list_gfr_entry (&thip->dbfd, bol, r_token,
		    &gfr_entry, 0, NULL, 1, &dblistptr);
		break;
	case 'P':
		while ((c = dpm_list_pfr_entry (&thip->dbfd, bol, r_token,
		    &pfr_entry, 0, NULL, 0, &dblistptr)) == 0) {
			bol = 0;
			if (pfr_entry.status == DPM_QUEUED)
				(*nb_queued)++;
			else if (pfr_entry.status == DPM_ACTIVE)
				(*nb_progress)++;
			else if (((pfr_entry.status & 0xF000) == DPM_FAILED) ||
			    pfr_entry.status == DPM_ABORTED)
				(*nb_failed)++;
		}
		(void) dpm_list_pfr_entry (&thip->dbfd, bol, r_token,
		    &pfr_entry, 0, NULL, 1, &dblistptr);
		break;
	}
	return (0);
}

dpm_relonefile (thip, gfr_entry, rec_addr, status)
struct dpm_srv_thread_info *thip;
struct dpm_get_filereq *gfr_entry;
dpm_dbrec_addr *rec_addr;
int *status;
{
	time_t t1;

	gfr_entry->lifetime = 0;
	if (gfr_entry->status < DPM_FAILED)
		gfr_entry->status = DPM_RELEASED;
	if (dpm_update_gfr_entry (&thip->dbfd, rec_addr, gfr_entry) < 0) {
		*status = DPM_FAILED | serrno;
		return (-1);
	}
	if (! *gfr_entry->pfn) {
		*status = DPM_SUCCESS;
		return (0);
	}
	if (dpm_get_max_get_lifetime (&thip->dbfd, gfr_entry->pfn, &t1) < 0)
		t1 = 0;
	if (Cns_setptime (gfr_entry->pfn, t1) < 0) {
		*status = DPM_FAILED | serrno;
		return (-1);
	}
	*status = DPM_RELEASED;
	return (0);
}
