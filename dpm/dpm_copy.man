.\" @(#)$RCSfile: dpm_copy.man,v $ $Revision: 1.2 $ $Date: 2006/12/20 15:21:57 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_COPY 3 "$Date: 2006/12/20 15:21:57 $" LCG "DPM Library Functions"
.SH NAME
dpm_copy \- copy a set of existing files
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_copy (int " nbreqfiles ,
.BI "struct dpm_copyfilereq *" reqfiles ,
.BI "char *" u_token ,
.BI "int " flags ,
.BI "time_t " retrytime ,
.BI "char *" r_token ,
.BI "int *" nbreplies ,
.BI "struct dpm_copyfilestatus **" filestatuses )
.SH DESCRIPTION
.B dpm_copy
copies a set of existing files.
.LP
The input arguments are:
.TP
.I nbreqfiles
specifies the number of files belonging to the request.
.TP
.I reqfiles
specifies the array of file requests (dpm_copyfilereq structures).
.PP
.nf
.ft CW
struct dpm_copyfilereq {
	char		*from_surl;
	char		*to_surl;
	time_t	lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	char		ac_latency;
	int		flags;
};
.ft
.fi
.TP
.I u_token
specifies the user provided description associated with the request.
.TP
.I flags
if set to non zero, it allows to overwrite an existing file.
.TP
.I retrytime
This field is currently ignored.
.LP
The output arguments are:
.TP
.I r_token
Address of a buffer to receive the system allocated token.
The buffer must be at least CA_MAXDPMTOKENLEN+1 characters long.
.TP
.I nbreplies
will be set to the number of replies in the array of file statuses.
.TP
.I filestatuses
will be set to the address of an array of dpm_copyfilestatus structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_copyfilestatus {
	char		*from_surl;
	char		*to_surl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t	f_lifetime;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.IR reqfiles ,
.IR r_token ,
.I nbreplies
or
.I filestatuses
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request.
.TP
.B EINVAL
.I nbreqfiles
is not strictly positive, the length of the user request description is greater
than 255 or all file requests have errors.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
