/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-rmpool.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-rmpool - remove a disk pool definition */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "serrno.h"
int help_flag;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	static struct Coptions longopts[] = {
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"poolname", REQUIRED_ARGUMENT, 0, OPT_POOL_NAME},
		{0, 0, 0, 0}
	};
	char *p;
	char *poolname = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_POOL_NAME:
			if (strlen (Coptarg) > CA_MAXPOOLNAMELEN) {
				fprintf (stderr,
				    "pool name too long: %s\n", Coptarg);
				errflg++;
			} else
				poolname = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || poolname == NULL || *poolname == '\0') {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "--poolname pool_name [--help]\n");
		exit (errflg ? USERR : 0);
	}

	if (dpm_rmpool (poolname) < 0) {
		fprintf (stderr, "dpm-rmpool %s: %s\n", poolname,
		    (serrno == ENOENT) ? "No such pool" : 
		    (serrno == EEXIST) ? "Spaces still associated" : sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
