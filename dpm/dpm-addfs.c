/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-addfs.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-addfs - add a filesystem to a disk pool */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "serrno.h"
int help_flag;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	int errflg = 0;
	char *fs = NULL;
	static struct Coptions longopts[] = {
		{"fs", REQUIRED_ARGUMENT, 0, OPT_FS},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"poolname", REQUIRED_ARGUMENT, 0, OPT_POOL_NAME},
		{"server", REQUIRED_ARGUMENT, 0, OPT_FS_SERVER},
		{"status", REQUIRED_ARGUMENT, 0, OPT_STATUS},
		{"weight", REQUIRED_ARGUMENT, 0, OPT_WEIGHT},
		{0, 0, 0, 0}
	};
	char *p;
	char *poolname = NULL;
	char *server = NULL;
	int status = 0;
	char statusa[50];
	int weight = -1;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_FS:
			if (strlen (Coptarg) > 79) {
				fprintf (stderr,
				    "filesystem name too long: %s\n", Coptarg);
				errflg++;
			} else
				fs = Coptarg;
			break;
		case OPT_FS_SERVER:
			if (strlen (Coptarg) > CA_MAXHOSTNAMELEN) {
				fprintf (stderr,
				    "server name too long: %s\n", Coptarg);
				errflg++;
			} else
				server = Coptarg;
			break;
		case OPT_POOL_NAME:
			if (strlen (Coptarg) > CA_MAXPOOLNAMELEN) {
				fprintf (stderr,
				    "pool name too long: %s\n", Coptarg);
				errflg++;
			} else
				poolname = Coptarg;
			break;
		case OPT_STATUS:
			if (isdigit (*Coptarg)) {
				if ((status = strtol (Coptarg, &dp, 10)) < 0 ||
				    *dp != '\0') {
					fprintf (stderr,
					    "invalid status %s\n", Coptarg);
					errflg++;
				}
			} else {
				if (strlen (Coptarg) >= sizeof (statusa)) {
					fprintf (stderr,
					    "invalid status %s\n", Coptarg);
					errflg++;
					break;
				}
				status = 0;
				strcpy (statusa, Coptarg);
				p = strtok (statusa, "|");
				while (p) {
					if (strcmp (p, "DISABLED") == 0)
						status |= FS_DISABLED;
					else if (strcmp (p, "RDONLY") == 0)
						status |= FS_RDONLY;
					else {
						fprintf (stderr,
						    "invalid status %s\n", Coptarg);
						errflg++;
						break;
					}
					p = strtok (NULL, "|");
				}
			}
			break;
		case OPT_WEIGHT:
			if ((weight = strtol (Coptarg, &dp, 10)) < 0 ||
			    *dp != '\0' || weight < 0) {
				fprintf (stderr,
				    "invalid weight %s\n", Coptarg);
				errflg++;
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || poolname == NULL || server == NULL || fs == NULL) {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "--poolname pool_name --server fs_server --fs fs_name [--st status] [--weight weight] [--help]\n");
		exit (errflg ? USERR : 0);
	}

	if (dpm_addfs (poolname, server, fs, status, weight) < 0) {
		fprintf (stderr, "dpm-addfs %s %s %s: %s\n", poolname, server,
		    fs, (serrno == EEXIST) ? "Filesystem already part of a pool" : sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
