.\" @(#)$RCSfile: dpm-modifyfs.man,v $ $Revision$ $Date$ CERN Jean-Philippe Baud
.\" Copyright (C) 2004-2011 by CERN
.\" All rights reserved
.\"
.TH DPM-MODIFYFS 1 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpm-modifyfs \- modify the parameters of a disk pool filesystem
.SH SYNOPSIS
.B dpm-modifyfs
.BI --server " fs_server"
.BI --fs " fs_name"
[
.BI --st " status"
] [
.BI --weight " weight"
] [
.B --help
]
.SH DESCRIPTION
.B dpm-modifyfs
modifies the parameters of a disk pool filesystem.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.I server
specifies the host name of the disk server where this filesystem is mounted.
.TP 
.I fs
specifies the mount point of the dedicated filesystem.
.TP
.I status
New status of this filesystem. It can be set to 0 (enabled read/write) or
.B DISABLED
or
.BR RDONLY .
This can be either alphanumeric or the corresponding numeric value.
.TP
.I weight
specifies the weight of the filesystem. This is used during the filesystem
selection. The value must be positive.
It is recommended to use a value lower than 10.
.SH EXAMPLE
.nf
.ft CW
	dpm-modifyfs --server sehost --fs /data --st RDONLY
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.B dpm_modifyfs(3)
