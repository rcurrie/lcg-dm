/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-rmfs.c,v $ $Revision: 1.3 $ $Date: 2007/01/09 10:12:30 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm-rmfs - remove a filesystem from a disk pool definition */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "serrno.h"
int help_flag;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	char *fs = NULL;
	static struct Coptions longopts[] = {
		{"fs", REQUIRED_ARGUMENT, 0, OPT_FS},
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"server", REQUIRED_ARGUMENT, 0, OPT_FS_SERVER},
		{0, 0, 0, 0}
	};
	char *server = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_FS:
			if (strlen (Coptarg) > 79) {
				fprintf (stderr,
				    "filesystem name too long: %s\n", Coptarg);
				errflg++;
			} else
				fs = Coptarg;
			break;
		case OPT_FS_SERVER:
			if (strlen (Coptarg) > CA_MAXHOSTNAMELEN) {
				fprintf (stderr,
				    "server name too long: %s\n", Coptarg);
				errflg++;
			} else
				server = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || server == NULL || fs == NULL) {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "--server fs_server --fs fs_name [--help]\n");
		exit (errflg ? USERR : 0);
	}

	if (dpm_rmfs (server, fs) < 0) {
		fprintf (stderr, "dpm-rmfs %s %s: %s\n", server, fs,
		    (serrno == ENOENT) ? "No such filesystem" : sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
