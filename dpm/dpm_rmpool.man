.\" @(#)$RCSfile: dpm_rmpool.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2010 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_RMPOOL 3 "$Date$" LCG "DPM Library Functions"
.SH NAME
dpm_rmpool \- remove a disk pool definition
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_rmpool (char *" poolname )
.SH DESCRIPTION
.B dpm_rmpool
removes a disk pool definition.
.TP
.I poolname
specifies the disk pool name.
.LP
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
Pool does not exist.
.TP
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B EFAULT
.I poolname
is a NULL pointer.
.TP
.B EEXIST
Space token still associated with
.IR poolname .
.TP
.B EINVAL
The length of
.I poolname
exceeds
.BR CA_MAXPOOLNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
