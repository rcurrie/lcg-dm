#!/bin/sh

VERBOSE='no'
basedir=`dirname $0`
SITE_DEF=${basedir}/config/site.def

# lfc, dpm, dpm-dicom, cns, dm
unset MODULE
unset SWIG_LOCATION
with_client_only='no'
with_dicom='no'
with_emi='no'

CC=${CC:-cc}
MAKE=${MAKE:-make}
export CC MAKE

if [ `uname -m` = 'x86_64' ]; then
    LIBDIR='lib64'
fi

echo "$@" >${basedir}/config.status

while test $# -ne 0; do
case "$1" in
    --verbose)
        shift
        VERBOSE='yes'
        export VERBOSE
        ;;
    --with-mysql)
        shift
		sed \
		-e 's#BackendFlavour	.*$#BackendFlavour	mysql#g' \
		-e 's#UseMySQL	.*$#UseMySQL		YES#g' \
		-e 's#UseOracle	.*$#UseOracle		NO#g' \
		-e 's#UsePostgres	.*$#UsePostgres		NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: with MySQL"
        with_client_only='no'
        ;;
    --with-oracle)
        shift
		sed \
		-e 's#BackendFlavour	.*$#BackendFlavour	oracle#g' \
		-e 's#UseMySQL	.*$#UseMySQL		NO#g' \
		-e 's#UseOracle	.*$#UseOracle		YES#g' \
		-e 's#UsePostgres	.*$#UsePostgres		NO#g' \
		-e 's#BuildInterfaces	.*$#BuildInterfaces		NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: with Oracle"
        with_client_only='no'
        ;;
    --with-postgres)
        shift
		sed \
		-e 's#BackendFlavour	.*$#BackendFlavour	postgres#g' \
		-e 's#UseMySQL	.*$#UseMySQL		NO#g' \
		-e 's#UseOracle	.*$#UseOracle		NO#g' \
		-e 's#UsePostgres	.*$#UsePostgres		YES#g' \
		-e 's#BuildInterfaces	.*$#BuildInterfaces		NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: with PostgreSQL"
        with_client_only='no'
        ;;
    --with-dicom)
        shift
		[ "$VERBOSE" = 'yes' ] && echo "configure: with DICOM"
        with_dicom='yes'
        with_client_only='no'
        ;;
    --with-client-only)
        shift
		sed \
		-e 's#UseMySQL	.*$#UseMySQL		NO#g' \
		-e 's#UseOracle	.*$#UseOracle		NO#g' \
		-e 's#UsePostgres	.*$#UsePostgres		NO#g' \
		-e 's#BuildDicomCopy	.*$#BuildDicomCopy		NO#g' \
		-e 's#BuildDLI	.*$#BuildDLI		NO#g' \
		-e 's#BuildDPMServer	.*$#BuildDPMServer		NO#g' \
		-e 's#BuildNameServerDaemon	.*$#BuildNameServerDaemon	NO#g' \
		-e 's#BuildSRMv1Server	.*$#BuildSRMv1Server	NO#g' \
		-e 's#BuildSRMv2Server	.*$#BuildSRMv2Server	NO#g' \
		-e 's#BuildSRMv2_2Server	.*$#BuildSRMv2_2Server	NO#g' \
		-e 's#BuildTest	.*$#BuildTest		NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: only clients"
        with_client_only='yes'
        ;;
    --with-emi)
        shift
		sed \
		-e 's#EMIBuild	.*$#EMIBuild		YES#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: with EMI"
        with_emi='yes'
        ;;
    --without-argus)
        shift
		sed \
		-e 's#UseArgus	.*$#UseArgus		NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: without Argus"
        ;;
    --with-argus-location*)
        if [ '--with-argus-location' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "Argus location: $location"
        if [ ! -d "$location" ]; then 
            echo "Argus location does not exists: '$location'" >&2
            exit 2
        fi
        sed -e "s# ArgusLocation\	.*\$# ArgusLocation	$location#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        argus_location=$location
        ;; 
    --py-dontrunswig)
        shift
        sed \
           -e 's# PyRunSwig.*$# PyRunSwig               NO#g' \
           $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        [ "$VERBOSE" = 'yes' ] && echo "configure: without running swig to regenerate the Python wrapper"
        ;;
    --without-semsg)
        shift
		sed \
		-e 's# UseSEMsg.*$# UseSEMsg		NO#g' \
		$SITE_DEF >$SITE_DEF.new
	mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: without SEMsg (messaging support for catalogue synchronization)"
        ;;
    --with-semsg-location*)
	if [ '--with-semsg-location' = "$1" ]; then
	    location="$2"
	    shift
	else
	    location=`echo $1 | cut -d= -f 2`
	fi
	shift
	[ 'yes' = "$VERBOSE" ] && echo "SEMsg location: $location"
	if [ ! -d "$location" ]; then 
	    echo "SEMsg location does not exists: '$location'" >&2
	    exit 2
	fi
	if [ -z "$LIBDIR" ]; then
	    if [ ! -e "$location/lib/libSEMsgConnector_C.a" ]; then
	       echo "SEMsg C library not found: '$location/lib/libSEMsgConnector_C.a'" >&2
	       exit 2
	    fi
	else
	    if [ ! -e "$location/$LIBDIR/libSEMsgConnector_C.a" ]; then 
	        echo "SEMsg C library not found: '$location/$LIBDIR/libSEMsgConnector_C.a'" >&2
	        exit 2
	    fi
	fi
	sed -e "s#SEMsgLocation.*\$#SEMsgLocation	$location#g" \
	    $SITE_DEF >$SITE_DEF.new
	mv $SITE_DEF.new $SITE_DEF
	semsg_location=$location
        sed  -e 's# UseSEMsg.*$# UseSEMsg                YES#g' \
	$SITE_DEF >$SITE_DEF.new
	mv $SITE_DEF.new $SITE_DEF
	;;  
    --with-globus-location*)
        if [ '--with-globus-location' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "Globus location: $location"
        if [ ! -d "$location" ]; then 
            echo "Globus location does not exists: '$location'" >&2
            exit 2
        fi
        sed -e "s# GlobusLocation\	.*\$# GlobusLocation	$location#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        globus_location=$location
        ;;  
    --with-voms-location*)
        if [ '--with-voms-location' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "VOMS location: $location"
        if [ ! -d "$location" ]; then 
            echo "VOMS location does not exists: '$location'" >&2
            exit 2
        fi
        sed -e "s# VomsLocation\	.*\$# VomsLocation	$location#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        voms_location=$location
        ;;  
    --with-cgsi-gsoap-location*)
        if [ '--with-cgsi-gsoap-location' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "CGSI_gSOAP location: $location"
        if [ ! -d "$location" ]; then 
            echo "CGSI_gSOAP location does not exists: '$location'" >&2
            exit 2
        fi
        sed -e "s# CgsiGsoapLocation\	.*\$# CgsiGsoapLocation	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-dpm-config-file*)
        if [ '--with-dpm-config-file' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "DPM config file: $location"
        sed -e "s# DpmConfigFile\	.*\$# DpmConfigFile	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-id-map-file*)
        if [ '--with-id-map-file' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "ID Map file: $location"
        sed -e "s# IdMapFile\	.*\$# IdMapFile	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-ns-config-file*)
        if [ '--with-ns-config-file' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "NS config file: $location"
        sed -e "s# NsConfigFile\	.*\$# NsConfigFile	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-etc-dir*)
        if [ '--with-etc-dir' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "Etc dir: $location"
        sed -e "s# EtcDir\	.*\$# EtcDir	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-sysconf-dir*)
        if [ '--with-sysconf-dir' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "Sysconf dir: $location"
        sed -e "s# SysconfDir\	.*\$# SysconfDir	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-initrc-dir*)
        if [ '--with-initrc-dir' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "Initrc dir: $location"
        sed -e "s# InitRcDir\	.*\$# InitRcDir	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-gsoap-location*)
        if [ '--with-gsoap-location' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "gSOAP location: $location"
        if [ ! -d "$location" ]; then 
            echo "gSOAP location does not exists: '$location'" >&2
            exit 2
        fi
        sed -e "s# GsoapLocation	.*\$# GsoapLocation	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        gsoap_location=$location
        ;;
    --with-gsoap-version*)
        if [ '--with-gsoap-version' = "$1" ]; then
            gsoap_version="$2"
            shift
        else
            gsoap_version=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "gSOAP version: $gsoap_version"
        GSOAP_MAJOR_MINOR=`echo $gsoap_version | cut -d. -f1,2`
        sed -e "s# GsoapVersion\	.*\$# GsoapVersion	${GSOAP_MAJOR_MINOR}#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        GSOAP_FULL_VERSION=`echo $gsoap_version | awk -F. '{ printf "%02d%02d%02d",$1,$2,$3 }'`
        if [ "${GSOAP_FULL_VERSION}" '>' '020715' ]; then
            sed -e "s# GsoapWsdl2hFlags.*\$# GsoapWsdl2hFlags	-z1#"  \
                $SITE_DEF >$SITE_DEF.new
            mv $SITE_DEF.new $SITE_DEF
        elif [ "${GSOAP_FULL_VERSION}" '>' '020706' ]; then
            sed -e "s# GsoapWsdl2hFlags.*\$# GsoapWsdl2hFlags	-z#"  \
                $SITE_DEF >$SITE_DEF.new
            mv $SITE_DEF.new $SITE_DEF
        fi
	    ;;
    --with-mysql-location*)
        if [ '--with-mysql-location' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "MySQL location: $location"
        if [ ! -d "$location" ]; then 
            echo "MySQL location does not exists: '$location'" >&2
            exit 2
        fi
        sed -e "s# MySQLLocation\	.*\$# MySQLLocation	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --with-swig-location*)
        if [ '--with-swig-location' = "$1" ]; then
            location="$2"
            shift
        else
            location=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "Swig location: $location"
        if [ ! -d "$location" ]; then 
            echo "Swig location does not exists: '$location'" >&2
            exit 2
        fi
        sed -e "s# SwigLocation\	.*\$# SwigLocation	$location#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        SWIG_LOCATION="$location"
        ;;
    --with-python*)
        if [ '--with-python' = "$1" ]; then
            python_prog="$2"
            shift
        else
            python_prog=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "Python: $python_prog"
        sed -e "s# PythonProgram\	.*\$# PythonProgram	$python_prog#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        ;;
    --libdir*)
        if [ '--libdir' = "$1" ]; then
            LIBDIR="$2"
            shift
        else
            LIBDIR=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "libdir: $libdir"
        ;;
    --prefix*)
        if [ '--prefix' = "$1" ]; then
            prefix="$2"
            shift
        else
            prefix=`echo $1 | cut -d= -f 2`
        fi
        shift
        [ 'yes' = "$VERBOSE" ] && echo "prefix: $prefix"
        ;;
    --*)
        echo "Unknown option '$1'"
        exit 2
        ;;
    CC=*)
        CC=`echo $1 | cut -d= -f 2`
        shift
        ;;
    MAKE=*)
        MAKE=`echo $1 | cut -d= -f 2`
        shift
        ;;
    *)
        # the first non-option is the MODULE
        if [ -z "$MODULE" ]; then
            MODULE="$1"
            shift
        else
            echo "Unknown parameter '$1'"
            exit 2
        fi
        ;;
esac
done

if [ -z "$MODULE" ]; then
    echo "Usage: $0 (lfc|dpm|cns|dm)"
    echo "  with the following build choices:"
    echo "    --with-mysql (default)"
    echo "    --with-oracle"
    echo "    --with-postgres"
    echo "    --with-client-only (no servers are built)"
    echo "  and the following configuration options:"
    echo "    --with-dicom (DPM server only)"
    echo "    --with-emi (EMI file names)"
    echo "    --with-argus-location=<directory>"
    echo "    --with-semsg-location=<directory>"
    echo "    --with-globus-location=<directory>"
    echo "    --with-voms-location=<directory>"
    echo "    --with-cgsi-gsoap-location=<directory>"
    echo "    --with-dpm-config-file=<file>"
    echo "    --with-id-map-file=<file>"
    echo "    --with-ns-config-file=<file>"
    echo "    --with-gsoap-location=<directory>"
    echo "    --with-gsoap-version=<major.minor.patch>"
    echo "    --with-mysql-location=<directory>"
    echo "    --with-python=<program>"
    echo "    --with-swig-location=<directory>"
    echo "    --without-argus"
    echo "    --without-semsg"
    echo "    --py-dontrunswig"
    exit 1
fi

case $MODULE in
	lfc)
		sed \
		-e 's#BuildFlavour	.*$#BuildFlavour	lfc#g' \
		-e 's#BuildNameServerAsLFC	.*$#BuildNameServerAsLFC	YES#g' \
		-e 's#BuildNameServerAsDPNS	.*$#BuildNameServerAsDPNS	NO#g' \
		-e 's#BuildDicomCopy	.*$#BuildDicomCopy		NO#g' \
		-e 's#BuildDLI	.*$#BuildDLI		YES#g' \
		-e 's#BuildDPMClient	.*$#BuildDPMClient		NO#g' \
		-e 's#BuildDPMServer	.*$#BuildDPMServer		NO#g' \
		-e 's#BuildNameServerClient	.*$#BuildNameServerClient	YES#g' \
		-e 's#BuildNameServerDaemon	.*$#BuildNameServerDaemon	YES#g' \
		-e 's#BuildNameServerLibrary	.*$#BuildNameServerLibrary	YES#g' \
		-e 's#BuildSRMv1Server	.*$#BuildSRMv1Server	NO#g' \
		-e 's#BuildSRMv2Server	.*$#BuildSRMv2Server	NO#g' \
		-e 's#BuildSRMv2_2Server	.*$#BuildSRMv2_2Server	NO#g' \
		-e 's#BuildTest	.*$#BuildTest		NO#g' \
		-e 's#BuildInterfaces	.*$#BuildInterfaces		YES#g' \
		-e 's#BuildRfioClient	.*$#BuildRfioClient		NO#g' \
		-e 's#BuildRfioServer	.*$#BuildRfioServer		NO#g' \
		-e 's#BuildSecurity	.*$#BuildSecurity		YES#g' \
		-e 's#UseGSI	.*$#UseGSI		YES#g' \
		-e 's#UseKRB5	.*$#UseKRB5		NO#g' \
		-e 's#SecMakeStaticLibrary	.*$#SecMakeStaticLibrary	NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        if [ "$with_client_only" = 'yes' ]; then
            sed \
            -e 's#BuildDLI	.*$#BuildDLI		NO#g' \
            -e 's#BuildNameServerDaemon	.*$#BuildNameServerDaemon	NO#g' \
            $SITE_DEF >$SITE_DEF.new
            mv $SITE_DEF.new $SITE_DEF
        fi
		[ "$VERBOSE" = 'yes' ] && echo "configure: for LFC"
		;;

	dpm)
		sed \
		-e 's#BuildFlavour	.*$#BuildFlavour	dpm#g' \
		-e 's#BuildNameServerAsLFC	.*$#BuildNameServerAsLFC	NO#g' \
		-e 's#BuildNameServerAsDPNS	.*$#BuildNameServerAsDPNS	YES#g' \
		-e 's#BuildDicomCopy	.*$#BuildDicomCopy		NO#g' \
		-e 's#BuildDLI	.*$#BuildDLI		NO#g' \
		-e 's#BuildDPMClient	.*$#BuildDPMClient		YES#g' \
		-e 's#BuildDPMServer	.*$#BuildDPMServer		YES#g' \
		-e 's#BuildNameServerClient	.*$#BuildNameServerClient	YES#g' \
		-e 's#BuildNameServerDaemon	.*$#BuildNameServerDaemon	YES#g' \
		-e 's#BuildNameServerLibrary	.*$#BuildNameServerLibrary	YES#g' \
		-e 's#BuildSRMv1Server	.*$#BuildSRMv1Server	YES#g' \
		-e 's#BuildSRMv2Server	.*$#BuildSRMv2Server	YES#g' \
		-e 's#BuildSRMv2_2Server	.*$#BuildSRMv2_2Server	YES#g' \
		-e 's#BuildTest	.*$#BuildTest		NO#g' \
		-e 's#BuildInterfaces	.*$#BuildInterfaces		YES#g' \
		-e 's#BuildRfioClient	.*$#BuildRfioClient		YES#g' \
		-e 's#BuildRfioServer	.*$#BuildRfioServer		YES#g' \
		-e 's#BuildSecurity	.*$#BuildSecurity		YES#g' \
		-e 's#UseGSI	.*$#UseGSI		YES#g' \
		-e 's#UseKRB5	.*$#UseKRB5		NO#g' \
		-e 's#SecMakeStaticLibrary	.*$#SecMakeStaticLibrary	NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        if [ "$with_client_only" = 'yes' ]; then
            sed \
            -e 's#BuildDPMServer	.*$#BuildDPMServer		NO#g' \
            -e 's#BuildNameServerDaemon	.*$#BuildNameServerDaemon	NO#g' \
            -e 's#BuildSRMv1Server	.*$#BuildSRMv1Server	NO#g' \
            -e 's#BuildSRMv2Server	.*$#BuildSRMv2Server	NO#g' \
            -e 's#BuildSRMv2_2Server	.*$#BuildSRMv2_2Server	NO#g' \
            -e 's#BuildRfioServer	.*$#BuildRfioServer		NO#g' \
            $SITE_DEF >$SITE_DEF.new
            mv $SITE_DEF.new $SITE_DEF
        fi
        if [ "$with_dicom" = 'yes' ]; then
            sed \
            -e 's#BuildDicomCopy	.*$#BuildDicomCopy		YES#g' \
                $SITE_DEF >$SITE_DEF.new
            mv $SITE_DEF.new $SITE_DEF
        fi
		[ "$VERBOSE" = 'yes' ] && echo "configure: for DPM"
		;;

	cns)
		sed \
		-e 's#BuildFlavour	.*$#BuildFlavour	cns#g' \
		-e 's#BuildNameServerAsLFC	.*$#BuildNameServerAsLFC	NO#g' \
		-e 's#BuildNameServerAsDPNS	.*$#BuildNameServerAsDPNS	NO#g' \
		-e 's#BuildDicomCopy	.*$#BuildDicomCopy		NO#g' \
		-e 's#BuildDPMClient	.*$#BuildDPMClient		NO#g' \
		-e 's#BuildDPMServer	.*$#BuildDPMServer		NO#g' \
		-e 's#BuildNameServerClient	.*$#BuildNameServerClient	YES#g' \
		-e 's#BuildNameServerDaemon	.*$#BuildNameServerDaemon	YES#g' \
		-e 's#BuildNameServerLibrary	.*$#BuildNameServerLibrary	YES#g' \
		-e 's#BuildSRMv1Server	.*$#BuildSRMv1Server	NO#g' \
		-e 's#BuildSRMv2Server	.*$#BuildSRMv2Server	NO#g' \
		-e 's#BuildSRMv2_2Server	.*$#BuildSRMv2_2Server	NO#g' \
		-e 's#BuildTest	.*$#BuildTest		NO#g' \
		-e 's#BuildRfioClient	.*$#BuildRfioClient		NO#g' \
		-e 's#BuildRfioServer	.*$#BuildRfioServer		NO#g' \
		-e 's#UseGSI	.*$#UseGSI		YES#g' \
		-e 's#UseKRB5	.*$#UseKRB5		NO#g' \
		-e 's#SecMakeStaticLibrary	.*$#SecMakeStaticLibrary	NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: for CNS"
		;;

	dm)
		sed \
		-e 's#BuildFlavour	.*$#BuildFlavour	dm#g' \
		-e 's#BuildNameServerAsLFC	.*$#BuildNameServerAsLFC	NO#g' \
		-e 's#BuildNameServerAsDPNS	.*$#BuildNameServerAsDPNS	NO#g' \
		-e 's#BuildDicomCopy	.*$#BuildDicomCopy		NO#g' \
		-e 's#BuildDLI	.*$#BuildDLI		NO#g' \
		-e 's#BuildDPMClient	.*$#BuildDPMClient		NO#g' \
		-e 's#BuildDPMServer	.*$#BuildDPMServer		NO#g' \
		-e 's#BuildNameServerClient	.*$#BuildNameServerClient	NO#g' \
		-e 's#BuildNameServerDaemon	.*$#BuildNameServerDaemon	NO#g' \
		-e 's#BuildNameServerLibrary	.*$#BuildNameServerLibrary	NO#g' \
		-e 's#BuildRfioClient	.*$#BuildRfioClient		NO#g' \
		-e 's#BuildRfioServer	.*$#BuildRfioServer		NO#g' \
		-e 's#BuildSecurity	.*$#BuildSecurity		YES#g' \
		-e 's#BuildSRMv1Server	.*$#BuildSRMv1Server	NO#g' \
		-e 's#BuildSRMv2Server	.*$#BuildSRMv2Server	NO#g' \
		-e 's#BuildSRMv2_2Server	.*$#BuildSRMv2_2Server	NO#g' \
		-e 's#BuildTest	.*$#BuildTest		NO#g' \
		-e 's#BuildInterfaces	.*$#BuildInterfaces		NO#g' \
		-e 's#UseGSI	.*$#UseGSI		YES#g' \
		-e 's#UseKRB5	.*$#UseKRB5		NO#g' \
		-e 's#SecMakeStaticLibrary	.*$#SecMakeStaticLibrary	NO#g' \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
		[ "$VERBOSE" = 'yes' ] && echo "configure: for lcg-dm-common"
		;;

	*)
		echo "Usage : $0 <module>"
		echo "The second argument can only be : cns, lfc, dpm or dm"
		;;
esac

if [ "$with_emi" = 'yes' ]; then
    if [ -z "$prefix" ]; then
        sed -e "s#^prefix.*\$#prefix = /usr#"  \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
        prefix=/usr
    fi
    if [ -z "$argus_location" ]; then
        sed -e "s# ArgusLocation\	.*\$# ArgusLocation	/usr#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
    fi
    if [ -z "$semsg_location" ]; then
        sed -e "s# SEMsgLocation\	.*\$# SEMsgLocation	/usr#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
    fi
    if [ -z "$globus_location" ]; then
        sed -e "s# GlobusLocation\	.*\$# GlobusLocation	/usr#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
    fi
    if [ -z "$voms_location" ]; then
        sed -e "s# VomsLocation\	.*\$# VomsLocation	/usr#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
    fi
    if [ -z "$gsoap_location" ]; then
        sed -e "s# GsoapLocation\	.*\$# GsoapLocation	/usr#" \
            $SITE_DEF >$SITE_DEF.new
        mv $SITE_DEF.new $SITE_DEF
    fi
fi

# setting prefix
if [ -n "$prefix" ]; then
    sed -e "s#^prefix.*\$#prefix = $prefix#"  \
        $SITE_DEF >$SITE_DEF.new
    mv $SITE_DEF.new $SITE_DEF
fi

# setting LIBDIR
if [ -n "$LIBDIR" ]; then
    sed -e "s#^_lib.*\$#_lib = $LIBDIR#"  \
        $SITE_DEF >$SITE_DEF.new
    mv $SITE_DEF.new $SITE_DEF
fi

# checking for Swig and disabling the build of interfaces, if not found
tmp_path="$PATH"
if [ -n "$SWIG_LOCATION" ]; then
    PATH="$SWIG_LOCATION/bin:$PATH"
fi
is_swig=`which swig 2>/dev/null`
if [ -z "$is_swig" ]; then
    echo "Warning: Swig not found, disabling the build of interfaces" >&2
    sed \
    -e 's#BuildInterfaces	.*$#BuildInterfaces		NO#g' \
        $SITE_DEF >$SITE_DEF.new
    mv $SITE_DEF.new $SITE_DEF
else
    # setting SwigLib according to SwigLocation
    # we cannot use 'swig -swiglib', because the 
    # ETICS dependency does not return this properly
    SWIG_LOCATION=`awk '/SwigLocation/ { print $3 }' config/site.def`
    for SWIG_LIB in $SWIG_LOCATION/share/swig/* \
                    $SWIG_LOCATION/share/swig* \
                    $SWIG_LOCATION/lib*/swig/* \
                    $SWIG_LOCATION/lib*/swig* \
           unkown; do
      if [ -f "$SWIG_LIB/perl5/perl5.swg" ]; then break; fi
    done
    if [ "$SWIG_LIB" = 'unknown' ]; then
        echo "Warning: unknown Swig lib location, trying the default!"
   SWIG_LIB=`swig -swiglib`
    fi
    sed -e "s# SwigLib.*\$# SwigLib    $SWIG_LIB#"  \
        $SITE_DEF >$SITE_DEF.new
    mv $SITE_DEF.new $SITE_DEF
fi
PATH="$tmp_path"

$MAKE -f Makefile.ini Makefiles

