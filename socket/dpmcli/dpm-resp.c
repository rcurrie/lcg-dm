/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-resp.c,v 1.4 2007/07/20 11:55:04 grodid Exp $

// Created by GG (03/11/2005)
// Modified   GG (20/07/2007)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#include "u64subr.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
  //#include "dpmestat.h"
	int i;
	//int r = 0;
	char s_token[CA_MAXDPMTOKENLEN+1];
	char u_token[CA_MAXDPMTOKENLEN+1];
	int status;
	int thisarg;
	char tmpbut[25];
	char tmpbug[25];

	char  s_type;
	char  s_retpol;
	char  s_aclat;
	u_signed64 req_t_space;
	u_signed64 req_g_space;
	time_t req_lifetime;
	//gid_t req_gid = 0; 
	char actual_s_type;
	u_signed64 actual_t_space;
	u_signed64 actual_g_space;
	time_t actual_lifetime;

	//char poolname[CA_MAXPOOLNAMELEN+1];
	char* poolname = NULL;

#include "dpmgdebug.h"

	if (argc < 3) {
		fprintf (stderr, "usage: %s \
[--dpmlifet=req_lifetime] (default=86400)   \
[--dpmftype=[V|D|P]] (default=V)	    \
[--dpmretpol=[R|O|C]] (default=-)	    \
[--dpmaclat=[O|N]] (default=O)				\
[--dpmreqgid=requestedGroupId] (default nothing)	\
[--dpmutoken=usertoken] (default=\"unknownutoken\")	\
[--dpmpool=poolName] (default nothing)	\
req_t_space req_g_space\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

/*    req_t_space = atoll(argv[thisarg++]); */
/*    req_g_space = atoll(argv[thisarg++]); */
  req_t_space = strutou64(argv[thisarg++]);
  req_g_space = strutou64(argv[thisarg++]);

	// Loading inputs

  //Pprintf (" \n");

  // Setting defaults

 if ( ! dpm_lifet ) {
   dpm_lifet = 86400;
 }
 req_lifetime = dpm_lifet;
 if ( ! dpm_utoken ) {
   dpm_utoken = "unknownutoken";
 }
 strncpy(u_token, dpm_utoken, CA_MAXDPMTOKENLEN+1);

 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   dpm_ftype = 'V';
 }
 s_type = dpm_ftype;

 if ( dpm_retpol == 'Z' || dpm_retpol == '0' ) {
   dpm_retpol = 'R';
 }
 s_retpol = dpm_retpol;

 if ( dpm_aclat == 'Z' || dpm_aclat == '0' ) {
   dpm_aclat = 'O';
 }
 s_aclat = dpm_aclat;

// if ( ! dpm_pool ) {
//   poolname = NULL;
// } else
 if ( dpm_pool )
   poolname = strdup(dpm_pool);
   //strncpy(poolname, dpm_pool, CA_MAXPOOLNAMELEN+1);

  printf (" \n");

	if ((status = dpm_reservespace (s_type, 
					u_token,
					s_retpol,
					s_aclat,
			       req_t_space, 
			       req_g_space, 
					req_lifetime,
					dpm_reqgid,
					poolname,
			       &actual_s_type, 
			       &actual_t_space, 
			       &actual_g_space, 
			       &actual_lifetime, 
			       s_token
			       )) < 0) {
		sperror ("dpm_reservespace");
		exit (1);
	}

	printf ("dpm_reservespace returned s_token: %s\n", s_token);

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");

	if (status == DPM_FAILED)
		exit (1);
	else {
                u64tostru(actual_t_space, tmpbut, 0); 
                u64tostru(actual_g_space, tmpbug, 0); 
		printf ("dpm_reservespace provided actual_s_type: %c actual_t_space: %s actual_g_space: %s actual_lifetime: %d\n", actual_s_type,  tmpbut, tmpbug, actual_lifetime );
/*  		printf ("dpm_reservespace provided actual_s_type: %c actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", actual_s_type, actual_t_space, actual_g_space, actual_lifetime ); */
        }

	return 0;
}
