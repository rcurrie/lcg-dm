/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-delreplica.c,v 1.3 2007/07/19 15:53:24 grodid Exp $

// Created by GG (08/03/2006)

/*
 * echo $DPM_HOST $DPNS_HOST
 * lxb1727 lxb1727
 * ../../socket/dpmcli/dpm-delreplica --dpmproto=rfio --dpmhost=lxb1727.cern.ch /dpm/cern.ch/home/dteam/grodid/tglob/Mlxb1727/D2006-03-09/H102327/srmv2_Tfile.fil080338A0
 * dpns-ls -l /dpm/cern.ch/home/dteam/grodid/tglob/Mlxb1727/D2006-03-09/H102327
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

/* major/minor number */
#include <sys/sysmacros.h>

#include <signal.h>   /* catch signals */

#include "dpns_api.h"
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

extern	char	*getenv();
extern	int	optind;
time_t current_time;

// gnu opts
#include "dpmgopts.h"

#include "gop_alloc.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
#include "dpmestat.h"
	struct dpm_getfilestatus *filestatuses;
	int i = 0;
	//int nbfiles;
	//int nbprotocols;
	int nbreplies;
	static char *def_protocols[] = {"rfio"};
	int r = 0;
	char r_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_getfilereq *reqfiles;
	//int reqid;
	int status;
	char *gbrk;
	int k;

            int flags;
            Cns_list list;
            struct Cns_filereplica *lp;

	    struct dpm_pool *lpools;
	    int pnbreplies;
	    u_signed64 t_free_space = 0;
	    u_signed64 t_free_space0 = 0;
	    u_signed64 t_free_space1 = 0;
	    u_signed64 smax = 0;
	    char *nmax;
	    u_signed64 imax;
	    char ipath[256];
	    //strcpy char lpsfn[256];
	    char *lpsfn;
	    int delarg = 0;

	char *pn;
	char *pm;
	//struct Cns_filestat statbuf;
	//time_t ltime;
	Cns_DIR *dirp;
	//struct Cns_direnstat *dxp;
	struct Cns_direnrep *dxp;

	int nbprotos;
	char **lprotos;
	int nbsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;
	char hostenvm[256];
	char hostenvn[256];

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURL\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

	if ( ! nbargs ) {
	  perror ("dpm-get: at least one SURL must be provided ");
	  exit(1);
	}
	//printf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);
	
	// End of Options stuff

	// Input preparation //////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURLs\n", argv[0]);
		exit (1);
	}

	if ( dpm_host ) {
	  printf ("    host: %s\n", dpm_host);
	  strcpy(hostenvm, "DPM_HOST=");
	  strcat(hostenvm, dpm_host);
	  putenv(hostenvm);
	  strcpy(hostenvn, "DPNS_HOST=");
	  strcat(hostenvn, dpm_host);
	  putenv(hostenvn);
	}
	(void) time (&current_time);
	if ((pn = getenv ("DPNS_HOST")) && (pm = getenv ("DPM_HOST")) && strcmp(pn, pm)) {
	  printf ( " DPNS_HOST: %s and DPM_HOST: %s mismatch\n", pn, pm);
	  exit(USERR);
	} else {
	  printf ("DPM_HOST: %s\n", pm);
	}

	// Loading inputs

  //Pprintf (" \n");

#if 0
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROTL)
#endif

  // Setting defaults

    nbprotos = 1;
//lprotos = calloc (nbprotos, sizeof(char*));
    //lprotos[0] = strdup("rfio");
    //strcpy(lprotos[0], def_protocols[0]);

 nbsurls = nbargs;
 nbsurls = 1;
 if ((reqfiles = calloc (nbsurls, sizeof(struct dpm_getfilereq))) == NULL) {
   perror ("calloc");
   exit (1);
 }
 if ( ! dpm_lifet ) {
   dpm_lifet = 0;
 }
 if ( ! dpm_stoken ) {
   dpm_stoken = "dontknowyet";
 }
 if ( ! dpm_utoken ) {
   dpm_utoken = "unknownutoken";
 }
 //if ( ! dpm_ftype ) {
 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   //dpm_ftype = strdup("Volatile");
   dpm_ftype = 'V';
 }

  printf (" \n");

	if ((status = dpm_getpools (&pnbreplies, 
				    &lpools
				    )) < 0) {
		sperror ("dpm_getpools");
		exit (1);
	}

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

	printf ("Current pools: ");
	for (i = 0; i < pnbreplies; i++) {
	  printf(" %s", lpools[i].poolname);
	  t_free_space += lpools[i].free;
	}
	printf (" t_free_space: %llu \n", t_free_space);
	free(lpools);
	t_free_space0 = t_free_space;
	t_free_space = 0;

 for (i = 0; i < nbsurls; i++) {
   reqfiles[i].lifetime = dpm_lifet;
   reqfiles[i].from_surl = remargs[i];
   reqfiles[i].f_type = dpm_ftype;
   //strcpy(reqfiles[i].s_token, dpm_stoken);
   //reqfiles[i].flags = dpm_flags;
   //printf (" File: %s %d %s %c \n", reqfiles[i].from_surl, (int) reqfiles[i].lifetime, reqfiles[i].s_token, reqfiles[i].f_type);
   printf ("SURL: %s \n", reqfiles[i].from_surl);
 }

  printf (" \n");
 
  // Listing the directory ...
#if 0
	if ((dirp = Cns_opendir (remargs[0])) == NULL)
		return (-1);
	while ((dxp = Cns_readdirx (dirp)) != NULL) {
	  printf ( " fileid filesize: %llu %llu %s %d\n", dxp->fileid, dxp->filesize, dxp->d_name, dxp->nbreplicas);
	  if ( smax < dxp->filesize ) {
	    smax = dxp->filesize;
	    nmax = strdup(dxp->d_name);
	    imax = dxp->fileid;
	  }
	}
	(void) Cns_closedir (dirp);
#endif
#if 0
	if ((dirp = Cns_opendir (remargs[0])) == NULL)
		return (-1);
	while ((dxp = Cns_readdirxr (dirp, NULL)) != NULL) {
	  printf ( " fileid filesize: %llu %llu %s %d\n", dxp->fileid, dxp->filesize, dxp->d_name, dxp->nbreplicas);
	  if ( dxp->nbreplicas && smax < dxp->filesize ) {
	    smax = dxp->filesize;
	    nmax = strdup(dxp->d_name);
	    imax = dxp->fileid;
	  }
	}
	(void) Cns_closedir (dirp);

	printf ("Selected SURL: %s/%s %llu \n", remargs[0], nmax, smax);
	reqfiles[0].from_surl = realloc(reqfiles[0].from_surl, sizeof(char)*(strlen(reqfiles[0].from_surl)+strlen(nmax)+2));
	strcat(reqfiles[0].from_surl, "/");
	strcat(reqfiles[0].from_surl, nmax);
	//status = Cns_getpath (pn, imax, ipath);
	if ( (status = Cns_getpath (pn, imax, ipath)) < 0 )
	  sperror ("Cns_getpath");
	printf ("Selected PURL: %d %d %s %s %llu \n", status, serrno, pn, ipath, imax);

	//listreplica(remargs[0]);

            flags = CNS_LIST_BEGIN;
            //while ((lp = dpns_listreplica (reqfiles[0].from_surl, NULL, flags, &list)) != NULL) {
            while ((lp = dpns_listreplica (remargs[0], NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
                 printf ("state[%d] = %s, SFN = %s, FS = %s\n", i, 
			 f_stat[(filestatuses+i)->status >> 12],
			 lp->sfn, lp->fs);
            }
            (void) dpns_listreplica (reqfiles[0].from_surl, NULL, CNS_LIST_END, &list);
#else

	printf ("Selected SURL: %s \n", remargs[0]);
	    if ( nbargs > 1 ) {
	      delarg = atoi(remargs[1]);
	    }

	i = 0;
            flags = CNS_LIST_BEGIN;
            while ((lp = dpns_listreplica (reqfiles[0].from_surl, NULL, flags, &list)) != NULL) {
            //while ((lp = dpns_listreplica (remargs[0], NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
		 if ( nbargs > 1 )
		   if ( i == delarg )
		     lpsfn = strdup(lp->sfn);
		 
                 printf ("before replica[%d] : SFN = %s, FS = %s\n", i++, 
			 lp->sfn, lp->fs);
		 //strcpy(lpsfn, lp->sfn);
		 //GOODlpsfn = strdup(lp->sfn);
            }
            (void) dpns_listreplica (reqfiles[0].from_surl, NULL, CNS_LIST_END, &list);
#endif

	// Effective call


	    //if ((status = dpm_delreplica (gbrk)) < 0) {
	    if ( nbargs > 1 ) {
	      printf(" Deleting %d %s\n", atoi(remargs[1]), lpsfn);
	      
			if ((status = dpm_delreplica (lpsfn)) < 0) {
			  sperror ("dpm_delreplica");
			  exit (1);
			}
	      
	    } else
	      return 0;

  printf (" \n");

            flags = CNS_LIST_BEGIN;
	    i = 0;
            while ((lp = dpns_listreplica (reqfiles[0].from_surl, NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
                 printf ("after  replica[%d] : SFN = %s, FS = %s\n", i++, 
			 lp->sfn, lp->fs);
		 //lpsfn = strdup(lp->sfn);
            }
            (void) dpns_listreplica (reqfiles[i].from_surl, NULL, CNS_LIST_END, &list);

  printf (" \n");

	if ((status = dpm_getpools (&pnbreplies, 
				    &lpools
				    )) < 0) {
		sperror ("dpm_getpools");
		exit (1);
	}

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

	printf ("Current pools: ");
	for (i = 0; i < pnbreplies; i++) {
	  printf(" %s", lpools[i].poolname);
	  t_free_space += lpools[i].free;
	}
	printf (" t_free_space: %llu \n", t_free_space);
	free(lpools);
	t_free_space0 = t_free_space;
	t_free_space = 0;

	return 0;

}
