/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-getreqid.c,v 1.8 2007/07/19 15:53:24 grodid Exp $

// Created by GG (04/11/2004)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include "dpm_api.h"
#include "serrno.h"
#include "u64subr.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"

#include <errno.h>
#include "dpm_util.h"

#include <grp.h>
#include <pwd.h>
//#include "Cns.h"
#include "dpns_api.h"
#define SIXMONTHS (6*30*24*60*60)

/***** sfnfromsurl from JPB */
char *
sfnfromsurl (char *surl)
{
        char *p;

        if (strncmp (surl, "srm://", 6)) {
                errno = EINVAL;
                return (NULL);
        }
        if ((p = strstr (surl + 6, "?SFN=")))
                return (p + 5);
        if ((p = strchr (surl + 6, '/')) == NULL) {
                errno = EINVAL;
                return (NULL);
        }
        return (p);
}

/***** the main DPM file array */
static uint ndpmfiles;
static uint ndpmfalloc;
static dpmfile_t **dpmfiles = NULL;

/***** check the header */
/* Unix98: must not print empty header */
static void check_headers(void){
  format_node *walk = format_list;
  int head_normal = 0;
  if(header_type==HEAD_MULTI){
    header_gap = screen_rows-1;  /* true BSD */
    return;
  }
  if(header_type==HEAD_NONE){
    lines_to_next_header = -1;  /* old Linux */
    return;
  }
  while(walk){
    if(!*(walk->name)){
      walk = walk->next;
      continue;
    }
    if(walk->pr){
      head_normal++;
      walk = walk->next;
      continue;
    }
    walk = walk->next;
  }
  if(!head_normal) lines_to_next_header = -1; /* how UNIX does --noheader */
}

/***** check sort needs */
/* see what files need to be read, etc. */
static unsigned check_sort_needs(sort_node *walk){
  unsigned needs = 0;
  while(walk){
    needs |= walk->need;
    walk = walk->next;
  }
  return needs;
}

/***** check needs */
/* see what files need to be read, etc. */
static unsigned collect_format_needs(format_node *walk){
  unsigned needs = 0;
  while(walk){
    needs |= walk->need;
    walk = walk->next;
  }
  return needs;
}

static format_node *proc_format_list;
static format_node *task_format_list;

static unsigned needs_for_threads;
static unsigned needs_for_sort;
static unsigned proc_format_needs;
static unsigned task_format_needs;

#define needs_for_format (proc_format_needs|task_format_needs)

#define PROC_ONLY_FLAGS (PROC_FILLENV|PROC_FILLARG|PROC_FILLCOM|PROC_FILLMEM)

/***** munge lists and determine openproc() flags */
static void lists_and_needs(void){
  check_headers();

  // only care about the difference when showing both
  if(thread_flags & TF_show_both){
    format_node pfn, tfn; // junk, to handle special case at begin of list
    format_node *walk = format_list;
    format_node *p_end = &pfn;
    format_node *t_end = &tfn;
    while(walk){
      format_node *new = malloc(sizeof(format_node));
      memcpy(new,walk,sizeof(format_node));
      p_end->next = walk;
      t_end->next = new;
      p_end       = walk;
      t_end       = new;
      switch(walk->flags & CF_PRINT_MASK){
      case CF_PRINT_THREAD_ONLY:
        p_end->pr   = pr_nop;
        p_end->need = 0;
        break;
      case CF_PRINT_PROCESS_ONLY:
        t_end->pr   = pr_nop;
        t_end->need = 0;
        break;
      default:
        fprintf(stderr, "please report this bug\n");
        // FALL THROUGH
      case CF_PRINT_AS_NEEDED:
      case CF_PRINT_EVERY_TIME:
        break;
      }
      walk = walk->next;
    }
    t_end->next = NULL;
    p_end->next = NULL;
    proc_format_list = pfn.next;
    task_format_list = tfn.next;
  }else{
    proc_format_list = format_list;
    task_format_list = format_list;
  }

  proc_format_needs = collect_format_needs(proc_format_list);
  task_format_needs = collect_format_needs(task_format_list);

  needs_for_sort = check_sort_needs(sort_list);

  // move process-only flags to the process
  proc_format_needs |= (task_format_needs &~ PROC_ONLY_FLAGS);
  task_format_needs &= ~PROC_ONLY_FLAGS;

  if(bsd_c_option){
    proc_format_needs &= ~PROC_FILLARG;
    needs_for_sort    &= ~PROC_FILLARG;
  }
  if(!unix_f_option){
    proc_format_needs &= ~PROC_FILLCOM;
    needs_for_sort    &= ~PROC_FILLCOM;
  }
  // convert ARG to COM as a standard
  if(proc_format_needs & PROC_FILLARG){
    proc_format_needs |= PROC_FILLCOM;
    proc_format_needs &= ~PROC_FILLARG;
  }
  if(bsd_e_option){
    if(proc_format_needs&PROC_FILLCOM) proc_format_needs |= PROC_FILLENV;
  }
  
  /* FIXME  broken filthy hack -- got to unify some stuff here */
  /* removed GG 
  if( ( (proc_format_needs|task_format_needs|needs_for_sort) & PROC_FILLWCHAN) && !wchan_is_number)
    if (open_psdb(namelist_file)) wchan_is_number = 1;
  */

  if(thread_flags&TF_loose_tasks) needs_for_threads |= PROC_LOOSE_TASKS;
}

//////////////////////////////////////////////////////////////////////////

/***** transfer one file data from main array to temp buffer */
//static dpmfile_t* read_dpmfile(int line, PROCTAB *restrict const PT, dpmfile_t *restrict p) {
static dpmfile_t* read_dpmfile(int line, dpmfile_t *restrict p) {
  dpmfile_t *ret;

  if ( line >= (int)ndpmfiles )
    goto out;

  //fprintf(stderr, " read_dpmfile once more ... %d \n", line);
  memcpy(p, dpmfiles[line], sizeof(dpmfile_t));
  //fprintf(stderr, " read_dpmfile once more ... %s \n", p->surl);

  ret = p;
  if (ret) return ret;

out:
  return NULL;
}

/***** just display dpmfiles */
static void simple_spewf(void){
  dpmfile_t buf;
  dpmfile_t *ret;
  //  PROCTAB* ptp;
  int line = 0;
  memset(&buf, '#', sizeof(dpmfile_t));
  //USELESSmemset(&buf, 0, sizeof(dpmfile_t));

  //    while( (ret = read_dpmfile(line++, ptp, &buf)) ){
    while( (ret = read_dpmfile(line++, &buf)) ){
      //pGOOD      fprintf(stderr, " before show_one_procf ... %s %s \n", buf.status, ret->status);
      show_one_procf(&buf, proc_format_list);
      /* old gg
      if (!line) 
	break;
      */
      if ( line == ndpmfiles )
	break;
      if ( line == dpm_list ) 
	break;
    }
}

#define TRF_COLD(NAME, OR) \
    (*dpmfiles[fcount]). ## NAME = malloc((1+strlen(OR))*(sizeof(char))); \
    strcpy((*dpmfiles[fcount]). ## NAME, OR);

#define TRF_C2(NAME, OR) \
    dpmfilel->/**/NAME = realloc(dpmfilel->/**/NAME, (1+strlen(OR))*(sizeof(char))); \
    dpmfilel->/**/NAME = strdup(OR);

#define TRF_C(NAME, OR) \
    dpmfiles[fcount]->/**/NAME = malloc((strlen(OR))*(sizeof(char))); \
    dpmfiles[fcount]->/**/NAME = strdup(OR);

#define TRF_I(NAME, OR) \
    dpmfiles[fcount]->/**/NAME = malloc(sizeof(OR)); \
    dpmfiles[fcount]->/**/NAME = OR;

//********************************************************//
static int
formentry(path, statbuf, slink, comment, fcount)
char *path;
struct Cns_filestat *statbuf;
char *slink;
char *comment;
uint fcount;
{
  int iflag = 1;
  int clflag = 1;
  int lflag = 1;
  
	struct group *gr;
	char modestr[11];
	struct passwd *pw;
	static gid_t sav_gid = -1;
	static char sav_gidstr[7];
	time_t ltime;
	static uid_t sav_uid = -1;
	static char sav_uidstr[CA_MAXUSRNAMELEN+1];
	char timestr[13];
	struct tm *tm;
	char tmpbuf[21];
	char tmpbus[64];
	time_t current_time;

	/*
	if (statbuf->status == 'D' && delflag == 0)
		return (0);
	*/
	if (iflag) {
	  u64tostr (statbuf->fileid, tmpbuf, 20);
	  TRF_C(fileid, tmpbuf)
	    }
	if (clflag) {
		sprintf (tmpbus, "%d", statbuf->fileclass);
	  TRF_C(filecl, tmpbus)
	}
	if (lflag) {
		if (statbuf->filemode & S_IFDIR)
			modestr[0] = 'd';
		else if ((statbuf->filemode & S_IFLNK) == S_IFLNK)
			modestr[0] = 'l';
		else
			modestr[0] = statbuf->status;
		modestr[1] = (statbuf->filemode & S_IRUSR) ? 'r' : '-';
		modestr[2] = (statbuf->filemode & S_IWUSR) ? 'w' : '-';
		if (statbuf->filemode & S_IXUSR)
			if (statbuf->filemode & S_ISUID)
				modestr[3] = 's';
			else
				modestr[3] = 'x';
		else
			modestr[3] = '-';
		modestr[4] = (statbuf->filemode & S_IRGRP) ? 'r' : '-';
		modestr[5] = (statbuf->filemode & S_IWGRP) ? 'w' : '-';
		if (statbuf->filemode & S_IXGRP)
			if (statbuf->filemode & S_ISGID)
				modestr[6] = 's';
			else
				modestr[6] = 'x';
		else
			modestr[6] = '-';
		modestr[7] = (statbuf->filemode & S_IROTH) ? 'r' : '-';
		modestr[8] = (statbuf->filemode & S_IWOTH) ? 'w' : '-';
		if (statbuf->filemode & S_IXOTH)
			if (statbuf->filemode & S_ISVTX)
				modestr[9] = 't';
			else
				modestr[9] = 'x';
		else
			modestr[9] = '-';
		modestr[10] = '\0';
		TRF_C(mode, modestr)

		if (statbuf->uid != sav_uid) {
			sav_uid = statbuf->uid;
			if ((pw = getpwuid (sav_uid)))
				strcpy (sav_uidstr, pw->pw_name);
			else
				sprintf (sav_uidstr, "%-8u", sav_uid);
		}
		TRF_C(owner, sav_uidstr)

		if (statbuf->gid != sav_gid) {
			sav_gid = statbuf->gid;
			if ((gr = getgrgid (sav_gid))) {
				strncpy (sav_gidstr, gr->gr_name, sizeof(sav_gidstr) - 1);
				sav_gidstr[sizeof(sav_gidstr) - 1] = '\0';
			} else
				sprintf (sav_gidstr, "%-6u", sav_gid);
		}
		TRF_C(ownerg, sav_gidstr)

		  /*
		if (cflag)
			ltime = statbuf->ctime;
		else if (uflag)
			ltime = statbuf->atime;
		else
			ltime = statbuf->mtime;
		  */

		  (void) time (&current_time);
		  ltime = statbuf->atime;
		tm = localtime (&ltime);
		if (ltime < current_time - SIXMONTHS ||
		    ltime > current_time + 60)
			strftime (timestr, 13, "%b %d  %Y", tm);
		else
			strftime (timestr, 13, "%b %d %H:%M", tm);
		TRF_C(laccs, timestr)
		  ltime = statbuf->mtime;
		tm = localtime (&ltime);
		if (ltime < current_time - SIXMONTHS ||
		    ltime > current_time + 60)
			strftime (timestr, 13, "%b %d  %Y", tm);
		else
			strftime (timestr, 13, "%b %d %H:%M", tm);
		TRF_C(modif, timestr)
		  ltime = statbuf->ctime;
		tm = localtime (&ltime);
		if (ltime < current_time - SIXMONTHS ||
		    ltime > current_time + 60)
			strftime (timestr, 13, "%b %d  %Y", tm);
		else
			strftime (timestr, 13, "%b %d %H:%M", tm);
		TRF_C(metam, timestr)

		/*
		printf ("%s %3d %-8.8s %-6.6s %s %s ",
		    modestr, statbuf->nlink, sav_uidstr, sav_gidstr,
		    u64tostr (statbuf->filesize, tmpbuf, 20), timestr);
		*/
	}
	//Pprintf ("%s", path);
#if 0
	if (lflag && modestr[0] == 'l')
		printf (" -> %s", slink);
	if (cmflag)
		printf (" %s", comment);
#endif
	//Pprintf ("\n");
	return (0);
}

//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
#include "dpmestat.h"
	struct dpm_putfilestatus *pfilestatuses;
	struct dpm_getfilestatus *gfilestatuses;
	struct dpm_reqsummary *summaries;
  //struct dpm_filestatus *filestatuses;
	struct dpm_tokeninfo *tokeninfos;
	int i, is;
	int nbfiles;
	//int nbprotocols;
	int nbreplies;
	int pnbreplies;
	int gnbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;

	//int nbprotos;
	//char **lprotos;
	int nbsurls;
	//char **lsurls;
	char tmpbuf[25];
	char tmpbup[25];
	uint fcount;
	dpmfile_t *dpmfilel;

	int thisarg;
	int nbargs;
	char **remargs;
	char **crereqs;

	//char fullpath[CA_MAXPATHLEN+1];
	struct Cns_filestat statbuf;
	char* sfn;

	char *pn;
	char *pm;
	if ((pn = getenv ("DPNS_HOST")) && (pm = getenv ("DPM_HOST")) && strcmp(pn, pm)) {
	  printf ( " DPNS_HOST: %s and DPM_HOST: %s mismatch\n", pn, pm);
	  exit(1);
	}

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s user_token\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( nbargs != 1 ) {
    perror ("dpm-getreqid: one, and only one user_token must be provided ");
    exit(1);
  }
  nbfiles = nbargs;
  //Pprintf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  //Pprintf (" \n");

#if 0
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROT)
#endif

  // Setting defaults

/*    if ( ! dpm_rtoken ) { */
/*      perror (" Reqid token option is mandatory: --dpmrtoken=request_token "); */
/*      exit(1); */
/*    } */
 nbsurls = nbargs;

//Pprintf (" \n");
  dpmfilel = calloc(1, sizeof(dpmfile_t));

  if ((status = dpm_getreqid (remargs[0],  //  NULL
				   &nbreplies, 
				   &tokeninfos
				   )) < 0) {
		sperror ("dpm_getreqid");
		exit (1);
	}

	printf ("For user token %s, request state %s, nbreplies %d \n", remargs[0], status == DPM_SUCCESS ? "Done" : "Failed", nbreplies);
	if (status == DPM_FAILED)
		exit (1);

	if ( nbreplies ) {
	  remargs = realloc(remargs, sizeof(char*)*(nbreplies));
	  crereqs = calloc(nbreplies, sizeof(char*));
	} else {
	  printf ("For user token %s, no request found\n", remargs[0]);
	  exit(1);
	}
	/*
	ndpmfiles = 0;
	fcount = 0;
	ndpmfalloc = nbreplies;
	dpmfiles = realloc(dpmfiles, sizeof(dpmfiles)*ndpmfalloc);
	*/

	for (i = 0; i < nbreplies; i++) {
	  strftime(tmpbuf, 25, "%y%m%d-%T", localtime(&((tokeninfos+i)->c_time)));
	  printf ("request[%d] r_token %s, creation time = <%s>\n", i,
		  (tokeninfos+i)->r_token,
		  tmpbuf);
	  //(int)(tokeninfos+i)->c_time); 
	  remargs[i] = strdup((tokeninfos+i)->r_token);
	  crereqs[i] = strdup(tmpbuf);
	}
	free(tokeninfos);

	// added 14/11/04

	nbfiles = nbreplies;
	if ((status = dpm_getreqsummary (nbfiles, 
				   remargs,
				   &nbreplies, 
				   &summaries
				   )) < 0) {
		sperror ("dpm_getreqsummary");
		exit (1);
	}

	printf ("\nrequest state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

	ndpmfiles = 0;
	fcount = 0;
	ndpmfalloc = 10;
	dpmfiles = realloc(dpmfiles, sizeof(dpmfiles)*ndpmfalloc);

        for (is = 0; is < nbreplies; is++) {
	  
	  printf ("state[%d] => r_token %s, r_type = %c, Q/P/F= %d/%d/%d \n", 
		  is,
		  (summaries+is)->r_token,
		  (summaries+is)->r_type,
		  (summaries+is)->nb_queued,
		  (summaries+is)->nb_progress,
		  (summaries+is)->nb_finished);
	  

	  if ( (summaries+is)->r_type == 'P' ) {
	    // PUT state
		if ((status = dpm_getstatus_putreq ((summaries+is)->r_token, 
						    0, 
						    NULL,
						    &pnbreplies, 
						    &pfilestatuses
						    )) < 0) {
			sperror ("dpm_getstatus_putreq");
			//exit (1);
			continue;
		}
	
		ndpmfiles += pnbreplies;
    if ( ndpmfalloc <= ndpmfiles ) {
      ndpmfalloc *= 2;
      dpmfiles = realloc(dpmfiles, sizeof(dpmfiles)*ndpmfalloc);
    }
		
        for (i = 0; i < pnbreplies; i++) {
	  if ((pfilestatuses+i)->turl) {
	        u64tostru((pfilestatuses+i)->filesize, tmpbuf, 0);
		strncpy(&tmpbup[0], ctime(&((pfilestatuses+i)->pintime)), 24);
		tmpbup[24] = '\0';
		/*
                        printf ("PUT request, state[%d] = %s, SURL = %s, TURL = %s, fsz = %s, pin = %s \n", i,
                            f_stat[(pfilestatuses+i)->status >> 12],
				(pfilestatuses+i)->to_surl,
				(pfilestatuses+i)->turl,
				tmpbuf,
				//(pfilestatuses+i)->filesize,
				//ctime(&((pfilestatuses+i)->pintime)));
				tmpbup);
		*/

		//Cdpmfiles[fcount] = malloc(sizeof(dpmfile_t));
			dpmfiles[fcount] = calloc(1, sizeof(dpmfile_t));
			/*
			(*dpmfiles[fcount]).lfn = malloc(strlen(tmpbup)*(sizeof(char)));
			(*dpmfiles[fcount]).lfn = strdup(tmpbup);
			*/
			
			TRF_C(status, f_stat[(pfilestatuses+i)->status >> 12])
			TRF_C(rtok, (summaries+is)->r_token)
			TRF_C(surl, (pfilestatuses+i)->to_surl)
			TRF_C(turl, (pfilestatuses+i)->turl)
			TRF_C(fsiz, tmpbuf)
			TRF_C(pint, tmpbup)
			TRF_C(rtyp, "Put")
			  //TRF_C(cred, "041116-08:47:42")
			TRF_C(cred, crereqs[is])
			  //TRF_I(count, fcount)
    dpmfiles[fcount]->count = fcount;
			  /*
			  dpmfiles[fcount] = malloc(sizeof(dpmfile_t));
			  memcpy(dpmfiles[fcount], &dpmfilel, sizeof(dpmfile_t));
			*/
			
			fcount++;
		//(pfilestatuses+i)->pintime);
	  } 
                else if (((pfilestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
                        printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
                            f_stat[(pfilestatuses+i)->status >> 12],
                            (pfilestatuses+i)->status & 0xFFF,
                            (pfilestatuses+i)->errstring ? (pfilestatuses+i)->errstring : "");
                else
                        printf ("state[%d] = %s\n", i,
                            f_stat[(pfilestatuses+i)->status >> 12]);
                if ((pfilestatuses+i)->to_surl)
                        free ((pfilestatuses+i)->to_surl);
                if ((pfilestatuses+i)->turl)
                        free ((pfilestatuses+i)->turl);
                if ((pfilestatuses+i)->errstring)
                        free ((pfilestatuses+i)->errstring);
        }
	free (pfilestatuses);
	//free (pnbreplies);
	//pnbreplies = 0;
	  }
	  else if ( (summaries+is)->r_type == 'G' ) {
	    // GET state
		if ((status = dpm_getstatus_getreq ((summaries+is)->r_token, 
						    0, 
						    NULL,
						    &gnbreplies, 
						    &gfilestatuses
						    )) < 0) {
			sperror ("dpm_getstatus_getreq");
			//exit (1);
			continue;
		}
	
		ndpmfiles += gnbreplies;
    if ( ndpmfalloc <= ndpmfiles ) {
      ndpmfalloc *= 2;
      dpmfiles = realloc(dpmfiles, sizeof(dpmfiles)*ndpmfalloc);
    }

	for (i = 0; i < gnbreplies; i++) {
		if ((gfilestatuses+i)->status != DPM_READY)
			printf ("state[%d] = %d, errstring = <%s>\n", i,
			    (gfilestatuses+i)->status,
			    (gfilestatuses+i)->errstring);
		else {
#if 0
			printf ("GET request, state[%d] = %d, TURL = %s\n", i,
			    (gfilestatuses+i)->status,
			    (gfilestatuses+i)->turl);
#else
		u64tostru((gfilestatuses+i)->filesize, tmpbuf, 0);
		strncpy(&tmpbup[0], ctime(&((gfilestatuses+i)->pintime)), 24);
		tmpbup[24] = '\0';
		/*
                        printf ("GET request, state[%d] = %s, SURL = %s, TURL = %s, fsz = %s, pin = %s \n", i,
                            f_stat[(gfilestatuses+i)->status >> 12],
				(gfilestatuses+i)->from_surl,
				(gfilestatuses+i)->turl,
				tmpbuf,
				//(gfilestatuses+i)->filesize,
				//ctime(&((gfilestatuses+i)->pintime)));
				tmpbup);
		*/

		//Cdpmfiles[fcount] = malloc(sizeof(dpmfile_t));
			dpmfiles[fcount] = calloc(1, sizeof(dpmfile_t));
			/*
			dpmfiles[fcount]->lfn = malloc(strlen(tmpbup)*(sizeof(char)));
			dpmfiles[fcount]->lfn = strdup(tmpbup);
			*/
			
			TRF_C(status, f_stat[(gfilestatuses+i)->status >> 12])
			TRF_C(rtok, (summaries+is)->r_token)
			TRF_C(surl, (gfilestatuses+i)->from_surl)
			TRF_C(turl, (gfilestatuses+i)->turl)
			TRF_C(fsiz, tmpbuf)
			TRF_C(pint, tmpbup)
			TRF_C(rtyp, "Get")
			  //TRF_C(cred, "041116-08:47:42")
			TRF_C(cred, crereqs[is])
			  //TRF_I(count, fcount)
			  //dpmfiles[fcount]->count = malloc(sizeof(fcount));
    dpmfiles[fcount]->count = fcount;
			  /*
			  dpmfiles[fcount] = malloc(sizeof(dpmfile_t));
			  memcpy(dpmfiles[fcount], &dpmfilel, sizeof(dpmfile_t));
			*/
			fcount++;
#endif
		}
	}
	free (gfilestatuses);
	  }
	  else if ( (summaries+is)->r_type == 'C' ) {
	    // COPY state
	    printf ("COPY request, nothing implemented yet\n");
	  }
	  else {
	    // Undefined
	    printf ("UNKNOWN request, should not occur\n");
	  }
	}

	free (summaries);

	//dpmfiles = realloc(dpmfiles, sizeof(dpmfiles)*fcount);
	ndpmfiles = fcount;
	for (i = 0; i < (int)ndpmfiles; i++) {
	  fcount = i;
	  sfn = sfnfromsurl(dpmfiles[i]->surl); 
	  //Pprintf(" Array Control: %s %s \n", dpmfiles[i]->surl, sfn);
	  if ( ! sfn )
	    sfn = dpmfiles[i]->surl;
	  if (Cns_stat (sfn, &statbuf) < 0) {
	    //Pprintf (" Stat problem %s %s \n", dpmfiles[i]->surl, sfn);
	    TRF_C(fstat, "FileNotFound")
	  } else {
	    //Pprintf (" Stat OK for %s with status: %c \n", dpmfiles[i]->surl, statbuf.status);
	    formentry(dpmfiles[i]->surl, &statbuf, "slink", "comment", i);
	    TRF_C(fstat, "OK")
	  }
	}

  init_output(); /* must be between parser and output */

  lists_and_needs();

  simple_spewf();

	return 0;
}
