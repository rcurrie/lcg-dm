/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-testPutTwo.c,v 1.4 2007/07/19 15:53:25 grodid Exp $

// Created by  GG (10/03/2006)
// Last mod by GG (25/09/2006)

/*
 * echo $DPM_HOST $DPNS_HOST
 * lxb1727 lxb1727
 * ../../socket/dpmcli/dpm-testPutTwo --dpmutoken=gilbert_Mar10_Sfil001 --dpmproto rfio --dpmreqsize 100 ../../dpm/dpm_put.man /dpm/cern.ch/home/dteam/grodid/tglob/Mlxb1727/D2006-03-10/H113057/socket_Tfi001
 * ../../socket/dpmcli/dpm-testPutTwo --dpmfiles aaa,bbb --dpmsurls S1,S2,S3 --dpmfromsurls FS1,FS2,FS3 --dpmpaths thepath --dpmtosurls ts10,ts11 --dpmhost lxb1727 --dpmutoken=gilbert_Mar10_Sfil001 --dpmproto rfio --dpmreqsize 10 ../../srmv2/srmv2 /dpm/cern.ch/home/dteam/grodid/tglob/Mlxb1727/D2006-03-10/H113057/socket_Tfi055
 *
 * cd test/dpm
 * ../srmv2/srm2_testReserveSpace https://lxb1727.cern.ch:8444 gilbert_May18_Sfil093257 StSysInfo1 30000000 15000000 15000 0
 * set s_token=d86fd2d1-1df6-4f35-b281-e665772d2757        
 * ../../socket/dpmcli/dpm-testPutTwo --dpmstoken $s_token --dpmfiles aaa,bbb --dpmsurls S1,S2,S3 --dpmfromsurls FS1,FS2,FS3 --dpmpaths thepath --dpmtosurls ts10,ts11 --dpmhost lxb1727 --dpmutoken=gilbert_Mar10_Sfil001 --dpmproto rfio --dpmreqsize 10 ../../srmv2/srmv2 /dpm/cern.ch/home/dteam/grodid/tglob/Mlxb1727/D2006-03-10/H113057/socket_Tfi076
 *
 * For lxb1727 ............
 * srm2_testGetSpaceToken https://lxb1727.cern.ch:8446 gilbert_Aug06_Sfil123456 | grep s_token | awk '{print $2}' | xargs -n1 -i'{}' srm2_testReleaseSpace https://lxb1727.cern.ch:8446 {} StSysInfo1 1
 *
 * ../../socket/dpmcli/dpm-testPutTwo --dpmfiles ../../dpm/dpm,../../ns/dpnsdaemon,../../rfio/rfiod,../../srmv1/srmv1,../../srmv2/srmv2 --dpmsurls S1,S2,S3 --dpmfromsurls FS1,FS2,FS3 --dpmpaths thepath --dpmtosurls ts10,ts11 --dpmhost lxb1727 --dpmutoken=gilbert_Aug06_Sfil123456 --dpmproto rfio --dpmreqsize 10 ../../srmv2/srmv2 /dpm/cern.ch/home/dteam/subdirst15219/socket_Tfil060922a103 > socket_Tfil060922.log103
 *
 * For lxdpm01 ..............
 * srm2_testGetSpaceToken https://lxdpm01.cern.ch:8446 gilbert_Aug06_Sfil123456 | grep s_token | awk '{print $2}' | xargs -n1 -i'{}' srm2_testReleaseSpace https://lxdpm01.cern.ch:8446 {} StSysInfo1 1 
 *
 * ../../socket/dpmcli/dpm-testPutTwo --dpmfiles ../../dpm/dpm,../../ns/dpnsdaemon,../../rfio/rfiod,../../srmv1/srmv1,../../srmv2/srmv2 --dpmsurls S1,S2,S3 --dpmfromsurls FS1,FS2,FS3 --dpmpaths thepath --dpmtosurls ts10,ts11 --dpmhost lxdpm01 --dpmutoken=gilbert_Aug06_Sfil123456 --dpmproto rfio --dpmreqsize 10 ../../srmv2/srmv2 /dpm/cern.ch/home/dteam/subdirst7000/socket_Tfil060922a16 > & socket_lxdpm01_Tfil060922.log016
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "dpns_api.h"
#include "serrno.h"
#include "u64subr.h"
#define DEFPOLLINT 10

#include <regex.h>
#define DEFREGMAX  10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"
#include "gop_comm.h"

#include <fcntl.h>
#include "rfio.h"
#define BLKLEN 131072
#include <unistd.h>
#include <libgen.h>
#include <time.h>

static time_t current_time;
int nbpoolfsV = 0;
int nbpoolfsP = 0;
int nbpoolfsD = 0;
int nbpoolfsB = 0;
int nbpoolfsT = 0;

int nboSurls;
char **loSurls;
char **loTurls;
#if 0
static  int nboSurls;
static  char **loSurls;
static  char **loTurls;
#endif

#if 0
//********************************************************//
static int
sethost(pm) 
     char *pm;
{
	char hostenvm[256];
	char hostenvn[256];
	char *pn;

	if ( dpm_host ) {
	  printf ("    host: %s\n", dpm_host);
	  strcpy(hostenvm, "DPM_HOST=");
	  strcat(hostenvm, dpm_host);
	  putenv(hostenvm);
	  strcpy(hostenvn, "DPNS_HOST=");
	  strcat(hostenvn, dpm_host);
	  putenv(hostenvn);
	}
	//(void) time (&current_time);
	if ((pn = getenv ("DPNS_HOST")) && (pm = getenv ("DPM_HOST")) && strcmp(pn, pm)) {
	  printf ( " DPNS_HOST: %s and DPM_HOST: %s mismatch\n", pn, pm);
	  exit(USERR);
	} else {
	  printf ("DPM_HOST: %s\n", pm);
	}
	system("env");
	return 0;
}

//********************************************************//
static int
listdir(dir)
     char *dir;
{
	Cns_DIR *dirp;
	struct Cns_direnrep *dxp;

	if ((dirp = Cns_opendir (dir)) == NULL)
		return (-1);
	printf (" Contents of dir: %s\n\n", dir);
	while ((dxp = dpns_readdirxr (dirp, NULL)) != NULL) {
	  if ( dxp->nbreplicas )
	    printf ( " fileid filesize: %llu %llu %d %hu %s\n", dxp->fileid, dxp->filesize, dxp->nbreplicas, dxp->d_reclen, dxp->d_name);
#if 0
	  if ( dxp->nbreplicas && smax < dxp->filesize ) {
	    smax = dxp->filesize;
	    nmax = strdup(dxp->d_name);
	    imax = dxp->fileid;
	  }
#endif
	}
	(void) Cns_closedir (dirp);
	return 0;
}
#endif

//********************************************************//
static int
reservespace(u_token, s_token)
     char* u_token;
     char* s_token;
{
  int status;
	char tmpbut[25];
	char tmpbug[25];

	char  s_type;
	u_signed64 req_t_space;
	u_signed64 req_g_space;
	time_t req_lifetime;
	char actual_s_type;
	u_signed64 actual_t_space;
	u_signed64 actual_g_space;
	time_t actual_lifetime;

  // Setting defaults

	req_t_space = 200000000;
	req_g_space = 100000000;

 if ( ! dpm_lifet ) {
   dpm_lifet = 86400;
 }
 req_lifetime = dpm_lifet;
 /*
 if ( ! dpm_utoken ) {
   dpm_utoken = "unknownutoken";
 }
 strncpy(u_token, dpm_utoken, CA_MAXDPMTOKENLEN+1);
 */
 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   dpm_ftype = 'V';
 }
 s_type = dpm_ftype;

	if ((status = dpm_reservespace (s_type, 
					u_token,
			       req_t_space, 
			       req_g_space, 
					req_lifetime,
			       &actual_s_type, 
			       &actual_t_space, 
			       &actual_g_space, 
			       &actual_lifetime, 
			       s_token
			       )) < 0) {
		sperror ("dpm_reservespace");
		exit (1);
	}

	printf ("dpm_reservespace returned s_token: %s\n", s_token);

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	//strcpy(reqfiles[i].s_token, dpm_stoken);
	//strncpy(dpm_stoken, s_token, CA_MAXDPMTOKENLEN+1);
	//dpm_stoken = s_token;
	//dpm_stoken = strdup(s_token);
 
	if (status == DPM_FAILED)
		return (-1);
	else {
                u64tostru(actual_t_space, tmpbut, 0); 
                u64tostru(actual_g_space, tmpbug, 0); 
		printf ("dpm_reservespace provided actual_s_type: %c actual_t_space: %s actual_g_space: %s actual_lifetime: %d\n", actual_s_type,  tmpbut, tmpbug, actual_lifetime );
/*  		printf ("dpm_reservespace provided actual_s_type: %c actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", actual_s_type, actual_t_space, actual_g_space, actual_lifetime ); */
        }

	return (0);
}

//********************************************************//
static int
listreplica(surl)
     char *surl;
{

            int flags, status;
            Cns_list list;
            struct Cns_filereplica *lp;
	    struct Cns_filestat statbuf;

            flags = CNS_LIST_BEGIN;
            while ((lp = dpns_listreplica (surl, NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
                 //printf (" SFN = %s, FS = %s, %llu, %c\n", lp->sfn, lp->fs, lp->ptime, lp->status);
		 status = dpns_stat(surl, &statbuf);
                 printf (" SFN = %s, FS = %s, %c, size = %llu\n", lp->sfn, lp->fs, lp->f_type, statbuf.filesize);
		 //lpsfn = strdup(lp->sfn);
            }
            (void) dpns_listreplica (surl, NULL, CNS_LIST_END, &list);

	    return 0;
}

//********************************************************//
static int
listreplicag(surl)
     char *surl;
{

  //int flags;
  //Cns_list list;
  int nbentries;
  int i;
            struct Cns_filereplica *lp;
	    (void) Cns_getreplica (surl, NULL, NULL, &nbentries, &lp);
	    for (i=0; i<nbentries; i++) {
	      printf (" getreplica SFN = %s, FS = %s\n", (lp+i)->sfn, (lp+i)->fs);
	    }
	    free(lp);
#if 0
            flags = CNS_LIST_BEGIN;
            while ((lp = dpns_listreplica (surl, NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
                 //printf (" SFN = %s, FS = %s, %llu, %c\n", lp->sfn, lp->fs, lp->ptime, lp->status);
                 printf (" SFN = %s, FS = %s\n", lp->sfn, lp->fs);
		 //lpsfn = strdup(lp->sfn);
            }
            (void) dpns_listreplica (surl, NULL, CNS_LIST_END, &list);
#endif

	    return 0;
}

//********************************************************//
static int
listdird(dir, regex)
     char *dir;
     char *regex;  //  filename to match
{
	Cns_DIR *dirp;
	struct Cns_direnrep *dxp;
	struct Cns_filestat statbuf;
	char *path;

#if 1
	/* GG for regexp test */
	regex_t preg;
	regmatch_t offmatch[DEFREGMAX];
	int rc, re;
#endif

	if ((dirp = Cns_opendir (dir)) == NULL)
		return (-1);

	rc = regcomp(&preg, regex, REG_EXTENDED);
	printf ("regcomp returns: %d \n", rc);

	printf ("\n Contents of dir: %s matching with: %s\n\n", dir, regex);
	while ((dxp = dpns_readdirxr (dirp, NULL)) != NULL) {
	  if ( dxp->nbreplicas ) {
	    //printf ( " fileid filesize: %llu %llu %d %hu %s\n", dxp->fileid, dxp->filesize, dxp->nbreplicas, dxp->d_reclen, dxp->d_name);
#if 0
	  if ( dxp->nbreplicas && smax < dxp->filesize ) {
	    smax = dxp->filesize;
	    nmax = strdup(dxp->d_name);
	    imax = dxp->fileid;
	  }
#else 
	  path = malloc(sizeof(char)*(strlen(dir)+strlen(dxp->d_name)+2));
	  strcpy(path, dir);
	  strcat(path, "/");
	  strcat(path, dxp->d_name);
	  if (Cns_stat (path, &statbuf) < 0)
	    return (-1);

#if 0
	  //if ( current_time - statbuf.mtime < 40000 ) {
	  if ( current_time - statbuf.mtime < 1000 ) {
	    printf ( " fileid filesize: %llu %llu %d %hu %s %ld\n", dxp->fileid, dxp->filesize, dxp->nbreplicas, dxp->d_reclen, dxp->d_name, current_time - statbuf.mtime);
	    listreplica(path);
	  }
#else
	re = regexec(&preg, dxp->d_name, (size_t)DEFREGMAX, offmatch, 0);
	//printf ("regexec returns: %d \n", re);

	if ( ! re ) {
	    printf ( " fileid filesize: %llu %llu %d %hu %s %ld\n", dxp->fileid, dxp->filesize, dxp->nbreplicas, dxp->d_reclen, dxp->d_name, current_time - statbuf.mtime);
	    listreplica(path);
	}
#endif

#endif
	  }
	}
	(void) Cns_closedir (dirp);
	return 0;
}

//********************************************************//
#if 0
static int
rfstat(dest) 
     char *dest;
{
        struct stat statbuf;

        if (rfio_stat (dest, &statbuf) < 0) {
                perror ("rfio_stat");
                exit (1);
        }

        printf ("\nrfio_stat successful for: %s\n", dest);
        printf ("mode = %o\n", statbuf.st_mode);
        printf ("nlink = %d\n", statbuf.st_nlink);
        printf ("uid = %d\n", statbuf.st_uid);
        printf ("gid = %d\n", statbuf.st_gid);
        printf ("size = %ld\n", statbuf.st_size);

  printf (" \n");

	return 0;
}
#else
static int
rfstat(dest) 
     char *dest;
{
	return 0;
}
#endif

//********************************************************//
static int
rfwrite(src, dest)
     char *src;
     char *dest;
{

	int in_fd;
	int out_fd;
	int i;
	//char ibuf[BLKLEN];
	char obuf[BLKLEN];
	int rc;
	int count;

	//int in_fdc, out_fdc;
	//char* grogfal;
	//int ic;

        //struct stat statbuf;

	printf ("\nreading file %s\n", src);
	if ((in_fd = open (src, O_RDONLY)) < 0) {
		perror ("unix_open");
		exit (1);
	}
	printf ("unix open successful, in_fd = %d\n", in_fd);

	printf ("writing file %s\n", dest);
	if ((out_fd = rfio_open (dest, O_WRONLY|O_CREAT, 0644)) < 0) {
		perror ("rfio_open");
		exit (1);
	}
	printf ("rfio_open successful, out_fd = %d\n", out_fd);
	count = 0;

	while ((i = read(in_fd, obuf, BLKLEN)) > 0 ) {

	  count++;

	  if ((rc = rfio_write (out_fd, obuf, i)) != i) {
	    if (rc < 0)
	      perror ("rfio_write");
	    else
	      fprintf (stderr, "rfio_write %d returns %d\n", count, rc);
	    (void) rfio_close (out_fd);
	    exit (1);
	  }

	}
	printf ("rfio_write %d successful\n", count);

	if ((rc = close (in_fd)) < 0) {
		perror ("unix_close");
		exit (1);
	}
	printf ("unix close successful\n");

	if ((rc = rfio_close (out_fd)) < 0) {
		perror ("rfio_close");
		exit (1);
	}
	printf ("rfio_close successful\n");

#if 0
        if (rfio_stat (dest, &statbuf) < 0) {
                perror ("rfio_stat");
                exit (1);
        }

        printf ("\nrfio_stat successful\n");
        printf ("mode = %o\n", statbuf.st_mode);
        printf ("nlink = %d\n", statbuf.st_nlink);
        printf ("uid = %d\n", statbuf.st_uid);
        printf ("gid = %d\n", statbuf.st_gid);
        printf ("size = %ld\n", statbuf.st_size);
#endif

	rfstat(dest);

	return 0;
}

//********************************************************//
static int
putdone1(nbfiles, r_token, oSurl)
     int nbfiles;
     char *r_token;
     char *oSurl;
{
#include "dpmestat.h"
	struct dpm_filestatus *filestatuses;
	int i;
	//int nbfiles;
	//int nbprotocols;
	int nbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;
	char *AoFiles[0];

	AoFiles[0] = oSurl;

	if ((status = dpm_putdone (r_token,
				   nbfiles, 
				   AoFiles,
				   &nbreplies, 
				   &filestatuses
				   )) < 0) {
	  sperror ("dpm_putdone");
	  //printf ("==> Failure in dpm-putdone: ");
		printf ("==> Failure in dpm_putdone called from %s, nbreplies=%d\n", "putdone1", nbreplies);
		//if ( ! nbreplies ) 
		  exit (1);
	}

	printf ("putdone1 request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

	for (i = 0; i < nbreplies; i++) {
		if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : sstrerror((filestatuses+i)->status & 0xFFF));
		else if ((filestatuses+i)->surl)
			printf ("state[%d] = %s, SURL = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->surl);
		else
			printf ("state[%d] = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12]);
	}

	free (filestatuses);
	return 0;
}

//********************************************************//
static int
abort1(nbfiles, r_token, oSurl)
     int nbfiles;
     char *r_token;
     char *oSurl;
{
#include "dpmestat.h"
	struct dpm_filestatus *filestatuses;
	int i;
	//int nbfiles;
	//int nbprotocols;
	int nbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;
	char *AoFiles[0];

	AoFiles[0] = oSurl;

	//printf ("==> dpm_abortfiles called from %s, nbfiles=%d, %s\n", "abort1", nbfiles, AoFiles[0]);

	if ((status = dpm_abortfiles (r_token,
				   nbfiles, 
				   AoFiles,
				   &nbreplies, 
				   &filestatuses
				   )) < 0) {
	  sperror ("dpm_abortfiles");
	  //printf ("==> Failure in dpm-putdone: ");
		printf ("==> Failure in dpm_abortfiles called from %s, nbreplies=%d, %s, %d\n", "abort1", nbreplies, sstrerror(serrno), serrno);
		//if ( ! nbreplies ) 
		  return (-1);
	}

	//printf ("==> dpm_abortfiles called from %s, nbfiles=%d, %s\n", "abort1", nbfiles, AoFiles[0]);

	printf ("abort1 request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		return (-1);

	//printf ("==> dpm_abortfiles called from %s, nbreplies=%d, %s\n", "abort1", nbreplies, AoFiles[0]);

	for (i = 0; i < nbreplies; i++) {
	  //if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
#if 1
	  //if ((filestatuses+i)->status != DPM_ABORTED)
	  if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : sstrerror((filestatuses+i)->status & 0xFFF));
		else if ((filestatuses+i)->surl)
			printf ("state[%d] = %s, SURL = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->surl);
		else
			printf ("state[%d] = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12]);
#else
		if ((filestatuses+i)->status) 
		  printf ("state[%d] = %d\n", i, (filestatuses+i)->status);
		if ((filestatuses+i)->surl)
			printf ("state[%d]: SURL = %s\n", i, (filestatuses+i)->surl);
#endif
	}

	//printf ("==> dpm_abortfiles called from %s, nbreplies=%d, %s\n", "abort1", nbreplies, AoFiles[0]);

	free (filestatuses);
	return 0;
}

#if 0
//********************************************************//
static int
grfcp(src, dest)
     char *src;
     char *dest;
{
  char cmd[1024];
  int ret;
  sprintf (cmd, "rfcp %s %s\n", src, dest);
  ret = system(cmd);
  ret = WEXITSTATUS(ret);
  if ( ret ) {
    printf("[!]\trfcp copy failed!!\n");
    exit(1);
  }
  return 0;
}
#endif

//********************************************************//
static int
listreplicar(surl, oLpsfn)
     char *surl;
     char *oLpsfn;
{

            int flags;
            Cns_list list;
            struct Cns_filereplica *lp;

            flags = CNS_LIST_BEGIN;
            while ((lp = dpns_listreplica (surl, NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
                 printf (" SFN = %s, FS = %s\n", lp->sfn, lp->fs);
		 //oLpsfn = strdup(lp->sfn);
		 strcpy(oLpsfn, lp->sfn);
                 //printf (" SFNO= %s\n", oLpsfn);
            }
            (void) dpns_listreplica (surl, NULL, CNS_LIST_END, &list);

	    return 0;
}

//********************************************************//
static u_signed64
getpoolsize()
     //int* nbpoolfs;
{

  int status;
  int i, j;
	    struct dpm_pool *lpools;
	struct dpm_fs *dpm_fs;
	    int pnbreplies;
	    int nbfs;
	    //int nbfst = 0;
	    u_signed64 t_free_space = 0;

	if ((status = dpm_getpools (&pnbreplies, 
				    &lpools
				    )) < 0) {
		sperror ("dpm_getpools");
		exit (1);
	}

	printf ("getpoolsize request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

	printf ("Current pools: ");
	for (i = 0; i < pnbreplies; i++) {
	  /*
	  if ( lpools[i].s_type == 'V' )
	    continue;
	  */
	  printf(" %s", lpools[i].poolname);
	  t_free_space += lpools[i].free;

	  if ( ! nbpoolfsT ) {
		if (dpm_getpoolfs ((lpools + i)->poolname, &nbfs, &dpm_fs) < 0) {
			fprintf (stderr, "dpm_getpoolfs: %s\n", sstrerror(serrno));
			//errflag++;
		} else 
		  for (j = 0; j < nbfs; j++) 
		    if ( ! (dpm_fs + j)->status ) {
		      if ( lpools[i].s_type == 'V' || lpools[i].s_type == '-')
			nbpoolfsV++;
		      else
			nbpoolfsP++;
		    }
		  //nbfst += nbfs;
		if (nbfs)
			free (dpm_fs);
	  }
	}
	printf (" t_free_space: %llu \n", t_free_space);
	free(lpools);

	/*
	if (nbpoolfs)
	  *nbpoolfs = nbfst; //pnbreplies;
	  */
	if ( ! nbpoolfsT ) {
	  nbpoolfsT = nbpoolfsV + nbpoolfsP + nbpoolfsD + nbpoolfsB;
	}


	return (t_free_space);
}

//********************************************************//
static int
prepare2put(nbfiles, iSurl, flags, s_token, r_token, oTurl, oSurl)
int nbfiles;
char *iSurl;
char *oSurl;
//struct dpm_putfilereq *reqfiles;
//int nbprotos;
//char **lprotos;
//char u_token[CA_MAXDPMTOKENLEN+1];
//char *u_token;
int flags;
char *s_token;
//time_t retryt;
//char r_token[CA_MAXDPMTOKENLEN+1];
char *r_token;
char *oTurl;
//int *nbreplies;
//struct dpm_putfilestatus **filestatuses;
{

#include "dpmestat.h"
	struct dpm_putfilestatus *filestatuses;
	int i;
	//int nbfiles;
	//int nbprotocols;
	int nbreplies;
	//static char *protocols[] = {"rfio"};
	int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//char u_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_putfilereq *reqfiles;
	int status;
	int nbprotos;
	char **lprotos;

  if ( ! dpm_proto ) {
    printf ("dpm-testPutTwo: Protocol option is mandatory, --dpmproto=some_protocol \n");
    exit(1);
  } else {
    nbprotos = 1;
    lprotos = calloc (nbprotos, sizeof(char*));
    lprotos[0] = strdup(dpm_proto);
  }
	
	if ((reqfiles = calloc (nbfiles, sizeof(struct dpm_putfilereq))) == NULL) {
		perror ("calloc");
		exit (1);
	}

 for (i = 0; i < nbfiles; i++) {
   reqfiles[i].lifetime = dpm_lifet;
   
   if ( nbfiles-1 ) {
     reqfiles[i].to_surl = realloc(reqfiles[i].to_surl, sizeof(char)*(strlen(iSurl)+3));
     if ( nbfiles-5 )
       sprintf(reqfiles[i].to_surl, "%s_%d", iSurl, i);
     else
       sprintf(reqfiles[i].to_surl, "%s_%d", iSurl, i+2);
   }
   else
     reqfiles[i].to_surl = iSurl;
   
   reqfiles[i].f_type = dpm_ftype;  //  pb with 'V' ?
   //reqfiles[i].f_type = 'V';

   /* OLD BAD
   if ( dpm_stoken )
     strcpy(reqfiles[i].s_token, dpm_stoken);
   */
   if ( s_token )
     strcpy(reqfiles[i].s_token, s_token);

   reqfiles[i].requested_size = dpm_reqsize*1024*1024;
   if ( flags == 1 )
     reqfiles[i].requested_size += 1024*1024;

   if ( s_token )
     printf (" File: %s %d %s %c %llu\n", reqfiles[i].to_surl, reqfiles[i].lifetime, reqfiles[i].s_token, reqfiles[i].f_type, reqfiles[i].requested_size);
   else
     printf (" File: %s %d %c %llu\n", reqfiles[i].to_surl, reqfiles[i].lifetime, reqfiles[i].f_type, reqfiles[i].requested_size);

 }

	if ((status = dpm_put (nbfiles, 
			       reqfiles, 
			       nbprotos, 
			       lprotos, 
			       dpm_utoken,
			       flags, 
			       0, 
			       r_token, 
			       &nbreplies, 
			       &filestatuses
			       )) < 0) {
		sperror ("dpm_put");
		exit (1);
	}

	printf ("dpm_put returned r_token: %s\n", r_token);

	/* wait for request status "Done" or "Failed" */

	while (status == DPM_QUEUED || status == DPM_ACTIVE) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->to_surl)
				free ((filestatuses+i)->to_surl);
			if ((filestatuses+i)->turl)
				free ((filestatuses+i)->turl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
		printf("request state Pending\n");
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((status = dpm_getstatus_putreq (r_token, 0, NULL,
		    &nbreplies, &filestatuses)) < 0) {
			sperror ("dpm_getstatus_putreq");
			//exit (1);
                        if (status == DPM_FAILED)
                               break;
		}
	}
	printf ("prepare2put request status %d %d %d\n", status, DPM_DONE, DPM_SUCCESS);
	printf ("prepare2put request state %s\n", ( status == DPM_DONE || status == DPM_SUCCESS ) ? "Done" : "Failed");

	// loSurls & loTurls business
	if ( nbreplies ) {
	  //nboSurls = nbreplies;
	  nboSurls = 0;
	  loSurls = realloc (loSurls, sizeof(char*)*nbreplies);
	  loTurls = realloc (loTurls, sizeof(char*)*nbreplies);
	}

	//new
        for (i = 0; i < nbreplies; i++) {
	  if ((filestatuses+i)->turl) {
                        printf ("state[%d] = %s, TURL = %s\n", i,
                            f_stat[(filestatuses+i)->status >> 12],
                            (filestatuses+i)->turl);
			//oTurl = strdup((filestatuses+i)->turl);
			strcpy(oTurl, (filestatuses+i)->turl);
			strcpy(oSurl, (filestatuses+i)->to_surl);

			if ( ((filestatuses+i)->status >> 12) == 3 ) {
			  //nboSurls++;
			  loSurls[nboSurls] = strdup((filestatuses+i)->to_surl);
			  loTurls[nboSurls++] = strdup((filestatuses+i)->turl);
			  //printf (" nboSurls: %d %s %s\n", nboSurls-1, loSurls[nboSurls-1], loTurls[nboSurls-1]);
			}

	  }
                else if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
                        printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
                            f_stat[(filestatuses+i)->status >> 12],
                            (filestatuses+i)->status & 0xFFF,
                            (filestatuses+i)->errstring ? (filestatuses+i)->errstring : sstrerror((filestatuses+i)->status & 0xFFF));
                else
                        printf ("state[%d] = %s\n", i,
                            f_stat[(filestatuses+i)->status >> 12]);
                if ((filestatuses+i)->to_surl)
                        free ((filestatuses+i)->to_surl);
                if ((filestatuses+i)->turl)
                        free ((filestatuses+i)->turl);
                if ((filestatuses+i)->errstring)
                        free ((filestatuses+i)->errstring);
        }

	
	if (status == DPM_FAILED)
		exit (1);
	

	free (filestatuses);
	//return 0;
	return (status);
}

/* debugging output */
#define DEBUGP
#ifdef DEBUGP
#define PRINTF(format, args...) printf("\n--------------------------------"); printf (format, ## args); printf("--------------------------------\n\n"); fflush(stdout)
#else
#define PRINTF(format, args...)
#endif 

//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
  //#include "dpmestat.h"
  //struct dpm_putfilestatus *filestatuses;
	int i = 0;
	//int nbfiles;
	//int nbprotocols;
	//int nbreplies;
	//static char *protocols[] = {"rfio"};
	int r = 0;
	char r_token1[CA_MAXDPMTOKENLEN+1];
	char r_token2[CA_MAXDPMTOKENLEN+1];
	char r_token3[CA_MAXDPMTOKENLEN+1];
	char r_token4[CA_MAXDPMTOKENLEN+1];
	char r_token5[CA_MAXDPMTOKENLEN+1];
	////char r_token6[CA_MAXDPMTOKENLEN+1];
	char r_token7[CA_MAXDPMTOKENLEN+1];
	char r_token8[CA_MAXDPMTOKENLEN+1];
	//char u_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;
	char inputFile[CA_MAXPATHLEN+1];
	char iSurl[CA_MAXPATHLEN+1];
	char iSurl_WT[CA_MAXPATHLEN+1];
	char iSurl_WO[CA_MAXPATHLEN+1];
	char iSurl_v3[CA_MAXPATHLEN+1];
	char iSurl_v4[CA_MAXPATHLEN+1];
	char oSurl[CA_MAXPATHLEN+1];
	char turl1[CA_MAXPATHLEN+1];
	u_signed64 t_free_space1;
	char turl2[CA_MAXPATHLEN+1];
	char turl3[CA_MAXPATHLEN+1];
	char turl4[CA_MAXPATHLEN+1];
	char turl5[CA_MAXPATHLEN+1];
	////char turl6[CA_MAXPATHLEN+1];
	char turl7[CA_MAXPATHLEN+1];
	char turl8[CA_MAXPATHLEN+1];
	u_signed64 t_free_space2;
	u_signed64 t_free_space0;
	char lpsfn[CA_MAXPATHLEN+1];

	//int nbprotos;
	//char **lprotos;
	int nblsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;
	char hostenvm[256];
	char hostenvn[256];
	//char host[256];
	char *pn;
	char *pm = NULL;
	//time_t current_time;

	char *iDurl, *dirc;
	char s_token[CA_MAXDPMTOKENLEN+1];

	int nbpoolfsC = 0;

  int nbreqfiles;
  char **lfiles;
  int nbpaths;
  char **lpaths;
  int nbsurls;
  char **lsurls;
  int nbtosurls;
  char **ltosurls;
  int nbfromsurls;
  char **lfromsurls;

#if 0
	/* GG for regexp test */
	regex_t preg;
	regmatch_t offmatch[DEFREGMAX];
	const char* regex = "GlueCESEBind([C,S]E)(Uni(que[I-K])D)";
	int rc, re;
	int io;
	size_t elen;
	char ext[100];
	const char* strtomatch = "GlueCESEBindCEUniqueID";
#endif

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s [--dpmlifet=lifetime] (default=0) [--dpmftype=[V|P]] (default=V) (UNUSED) [--dpmstoken=spacetoken] (UNUSED) [--dpmutoken=usertoken] (default=\"unknownutoken\") [--dpmreqsize=filesize] (default=200Mb) --dpmproto=some_protocol (no default) inputFile SURL\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror ("dpm-put: at least one SURL must be provided ");
    exit(1);
  }
  nblsurls = nbargs - 1;
  //Pprintf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

  //sethost(host);
	if ( dpm_host ) {
	  printf ("    host: %s\n", dpm_host);
	  strcpy(hostenvm, "DPM_HOST=");
	  strcat(hostenvm, dpm_host);
	  putenv(hostenvm);
	  strcpy(hostenvn, "DPNS_HOST=");
	  strcat(hostenvn, dpm_host);
	  putenv(hostenvn);
	}
	(void) time (&current_time);
	if ((pn = getenv ("DPNS_HOST")) && (pm = getenv ("DPM_HOST")) && strcmp(pn, pm)) {
	  printf ( " DPNS_HOST: %s and DPM_HOST: %s mismatch\n", pn, pm);
	  exit(USERR);
	} else {
	  printf ("DPM_HOST: %s\n", pm);
	}

	// Loading inputs

  printf (" \n");

  nbreqfiles = 0;
  lfiles = NULL;
  item_store(DPM_FILE, &nbreqfiles, &lfiles);
  p_array("File", nbreqfiles, lfiles);

#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

  //V_LSTR(protos, PROT)
V_LSTR(paths, PATH)
V_LSTR(surls, SURL)
V_LSTR(tosurls, TSURL)
V_LSTR(fromsurls, FSURL)

  // Setting defaults

  if ( ! dpm_proto ) {
    printf ("dpm-put: Protocol option is mandatory, --dpmproto=some_protocol \n");
    exit(1);
  }
#if 0
  if ( ! dpm_proto ) {
    printf ("dpm-put: Protocol option is mandatory, --dpmproto=some_protocol \n");
    exit(1);
  } else {
    nbprotos = 1;
    lprotos = calloc (nbprotos, sizeof(char*));
    lprotos[0] = strdup("rfio");
  }
	
	if ((reqfiles = calloc (nblsurls, sizeof(struct dpm_putfilereq))) == NULL) {
		perror ("calloc");
		exit (1);
	}
#endif

 if ( ! dpm_reqsize ) {
   dpm_reqsize = 200; // Mbytes
 }
 if ( ! dpm_lifet ) {
   dpm_lifet = 0;
 }
#if 0
 if ( ! dpm_stoken ) {
   dpm_stoken = "dontknowyet";
 }
#endif
 if ( ! dpm_utoken ) {
   dpm_utoken = "unknownutoken";
 }
 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   dpm_ftype = 'V';
 }

 strcpy(inputFile, remargs[0]);
 strcpy(iSurl, remargs[1]);
 //inputFile = strdup(remargs[0]);
 //iSurl     = strdup(remargs[1]);

#if 0
 for (i = 0; i < nblsurls; i++) {
   reqfiles[i].lifetime = dpm_lifet;
   reqfiles[i].to_surl = remargs[i+1];
   reqfiles[i].f_type = dpm_ftype;  //  pb with 'V' ?
   //reqfiles[i].f_type = 'V';
   strcpy(reqfiles[i].s_token, dpm_stoken);
   reqfiles[i].requested_size = dpm_reqsize*1024*1024;
   printf (" File: %s %d %s %c \n", reqfiles[i].to_surl, reqfiles[i].lifetime, reqfiles[i].s_token, reqfiles[i].f_type);
   //printf (" File: %s \n", reqfiles[i].to_surl);
 }
#endif

  printf (" \n");

  //system("env");
  //exit(0);

  t_free_space0 = getpoolsize();

  PRINTF ("\n Trying to get reserved space\n");
  status = reservespace(dpm_utoken, s_token);

  t_free_space1 = getpoolsize();

  //BADstrcpy(s_token, NULL);

  printf (" reservespace u_token s_token: %s %s space used: %llu nbpoolfsT: %d\n", dpm_utoken, s_token, t_free_space0-t_free_space1, nbpoolfsT);
  //printf (" reservespace u_token s_token: %s %s space used: %llu nbpoolfs: %d\n", dpm_utoken, dpm_stoken, t_free_space0-t_free_space1, nbpoolfs);

  //--------------------------------------------------------------------------
  strcpy(iSurl_WO, iSurl);
  strcat(iSurl_WO, "_WO");
  t_free_space0 = t_free_space1;

  PRINTF ("\n Trying to 1st prepare2put WITHOUT s_token: %s with %d files\n", iSurl_WO, nblsurls);
  /* zeroing s_token or not */
  //status = prepare2put(nblsurls, iSurl, 0, s_token, r_token1, turl1, oSurl);
  status = prepare2put(nblsurls, iSurl_WO, 0, NULL, r_token1, turl1, oSurl);
  /* */

  t_free_space1 = getpoolsize();

  printf (" prepare2put1: %s %s space used: %llu nbpoolfsT: %d\n", r_token1, turl1, t_free_space0-t_free_space1, nbpoolfsT);
  //exit(0);

  //grfcp (inputFile, turl1);
  //l rfwrite (inputFile, turl1);
  rfwrite (lfiles[4], turl1);

  PRINTF ("\n Trying to putdone: %s \n", iSurl_WO);
  putdone1(nblsurls, r_token1, iSurl_WO);

  PRINTF ("\n Trying to listreplica: %s \n", iSurl_WO);
  listreplica(iSurl_WO);

  rfstat(iSurl_WO);

  //--------------------------------------------------------------------------
  strcpy(iSurl_WT, iSurl);
  strcat(iSurl_WT, "_WT");

  t_free_space1 = getpoolsize();
  t_free_space0 = t_free_space1;

  PRINTF ("\n Trying to 1st prepare2put WITH s_token: %s with %d files\n", iSurl, nblsurls);
  /* zeroing s_token or not */
  status = prepare2put(nblsurls, iSurl, 0, s_token, r_token1, turl1, oSurl);
  //status = prepare2put(nblsurls, iSurl, 0, NULL, r_token1, turl1, oSurl);
  /* */

  t_free_space1 = getpoolsize();

  printf (" prepare2put1: %s %s space used: %llu nbpoolfsT: %d\n", r_token1, turl1, t_free_space0-t_free_space1, nbpoolfsT);
  //exit(0);

  //grfcp (inputFile, turl1);
  //l rfwrite (inputFile, turl1);
  rfwrite (lfiles[0], turl1);

  PRINTF ("\n Trying to putdone: %s \n", iSurl);
  putdone1(nblsurls, r_token1, iSurl);

  PRINTF ("\n Trying to listreplica: %s \n", iSurl);
  listreplica(iSurl);

  rfstat(iSurl);

  //exit(0);

#if 1
  t_free_space1 = getpoolsize();

  PRINTF ("\n Trying to 2nd prepare2put with overwriting: %s with %d files\n", iSurl, nblsurls);
  status = prepare2put(nblsurls, iSurl, 4, s_token, r_token2, turl2, oSurl);
  //l status = prepare2put(nblsurls, iSurl, 1, r_token2, turl2, oSurl);

  t_free_space2 = getpoolsize();

  printf (" prepare2put2: %s %s space used: %llu \n", r_token2, turl2, t_free_space1-t_free_space2);

  //grfcp (inputFile, turl2);
  //l rfwrite (inputFile, turl2);
  rfwrite (lfiles[0], turl2);

  PRINTF ("\n Trying to putdone: %s \n", iSurl);
  putdone1(nblsurls, r_token2, iSurl);

  rfstat(iSurl);
#endif

#if 1
  PRINTF ("\n Trying to getreplica  to get the sfn for: %s\n", iSurl);
  //(void) Cns_getreplica (iSurl, NULL, NULL, &nbentries, &lp);
  listreplicag(iSurl);
#endif

  PRINTF ("\n Trying to listreplica to get the sfn: %s\n", iSurl);
  listreplicar(iSurl, lpsfn);

  //printf (" after listreplicar: %s \n", lpsfn);

  t_free_space1 = getpoolsize();

  PRINTF ("\n Testing dpm_delreplica: %s \n", lpsfn);
  
			if ((status = dpm_delreplica (lpsfn)) < 0) {
			  sperror ("dpm_delreplica");
			  exit (1);
			}
  
  t_free_space2 = getpoolsize();

  //exit (0);

  printf (" Space freed after dpm_delreplica: %llu \n", t_free_space2-t_free_space1);

  PRINTF ("\n Trying to listreplica: %s \n", iSurl);
  listreplica(iSurl);

  PRINTF ("\n Trying a 2nd prepare2put with overwriting to create a 2nd replica: %s with %d files\n", iSurl, nblsurls);
  status = prepare2put(nblsurls, iSurl, 4, s_token, r_token7, turl7, oSurl);
  rfwrite (lfiles[0], turl7);
  putdone1(nblsurls, r_token7, iSurl);

  printf ("\n Trying to listreplica: %s \n", iSurl);
  listreplica(iSurl);

  PRINTF ("\n Trying a 3rd prepare2put with forced overwriting (different size): %s with %d files\n", iSurl, nblsurls);
  status = prepare2put(nblsurls, iSurl, 1, s_token, r_token8, turl8, oSurl);
  rfwrite (lfiles[1], turl8);
  putdone1(nblsurls, r_token8, iSurl);

  printf ("\n Trying to listreplica: %s \n", iSurl);
  listreplica(iSurl);

  if ( nbpoolfsT > 1 ) {
    PRINTF ("\n Trying a 4th prepare2put with overwriting to create a 2nd replica: %s with %d files\n", iSurl, nblsurls);
    status = prepare2put(nblsurls, iSurl, 4, s_token, r_token7, turl7, oSurl);

    if ( ! status) {
      rfwrite (lfiles[1], turl7);
      putdone1(nblsurls, r_token7, iSurl);

      printf ("\n Trying to listreplica: %s \n", iSurl);
      listreplica(iSurl);
    }
  }


  if ( nbpoolfsT > 2 ) {
    PRINTF ("\n Trying a 5th prepare2put with overwriting to create a 3rd replica: %s with %d files\n", iSurl, nblsurls);
    status = prepare2put(nblsurls, iSurl, 4, s_token, r_token8, turl8, oSurl);

  /*
  if (status) {
    dpm_ftype = 'P';
    status = prepare2put(nblsurls, iSurl, 4, s_token, r_token8, turl8, oSurl);
    if (status)
      exit (1);
    dpm_ftype = 'V';
  }
  */

    if ( ! status) {
      rfwrite (lfiles[1], turl8);
      putdone1(nblsurls, r_token8, iSurl);

      printf ("\n Trying to listreplica: %s \n", iSurl);
      listreplica(iSurl);
    }
  }


  //--------------------------------------------------------------------------
  nblsurls = 3;
  //nblsurls = 1;
  strcpy(iSurl_v3, iSurl);
  strcat(iSurl_v3, "_v3");

  PRINTF ("\n Trying to prepare2put: %s with %d files\n", iSurl_v3, nblsurls);
  status = prepare2put(nblsurls, iSurl_v3, 0, s_token, r_token3, turl3, oSurl);

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);

  t_free_space1 = getpoolsize();

  PRINTF ("\n Trying to abort file: %s \n", oSurl);

  status = abort1(1, r_token3, oSurl);

  t_free_space2 = getpoolsize();

  printf (" Space freed after dpm_abortfiles: %llu \n", t_free_space2-t_free_space1);

  PRINTF ("\n Trying to abort req: %s \n", r_token3);

        if ((status = dpm_abortreq (r_token3)) < 0) {
                sperror ("dpm_abortreq");
                exit (1);
        }

        printf ("abortreq request state %d %s\n", status, status == DPM_SUCCESS ? "Done" : "Failed");
        if (status == DPM_FAILED)
                exit (1);

  t_free_space1 = getpoolsize();

  printf (" Space freed after dpm_abortreq: %llu \n", t_free_space1-t_free_space2);

  //-------------------------------------------------------------------
  nblsurls = 4;
  //nblsurls = 1;
  strcpy(iSurl_v4, iSurl);
  strcat(iSurl_v4, "_v4");

  PRINTF ("\n Trying to prepare2put: %s with %d files\n", iSurl_v4, nblsurls);
  status = prepare2put(nblsurls, iSurl_v4, 0, s_token, r_token4, turl4, oSurl);

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);

  printf ("\n Trying to listreplicaL: %s \n", oSurl);
  for (r=0; r<nboSurls; r++) {
    listreplica(loSurls[r]);
    //(void) fflush(NULL);
  }

#if 1
  rfwrite (inputFile, turl4);

  PRINTF ("\n Trying to putdone: %s \n", oSurl);
  putdone1(1, r_token4, oSurl);

  rfstat(oSurl);

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);
#endif

  t_free_space1 = getpoolsize();

  PRINTF ("\n Trying to abort file: %s \n", oSurl);
  status = abort1(1, r_token4, oSurl);

  t_free_space2 = getpoolsize();

  printf (" Space freed after dpm_abortfiles: %llu \n", t_free_space2-t_free_space1);

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);

#if 1
  for (r=0; r<nboSurls-1; r++) {
    PRINTF ("\n Trying to putdoneL: %d %s \n", r, loSurls[r]);
    rfwrite (lfiles[r], loTurls[r]);
    putdone1(1, r_token4, loSurls[r]);
    listreplica(loSurls[r]);
  }
#endif

  //-------------------------------------------------------------------
  nblsurls = 5;

  nbpoolfsC++;
  PRINTF ("\n Trying to prepare2put: %s with %d files\n", iSurl_v4, nblsurls);
  status = prepare2put(nblsurls, iSurl_v4, 0, s_token, r_token5, turl5, oSurl);

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);

  printf ("\n Trying to listreplicaL: %s \n", oSurl);
  for (r=0; r<nboSurls; r++) {
    listreplica(loSurls[r]);
    //(void) fflush(NULL);
  }

#if 0
  rfwrite (inputFile, turl5);

  PRINTF ("\n Trying to putdone: %s \n", oSurl);
  putdone1(1, r_token5, oSurl);

  rfstat(oSurl);

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);
#endif

#if 1
  //PRINTF ("\n Trying to putdoneL: %s \n", oSurl);
  for (r=0; r<nboSurls; r++) {
    PRINTF ("\n Trying to putdoneL: %d %s \n", r, loSurls[r]);
    rfwrite (lfiles[r], loTurls[r]);
    putdone1(1, r_token5, loSurls[r]);
    listreplica(loSurls[r]);
  }
#endif

  //grfcp (inputFile, turl3);

  //putdone1(1, r_token3, oSurl);

  
  if ( nbpoolfsT > 1 ) {
    if ( nbpoolfsC++ >= nbpoolfsV )
      dpm_ftype = 'P';
  PRINTF ("\n Trying to replicate file: %s \n", oSurl);
  
	if (dpm_replicate (oSurl, dpm_ftype) < 0) {
		fprintf (stderr, "dpm_replicate %s: %s\n", oSurl,
		    sstrerror(serrno));
		exit (USERR);
	}

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);
  }
  
  if ( nbpoolfsT > 2 ) {
    if ( nbpoolfsC++ >= nbpoolfsV )
      dpm_ftype = 'P';
  PRINTF ("\n Trying to create a 3rd replicate of file: %s \n", oSurl);
  
	if (dpm_replicate (oSurl, dpm_ftype) < 0) {
		fprintf (stderr, "dpm_replicate %s: %s\n", oSurl,
		    sstrerror(serrno));
		exit (USERR);
	}

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);

  PRINTF ("\n Trying to getreplica  to get the sfn for: %s\n", oSurl);
  listreplicag(oSurl);
  }
  
#if 1
  if ( nbpoolfsT > 3 ) {
  PRINTF ("\n Trying to create a 4th replicate of file: %s \n", oSurl);
  
	if (dpm_replicate (oSurl, dpm_ftype) < 0) {
		fprintf (stderr, "dpm_replicate %s: %s\n", oSurl,
		    sstrerror(serrno));
		exit (USERR);
	}

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);

  PRINTF ("\n Trying to getreplica  to get the sfn for: %s\n", oSurl);
  listreplicag(oSurl);
  }

  if ( nbpoolfsT > 4 ) {
  PRINTF ("\n Trying to create a 5th replicate of file: %s \n", oSurl);
  
	if (dpm_replicate (oSurl, dpm_ftype) < 0) {
		fprintf (stderr, "dpm_replicate %s: %s\n", oSurl,
		    sstrerror(serrno));
		exit (USERR);
	}

  printf ("\n Trying to listreplica: %s \n", oSurl);
  listreplica(oSurl);
  }
#endif
  
  
	(void) time (&current_time);
  dirc = strdup(iSurl);
  iDurl  = dirname(dirc);
  //free(dirc);
  dirc = strdup(iSurl);
  status = listdird(iDurl, basename(dirc));
  //free(dirc);

#if 0
	if (dpm_regex) {
	/*
	regex_t preg;
	regmatch_t offmatch[DEFREGMAX];
	const char* regex = "GlueCESEBind([C,S]E)(Uni(que[I-K])D)";
	*/

	rc = regcomp(&preg, regex, REG_EXTENDED);
	printf ("regcomp returns: %d \n", rc);

	//OK	int re = regexec(&preg, strtomatch, (size_t)0, (regmatch_t *)NULL, 0);
	re = regexec(&preg, strtomatch, (size_t)DEFREGMAX, offmatch, 0);
	printf ("regexec returns: %d \n", re);

	for (io=0; io<DEFREGMAX; io++) {
	  if ( offmatch[io].rm_so == -1 )
	    break;
	  //good printf (" Offset #%d: %d %d \n", io, offmatch[io].rm_so, offmatch[io].rm_eo);
	  //ext = strncpy(ext, strtomatch[offmatch[io].rm_so], 5);
	  elen = offmatch[io].rm_eo-offmatch[io].rm_so;
	  strncpy(ext, &strtomatch[offmatch[io].rm_so], elen);
	  ext[elen] = '\0';
	  printf (" Offset #%d: %d %d %s \n", io, offmatch[io].rm_so, offmatch[io].rm_eo, ext);
	}

	regfree(&preg);
	}
#endif

  exit(status);
		      
}
