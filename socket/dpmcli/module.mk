### 
### Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
### All rights reserved
### 

# $Id: module.mk,v 1.12 2006/12/19 20:17:29 grodid Exp $

prefix = /opt/lcg
prefixcli = $(prefix)/test/dpm-socket/
prefixcli = $(prefix)/test/sbin/

# This file gets included into the main Makefile, in the top directory.

INSTALL += $(bin)dpm_testgnu $(man1)dpm_testgnu.1

# files to remove
CLEAN += dpmcli/dpm_testgnu dpmcli/dpm-get dpmcli/debug

# a directory for cleaning
DIRS += dpmcli/

# files to create ###########################################
##ALL += dpmcli/dpm_testgnu dpmcli/dpm-get

## testing
####EXE_N := dpm_testgnu
## basic
EXE_N := dpm-get dpm-put dpm-copy 
EXE_N += dpm-putdone dpm-rm dpm-Rm dpm-Ls
## file management
EXE_N += dpm-relfiles dpm-extendfilelife
## file status
EXE_N += dpm-getreqsummary dpm-getreqstatus
## req management
EXE_N += dpm-getreqid dpm-abortreq
## fs management
EXE_N += dpm-addfs dpm-modifyfs dpm-getpoolfs
## pool management
EXE_N += dpm-getpools dpm-getprotocols
EXE_N += dpm-delreplica
## Not tested
##laterEXE_N += dpm-replicate
## space management
EXE_N += dpm-resp dpm-gspt dpm-spmd dpm-upsp dpm-rlsp
## misc
EXE_N += dpm-getusrbynam dpm-getgrpbynam

EXE_P := dpm-testPutTwo dpm-listReplicas dpm-purgeFs
##EXE_P += dpm-Drain dpm-Replicate

##short EXE_N := dpm_testgnu dpm-get
EXEDPN := $(addprefix dpmcli/,$(EXE_N))
EXEDPP += $(addprefix dpmcli/,$(EXE_P))

EXEDPM := $(EXEDPN)
EXEDPM += $(EXEDPP)
EXEDOBJ := $(addsuffix .o,$(EXEDPM))
EXEDSRC := $(addsuffix .c,$(EXEDPM))

EXEDBIN := $(addprefix $(prefixcli),$(EXE_N))
EXEDBIN += $(addprefix $(prefixcli),$(EXE_P))

#LIBS += -L/usr/local/lib -lshift
##GOODLIBS += -Ldpmmain -ldpm -L/usr/local/lib -lshift
##PREVGOOD LIBDPM += -Lproc -lproc -Ldpmmain -ldpm -L/usr/local/lib -lshift
##001 LIBDPM += -L/afs/cern.ch/user/b/baud/w0/DPNS_MYSQL/lib -llfc -Lproc -lproc -Ldpmmain -ldpm -L/usr/local/lib -lshift
##002 LIBDPM += -Ldpmmain/../ns -lns -Lproc -lproc -Ldpmmain -ldpm -L/usr/local/lib -lshift
#laterLIBDPM += -Ldpmmain -ldpm -L/usr/local/lib -lshift
##NOTENOUGHLIBDPM += dpmmain/dpm_util.o

##3GOOD LD_DPM := -Lgoparse -lgoparse
##6LD_DPM += -Lgoputils -lgoputils -Ldpmmain/../lib -ldpm -L/usr/local/lib -lshift
##6LIBDPM  := dpmmain/../lib/libdpm.a
##6LIBPROG := goputils/libgoputils.a
##??LD_DPM += -pthread
##CSECLD_DPM += -Lgoputils -lgoputils -L../lib -ldpm -L/usr/local/lib -lCsec -lshift
##CSECLD_DPM +=  -lCsec -ldl

GLOBUS_LOCATION=/opt/globus

GLOBUS_FLAVOUR=gcc32dbgpthr

GLOBUS_LIBS=-L$(GLOBUS_LOCATION)/lib -lglobus_gssapi_gsi_$(GLOBUS_FLAVOUR) -lglobus_gss_assist_$(GLOBUS_FLAVOUR)
GGC_LIB=-L$(GLOBUS_LOCATION)/lib -lglobus_gass_copy_$(GLOBUS_FLAVOUR)

CSECFLAG = -DCSEC
LIBCSEC += $(GLOBUS_LIBS)

LIBCSEC += -L/usr/local/lib -ldl

OLD_DPM += -Lgoputils -lgoputils -L../lib -ldpm -L/usr/local/lib -ldl
LD_DPM += -Lgoputils -lgoputils -L../lib -ldpm $(LIBCSEC)

##??LD_DPM += -lnsl -luuid
LIBDPM  := ../lib/libdpm.a
LIBPROG := goputils/libgoputils.a

##4DPMINC += -Igoparse -I./ 
##4DPMINC += -Idpmh 

##PS_C    := global help output parser select sortformat
PS_C    := global help output parser select sortformat
PSNAMES := $(addprefix goparse/,$(PS_C))
PSOBJ   := $(addsuffix .o,$(PSNAMES))
PSSRC   := $(addsuffix .c,$(PSNAMES))

##LATER DPM_C    := dpm_testgnu dpm-get dpm-put
##NEW2DPM_C    := dpm_testgnu dpm-get
DPM_C    := dpm_utils
DPMNAMES := $(addprefix dpmcli/,$(DPM_C))
DPMOBJ   := $(addsuffix .o,$(DPMNAMES))
DPMSRC   := $(addsuffix .c,$(DPMNAMES))

## The executables to create
##NEW2ALL += $(DPMNAMES)
ALL += $(EXEDPM)

PS_X := COPYING HACKING TRANSLATION common.h module.mk it p goparse.1 regression
TARFILES += $(PSSRC) $(addprefix dpmcli/,$(PS_X))

######dpmcli/dpm_testgnu: $(DPMOBJ) $(PSOBJ) $(LIBPROC)
#######	echo $@ $^ $(LIBPROC)
######	echo $^
######	echo $(LIBPROC)
######	$(CC) $(ALL_CFLAGS) $(ALL_LDFLAGS) $(LIBS) -o $@ $^ $(LIBPROC)
########	$(CC) $(ALL_CFLAGS) $(ALL_LDFLAGS) $(LIBS) -o $@ $^ $(LIBPROC)

#dpmcli/dpm-get: %: %.o $(PSOBJ) $(LIBPROC)
##dpmcli/dpm_testgnu dpmcli/dpm-get: %: %.o $(PSOBJ) $(LIBPROC)
##NEW2$(ALL): %: %.o $(PSOBJ) $(LIBPROC)

##NOTGOODLIBPROC += $(LIBDPM)

##NOTSOGOOD$(EXEDPM): %: %.o $(DPMOBJ) $(PSOBJ) $(LIBPROC) $(LIBDPM)
##1GOOD$(EXEDPM): %: %.o $(DPMOBJ) $(PSOBJ) $(LIBPROC)
##4$(EXEDPM): %: %.o $(DPMOBJ) $(PSOBJ) $(LIBPROC) $(LIBDPM)
$(EXEDPN): %: %.o $(LIBPROG) $(LIBDPM)
##	@echo " "
	@echo "========================P" $@
##	@echo "=^^=" $^
##	@echo "=<<=" $<
##	@echo "=--=" $(LIBPROG) "=--=" $(LD_DPM)
	$(CC) -o $@ $(ALL_CFLAGS) $< $(ALL_LDFLAGS) $(OLD_DPM)

$(EXEDPP): %: %.o $(LIBPROG) $(LIBDPM)
	@echo "========================N" $@
	$(CC) -o $@ $(ALL_CFLAGS) $< ../dpm/dpm_replicate.o ../dpm/dpm_copyfile.o $(GGC_LIB) $(ALL_LDFLAGS) $(LD_DPM)

$(prefixcli)%: dpmcli/%
#good	cp $(notdir $@) $@
#GOOD	cp dpmcli/$(@F) $@
	cp $? $@

install: $(EXEDBIN)

install.man:

# This just adds the stacktrace code
dpmcli/debug: $(PSOBJ) dpmcli/stacktrace.o $(LIBPROC)
	$(CC) $(ALL_CFLAGS) $(ALL_LDFLAGS) -o $@ $^ -lefence

##$(PSOBJ): %.o: %.c goparse/common.h $(LIBPROC)
##	$(CC) -c $(ALL_CPPFLAGS) $(ALL_CFLAGS) $< -o $@

##NEW2$(DPMOBJ): %.o: %.c goparse/common.h $(LIBPROC)
##NEW2	$(CC) -c $(ALL_CPPFLAGS) $(DPMINC) $(ALL_CFLAGS) $< -o $@

##$%.o : %.c goparse/common.h
%.o : %.c goputils/common.h
	@echo " "
	@echo ">>>>>>>>>>>>" $@
	$(CC) $(ALL_CPPFLAGS) $(DPMINC) $(ALL_CFLAGS) -o $@ -c $<

dpmcli/stacktrace.o: dpmcli/stacktrace.c
##gg	$(CC) -c $(ALL_CPPFLAGS) $(ALL_CFLAGS) $< -o $@

$(bin)dpm_testgnu: dpmcli/dpm_testgnu
	$(install) --mode a=rx $< $@

$(man1)dpm_testgnu.1 : dpmcli/dpm_testgnu.1
	$(install) --mode a=r $< $@
	-rm -f $(DESTDIR)/var/catman/cat1/dpm_testgnu.1.gz $(DESTDIR)/var/man/cat1/dpm_testgnu.1.gz

clobber : FORCE
	rm -f $(EXEDPM)

usage : $(filter dpm-%,$(EXE_N))
	@echo ">> Usage Main: >>" $?

$(EXE_N) : FORCE
	@echo "========================================" 
	@echo " "
	@echo ">> Usage: >>" dpmcli/$@
	dpmcli/$@

FORCE:
