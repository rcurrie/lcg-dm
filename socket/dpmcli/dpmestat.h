/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpmestat.h,v 1.2 2005/02/25 04:37:19 grodid Exp $

        static char *f_stat[] = {"Success", 
				 "Queued", 
				 "Active", 
				 "Ready", 
				 "Running", 
				 "Done", 
				 "Failed", 
				 "Aborted",
				 "Expired", 
				 "Released"
	};
