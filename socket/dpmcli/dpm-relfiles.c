/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-relfiles.c,v 1.3 2007/07/19 15:53:25 grodid Exp $

// Created by GG (03/11/2004)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
#include "dpmestat.h"
	struct dpm_filestatus *filestatuses;
	int i;
	int nbfiles;
	//int nbprotocols;
	int nbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;

	//int nbprotos;
	//char **lprotos;
	int nbsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
	  fprintf (stderr, "usage: %s  \
[--dpmkeepspace=kflag] (default=0)     \
--dpmrtoken=request_token (no default) \
list-of-SURLs\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    printf ("dpm-relfiles: at least one SURL must be provided \n");
    exit(1);
  }
  nbfiles = nbargs;
  //Pprintf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

 if ( ! dpm_keepspace ) {
   dpm_keepspace = 0;  //  was 0
 }
 //Pprintf (" \n");

#if 0
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROTL)
#endif

  // Setting defaults

  if ( ! dpm_rtoken ) {
    printf ("dpm-relfiles: Reqid token option is mandatory, --dpmrtoken=request_token \n");
    exit(1);
  }
 nbsurls = nbargs;

 //printf (" \n");

	if ((status = dpm_relfiles (dpm_rtoken,
				   nbfiles, 
				   remargs,
				    dpm_keepspace,
				   &nbreplies, 
				   &filestatuses
				   )) < 0) {
	  sperror ("dpm_relfiles");
	  /*
                if ( ! nbreplies )
                    printf ("Non-existing SURL: 
	  */
		printf ("==> Failure in dpm_relfiles called from %s, nbreplies=%d\n", argv[0], nbreplies);
                //Risky if ( ! nbreplies ) 
		    exit (1);
	}

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

	for (i = 0; i < nbreplies; i++) {
#if 0 
	  if ((filestatuses+i)->status != DPM_READY)
	  //if ((filestatuses+i)->status != DPM_DONE)
			printf ("state[%d] = %s, errstring = <%s>\n", i,
				f_stat[(filestatuses+i)->status >> 12],
				//(filestatuses+i)->status,
			    (filestatuses+i)->errstring);
		else
			printf ("state[%d] = %s, SURL = %s\n", i,
				f_stat[(filestatuses+i)->status >> 12],
				//(filestatuses+i)->status,
			    (filestatuses+i)->surl);
#elsif 0
		if ((filestatuses+i)->surl)
			printf ("state[%d] = %s, SURL = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->surl);
		else if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : "");
		else
			printf ("state[%d] = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12]);
#else
		if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : sstrerror((filestatuses+i)->status & 0xFFF));
		else if ((filestatuses+i)->surl)
			printf ("state[%d] = %s, SURL = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->surl);
		else
			printf ("state[%d] = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12]);
#endif
	}

	free (filestatuses);
	return 0;

}
