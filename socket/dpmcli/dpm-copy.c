/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-copy.c,v 1.2 2007/07/19 15:53:24 grodid Exp $

// Created by GG (21/10/2004)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

	struct dpm_copyfilestatus *filestatuses;
	int i;
	int nbfiles;
	int nbprotocols;
	int nbreplies;
	static char *protocols[] = {"rfio"};
	int r = 0;
	char r_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_copyfilereq *reqfiles;
	int status;

	int nbprotos;
	char **lprotos;
	int nbsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"


	if (argc < 2) {
		fprintf (stderr, "usage: %s list-of-SURLs\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	printf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	printf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror (" dpm-copy: at least one SURL must be provided ");
    exit(1);
  }
  nbfiles = nbargs;
  printf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  printf (" \n");

#if 0
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROTL)
#endif

  // Setting defaults

  if ( ! dpm_proto ) {
    printf ("dpm-put: Protocol option is mandatory, --dpmproto=some_protocol \n");
    exit(1);
  } else {
    nbprotos = 1;
    lprotos = calloc (nbprotos, sizeof(char*));
    lprotos[0] = strdup(dpm_proto);
  }

#if 0
  if ( ! nbprotos ) {
    perror (" Protocol option is mandatory: --dpmprotos=someprotocol ");
    exit(1);
  }
#endif
  if ( ! dpm_dest ) {
    perror (" Destination option is mandatory: --dpmdest=someSE ");
    exit(1);
  }
  if ( ! dpm_utoken ) {
    perror (" User token option is mandatory: --dpmutoken=user_token ");
    exit(1);
  }
	
	if ((reqfiles = calloc (nbfiles, sizeof(struct dpm_copyfilereq))) == NULL) {
		perror ("calloc");
		exit (1);
	}

/*   if ( ! dpm_reqsize ) { */
/*     dpm_reqsize = 200; // Mbytes */
/*   } */
 if ( ! dpm_lifet ) {
   dpm_lifet = 0;
 }
 if ( ! dpm_stoken ) {
   dpm_stoken = "dontknowyet";
 }
/*   if ( ! dpm_utoken ) { */
/*     dpm_stoken = "dontknowyet"; */
/*   } */
 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   dpm_ftype = 'V';
 }
 nbsurls = nbargs;

 for (i = 0; i < nbsurls; i++) {
   reqfiles[i].lifetime = dpm_lifet;
   reqfiles[i].from_surl = remargs[i];
   reqfiles[i].to_surl = dpm_dest;
   reqfiles[i].f_type = dpm_ftype;
   strcpy(reqfiles[i].s_token, dpm_stoken);
   reqfiles[i].flags = dpm_flags;
   printf (" File: %s %d %s %c \n", reqfiles[i].from_surl, reqfiles[i].lifetime, reqfiles[i].s_token, reqfiles[i].f_type);
   //printf (" File: %c \n", reqfiles[i].f_type);
 }

  printf (" \n");

	nbprotocols = sizeof(protocols) / sizeof(char *);

	if ((status = dpm_copy (nbfiles, 
				reqfiles, 
				dpm_utoken,
				dpm_flags,
				dpm_rtime, 
				r_token, 
				&nbreplies, 
				&filestatuses
				)) < 0) {
		sperror ("dpm_copy");
		exit (1);
	}

	printf ("dpm_copy returned r_token: %s\n", r_token);

	/* wait for request status "Done" or "Failed" */

	//#if 0
	while (status == DPM_QUEUED || status == DPM_ACTIVE) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->from_surl)
				free ((filestatuses+i)->from_surl);
			if ((filestatuses+i)->to_surl)
				free ((filestatuses+i)->to_surl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
		printf("request state Pending\n");
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((status = dpm_getstatus_copyreq (r_token, 
						     0, 
						     NULL,
						     NULL,
						     &nbreplies, 
						     &filestatuses
						     )) < 0) {
			sperror ("dpm_getstatus_copyreq");
			exit (1);
		}
	}
	printf ("request state %s\n", status == DPM_DONE ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);
	for (i = 0; i < nbreplies; i++) {
		if ((filestatuses+i)->status != DPM_READY)
			printf ("state[%d] = %s, errstring = <%s>\n", i,
			    (filestatuses+i)->status,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : "");
		else
			printf ("state[%d] = %s, TO_SURL = %s\n", i,
			    (filestatuses+i)->status,
			    (filestatuses+i)->to_surl);
		if ((filestatuses+i)->from_surl)
			free ((filestatuses+i)->from_surl);
		if ((filestatuses+i)->to_surl)
			free ((filestatuses+i)->to_surl);
		if ((filestatuses+i)->errstring)
			free ((filestatuses+i)->errstring);
	}

	free (filestatuses);
	//#endif
	return 0;

}
