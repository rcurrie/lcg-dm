/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-get.c,v 1.5 2007/07/19 15:53:24 grodid Exp $

// Template by JPB: dpm_testget
// Modified by GG (06/11/2006)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

/* major/minor number */
#include <sys/sysmacros.h>

#include <signal.h>   /* catch signals */

#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

// gnu opts
#include "dpmgopts.h"

#include "gop_alloc.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
#include "dpmestat.h"
	struct dpm_getfilestatus *filestatuses;
	int i;
	//int nbfiles;
	//int nbprotocols;
	int nbreplies;
	//static char *def_protocols[] = {"rfio"};
	int r = 0;
	char r_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_getfilereq *reqfiles;
	//int reqid;
	int status;

	int nbprotos;
	char **lprotos;
	int nbsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
	  fprintf (stderr, "usage: %s \
[--dpmlifet=lifetime] (default=0)     \
[--dpmftype=[V|D|P]] (default=V)      \
[--dpmretpol=[R|O|C]] (default=-)     \
[--dpmstoken=spacetoken] (UNUSED)     \
[--dpmflags=1] (default=0) (UNUSED)			\
[--dpmutoken=usertoken] (default=\"unknownutoken\")	\
--dpmproto=some_protocol (no default)			\
list-of-SURLs\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

	if ( ! nbargs ) {
	  perror ("dpm-get: at least one SURL must be provided ");
	  exit(1);
	}
	//Pprintf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);
	
	// End of Options stuff

	// Input preparation //////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURLs\n", argv[0]);
		exit (1);
	}

	// Loading inputs

  //Pprintf (" \n");

#if 0
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROTL)
#endif

  // Setting defaults

  if ( ! dpm_proto ) {
    perror ("dpm-get: Protocol option is mandatory, --dpmproto=some_protocol ");
    exit(1);
  } else {
    nbprotos = 1;
    lprotos = calloc (nbprotos, sizeof(char*));
    lprotos[0] = strdup(dpm_proto);
  }

 nbsurls = nbargs;
 if ((reqfiles = calloc (nbsurls, sizeof(struct dpm_getfilereq))) == NULL) {
   perror ("calloc");
   exit (1);
 }
 if ( ! dpm_lifet ) {
   dpm_lifet = 0;
 }
 if ( ! dpm_stoken ) {
   dpm_stoken = "dontknowyet";
 }
 if ( ! dpm_utoken ) {
   dpm_utoken = "unknownutoken";
 }
 //if ( ! dpm_ftype ) {
 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   //dpm_ftype = strdup("Volatile");
   dpm_ftype = 'V';
 }

 if ( dpm_retpol == 'Z' || dpm_retpol == '0' ) {
   dpm_retpol = '\0';
 }

 for (i = 0; i < nbsurls; i++) {
   reqfiles[i].lifetime = dpm_lifet;
   reqfiles[i].from_surl = remargs[i];
   reqfiles[i].f_type = dpm_ftype;
   reqfiles[i].ret_policy = dpm_retpol;  
   //strcpy(reqfiles[i].s_token, dpm_stoken);
   //reqfiles[i].flags = dpm_flags;
   //printf (" File: %s %d %s %c \n", reqfiles[i].from_surl, (int) reqfiles[i].lifetime, reqfiles[i].s_token, reqfiles[i].f_type);
   printf (" SURL: %s \n", reqfiles[i].from_surl);
 }

  //Pprintf (" \n");
 
	// Effective call

	if ((status = dpm_get (nbsurls, 
			       reqfiles, 
			       nbprotos, 
			       lprotos, 
			       dpm_utoken,
			       0, 
			       r_token, 
			       &nbreplies, 
			       &filestatuses
			       )) < 0) {
		sperror ("==> dpm-get");
		exit (1);
	}
	//reqid = atoi (r_token);

	printf ("dpm-get returned r_token: %s\n", r_token);

	/* wait for request status "Done" or "Failed" */

	while (status == DPM_QUEUED || status == DPM_ACTIVE) {
		printf("request state Pending\n");
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((status = dpm_getstatus_getreq (r_token, 0, NULL,
		    &nbreplies, &filestatuses)) < 0) {
			sperror ("dpm_getstatus_getreq");
			exit (1);
		}
	}
	//printf ("request state %s\n", status == DPM_DONE ? "Done" : "Failed");
	printf ("request state %s\n", ( status == DPM_SUCCESS || status == DPM_DONE || status == DPM_READY ) ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);
	for (i = 0; i < nbreplies; i++) {
#if 0
		if ((filestatuses+i)->status != DPM_READY)
			printf ("state[%d] = %s, errstring = <%s>\n", i,
                            f_stat[(filestatuses+i)->status >> 12],
			    //(filestatuses+i)->status,
			    (filestatuses+i)->errstring);
		else
			printf ("state[%d] = %s, TURL = %s\n", i,
                            f_stat[(filestatuses+i)->status >> 12],
			    //(filestatuses+i)->status,
			    (filestatuses+i)->turl);
#else
		if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : sstrerror((filestatuses+i)->status & 0xFFF));
		else if ((filestatuses+i)->turl)
			printf ("state[%d] = %s, TURL = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->turl);
		else
			printf ("state[%d] = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12]);
#endif
	}

	free (filestatuses);
	return 0;

}
