/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-gspt.c,v 1.3 2006/12/19 20:17:29 grodid Exp $

// Created by GG (03/11/2005)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
  //#include "dpmestat.h"
	int i;
	//int r = 0;
	//char s_token[CA_MAXDPMTOKENLEN+1];
	char** s_tokens;
	//char* s_tokens[CA_MAXDPMTOKENLEN+1];
	char u_token[CA_MAXDPMTOKENLEN+1];
	int status;
	int thisarg;
	int nbreplies;

/*  	char  s_type; */
/*  	u_signed64 req_t_space; */
/*  	u_signed64 req_g_space; */
/*  	time_t req_lifetime; */
/*  	char actual_s_type; */
/*  	u_signed64 actual_t_space; */
/*  	u_signed64 actual_g_space; */
/*  	time_t actual_lifetime; */

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s [-|user_space_token_description]\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	//u_token = argv[thisarg++];
  strncpy(u_token, argv[thisarg++], CA_MAXDPMTOKENLEN+1);

	// Loading inputs

  //Pprintf (" \n");

  // Setting defaults
 
  printf (" u_token: %s \n", u_token);

  if ( strchr(u_token, '-' ) == u_token ) {
    printf (" Nu_token: %s \n", u_token);    
    if ((status = dpm_getspacetoken (NULL,
				     &nbreplies, 
				     &s_tokens
				     )) < 0) {
      sperror ("dpm_getspacetoken");
      exit (1);
    }
  }
  else {
    printf (" Cu_token: %s \n", u_token);
    if ((status = dpm_getspacetoken (u_token,
				     &nbreplies, 
				     &s_tokens
				     )) < 0) {
      sperror ("dpm_getspacetoken");
      exit (1);
    }
  }

	printf ("dpm_getspacetoken returned nbreplies: %d\n", nbreplies);

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");

	if (status == DPM_FAILED)
		exit (1);

	for (i = 0; i < nbreplies; i++) {
	  printf ("s_token[%d] = %s\n", i, s_tokens[i]);
	  free(s_tokens[i]);
	}

	return 0;
}
