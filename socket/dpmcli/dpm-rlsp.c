/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-rlsp.c,v 1.3 2007/03/20 07:53:24 grodid Exp $

// Created by GG (07/11/2005)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
  //#include "dpmestat.h"
	int i;
	//int r = 0;
	char s_token[CA_MAXDPMTOKENLEN+1];
	int status;
	int thisarg;
	uint force;

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s \
spacetoken forceflag\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	//u_token = argv[thisarg++];
  strncpy(s_token, argv[thisarg++], CA_MAXDPMTOKENLEN+1);
  force = atoi(argv[thisarg++]);

	// Loading inputs

  // Setting defaults
 
  printf (" \n");

	if ((status = dpm_releasespace (s_token,
			       force
			       )) < 0) {
		sperror ("dpm_releasespace");
		exit (1);
	}

/*  	printf ("dpm_getspacetoken returned nbreplies: %d\n", nbreplies); */

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");

	if (status == DPM_FAILED)
		exit (1);

	return 0;
}
