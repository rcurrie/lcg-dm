/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-getreqsummary.c,v 1.3 2007/07/19 15:53:24 grodid Exp $

// Created by GG (03/11/2004)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

	struct dpm_reqsummary *summaries;
	int i;
	int nbfiles;
	//int nbprotocols;
	int nbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;

/*  	int nbprotos; */
/*  	char **lprotos; */
	int nbsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s list-of-r_tokens\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//printf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//printf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror (" dpm-getreqsummary: at least one reqid_token must be provided ");
    exit(1);
  }
  nbfiles = nbargs;
  //printf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  printf (" \n");

  /*
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROT)
  */

  // Setting defaults

/*    if ( ! dpm_rtoken ) { */
/*      perror (" Reqid token option is mandatory: --dpmrtoken=request_token "); */
/*      exit(1); */
/*    } */
 nbsurls = nbargs;

  printf (" \n");

	if ((status = dpm_getreqsummary (	   nbfiles, 
				   remargs,
				   &nbreplies, 
				   &summaries
				   )) < 0) {
	        sperror ("dpm_getreqsummary");
		printf ("==> Failure in dpm_getreqsummary called from %s\n", argv[0]);
		exit (1);
	}

	//sperror ("dpm_getreqsummary");
	//printf ("request status %d\n", status);
	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

        for (i = 0; i < nbreplies; i++) {
	  printf ("state[%d] => r_token %s, r_type = %c, Q/P/F= %d/%d/%d \n",
		  i,
		  (summaries+i)->r_token,
		  (summaries+i)->r_type,
		  (summaries+i)->nb_queued,
		  (summaries+i)->nb_progress,
		  (summaries+i)->nb_finished);
	}

	free (summaries);
	return 0;

}
