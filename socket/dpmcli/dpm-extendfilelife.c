/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-extendfilelife.c,v 1.3 2007/07/19 15:53:24 grodid Exp $

// Created by GG (03/11/2004)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //struct dpm_filestatus *filestatuses;
	int i;
	int nbfiles;
	//int nbprotocols;
	//int nbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;
	time_t lifetime;
	time_t actual_lifetime;

	//int nbprotos;
	//char **lprotos;
	int nbsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s \
--dpmrtoken=request_token (no default)	    \
only-one-SURL\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( nbargs != 1 ) {
    printf ("dpm-extendfilelife: one SURL, and only one, must be provided \n");
    exit(1);
  }
  nbfiles = nbargs;
  //Pprintf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  //Pprintf (" \n");

  /*
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROT)
  */

  // Setting defaults

  if ( ! dpm_rtoken ) {
    perror ("dpm-extendfilelife: Reqid token option is mandatory, --dpmrtoken=request_token ");
    exit(1);
  }
 if ( ! dpm_lifet ) {
   dpm_lifet = 0;
 }
 nbsurls = nbargs;
 lifetime = dpm_lifet;

 //Pprintf (" \n");

	if ((status = dpm_extendfilelife (dpm_rtoken, 
				   remargs[0],
				   lifetime, 
				   &actual_lifetime
				   )) < 0) {
		sperror ("==> Failure in dpm-extendfilelife");
		exit (1); 
	}

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	printf ("request status %d new_lifetime = %d \n", status, actual_lifetime);
	if (status == DPM_FAILED)
		exit (1);

	return 0;

}
