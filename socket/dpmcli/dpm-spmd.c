/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-spmd.c,v 1.3 2007/07/20 11:55:04 grodid Exp $

// Created by GG (03/11/2005)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#include "u64subr.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

	struct dpm_space_metadata *spacemds;
	int i;
	int nbtokens;
	//int nbprotocols;
	int nbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;
	char tmpbut[25];
	char tmpbug[25];
	char tmpbuu[25];

/*  	int nbprotos; */
/*  	char **lprotos; */
/*  	int nbsurls; */
/*  	char **lsurls; */

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s list-of-s_tokens\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//printf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//printf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror (" dpm-spmd: at least one space_token must be provided ");
    exit(1);
  }
  nbtokens = nbargs;
  //printf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  printf (" \n");

  /*
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROT)
  */

  // Setting defaults

/*    if ( ! dpm_rtoken ) { */
/*      perror (" Reqid token option is mandatory: --dpmrtoken=request_token "); */
/*      exit(1); */
/*    } */
  nbtokens = nbargs;

  printf (" \n");

	if ((status = dpm_getspacemd (nbtokens, 
				      remargs,
				      &nbreplies, 
				      &spacemds
				      )) < 0) {
	        sperror ("dpm_getspacemd");
		printf ("==> Failure in dpm_getspacemd called from %s\n", argv[0]);
		exit (1);
	}

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

        for (i = 0; i < nbreplies; i++) {
                u64tostru((spacemds+i)->t_space, tmpbut, 0); 
                u64tostru((spacemds+i)->g_space, tmpbug, 0); 
                u64tostru((spacemds+i)->u_space, tmpbuu, 0); 
/*  	  printf ("state[%d] => s_token %s, s_type: %c t_space: %llu g_space: %llu u_space: %llu a_lifetime: %lu r_lifetime: %lu client_dn: %s\n", */
	  printf ("state[%d] => s_token %s, s_type: %c t_space: %s g_space: %s u_space: %s a_lifetime: %lu r_lifetime: %lu u_token: %s poolname: %s\n",
		  i,
		  (spacemds+i)->s_token,
		  (spacemds+i)->s_type,
		  tmpbut,
		  tmpbug,
		  tmpbuu,
/*  		  (spacemds+i)->t_space, */
/*  		  (spacemds+i)->g_space, */
/*  		  (spacemds+i)->u_space, */
		  (spacemds+i)->a_lifetime,
		  (spacemds+i)->r_lifetime,
		  (spacemds+i)->u_token,
		  (spacemds+i)->poolname
		  );
/*  		  (summaries+i)->nb_queued, */
/*  		  (summaries+i)->nb_progress, */
/*  		  (summaries+i)->nb_finished); */
	}

	free (spacemds);
	return 0;

}
