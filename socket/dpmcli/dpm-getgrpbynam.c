/*
 * Copyright (C) 2006 by CERN/IT/GD/ITR & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-getgrpbynam.c,v 1.1 2006/04/28 06:57:40 grodid Exp $

// Created by GG (14/04/2006)

/*     getgrpbynam  - get a  user entry from Virtual Gid table */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpns_api.h"
#include "serrno.h"

int
main(argc, argv)
int argc;
char **argv;
{
  //int c;
  //char *dp;
	int errflg = 0;
#if 0
	static struct Coptions longopts[] = {
		{"uid", REQUIRED_ARGUMENT, 0, OPT_IDMAP_UID},
		{"user", REQUIRED_ARGUMENT, 0, OPT_IDMAP_USER},
		{0, 0, 0, 0}
	};
#endif
	gid_t uid = -1;
	char *username = NULL;

#if 0
	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_IDMAP_UID:
			if ((uid = strtol (Coptarg, &dp, 10)) < 0 || *dp != '\0') {
				fprintf (stderr, "invalid uid: %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_IDMAP_USER:
			if (strlen (Coptarg) > 255) {
				fprintf (stderr,
				    "user name too long: %s\n", Coptarg);
				errflg++;
			} else
				username = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || username == NULL)
		errflg++;
#endif

	if ( argc < 1 )
	  errflg++;

	if (errflg) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    " groupname\n");
		exit (USERR);
	}

	username = argv[1];

	if (dpns_getgrpbynam (username, &uid) < 0) {
		fprintf (stderr, "getgrpbynam %d: %s\n", uid,
		    (serrno == EINVAL) ? "Group name does not exist" : sstrerror(serrno));
		exit (USERR);
	}
	printf (" getgrpbynam mapping groupname: %s gid: %d\n", username, uid);

	exit (0);
}
