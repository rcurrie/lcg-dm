/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpmgdebug.h,v 1.1 2005/01/20 09:47:19 grodid Exp $

// Created by GG (26/10/2004)

#ifndef SIGCHLD
#define SIGCHLD SIGCLD
#endif

#ifdef DEBUG
  init_stack_trace(argv[0]);
#else
  do {
    struct sigaction sa;
    int si = 32;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = signal_handler;
    sigfillset(&sa.sa_mask);
    while(si--) switch(si){
    default:
      sigaction(i,&sa,NULL);
    case 0:
    case SIGINT:   /* ^C */
    case SIGQUIT:  /* ^\ */
    case SIGPROF:  /* profiling */
    case SIGKILL:  /* can not catch */
    case SIGSTOP:  /* can not catch */
    case SIGWINCH: /* don't care if window size changes */
      ;
    }
  } while (0);
#endif
