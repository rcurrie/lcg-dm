/*
 * Copyright (C) 1999-2007 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-Ls.c,v $ $Revision: 1.2 $ $Date: 2007/07/19 15:53:24 $ CERN IT-PDP/DM Jean-Philippe Baud";
#endif /* not lint */

// $Id: dpm-Ls.c,v 1.2 2007/07/19 15:53:24 grodid Exp $

/*	dpm-Ls - list DPM directory/file entries recursively when older than a given threshold */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#if defined(_WIN32)
#define W_OK 2
#include <winsock2.h>
#else
#include <unistd.h>
#endif
#include "Cns.h"
//#include "Cns_api.h"
#include "dpns_api.h"
#include "dpm_api.h"
#include "serrno.h"
extern	char	*getenv();
extern	int	optind;
time_t current_time;
#if sgi
extern char *strdup _PROTO((CONST char *));
#endif
int errflg;
int fflag;
int iflag;
int rflag;

#include <signal.h>   /* catch signals */
// gnu opts
#include "dpmgopts.h"
#include "dpmutils.h"

int main(argc, argv)
int argc;
char **argv;
{
	int c;
	char fullpath[CA_MAXPATHLEN+1];
	int i;
	char *p;
	char *path;
	char *pn;
	char *pm;
	struct Cns_filestat statbuf;
	time_t ltime;
#if defined(_WIN32)
	WSADATA wsadata;
#endif

#define _DPMOPT yes

#ifndef _DPMOPT
	while ((c = getopt (argc, argv, "fiRr")) != EOF) {
		switch (c) {
		case 'f':
			fflag++;
			break;
		case 'i':
			iflag++;
			break;
		case 'R':
		case 'r':
			rflag++;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (optind >= argc) {
		fprintf (stderr,
		    "usage: %s [-f] [-i] file...\n\t%s [-f] [-i] -r dirname...\n",
		    argv[0], argv[0]);
		exit (USERR);
	}
#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, NS052);
		exit (SYERR);
	}
#endif
#else

	struct dpm_filestatus *filestatuses = NULL;
	int nbfiles;
	int nbreplies;
	char **dpmrmargs = NULL;

	int thisarg;
	int nbargs = 0;
	char **remargs = NULL;
	char hostenvm[256];
	char hostenvn[256];

#include "dpmgdebug.h"
	if (argc < 2) {
		fprintf (stderr, "usage: %s --dpmolder=age list-of-SURLs and/or list-of-SURL-of-DIRs\n", argv[0]);
		exit (USERR);
	}

	// gnu opts
	reset_global();  /* must be before parser */
	prog_gname = strdup(argv[0]);

	//P printf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//P printf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}


	if ( dpm_host ) {
	  printf ("    host: %s\n", dpm_host);
	  strcpy(hostenvm, "DPM_HOST=");
	  strcat(hostenvm, dpm_host);
	  putenv(hostenvm);
	  strcpy(hostenvn, "DPNS_HOST=");
	  strcat(hostenvn, dpm_host);
	  putenv(hostenvn);
	}
	(void) time (&current_time);
	if ((pn = getenv ("DPNS_HOST")) && (pm = getenv ("DPM_HOST")) && strcmp(pn, pm)) {
	  printf ( " DPNS_HOST: %s and DPM_HOST: %s mismatch\n", pn, pm);
	  exit(USERR);
	} else {
	  printf ("DPM_HOST: %s\n", pm);
	}
	rflag++;
	fflag++;
	if ( ! dpm_older ) {
	  perror (" Older option is mandatory: --dpmolder=age (sec.) ");
	  exit(1);
	}
#endif

#if defined(_DPMOPT)
	for (i = 0; i < nbargs; i++) {
		path = remargs[i];
#else
	for (i = optind; i < argc; i++) {
		path = argv[i];
#endif
		printf ("path: %s\n", path);
		if (*path != '/' && strstr (path, ":/") == NULL) {
			if ((p = getenv (CNS_HOME_ENV)) == NULL ||
			    strlen (p) + strlen (path) + 1 > CA_MAXPATHLEN) {
				fprintf (stderr, "%s: invalid path\n", path);
				errflg++;
				continue;
			} else
				sprintf (fullpath, "%s/%s", p, path);
		} else {
			if (strlen (path) > CA_MAXPATHLEN) {
				fprintf (stderr, "%s: %s\n", path,
				    sstrerror(SENAMETOOLONG));
				errflg++;
				continue;
			} else
				strcpy (fullpath, path);
		}
		if ((c = Cns_lstat (fullpath, &statbuf)) == 0) {
			if (statbuf.filemode & S_IFDIR) {
				if (rflag) {
					c = listdir (fullpath);
				} else {
					serrno = EISDIR;
					c = -1;
				}
			} else {
				if ((statbuf.filemode & S_IFLNK) != S_IFLNK &&
				    ! fflag && Cns_access (fullpath, W_OK) &&
				    isatty (fileno (stdin))) {
					printf ("override write protection for %s? ", fullpath);
					if (! isyes())
						continue;
				} else if (iflag) {
					printf ("list %s? ", fullpath);
					if (! isyes())
						continue;
				}
#if 0
				c = Cns_unlink (fullpath);
#else
				ltime = statbuf.atime;
				if ( current_time-ltime > dpm_older ) {
				  printf ("listING file: %s %d\n", fullpath, current_time-ltime);
				  // To replace with dpm_rm ...
				  // c = Cns_unlink (fullpath);

#ifdef _REMOVAL
				  nbfiles = 1;
				  dpmrmargs = realloc(dpmrmargs, sizeof(char*)*nbfiles);
				  dpmrmargs[0] = strdup(fullpath);
				  if ((c = dpm_rm (nbfiles, 
						   dpmrmargs,
						   &nbreplies, 
						   &filestatuses
						   )) < 0) {
				    fprintf (stderr, "dpm_rm: %s\n", sstrerror(serrno));
				    //nb_file_err++;
				  }
				  nbreplies = 0;
				  if (dpmrmargs)
				    free(dpmrmargs);
				  if (filestatuses)
				    free (filestatuses);
#endif
				} else {
				  printf (" excludING file: %s %d\n", fullpath, current_time-ltime);
				}
#endif
			}
		}
		if (c && (serrno != ENOENT || fflag == 0)) {
			fprintf (stderr, "%s: %s\n", path,
			    sstrerror(serrno));
			errflg++;
		}
	}
#if defined(_WIN32)
	WSACleanup();
#endif
	if (errflg)
		exit (USERR);
	exit (0);
}

isyes()
{
	int c;
	int fchar;

	fchar = c = getchar();
	while (c != '\n' && c != EOF)
		c = getchar();
	return (fchar == 'y');
}

listdir (dir)
char *dir;
{
	char curdir[CA_MAXPATHLEN+1];
	struct dirlist {
		char *d_name;
		struct dirlist *next;
	};
	Cns_DIR *dirp;
	struct dirlist *dlc;		/* pointer to current directory in the list */
	struct dirlist *dlf = NULL;	/* pointer to first directory in the list */
	struct dirlist *dll;		/* pointer to last directory in the list */
	struct Cns_direnstat *dxp;
	char fullpath[CA_MAXPATHLEN+1];

	struct dpm_filestatus *filestatuses = NULL;
	int nbfiles = 0;
	//int nb_file_err = 0;
	int nbreplies;
	char **dpmrmargs = NULL;

	int c;
	struct Cns_filestat statbuf;
	time_t ltime;

	if (! fflag && Cns_access (dir, W_OK) &&
	    isatty (fileno (stdin))) {
		printf ("override write protection for %s? ", dir);
		if (! isyes())
			return (0);
	} else if (iflag) {
		printf ("list files in %s? ", dir);
		if (! isyes())
			return (0);
	}
	if ((dirp = Cns_opendir (dir)) == NULL)
		return (-1);

	if (Cns_chdir (dir) < 0)
		return (-1);
	while ((dxp = Cns_readdirx (dirp)) != NULL) {
		if (dxp->filemode & S_IFDIR) {
			if ((dlc = (struct dirlist *)
			    malloc (sizeof(struct dirlist))) == NULL ||
			    (dlc->d_name = strdup (dxp->d_name)) == NULL) {
				serrno = errno;
				return (-1);
			}
			dlc->next = 0;
			if (dlf == NULL)
				dlf = dlc;
			else
				dll->next = dlc;
			dll = dlc;
		} else {
			sprintf (fullpath, "%s/%s", dir, dxp->d_name);
			if ((dxp->filemode & S_IFLNK) != S_IFLNK &&
			    ! fflag && Cns_access (fullpath, W_OK) &&
			    isatty (fileno (stdin))) {
				printf ("override write protection for %s? ", fullpath);
				if (! isyes())
					continue;
			} else if (iflag) {
				printf ("list %s? ", fullpath);
				if (! isyes())
					continue;
			}
#if 0
			if (Cns_unlink (dxp->d_name)) {
				fprintf (stderr, "%s/%s: %s\n", dir,
				    dxp->d_name, sstrerror(serrno));
				errflg++;
			}
#else
			if ((c = Cns_lstat (fullpath, &statbuf)) == 0) {
			  ltime = statbuf.atime;
			  if ( current_time-ltime > dpm_older ) {
			    printf ("listING file: %s %d\n", fullpath, current_time-ltime);
#ifdef _REMOVAL
			    // Putting this file on the list stack ...
				  nbfiles++;
				  dpmrmargs = realloc(dpmrmargs, sizeof(char*)*nbfiles);
				  dpmrmargs[nbfiles-1] = strdup(fullpath);
#endif
			  } else {
			    printf (" excludING file: %s %d\n", fullpath, current_time-ltime);
			  }
			} else {
			  // No info found on this file ...
			  //NO printf ("listING file: %s\n", fullpath);
			}
#endif
		}
	}

#ifdef _REMOVAL
	if (nbfiles) {
	                          printf ("listING nbfiles: %d\n", nbfiles);
				  if ((c = dpm_rm (nbfiles, 
						   dpmrmargs,
						   &nbreplies, 
						   &filestatuses
						   )) < 0) {
				    fprintf (stderr, "listdir dpm_rm: %s\n", sstrerror(serrno));
				    errflg++;
				  } 
				  nbfiles = 0;
				  nbreplies = 0;
				  if (dpmrmargs)
				    free(dpmrmargs);
				  if (filestatuses)
				    free (filestatuses);
	}
#endif


	(void) Cns_closedir (dirp);
	while (dlf) {
		sprintf (curdir, "%s/%s", dir, dlf->d_name);
		if (listdir (curdir) < 0)
			fprintf (stderr, "%s: %s\n", curdir, sstrerror(serrno));
		free (dlf->d_name);
		dlc = dlf;
		dlf = dlf->next;
		free (dlc);
	}
	if (Cns_chdir ("..") < 0)
		return (-1);
	if (iflag) {
		printf ("list %s? ", dir);
		if (! isyes())
			return (0);
	}
#if 0
	if (Cns_rmdir (dir)) {
		fprintf (stderr, "%s: %s\n", dir, (serrno == EEXIST) ?
		    "Directory not empty" : sstrerror(serrno));
		errflg++;
	}
#else
	printf ("listING  dir: %s\n", dir);
	
#ifdef _REMOVAL
	if (Cns_rmdir (dir)) {
		fprintf (stderr, "%s: %s\n", dir, (serrno == EEXIST) ?
		    "Directory not empty" : sstrerror(serrno));
		errflg++;
	}
#endif
	
#endif
	return (0);
}
