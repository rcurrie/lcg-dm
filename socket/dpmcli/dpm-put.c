/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-put.c,v 1.7 2007/07/19 15:53:24 grodid Exp $

// Template by JPB: dpm_testput
// Modified by GG (02/11/2004)
// Last mod by GG (06/11/2006)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
#include "dpmestat.h"
	struct dpm_putfilestatus *filestatuses;
	int i;
	int nbfiles;
	int nbprotocols;
	int nbreplies;
	static char *protocols[] = {"rfio"};
	int r = 0;
	char r_token[CA_MAXDPMTOKENLEN+1];
	//char u_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_putfilereq *reqfiles;
	int status;

	int nbprotos;
	char **lprotos;
	int nbsurls;
	//char **lsurls;

	/*
  int nbtosurls;
  char **ltosurls;
	*/
  int nbfsizes;
  char **lfsizes;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
	  fprintf (stderr, "usage: %s  \
[--dpmlifet=lifetime] (default=0)      \
[--dpmftype=[V|D|P]] (default=V)       \
[--dpmretpol=[R|O|C]] (default=-)      \
[--dpmaclat=[O|N]] (default=O)	       \
[--dpmstoken=spacetoken]				\
[--dpmutoken=usertoken] (default=\"unknownutoken\")	\
[--dpmfsizes=array-of-filsizes] (no default)		\
[--dpmreqsize=filesize] (default=200Mb)			\
--dpmproto=some_protocol (no default)			\
list-of-SURLs\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror ("dpm-put: at least one SURL must be provided ");
    exit(1);
  }
  nbfiles = nbargs;
  //Pprintf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  //Pprintf (" \n");

#if 0
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROTL)
#endif

  // Setting defaults

  if ( ! dpm_proto ) {
    printf ("dpm-put: Protocol option is mandatory, --dpmproto=some_protocol \n");
    exit(1);
  } else {
    nbprotos = 1;
    lprotos = calloc (nbprotos, sizeof(char*));
    lprotos[0] = strdup(dpm_proto);
  }
	
	if ((reqfiles = calloc (nbfiles, sizeof(struct dpm_putfilereq))) == NULL) {
		perror ("calloc");
		exit (1);
	}

#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

//V_LSTR(paths, PATH)
//V_LSTR(surls, SURL)
V_LSTR(fsizes, FSIZE)
//V_LSTR(fromsurls, FSURL)
//BADV_LSTR(reqsize, TSURL)

 if ( ! dpm_reqsize ) {
   dpm_reqsize = 2000; // kbytes
 }
 if ( ! dpm_lifet ) {
   dpm_lifet = 0;
 }
/*
 if ( ! dpm_stoken ) {
   dpm_stoken = "dontknowyet";
 }
*/
 if ( ! dpm_utoken ) {
   dpm_utoken = "unknownutoken";
 }

 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   dpm_ftype = 'V';
 }

 if ( dpm_retpol == 'Z' || dpm_retpol == '0' ) {
   dpm_retpol = '\0';
 }

 if ( dpm_aclat == 'Z' || dpm_aclat == '0' ) {
   dpm_aclat = 'O';
 }

 nbsurls = nbargs;

    if (nbfsizes && nbfsizes != nbsurls) {
        printf (" nbfsizes not correctly set: nbfsizes: %d, nbsurls: %d\n", nbfsizes, nbsurls);
    }

 for (i = 0; i < nbsurls; i++) {
   reqfiles[i].lifetime = dpm_lifet;
   reqfiles[i].to_surl = remargs[i];
   reqfiles[i].f_type = dpm_ftype;  //  pb with 'V' ?
   reqfiles[i].ret_policy = dpm_retpol;  
   reqfiles[i].ac_latency = dpm_aclat;  
   //reqfiles[i].f_type = 'V';
/* removed 100406 gg */
    if ( dpm_stoken ) 
        strcpy(reqfiles[i].s_token, dpm_stoken);
/*
    else
        reqfiles[i].s_token = NULL;
*/
/* */
   //reqfiles[i].s_token = 0;
    if ( nbfsizes )
       reqfiles[i].requested_size = atoi(lfsizes[i])*1024;
    else
       reqfiles[i].requested_size = dpm_reqsize*1024;
/*
   printf (" File: %s %d %s %c \n", reqfiles[i].to_surl, reqfiles[i].lifetime, reqfiles[i].s_token, reqfiles[i].f_type);
*/
   printf (" File: %s %d %c \n", reqfiles[i].to_surl, reqfiles[i].lifetime, reqfiles[i].f_type);
   //printf (" File: %s \n", reqfiles[i].to_surl);
 }

  printf (" \n");

	nbprotocols = sizeof(protocols) / sizeof(char *);

	if ((status = dpm_put (nbfiles, 
			       reqfiles, 
			       //nbprotocols, 
			       //protocols, 
			       nbprotos, 
			       lprotos, 
			       dpm_utoken, //NULL,  //  u_token
			       0, 
			       0, 
			       r_token, 
			       &nbreplies, 
			       &filestatuses
			       )) < 0) {
		sperror ("dpm_put");
		exit (1);
	}

	printf ("dpm_put returned r_token: %s\n", r_token);

	/* wait for request status "Done" or "Failed" */

	while (status == DPM_QUEUED || status == DPM_ACTIVE) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->to_surl)
				free ((filestatuses+i)->to_surl);
			if ((filestatuses+i)->turl)
				free ((filestatuses+i)->turl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
		printf("request state Pending\n");
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((status = dpm_getstatus_putreq (r_token, 0, NULL,
		    &nbreplies, &filestatuses)) < 0) {
			sperror ("dpm_getstatus_putreq");
			//exit (1);
                        if (status == DPM_FAILED)
                               break;
		}
	}
	printf ("request status %d\n", status);
	//printf ("request state %s\n", status == DPM_DONE ? "Done" : "Failed");
	//printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	printf ("request state %s\n", ( status == DPM_SUCCESS || status == DPM_DONE || status == DPM_READY ) ? "Done" : "Failed");
/*
	if (status == DPM_FAILED)
		exit (1);
*/

#if 0
	//old
	for (i = 0; i < nbreplies; i++) {
	  //		if ((filestatuses+i)->status != DPM_READY) 
		if ((filestatuses+i)->status == DPM_DONE) 
			printf ("state[%d] = %d, errstring = <%s>\n", i,
			    (filestatuses+i)->status,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : "");
		else
			printf ("state[%d] = %d, TURL = %s\n", i,
			    (filestatuses+i)->status,
			    (filestatuses+i)->turl);
		if ((filestatuses+i)->to_surl)
			free ((filestatuses+i)->to_surl);
		if ((filestatuses+i)->turl)
			free ((filestatuses+i)->turl);
		if ((filestatuses+i)->errstring)
			free ((filestatuses+i)->errstring);
	}
#else
	//new
        for (i = 0; i < nbreplies; i++) {
                if ((filestatuses+i)->turl)
                        printf ("state[%d] = %s, TURL = %s\n", i,
                            f_stat[(filestatuses+i)->status >> 12],
                            (filestatuses+i)->turl);
                else if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
                        printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
                            f_stat[(filestatuses+i)->status >> 12],
                            (filestatuses+i)->status & 0xFFF,
                            (filestatuses+i)->errstring ? (filestatuses+i)->errstring : sstrerror((filestatuses+i)->status & 0xFFF));
                else
                        printf ("state[%d] = %s\n", i,
                            f_stat[(filestatuses+i)->status >> 12]);
                if ((filestatuses+i)->to_surl)
                        free ((filestatuses+i)->to_surl);
                if ((filestatuses+i)->turl)
                        free ((filestatuses+i)->turl);
                if ((filestatuses+i)->errstring)
                        free ((filestatuses+i)->errstring);
        }
#endif

	if (status == DPM_FAILED)
		exit (1);

	free (filestatuses);
	return 0;

}
