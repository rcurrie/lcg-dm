// $Id: gop_sig.h,v 1.1 2005/01/20 09:47:21 grodid Exp $

#ifndef DPMSIG_H
#define DPMSIG_H

#include "gop_clic.h"

EXTERN_C_BEGIN

/* return -1 on failure */
extern int signal_name_to_number(const char *restrict name);

extern const char *signal_number_to_name(int signo);

extern int print_given_signals(int argc, const char *restrict const *restrict argv, int max_line);

extern void pretty_print_signals(void);

extern void unix_print_signals(void);

EXTERN_C_END
#endif
