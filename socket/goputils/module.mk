# $Id: module.mk,v 1.2 2005/01/20 14:03:34 grodid Exp $

# This file gets included into the main Makefile, in the top directory.

# Ideally, we want something like this:
#
# /lib/libproc.so.w        ELF soname ('w' is a digit, starting from 1)
# /lib/procps-x.y.z.so     file itself (x.y.z is the procps version)
# /lib/libproc.so          for linking, UNSUPPORTED
# /usr/lib/libproc.a       for linking, UNSUPPORTED
# proc/libproc.so.w        as above, if testing with LD_LIBRARY_PATH
# proc/whatever            if testing with LD_PRELOAD
# proc/libproc.a           for static build
#
# Without a stable ABI, there's no point in having any of that.
# Without a stable API, there's no point in having the *.a file.
#
# A new ELF soname is required for every big ABI change. To conserve
# numbers for future use, the ELF soname can be set equal to the
# file name until some future date when a stable ABI is declared.

SHARED     := 0

# for lib$(NAME).so and /usr/include/($NAME) and such
NAME       :=  goputils

LIBVERSION := $(VERSION).$(SUBVERSION).$(MINORVERSION)
ABIVERSION := 0

SOFILE     := lib$(NAME)-$(LIBVERSION).so
ifneq ($(ABIVERSION),0)
SOLINK     := lib$(NAME).so
SONAME     := lib$(NAME).so.$(ABIVERSION)
else
SONAME     := $(SOFILE)
SOLINK     := $(SOFILE)
endif

ANAME      := lib$(NAME).a

############

FPIC       := -fpic

ifeq ($(SHARED),1)
ALL        += goputils/$(SONAME)
INSTALL    += ldconfig
LIBFLAGS   := -DSHARED=1 $(FPIC)
LIBPROC    := goputils/$(SONAME)
else
ALL        += goputils/$(ANAME)
#INSTALL    += $(usr/lib)$(ANAME)
LIBFLAGS   := -DSHARED=0
LIBPROC    := goputils/$(ANAME)
endif
##LIBPROG += $(LIBPROC)

LIBSRC :=  $(wildcard goputils/*.c)
LIBHDR :=  $(wildcard goputils/*.h)
LIBOBJ :=  $(LIBSRC:.c=.o)

# Separate rule for this directory, to use -fpic or -fPIC
$(filter-out goputils/gop_verf.o,$(LIBOBJ)): goputils/%.o: goputils/%.c
	$(CC) -c $(ALL_CPPFLAGS) $(ALL_CFLAGS) $(LIBFLAGS) $< -o $@

LIB_X := COPYING module.mk library.map
TARFILES += $(LIBSRC) $(LIBHDR) $(addprefix goputils/,$(LIB_X))


# Clean away all output files, .depend, and symlinks.
# Use wildcards in case the version has changed.
CLEAN += goputils/.depend goputils/lib*.so* goputils/lib*.a $(LIBOBJ)
DIRS  += goputils/

goputils/$(ANAME): $(LIBOBJ)
	$(AR) rcs $@ $^
##BAD	$(RANLIB) $@

#goputils/$(SONAME): goputils/library.map
goputils/$(SONAME): $(LIBOBJ)
	$(CC) -shared -Wl,-soname,$(SONAME) -Wl,--version-script=goputils/library.map -o $@ $^ -lc


# AUTOMATIC DEPENDENCY GENERATION -- GCC AND GNUMAKE DEPENDENT
goputils/.depend: $(LIBSRC) $(LIBHDR)
	$(strip $(CC) $(ALL_CPPFLAGS) $(LIB_CFLAGS) -MM -MG $(LIBSRC) > $@)

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),tar)
ifneq ($(MAKECMDGOALS),extratar)
-include goputils/.depend
endif
endif
endif

#################### install rules ###########################

$(lib)$(SOFILE) : goputils/$(SONAME)
	$(install) --mode a=rx $< $@

ifneq ($(SOLINK),$(SOFILE))
.PHONY: $(lib)$(SOLINK)
$(lib)$(SOLINK) : $(lib)$(SOFILE)
	cd $(lib) && $(ln_sf) $(SOFILE) $(SOLINK)
endif

ifneq ($(SONAME),$(SOFILE))
.PHONY: $(lib)$(SONAME)
$(lib)$(SONAME) : $(lib)$(SOFILE)
	cd $(lib) && $(ln_sf) $(SOFILE) $(SONAME)
endif

.PHONY: ldconfig
ldconfig : $(lib)$(SONAME) $(lib)$(SOLINK)
	$(ldconfig)

$(usr/lib)$(ANAME) : goputils/$(ANAME)
	$(install) --mode a=r $< $@

##################################################################

goputils/gop_verf.o: goputils/gop_verf.c goputils/gop_verf.h
	$(CC) $(ALL_CPPFLAGS) $(ALL_CFLAGS) $(LIBFLAGS) -DVERSION=\"$(VERSION)\" -DSUBVERSION=\"$(SUBVERSION)\" -DMINORVERSION=\"$(MINORVERSION)\" -c -o $@ $<
