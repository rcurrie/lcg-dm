// $Id: gop_alloc.h,v 1.1 2005/01/20 09:47:20 grodid Exp $

#ifndef DPMC_ALLOC_H
#define DPMC_ALLOC_H

#include "gop_clic.h"

EXTERN_C_BEGIN

extern void *xrealloc(void *oldp, unsigned int size) MALLOC;
extern void *xmalloc(unsigned int size) MALLOC;
extern void *xcalloc(void *pointer, int size) MALLOC;

EXTERN_C_END

#endif
