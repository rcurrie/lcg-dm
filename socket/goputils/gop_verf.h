// $Id: gop_verf.h,v 1.1 2005/01/20 09:47:22 grodid Exp $

#ifndef VERSION_H
#define VERSION_H

#include "gop_clic.h"

EXTERN_C_BEGIN

extern void display_version(void);	/* display suite version */
extern const char procps_version[];		/* global buf for suite version */

extern int linux_version_code;		/* runtime version of LINUX_VERSION_CODE
					   in /usr/include/linux/gop_verf.h */

/* Convenience macros for composing/decomposing version codes */
#define LINUX_VERSION(x,y,z)   (0x10000*(x) + 0x100*(y) + z)
#define LINUX_VERSION_MAJOR(x) (((x)>>16) & 0xFF)
#define LINUX_VERSION_MINOR(x) (((x)>> 8) & 0xFF)
#define LINUX_VERSION_PATCH(x) ( (x)      & 0xFF)

EXTERN_C_END

#endif	/* PROC_VERSION_H */
