// $Id: gop_devnm.h,v 1.1 2005/01/20 09:47:20 grodid Exp $

#ifndef DEVNAME_H
#define DEVNAME_H

#include "gop_clic.h"

EXTERN_C_BEGIN

#define ABBREV_DEV  1     /* remove /dev/         */
#define ABBREV_TTY  2     /* remove tty           */
#define ABBREV_PTS  4     /* remove pts/          */

extern unsigned dev_to_tty(char *restrict ret, unsigned chop, dev_t dev_t_dev, int pid, unsigned int flags);

extern int tty_to_dev(const char *restrict const name);

EXTERN_C_END
#endif
