// $Id: gop_help.c,v 1.2 2005/12/29 12:16:59 grodid Exp $

/*
 * The help message must not become longer, because it must fit
 * on an 80x24 screen _with_ the error message and command prompt.
 */

#if 0
const char *help_message =
"********* simple selection *********  ********* selection by list *********\n"
"-A all processes                      -C by command name\n"
"-N negate selection                   -G by real group ID (supports names)\n"
"-a all w/ tty except session leaders  -U by real user ID (supports names)\n"
"-d all except session leaders         -g by session OR by effective group name\n"
"-e all processes                      -p by process ID\n"
"T  all processes on this terminal     -s processes in the sessions given\n"
"a  all w/ tty, including other users  -t by tty\n"
"g  OBSOLETE -- DO NOT USE             -u by effective user ID (supports names)\n"
"r  only running processes             U  processes for specified users\n"
"x  processes w/o controlling ttys     t  by tty\n"
"*********** output format **********  *********** long options ***********\n"
"-o,o user-defined  -f full            --Group --User --pid --cols --ppid\n"
"-j,j job control   s  signal          --group --user --sid --rows --info\n"
"-O,O preloaded -o  v  virtual memory  --cumulative --format --deselect\n"
"-l,l long          u  user-oriented   --sort --tty --forest --version\n"
"-F   extra full    X  registers       --heading --no-heading --context\n"
"                    ********* misc options *********\n"
"-V,V  show version      L  list format codes  f  ASCII art forest\n"
"-m,m,-L,-T,H  threads   S  children in sum    -y change -l format\n"
"-M,Z  security data     c  true command name  -c scheduling class\n"
"-w,w  wide output       n  numeric WCHAN,UID  -H process hierarchy\n"
;
#else
/* FIXME: not up to date ... */
const char *help_message =
"********* available DPM options *********\n"
"--dpmpool=private\n" //17
"--dpmprotos=gridftp,rfio\n" //10
"--dpmproto\n"      // 6
"--dpmfiles\n"      // 6
"--dpmlifet:2000\n" // 6
"--dpmrtime=100\n"  //10
"--dpmftype\n"      // 6
"--dpmflags\n"      // 6
"--dpmoverwrite\n"  //11
"--dpmpaths\n"      // 1
"--dpmkeepspace\n"  //13
"--dpmsurls\n"      //13
"--dpmtosurls\n"    //15
"--dpmfromsurls\n"  //14
"--dpmrtokenlist\n" //16
"--dpmrtoken\n"     //10
"--dpmstoken\n"     // 6
"--dpmutoken\n"     //10
"--dpmpools\n"      //18
"--dpmreqsize\n"      //19
"--dpmdest\n"       //20
"--format=dpmsurl:80,dpmrtyp,dpmpint,dpmfsiz,dpmmode\n" 
"--dpmolder\n" 
"--dpmhost\n" 
"*********  reserved options *********\n"
"--dpmlist=11\n"
"--dpmowner=grodid\n"
"--dpmmatch=agshd[C,E]hskaj\n"
"--dpmdebug\n"
"--dpmregex\n"
"--dpmfull\n"
;
#endif



/* Missing:
 *
 * -P e k
 *
 */
