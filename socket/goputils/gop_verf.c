// $Id: gop_verf.c,v 1.1 2005/01/20 09:47:21 grodid Exp $

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include "gop_verf.h"

#ifdef MINORVERSION
const char procps_version[] = "procps version " VERSION "." SUBVERSION "." MINORVERSION;
#else
const char procps_version[] = "procps version " VERSION "." SUBVERSION;
#endif

void display_version(void) {
    fprintf(stdout, "%s\n", procps_version);
}

#include <sys/utsname.h>

#define LINUX_VERSION(x,y,z)   (0x10000*(x) + 0x100*(y) + z)

int linux_version_code;

static void init_Linux_version(void) __attribute__((constructor));
static void init_Linux_version(void) {
    static struct utsname uts;
    int x = 0, y = 0, z = 0;	/* cleared in case sscanf() < 3 */
    
    if (uname(&uts) == -1)	/* failure implies impending death */
	exit(1);
    if (sscanf(uts.release, "%d.%d.%d", &x, &y, &z) < 3)
	fprintf(stderr,		/* *very* unlikely to happen by accident */
		"Non-standard uts for running kernel:\n"
		"release %s=%d.%d.%d gives version code %d\n",
		uts.release, x, y, z, LINUX_VERSION(x,y,z));
    linux_version_code = LINUX_VERSION(x, y, z);
}
