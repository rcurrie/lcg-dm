// $Id: gop_cach.h,v 1.1 2005/01/20 09:47:20 grodid Exp $

#ifndef PWCACHE_H
#define PWCACHE_H

#include <sys/types.h>
#include "gop_clic.h"

EXTERN_C_BEGIN

// used in pwcache and in readproc to set size of username or groupname
#define P_G_SZ 20

extern char *user_from_uid(uid_t uid);
extern char *group_from_gid(gid_t gid);

EXTERN_C_END

#endif
