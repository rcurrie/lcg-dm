// $Id: gop_chan.h,v 1.1 2005/01/20 09:47:20 grodid Exp $

#ifndef DPMWCHAN_H
#define DPMWCHAN_H

#include "gop_clic.h"

EXTERN_C_BEGIN

extern const char * lookup_wchan(unsigned KLONG address, unsigned pid);
extern int   open_psdb(const char *restrict override);
extern int   open_psdb_message(const char *restrict override, void (*message)(const char *, ...));

EXTERN_C_END

#endif
