// $Id: gop_escp.h,v 1.2 2005/01/25 21:11:58 grodid Exp $

#ifndef ESCAPE_H
#define ESCAPE_H

//#include <stdio.h>
#include "gop_clic.h"
#include <sys/types.h>
#include "gop_readf.h"

EXTERN_C_BEGIN

#define ESC_ARGS     0x1  // try to use cmdline instead of cmd
#define ESC_BRACKETS 0x2  // if using cmd, put '[' and ']' around it
#define ESC_DEFUNCT  0x4  // mark zombies with " <defunct>"

extern int escape_strlist(char *restrict dst, const char *restrict const *restrict src, size_t n);
extern int escape_str(char *restrict dst, const char *restrict src, int bufsize, int maxglyphs);
extern int octal_escape_str(char *restrict dst, const char *restrict src, size_t n);
extern int simple_escape_str(char *restrict dst, const char *restrict src, size_t n);

#if 0
extern int escape_command(char *restrict const outbuf, const proc_t *restrict const pp, int bytes, int glyphs, unsigned flags);
#endif

EXTERN_C_END
#endif
