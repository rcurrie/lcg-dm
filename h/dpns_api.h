/*
 * $Id$
 */

/*
 * Copyright (C) 2004-2009 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpns_api.h,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPNS_API_H
#define _DPNS_API_H
#define C__Cns_errno C__dpns_errno
#define Cns_DIR dpns_DIR
#define Cns_aborttrans dpns_aborttrans
#define Cns_access dpns_access
#define Cns_accessr dpns_accessr
#define Cns_acl dpns_acl
#define Cns_addreplica dpns_addreplica
#define Cns_addreplicax dpns_addreplicax
#define Cns_api_key dpns_api_key
#define Cns_api_thread_info dpns_api_thread_info
#define Cns_apiinit dpns_apiinit
#define Cns_chclass dpns_chclass
#define Cns_chdir dpns_chdir
#define Cns_chmod dpns_chmod
#define Cns_chown dpns_chown
#define Cns_client_getAuthorizationId dpns_client_getAuthorizationId
#define Cns_client_resetAuthorizationId dpns_client_resetAuthorizationId
#define Cns_client_setAuthorizationId dpns_client_setAuthorizationId
#define Cns_client_setVOMS_data dpns_client_setVOMS_data
#define Cns_client_setSecurityProtocols dpns_client_setSecurityProtocols 
#define Cns_closedir dpns_closedir
#define Cns_creat dpns_creat
#define Cns_creatc dpns_creatc
#define Cns_creatg dpns_creatg
#define Cns_creatx dpns_creatx
#define Cns_delcomment dpns_delcomment
#define Cns_delete dpns_delete
#define Cns_deleteclass dpns_deleteclass
#define Cns_delreplica dpns_delreplica
#define Cns_delreplicasbysfn dpns_delreplicasbysfn
#define Cns_direncomm dpns_direncomm
#define Cns_direnrep dpns_direnrep
#define Cns_direnstat dpns_direnstat
#define Cns_direnstatc dpns_direnstatc
#define Cns_direnstatg dpns_direnstatg
#define Cns_du dpns_du
#define Cns_endsess dpns_endsess
#define Cns_endtrans dpns_endtrans
#define Cns_enterclass dpns_enterclass
#define Cns_entergrpmap dpns_entergrpmap
#define Cns_enterusrmap dpns_enterusrmap
#define Cns_errmsg dpns_errmsg
#define Cns_fileclass dpns_fileclass
#define Cns_fileid dpns_fileid
#define Cns_filereg dpns_filereg
#define Cns_filereplica dpns_filereplica
#define Cns_filereplicax dpns_filereplicax
#define Cns_filestat dpns_filestat
#define Cns_filestatg dpns_filestatg
#define Cns_getacl dpns_getacl
#define Cns_getcomment dpns_getcomment
#define Cns_getcwd dpns_getcwd
#define Cns_getifcevers dpns_getifcevers
#define Cns_getlinks dpns_getlinks
#define Cns_getpath dpns_getpath
#define Cns_getreplica dpns_getreplica
#define Cns_getreplicax dpns_getreplicax
#define Cns_lchown dpns_lchown
#define Cns_linkinfo dpns_linkinfo
#define Cns_list dpns_list
#define Cns_listclass dpns_listclass
#define Cns_listlinks dpns_listlinks
#define Cns_listrep4gc dpns_listrep4gc
#define Cns_listreplica dpns_listreplica
#define Cns_listreplicax dpns_listreplicax
#define Cns_listrepset dpns_listrepset
#define Cns_lstat dpns_lstat
#define Cns_mkdir dpns_mkdir
#define Cns_mkdirg dpns_mkdirg
#define Cns_modifyclass dpns_modifyclass
#define Cns_modreplica dpns_modreplica
#define Cns_modreplicax dpns_modreplicax
#define Cns_opendir dpns_opendir
#define Cns_opendirg dpns_opendirg
#define Cns_opendirxg dpns_opendirxg
#define Cns_ping dpns_ping
#define Cns_queryclass dpns_queryclass
#if ((defined(sun) || defined(linux)) && defined _FILE_OFFSET_BITS && _FILE_OFFSET_BITS == 64)
#define dpns_readdir dpns_readdir64
#endif
#define Cns_readdir dpns_readdir
#define Cns_readdir64 dpns_readdir64
#define Cns_readdirc dpns_readdirc
#define Cns_readdirg dpns_readdirg
#define Cns_readdirx dpns_readdirx
#define Cns_readdirxc dpns_readdirxc
#define Cns_readdirxp dpns_readdirxp
#define Cns_readdirxr dpns_readdirxr
#define Cns_readlink dpns_readlink
#define Cns_registerfiles dpns_registerfiles
#define Cns_rename dpns_rename
#define Cns_rep_info dpns_rep_info
#define Cns_rewinddir dpns_rewinddir
#define Cns_rmdir dpns_rmdir
#define Cns_selectsrvr dpns_selectsrvr
#define Cns_set_selectsrvr dpns_set_selectsrvr
#define Cns_setacl dpns_setacl
#define Cns_setatime dpns_setatime
#define Cns_setcomment dpns_setcomment
#define Cns_seterrbuf dpns_seterrbuf
#define Cns_setfsize dpns_setfsize
#define Cns_setfsizec dpns_setfsizec
#define Cns_setfsizeg dpns_setfsizeg
#define Cns_setptime dpns_setptime
#define Cns_setratime dpns_setratime
#define Cns_setrltime dpns_setrltime
#define Cns_setrstatus dpns_setrstatus
#define Cns_setrtype dpns_setrtype
#define Cns_startsess dpns_startsess
#define Cns_starttrans dpns_starttrans
#define Cns_stat dpns_stat
#define Cns_statg dpns_statg
#define Cns_statr dpns_statr
#define Cns_statx dpns_statx
#define Cns_symlink dpns_symlink
#define Cns_umask dpns_umask
#define Cns_undelete dpns_undelete
#define Cns_unlink dpns_unlink
#define Cns_utime dpns_utime
#define send2nsd send2dpns
#define send2nsdx send2dpnsx

#define Cns_getgrpbygid dpns_getgrpbygid
#define Cns_getgrpbygids dpns_getgrpbygids
#define Cns_getgrpbynam dpns_getgrpbynam
#define Cns_getgrpmap dpns_getgrpmap
#define Cns_getidmap dpns_getidmap
#define Cns_getidmapc dpns_getidmapc
#define Cns_getusrbynam dpns_getusrbynam
#define Cns_getusrbyuid dpns_getusrbyuid
#define Cns_getusrmap dpns_getusrmap
#define Cns_groupinfo dpns_groupinfo
#define Cns_modifygrpmap dpns_modifygrpmap
#define Cns_modifyusrmap dpns_modifyusrmap
#define Cns_rmgrpmap dpns_rmgrpmap
#define Cns_rmusrmap dpns_rmusrmap
#define Cns_userinfo dpns_userinfo

#define Cns_setenv dpns_setenv
#define Cns_getenv dpns_getenv

#include "Cns_api.h"
#endif
