/*
 * $Id$
 */

/*
 * Copyright (C) 2010-2011 by CERN/IT/GT/DMS
 * All rights reserved
 */

/*
 * @(#)$RCSfile: Clogit.h $ $Revision$ $Date$ CERN IT-GT/DMS Jean-Philippe Baud
 */

#ifndef _CLOGIT_H
#define _CLOGIT_H
#include <stdarg.h>
#include "osdep.h"

EXTERN_C int DLL_DECL Cinitlog _PROTO((char *, char *));
EXTERN_C int DLL_DECL Clogit _PROTO((int, char *, char *, ...));
EXTERN_C int DLL_DECL Cvlogit _PROTO((int, char *, char *, va_list));
#endif

