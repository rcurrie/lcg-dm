/*
 * Copyright (C) 2005 by CERN
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dli_server.h,v $ $Revision: 1.3 $ $Date: 2008/01/15 10:24:23 $ CERN Jean-Philippe Baud
 */

#ifndef _DLI_SERVER_H
#define _DLI_SERVER_H

			/* lfc-dli server constants and macros */

#define DLI_TIMEOUT 300 /* network timeout while receiving a request */
#define BACKLOG (100)   /* Max. request backlog */
#define LOGBUFSZ 1024
#define PRTBUFSZ 180
#define DLI_NBTHREADS 20
#define DLI_PORT 8085

#define RETURN(x) \
	{ \
	dlilogit (func, "returns %d\n", (x)); \
	return ((x)); \
	}

                        /* lfc-dli server structures */

struct dli_srv_thread_info {
	int		s;		/* socket for communication with client */
	char		errbuf[PRTBUFSZ];
};

			/* lfc-dli server exit codes */

#define USERR	1	/* user error */
#define SYERR	2	/* system error */
#define CONFERR	4	/* configuration error */

			/* lfc-dli server messages */

#define	DLI02	"DLI02 - %s error : %s\n"
#define	DLI98	"DLI98 - %s\n"
#endif
