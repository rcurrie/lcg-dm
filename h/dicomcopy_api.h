/*
 * $Id: dicomcopy_api.h,v 1.1 2008/01/14 10:06:10 szamsu Exp $
 */

/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dicomcopy_api.h,v $ $Revision: 1.1 $ $Date: 2008/01/14 10:06:10 $ CERN IT-GD/ITR Jean-Philippe Baud
 */

#ifndef _DICOMCOPY_API_H
#define _DICOMCOPY_API_H
#include "dicomcopy_constants.h"
#include "osdep.h"

                        /* DPM DICOM client function prototypes */

EXTERN_C int DLL_DECL dicomcopy_inc_reqctr _PROTO(());
EXTERN_C int DLL_DECL send2dicomcopyd _PROTO((char *, char *, int, char *, int));
#endif
