/*
 * $Id: dicomcopy_constants.h,v 1.1 2008/01/14 10:06:10 szamsu Exp $
 */

/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dicomcopy_constants.h,v $ $Revision: 1.1 $ $Date: 2008/01/14 10:06:10 $ CERN IT-GD/ITR Jean-Philippe Baud
 */

#ifndef _DICOMCOPY_CONSTANTS_H
#define _DICOMCOPY_CONSTANTS_H
#include "Castor_limits.h"

			/* DPM DICOM server constants */

#define DICOMCOPYD_PORT 5016

#endif
