/*
 * $Id: Castor_limits.h,v 1.3 2007/02/03 11:16:45 baud Exp $
 */

/*
 * Copyright (C) 1999-2007 by CERN/IT/PDP/DM
 * All rights reserved
 */

/*
 * @(#)$RCSfile: Castor_limits.h,v $ $Revision: 1.3 $ $Date: 2007/02/03 11:16:45 $ CERN IT-PDP/DM Jean-Philippe Baud
 */

#ifndef _CASTOR_LIMITS_H
#define _CASTOR_LIMITS_H

	/* all maximum lengths defined below do not include the trailing null */

#define CA_MAXACLENTRIES 300	/* maximum number of ACL entries for a file/dir */
#define CA_MAXCLASNAMELEN 15	/* maximum length for a fileclass name */
#define	CA_MAXCOMMENTLEN 255	/* maximum length for user comments in metadata */
#define	CA_MAXDENFIELDS    8	/* maximum number of density values in devinfo */
#define	CA_MAXDENLEN       8	/* maximum length for a alphanumeric density */
#define	CA_MAXDGNLEN       6	/* maximum length for a device group name */
#define	CA_MAXDPMTOKENLEN 36	/* maximum length for a DPM token */
#define CA_MAXDVNLEN      63	/* maximum length for a device name */
#define	CA_MAXDVTLEN       8	/* maximum length for a device type */
#define	CA_MAXFIDLEN      17	/* maximum length for a fid (DSN) */
#define	CA_MAXFSEQLEN     14	/* maximum length for a fseq string */
#define	CA_MAXGRPNAMELEN   2	/* maximum length for a group name */
#define	CA_MAXGUIDLEN     36	/* maximum length for a guid */
#define CA_MAXHOSTNAMELEN 63	/* maximum length for a hostname */
#define	CA_MAXLBLTYPLEN    3	/* maximum length for a label type */
#define CA_MAXLINELEN   1023	/* maximum length for a line in a log */
#define	CA_MAXMANUFLEN    12	/* maximum length for a cartridge manufacturer */
#define	CA_MAXMIGPNAMELEN 15	/* maximum length for a migration policy name */
#define	CA_MAXMIGRNAMELEN 15	/* maximum length for a migrator name */
#define	CA_MAXMLLEN        1	/* maximum length for a cartridge media_letter */
#define	CA_MAXMODELLEN     6	/* maximum length for a cartridge model */
#define	CA_MAXNAMELEN    255	/* maximum length for a pathname component */
#define	CA_MAXNBDRIVES    32	/* maximum number of tape drives per server */
#define	CA_MAXPATHLEN   1023	/* maximum length for a pathname */
#define	CA_MAXPOLICYLEN   15	/* maximum length for a policy name */
#define CA_MAXPOOLNAMELEN 15	/* maximum length for a pool name */
#define CA_MAXPROTOLEN     7	/* maximum length for a protocol name */
#define	CA_MAXRBTNAMELEN  17	/* maximum length for a robot name */
#define	CA_MAXRECFMLEN     3	/* maximum length for a record format */
#define CA_MAXREGEXPLEN   63    /* Maximum length for a regular expression */
#define CA_MAXCSECNAMELEN 511	/* Maximum length for a Csec authorization id */
#define CA_MAXCSECPROTOLEN 15	/* Maximum length for a Csec mechanism */
#define	CA_MAXSFNLEN    1103	/* maximum length for a replica */
#define CA_MAXSHORTHOSTLEN 10	/* maximum length for a hostname without domain */
#define	CA_MAXSNLEN       24	/* maximum length for a cartridge serial nb */
#define	CA_MAXSTGRIDLEN   77	/* maximum length for a stager full request id */
				/* must be >= nb digits in CA_MAXSTGREQID +
				   CA_MAXHOSTNAMELEN + 8 */
#define	CA_MAXSTGREQID 999999	/* maximum value for a stager request id */
#define	CA_MAXSYMLINKS     5	/* maximum number of symbolic links */
#define	CA_MAXTAGLEN     255	/* maximum length for a volume tag */
#define	CA_MAXTAPELIBLEN   8	/* maximum length for a tape library name */
#define	CA_MAXUNMLEN       8	/* maximum length for a drive name */
#define	CA_MAXUSRNAMELEN  14	/* maximum length for a login name */
#define	CA_MAXVIDLEN       6	/* maximum length for a VID */
#define	CA_MAXVSNLEN       6	/* maximum length for a VSN */

/* Max allowed uid/gif */
#define CA_MAXUID    0x7FFFFFFF /* Maximum uid */
#define CA_MAXGID    0x7FFFFFFF /* Maximum gid */
#endif
