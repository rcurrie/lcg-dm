/*
 * $Id: dpmcopy_api.h,v 1.1 2008/09/24 11:25:00 dhsmith Exp $
 */

/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpmcopy_api.h,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud
 */

#ifndef _DPMCOPY_API_H
#define _DPMCOPY_API_H
#include "dpmcopy_constants.h"
#include "osdep.h"

                        /* DPM COPY backend client function prototypes */

EXTERN_C int DLL_DECL dpmcopy_inc_reqctr _PROTO(());
EXTERN_C int DLL_DECL send2dpmcopyd _PROTO((char *, char *, int, char *, int));
#endif
