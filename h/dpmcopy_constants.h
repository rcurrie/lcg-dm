/*
 * $Id: dpmcopy_constants.h,v 1.1 2008/09/24 11:25:00 dhsmith Exp $
 */

/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpmcopy_constants.h,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud
 */

#ifndef _DPMCOPY_CONSTANTS_H
#define _DPMCOPY_CONSTANTS_H

			/* DPM COPY backend server constants */

#define DPMCOPYD_PORT 5017

#endif
