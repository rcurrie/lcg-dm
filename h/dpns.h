/*
 * $Id: dpns.h,v 1.2 2006/08/01 12:42:08 baud Exp $
 */

/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpns.h,v $ $Revision: 1.2 $ $Date: 2006/08/01 12:42:08 $ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPNS_H
#define _DPNS_H
#define CNS_SCE "DPNS"
#define CNS_SVC "dpns"
#define CNS_HOME_ENV "DPNS_HOME"
#define CNS_HOST_ENV "DPNS_HOST"
#define CNS_PORT_ENV "DPNS_PORT"
#define CNS_CONNTIMEOUT_ENV "DPNS_CONNTIMEOUT"
#define CNS_CONRETRY_ENV "DPNS_CONRETRY"
#define CNS_CONRETRYINT_ENV "DPNS_CONRETRYINT"
#endif
