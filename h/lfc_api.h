/*
 * $Id$
 */

/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: lfc_api.h,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _LFC_API_H
#define _LFC_API_H
#define C__Cns_errno C__lfc_errno
#define Cns_DIR lfc_DIR
#define Cns_aborttrans lfc_aborttrans
#define Cns_access lfc_access
#define Cns_accessr lfc_accessr
#define Cns_acl lfc_acl
#define Cns_addreplica lfc_addreplica
#define Cns_addreplicax lfc_addreplicax
#define Cns_api_key lfc_api_key
#define Cns_api_thread_info lfc_api_thread_info
#define Cns_apiinit lfc_apiinit
#define Cns_chclass lfc_chclass
#define Cns_chdir lfc_chdir
#define Cns_chmod lfc_chmod
#define Cns_chown lfc_chown
#define Cns_client_getAuthorizationId lfc_client_getAuthorizationId
#define Cns_client_resetAuthorizationId lfc_client_resetAuthorizationId
#define Cns_client_setAuthorizationId lfc_client_setAuthorizationId
#define Cns_client_setVOMS_data lfc_client_setVOMS_data
#define Cns_client_setSecurityProtocols lfc_client_setSecurityProtocols 
#define Cns_closedir lfc_closedir
#define Cns_creat lfc_creat
#define Cns_creatc lfc_creatc
#define Cns_creatg lfc_creatg
#define Cns_creatx lfc_creatx
#define Cns_delcomment lfc_delcomment
#define Cns_delete lfc_delete
#define Cns_deleteclass lfc_deleteclass
#define Cns_delfilesbyguid lfc_delfilesbyguid
#define Cns_delfilesbyname lfc_delfilesbyname
#define Cns_delfilesbypattern lfc_delfilesbypattern
#define Cns_delreplica lfc_delreplica
#define Cns_delreplicas lfc_delreplicas
#define Cns_delreplicasbysfn lfc_delreplicasbysfn
#define Cns_direncomm lfc_direncomm
#define Cns_direnrep lfc_direnrep
#define Cns_direnstat lfc_direnstat
#define Cns_direnstatc lfc_direnstatc
#define Cns_direnstatg lfc_direnstatg
#define Cns_du lfc_du
#define Cns_endsess lfc_endsess
#define Cns_endtrans lfc_endtrans
#define Cns_enterclass lfc_enterclass
#define Cns_entergrpmap lfc_entergrpmap
#define Cns_enterusrmap lfc_enterusrmap
#define Cns_errmsg lfc_errmsg
#define Cns_fileclass lfc_fileclass
#define Cns_fileid lfc_fileid
#define Cns_filereg lfc_filereg
#define Cns_filereplica lfc_filereplica
#define Cns_filereplicas lfc_filereplicas
#define Cns_filereplicax lfc_filereplicax
#define Cns_filestat lfc_filestat
#define Cns_filestatus lfc_filestatus
#define Cns_filestatg lfc_filestatg
#define Cns_getacl lfc_getacl
#define Cns_getcomment lfc_getcomment
#define Cns_getcwd lfc_getcwd
#define Cns_getifcevers lfc_getifcevers
#define Cns_getlinks lfc_getlinks
#define Cns_getpath lfc_getpath
#define Cns_getreplica lfc_getreplica
#define Cns_getreplicas lfc_getreplicas
#define Cns_getreplicasl lfc_getreplicasl
#define Cns_getreplicass lfc_getreplicass
#define Cns_getreplicax lfc_getreplicax
#define Cns_lchown lfc_lchown
#define Cns_linkinfo lfc_linkinfo
#define Cns_list lfc_list
#define Cns_listclass lfc_listclass
#define Cns_listlinks lfc_listlinks
#define Cns_listrep4gc lfc_listrep4gc
#define Cns_listreplica lfc_listreplica
#define Cns_listreplicax lfc_listreplicax
#define Cns_listrepset lfc_listrepset
#define Cns_lstat lfc_lstat
#define Cns_mkdir lfc_mkdir
#define Cns_mkdirg lfc_mkdirg
#define Cns_modifyclass lfc_modifyclass
#define Cns_modreplica lfc_modreplica
#define Cns_modreplicax lfc_modreplicax
#define Cns_opendir lfc_opendir
#define Cns_opendirg lfc_opendirg
#define Cns_opendirxg lfc_opendirxg
#define Cns_ping lfc_ping
#define Cns_queryclass lfc_queryclass
#if ((defined(sun) || defined(linux)) && defined _FILE_OFFSET_BITS && _FILE_OFFSET_BITS == 64)
#define lfc_readdir lfc_readdir64
#endif
#define Cns_readdir lfc_readdir
#define Cns_readdir64 lfc_readdir64
#define Cns_readdirc lfc_readdirc
#define Cns_readdirg lfc_readdirg
#define Cns_readdirx lfc_readdirx
#define Cns_readdirxc lfc_readdirxc
#define Cns_readdirxp lfc_readdirxp
#define Cns_readdirxr lfc_readdirxr
#define Cns_readlink lfc_readlink
#define Cns_registerfiles lfc_registerfiles
#define Cns_rename lfc_rename
#define Cns_rep_info lfc_rep_info
#define Cns_rewinddir lfc_rewinddir
#define Cns_rmdir lfc_rmdir
#define Cns_selectsrvr lfc_selectsrvr
#define Cns_set_selectsrvr lfc_set_selectsrvr
#define Cns_setacl lfc_setacl
#define Cns_setatime lfc_setatime
#define Cns_setcomment lfc_setcomment
#define Cns_seterrbuf lfc_seterrbuf
#define Cns_setfsize lfc_setfsize
#define Cns_setfsizec lfc_setfsizec
#define Cns_setfsizeg lfc_setfsizeg
#define Cns_setptime lfc_setptime
#define Cns_setratime lfc_setratime
#define Cns_setrltime lfc_setrltime
#define Cns_setrstatus lfc_setrstatus
#define Cns_setrtype lfc_setrtype
#define Cns_startsess lfc_startsess
#define Cns_starttrans lfc_starttrans
#define Cns_stat lfc_stat
#define Cns_statg lfc_statg
#define Cns_statr lfc_statr
#define Cns_statx lfc_statx
#define Cns_symlink lfc_symlink
#define Cns_umask lfc_umask
#define Cns_undelete lfc_undelete
#define Cns_unlink lfc_unlink
#define Cns_utime lfc_utime
#define send2nsd send2lfc
#define send2nsdx send2lfcx

#define Cns_getgrpbygid lfc_getgrpbygid
#define Cns_getgrpbygids lfc_getgrpbygids
#define Cns_getgrpbynam lfc_getgrpbynam
#define Cns_getgrpmap lfc_getgrpmap
#define Cns_getidmap lfc_getidmap
#define Cns_getidmapc lfc_getidmapc
#define Cns_getusrbynam lfc_getusrbynam
#define Cns_getusrbyuid lfc_getusrbyuid
#define Cns_getusrmap lfc_getusrmap
#define Cns_groupinfo lfc_groupinfo
#define Cns_modifygrpmap lfc_modifygrpmap
#define Cns_modifyusrmap lfc_modifyusrmap
#define Cns_rmgrpmap lfc_rmgrpmap
#define Cns_rmusrmap lfc_rmusrmap
#define Cns_userinfo lfc_userinfo

#define Cns_setenv lfc_setenv
#define Cns_getenv lfc_getenv

#include "Cns_api.h"
#endif
