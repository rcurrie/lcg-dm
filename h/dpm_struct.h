/*
 * $Id$
 */

/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm_struct.h,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPM_STRUCT_H
#define _DPM_STRUCT_H

                        /* structures common to Disk Pool Manager client and server */

struct dpm_pool {
	char		poolname[CA_MAXPOOLNAMELEN+1];
	u_signed64	defsize;
	int		gc_start_thresh;
	int		gc_stop_thresh;
	int		def_lifetime;
	int		defpintime;
	int		max_lifetime;
	int		maxpintime;
	char		fss_policy[CA_MAXPOLICYLEN+1];
	char		gc_policy[CA_MAXPOLICYLEN+1];
	char		mig_policy[CA_MAXPOLICYLEN+1];
	char		rs_policy[CA_MAXPOLICYLEN+1];
	int		nbgids;
	gid_t		*gids;		/* restrict the pool to given group(s) */
	char		ret_policy;	/* retention policy: 'R', 'O' or 'C' */
	char		s_type;		/* space type: 'V', 'D' or 'P' */
	u_signed64	capacity;
	signed64	free;
	struct dpm_fs	*elemp;
	int		nbelem;
	int		next_elem;	/* next pool element to be used */
	int		*fs_vec;
	int		fs_veclen;
};

struct dpm_fs {
	char		poolname[CA_MAXPOOLNAMELEN+1];
	char		server[CA_MAXHOSTNAMELEN+1];
	char		fs[80];
	u_signed64	capacity;
	signed64	free;
	int		status;
	int		weight;
};
#endif
