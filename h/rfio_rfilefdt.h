/*
 * $Id: rfio_rfilefdt.h,v 1.2 2005/05/04 06:06:57 baud Exp $
 */

#ifndef __rfio_rfilefdt_h
#define __rfio_rfilefdt_h
#include "osdep.h"

EXTERN_C int DLL_DECL rfio_rfilefdt_allocentry _PROTO((int));
#define FINDRFILE_WITH_SCAN     1
#define FINDRFILE_WITHOUT_SCAN  0
EXTERN_C int DLL_DECL rfio_rfilefdt_findentry _PROTO((int, int));
#if defined(RFIO_KERNEL)
EXTERN_C int DLL_DECL rfio_rfilefdt_findptr _PROTO((RFILE *, int));
#else /* RFIO_KERNEL */
EXTERN_C int DLL_DECL rfio_rfilefdt_findptr _PROTO((FILE *, int));
#endif /* RFIO_KERNEL */
EXTERN_C int DLL_DECL rfio_rfilefdt_freeentry _PROTO((int));

#endif /* __rfio_rfilefdt_h */
