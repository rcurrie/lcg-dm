/*
 * $Id: Cinit.h,v 1.1.1.1 2001/11/08 11:20:28 baud Exp $
 */

/*
 * Copyright (C) 2000 by CERN/IT/PDP/DM
 * All rights reserved
 */

/*
 * @(#)$RCSfile: Cinit.h,v $ $Revision: 1.1.1.1 $ $Date: 2001/11/08 11:20:28 $ CERN IT-PDP/DM Jean-Philippe Baud
 */

#ifndef _CINIT_H
#define _CINIT_H

#include "osdep.h"

	/* structure to be used with Cinitdaemon()/Cinitservice() */

struct main_args {
	int	argc;
	char	**argv;
};

EXTERN_C int Cinitdaemon _PROTO((char *, void (*) _PROTO((int))));

#endif
