/*
 * $RCSfile: Cthread_env.h,v $ $Revision: 1.0 $ $Date: 2011/11/07 17:13:00 $ CERN IT-GT/DMS Alejandro Álvarez Ayllón
 */

/*
 * Copyright (C) 2011 by CERN/IT/GT/DMS
 * All rights reserved
 */

#ifndef __Cthread_env_h
#define	__Cthread_env_h

#include <osdep.h>

EXTERN_C int   DLL_DECL Cthread_setenv _PROTO((const char*, const char*, int));
EXTERN_C char* DLL_DECL Cthread_getenv _PROTO((const char*));

#endif	/* __Cthread_env_h */

