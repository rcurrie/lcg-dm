/*
 * $Id$
 */

/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm_server.h,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPM_SERVER_H
#define _DPM_SERVER_H
#ifdef USE_MYSQL
#include <mysql.h>
#else
#ifdef USE_POSTGRES
#include <libpq-fe.h>
#endif
#endif
#include "osdep.h"
#ifdef CSEC
#include "Csec_api.h"
#endif
#include "dpm_struct.h"

                        /* Disk Pool Manager constants and macros */

#define CHECKEXP	3600	/* max interval to check for expired puts/spaces and requests */
#define CHECKI	5	/* max interval to check for work to be done */
#define DPM_DBPINGI	600	/* max interval before pinging DB */
#define DPM_MAXNBTHREADS 1000	/* maximum number of threads */
#define DPM_NBFTHREADS	20	/* number of threads to process fast requests */
#define DPM_NBSTHREADS	20	/* number of threads to process slow requests */
#define DPM_NBFTHREADSMULT 5    /* Thread multiplier when pooling the db connections of the dpm daemon */
#define DPM_MAXNBPOOLS	(DPM_MAXNBTHREADS-DPM_NBFTHREADS-DPM_NBSTHREADS-2)
#define DPM_FSCHECK	60	/* max interval before updating the free space in the filesystems */

#define RETURN(x) \
	{ \
	dpmlogit (func, "returns %d\n", (x)); \
	if (thip->dbfd.tr_started) {\
		if (x) \
			(void) dpm_abort_tr (&thip->dbfd); \
		else \
			(void) dpm_end_tr (&thip->dbfd); \
	}\
	return ((x)); \
	}
#define RETURNS(x,c) \
	{ \
	char buf[256]; \
	dpmlogit (func, "returns %d, status=%s\n", (x), status2str (c, buf)); \
	if (thip->dbfd.tr_started) {\
		if (x) \
			(void) dpm_abort_tr (&thip->dbfd); \
		else \
			(void) dpm_end_tr (&thip->dbfd); \
	}\
	return ((x)); \
	}

                        /* Disk Pool Manager server structures */

struct dpm_dbfd {
	int		idx;		/* index in array of dpm_dbfd */
#ifdef USE_MYSQL
	MYSQL		mysql;
#else
#ifdef USE_POSTGRES
	PGconn		*Pconn;
#endif
#endif
	int		tr_started;
};

#ifdef USE_ORACLE
typedef char dpm_dbrec_addr[19];
typedef int DBLISTPTR;
#else
#ifdef USE_MYSQL
typedef char dpm_dbrec_addr[21];
typedef MYSQL_RES *DBLISTPTR;
#else
#ifdef USE_POSTGRES
typedef char dpm_dbrec_addr[21];
typedef struct {
	PGresult *res;
	int row;
} DBLISTPTR;
#endif
#endif
#endif

struct dpm_srv_thread_info {
	int		s;		/* socket for communication with client */
	int		db_open_done;
	time_t		last_db_use;
	struct dpm_dbfd	dbfd;
	char		errbuf[PRTBUFSZ];
#ifdef CSEC
	Csec_context_t	sec_ctx;
	uid_t		Csec_uid;
	gid_t		Csec_gid;
	int		Csec_nbgids;
	gid_t		*Csec_gids;
	char		*Csec_mech;
	char		*Csec_auth_id;
#endif
};

struct dpm_req {
	int		r_ordinal;
	char		r_token[CA_MAXDPMTOKENLEN+1];
	uid_t		r_uid;
	gid_t		r_gid;
	char		client_dn[256];
	char		clienthost[CA_MAXHOSTNAMELEN+1];
	char		r_type;
	char		u_token[256];
	int		flags;
	time_t		retrytime;
	int		nbreqfiles;
	time_t		ctime;		/* submit time */
	time_t		stime;		/* start time */
	time_t		etime;		/* end time */
	int		status;
	char		errstring[256];
	char		groups[256];
};

struct dpm_req_cleanup {
	char scaning;
	time_t time_lower;
	time_t time_upper;
	time_t req_retention;
	u_signed64 num_deleted;
	int max_recs;
	struct dpm_dbfd *dbfd;
};

struct dpm_get_filereq {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	int		f_ordinal;
	uid_t		r_uid;
	char		from_surl[CA_MAXSFNLEN+1];
	char		protocol[CA_MAXPROTOLEN+1];
	time_t		lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	int		flags;
	char		server[CA_MAXHOSTNAMELEN+1];
	char		pfn[CA_MAXSFNLEN+1];
	u_signed64	actual_size;
	int		status;
	char		errstring[256];
};

struct dpm_put_filereq {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	int		f_ordinal;
	char		to_surl[CA_MAXSFNLEN+1];
	char		protocol[CA_MAXPROTOLEN+1];
	time_t		lifetime;
	time_t		f_lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	char		ac_latency;
	u_signed64	requested_size;
	char		server[CA_MAXHOSTNAMELEN+1];
	char		pfn[CA_MAXSFNLEN+1];
	u_signed64	actual_size;
	int		status;
	char		errstring[256];
};

struct dpm_copy_filereq {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	int		f_ordinal;
	char		from_surl[CA_MAXSFNLEN+1];
	char		to_surl[CA_MAXSFNLEN+1];
	time_t		f_lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	char		ac_latency;
	int		flags;
	u_signed64	actual_size;
	int		status;
	char		errstring[256];
};

struct dpm_space_reserv {
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		client_dn[256];
	uid_t		s_uid;
	gid_t		s_gid;
	char		ret_policy;
	char		ac_latency;
	char		s_type;
	char		u_token[256];
	u_signed64	t_space;	/* Total space */
	u_signed64	g_space;	/* Guaranteed space */
	signed64	u_space;	/* Unused space */
	char		poolname[CA_MAXPOOLNAMELEN+1];
	time_t		assign_time;	/* Timestamp of space allocation */
	time_t		expire_time;
	char		groups[256];	/* restrict the space to given group(s) */
};

struct gc_entry {
	int		gc_idx;
	char		poolname[CA_MAXPOOLNAMELEN+1];
	int		status;	/* 0 --> free, 1 --> active, 2 -> being removed */
	int		tid;	/* thread id */
};
			/* Disk Pool Manager server function prototypes */

EXTERN_C int dpm_abort_tr _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_addfs2poolconf _PROTO((struct dpm_fs *));
EXTERN_C int dpm_addpool2poolconf _PROTO((struct dpm_pool *));
EXTERN_C int dpm_build_fs_vector _PROTO((int, struct dpm_fs *, int **));
EXTERN_C int dpm_closedb _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_compute_fs_vector_elem _PROTO((int, int *, int *, int));
EXTERN_C int dpm_delete_cprs_by_token _PROTO((struct dpm_dbfd *, char *));
EXTERN_C int dpm_delete_fs_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *));
EXTERN_C int dpm_delete_gfrs_by_token _PROTO((struct dpm_dbfd *, char *));
EXTERN_C int dpm_delete_pending_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *));
EXTERN_C int dpm_delete_pfrs_by_token _PROTO((struct dpm_dbfd *, char *));
EXTERN_C int dpm_delete_pool_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *));
EXTERN_C int dpm_delete_req_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *));
EXTERN_C int dpm_delete_spcmd_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *));
EXTERN_C int dpm_end_tr _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_enoughfreespace _PROTO((char *, int));
EXTERN_C int dpm_get_cpr_by_fullid _PROTO((struct dpm_dbfd *, char *, int, struct dpm_copy_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_cpr_by_surl _PROTO((struct dpm_dbfd *, char *, char *, struct dpm_copy_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_cpr_by_surls _PROTO((struct dpm_dbfd *, char *, char *, char *, struct dpm_copy_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_dbconf _PROTO((char *));
EXTERN_C int dpm_get_def_lifetime _PROTO((char *, char *));
EXTERN_C int dpm_get_defpintime _PROTO((char *));
EXTERN_C int dpm_get_fs_entry _PROTO((struct dpm_dbfd *, char *, char *, struct dpm_fs *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_fs_status _PROTO((char *, char *, int *));
EXTERN_C int dpm_get_fs_status_and_check_pfn _PROTO((char *, char *, char *, int *));
EXTERN_C int dpm_get_gfr_by_fullid _PROTO((struct dpm_dbfd *, char *, int, struct dpm_get_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_gfr_by_pfn _PROTO((struct dpm_dbfd *, int, char *, struct dpm_get_filereq *, int, dpm_dbrec_addr *, int, DBLISTPTR *));
EXTERN_C int dpm_get_gfr_by_surl _PROTO((struct dpm_dbfd *, char *, char *, struct dpm_get_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_max_get_lifetime _PROTO((struct dpm_dbfd *, char *, time_t *));
EXTERN_C int dpm_get_max_lifetime _PROTO((char *));
EXTERN_C int dpm_get_maxpintime _PROTO((char *));
EXTERN_C int dpm_get_next_req _PROTO((struct dpm_dbfd *, int, struct dpm_req *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_pending_req_by_token _PROTO((struct dpm_dbfd *, char *, struct dpm_req *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_pending_reqs_by_u_desc _PROTO((struct dpm_dbfd *, int, char *, uid_t, struct dpm_req *, int, DBLISTPTR *));
EXTERN_C int dpm_get_pfr_by_fullid _PROTO((struct dpm_dbfd *, char *, int, struct dpm_put_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_pfr_by_pfn _PROTO((struct dpm_dbfd *, char *, struct dpm_put_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_pfr_by_surl _PROTO((struct dpm_dbfd *, char *, char *, struct dpm_put_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_getpoolconf _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_get_pool_entry _PROTO((struct dpm_dbfd *, char *, struct dpm_pool *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_req_by_token _PROTO((struct dpm_dbfd *, char *, struct dpm_req *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_reqs4del _PROTO((struct dpm_dbfd *, int, time_t, time_t, int, struct dpm_req *, dpm_dbrec_addr *, int, DBLISTPTR *));
EXTERN_C int dpm_get_reqs_by_u_desc _PROTO((struct dpm_dbfd *, int, char *, uid_t, struct dpm_req *, int, DBLISTPTR *));
EXTERN_C int dpm_get_spcmd_by_token _PROTO((struct dpm_dbfd *, char *, struct dpm_space_reserv *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_spcmd_by_u_desc _PROTO((struct dpm_dbfd *, int, char *, uid_t, struct dpm_space_reserv *, int, DBLISTPTR *));
EXTERN_C int dpm_get_spcmd_by_pathpfx _PROTO((struct dpm_dbfd *, char *, struct dpm_space_reserv *, int, DBLISTPTR *));
EXTERN_C int dpm_getfsfrompoolconf _PROTO((char *, struct dpm_fs **));
EXTERN_C int dpm_getonereqsummary _PROTO((struct dpm_srv_thread_info *, char *, char *, int *, int *, int *, int *, int *));
EXTERN_C int dpm_getpoolsfrompoolconf _PROTO((struct dpm_pool **));
EXTERN_C int dpm_init_dbpkg _PROTO(());
EXTERN_C int dpm_insert_cpr_entry _PROTO((struct dpm_dbfd *, struct dpm_copy_filereq *));
EXTERN_C int dpm_insert_fs_entry _PROTO((struct dpm_dbfd *, struct dpm_fs *));
EXTERN_C int dpm_insert_gfr_entry _PROTO((struct dpm_dbfd *, struct dpm_get_filereq *));
EXTERN_C int dpm_insert_pending_entry _PROTO((struct dpm_dbfd *, struct dpm_req *));
EXTERN_C int dpm_insert_pfr_entry _PROTO((struct dpm_dbfd *, struct dpm_put_filereq *));
EXTERN_C int dpm_insert_pool_entry _PROTO((struct dpm_dbfd *, struct dpm_pool *));
EXTERN_C int dpm_insert_spcmd_entry _PROTO((struct dpm_dbfd *, struct dpm_space_reserv *));
EXTERN_C int dpm_insert_xferreq_entry _PROTO((struct dpm_dbfd *, struct dpm_req *));
EXTERN_C int dpm_list_cpr_entry _PROTO((struct dpm_dbfd *, int, char *, struct dpm_copy_filereq *, int, dpm_dbrec_addr *, int, DBLISTPTR *));
EXTERN_C int dpm_list_expired_puts _PROTO((struct dpm_dbfd *, int, struct dpm_put_filereq *, int, DBLISTPTR *));
EXTERN_C int dpm_list_expired_spaces _PROTO((struct dpm_dbfd *, int, struct dpm_space_reserv *, int, DBLISTPTR *));
EXTERN_C int dpm_list_rr_puts _PROTO((struct dpm_dbfd *, int, struct dpm_put_filereq *, int, DBLISTPTR *));
EXTERN_C int dpm_list_fs_entry _PROTO((struct dpm_dbfd *, int, char *, struct dpm_fs *, int, DBLISTPTR *));
EXTERN_C int dpm_list_gfr_entry _PROTO((struct dpm_dbfd *, int, char *, struct dpm_get_filereq *, int, dpm_dbrec_addr *, int, DBLISTPTR *));
EXTERN_C int dpm_list_pending_req _PROTO((struct dpm_dbfd *, int, struct dpm_req *, int, dpm_dbrec_addr *, int, DBLISTPTR *));
EXTERN_C int dpm_list_pfr_entry _PROTO((struct dpm_dbfd *, int, char *, struct dpm_put_filereq *, int, dpm_dbrec_addr *, int, DBLISTPTR *));
EXTERN_C int dpm_list_pool_entry _PROTO((struct dpm_dbfd *, int, struct dpm_pool *, int, DBLISTPTR *));
EXTERN_C int dpm_list_spcmd_entry _PROTO((struct dpm_dbfd *, int, struct dpm_space_reserv *, int, DBLISTPTR *));
EXTERN_C int dpm_modfsinpoolconf _PROTO((char *, char *, int, int));
EXTERN_C int dpm_fixfsfreespaceinpoolconf (char *, char *, signed64);
EXTERN_C int dpm_modpoolinpoolconf _PROTO((char *, u_signed64, int, int, int, int, int, int, char *, char *, char *, char *, int, gid_t *, char, char));
EXTERN_C int dpm_opendb _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_pingdb _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_reallocate_space _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_recover_queue _PROTO((struct dpm_dbfd *));
EXTERN_C int dpm_relonefile _PROTO((struct dpm_srv_thread_info *, struct dpm_get_filereq *, dpm_dbrec_addr *, int *));
EXTERN_C int dpm_reset_gc_entry _PROTO((int));
EXTERN_C int dpm_rmfsfrompoolconf _PROTO((char *, char *));
EXTERN_C int dpm_rmpoolfrompoolconf _PROTO((char *));
EXTERN_C int dpm_selectfs _PROTO((gid_t, char, char, char, char **, char **, u_signed64 *, char *, char *, char *, int *));
EXTERN_C int dpm_selectpool _PROTO((gid_t, char, char, char, u_signed64, char *, char *, int));
EXTERN_C int dpm_start_tr _PROTO((int, struct dpm_dbfd *));
EXTERN_C int dpm_stop_gc _PROTO((char *));
EXTERN_C int dpm_unique_id _PROTO((struct dpm_dbfd *, u_signed64 *));
EXTERN_C int dpm_upd_u_space _PROTO((struct dpm_dbfd *, char *, signed64));
EXTERN_C int dpm_update_cpr_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_copy_filereq *));
EXTERN_C int dpm_update_fs_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_fs *));
EXTERN_C int dpm_update_gfr_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_get_filereq *));
EXTERN_C int dpm_update_pending_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_req *));
EXTERN_C int dpm_update_pfr_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_put_filereq *));
EXTERN_C int dpm_update_pool_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_pool *));
EXTERN_C int dpm_update_spcmd_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_space_reserv *));
EXTERN_C int dpm_update_uspcincr_spcmd_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_space_reserv *, signed64 uspaceincr));
EXTERN_C int dpm_update_xferreq_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_req *));
EXTERN_C int dpm_updfreespace _PROTO((char *, signed64, char *, int));
EXTERN_C int dpm_updpoolfreespace _PROTO((char *, signed64));
EXTERN_C int dpm_wait4allgcs _PROTO(());
#endif
