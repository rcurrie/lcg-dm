/*
 * $Id$
 */

/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm_api.h,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPM_API_H
#define _DPM_API_H
#include "dpm_constants.h"
#include "osdep.h"
#include "dpm_struct.h"

int *C__dpm_errno();
#define dpm_errno (*C__dpm_errno())

			/* Disk Pool Manager client structures */

struct dpm_api_thread_info {
	char *		errbufp;
	int		errbuflen;
	int		initialized;
	int		dp_errno;
#ifdef CSEC
	int		use_authorization_id;
	uid_t		Csec_uid;
	gid_t		Csec_gid;
	char		Csec_mech[CA_MAXCSECPROTOLEN+1];
	char		Csec_auth_id[CA_MAXCSECNAMELEN+1];
	int		Csec_opt;
	char		*voname;
	char		**fqan;
	int		nbfqan;
#endif
};

struct dpm_copyfilereq {
	char		*from_surl;
	char		*to_surl;
	time_t		lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	char		ac_latency;
	int		flags;
};

struct dpm_copyfilestatus {
	char		*from_surl;
	char		*to_surl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t		f_lifetime;
};

struct dpm_filestatus {
	char		*surl;
	int		status;
	char		*errstring;
};

struct dpm_getfilereq {
	char		*from_surl;
	time_t		lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	int		flags;
};

struct dpm_getfilestatus {
	char		*from_surl;
	char		*turl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t		pintime;
};

struct dpm_putfilereq {
	char		*to_surl;
	time_t		lifetime;
	time_t		f_lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	char		ac_latency;
	u_signed64	requested_size;
};

struct dpm_putfilereqx {
	char		*to_surl;
	time_t		lifetime;
	time_t		f_lifetime;
	char		f_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	char		ac_latency;
	u_signed64	requested_size;
	int		reserved; /* must be zero */
	char		server[CA_MAXHOSTNAMELEN+1];
	char		pfnhint[CA_MAXSFNLEN+1];
};

struct dpm_putfilestatus {
	char		*to_surl;
	char		*turl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t		pintime;
	time_t		f_lifetime;
};

struct dpm_reqsummary {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	char		r_type;
	int		nb_reqfiles;
	int		nb_queued;
	int		nb_finished;
	int		nb_progress;
};

struct dpm_space_metadata {
	char		s_type;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	uid_t		s_uid;
	gid_t		s_gid;
	char		ret_policy;
	char		ac_latency;
	char		u_token[256];
	char		client_dn[256];
	u_signed64	t_space;	/* Total space */
	u_signed64	g_space;	/* Guaranteed space */
	u_signed64	u_space;	/* Unused space */
	char		poolname[CA_MAXPOOLNAMELEN+1];
	time_t		a_lifetime;	/* Lifetime assigned */
	time_t		r_lifetime;	/* Remaining lifetime */
	int		nbgids;
	gid_t		*gids;		/* restrict the space to given group(s) */
};

struct dpm_tokeninfo {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	time_t		c_time;
};


#ifndef SWIGNOPROTO        /* include function prototypes only if outside SWIG */

			/* function prototypes */

EXTERN_C int DLL_DECL dpm_abortfiles _PROTO((char *, int, char **, int *, struct dpm_filestatus **));
EXTERN_C int DLL_DECL dpm_abortreq _PROTO((char *));
EXTERN_C int DLL_DECL dpm_accessr _PROTO((const char*, int));
EXTERN_C int DLL_DECL dpm_addfs _PROTO((char *, char *, char *, int, int));
EXTERN_C int DLL_DECL dpm_addpool _PROTO((struct dpm_pool *));
EXTERN_C int DLL_DECL dpm_apiinit _PROTO((struct dpm_api_thread_info **));
EXTERN_C int DLL_DECL dpm_client_getAuthorizationId _PROTO((uid_t *, gid_t *, char **, char **));
EXTERN_C int DLL_DECL dpm_client_setAuthorizationId _PROTO((uid_t, gid_t, const char *, char *));
EXTERN_C int DLL_DECL dpm_client_resetAuthorizationId _PROTO(());
EXTERN_C int DLL_DECL dpm_client_setSecurityOpts _PROTO((int));
EXTERN_C int DLL_DECL dpm_client_setVOMS_data _PROTO((char *, char **, int));
EXTERN_C int DLL_DECL dpm_copy _PROTO((int, struct dpm_copyfilereq *, char *, int, time_t, char *, int *, struct dpm_copyfilestatus **));
#ifndef SWIG
EXTERN_C int DLL_DECL dpm_copyfile _PROTO((char *, char *, int, int));
#endif
EXTERN_C int DLL_DECL dpm_delreplica _PROTO((char *));
EXTERN_C int DLL_DECL dpm_errmsg _PROTO((char *, char *, ...));
EXTERN_C int DLL_DECL dpm_extendfilelife _PROTO((char *, char *, time_t, time_t *));
EXTERN_C void DLL_DECL dpm_free_cfilest _PROTO((int, struct dpm_copyfilestatus *));
EXTERN_C void DLL_DECL dpm_free_filest _PROTO((int, struct dpm_filestatus *));
EXTERN_C void DLL_DECL dpm_free_gfilest _PROTO((int, struct dpm_getfilestatus *));
EXTERN_C void DLL_DECL dpm_free_pfilest _PROTO((int, struct dpm_putfilestatus *));
EXTERN_C int DLL_DECL dpm_get _PROTO((int, struct dpm_getfilereq *, int, char **, char *, time_t, char *, int *, struct dpm_getfilestatus **));
EXTERN_C int DLL_DECL dpm_getifcevers _PROTO((char *));
EXTERN_C int DLL_DECL dpm_getpoolfs _PROTO((char *, int *, struct dpm_fs **));
EXTERN_C int DLL_DECL dpm_getpools _PROTO((int *, struct dpm_pool **));
EXTERN_C int DLL_DECL dpm_getprotocols _PROTO((int *, char ***));
EXTERN_C int DLL_DECL dpm_getreqid _PROTO((const char *, int *, struct dpm_tokeninfo **));
EXTERN_C int DLL_DECL dpm_getreqsummary _PROTO((int, char **, int *, struct dpm_reqsummary **));
EXTERN_C int DLL_DECL dpm_getspacemd _PROTO((int, char **, int *, struct dpm_space_metadata **));
EXTERN_C int DLL_DECL dpm_getspacetoken _PROTO((const char *, int *, char ***));
EXTERN_C int DLL_DECL dpm_getstatus_copyreq _PROTO((char *, int, char **, char **, int *, struct dpm_copyfilestatus **));
EXTERN_C int DLL_DECL dpm_getstatus_getreq _PROTO((char *, int, char **, int *, struct dpm_getfilestatus **));
EXTERN_C int DLL_DECL dpm_getstatus_putreq _PROTO((char *, int, char **, int *, struct dpm_putfilestatus **));
EXTERN_C char DLL_DECL *dpm_getturl _PROTO((const char *, int, u_signed64, char *));
EXTERN_C int DLL_DECL dpm_inc_reqctr _PROTO(());
EXTERN_C int DLL_DECL dpm_modifyfs _PROTO((char *, char *, int, int));
EXTERN_C int DLL_DECL dpm_modifypool _PROTO((struct dpm_pool *));
EXTERN_C int DLL_DECL dpm_ping _PROTO((char *, char *));
EXTERN_C int DLL_DECL dpm_put _PROTO((int, struct dpm_putfilereq *, int, char **, char *, int, time_t, char *, int *, struct dpm_putfilestatus **));
EXTERN_C int DLL_DECL dpm_putx _PROTO((int, struct dpm_putfilereqx *, int, char **, char *, int, time_t, char *, int *, struct dpm_putfilestatus **));
EXTERN_C int DLL_DECL dpm_putdone _PROTO((char *, int, char **, int *, struct dpm_filestatus **));
EXTERN_C int DLL_DECL dpm_releasespace _PROTO((char *, int));
EXTERN_C int DLL_DECL dpm_relfiles _PROTO((char *, int, char **, int, int *, struct dpm_filestatus **));
#ifndef SWIG
EXTERN_C int DLL_DECL dpm_replicate _PROTO((const char *, char));
EXTERN_C int DLL_DECL dpm_replicatex _PROTO((const char *, char, const char *, time_t, char *));
#endif
EXTERN_C int DLL_DECL dpm_reservespace _PROTO((const char, const char *, const char, const char, u_signed64, u_signed64, time_t, int, gid_t *, const char *, char *, u_signed64 *, u_signed64 *, time_t *, char *));
EXTERN_C int DLL_DECL dpm_rm _PROTO((int, char **, int *, struct dpm_filestatus **));
EXTERN_C int DLL_DECL dpm_rmfs _PROTO((char *, char *));
EXTERN_C int DLL_DECL dpm_rmpool _PROTO((char *));
EXTERN_C int DLL_DECL dpm_seterrbuf _PROTO((char *, int));
EXTERN_C int DLL_DECL dpm_updatespace _PROTO((char *, u_signed64, u_signed64, time_t, int, gid_t *, u_signed64 *, u_signed64 *, time_t *));
EXTERN_C int DLL_DECL send2dpm _PROTO((char *, char *, int, char *, int, void **, int *));

#endif /* #ifndef SWIGNOPROTO */

#endif
