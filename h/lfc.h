/*
 * $Id: lfc.h,v 1.2 2006/08/01 12:42:08 baud Exp $
 */

/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: lfc.h,v $ $Revision: 1.2 $ $Date: 2006/08/01 12:42:08 $ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _LFC_H
#define _LFC_H
#define CNS_SCE "LFC"
#define CNS_SVC "lfc"
#define CNS_HOME_ENV "LFC_HOME"
#define CNS_HOST_ENV "LFC_HOST"
#define CNS_PORT_ENV "LFC_PORT"
#define CNS_CONNTIMEOUT_ENV "LFC_CONNTIMEOUT"
#define CNS_CONRETRY_ENV "LFC_CONRETRY"
#define CNS_CONRETRYINT_ENV "LFC_CONRETRYINT"
#endif
