	@(#)$RCSfile: README,v $ $Revision: 1.1.1.1 $ $Date: 2004/11/21 06:38:30 $ CERN IT-PDP/DM Jean-Philippe Baud

                     imake and LCG-DM
                     -----------------

The LCG-DM software generation uses imake.
This eases the port to new platforms, the customization for a specific
project or site and an easy selection of a specific component.
Furthermore the type of platform is normally selected without manual
intervention.

Implementation
--------------

Two directories have been added to the source structure: imake and
config. The first one contains the source of the imake program which is part
of the X11 distribution, the second one contains templates:

   Imake.tmpl			does platform selection and includes
	site.def		site selected defaults
	'platform'.cf		platform specific template
	Imake.rules		project specific rules
   Library.tmpl			selects the list of libraries to put in 
				the archive and shared global libraries.
   Project.tmpl			selects the list of directories from
				the site selected defaults.

Setting the defaults
--------------------

Only the file site.def should be edited.
It contains the pathnames for the configuration files, the logfiles
and the directories where to install the commands, libraries and man pages.
Be careful: put only one tab between variable name and value, otherwise cpp
will crash on certain platforms.
You specify there which components you want to be built:

   BuildDPMClient		Disk Pool Manager Client api and utilities
   BuildDPMServer		Disk Pool Manager daemon
   BuildNameServerClient	Name Server Client api and utilities
   BuildNameServerDaemon	Name Server daemon
   BuildNameServerLibrary	Name Server API library
   BuildNameServerAsLFC		LFC flavour of the Name Server (guid)
   BuildNameServerAsDPNS	DPNS flavour of the Name Server (DPM Name Space)
   BuildSRMv1Server		Storage Resource Manager v1
   BuildSRMv2Server		Storage Resource Manager v2
   BuildTest			LCG-DM test suite

You specify there also special options like

   HasNroff		to use the nroff program to format the man pages
   UseMySQL		to use MySQL for the backend database
   UseOracle		to use Oracle for the backend database



Software build
--------------

When defaults are correctly set up in site.def, you may build the software.
If you are on a Unix platform, you use:

	make

On Windows/NT, you should say:

	make -f Makefile.ini.Win32

(the program imake is automatically build if necessary).

If you just want to create the makefiles without compiling,
you should say at the top directory:

	make Makefiles (under Unix)
or
	make -f Makefile.ini.Win32 Makefiles (under Windows/NT)

Then you may simply type:

	make (under Unix)
or
	nmake (under Windows/NT)

in any subdirectory.

If the Makefile at the top directory gets corrupted, you may restore
a working version by typing:

	cp Makefile.ini Makefile  (under Unix)
or
	cp Makefile.ini.Win32 Makefile (under Windows/NT)

Then you can execute make as indicated above.
