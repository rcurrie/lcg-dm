#!/usr/bin/perl
#
# This script creates users for the RMC and LRC for a single VO.
#
# Author: James Casey <james.casey@cern.ch>
# Author: Miguel Anjo <miguel.anjo@cern.ch>
#
# 2004/12/14 : Changes made for the LCG File Catalog (LFC) by Sophie Lemaitre <Sophie.Lemaitre@cern.ch>
#

use strict;
use warnings;

use Getopt::Long;
use Env qw(ORACLE_SID ORACLE_HOME);
	  
#
# Configuration constants.  These could be changed if required
#

#################################################################
# start of script - do not change past here
#################################################################

#
# forwards
#
sub configuration();
sub usage($);
	  
#
# check env variables
# 
die "ORACLE_HOME environment variable not defined.\n" 
	if !defined($ORACLE_HOME);	
die "ORACLE_SID environment variable not defined.\n" 
	if !defined($ORACLE_SID);	

#
# get options
#
my ($name, $password);
my $tempTablespace="TEMP01";

my $verbose=0;
GetOptions("name=s" => \$name,
	   "password=s" => \$password,
	   "temp=s" => \$tempTablespace,
	   "v" => \$verbose);

usage("No user name specified") if !$name;
usage("No password specified") if !$password;

# capitalize
$name =~ tr/a-z/A-Z/;
$tempTablespace=~ tr/a-z/A-Z/;

# prefix the name with "LFC_"
$name="LFC_${name}";

my $logFile="/tmp/create-users.$$.log";

configuration() if $verbose;

print "Creating ROLE...\n" if $verbose;
# start sqlplus
open(SQLPLUS, "| $ORACLE_HOME/bin/sqlplus \"/ as sysdba\" > /dev/null") 
  or die("can't start SQLPLUS");
print SQLPLUS <<EOF;

-- setup error handling and logging
--

WHENEVER OSERROR EXIT FAILURE ROLLBACK
-- WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
spool $logFile


CREATE ROLE lfcuser;
grant ALTER SESSION to lfcuser;
grant CREATE PROCEDURE to lfcuser;
grant CREATE SEQUENCE to lfcuser;
grant CREATE SESSION to lfcuser;
grant CREATE SNAPSHOT to lfcuser;
grant CREATE SYNONYM to lfcuser;
grant CREATE TABLE to lfcuser;
grant CREATE TRIGGER to lfcuser;
grant CREATE VIEW to lfcuser;
grant CREATE TYPE to lfcuser;
grant QUERY REWRITE to lfcuser;

EOF
close SQLPLUS;

print "Running SQLPLUS...\n" if $verbose;
# start sqlplus
open(SQLPLUS, "| $ORACLE_HOME/bin/sqlplus \"/ as sysdba\" > /dev/null") 
  or die("can't start SQLPLUS");
print SQLPLUS <<EOF;

-- setup error handling and logging
--

WHENEVER OSERROR EXIT FAILURE ROLLBACK
-- WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
spool $logFile

-- DROP USER $name CASCADE;
CREATE USER $name IDENTIFIED BY "$password"
  profile "DEFAULT"
  DEFAULT TABLESPACE "${name}_DATA"
  TEMPORARY TABLESPACE "$tempTablespace"
  QUOTA UNLIMITED ON "${name}_DATA"
  QUOTA UNLIMITED ON "${name}_IDX"
  ACCOUNT UNLOCK;

GRANT "LFCUSER" to $name;

EOF
close SQLPLUS;

if($? != 0 ) {
    die "Error while running sqlplus : see $logFile for more details";
}

unlink $logFile;

print "Done.\n" if $verbose;
exit;

#################################################################
# end of script
#################################################################
sub usage($) {
    my $error = shift @_;
    print <<EOF and die "Wrong usage of the script : $error\n";
usage : $0 --name=NAME --password=password 
           [--temp=tablespace] [--v]

Options
    --name          name       The database user will be called LFC_<NAME>
    --password      password   Password of the LFC_<NAME> account
    --temp          tablespace The name of the temp tablespace (defaults
                               to 'TEMP01')
    --v                        verbose mode
EOF
}

sub configuration() {
	print <<EOF;	
Configuration :
    ORACLE_HOME     : $ORACLE_HOME
    ORACLE_SID      : $ORACLE_SID
    Name            : $name
    Password        : $password
    Temp tablespace : $tempTablespace
EOF
}
