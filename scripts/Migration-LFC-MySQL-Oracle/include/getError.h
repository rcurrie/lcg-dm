#include "oci.h"
#include <iostream>
#include <math.h>
#include <ctime>
//#include "DBparam.h"

#define MINSTRINGLEN 50
#define ERROR_BUFFER 200
#define MAXROWS 10000
static char logfilename[100]="MigrateLFC_logfile.log";
int ShowErrors (sword status, OCIError *errhp, const char* log);
void OCIReportError(OCIError *errhp,const char* ApplicName, char* errMessg, int errnum);
void GetErrorMess(const char* ApplicName, const char* logMess,char* errMessg, int errnum);
void Format_output(int output_oraclecode, char* attribute_value, char* logmessage, char separator);
void MinStringLength(int &actual_len);
void WriteToLogFile(char* appliName,  char* statement);
void ErrorExit(const char* FctName,const char* logMess,char* ErrMess, int errnum);
int getLoginUser(char* username,int len_host,char* ErrorMess);
int getTerminalName(char* hostname,int len_host,char* ErrorMess);



