/********************************************************************************/
//Author: L.Abadie
// version:v1.0
//get connected to an Oracle DB using OCI
// all the elements in char* are separated by '\\0' unless specified
/********************************************************************************/


#include "Ora_GetMetadataInfo.h"
#include "getError.h"


#define MIGRATE_LFC_VERSION "v1.0"  



OCIError* ociError=0; /* the error handle */ 
OCIEnv* ociEnv=0; /* the environment handle */ 
OCISvcCtx* ociHdbc=0; /* the context handle */
OCIServer *mysrvhp=0; /* the server handle */
OCISession *myusrhp=0; /* user session handle */
/**************************************************************************************/


//internal fct to disconnect to DB
sword Ora_Disconnect(OCIEnv* env,OCIError* err,OCIServer* mysrvhp1,OCISession* myusrhp1, OCISvcCtx* mysvchp, int count_free, char* ErrorMessage)   
{
	char appliName[100]="Disconnect";
sword status=OCI_SUCCESS;
 sword errcode=0;

if(count_free>4)
{
	status=OCISessionEnd (mysvchp,err,myusrhp1,OCI_DEFAULT);
	//std::cout<<"OCISessionEnd and status="<<status<<std::endl;
}
if(count_free>3)
{
status=OCIServerDetach (mysrvhp1,err,OCI_DEFAULT);
//std::cout<<"OCIServerDetach and status="<<status<<std::endl;
}                      



if(count_free>4)
{
	status=OCIHandleFree(myusrhp1,OCI_HTYPE_SESSION);
	//std::cout<<"OCIHandleFree 1 and status="<<status<<std::endl;

}
if(count_free>3)
{
	status+=OCIHandleFree(mysvchp,OCI_HTYPE_SVCCTX);
		//std::cout<<"OCIHandleFree 2 and status="<<status<<std::endl;

}
if(count_free>2)
{
	status+=OCIHandleFree (err, OCI_HTYPE_ERROR);
			//std::cout<<"OCIHandleFree 3 and status="<<status<<std::endl;

}
if(count_free>1)
{
	status+=OCIHandleFree(mysrvhp1,OCI_HTYPE_SERVER);
			//std::cout<<"OCIHandleFree 4 and status="<<status<<std::endl;

}
if(count_free>0)
{
	status+=OCIHandleFree (env, OCI_HTYPE_ENV);
	//std::cout<<"OCIHandleFree 5 and status="<<status<<std::endl;

	//status+=OCITerminate ( OCI_DEFAULT );
	//std::cout<<"OCITerminate 2 and status="<<status<<std::endl;

}
if(status== OCI_SUCCESS)
{
	//std::cout << "OCI Connection successfully terminated...." << count_free << std::endl;
	//OCIReportError(err,appliName,ErrorMessage,0);
	GetErrorMess(appliName,"DISCONNECTION SUCCESSFUL",ErrorMessage,0);
	
}
else
{	
	//std::cout << "OCI Connection not successfully terminated...." << count_free << std::endl;
	//OCIReportError(err,appliName,ErrorMessage,1);
	GetErrorMess(appliName,"DISCONNECTION UNSUCCESSFUL",ErrorMessage,1);
	
}

return status;
}

//internal fct to connect to DB
sword Ora_Connect(char* server,char* usr,char* pwd,OCIEnv*& myenvhp,OCIServer*& mysrvhp1,OCISession*& myusrhp1, OCISvcCtx*& mysvchp, OCIError*& myerrhp, char* ErrorMessage)      
{
	char appliName[100]="Connect";
	sword status=0;
	sword errcode=0;
	int count_free=0;
	int rescode=0;
	int res_win=0;
	char* errmessg=(char*)malloc(ERROR_BUFFER*sizeof(char));
	if(errmessg==NULL)
	{
		GetErrorMess(appliName, "Malloc Unsuccessful",ErrorMessage,1);
		return -1;
	}
  //create an environment to handle a session:first thing to do 
  //use of the default to initiliaze the environment
  //use of OCIHandleAlloc because we want the application to support multiple connections and users so we can't use OCILogon fct
  //We initialize the session as follows

/* initialize the mode to be not threaded and default environment */
status=OCIEnvCreate(&myenvhp, OCI_DEFAULT, (dvoid *)0,0, 0, 0, (size_t) 0, (dvoid **)0);
if(status!= OCI_SUCCESS)
{ 
	count_free++; //1
	status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
	if(status==OCI_SUCCESS)
		status=-1;
	rescode= -1;
}
else
{
/* allocate a server handle */
	count_free++;
status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&mysrvhp1,OCI_HTYPE_SERVER, 0, (dvoid **) 0);
}

if(status!= OCI_SUCCESS )
{ 
	if(rescode==0)
	{
		count_free++;  //2
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
}
else
{
/* allocate an error handle */
	count_free++;
status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&myerrhp,OCI_HTYPE_ERROR, 0, (dvoid **) 0);
}

if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		count_free++;  //3
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
	
}
else
{
/* create a server context */
	count_free++;
status=OCIServerAttach (mysrvhp1, myerrhp, (text *)server,strlen(server), OCI_DEFAULT);
}

if(status!= OCI_SUCCESS)
{ 
	
	if(rescode==0)
	{
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
}
else
{
/* allocate a service handle */
status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&mysvchp,OCI_HTYPE_SVCCTX, 0, (dvoid **) 0);
}

if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		count_free++;  //4
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
		
	}
	
}
else
{
		count_free++;
		/* set the server attribute in the service context handle*/
		status=OCIAttrSet ((dvoid *)mysvchp, OCI_HTYPE_SVCCTX,(dvoid *)mysrvhp1, (ub4) 0, OCI_ATTR_SERVER, myerrhp);
}

if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
}
else
{
/* allocate a user session handle */
status=OCIHandleAlloc ((dvoid *)myenvhp, (dvoid **)&myusrhp1,OCI_HTYPE_SESSION, 0, (dvoid **) 0);
}

if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		count_free++;//5
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);

		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}

}
else
{
/* set user name attribute in user session handle */
status=OCIAttrSet ((dvoid *)myusrhp1, OCI_HTYPE_SESSION,(dvoid *)usr, (ub4)strlen(usr),OCI_ATTR_USERNAME, myerrhp);
count_free++;
}


if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
}
else
{
/* set password attribute in user session handle */
status=OCIAttrSet ((dvoid *)myusrhp1, OCI_HTYPE_SESSION,(dvoid *)pwd, (ub4)strlen(pwd),OCI_ATTR_PASSWORD, myerrhp);
}

if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
}
else
	status=OCISessionBegin(mysvchp, myerrhp, myusrhp1,OCI_CRED_RDBMS, OCI_DEFAULT);

if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
}
else
{
/* set the user session attribute in the service context handle*/
status=OCIAttrSet ( (dvoid *)mysvchp, OCI_HTYPE_SVCCTX,(dvoid *)myusrhp1, (ub4) 0, OCI_ATTR_SESSION, myerrhp);
}

if(status!= OCI_SUCCESS)
{ 
	if(rescode==0)
	{
		status=Ora_Disconnect(myenvhp,myerrhp,mysrvhp1,myusrhp1, mysvchp,count_free,errmessg);
		if(status==OCI_SUCCESS)
			status=-1;
		rescode= -1;
	}
}



if(rescode==0)
{
	OCIReportError(myerrhp,appliName,ErrorMessage,0);
	std::cout << " OCI Connection successfully established and current version of the library " << MIGRATE_LFC_VERSION << std::endl;
}
else
{
	OCIReportError(myerrhp,appliName,ErrorMessage,1);
	std::cout << "OCI Connection not successfully established ...." <<  std::endl;
}
free(errmessg);
return (rescode);
}
 
/************************************************************************/
// Connect to the Oracle DB
/***********************************************************************/
/**
       * Connect to the given Oracle DB,  returning an integer value.
       * @param server : name of the DB instance.
       * @param usr : the user name.
	   * @param pwd : the password
	   * @param ErrorMessage : error message in case of failure
       * @return 0 if it is successful
       */
int Ora_DBConnect(char* server,char* usr,char* pwd,char* ErrorMessage)
{
	int status;
	char appliName[100]="Ora_DBConnect";
	int rescode=0;
	
	status=Ora_Connect(server,usr,pwd,ociEnv,mysrvhp,myusrhp, ociHdbc, ociError,ErrorMessage);	
	if(status!=0)
	{
		ociError=0; /* the error handle */ 
		ociEnv=0; /* the environment handle */ 
		ociHdbc=0; /* the context handle */
		mysrvhp=0; /* the server handle */
		myusrhp=0; /* user session handle */
	}
	
	return status;
}
 /**
       * Disconnect from the database  returning an integer value.
	   * @param ErrorMessage : error message in case of failure
       * @return 0 if the disconnection is successful
       */

void Ora_DBDisconnect(char* ErrorMessage)
{
int status;
status=Ora_Disconnect(ociEnv,ociError,mysrvhp,myusrhp, ociHdbc,5,ErrorMessage);
ociError=0; /* the error handle */ 
ociEnv=0; /* the environment handle */ 
ociHdbc=0; /* the context handle */
mysrvhp=0; /* the server handle */
myusrhp=0; /* user session handle */
//return status;
}


/************************************************************************/
// return the information about the columns of the given table
/***********************************************************************/


/**
       * Get the information about the columns of the given table,  returning an integer value.
       * @param table_name : name of the table: it's not CASE SENSITIVE.
       * @param len_columns : length of the column_list. If the buffer is too small, we put the size needed here.
	   * @param column_list : char* buffer where the list of column is written
	   * @param  len_nb_col : length of the column_pos_list array
	   * @param column_pos_list : array which contains the position of the column
	   * @param datatype_len : length of the datatype_list buffer
	   * @param datatype_list : buffer which indicates the type of the column
	   * @param column_pos_list : array which contains the precision of the datatype of the column. We asssume that its size is the same as column_pos_list
	   * @param ErrorMessage : error message in case of failure
       * @return 0 if it is successful
       */
int Ora_GetColInfo(char* table_name,int &len_columns,char* column_list,int & len_nb_col, int* column_pos_list, int& datatype_len,char* datatype_list, int* dataprecision_list,char* ErrorMessage)
{
char appliName[100]="Ora_GetColInfo";
int i = 0;
int j=0;
int pos1=0;
int pos2=0;
int len1=0;
int pos3=0;
int pos4=0;
int pos5=0;
int pos6=0;
int rescode=0;
int free_mem=0;
ub4 numcols = 0;
ub4 collen=0;
ub2 type = 0;
sword status;
OCIStmt *stmthp;
OCIBind  *bnd1p = (OCIBind *) 0; 
OCIDefine* def[4];
sb4  columnlen=30;
sb4 datatypelen=0;
OCIParam *parmdp;
char * datatype_temp=NULL;
char column_temp[31];
int dtype_null=0;
int col_null=0;
char  buffer[10];
int precision=0;
int colID=0;
int dtypeID=0;
char logmessage[100];
int res1=0;
char* column_list_temp=NULL;
int* column_pos_list_temp=NULL;
char* datatype_list_temp=NULL;
int* dataprecision_list_temp=NULL;
int nb_of_col=1000;
char selectdevtype[1000];
status =OCIHandleAlloc (ociEnv, (void**)&stmthp, OCI_HTYPE_STMT , 0, 0);
if(status!=OCI_SUCCESS)
{	
	rescode=ShowErrors (status, ociError, "OCIStmtHAndleStmt unsuccessful");
	if(ociError!=0)
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	else
		GetErrorMess(appliName,"NOT CONNECTED TO ANY DB",ErrorMessage,1);
	return -1;
}
else
{
	
	sprintf(selectdevtype,"select lower(column_name)||'?',data_type||'?',decode(data_type,'NUMBER',nvl(data_precision,5),data_length),column_id from user_tab_columns where table_name=upper(:tabname) order by column_id");
	status=OCIStmtPrepare(stmthp, ociError, (text*) selectdevtype,(ub4) strlen(selectdevtype),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
}
//std::cout<<"selectdevtype="<<selectdevtype<<std::endl;
if(status!=OCI_SUCCESS )
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIStmtPrepare unsuccessful");
	}
}
else
	status=OCIBindByName(stmthp, &bnd1p, ociError,(text*)":tabname",-1,(dvoid*) table_name,strlen(table_name)+1, SQLT_STR, (dvoid *) 0,(ub2 *) 0, (ub2*) 0, (ub4) 0, (ub4 *) 0, OCI_DEFAULT);

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIBindByPos unsuccessful");
	}
}
else
	status=OCIStmtExecute(ociHdbc, stmthp, ociError, 0, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIStmtExecute unsuccessful");
	}
}
else
{
	/* The next two statements describe the select-list item, dept, and return its length to allocate the memory*/
	status=OCIParamGet(stmthp, OCI_HTYPE_STMT, ociError,(dvoid **) &parmdp, (ub4) 2);
}
	
if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIParamGet1 unsuccessful");
	}
}
else
	status=OCIAttrGet((dvoid*) parmdp, (ub4) OCI_DTYPE_PARAM,(dvoid*) &datatypelen, (ub4 *) 0, (ub4) OCI_ATTR_DATA_SIZE,(OCIError *) ociError);
//std::cout<<"devtypelen="<<devtypelen<<std::endl;	

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{

		rescode=ShowErrors (status, ociError, "OCIAttrGet2 unsuccessful");
	}
}
else
{
	/* Use the retrieved length of dept to allocate an output buffer, and then define the output variable. If the define call returns an error,exit the application */
	MinStringLength(datatypelen); //case it will be a null value need a buffer bigger than 5
	
	datatype_temp = (char *) realloc(datatype_temp,(datatypelen + 1)*sizeof(char));
	if(datatype_temp==NULL )
	{
		
		rescode=ShowErrors (status, ociError, "  pointer allocation unsuccessful");
		rescode=-1;
		if(datatype_temp!=NULL)
			free(datatype_temp);
		
		status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
		GetErrorMess(appliName,"REALLOC UNSUCCESSFUL",ErrorMessage,1);
		return rescode;
	}
	else
		status =OCIDefineByPos(stmthp, &def[0], ociError,1, (ub1 *) (column_temp), columnlen + 1,SQLT_STR, (dvoid *)&col_null,(ub2 *) 0,0, OCI_DEFAULT);
}

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
 
		rescode=ShowErrors (status, ociError, "OCIDefineByPos0 unsuccessful");		
	}
}
else
	status=OCIDefineByPos (stmthp, &def[1],ociError, 2,(ub1 *) (datatype_temp),datatypelen+1, SQLT_STR, (dvoid *)&dtype_null, 0, 0, OCI_DEFAULT);

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
 
		rescode=ShowErrors (status, ociError, "OCIDefineByPos0 unsuccessful");		
	}
}
else
	status=OCIDefineByPos (stmthp, &def[2],ociError, 3,&precision,sizeof(precision), SQLT_INT, 0, 0, 0, OCI_DEFAULT);

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIDefineByPos1 unsuccessful");
		
	}
}
else
	status=OCIDefineByPos (stmthp, &def[3], ociError, 4,&colID,sizeof(colID), SQLT_INT, 0, 0, 0, OCI_DEFAULT);

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIDefineByPos unsuccessful");
}
else
{
	
	
	pos2=0;
	pos6=0;
	i=0;
	column_pos_list_temp=(int*) realloc(column_list_temp,nb_of_col*sizeof(int));
	dataprecision_list_temp=(int*) realloc(dataprecision_list_temp,nb_of_col*sizeof(int));
	if(column_pos_list_temp==NULL || dataprecision_list_temp==NULL)
	{
		
		rescode=ShowErrors (status, ociError, " Malloc  unsuccessful");
		rescode=-1;
		free(datatype_temp);
		
		if(column_pos_list_temp!=NULL)
			free(column_pos_list_temp);
		if(dataprecision_list_temp!=NULL)
			free(dataprecision_list_temp);

		status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
		GetErrorMess(appliName,"REALLOC UNSUCCESSFUL",ErrorMessage,1);
		return rescode;	
	}
	//std::cout<<"before the loop"<<std::endl;
	while(status==OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO)
	{
		status =OCIStmtFetch2 (stmthp, ociError, 1, OCI_FETCH_NEXT,1, OCI_DEFAULT);
		if(status==OCI_SUCCESS)
		{
			//Format_output(dtype_null, datatype_temp, logmessage, '?');
			pos1=strcspn(datatype_temp,"?");
			datatype_temp[pos1]='\0';
			pos3=pos2;
			pos2+=pos1+1;
			//std::cout<<"datatype_temp before copy"<<datatype_temp<<std::endl;
			//Format_output(col_null, column_temp, logmessage, '?');
			pos4=strcspn(column_temp,"?");
			column_temp[pos4]='\0';
			pos5=pos6;
			pos6+=pos4+1;
			

			

			column_list_temp=(char*) realloc(column_list_temp,pos6*sizeof(char));
			datatype_list_temp=(char*) realloc(datatype_list_temp,pos2*sizeof(char));
			if(datatype_list_temp==NULL || column_list_temp==NULL)
			{
				rescode=ShowErrors (status, ociError, "  pointer allocation unsuccessful");
				rescode=-1;
				free(datatype_temp);
				free(column_pos_list_temp);
				free(dataprecision_list_temp);
				if(datatype_list_temp!=NULL)
					free(datatype_list_temp);
				if(column_list_temp!=NULL)
					free(column_list_temp);

				status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
				GetErrorMess(appliName,"REALLOC UNSUCCESSFUL",ErrorMessage,1);
				return rescode;	
			}
			else
			{
				memcpy(column_list_temp+pos5,column_temp,pos4+1);
				//std::cout<<"datatype_temp"<<datatype_temp<<std::endl;
				memcpy(datatype_list_temp+pos3,datatype_temp,pos1+1);
				//std::cout<<"colum_temp"<<column_temp<<std::endl;
				column_pos_list_temp[i]=colID;
				dataprecision_list_temp[i]=precision;
				//std::cout<<"colID"<<precision<<" and i="<<i<<std::endl;
				i++;
			}


		}
	}
	//std::cout<<"after the loop pos2="<<pos2<<" and pos6="<<pos6<<std::endl;
	if(rescode==0)
	{	
		//std::cout<<"pos2="<<pos2<<"pos6="<<pos6<<std::endl;
		if(pos2>datatype_len || pos6>len_columns || i>len_nb_col)
		{
			datatype_len=pos2;
			len_columns=pos6;
			len_nb_col=i;
			rescode= -1;	
			free(datatype_temp);
			free(column_pos_list_temp);
			free(dataprecision_list_temp);
			free(column_list_temp);
			free(datatype_list_temp);
			status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
			GetErrorMess(appliName,"BUFFER_TOO_SMALL",ErrorMessage,1);
			return rescode;	
		}
		else
		{
			if(i>0)
			{
				memcpy(datatype_list,datatype_list_temp,pos2);
				memcpy(column_list,column_list_temp,pos6);
				for(j=0;j<i;j++)
				{
					column_pos_list[j]=column_pos_list_temp[j];
					dataprecision_list[j]=dataprecision_list_temp[j];
				}
				datatype_len=pos2;
				len_columns=pos6;
				len_nb_col=i;
			}
			else
			{
					rescode=0;
					strcpy(datatype_list,"NO_ROWS_SELECTED");
					GetErrorMess(appliName,"NO_ROWS_SELECTED",ErrorMessage,1);

			}
		}
		free(datatype_temp);
		free(column_pos_list_temp);
		free(dataprecision_list_temp);
		if(column_list_temp!=NULL)
			free(column_list_temp);
		if(datatype_list_temp!=NULL)
			free(datatype_list_temp);
	}
}
status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
if(strstr(ErrorMessage,"NO_ROWS_SELECTED")==NULL)
{
	if(rescode==0)
	{
		OCIReportError(ociError,appliName,ErrorMessage,0); 
	}
	else
	{
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	}
}

return rescode;
}

/*****************************************************************************************/
/**
       * Get the list of tables returning an integer value.
	   * @param len_array : length of the tabname_list. If the buffer is too small, we put the size needed here.
	   * @param tabname_list :  char* buffer where to put the result (list of table names). each line seaprated by '\\0'
	   * @param nb_of_tab : nb of tables found.
	   * @param ErrorMessage : error message in case of failure
	   * @return 0 if it is successful
       */
int Ora_GetListOfTables(int &len_array, char* tabname_list,int & nb_of_tab,char* ErrorMessage)
{
char appliName[100]="Ora_GetListOfTables";
int i = 0;
int j=0;
int rescode=0;
ub4 numcols = 0;
ub4 collen=0;
ub2 type = 0;
int pos1=0;
int pos3=0;
int actual_len=0;
int pos2=0;
int len_tabname=30;
char tabName[31];
char* tabnames_temp=NULL;
sword status;
int nline=0;
int nb_rdtrip=1;
int prefetch_rows=2000;
OCIStmt *stmthp;
OCIDefine* def;
char selectdevID[100];
status =OCIHandleAlloc (ociEnv, (void**)&stmthp, OCI_HTYPE_STMT , 0, 0);
char tab_version[31]="schema_version";
if(status!=OCI_SUCCESS)
{	
	rescode=ShowErrors (status, ociError, "OCIStmtHAndleAlloc unsuccessful");
	if(ociError!=0)
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	else
		GetErrorMess(appliName,"NOT CONNECTED TO ANY DB",ErrorMessage,1);
	return -1;
}
else
{
	sprintf(selectdevID,"select table_name||'?' from user_tables where lower(table_name)!='%s'",tab_version);
	status=OCIStmtPrepare(stmthp, ociError, (text*) selectdevID,(ub4) strlen(selectdevID),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
	//std::cout<<"stmt"<<selectdevID<<std::endl;
}

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIBindByName unsuccessful");
}
else
	status=	OCIAttrSet (stmthp,OCI_HTYPE_STMT,&prefetch_rows,0,OCI_ATTR_PREFETCH_ROWS,ociError);
	
if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIAttrSet unsuccessful");
}
else
	status=OCIStmtExecute(ociHdbc, stmthp, ociError, 0, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);

	

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIStmtExecute unsuccessful");
}
else
{
	
	status=OCIDefineByPos (stmthp, &def, ociError, 1,(ub1*) tabName,len_tabname+1, SQLT_STR, 0, 0, 0, OCI_DEFAULT);
	
}
if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIDefineByPos unsuccessful");
}
else
{

	
	pos2=0;
	i=0;
	while(status==OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO)
	{
		status =OCIStmtFetch2(stmthp, ociError, 1, OCI_FETCH_NEXT,1,OCI_DEFAULT);
		if(status==OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO)
		{
			pos1=strcspn(tabName,"?");
			tabName[pos1]='\0';
			pos3=pos2;
			pos2+=pos1+1;
			tabnames_temp=(char*) realloc(tabnames_temp,pos2*sizeof(char));
			if(tabnames_temp==NULL)
			{
				status=-10;
				rescode=ShowErrors (status, ociError, "  pointer allocation unsuccessful");
				rescode=-1;
				
				status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
				GetErrorMess(appliName,"REALLOC UNSUCCESSFUL",ErrorMessage,1);
				return rescode;
			}
			else
			{
				memcpy(tabnames_temp+pos3,tabName,pos1+1);
				i++;
			}
		}
	}
	if(rescode==0)
	{	
		
		if(pos2>len_array)
		{
			len_array=pos2;
			free(tabnames_temp);
			rescode=-1;
			
			status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
			GetErrorMess(appliName,"BUFFER_TOO_SMALL",ErrorMessage,1);
			return rescode;
		}
		else
		{
			if(i>0)
			{
				memcpy(tabname_list,tabnames_temp,pos2);
				len_array=pos2;
				nb_of_tab=i;
			}
			else
			{
					rescode=0;
					strcpy(tabname_list,"NO_ROWS_SELECTED");
					GetErrorMess(appliName,"NO_ROWS_SELECTED",ErrorMessage,1);
			}
		}
		free(tabnames_temp);
		
	}
}
status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
if(strstr(ErrorMessage,"NO_ROWS_SELECTED")==NULL)
{
if(rescode==0)
{
	OCIReportError(ociError,appliName,ErrorMessage,0); 
}
else
{
	OCIReportError(ociError,appliName,ErrorMessage,1); 
}
}
return rescode;
}



/***************************************************************************************/
/**
       * Get the foreign key of a given table,  returning an integer value.
       * @param table_name : name of the table
       * @param len_cst_list : length of the cstname_list. If the buffer is too small, we put the size needed here.
	   * @param cstname_list :char* buffer where to put the result
	   * @param ErrorMessage : error message in case of failure
       * @return 0 if it is successful
       */

int Ora_GetFkConstraints(char* table_name,int& len_cst_list,char* cstname_list,char* ErrorMessage)
{
char appliName[100]="Ora_GetFkConstraints";
int i = 0;
int j=0;
int pos1=0;
int pos2=0;
int pos3=0;
int pos4=0;
int pos5=0;
int node=0;
int cstname_len=30;
int status1=0;
int rescode=0;
sword status;

OCIStmt *stmthp;
OCIBind  *bnd1p = (OCIBind *) 0; 
OCIDefine* def;

OCIParam *parmdp;
int len=0;
char buffer[20];
char * cstname_list_temp=NULL;
char cstname[31];
char selectdev[1000];

int cst_null=0;
int res1=0;
char logmessage[100];
status =OCIHandleAlloc (ociEnv, (void**)&stmthp, OCI_HTYPE_STMT , 0, 0);
if(status!=OCI_SUCCESS)
{
	rescode=ShowErrors (status, ociError, "OCIStmtHAndleStmt unsuccessful");
	if(ociError!=0)
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	else
		GetErrorMess(appliName,"NOT CONNECTED TO ANY DB",ErrorMessage,1);
	return -1;
}
else
{
	sprintf(selectdev," select constraint_name||'?' from user_constraints where table_name=:tabname and constraint_type='R'");
	status=OCIStmtPrepare(stmthp, ociError, (text*) selectdev,(ub4) strlen(selectdev),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
}

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIStmtPrepare unsuccessful");
}
else
	status=OCIBindByName(stmthp, &bnd1p, ociError,(text*)":tabname",-1,(dvoid*) table_name,strlen(table_name)+1, SQLT_STR, (dvoid *) 0,(ub2 *) 0, (ub2*) 0, (ub4) 0, (ub4 *) 0, OCI_DEFAULT);
	
if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIBindByPos unsuccessful");
}
else	
	status=OCIStmtExecute(ociHdbc, stmthp, ociError, 0, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);
	



if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIAttrGet4  unsuccessful");
}
else
{
	

	
		status =OCIDefineByPos(stmthp, &def, ociError,1, (ub1 *) (cstname), cstname_len + 1,SQLT_STR, (dvoid *) &cst_null,(ub2 *) 0,0, OCI_DEFAULT);
}


if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIDefineByPos unsuccessful");
}
else
{

	
	pos2=0;
	i=0;
	while(status==OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO)
	{
		status =OCIStmtFetch2(stmthp, ociError, 1, OCI_FETCH_NEXT,1,OCI_DEFAULT);
		if(status==OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO)
		{
			pos1=strcspn(cstname,"?");
			cstname[pos1]='\0';
			pos3=pos2;
			pos2+=pos1+1;
			cstname_list_temp=(char*) realloc(cstname_list_temp,pos2*sizeof(char));
			if(cstname_list_temp==NULL)
			{
				status=-10;
				rescode=ShowErrors (status, ociError, "  pointer allocation unsuccessful");
				rescode=-1;
				
				status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
				GetErrorMess(appliName,"REALLOC UNSUCCESSFUL",ErrorMessage,1);
				return rescode;
			}
			else
			{
				memcpy(cstname_list_temp+pos3,cstname,pos1+1);
				i++;
			}
		}
	}
	if(rescode==0)
	{	
		
		if(pos2>len_cst_list)
		{
			len_cst_list=pos2;
			free(cstname_list_temp);
			rescode=-1;
			
			status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
			GetErrorMess(appliName,"BUFFER_TOO_SMALL",ErrorMessage,1);
			return rescode;
		}
		else
		{
			if(i>0)
			{
				memcpy(cstname_list,cstname_list_temp,pos2);
				len_cst_list=pos2;
			}
			else
			{
					rescode=0;
					strcpy(cstname_list,"NO_ROWS_SELECTED");
					GetErrorMess(appliName,"NO_ROWS_SELECTED",ErrorMessage,1);
			}
		}
		free(cstname_list_temp);
		
	}
}
status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
if(strstr(ErrorMessage,"NO_ROWS_SELECTED")==NULL)
{
if(rescode==0)
{
	OCIReportError(ociError,appliName,ErrorMessage,0); 
}
else
{
	OCIReportError(ociError,appliName,ErrorMessage,1); 
}
}
return rescode;
}

/***************************************************************************************/
/**
       * Disable or enable a given fk constraint,  returning an integer value.
       * @param table_name : name of the table
       * @param len_cst_list : length of the cstname_list. If the buffer is too small, we put the size needed here.
	   * @param cstname_list :char* buffer which contains the name of the constraint
	   * @param enable_cst : 1 to enable the cst, 0 to disable it
	   * @param ErrorMessage : error message in case of failure
       * @return 0 if it is successful
       */

int Ora_DisEnableFkConstraint(char* table_name,int len_cst_list,char* cstname_list,int enable_cst,char* ErrorMessage)
{
char appliName[100]="Ora_DisEnableFkConstraint";
int i = 0;
int j=0;
int pos1=0;
int pos2=0;
int pos3=0;
int pos4=0;
int pos5=0;
int node=0;
int cstname_len=30;
int status1=0;
int rescode=0;
sword status;

OCIStmt *stmthp;

int len=0;
char buffer[20];
char * cstname_list_temp=NULL;
char cstname[31];
char selectdev[1000];

int cst_null=0;
int res1=0;
char logmessage[100];
status =OCIHandleAlloc (ociEnv, (void**)&stmthp, OCI_HTYPE_STMT , 0, 0);
if(status!=OCI_SUCCESS)
{
	rescode=ShowErrors (status, ociError, "OCIStmtHAndleStmt unsuccessful");
	if(ociError!=0)
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	else
		GetErrorMess(appliName,"NOT CONNECTED TO ANY DB",ErrorMessage,1);
	return -1;
}
pos2=0;

while(pos2<len_cst_list)
{
	pos1=strlen(cstname_list+pos2)+1;
	memcpy(cstname,cstname_list+pos2, pos1);

	if (enable_cst==0)
		sprintf(selectdev," alter table %s modify constraint %s disable",table_name,cstname);
	else
	{
		if (enable_cst==1)
			sprintf(selectdev," alter table %s modify constraint %s enable",table_name,cstname);
		else
		{
			rescode=ShowErrors (status, ociError, "  pointer allocation unsuccessful");
			rescode=-1;
			status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
			GetErrorMess(appliName,"enable_cst is either 1 or 0",ErrorMessage,1);
			return rescode;
		}
	}
	status=OCIStmtPrepare(stmthp, ociError, (text*) selectdev,(ub4) strlen(selectdev),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS)
	{
		if(rescode==0)
			rescode=ShowErrors (status, ociError, "OCIBindByPos unsuccessful");
	}
	else	
		status=OCIStmtExecute(ociHdbc, stmthp, ociError, 1, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);

	pos2=pos1;
}


if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIExecute  unsuccessful");
}

status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
if(strstr(ErrorMessage,"NO_ROWS_SELECTED")==NULL)
{
if(rescode==0)
{
	OCIReportError(ociError,appliName,ErrorMessage,0); 
}
else
{
	OCIReportError(ociError,appliName,ErrorMessage,1); 
}
}
return rescode;
}

/*****************************************************************************************/
/**
       * Get the nb of rows of the given table returning an integer value.
	   * @param table_name : name of the table.
	   * @param nb_of_rows :  nb of rows. if it is <0, means there is an error
	   * @param ErrorMessage : error message in case of failure
	   * @return 0 if it is successful
       */

int Ora_GetNbOfRows(char*table_name, int& nb_of_rows,char* ErrorMessage)
{
char appliName[100]="Ora_GetNbOfRows";
int i = 0;
int j=0;
int rescode=0;
ub4 numcols = 0;
ub4 collen=0;
ub2 type = 0;
int pos1=0;
int nb_of_rows_temp=0;
int actual_len=0;
int pos2=0;
int len_tabname=30;
char tabName[31];
char* tabnames_temp=NULL;
sword status;
int nline=0;
int nb_rdtrip=1;
int prefetch_rows=2000;
OCIStmt *stmthp;
OCIDefine* def;
char selectdevID[100];
nb_of_rows=-1;
status =OCIHandleAlloc (ociEnv, (void**)&stmthp, OCI_HTYPE_STMT , 0, 0);

if(status!=OCI_SUCCESS)
{	
	rescode=ShowErrors (status, ociError, "OCIStmtHAndleAlloc unsuccessful");
	if(ociError!=0)
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	else
		GetErrorMess(appliName,"NOT CONNECTED TO ANY DB",ErrorMessage,1);
	return -1;
}
else
{
	sprintf(selectdevID,"select count(*) from %s",table_name);
	status=OCIStmtPrepare(stmthp, ociError, (text*) selectdevID,(ub4) strlen(selectdevID),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
	//std::cout<<"stmt"<<selectdevID<<std::endl;
}

	
if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIAttrSet unsuccessful");
}
else
	status=OCIStmtExecute(ociHdbc, stmthp, ociError, 0, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);

	

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIStmtExecute unsuccessful");
}
else
{
	
	status=OCIDefineByPos (stmthp, &def, ociError, 1,(ub1*)&nb_of_rows_temp,sizeof(int), SQLT_INT, 0, 0, 0, OCI_DEFAULT);
	
}
if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIDefineByPos unsuccessful");
}
else
{

	status =OCIStmtFetch2(stmthp, ociError, 1, OCI_FETCH_NEXT,1,OCI_DEFAULT);
	if(status==OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO)
	{
			nb_of_rows=nb_of_rows_temp;
	}
	
	
	
}
status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);

if(rescode==0)
{
	OCIReportError(ociError,appliName,ErrorMessage,0); 
}
else
{
	OCIReportError(ociError,appliName,ErrorMessage,1); 
}

return rescode;
}
/***************************************************************************************/
/**
       * Get the object type of a given object name,  returning an integer value.
       * @param object_name : name of the object
       * @param object_type_len : length of the object_type buffer. If the buffer is too small, we put the size needed here.
	   * @param object_type :char* buffer which will contains the object type
	   * @param ErrorMessage : error message in case of failure
       * @return 0 if it is successful
       */
int Ora_GetObjectType(char* object_name,int & object_type_len, char* object_type,char* ErrorMessage)
{
char appliName[100]="Ora_GetObjectType";
int i = 0;
int j=0;
int pos1=0;
int pos2=0;
int len1=0;
int pos3=0;
int rescode=0;
int objtype_null=0;
sword status;
OCIStmt *stmthp;
OCIBind  *bnd1p = (OCIBind *) 0; 
OCIDefine* def;
sb4  objtypelen=30;
char object_type_temp[31];

char  buffer[10];

char logmessage[100];
int res1=0;
if(object_name==NULL)
{
	GetErrorMess(appliName," object name MUST be given",ErrorMessage,1);
	return -1;
}
status =OCIHandleAlloc (ociEnv, (void**)&stmthp, OCI_HTYPE_STMT , 0, 0);
if(status!=OCI_SUCCESS)
{	
	rescode=ShowErrors (status, ociError, "OCIStmtHAndleStmt unsuccessful");
	if(ociError!=0)
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	else
		GetErrorMess(appliName,"NOT CONNECTED TO ANY DB",ErrorMessage,1);
	return -1;
}
else
{
	char selectdevtype[1000];
	sprintf(selectdevtype,"select object_type||'?' from user_objects where object_name=upper(:obj_name) ");
	status=OCIStmtPrepare(stmthp, ociError, (text*) selectdevtype,(ub4) strlen(selectdevtype),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
}

if(status!=OCI_SUCCESS )
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIStmtPrepare unsuccessful");
	}
}
else
	status=OCIBindByName(stmthp, &bnd1p, ociError,(text*)":obj_name",-1,(dvoid*) object_name,strlen(object_name)+1, SQLT_STR, (dvoid *) 0,(ub2 *) 0, (ub2*) 0, (ub4) 0, (ub4 *) 0, OCI_DEFAULT);

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIBindByPos unsuccessful");
	}
}
else
	status=OCIStmtExecute(ociHdbc, stmthp, ociError, 0, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);


if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{

		rescode=ShowErrors (status, ociError, "OCIAttrGet2 unsuccessful");
	}
}
else
{
	
		status =OCIDefineByPos(stmthp, &def, ociError,1, (ub1 *) (object_type_temp), objtypelen + 1,SQLT_STR, (dvoid *)&objtype_null,(ub2 *) 0,0, OCI_DEFAULT);
}


if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIDefineByPos4 unsuccessful");
	}
}
else
	status =OCIStmtFetch2(stmthp,ociError, 1, OCI_FETCH_NEXT,1, OCI_DEFAULT);

if (status == OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO) 
{	
	Format_output(objtype_null,object_type_temp, logmessage,'?');

	//case where value has been truncated
		pos1=strcspn(object_type_temp,"?");
	
		object_type_temp[pos1]='\0';
		
		strcpy(object_type,object_type_temp);
		

} 
else 
{
	if(rescode==0)
	{
		rescode=-1;
		strcpy(object_type,"NO_ROWS_SELECTED");
		GetErrorMess(appliName, "NO_ROWS_SELECTED",ErrorMessage,1);
	}
}
status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
rescode+=status;
if(strstr(ErrorMessage,"NO_ROWS_SELECTED")==NULL)
{
	if(rescode==0)
	{
		OCIReportError(ociError,appliName,ErrorMessage,0); 
	}
	else
	{
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	}
}
return rescode;
}

/***************************************************************************************/
/**
       * Set the last value of a sequence to a given value,  returning an integer value.
       * @param new_value : new value of the sequence to be set
       * @param sequence_name : name of the sequence.
	   * @param ErrorMessage : error message in case of failure
       * @return 0 if it is successful
       */
int Ora_UpdateSequence(int new_value, char* sequence_name, char* ErrorMessage)
{
char appliName[100]="Ora_UpdateSequence";
int i = 0;
int j=0;
int inc_by=0;
int last_nb=0;
int minval=0;
int status1=0;
int rescode=0;
sword status;
OCIDefine* def[3];
int next_val=0;
OCIStmt *stmthp;
OCIBind  *bnd1p = (OCIBind *) 0; 

int len=0;
char selectdev[1000];

int cst_null=0;
int res1=0;
char logmessage[100];
status =OCIHandleAlloc (ociEnv, (void**)&stmthp, OCI_HTYPE_STMT , 0, 0);
if(status!=OCI_SUCCESS)
{
	rescode=ShowErrors (status, ociError, "OCIStmtHAndleStmt unsuccessful");
	if(ociError!=0)
		OCIReportError(ociError,appliName,ErrorMessage,1); 
	else
		GetErrorMess(appliName,"NOT CONNECTED TO ANY DB",ErrorMessage,1);
	return -1;
}


sprintf(selectdev," select min_value,  last_number,increment_by from user_sequences where sequence_name=upper('%s')",sequence_name);
status=OCIStmtPrepare(stmthp, ociError, (text*) selectdev,(ub4) strlen(selectdev),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIBindByPos unsuccessful");
}
else	
	status=OCIStmtExecute(ociHdbc, stmthp, ociError, 0, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);


if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{

		rescode=ShowErrors (status, ociError, "OCIExecute unsuccessful");
	}
}
else
{
	
		status =OCIDefineByPos(stmthp, &def[0], ociError,1, (ub1 *) &minval,sizeof(int),SQLT_INT, (dvoid *) 0,(ub2 *) 0,0, OCI_DEFAULT);
}

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{

		rescode=ShowErrors (status, ociError, "OCIDefine unsuccessful");
	}
}
else
{
	
		status =OCIDefineByPos(stmthp, &def[1], ociError,2, (ub1 *) &last_nb, sizeof(int),SQLT_INT, (dvoid *) 0,(ub2 *) 0,0, OCI_DEFAULT);
}

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{

		rescode=ShowErrors (status, ociError, "OCIDefine unsuccessful");
	}
}
else
{
	
		status =OCIDefineByPos(stmthp, &def[2], ociError,3, (ub1 *) &inc_by, sizeof(int),SQLT_INT, (dvoid *) 0,(ub2 *) 0,0, OCI_DEFAULT);
}

if(status!=OCI_SUCCESS)
{
	if(rescode==0)
	{
		rescode=ShowErrors (status, ociError, "OCIDefineByPos4 unsuccessful");
	}
}
else
	status =OCIStmtFetch2(stmthp,ociError, 1, OCI_FETCH_NEXT,1, OCI_DEFAULT);

if (status == OCI_SUCCESS || status== OCI_SUCCESS_WITH_INFO) 
{
	std::cout<<"minval="<<minval<<std::endl;
	sprintf(selectdev," begin execute immediate 'alter sequence %s increment by %d'; select %s.nextval into :nextval from dual; execute immediate 'alter sequence %s increment by %d'; end;",sequence_name, new_value,sequence_name,sequence_name,inc_by );
	status=OCIStmtPrepare(stmthp, ociError, (text*) selectdev,(ub4) strlen(selectdev),(ub4) OCI_NTV_SYNTAX, (ub4) OCI_DEFAULT);
	if(status!=OCI_SUCCESS )
	{
		if(rescode==0)
		{
			rescode=ShowErrors (status, ociError, "OCIStmtPrepare unsuccessful");
		}
	}
	else
		status=OCIBindByName(stmthp, &bnd1p, ociError,(text*)":nextval",-1,(dvoid*) &next_val,sizeof(int), SQLT_INT, (dvoid *) 0,(ub2 *) 0, (ub2*) 0, (ub4) 0, (ub4 *) 0, OCI_DEFAULT);

	if(status!=OCI_SUCCESS)
	{
		if(rescode==0)
			rescode=ShowErrors (status, ociError, "OCIBindByPos unsuccessful");
	}
	else	
		status=OCIStmtExecute(ociHdbc, stmthp, ociError, 1, 0,(OCISnapshot *) 0, (OCISnapshot *) 0, OCI_DEFAULT);
	if(status!=OCI_SUCCESS)
	{
		if(rescode==0)
		{
		rescode=ShowErrors (status, ociError, "OCIExecute 2 unsuccessful");
		}
	}
}
else
{
	if(rescode==0)
	{
		rescode=-1;
		
		GetErrorMess(appliName, "NO_ROWS_SELECTED",ErrorMessage,1);
	}

}



if(status!=OCI_SUCCESS)
{
	if(rescode==0)
		rescode=ShowErrors (status, ociError, "OCIExecute  unsuccessful");
}

status =OCIHandleFree (stmthp,OCI_HTYPE_STMT);
if(strstr(ErrorMessage,"NO_ROWS_SELECTED")==NULL)
{
if(rescode==0)
{
	OCIReportError(ociError,appliName,ErrorMessage,0); 
}
else
{
	OCIReportError(ociError,appliName,ErrorMessage,1); 
}
}
return rescode;

}
