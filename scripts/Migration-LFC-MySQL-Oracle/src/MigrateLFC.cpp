/********************************************************************************/
// Author: L.Abadie
// version:v1.0
// contains the main function
/********************************************************************************/
#include "MigrationStmt.h"


 extern char _ora_db_name[100];
 extern char _ora_user_name[100];
 extern char _ora_pwd[100];

extern char _mysql_db_name[100];
 extern char _mysql_user_name[100];
 extern char _mysql_pwd[100];
 extern char _mysql_host_name[100];
 extern int _mysql_port_nb;
 extern char _mysql_socket_path[500];

 extern char _create_ora_schema_path[500];
 extern char _drop_ora_schema_path[500];
 extern MYSQL* conn_handler;


int main()
{

char config_file[20];
int status=0;
int flags=0;
int rescode=0;
int rescode1=0;
char ErrMess[1000];
char ErrMess_bis[1000];
int i=0;
char cmd_line[1000];
char* mysql_tab_list;
char* ora_tab_list;
int* nb_of_elt_per_tab;
int table_list_len=3000;
int table_list_len1=table_list_len;
char appliName[100];
int nb_of_tab=0;

//get info about the conn for oracle and mysql
strcpy(config_file,"config_file.dat");
rescode=GetParamInfo(config_file, ErrMess);
if(rescode!=0)
{
	strcpy(appliName,"GetParamInfo");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess);

	std::cout<<ErrMess_bis<<std::endl;
	return -1;
}
else
	WriteToLogFile(appliName,  ErrMess);

//execute the db schema in Oracle
rescode=GenerateShellScript("load_data.sh", 2,ErrMess);
if(rescode==0)
{
	
	if(system("sh load_data.sh")!=0)
	{
		strcpy(appliName,"main");
		GetErrorMess(appliName,"Error when creating the oracle schema",ErrMess_bis,1);
		WriteToLogFile(appliName,  "Error when creating the oracle schema");
		//std::cout<<ErrMess_bis<<std::endl;
		perror("pb when creating oracle schema");
		return -1;
	}	
}
else
{
	strcpy(appliName,"GenerateShellScript");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess);
	//std::cout<<ErrMess_bis<<std::endl;
	return -1;
}

WriteToLogFile("Main",  "The oracle schema has been created");


rescode1=mysql_DBConnect(_mysql_host_name,_mysql_user_name,_mysql_pwd, _mysql_db_name,_mysql_port_nb, _mysql_socket_path, flags, ErrMess);
rescode=Ora_DBConnect(_ora_db_name,_ora_user_name,_ora_pwd, ErrMess_bis);

if(rescode1!=0 || rescode!=0)
{
	if(rescode1!=0)
	{
		strcpy(appliName,"mysql_DBConnect");
		GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
		WriteToLogFile(appliName,  ErrMess);
		//std::cout<<ErrMess_bis<<std::endl;
		if(rescode==0)
		{
			Ora_DBDisconnect(ErrMess);
			WriteToLogFile("Ora_DBDisconnect",  ErrMess);
		}
	}
	if(rescode!=0)
	{
		strcpy(appliName,"Ora_DBConnect");
		GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
		//std::cout<<ErrMess_bis<<std::endl;
		WriteToLogFile(appliName,  ErrMess);
		if(rescode1!=0)
		{
			mysql_disconnect();
			WriteToLogFile("mysql_disconnect", "NO_ERROR");
		}
	}
	rescode=GenerateShellScript("load_data.sh", 3,ErrMess);
	if(rescode==0)
	{
		
		if(system("sh load_data.sh")!=0)
		{
		strcpy(appliName,"main");
		WriteToLogFile(appliName, "Error when dropping the oracle schema");
		GetErrorMess(appliName,"Error when dropping the oracle schema",ErrMess_bis,1);
		//std::cout<<ErrMess_bis<<std::endl;
		perror("error when dropping the oracle schema");
		return -1;
		}
		
	}
	else
	{
		strcpy(appliName,"GenerateShellScript");
		GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
		WriteToLogFile(appliName, ErrMess);
		//std::cout<<ErrMess_bis<<std::endl;
		return -1;
	}
	WriteToLogFile("Main",  "The oracle schema has been dropped");

	
}
	WriteToLogFile("main", "Connected to Oracle and MySQL server successfully");
	mysql_tab_list=(char*)malloc(table_list_len*sizeof(char));
	ora_tab_list=(char*)malloc(table_list_len*sizeof(char));
	if(mysql_tab_list==NULL || ora_tab_list==NULL)
	{
		
		WriteToLogFile("main", "Malloc unsuccessful for mysql_tab_list and/or ora_tab_list");
		if(mysql_tab_list!=NULL)
			free(mysql_tab_list);
		if(ora_tab_list!=NULL)
			free(ora_tab_list);
		strcpy(appliName,"main");
		GetErrorMess(appliName,"Malloc unsuccessful",ErrMess_bis,1);
		
		//std::cout<<ErrMess_bis<<std::endl;
		mysql_disconnect();
		WriteToLogFile("main", "Disconnected from MySQL");
		Ora_DBDisconnect(ErrMess);
		WriteToLogFile("main", "Disconnected from Oracle");
		rescode=GenerateShellScript("load_data.sh", 3,ErrMess);
		if(rescode==0)
		{
			
			if(system("sh load_data.sh")!=0)
			{
				strcpy(appliName,"main");
				GetErrorMess(appliName,"Error when dropping the oracle schema",ErrMess_bis,1);
				//std::cout<<ErrMess_bis<<std::endl;
				WriteToLogFile("main", "Error when dropping the oracle schema");
				perror("error when dropping the oracle schema");
			}

		
		}
		else
		{
			strcpy(appliName,"GenerateShellScript");
			GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
			WriteToLogFile("main", ErrMess);
			//std::cout<<ErrMess_bis<<std::endl;
			return -1;
		}	
		WriteToLogFile("main", "The oracle has been successfully dropped");
		return -1;
	}
	WriteToLogFile("main", "Successful malloc");
	rescode=mysql_GetTableList(conn_handler,_mysql_db_name, table_list_len, mysql_tab_list,  ErrMess);
	if(rescode!=0)
	{
		strcpy(appliName,"mysql_GetTableList");
		GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
		WriteToLogFile("main", ErrMess);
		//std::cout<<ErrMess_bis<<std::endl;
		free(mysql_tab_list);
		free(ora_tab_list);
		mysql_disconnect();
		WriteToLogFile("main", "Disconnected from MySQL");
		Ora_DBDisconnect(ErrMess);
		WriteToLogFile("main", "Disconnected from Oracle");
		rescode=GenerateShellScript("load_data.sh", 3,ErrMess);
		if(rescode==0)
		{
			
			if(system("sh load_data.sh")!=0)
			{
			strcpy(appliName,"main");
			GetErrorMess(appliName,"Error when dropping the oracle schema",ErrMess_bis,1);
			WriteToLogFile("main", "Error when dropping the oracle schema");
			//std::cout<<ErrMess_bis<<std::endl;
			perror("error when dropping the oracle schema");
			}
		
		}
		else
		{
			strcpy(appliName,"GenerateShellScript");
			GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
			WriteToLogFile("main", ErrMess);
			//std::cout<<ErrMess_bis<<std::endl;
			return -1;
		}		
		WriteToLogFile("main", "Oracle schema has been successfully dropped");
		return -1;
	}
	//std::cout<<"mysql_tab_list="<<mysql_tab_list<<std::endl;
	
	//std::cout<<"No error until now"<<std::endl;
	WriteToLogFile("main", ErrMess);
	rescode=Ora_GetListOfTables(table_list_len1, ora_tab_list,nb_of_tab,ErrMess);
	if(rescode!=0)
	{
		strcpy(appliName,"Ora_GetListOfTables");
		GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
		WriteToLogFile("main", ErrMess);
		//std::cout<<ErrMess_bis<<std::endl;
		free(mysql_tab_list);
		free(ora_tab_list);
		mysql_disconnect();
		WriteToLogFile("main", "Disconnected from MySQL server");
		Ora_DBDisconnect(ErrMess);
		WriteToLogFile("main", "Disconnected from Oracle server");
		rescode=GenerateShellScript("load_data.sh", 3,ErrMess);
		if(rescode==0)
		{
			
			if(system("sh load_data.sh")!=0)
			{
			strcpy(appliName,"main");
			GetErrorMess(appliName,"Error when dropping the oracle schema",ErrMess_bis,1);
			//std::cout<<ErrMess_bis<<std::endl;
			WriteToLogFile("main", "Error when dropping the oracle schema");
			perror("error when dropping the oracle schema");
			}
		
		}
		else
		{
			strcpy(appliName,"GenerateShellScript");
			GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
			WriteToLogFile("main", ErrMess);
			//std::cout<<ErrMess_bis<<std::endl;
			return -1;
		}		
		WriteToLogFile("main","Oracle schema has been dropped successfully");
		return -1;
	}
	//std::cout<<"ora_tab_list="<<ora_tab_list<<std::endl;
	WriteToLogFile("main",ErrMess);
	nb_of_elt_per_tab=(int*)malloc(nb_of_tab*sizeof(int));
	if(nb_of_elt_per_tab==NULL)
	{
		strcpy(appliName,"main");
		GetErrorMess(appliName,"Malloc unsuccessful",ErrMess_bis,1);
		WriteToLogFile("main","Malloc unsuccessful for nb_of_elt_per_tab");
		//std::cout<<ErrMess_bis<<std::endl;
		free(mysql_tab_list);
		free(ora_tab_list);
		mysql_disconnect();
		WriteToLogFile("main","Disconnected from MySQL");
		Ora_DBDisconnect(ErrMess);
		WriteToLogFile("main","Disconnected from Oracle");
		rescode=GenerateShellScript("load_data.sh", 3,ErrMess);
		if(rescode==0)
		{
			if(system("sh load_data.sh")!=0)
			{
			strcpy(appliName,"main");
			GetErrorMess(appliName,"Error when dropping the oracle schema",ErrMess_bis,1);
			WriteToLogFile("main","Error when dropping the oracle schema");
			//std::cout<<ErrMess_bis<<std::endl;
			perror("error when dropping the oracle schema");
			}
		
		}
		else
		{
			strcpy(appliName,"GenerateShellScript");
			GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
			WriteToLogFile("main",ErrMess);
			//std::cout<<ErrMess_bis<<std::endl;
			return -1;
		}		
		WriteToLogFile("main","Oracle schema has been successfully dropped");
		return -1;

	}
	else
	{
		for(i=0;i<nb_of_tab;i++)
		{
			nb_of_elt_per_tab[i]=0;
		}

	}
	WriteToLogFile("main","Successful malloc for nb_of_elt_per_tab");
	rescode=CheckSchemasCompatibility(conn_handler,mysql_tab_list,table_list_len, ora_tab_list,table_list_len1,nb_of_tab,nb_of_elt_per_tab, ErrMess);
	if(rescode!=0)
	{
		WriteToLogFile("main",ErrMess);
		strcpy(appliName,"CheckSchemasCompatibility");
		GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
		//std::cout<<ErrMess_bis<<std::endl;
		free(mysql_tab_list);
		free(ora_tab_list);
		free(nb_of_elt_per_tab);
		Ora_DBDisconnect(ErrMess);
		WriteToLogFile("main","Disconnected from Oracle");
		
		mysql_disconnect();
		WriteToLogFile("main","Disconnected from Oracle");
		//std::cout<<"after disconnecting myslq"<<std::endl;
		rescode=GenerateShellScript("load_data.sh", 4,ErrMess);
		if(rescode==0)
		{
			
			if(system("sh load_data.sh")!=0)
			{
			strcpy(appliName,"main");
			WriteToLogFile("main","Error when dropping the oracle schema");
			GetErrorMess(appliName,"Error when dropping the oracle schema",ErrMess_bis,1);
			//std::cout<<ErrMess_bis<<std::endl;
			perror("error when dropping the oracle schema");
			}
		
		}
		else
		{
			strcpy(appliName,"GenerateShellScript");
			GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
			WriteToLogFile("main",ErrMess);
			//std::cout<<ErrMess_bis<<std::endl;
			return -1;
		}	
		WriteToLogFile("main","Oracle schema successfully dropped");
		return -1;

	}

	WriteToLogFile("main",ErrMess);		
	rescode=GenerateShellScript("load_data.sh",1,ErrMess);
	//need to disable the constraint
	if(rescode==0)
	{
		WriteToLogFile("main",ErrMess);	
		if(system("sh load_data.sh")!=0)
		{
			strcpy(appliName,"Loading_GenerateShellScript");
			GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
			WriteToLogFile("main","could not load data into oracle see log files for errors");		
			//std::cout<<ErrMess_bis<<std::endl;
			free(mysql_tab_list);
			free(ora_tab_list);
			free(nb_of_elt_per_tab);
			mysql_disconnect();
			WriteToLogFile("main","disconnected from mySQL");
			Ora_DBDisconnect(ErrMess);
			WriteToLogFile("main","disconnected from Oracle");
			rescode=GenerateShellScript("load_data.sh", 4,ErrMess);
			if(rescode==0)
			{
				if(system("sh load_data.sh")!=0)
				{
				strcpy(appliName,"main");
				GetErrorMess(appliName,"Error when dropping the oracle schema",ErrMess_bis,1);
				WriteToLogFile("main","Error when dropping the oracle schema");
				//std::cout<<ErrMess_bis<<std::endl;
				perror("error when dropping the oracle schema");
				}
		
			}
			else
			{
				strcpy(appliName,"GenerateShellScript");
				GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
				WriteToLogFile("main",ErrMess);
				//std::cout<<ErrMess_bis<<std::endl;
				return -1;
			}	
			WriteToLogFile("main","Oracle schema has been dropped successfully");
			return -1;
		}
		WriteToLogFile("main","Data loaded into Oracle see log files if any rows dropped");
		rescode=CheckImportData(conn_handler,ora_tab_list,table_list_len1,nb_of_elt_per_tab, ErrMess);
		if(rescode!=0)
		{
			WriteToLogFile("main",ErrMess);
			strcpy(appliName,"CheckImportData");
			GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
			//std::cout<<ErrMess_bis<<std::endl;
			free(mysql_tab_list);
			free(ora_tab_list);
			free(nb_of_elt_per_tab);
			mysql_disconnect();
			WriteToLogFile("main","Disconnect from MySQL");
			//std::cout<<"before Disconnect from Oracle"<<std::endl;
			Ora_DBDisconnect(ErrMess);
			//std::cout<<"Disconnect from Oracle"<<std::endl;
			WriteToLogFile("main","Disconnect from Oracle");
			rescode=GenerateShellScript("load_data.sh", 4,ErrMess);
			if(rescode==0)
			{
				WriteToLogFile("main",ErrMess);
				if(system("sh load_data.sh")!=0)
				{
				strcpy(appliName,"main");
				GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
				WriteToLogFile("main","error when dropping the oracle schema");
				//std::cout<<ErrMess_bis<<std::endl;
				perror("error when dropping the oracle schema");
				}
		
			}
			else
			{
				strcpy(appliName,"GenerateShellScript");
				WriteToLogFile("main",ErrMess);
				GetErrorMess(appliName,"Error when creating the shell script to drop the oracle schema",ErrMess_bis,1);
				//std::cout<<ErrMess_bis<<std::endl;
				return -1;
			}		
		
			WriteToLogFile("main","Oracle schema has been dropped successfully");
			return -1;
		}
		//std::cout<<"it works!!!!!!!!"<<std::endl;
		WriteToLogFile("main",ErrMess);
		free(mysql_tab_list);
		free(ora_tab_list);
		free(nb_of_elt_per_tab);
		mysql_disconnect();
		WriteToLogFile("main","Disconnected from MySQL");
		Ora_DBDisconnect(ErrMess);
		WriteToLogFile("main","Disconnected from Oracle");
		WriteToLogFile("main","The migration of the LFC data has been successful");
		return 0;

							
	}
	else
	{
		WriteToLogFile("main",ErrMess);	
		free(mysql_tab_list);
		free(ora_tab_list);
		free(nb_of_elt_per_tab);
		mysql_disconnect();
		WriteToLogFile("main","Disconnected from MySQL");
		Ora_DBDisconnect(ErrMess);
		WriteToLogFile("main","Disconnected from Oracle");
		return -1;
	}
    
 



}