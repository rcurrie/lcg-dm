/********************************************************************************/
// Author: L.Abadie
// version:v1.0
/********************************************************************************/

#include "MigrationStmt.h"

#ifndef stricmp
	#include <ctype.h>

	int stricmp(const char *s1, const char *s2)
	{
	    while(toupper(*s1++) == toupper(*s2++))
	        if(*s1 == '\0' && *s2 == '\0')
	            return 0;
	    if(toupper(*s1) < toupper(*s2))
	        return -1;
	    else
	        return 1;
	}
#endif

	char _ora_db_name[100];
 char _ora_user_name[100];
  char _ora_pwd[100];

 char _mysql_db_name[100];
  char _mysql_user_name[100];
 char _mysql_pwd[100];
  char _mysql_host_name[100];
 int _mysql_port_nb;
char _mysql_socket_path[500];

char _create_ora_schema_path[500];
 char _drop_ora_schema_path[500];

 /***********************************************************************/
/**
       * prepare the MySQL select for the export: the order of the columns will match the ones in Oracle,  returning an integer value.
       * @param table_name : name of the table: it's CASE SENSITIVE.
       * @param col_list_len : length of the column_list.
	   * @param colname_list : char* buffer which contain the list of column
	   * @param  stmt_len : length of the select_stmt. if too small, we put the size here.
	   * @param select_stmt : array which will contains the mysql select statement
	   * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */
int PrepareSelectForMysql(char* table_name, int col_list_len, char* colname_list,int& stmt_len, char* select_stmt,char* ErrMess)
{
	char appliName[100]="PrepareSelectForMysql";
	int pos2=0;
	int pos1=0;
	int stmt_len_temp=0;
	int pos3=0;
	int rescode=0;
	
	char* colname_concat=(char*)malloc(col_list_len*sizeof(char));
	if(colname_concat==NULL)
	{
		rescode=-1;
		GetErrorMess(appliName,"UNSUCCESSFUL MALLOC",ErrMess,1);
		return rescode;
	}
		memcpy(colname_concat,colname_list,col_list_len);


	while(pos2<col_list_len)
	{
		pos1=strlen(colname_list+pos2);
		if(pos1+1+pos2<col_list_len)
			colname_concat[pos2+pos1]=',';
		pos2+=pos1+1;
	
		
	}
	stmt_len_temp=strlen("SELECT   FROM  ")+ strlen(colname_concat)+  strlen(table_name);
if( stmt_len_temp>stmt_len)
{
	stmt_len=stmt_len_temp;
	free(colname_concat);
	rescode=-1;
	GetErrorMess(appliName,"BUFFER_TOO_SMALL",ErrMess,1);
	return rescode;
}
else
{
	stmt_len=stmt_len_temp;
	sprintf(select_stmt,"SELECT %s  FROM %s ",colname_concat,table_name);
	
}


rescode=0;
free(colname_concat);
GetErrorMess(appliName,"",ErrMess,1);
return rescode;
}
/***********************************************************************/
/**
       * read the config file and load the DB param in memory, the pwd for both MySQL/Oracle are deleted,  returning an integer value.
       * @param config_file : name of the config file.
       * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */
int GetParamInfo(char* config_file,char* ErrMess)
{
	char appliName[100]="GetParamInfo";
	char* s=(char*)malloc(2001*sizeof(char));
	FILE* f=NULL;
	double next=1;
	int stop=0;
	int pos1=0;
	int pos2=0;
	int rescode=0;
	if(s==NULL)
	{
		GetErrorMess(appliName,"MALLOC UNSUCCESSFUL",ErrMess,1);
		return -1;
	}

	if ((f=fopen( config_file,"r"))== NULL)
	{
		GetErrorMess(appliName,"Cannot open the config file",ErrMess,1);
		free(s);
		return -1;
	}
	else
	{
		
		while (fgets(s,1000,f)!=NULL )
		{
			if(strstr(s,"ora_db_name=")!=NULL)
			{
				next=next*2;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				//std::cout<<"pos1="<<pos1<<" and pos2="<<pos2<<" and s="<<s<<std::endl;
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_ora_db_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The ora db name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			
			if(strstr(s,"ora_user_name=")!=NULL)
			{
				next=next*3;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				//std::cout<<"pos1="<<pos1<<" and pos2="<<pos2<<" and s="<<s<<std::endl;
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					
					s[pos1+pos2]='\0';
					strcpy(_ora_user_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The ora user name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"ora_pwd=")!=NULL)
			{
				next=next*5;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_ora_pwd,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The ora pwd entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"mysql_db_name=")!=NULL)
			{
				next=next*7;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
				s[pos1+pos2]='\0';
				strcpy(_mysql_db_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql db name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"mysql_user_name=")!=NULL)
			{
				next=next*11;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_user_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql user name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"mysql_pwd=")!=NULL)
			{
				next=next*13;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_pwd,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql pwd entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"mysql_host_name=")!=NULL)
			{
				next=next*17;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_host_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql host name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"mysql_port_nb=")!=NULL)
			{
				next=next*19;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					_mysql_port_nb=atoi(s+pos1+1);
				}
				
			}
			if(strstr(s,"mysql_socket_path=")!=NULL)
			{
				next=next*23;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_socket_path,s+pos1+1);
				}
			}
			if(strstr(s,"create_ora_schema_path=")!=NULL)
			{
				next=next*29;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_create_ora_schema_path,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The create ora schema path entry is empty or you have forgotten the ';' at the end!!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"drop_ora_schema_path=")!=NULL)
			{
				next=next*31;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_drop_ora_schema_path,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The drop ora schema path entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

		}
		fclose(f);
		free(s);
		if(next==200560490130LL)
		{
			GetErrorMess(appliName,"",ErrMess,0);
			rescode=RecreateConfigFile( config_file,  ErrMess);
			rescode=0;

		}
		else
		{
			GetErrorMess(appliName,"Invalid config file, not all the entries were filled",ErrMess,1);
			rescode=-1;
		}
		
		return rescode;
		
	}
}


/***********************************************************************/
/**
       * fct which returns the indexe of the elt,  returning an integer value.
       * @param element_List : list of elements to be looked up.
       * @param elt_List_len : length of the list.
	   * @param element_searched : element to search for
	   * @return 0 if it is successful
       */
int GetIndexForString_second(char*  element_List,int elt_List_len, char* element_searched)
{
char appliName[100]="GetIndexForString_second";
int i=0;
int elt_idx=-1;
int res=-1;
int stop=1;
int pos2=0;
int pos1=0;
int j=0;
int elt_searched_len=strlen(element_searched);
char object_name[100];
//std::cout<<"elt_searched_len="<<element_searched<<std::endl;
while(pos2<elt_List_len)
{
	strcpy(object_name,element_List+pos2);
	//std::cout<<"object_name="<<object_name<<std::endl;
	if(stricmp(object_name,element_searched)==0)
	{
		elt_idx=j;
		pos2=elt_List_len+10;
		//std::cout<<"object_name found="<<object_name<<std::endl;
		//std::cout<<"found matching element element_List[i]="<<element_List[i]<<" and i="<<i<<std::endl; 
	}
	else
	{
		j++;
		pos2+=strlen(object_name)+1;
	}
}

return elt_idx;
}

/***********************************************************************/
/**
       * fct which returns the elt at the given index,  returning an integer value.
       * @param element_List : list of elements to be looked up.
       * @param elt_List_len : length of the list.
	   * @param elt_idx : index to be looked up.
	   * @param element_searched : element which is located at this index
	   * @return 0 if it is successful
       */
int GetEltAt(char*  element_List,int elt_List_len, int elt_idx, char* element_searched)
{
char appliName[100]="GetEltAt";

int i=0;

int res=-1;
int stop=1;
int pos2=2;
int pos1=0;
int j=0;
char object_name[100];
//std::cout<<"elt_searched_len="<<elt_searched_len<<" and elt_List_nb="<<elt_List_nb<<" and element_searched="<<element_searched<<std::endl;
while(pos2<elt_List_len)
{
	strcpy(object_name,element_List+pos2);
	if(elt_idx==j)
	{
		strcpy(element_searched,object_name);
		res=0;
		pos2=elt_List_len+1;

	}
	else
	{
		j++;
		pos2+=strlen(element_List)+1;
	}
	
}

return res;
}
/***********************************************************************/
/**
       * fct which writes the control file for a given table. will be used by SQLLoader,  returning an integer value.
       * @param object_name : name of the table.
       * @param ora_column_list : list of the columns of this table.
	   * @param len_columns : size of ora_column_list.
	   * @param datatype_len : length of the datatype_list.
	   * @param datatype_list : list of the type of the columns of this table.
	   * @param datatype_list : list of the precision type of the columns of this table.
	   * @param ErrMess : error message
	   * @return 0 if it is successful
       */
int WriteCtlFile(char* object_name,char* ora_column_list,int len_columns,int datatype_len, char* datatype_list, int* dataprecision_list, char* ErrMess)
{
char appliName[100]="WriteCtlFile";

FILE *f1; 
int i=0;
int k=0;
int nb_line=0;
int rescode=0;
char  filename[100];
char col_name[31];
char datatype[31];
int stmt_len=5000;
sprintf(filename,"%s.ctl",object_name);
char* col_stmt=(char*)malloc(stmt_len*sizeof(char));
char* col_stmt_temp=(char*)malloc(stmt_len*sizeof(char));
int elt_idx=0;
if(col_stmt==NULL || col_stmt_temp==NULL)
{
	if(col_stmt!=NULL)
		free(col_stmt);
	if(col_stmt_temp!=NULL)
		free(col_stmt_temp);
	GetErrorMess(appliName,"Malloc Unsuccessful",ErrMess,1);
	rescode=-1;
	return rescode;

}
else
	strcpy(col_stmt_temp,"(");

while(i<len_columns)
{
	strcpy(col_name,ora_column_list+i);
	rescode=GetEltAt(datatype_list,datatype_len, k, datatype);
	if(rescode==0)
	{
		if(strstr(datatype,"CHAR") && dataprecision_list[k]>254)
		{
			if(k>0)
				sprintf(col_stmt,"%s, %s CHAR(%d)",col_stmt_temp,col_name,dataprecision_list[k]);
			else
				sprintf(col_stmt,"%s %s CHAR(%d)",col_stmt_temp,col_name,dataprecision_list[k]);

		}
		else
		{
			if(k>0)
				sprintf(col_stmt,"%s, %s ",col_stmt_temp,col_name);
			else
				sprintf(col_stmt,"%s %s ",col_stmt_temp,col_name);

		}
		
	}
	else
	{
		
		sprintf(col_stmt,"Could not find the column type of %s.%s",object_name,col_name);
		GetErrorMess(appliName,col_stmt,ErrMess,1);
		free(col_stmt);
		free(col_stmt_temp);
		rescode=-1;
		return rescode;
	}
	k++;
	i+=strlen(col_name)+1;
	strcpy(col_stmt_temp,col_stmt);

}
sprintf(col_stmt,"%s )", col_stmt_temp);
f1 = fopen (filename, "w+t");  
if (f1==NULL)
{
	sprintf(col_stmt,"Could not create the control file  %s.ctl",object_name);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	free(col_stmt);
	free(col_stmt_temp);
	rescode=-1;
	return rescode;
}

rescode=fprintf(f1,"Load data infile '%s.txt'  badfile '%s.bad' insert into table %s FIELDS TERMINATED BY '|' trailing nullcols \n %s ",object_name,object_name,object_name, col_stmt);

fclose (f1); 
if(rescode<0)
{
	sprintf(col_stmt,"Could not write in the control file  %s.ctl",object_name);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
}
else
	rescode=0;
free(col_stmt);
free(col_stmt_temp);
return rescode;
}
/***********************************************************************/
/**
       * fct which check the compatibility of Oracle and MySQL schemas,  returning an integer value.
       * @param conn_temp : MySQL connection handler
       * @param mysql_tables : list of the MySQL tables.
	   * @param mysql_tab_len : size of mysql_tables.
	   * @param ora_tables : list of the oracle tables.
	   * @param ora_tab_len : size of ora_tables.
	   * @param ora_nb_tab : nb of tables in Oracle
	   * @param nb_rows_per_tab : nb of rows per MySQL table (will be wriiten).
	   * @param ErrMess : error message
	   * @return 0 if it is successful
       */
int CheckSchemasCompatibility(MYSQL* conn_temp,char * mysql_tables,int mysql_tab_len, char* ora_tables,int ora_tab_len,int ora_nb_tab, int* nb_rows_per_tab, char* ErrMess)
{
char appliName[100]="CheckSchemasCompatibility";

int pos2=0;
char object_name[100];
char object_type[31];
char col_name[31];
int elt_idx=0;
int rescode=0;
int res1=0;
int res2=0;
int len_columns=3000;
int  len_nb_col=100;
int datatype_len=len_columns;
int mysql_col_len=len_columns;
int* column_pos_list=(int*)malloc(len_nb_col*sizeof(int));
char* ora_column_list=(char*)malloc(len_columns*sizeof(char));
char* column_type_list=(char*)malloc(len_columns*sizeof(char));
int* dataprecision_list=(int*)malloc(len_nb_col*sizeof(int));
char* mysql_column_list=(char*)malloc(len_columns*sizeof(char));
char logmessage[1000];
char errmess1[500];
int k=0;
int p=0;
int nb_of_tab=0;//start with -1 because the schema version table is counted for MYSQL but not for Oracle
int nb_of_rows=0;
int nb_of_col=0;
int tab_len=0;
int max_value= -1;
char tab_version[31]="schema_version";
if(dataprecision_list==NULL||mysql_column_list==NULL ||ora_column_list==NULL||column_pos_list==NULL||column_type_list==NULL)
{
	if(mysql_column_list!=NULL)
		free(mysql_column_list);
	if(ora_column_list!=NULL)
		free(ora_column_list);
	if(column_pos_list!=NULL)
		free(column_pos_list);
	if(column_type_list!=NULL)
		free(column_type_list);
	if(dataprecision_list!=NULL)
		free(dataprecision_list);
	GetErrorMess(appliName,"Malloc Unsuccessful",ErrMess,1);
	rescode=-1;
	return rescode;

}
while(pos2<mysql_tab_len)
{
	strcpy(object_name,mysql_tables+pos2);
	
	if(stricmp(object_name,tab_version)!=0)
	{
		
	elt_idx=GetIndexForString_second(ora_tables, ora_tab_len, object_name);
	if(elt_idx==-1)
	{
		tab_len=strlen(object_name)+1;
		rescode=Ora_GetObjectType(object_name,tab_len,  object_type, ErrMess);
		if(rescode!=0)
		{
			sprintf(logmessage,"Table %s exists in MySQL but not in Oracle, even as another object",object_name);
			GetErrorMess(appliName,logmessage,ErrMess,1);
			free(mysql_column_list);
			free(column_pos_list);
			free(ora_column_list);
			free(column_type_list);
			free(dataprecision_list);
			rescode=-1;
			return rescode;
		}
		else
		{
			//std::cout<<"sequence="<<object_type<<std::endl;
			if(stricmp(object_type,"SEQUENCE")==0)
			{
				rescode=mysql_GetMaxValue(conn_temp,_mysql_db_name, object_name,max_value,ErrMess);
				if(rescode == 0)
				{
					//std::cout<<"max_value="<<max_value<<std::endl;
					if (max_value > 0) {
						rescode=Ora_UpdateSequence(max_value, object_name, ErrMess);
						if(rescode!=0)
						{
							sprintf(logmessage,"Could not  update the oracle sequence %s",object_name);
							GetErrorMess(appliName,logmessage,ErrMess,1);
							free(mysql_column_list);
							free(column_pos_list);
							free(ora_column_list);
							free(column_type_list);
							free(dataprecision_list);
							rescode=-1;
							return rescode;


						}
					}

				}
				else
				{
					sprintf(logmessage,"Could not get the max value of %s in MySQL: needed to update the oracle sequence",object_name);
					GetErrorMess(appliName,logmessage,ErrMess,1);
					free(mysql_column_list);
					free(column_pos_list);
					free(ora_column_list);
					free(column_type_list);
					free(dataprecision_list);
					rescode=-1;
					return rescode;
				}

			}

		}
		

	}
	else
	{
		//std::cout<<"tabname ora="<<object_name<<std::endl;
		p=elt_idx;
		len_columns=3000;
		len_nb_col=100;
		datatype_len=len_columns;
		mysql_col_len=len_columns;
		res2=mysql_GetColList( conn_temp,object_name,_mysql_db_name,mysql_col_len, mysql_column_list, errmess1);
		//std::cout<<"tabname mysqllen_nb_col="<<mysql_col_len<<std::endl;
			//std::cout<<"tabname ora 3="<<object_name<<std::endl;
		res1=Ora_GetColInfo(object_name,len_columns, ora_column_list,len_nb_col, column_pos_list, datatype_len, column_type_list, dataprecision_list, ErrMess);
		//std::cout<<"tabname len_nb_col="<<len_nb_col<<std::endl;
		
		if(res1==0 && res2==0)
		{	
			//std::cout<<"before the loop="<<mysql_col_len<<std::endl;
			k=0;
			nb_of_col=0;
			while(k<mysql_col_len)
			{
				strcpy(col_name,mysql_column_list+k);
				//std::cout<<"col_name="<<col_name<<std::endl;
				elt_idx=GetIndexForString_second(ora_column_list, len_columns, col_name);
				if(elt_idx==-1)
				{
					//std::cout<<"ora_column_list="<<ora_column_list<<std::endl;
					sprintf(logmessage,"Column %s in table %s exists in MySQL but not in Oracle, even as another object",col_name,object_name);
					GetErrorMess(appliName,logmessage,ErrMess,1);
					free(mysql_column_list);
					free(column_pos_list);
					free(ora_column_list);
					free(column_type_list);
					free(dataprecision_list);
					rescode=-1;
					return rescode;
				}
				else
				{
					k+=strlen(col_name)+1;
				}
				nb_of_col++;
			}
			//std::cout<<"end of loop column="<<col_name<<std::endl;
			if(nb_of_col==len_nb_col)
			{
				//std::cout<<"before disabling the cst"<<std::endl;
				res1=DisEnableFkCst(object_name, 0, ErrMess);
				if(res1!=0 && strstr(ErrMess,"NO_ROWS_SELECTED")==NULL)
				{
					strcpy(logmessage,ErrMess);
					GetErrorMess(appliName,logmessage,ErrMess,1);
					free(mysql_column_list);
					free(column_pos_list);
					free(ora_column_list);
					free(column_type_list);
					free(dataprecision_list);
					rescode=-1;
					return rescode;
				}
				//std::cout<<"after disabling cst="<<nb_of_col<<std::endl;
				nb_of_rows=-1;
				res1=mysql_GetNbOfrows(conn_temp,_mysql_db_name, object_name,nb_of_rows,ErrMess);
				if(res1==0)
					nb_rows_per_tab[p]=nb_of_rows;
				else
					nb_rows_per_tab[p]=-10;
				//std::cout<<"mysql_GetNbOfrows="<<res1<<" and nb or fows ="<<nb_of_rows<<std::endl;
				if(res1==0 && nb_of_rows>0)
				{
					//std::cout<<"wrtiting file="<<nb_of_col<<std::endl;
				
					res1=WriteCtlFile(object_name,ora_column_list,len_columns,datatype_len, column_type_list, dataprecision_list, ErrMess);
					if(res1!=0)
					{
						sprintf(logmessage,"Pb occurs :%s  when creating the ctl file for table %s",ErrMess,object_name);
						GetErrorMess(appliName,logmessage,ErrMess,1);
						free(mysql_column_list);
						free(column_pos_list);
						free(ora_column_list);
						free(column_type_list);
						free(dataprecision_list);
						rescode=-1;
						return rescode;
					}
					else
					{
						//std::cout<<"exporting the schema="<<nb_of_col<<std::endl;
						rescode=ExportTableFromMySQL( conn_temp,object_name, len_columns, ora_column_list,  ErrMess);
						if(rescode!=0)
						{
							sprintf(logmessage,"Pb occurs :%s  when exporting data for mysql table %s",ErrMess,object_name);
							GetErrorMess(appliName,logmessage,ErrMess,1);
							free(mysql_column_list);
							free(column_pos_list);
							free(ora_column_list);
							free(column_type_list);
							free(dataprecision_list);
							rescode=-1;
							return rescode;
						}
					}
				}
				else
				{
					if(nb_of_rows!=0)
					{
					sprintf(logmessage,"Pb occurs when counting nb of rows for mysql table %s  ",object_name);
					GetErrorMess(appliName,logmessage,ErrMess,1);
					
					free(mysql_column_list);
					free(column_pos_list);
					free(ora_column_list);
					free(column_type_list);
					free(dataprecision_list);
					rescode=-1;
					return rescode;
					}
					
				}
			}
			else
			{
				
					sprintf(logmessage,"There are too many columns in table %s stored in the Oracle DB compared to the same one in MySQL ",object_name);
					GetErrorMess(appliName,logmessage,ErrMess,1);
					
					free(mysql_column_list);
					free(column_pos_list);
					free(ora_column_list);
					free(column_type_list);
					free(dataprecision_list);
					rescode=-1;
					return rescode;
				

			}

		}
		else
		{
			if(res1!=0 && res2!=0)
				sprintf(logmessage,"%s and %s", ErrMess, errmess1);
			else
			{
				if(res1==0)
					strcpy(logmessage,errmess1);
				else
					strcpy(logmessage,ErrMess);
			}
			GetErrorMess(appliName,logmessage,ErrMess,1);
			free(mysql_column_list);
			free(column_pos_list);
			free(ora_column_list);
			free(column_type_list);
			free(dataprecision_list);
			rescode=-1;
			return rescode;
		}
		nb_of_tab++;


	}
	
	
	}
	pos2+=strlen(object_name)+1;
	
	
}
free(mysql_column_list);
free(column_pos_list);
free(ora_column_list);
free(column_type_list);
free(dataprecision_list);

if(ora_nb_tab!=nb_of_tab)
{
	sprintf(logmessage,"There are more tables stored in the Oracle DB (%d ) than  in MySQL (%d) ",ora_nb_tab,nb_of_tab);
	GetErrorMess(appliName,logmessage,ErrMess,1);
	
	rescode=-1;
	
}
else
	rescode=0;

return rescode;

}

/***********************************************************************/
/**
       * fct which export the data stored in  MySQL DB into a text file, one per table,  returning an integer value.
       * @param conn_temp : MySQL connection handler
       * @param table_name : name of the table.
	   * @param col_list_len : size of col_list.
	   * @param col_list : list of the column.
	   * @param ErrMess : error message
	   * @return 0 if it is successful
       */
int ExportTableFromMySQL(MYSQL* conn_temp,char* table_name, int col_list_len, char* col_list, char* ErrMess)
{
char appliName[100]="ExportTableFromMySQL";
int rescode=0;
char outputfile[50];
int stmt_len=3000;
char logmessage[1000];
char* select_stmt=(char*)malloc(stmt_len*sizeof(char));
if(select_stmt==NULL)
{
	GetErrorMess(appliName,"Malloc Unsuccessful",ErrMess,1);
rescode=-1;
return rescode;
}
sprintf(outputfile,"%s.txt",table_name);
rescode=PrepareSelectForMysql( table_name,  col_list_len,  col_list, stmt_len,  select_stmt, ErrMess);
//std::cout<<"select_stmt"<<select_stmt<<std::endl;
if(rescode==0)
	rescode=mysql_ExportTab(conn_temp,select_stmt,table_name, ErrMess);
else
{
	strcpy(logmessage,ErrMess);
	free(select_stmt);
	GetErrorMess(appliName,logmessage,ErrMess,1);
	rescode=-1;
	return rescode;
}

if(rescode!=0)
{
	strcpy(logmessage,ErrMess);
	free(select_stmt);
	GetErrorMess(appliName,logmessage,ErrMess,1);
	rescode=-1;
	return rescode;
}

GetErrorMess(appliName,"",ErrMess,0);
rescode=0;
return rescode;

}
/***********************************************************************/
/**
       * fct which generates shell scripts,  returning an integer value.
       * @param source_file : name of the source file
       * @param sql_load : specify what to write in it.
	   * @param ErrMess : error message
	   * @return 0 if it is successful
       */
int GenerateShellScript(char* source_file, int sql_load,char* ErrMess)
{
char appliName[100]="GenerateShellScript";

FILE *f1; 
char col_stmt[200];
int rescode=0;
int res2;

f1 = fopen (source_file, "w+t");  
if (f1==NULL)
{
	sprintf(col_stmt,"Could not create the shell script ",source_file);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	
	rescode=-1;
	return rescode;
}
rescode=fprintf(f1,"#! /bin/sh -f \n" );
switch(sql_load)
{
	case 0:
		rescode=fprintf(f1,"for name in `ls *.ctl`; \n do \n rm -r $name \n done \n");
		rescode=fprintf(f1,"for  name in `ls  *.txt` ; \n do \n rm -r $name done \n");
		break;
	case 1 :
		rescode=fprintf(f1,"for  d in `ls  *.ctl |cut -d . -f 1 -s` ; \n do \n");
		rescode=fprintf(f1,"sqlldr %s/%s@%s control=$d.ctl log=$d.log ERRORS=0   \n",_ora_user_name, _ora_pwd,_ora_db_name);
		rescode=fprintf(f1,"done \n");
		rescode=fprintf(f1,"for name in `ls  *.ctl` ; \n do \n  rm -r $name \n done \n");
		rescode=fprintf(f1,"for  name in  `ls  *.txt`;  \n do \n rm -r $name \n done \n");
		break;
	case 2 :

		fprintf(f1,"echo exit | sqlplus %s/%s@%s @%s \n",_ora_user_name,_ora_pwd,_ora_db_name,_create_ora_schema_path);
		//fprintf(f1,"exit \n");
		break;
	case 3 :
		fprintf(f1,"echo exit | sqlplus %s/%s@%s @%s \n",_ora_user_name,_ora_pwd,_ora_db_name,_drop_ora_schema_path);
		//fprintf(f1,"exit \n");
		break;

	case 4 :
		fprintf(f1,"echo exit | sqlplus %s/%s@%s @%s \n",_ora_user_name,_ora_pwd,_ora_db_name,_drop_ora_schema_path);
		//fprintf(f1,"exit \n");
		rescode=fprintf(f1," for name in `ls  *.ctl` ; \n do \n  rm -r $name \n done \n");
		rescode=fprintf(f1,"for  name in `ls  *.txt`; \n do \n rm -r $name\n done \n");
		break;
}

res2=fprintf(f1,"echo \"exiting from the script\" \n " );

fclose (f1); 
if( res2>0)
	rescode=0;
else
	rescode=-1;

return rescode;
}
/***********************************************************************/
/**
       * fct which disable or enable all the fk of  a given ora table,  returning an integer value.
       * @param table_name : name of the ora table
       * @param enable_cst : 1 to enbale, 0 otherwise.
	   * @param ErrMess : error message
	   * @return 0 if it is successful
       */
int DisEnableFkCst(char* table_name, int enable_cst,char* ErrMess)
{
	char appliName[100]="DisEnableFkCst";

	int rescode=0;
	int len_cst_list=3000;
	char* cstname_list=(char*)malloc(len_cst_list*sizeof(char));
	char logmessage[1000];
	int pos2=0;
	char cst_name[31];
	if(cstname_list==NULL)
	{
		GetErrorMess(appliName,"Malloc unsuccessful",ErrMess,1);
		rescode=-1;
		return rescode;

	}
	rescode=Ora_GetFkConstraints( table_name,len_cst_list, cstname_list, ErrMess);
	if(rescode==0 && strstr(ErrMess,"NO_ROWS_SELECTED")==NULL)
	{
		rescode=Ora_DisEnableFkConstraint(table_name,len_cst_list, cstname_list,enable_cst, ErrMess);
		if(rescode!=0)
		{
			strcpy(logmessage,ErrMess);
			free(cstname_list);
			GetErrorMess(appliName,logmessage,ErrMess,1);
			rescode=-1;
			return rescode;
		}
	}
	else
	{
		strcpy(logmessage,ErrMess);
		free(cstname_list);
		GetErrorMess(appliName,logmessage,ErrMess,1);
		rescode=-1;
		return rescode;
	}



}

/***********************************************************************/
/**
       * fct which checks the data imported in Oracle,  returning an integer value.
       * @param conn_temp : MySQL connection handler
       * @param ora_tab_list : list of the oracle tables.
	   * @param table_list_len : length of the ora_tab_list.
	   * @param nb_rows_tab_list : nb of rows per MySQL table.
	   * @param ErrMess : error message
	   * @return 0 if it is successful
       */
int CheckImportData(MYSQL* conn_temp,char* ora_tab_list,int table_list_len,int* nb_rows_tab_list,  char* ErrMess)
{
char appliName[100]="CheckImportData";

char tabname[31];
char col_stmt[500];
int k=0;
int p=0;
int nb_rows1=0;
int nb_rows2=0;
int rescode=0;
int rescode1=0;
int new_value=500000;
char sequence_name[31]="cns_unique_id";
while(k<table_list_len)
{
strcpy(tabname,ora_tab_list+k);


nb_rows1=nb_rows_tab_list[p];
//std::cout<<"tabname="<<tabname<<" and nb of rows="<<nb_rows1<<std::endl;
if(nb_rows1>-1)
{
	rescode1=Ora_GetNbOfRows(tabname,nb_rows2, ErrMess);
	if(rescode1==0)
	{
		if(nb_rows1!=nb_rows2)
		{
			k=table_list_len+10;
			sprintf(col_stmt,"Nb of rows in table %s is not the same in MySQL (%d rows) and Oracle (%d rows) ",tabname,nb_rows1, nb_rows2);
			GetErrorMess(appliName,col_stmt,ErrMess,1);
			rescode=-1;
			return rescode;
		}

	}
	else
	{
		k=table_list_len+10;
		if(rescode!=0)
			sprintf(col_stmt,"could not count the nb of rows in table %s in MySQL ",tabname);
		else
			sprintf(col_stmt,"could not count the nb of rows in table %s in Oracle ",tabname);

		GetErrorMess(appliName,col_stmt,ErrMess,1);
		rescode=-1;
		return rescode;
	}
	rescode=DisEnableFkCst(tabname, 1, col_stmt);
	if(rescode!=0 && strstr(col_stmt,"NO_ROWS_SELECTED")==NULL)
	{
		k=table_list_len+10;
		GetErrorMess(appliName,col_stmt,ErrMess,1);
		rescode=-1;
		return rescode;
	}
	
	
}

k+=strlen(tabname)+1;
p++;

}
//std::cout<<"ErrMess="<<ErrMess<<std::endl;
rescode=0;
GetErrorMess(appliName,"none",ErrMess,0);
return rescode;
}

/***********************************************************************/
/**
       * fct which deletes the pwd for both Oracle and MySQL DB in the config file,  returning an integer value.
       * @param filename : name of the config file
	   * @param ErrMess : error message
	   * @return 0 if it is successful
       */
int RecreateConfigFile(char* filename, char* ErrMess)
{
char appliName[100]="RecreateConfigFile";
FILE *f1;
int rescode=0;
f1 = fopen (filename, "w+t");  
char col_stmt[200];
if (f1==NULL)
{
	sprintf(col_stmt,"Could not open the config file  %s ",filename);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	
	rescode=-1;
	return rescode;
}

rescode=fprintf(f1,"ora_db_name=%s;\nora_user_name=%s;\nora_pwd=\nmysql_host_name=%s;\nmysql_user_name=%s;\nmysql_pwd=\nmysql_db_name=%s;\nmysql_port_nb=\nmysql_socket_path=\ncreate_ora_schema_path=%s;\ndrop_ora_schema_path=%s;\n",_ora_db_name,_ora_user_name,_mysql_host_name, _mysql_user_name,_mysql_db_name,_create_ora_schema_path,_drop_ora_schema_path);
fclose (f1); 
GetErrorMess(appliName,"none",ErrMess,0);
return 0;
}



			
 
 

