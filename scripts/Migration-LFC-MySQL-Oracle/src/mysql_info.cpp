#include "mysql_info.h"
#include "getError.h"
MYSQL* conn_handler;

/***********************************************************************/
/**
       * to connect to a MySQL DB,  returning an integer value.
       * @param host_name : name of the server which hosts the MySQL server
       * @param user_name : name of the MySQL user.
	   * @param password : its pwd
	   * @param  db_name : name of the database (in our case should cns_db)
	   * @param port_num : optional
	   * @param socket_name : optional
	   * @param flags : optional
	   * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */
int mysql_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess)
{
	char appliName[100]="mysql_DBConnexion";
	
	char errmessage[500];
	conn_handler=mysql_init(NULL);
	if(conn_handler==NULL)
	{
		GetErrorMess(appliName,"mysql_init() failed ",ErrMess,1);
		return -1;
	}
	if(mysql_real_connect (conn_handler,host_name, user_name,password,db_name,port_num,socket_name,flags)==NULL)
	{
		sprintf(errmessage,"mysql_real_connect() failed. Error %u (%s)",mysql_errno(conn_handler),mysql_error(conn_handler));
		GetErrorMess(appliName,errmessage,ErrMess,1);
		return -1;
	}
	return 0;
}

/***********************************************************************/
/**
       * to disconnect from a MySQL DB,  returning an integer value.
       */
void mysql_disconnect()
{
mysql_close(conn_handler);
}
/***********************************************************************/
/**
       * to get the list of tables in a MySQL DB,  returning an integer value.
       * @param conn_temp : MySQL connection handler
       * @param db_name : name of the MySQL db.
	   * @param table_list_len : length of the table_list
	   * @param  table_list : list of the tables
	   * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */
int mysql_GetTableList(MYSQL * conn_temp,char* db_name, int& table_list_len, char* table_list, char* ErrMess)
{
	char appliName[100]="mysql_GetTableList";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;
	char mysql_stmt[1000];
	char* tabnames_temp=NULL;
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"SHOW TABLES from %s ",db_name);
	else
		strcpy(mysql_stmt,"SHOW TABLES");

	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		return rescode;
	}
	else
		res_set=mysql_store_result(conn_temp);

	if (res_set == NULL){
		if (*mysql_error (conn_temp))
			sprintf (ErrMess,"%s : mysql_store_result () failed %s",appliName, mysql_error (conn_temp));
		else
			sprintf (ErrMess, "%s: mysql_store_result () failed : %s",appliName, mysql_stmt);
		return -1;
	} 

	while((row=mysql_fetch_row(res_set))!=NULL)
	{
		pos1=strlen(row[0]);
		pos3=pos2;
		pos2+=pos1+1;
		tabnames_temp=(char*) realloc(tabnames_temp,pos2*sizeof(char));
		if(tabnames_temp==NULL)
		{
			
			rescode=-1;
			mysql_free_result(res_set);
			GetErrorMess(appliName,"REALLOC UNSUCCESSFUL",ErrMess,1);
			return rescode;
		}
		else
		{
			memcpy(tabnames_temp+pos3,row[0],pos1+1);
			i++;
		}

	}
	mysql_free_result(res_set);
	if(pos2>table_list_len)
	{
		free(tabnames_temp);
		table_list_len=pos2;
		GetErrorMess(appliName,"BUFFER TOO SMALL",ErrMess,1);
		return rescode;
	}
	else
	{
		if(i>0)
		{
			table_list_len=pos2;
			memcpy(table_list,tabnames_temp,pos2);
			GetErrorMess(appliName,"",ErrMess,0);
			rescode=0;
		}
		else
		{
			GetErrorMess(appliName,"NO ROWS SELECTED",ErrMess,1);
		}
	}

	free(tabnames_temp);
	return rescode;
	
}

/***********************************************************************/
/**
       * to get the list of columns of the given table in a MySQL DB,  returning an integer value.
       * @param conn_temp : MySQL connection handler
	   * @param table_name : name of a MySQL table.
       * @param db_name : name of the MySQL db (put "none", if you have provided a db name in the config file).
	   * @param column_list_len : length of the column_list
	   * @param  column_list : list of the columns
	   * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */

int mysql_GetColList(MYSQL * conn_temp, char* table_name,char* db_name,int& column_list_len, char* column_list, char* ErrMess)
{
	char appliName[100]="mysql_GetColList";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;
	char dot_del='.';
	char mysql_stmt[1000];
	char* tabnames_temp=NULL;
	//std::cout<<"je suis dans la fct chainte"<<std::endl;
	


	//if(strstr(db_name,"none")==NULL)
	//	sprintf(mysql_stmt,"SHOW columns from %s%c%s",db_name,dot_del,table_name);
	//else
		sprintf(mysql_stmt,"SHOW columns from %s",table_name);

	rescode=strlen(mysql_stmt)+1;
	if(mysql_real_query(conn_temp,mysql_stmt,rescode)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		return rescode;
	}
	else
	{
		//std::cout<<"before the mysql_query 2"<<std::endl;
		res_set=mysql_store_result(conn_temp);
	}

	//std::cout<<"before the mysql_store_result"<<std::endl;
	if (res_set == NULL){
		if (*mysql_error (conn_temp))
			sprintf (ErrMess,"%s : mysql_store_result () failed %s",appliName, mysql_error (conn_temp));
		else
			sprintf (ErrMess, "%s: mysql_store_result () failed : %s",appliName, mysql_stmt);
		return -1;
	} 
	//std::cout<<"before the loop"<<std::endl;
	while((row=mysql_fetch_row(res_set))!=NULL)
	{
		//std::cout<<"row[0]="<<row[0]<<std::endl;
		pos1=strlen(row[0]);
		if(strstr(row[0],"rowid")==NULL)
		{
			pos3=pos2;
			pos2+=pos1+1;
			tabnames_temp=(char*) realloc(tabnames_temp,pos2*sizeof(char));
			if(tabnames_temp==NULL)
			{
				
				rescode=-1;
				mysql_free_result(res_set);
				GetErrorMess(appliName,"REALLOC UNSUCCESSFUL",ErrMess,1);
				return rescode;
			}
			else
			{
				memcpy(tabnames_temp+pos3,row[0],pos1+1);
				i++;
			}

		}
	}
	mysql_free_result(res_set);
	//std::cout<<"end of loop mysql="<<std::endl;
	if(pos2>column_list_len)
	{
		free(tabnames_temp);
		column_list_len=pos2;
		GetErrorMess(appliName,"BUFFER TOO SMALL",ErrMess,1);
		return rescode;
	}
	else
	{
		if(i>0)
		{
			column_list_len=pos2;
			memcpy(column_list,tabnames_temp,pos2);
			GetErrorMess(appliName,"",ErrMess,0);
			rescode=0;
		}
		else
		{
			GetErrorMess(appliName,"NO ROWS SELECTED",ErrMess,1);
		}
	}

	free(tabnames_temp);
	return rescode;
}

/***********************************************************************/
/**
       * export the data of a table into a text file of a given table in a MySQL DB,  returning an integer value.
       * @param conn_temp : MySQL connection handler
	   * @param select_stmt : the select stmt already prepared.
       * @param table_name : name of the table .
	   * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */
int mysql_ExportTab(MYSQL* conn_temp,char* select_stmt,char* table_name, char* ErrMess)
{
	char appliName[100]="mysql_ExportTab";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;
	char mysql_stmt[1000];
	char* tabnames_temp=NULL;
	FILE *f1; 
	char  filename[100];
	char col_stmt[150];
	char* table_row=NULL;
	int len_row=2000;
	int current_len=0;
	sprintf(filename,"%s.txt",table_name);

	if(mysql_query(conn_temp,select_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		std::cout<<" Error "<<mysql_errno(conn_handler)<<" and "<< mysql_error(conn_handler) <<std::endl;
	}
	else
	{
		//std::cout<<"before the mysql_query 2"<<std::endl;
		res_set=mysql_store_result(conn_temp);
	}

	//std::cout<<"before the mysql_store_result"<<std::endl;
	if (res_set == NULL){
		if (*mysql_error (conn_temp))
			sprintf (ErrMess,"%s : mysql_store_result () failed %s",appliName, mysql_error (conn_temp));
		else
			sprintf (ErrMess, "%s: mysql_store_result () failed : %s",appliName, mysql_stmt);
		return -1;
	}
	//std::cout<<"before the loop"<<std::endl;
	
table_row=(char*)realloc(table_row,len_row*sizeof(char));
if (table_row==NULL)
{
	sprintf(col_stmt,"Malloc unsuccesful for table_row of size %d ",len_row);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	rescode=-1;
	return rescode;
}
f1 = fopen (filename, "w+t");  
if (f1==NULL)
{
	sprintf(col_stmt,"Could not create the data file  %s.txt",table_name);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	free(table_row);
	rescode=-1;
	return rescode;
}
int num_fields = mysql_num_fields (res_set);
 
	while((row=mysql_fetch_row(res_set))!=NULL)
	{
		current_len=num_fields;
		for(i=0;i<num_fields;i++)
		{
			if(row[i]!=NULL)
				current_len+=strlen(row[i]);
		}
		if(current_len>len_row)
		{
			table_row=(char*)realloc(table_row,current_len*sizeof(char));
			if (table_row==NULL)
			{
				sprintf(col_stmt,"Malloc unsuccesful for table_row of size %d ",len_row);
				GetErrorMess(appliName,col_stmt,ErrMess,1);
				rescode=-1;
				return rescode;
			}

		}
		if(row[0]!=NULL)
			strcpy(table_row,row[0]);
		for(i=1;i<num_fields;i++)
		{
			strcat(table_row,"|");
			if(row[i]!=NULL)
				strcat(table_row,row[i]);
		}
		rescode=fprintf(f1,"%s\n",table_row);

		
	}
	mysql_free_result(res_set);
	fclose (f1);
	return 0;
}

/***********************************************************************/
/**
       * get the nb of rows of a given table in a MySQL DB,  returning an integer value.
       * @param conn_temp : MySQL connection handler
	   * @param db_name : name of the MySQL db (put "none", if you have provided a db name in the config file)
       * @param table_name : name of the table .
       * @param nb_of_rows : nb of rows of this table .
	   * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */
int mysql_GetNbOfrows(MYSQL * conn_temp,char* db_name, char* table_name,int &nb_of_rows,char* ErrMess)
{
	char appliName[100]="mysql_GetNbOfrows";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;
	char mysql_stmt[1000];
	
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"select count(*) from %s.%s",db_name,table_name);
	else
		sprintf(mysql_stmt,"select count(*) from %s",table_name);

	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		
	}
	else
		res_set=mysql_store_result(conn_temp);

	if (res_set == NULL){
		if (*mysql_error (conn_temp))
			sprintf (ErrMess,"%s : mysql_store_result () failed %s",appliName, mysql_error (conn_temp));
		else
			sprintf (ErrMess, "%s: mysql_store_result () failed : %s",appliName, mysql_stmt);
		return -1;
	}
	while((row=mysql_fetch_row(res_set))!=NULL)
	{
		nb_of_rows=atoi(row[0]);
		i++;
		
	}
	if (nb_of_rows>-1)
		rescode=0;
	mysql_free_result(res_set);
	return rescode;
}

/***********************************************************************/
/**
       * get the max value of a given table in a MySQL DB used to update the last value of  oracle sequence,  returning an integer value.
       * @param conn_temp : MySQL connection handler
	   * @param db_name : name of the MySQL db (put "none", if you have provided a db name in the config file)
       * @param table_name : name of the table .
       * @param max_value : max value of this table .
	   * @param ErrMess : error message in case of failure
       * @return 0 if it is successful
       */
int mysql_GetMaxValue(MYSQL * conn_temp,char* db_name, char* table_name,int &max_value,char* ErrMess)
{
	char appliName[100]="mysql_GetNbOfrows";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=-1;
	int rescode=-1;
	char colname[31]="id";
	char mysql_stmt[1000];
	
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"select %s from %s.%s",colname,db_name,table_name);
	else
		sprintf(mysql_stmt,"select %s from %s",colname,table_name);

	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		
	}
	else
		res_set=mysql_store_result(conn_temp);

	if (res_set == NULL){
		if (*mysql_error (conn_temp))
			sprintf (ErrMess,"%s : mysql_store_result () failed %s",appliName, mysql_error (conn_temp));
		else
			sprintf (ErrMess, "%s: mysql_store_result () failed : %s",appliName, mysql_stmt);
		return -1;
	}
	i=0;
	while((row=mysql_fetch_row(res_set))!=NULL)
	{
		max_value=atoi(row[0]);
		i++;
		
	}
	if (i>-1)
		rescode=0;
	if (i==0) //means table empty
		max_value == 0;
	mysql_free_result(res_set);
	return rescode;
}
