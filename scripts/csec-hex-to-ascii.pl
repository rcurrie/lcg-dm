#!/usr/bin/perl -w
#
# Copyright (c) Members of the EGEE Collaboration. 2006.
# See http://public.eu-egee.org/partners/ for details on 
# the copyright holders.
#
# Authors: 
#      Akos Frohner <Akos.Frohner@cern.ch>
#
# This script takes the tracefile of the Csec library
# and aguments the hexadecimal buffer dumps with text
# representations.

while (<>) {
    if (/^[[:xdigit:]][[:xdigit:]]( [[:xdigit:]][[:xdigit:]])*/) {
        my $line = '';
        my @line = map(hex, split(/\s+/));
        foreach my $char (@line) {
            if ($char < 32 or $char > 126) {
                $line .= '.';
            }
            else {
                $line .= chr($char);
            }
        }
        chomp($_);
        $_ =~ s/\s+$//;
        print $_, ' 'x(60 - length($_)), $line, "\n";
    }
    else {
        print;
    }
}

