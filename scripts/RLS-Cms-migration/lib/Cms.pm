#
# constants
#

package Cms;

use Time::Local;
use DBI;

sub queryFromRLSAndFileAndUpdateLFC($$$) {
    my ($dbh_lrc, $lrc_user, $file) = @_;

    my $count = 0;
    my ($line, $file_size, $guid, $pfn, $lfc_alias);
    my @values;

    # read file
    open (FILE, $file) or die("Error: cannot open file $file");

    while ($line = <FILE>) {

	# get the guid, pfn and lfc_alias
	$line =~ s/\n//;
	@values = split(' ', $line);
	$guid = $values[0];
	$pfn = $values[1];
	$lfc_alias = $values[3];

	# get the file size from the LRC database
	$file_size = getFileSize($dbh_lrc, $lrc_user, $pfn);

	# update the LFC
	updateLFC($guid, $lfc_alias, $pfn, $file_size);
	
	$count = $count+1;

    }

    close(FILE) or die ("Error: cannot close file $file.");

    return $count;
}


sub getFileSize($$$) {

    my ($dbh_lrc, $lrc_user, $pfn) = @_;
    my $select;
    my $file_size;

    $select= $dbh_lrc->prepare("
	SELECT \"ATTR_size\" 
        FROM pfn
        WHERE pfn_pfn = '$pfn'");
    $select->execute();

    $file_size = $select->fetchrow_array();
    print "file_size = $file_size\n";
    $select->finish;

    if (defined ($file_size)) {
           return $file_size;
    } else {
           return 0;
    }

}


sub updateLFC($$$$) {

    my ($guid, $lfc_alias, $pfn, $file_size) = @_;
    my $directory;

    # if $guid starts with "guid:",  remove this part
    if ($guid =~ /guid:/) {
        $guid =~ s/guid://;
    }

    # parse the lfc_alias
    ($directory, $lfc_alias) = parseAlias($lfc_alias);

    # create the directories where to put the file
    print "./migrate_path /grid/cms/$directory -v cms\n";
    `./migrate_path /grid/cms/$directory -v cms`;

    # create the guid (if it doesn't exist), the alias and the pfn
    print "./migrate_files -p $directory -a $lfc_alias -g $guid -r $pfn -v cms\n";
    `./migrate_files -p $directory -a $lfc_alias -g $guid -r $pfn -v cms`;

    # migrate the files size
    print "./migrate_info -g $guid -s $file_size\n";
    `./migrate_info -g $guid -s $file_size`;
} 

sub parseAlias($) {

    my ($lfc_alias) = @_;
    my $directory, $index;

    print "alias before parsing = $lfc_alias\n";

    # remove /grid/cms from $lfc_alias
    $lfc_alias =~ s/\/grid\/cms//;

    # find the position of the last occurence of "/"
    $index = rindex($lfc_alias, "/");

    $directory = substr($lfc_alias, 0, $index);
    $lfc_alias = substr($lfc_alias, $index+1);
    print "directory = $directory\nalias after parsing = $lfc_alias\n";
    return ($directory, $lfc_alias);

}



1;
