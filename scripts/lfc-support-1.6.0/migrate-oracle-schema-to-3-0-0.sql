ALTER TABLE Cns_file_metadata MODIFY name VARCHAR2(255);
ALTER TABLE Cns_file_replica ADD ctime NUMBER(10);
ALTER TABLE Cns_file_replica ADD ltime NUMBER(10);
ALTER TABLE Cns_file_replica ADD r_type CHAR(1);
ALTER TABLE Cns_file_replica ADD setname VARCHAR2(36);
CREATE INDEX replica_host ON Cns_file_replica(host);

DELETE FROM schema_version;
INSERT INTO schema_version VALUES(3, 0, 0);
