ALTER TABLE Cns_file_metadata MODIFY name VARCHAR(255) BINARY;
ALTER TABLE Cns_file_replica ADD ctime INTEGER;
ALTER TABLE Cns_file_replica ADD ltime INTEGER;
ALTER TABLE Cns_file_replica ADD r_type CHAR(1) BINARY;
ALTER TABLE Cns_file_replica ADD setname VARCHAR(36) BINARY;
ALTER TABLE Cns_file_replica ADD INDEX (host);

-- replace unique constraint with an index

ALTER TABLE Cns_file_replica DROP INDEX sfn;
ALTER TABLE Cns_file_replica ADD INDEX (sfn(255));

DELETE FROM schema_version;
INSERT INTO schema_version VALUES(3, 0, 0);
