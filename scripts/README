#
# README - Description of the contents of this directory
#
# Author : James Casey <james.casey@cern.ch>
#
# 2004/12/14 : Changes made for the LCG File Catalog (LFC) by Sophie Lemaitre <Sophie.Lemaitre@cern,ch>
#

OVERVIEW
========

This directory contains scripts that allow you to configure an existing
Oracle 10 database to contain the appropriate user and tablespace to
support the LCG File Catalog (LFC). We would assume any running oracle 
instance could be used, given some small configuration of these scripts.

It consists of 3 scripts, described below:


create-tablespaces-lfc
----------------------

This creates the tablespace for a given user name, that will then be 
prefixed with 'LFC_'. The only required argument for this is this name. 
The default location is the standard CERN OFA data directory -
'/ORA/dbs03/oradata'.  It is possible to change this location to match
a different file system layout.

Usage
-----
usage : ./create-tablespaces-lfc --name name [--datadir dir] [--v]

Options
    --name      name   The LFC database user called LFC_<NAME>
    --datadir   dir    The name of the oracle data directory to use. Defaults
                       to "/ORA/dbs03/oradata"
    --v                verbose mode

create-user-lfc
---------------

This script creates a user, named LFC_<NAME> where <NAME> is the name 
given with the --name option. 

It also sets the default data and index tablespaces to those created
in the 'create-tablespaces' script.  A temporary tablespace is also
required for the user.  This defaults to the tablespace name 'TEMP01'.
It is possible to change this to a different tablespace if you already
have a temporary tablespace.

* Usage

usage : ./create-user-lfc --name name 
                          --password=password
                          [--temp tablespace] [--v]

Options
    name          name       The LFC user will be called LFC_<NAME>
    password      password   Password for the LFC user
    temp          tablespace The name of the temp tablespace (defaults
                             to 'TEMP01')
    v                        verbose mode


create-schema
-------------

This script creates the LFC database schema for the LFC_<NAME> user.

By default, the schema template used is :
/opt/lcg/share/LFC/db-deployment/create_lfc_oracle_tables.sql.templ

But you can specify your own location, if needed.

* Usage

usage : ./create-schema-lfc --name name --password=password
                            [--schema location] [--v]

Options
    name      name       The username for the LFC account will be "LFC_<NAME>"
    password  password   Password for the LFC account
    schema    location   The location on disk of the LFC schema template. Defaults
                         to /opt/lcg/share/LFC/db-deployment/create_lfc_oracle_tables.sql.templ
    v                    verbose mode



HOWTO : Setting up the DB for a single user 
===========================================

To do this,  you need to run the three scripts in the right order:

* create-tablespaces-lfc
* create-user-lfc
* create-schema-lfc

After running them, you will have one configured user account. The user 
name is set to LFC_<NAME> where 'NAME' is the value given to the --name option.
It will use its own separate tablespace for data and indexes.

Before running it, you need to gather the following information (the names
in brackets refer to the specific scripts that need the information):


Required information
--------------------

* name [tablespaces, user, schema]. The database user will be called LFC_<NAME>.

* user password [user, schema].  This is the password for the database user.  

Optional information
--------------------
* Oracle Data directory [tablespaces].  This is where the oracle databases
  files will reside (inside a subdirectory with the same name as the
  ORACLE_SID).

* Temp tablespace name [user] The user requires a temporary tablespace.  We 
  assume one exists, called 'TEMP01'.  If you wish to use a different temp
  tablespace, you can configure it easily.

* Schema location [schema] The complete path to the schema SQL template. 

Example
-------

Here we show the commands to set up a user for the name 'foo' (in fact LFC_FOO) 
with the password 'hello'.


> create-tablespaces-lfc --name foo

> create-user-lfc --name foo --password hello

> create-schema-lfc --name foo --password hello --schema /tmp/create_lfc_oracle_tables.sql.templ
