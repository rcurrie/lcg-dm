--
-- Scripts that migrates the Database Schema from 1.1.1 to 2.0.0
--
-- The 2.1.0 schema for Oracle replace the virtual ids sequences by tables.
--

--
-- Create the new tables
--
CREATE TABLE Cns_unique_uid (
       id NUMBER(10));
INSERT INTO Cns_unique_uid SELECT Cns_uid_seq.NEXTVAL FROM DUAL;

CREATE TABLE Cns_unique_gid (
       id NUMBER(10));
INSERT INTO Cns_unique_gid SELECT Cns_gid_seq.NEXTVAL FROM DUAL;

--
-- Delete the sequences
--
DROP SEQUENCE Cns_uid_seq;
DROP SEQUENCE Cns_gid_seq;

--
-- Update the schema version
--
UPDATE schema_version SET major=2, minor=1, patch=0;
