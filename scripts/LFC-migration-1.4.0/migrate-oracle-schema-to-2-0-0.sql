--
-- Scripts that migrates the Database Schema from 1.1.1 to 2.0.0
--
-- The 2.0.0 schema integrates :
--	- virtual uids/gids
--	- VOMS support
--

--
-- Create the new tables
--
CREATE TABLE Cns_groupinfo (
       gid NUMBER(10),
       groupname VARCHAR2(255));

CREATE TABLE Cns_userinfo (
       userid NUMBER(10),
       username VARCHAR2(255));

CREATE TABLE Cns_unique_gid (
       id NUMBER(10));

CREATE TABLE Cns_unique_uid (
       id NUMBER(10));

ALTER TABLE Cns_groupinfo
       ADD CONSTRAINT map_groupname UNIQUE (groupname);
ALTER TABLE Cns_userinfo
       ADD CONSTRAINT map_username UNIQUE (username);

--
-- Update the schema version
--
UPDATE schema_version SET major=2, minor=1, patch=0;
