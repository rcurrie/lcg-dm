#include "check_migration.h"
MYSQL* conn_handler;
#include <string.h>
#define MAX_ROWS_UPDATED 1000
  char _mysql_user_name[100];

  char _mysql_host_name[100];
 int _mysql_port_nb;
char _mysql_socket_path[500];



int GetParamInfo(char* config_file,char* ErrMess)
{
	char appliName[100]="GetParamInfo";
	char* s=(char*)malloc(2001*sizeof(char));
	FILE* f=NULL;
	double next=1;
	int stop=0;
	int pos1=0;
	int pos2=0;
	int rescode=0;
	if(s==NULL)
	{
		GetErrorMess(appliName,"MALLOC UNSUCCESSFUL",ErrMess,1);
		return -1;
	}

	if ((f=fopen( config_file,"r"))== NULL)
	{
		GetErrorMess(appliName,"Cannot open the config file",ErrMess,1);
		free(s);
		return -1;
	}
	else
	{
		
		while (fgets(s,1000,f)!=NULL )
		{
			

			

			if(strstr(s,"mysql_user_name=")!=NULL)
			{
				next=next*3;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_user_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql user name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			
			if(strstr(s,"mysql_host_name=")!=NULL)
			{
				next=next*5;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_host_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql host name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"mysql_port_nb=")!=NULL)
			{
				next=next*7;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					_mysql_port_nb=atoi(s+pos1+1);
				}
				
			}
			if(strstr(s,"mysql_socket_path=")!=NULL)
			{
				next=next*11;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_socket_path,s+pos1+1);
				}
			}
		
			
		}
		
		fclose(f);
		free(s);
		if(next==1155)
		{
			GetErrorMess(appliName,"",ErrMess,0);
			rescode=RecreateConfigFile( config_file,  ErrMess);
			rescode=0;

		}
		else
		{
			GetErrorMess(appliName,"Invalid config file, not all the entries were filled",ErrMess,1);
			rescode=-1;
		}
		
		return rescode;
		
	}
}

int MYSQL_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess)
{
	char appliName[100]="mysql_DBConnexion";
	
	char errmessage[500];
	conn_handler=mysql_init(NULL);
	if(conn_handler==NULL)
	{
		GetErrorMess(appliName,"mysql_init() failed ",ErrMess,1);
		return -1;
	}
	if(mysql_real_connect (conn_handler,host_name, user_name,password,db_name,port_num,socket_name,flags)==NULL)
	{
		sprintf(errmessage,"mysql_real_connect() failed. Error %u (%s)",mysql_errno(conn_handler),mysql_error(conn_handler));
		GetErrorMess(appliName,errmessage,ErrMess,1);
		return -1;
	}
	


	return 0;
}

void MYSQL_disconnect()
{
mysql_close(conn_handler);
}



int MYSQL_check_col_val(MYSQL * conn_temp,char* db_name, char* tab_name,char* col_name, char* cond_needed, int &nb_elt,char* ErrMess)
{
	char appliName[100]="mysql_check_col";
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;

	int rescode=-1;
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	char mysql_stmt[1000];
	char ErrMess_temp[1000];
	
	if(strstr(db_name,"none")==NULL)
	  sprintf(mysql_stmt,"select count(*) from %s.%s where %s is NULL and %s",db_name,tab_name,col_name,cond_needed);
	 
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		sprintf(ErrMess_temp,"mysql_query failed error :%s",mysql_error(conn_temp));
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);		
	}
	else
		res_set=mysql_store_result(conn_temp);



	if(res_set==NULL)
	{
		sprintf(ErrMess_temp,"mysql_store_result() failed error :%s",mysql_error(conn_temp));
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);

		return rescode;
	}
	if((row=mysql_fetch_row(res_set))!=NULL)
	{
		nb_elt=atoi(row[0]);
		
		rescode=0;
		sprintf(ErrMess,"result_query : tabname=%s, col_name=%s, cond_put=%s , nb_rows_matched=%d",tab_name,col_name,cond_needed,nb_elt);
		WriteToLogFile(appliName,ErrMess );
	}
	else
		rescode=0;
	

	return rescode;
}


int MYSQL_update_col_val(MYSQL * conn_temp,char* db_name,  char* tab_name,char* col_name,char col_type,char* col_value, char* cond_needed, int  nb_elt,int col_given,char* ErrMess)
{
	char appliName[100]="mysql_update_col_val";
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int col_val_temp=0;
	int rescode=-1;
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	char mysql_stmt[1000];
	char ErrMess_temp[1000];
	
	if(col_given==0) //means we update the column to a real value
	{
	    if(col_type=='C') //means it is not a int
		{
			
				sprintf(mysql_stmt,"update %s.%s set %s='%s' where %s is NULL and %s",db_name,tab_name,col_name,col_value,col_name,cond_needed);
		}	   
		else //needs to convert char to int
		{
		  col_val_temp=atoi(col_value);
		  //printf("in update col_val_temp=%d \n",col_val_temp);
	      sprintf(mysql_stmt,"update %s set %s=%d where %s is NULL and %s",tab_name,col_name,col_val_temp,col_name,cond_needed);
		}
	}
	else
		sprintf(mysql_stmt,"update %s.%s set %s%s where %s is NULL and %s",db_name,tab_name,col_name,col_value,col_name,cond_needed);

	
	
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		sprintf(ErrMess_temp,"mysql_query() failed error :%s",mysql_error(conn_temp));
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
		
	}
	else
	{
		sprintf(ErrMess,"result_query : tabname=%s, col_name=%s, col_value=%d,cond_put=%s , nb_rows_matched=%ld",tab_name,col_name,col_val_temp,cond_needed,(long)mysql_affected_rows(conn_temp));
		WriteToLogFile(appliName,ErrMess );
		rescode=0;
	}

	return rescode;
}


int CheckMigration(MYSQL * conn_temp,char* db_name,int updated_needed,char* ErrMess)
{

	int i=0;
	int j=0;
	int pos1=0;
	int pos2=0;
	int pos1bis=0;
	int pos2bis=0;
	char col_value[255];
	char col_value_temp[255];
	char cond_value[500];
	char cond_value_temp[500];
	int rescode=-1;
	int nb_elt=-1;
	int update_required=0;
	char appliName[100]="CheckMigration";
	char ErrMess_temp[1000];
	if(updated_needed==1)
		rescode=mysql_autocommit(conn_temp, 1) ;
	while(i<NB_TAB_TO_CHECK)
	{
		while(strcmp(TABLE_COL[j],"END")!=0 && j<NB_COL_TO_CHECK)
		{
			rescode=MYSQL_check_col_val(conn_temp,db_name, TABLE_ADD_COL[i],TABLE_COL[j],"1=1",nb_elt, ErrMess_temp);
			update_required+=nb_elt;
			if(rescode!=0)
			{
				GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
				return rescode;
			}
			else
			{
				if(nb_elt!=0 && updated_needed==1)
				{
					pos1=strcspn(TABLE_COL_DEF_VALUE[j],"|");
					if(pos1==strlen(TABLE_COL_DEF_VALUE[j]))
					{
						if(TABLE_COL_DEF_VALUE[j][0]!=':')
							rescode=MYSQL_update_col_val(conn_temp,db_name, TABLE_ADD_COL[i],TABLE_COL[j],TABLE_COL_TYPE[j],TABLE_COL_DEF_VALUE[j], TABLE_COL_UP_COND[j], nb_elt, 0,ErrMess_temp);
						else
						{
							TABLE_COL_DEF_VALUE[j][0]='=';
							rescode=MYSQL_update_col_val(conn_temp,db_name, TABLE_ADD_COL[i],TABLE_COL[j],TABLE_COL_TYPE[j],TABLE_COL_DEF_VALUE[j], TABLE_COL_UP_COND[j], nb_elt, 1,ErrMess_temp);
						}
						if(rescode!=0)
						{
							GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
							return rescode;
						}
					}
					else
					{
						strcpy(col_value,TABLE_COL_DEF_VALUE[j]);
						strcpy(cond_value,TABLE_COL_UP_COND[j]);
						pos1bis=strcspn(cond_value,"|");
						while(pos2<strlen(col_value))
						{
							
							memcpy(col_value_temp,col_value+pos2,pos1+1);
							pos2+=pos1+1;
							memcpy(cond_value_temp,cond_value+pos2bis,pos1bis+1);
							pos2bis+=pos1bis+1;
							cond_value_temp[pos1bis]='\0';
							col_value_temp[pos1]='\0';
							//printf("col_value_temp=%s and cond_value_temp=%s \n",col_value_temp,cond_value_temp);
							if(col_value_temp[0]!=':')
								rescode=MYSQL_update_col_val(conn_temp,db_name, TABLE_ADD_COL[i],TABLE_COL[j],TABLE_COL_TYPE[j],col_value_temp, cond_value_temp, nb_elt,0, ErrMess_temp);
							else
							{	
								col_value_temp[0]='=';
								rescode=MYSQL_update_col_val(conn_temp,db_name, TABLE_ADD_COL[i],TABLE_COL[j],TABLE_COL_TYPE[j],col_value_temp, cond_value_temp, nb_elt,1, ErrMess_temp);
							}
							if(rescode!=0)
							{
								GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
								return rescode;
							}
							pos1bis=strcspn(cond_value+pos2bis,"|");
							pos1=strcspn(col_value+pos2,"|");
							
						}


					}
				}
			}
			j++;
		}
		if(strcmp(TABLE_COL[j],"END")==0)
		{
			i++;
			j++;
		}
		else
			i++;
	}
	if(update_required!=0 && updated_needed!=1 )
	  {
	    GetErrorMess(appliName,"The application needs to be updated ",ErrMess,1);
	  }
	rescode=0;
	return rescode;
}

int RecreateConfigFile(char* filename, char* ErrMess)
{
char appliName[100]="RecreateConfigFile";
FILE *f1;
int rescode=0;
f1 = fopen (filename, "w+t");  
char col_stmt[200];
if (f1==NULL)
{
	sprintf(col_stmt,"Could not open the config file  %s ",filename);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	
	rescode=-1;
	return rescode;
}

rescode=fprintf(f1,"mysql_host_name=%s;\nmysql_user_name=%s;\nmysql_port_nb=\nmysql_socket_path=\n",_mysql_host_name, _mysql_user_name,_mysql_db_name);
fclose (f1); 
GetErrorMess(appliName,"none",ErrMess,0);
return 0;
}
