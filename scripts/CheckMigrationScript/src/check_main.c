#include "check_migration.h"
#include<pwd.h>
#include <unistd.h> 
 extern char _mysql_user_name[100];

  extern char _mysql_host_name[100];
  extern int _mysql_port_nb;
 extern char _mysql_socket_path[500];
extern MYSQL* conn_handler;

int main(int argc, char* argv[])
{
	int rescode=0;
	char ErrMess[1000];
	char appliName[101]="check_main";
	char ErrMess_bis[1000];
	char config_file[31];
	char* mysql_pwd;
	int flags;
	int updated_needed=0;
	int update_required=0;
	sprintf(ErrMess,"Checking the update of the dpm upgrade version %s ",DPM_UPGRADE_VERSION);
	WriteToLogFile(appliName,  ErrMess);
	if (argc==2)
	{
		updated_needed=atoi(argv[1]);
		strcpy(ErrMess,"The application will check and update ");
		WriteToLogFile(appliName,  ErrMess);
	}
	

	mysql_pwd=(char*)malloc(101*sizeof(char));
	if(mysql_pwd==NULL)
	{
		strcpy(ErrMess,"Unsuccessful malloc ");
		WriteToLogFile(appliName,  ErrMess);
	}
mysql_pwd=getpass("Enter the DB password\n");

 

strcpy(config_file,"connexion_param.txt");
 
rescode=GetParamInfo(config_file, ErrMess);
 
if(rescode!=0)
{
	strcpy(appliName,"GetParamInfo");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess_bis);
	
	return -1;
}
else
	WriteToLogFile(appliName,  ErrMess);

rescode=MYSQL_DBConnect(_mysql_host_name,_mysql_user_name,mysql_pwd, _mysql_db_name,_mysql_port_nb, _mysql_socket_path,  flags,ErrMess);
if(rescode!=0)
{
	strcpy(appliName,"mysql_DBConnect");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess_bis);
	return -1;
}
else
	WriteToLogFile(appliName,  ErrMess);


rescode=CheckMigration(conn_handler,_mysql_db_name,updated_needed, ErrMess);
if(rescode!=0)
{
	strcpy(appliName,"CheckMigration");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess_bis);
	MYSQL_disconnect();
	return -1;
}
else
	WriteToLogFile(appliName,  ErrMess);
if(updated_needed!=1 && (strstr(ErrMess,"needs to be updated")!=NULL))
     printf("You need to launch the application in update mode: the upgrade was not successful \n");
     else
{
     if(updated_needed!=1)
     printf("the upgrade was successful : no need to updated \n");
}

printf("Application successfully finished \n");
MYSQL_disconnect();
return rescode;

}
