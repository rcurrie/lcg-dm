#include "list_tab.h"
#include <stdio.h>
#include <mysql.h>
#include <stdlib.h>
#include "getError.h"

int GetParamInfo(char* config_file,char* ErrMess);
int MYSQL_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess);
void MYSQL_disconnect();
int MYSQL_check_col_val(MYSQL * conn_temp,char* db_name, char* tab_name,char* col_name, char* cond_needed, int &nb_elt,char* ErrMess);
int MYSQL_update_col_val(MYSQL * conn_temp,char* db_name,  char* tab_name,char* col_name,char col_type,char* col_value, char* cond_needed, int  nb_elt,char* ErrMess);
int CheckMigration(MYSQL * conn_temp,char* db_name,int updated_needed,char* ErrMess);
int RecreateConfigFile(char* filename, char* ErrMess);



