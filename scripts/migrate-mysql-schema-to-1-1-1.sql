
--
-- For an Oracle database backend : 
-- script to migrate the schema from the first version to version 1.1.0 
--
-- Author : Sophie Lemaitre <Sophie.Lemaitre@cern.ch>
--

USE cns_db;

-- Add the "status" and "poolname" fields"

ALTER TABLE Cns_file_replica ADD ( status CHAR(1) );
ALTER TABLE Cns_file_replica ADD ( poolname VARCHAR(15) );
ALTER TABLE Cns_file_replica ADD ( fs VARCHAR(79) );
ALTER TABLE Cns_file_replica ADD ( f_type CHAR(1) );


-- Create an index on Cns_file_metadata(parent_fileid)

create index PARENT_FILEID_IDX on Cns_file_metadata(PARENT_FILEID);


-- Create the "schema_version" table

DROP TABLE schema_version;
CREATE TABLE schema_version (
  major INTEGER NOT NULL,
  minor INTEGER NOT NULL,
  patch INTEGER NOT NULL
) TYPE=INNODB;

INSERT INTO schema_version (major, minor, patch)
  VALUES (1, 1, 1);
