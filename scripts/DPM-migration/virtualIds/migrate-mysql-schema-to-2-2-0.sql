--
-- Scripts that migrates the Database Schema from 2.1.0 to 2.2.0
--
-- The 2.2.0 schema integrates :
--	- virtual uids/gids
--	- VOMS support
--

--
-- Note : this script should be called with "--database <dpm_name_server_db>" (the database name is "cns_db" is the default).
--

--
-- Create the new tables
--
CREATE TABLE Cns_groupinfo (
       rowid INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       gid INTEGER,
       groupname VARCHAR(255) BINARY)
      TYPE = InnoDB;

CREATE TABLE Cns_userinfo (
       rowid INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       userid INTEGER,
       username VARCHAR(255) BINARY)
      TYPE = InnoDB;

CREATE TABLE Cns_unique_gid (
       id INTEGER UNSIGNED)
      TYPE = InnoDB;

CREATE TABLE Cns_unique_uid (
       id INTEGER UNSIGNED)
       TYPE = InnoDB;

ALTER TABLE Cns_groupinfo
       ADD UNIQUE (groupname);
ALTER TABLE Cns_userinfo
       ADD UNIQUE (username);

--
-- Update the schema version
--
DROP TABLE IF EXISTS schema_version;
CREATE TABLE schema_version (
  major INTEGER NOT NULL,
  minor INTEGER NOT NULL,
  patch INTEGER NOT NULL
) TYPE=INNODB;

INSERT INTO schema_version (major, minor, patch)
  VALUES (2, 2, 0);


USE dpm_db;
DROP TABLE IF EXISTS schema_version;
CREATE TABLE schema_version (
  major INTEGER NOT NULL,
  minor INTEGER NOT NULL,
  patch INTEGER NOT NULL
) TYPE=INNODB;

INSERT INTO schema_version (major, minor, patch)
  VALUES (2, 2, 0);


--
-- Create possible missing indexes
-- 
CREATE INDEX P_U_DESC_IDX ON dpm_pending_req(u_token);
CREATE INDEX U_DESC_IDX ON dpm_req(u_token);
CREATE INDEX G_SURL_IDX ON dpm_get_filereq(FROM_SURL(255));
CREATE INDEX P_SURL_IDX ON dpm_put_filereq(TO_SURL(255));
CREATE INDEX CF_SURL_IDX ON dpm_copy_filereq(FROM_SURL(255));
CREATE INDEX CT_SURL_IDX ON dpm_copy_filereq(TO_SURL(255));
