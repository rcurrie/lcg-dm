--
-- Scripts that migrates the Database Schema from 2.1.0 to 2.2.0
--
-- The 2.2.0 schema integrates :
--	- virtual uids/gids
--	- VOMS support
--

--
-- Create the new tables
--
CREATE TABLE Cns_groupinfo (
       gid NUMBER(10),
       groupname VARCHAR2(255));

CREATE TABLE Cns_userinfo (
       userid NUMBER(10),
       username VARCHAR2(255));

CREATE TABLE Cns_unique_gid (
       id NUMBER(10));

CREATE TABLE Cns_unique_uid (
       id NUMBER(10));

ALTER TABLE Cns_groupinfo
       ADD CONSTRAINT map_groupname UNIQUE (groupname);
ALTER TABLE Cns_userinfo
       ADD CONSTRAINT map_username UNIQUE (username);

--
-- Add possible missing indexes
--
CREATE INDEX P_U_DESC_IDX ON dpm_pending_req(u_token);
CREATE INDEX U_DESC_IDX ON dpm_req(u_token);
CREATE INDEX G_SURL_IDX ON dpm_get_filereq(FROM_SURL);
CREATE INDEX P_SURL_IDX ON dpm_put_filereq(TO_SURL);
CREATE INDEX CF_SURL_IDX ON dpm_copy_filereq(FROM_SURL);
CREATE INDEX CT_SURL_IDX ON dpm_copy_filereq(TO_SURL);

--
-- Update the schema version
--
UPDATE schema_version SET major=2, minor=2, patch=0;
