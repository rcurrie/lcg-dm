#!/bin/bash

#
# Script modifying the CASTOR Name Server man pages for the LFC commands
# 
# Author : Sophie Lemaitre <Sophie.Lemaitre@cern.ch>
#

#
# Deduct the type of module (LFC, DPNS, RFIO or COMMON)
#
case $1 in 
lfc*)
	MODULE="LFC"
	;;
dpns*)
	MODULE="DPNS"
	;;
rf*)
	MODULE="RFIO"
	;;
*)
	MODULE="COMMON"
	;;
esac

#
# Create the LFC man page 
#
if [ "$MODULE" = "LFC" ]; then 

case $2 in
Cns_*)
        NS_COMMAND_NAME=$2
        COMMAND_NAME=`echo ${NS_COMMAND_NAME##*Cns_}`   # remove the Cns_ part
        rm -f lfc_$COMMAND_NAME.man                     # remove the lfc_<command>.man file

	# To handle the ".so" man pages
        if [ `head -1 $2.man | cut -c1-3` == ".so" ]; then
        	ACTUAL_COMMAND_NAME=`head -1 $2.man | cut -c5-`;
        else
		ACTUAL_COMMAND_NAME="Cns_${COMMAND_NAME}.man";
	fi

        # Create the lfc_<command>.man man page by parsing the Cns_<command>.man file
        COMMAND_NAME_UPPER_CASE=`echo $COMMAND_NAME | tr 'a-z' 'A-Z'`
        sed -e 's/cns/lfc/g' -e 's/CNS/LFC/g' -e 's/Cns /LFC /g' -e 's/Cns_/lfc_/g' -e 's/CASTOR/LFC/g' -e 's/ns'$COMMAND_NAME'/lfc-'$COMMAND_NAME'/g' -e 's/NS'$COMMAND_NAME_UPPER_CASE'/LFC-'$COMMAND_NAME_UPPER_CASE'/g' -e 's/<castor.support@cern.ch>//g' -e 's/\/castor\/cern.ch\/user\/b\/baud\/d2/\/grid\/atlas\/test\/file.log/g' -e 's/LFC_ACL/CNS_ACL/g' -e 's/LFC_LIST_/CNS_LIST_/g' -e 's/lfc_stat(3)/lfc_statg(3)/g' -e 's/lfc_creat(3)/lfc_creatg(3)/g' -e 's/lfc_mkdir(3)/lfc_mkdirg(3)/g' -e 's/lfc_opendir(3)/lfc_opendirg(3)/g' -e 's/lfc_readdir(3)/lfc_readdir(3),\\ lfc_readdirg(3)/g' -e 's/ELFC_NACT/ENSNACT/g' -e 's/\\fBLFC\\fP Team/\\fBLCG Grid Deployment\\fP Team/g' $ACTUAL_COMMAND_NAME | nroff -man -i > $3/lfc_$COMMAND_NAME.$4

        ;;

ns*)
        NS_COMMAND_NAME=$2
        COMMAND_NAME=`echo ${NS_COMMAND_NAME##*ns}`     # remove the Cns_ part
        rm -f lfc-$COMMAND_NAME.man                     # remove the lfc-<command>.man file if it already exist

        # To handle the ".so" man pages
        if [ `head -1 $2.man | cut -c1-3` == ".so" ]; then
                ACTUAL_COMMAND_NAME=`head -1 $2.man | cut -c5-`;
        else
                ACTUAL_COMMAND_NAME="ns${COMMAND_NAME}.man";
        fi

        # Create the lfc-<command>.man man page by parsing the ns<command>.man file
        COMMAND_NAME_UPPER_CASE=`echo $COMMAND_NAME | tr 'a-z' 'A-Z'`
        sed -e 's/.B Cns_getsegattrs(3)//g' -e 's/.BR Cns_getcomment(3) ,//g' -e 's/cns/lfc/g' -e 's/CNS/LFC/g' -e 's/Cns /LFC /g' -e 's/Cns_/lfc_/g' -e 's/CASTOR/LFC/g' -e 's/ns'$COMMAND_NAME'/lfc-'$COMMAND_NAME'/g' -e 's/NS'$COMMAND_NAME_UPPER_CASE'/LFC-'$COMMAND_NAME_UPPER_CASE'/g' -e 's/<castor.support@cern.ch>//g' -e 's/\/castor\/cern.ch\/user\/b\/baud/\/grid\/atlas\/test\/file.log/g' -e 's/lfc-daemon/lfcdaemon/g' -e 's/LFC-DAEMON/LFCDAEMON/g' -e 's/lfc_srv/Cns_srv/g' -e 's/lfc_serv/Cns_serv/g' -e 's/lfc_constants/Cns_constants/g' -e 's/nsshutdown/lfc-shutdown/g' -e 's/\/usr\/spool\/ns/\/var\/log\/lfc/g' -e 's/\/castor/\/grid/g' -e 's/castor5/lxb1925/g' -e 's/\\fBLFC\\fP Team/\\fBLCG Grid Deployment\\fP Team/g' -e 's/\/etc\/NSCONFIG/\/opt\/lcg\/etc\/NSCONFIG/g' -e 's/nstouch/lfc-touch/g' -e 's/nsmkdir/lfc-mkdir/g' -e 's/nsgetacl/lfc-getacl/g' -e 's/nssetacl/lfc-setacl/g' -e 's/lfc_db/cns_db/g' $ACTUAL_COMMAND_NAME | nroff -man -i > $3/lfc-$COMMAND_NAME.$4

        # Rename the lfc-daemon.man file to lfcdaemon.man
        if [ $COMMAND_NAME = "daemon" ]; then
                `mv $3/lfc-daemon.$4 $3/lfcdaemon.$4`
        fi

        ;;

esac

fi


#
# Create the DPNS man page
#
if [ "$MODULE" = "DPNS" ]; then

case $2 in
Cns_*)
        NS_COMMAND_NAME=$2
        COMMAND_NAME=`echo ${NS_COMMAND_NAME##*Cns_}`   # remove the Cns_ part
        rm -f dpns_$COMMAND_NAME.man                    # remove the dpns_<command>.man file

        # To handle the ".so" man pages
        if [ `head -1 $2.man | cut -c1-3` == ".so" ]; then
                ACTUAL_COMMAND_NAME=`head -1 $2.man | cut -c5-`;
        else
                ACTUAL_COMMAND_NAME="Cns_${COMMAND_NAME}.man";
        fi

        # Create the dpns_<command>.man man page by parsing the Cns_<command>.man file
        COMMAND_NAME_UPPER_CASE=`echo $COMMAND_NAME | tr 'a-z' 'A-Z'`
        sed -e 's/cns/dpns/g' -e 's/CNS/DPNS/g' -e 's/Cns /DPNS /g' -e 's/Cns_/dpns_/g' -e 's/CASTOR/DPNS/g' -e 's/ns'$COMMAND_NAME'/dpns-'$COMMAND_NAME'/g' -e 's/NS'$COMMAND_NAME_UPPER_CASE'/DPNS-'$COMMAND_NAME_UPPER_CASE'/g' -e 's/<castor.support@cern.ch>//g' -e 's/\/castor\/cern.ch\/user\/b\/baud\/d2/\/dpm\/dteam\/test\/file.log/g' -e 's/DPNS_ACL/CNS_ACL/g' -e 's/DPNS_LIST_/CNS_LIST_/g' -e 's/dpns_stat(3)/dpns_stat(3),\\ dpns_statg(3)/g' -e 's/dpns_readdir(3)/dpns_readdir(3),\\ dpns_readdirx(3)/g' -e 's/opendirg/opendir/g' -e 's/mkdirg/mkdir/g' -e 's/creatg/creat/g' -e 's/readdirg/readdir/g' -e 's/EDPNS_NACT/ENSNACT/g' -e 's/\\fBDPNS\\fP Team/\\fBLCG Grid Deployment\\fP Team/g' $ACTUAL_COMMAND_NAME | nroff -man -i > $3/dpns_$COMMAND_NAME.$4

        ;;

ns*)
        NS_COMMAND_NAME=$2
        COMMAND_NAME=`echo ${NS_COMMAND_NAME##*ns}`     # remove the Cns_ part
        rm -f dpns-$COMMAND_NAME.man                    # remove the dpns-<command>.man file if it already exist

        # To handle the ".so" man pages
        if [ `head -1 $2.man | cut -c1-3` == ".so" ]; then
                ACTUAL_COMMAND_NAME=`head -1 $2.man | cut -c5-`;
        else
                ACTUAL_COMMAND_NAME="ns${COMMAND_NAME}.man";
        fi


        # Create the dpns-<command>.man man page by parsing the ns<command>.man file
        COMMAND_NAME_UPPER_CASE=`echo $COMMAND_NAME | tr 'a-z' 'A-Z'`
        sed -e 's/.B Cns_getsegattrs(3)//g' -e 's/cns/dpns/g' -e 's/CNS/DPNS/g' -e 's/Cns /DPNS /g' -e 's/Cns_/dpns_/g' -e 's/CASTOR/DPNS/g' -e 's/ns'$COMMAND_NAME'/dpns-'$COMMAND_NAME'/g' -e 's/NS'$COMMAND_NAME_UPPER_CASE'/DPNS-'$COMMAND_NAME_UPPER_CASE'/g' -e 's/<castor.support@cern.ch>//g' -e 's/\/castor\/cern.ch\/user\/b\/baud/\/dpm\/dteam\/test\/file.log/g' -e 's/dpns-daemon/dpnsdaemon/g' -e 's/DPNS-DAEMON/DPNSDAEMON/g' -e 's/dpns_srv/Cns_srv/g' -e 's/dpns_serv/Cns_serv/g' -e 's/dpns_constants/Cns_constants/g' -e 's/\/usr\/spool\/ns/\/var\/log\/dpns/g' -e 's/nsshutdown(1)/dpns-shutdown(1)/g' -e 's/castor5/lxb1925/g' -e 's/\/castor/\/dpm/g' -e 's/\\fBDPNS\\fP Team/\\fBLCG Grid Deployment\\fP Team/g'  -e 's/\/etc\/NSCONFIG/\/opt\/lcg\/etc\/NSCONFIG/g' -e 's/nstouch/dpns-touch/g' -e 's/nsmkdir/dpns-mkdir/g' -e 's/nsgetacl/dpns-getacl/g' -e 's/nssetacl/dpns-setacl/g' -e 's/dpns_db/cns_db/g' $ACTUAL_COMMAND_NAME | nroff -man -i > $3/dpns-$COMMAND_NAME.$4

        # Rename the dpns-daemon.man file to dpnsdaemon.man
        if [ $COMMAND_NAME = "daemon" ]; then
                `mv $3/dpns-daemon.$4 $3/dpnsdaemon.$4`
        fi

        ;;

esac

fi

#
# Create the RFIO man page
#
if [ "$MODULE" = "RFIO" ]; then

COMMAND_NAME=$2		

# To handle the ".so" man pages
if [ `head -1 $2.man | cut -c1-3` == ".so" ]; then
	ACTUAL_COMMAND_NAME=`head -1 $2.man | cut -c5-`;
else
	ACTUAL_COMMAND_NAME=$COMMAND_NAME.man;
fi

# Remove the Castor Team reference in the man page
sed -e 's/<castor.support@cern.ch>//g' -e 's/\\fBCASTOR\\fP Team/\\fBLCG Grid Deployment\\fP Team/g' -e 's/CASTOR/LCG/g' $ACTUAL_COMMAND_NAME | nroff -man -i > $3/$COMMAND_NAME.$4

fi

#
# Create the other man pages
#
if [ "$MODULE" = "COMMON" ]; then

COMMAND_NAME=$2         # remove the .man extension

# To handle the ".so" man pages
if [ `head -1 $2.man | cut -c1-3` == ".so" ]; then
	ACTUAL_COMMAND_NAME=`head -1 $2.man | cut -c5-`;
else
	ACTUAL_COMMAND_NAME=$COMMAND_NAME.man;
fi

# Remove the Castor Team reference in the man page
sed -e 's/<castor.support@cern.ch>//g' -e 's/\\fBCASTOR\\fP Team/\\fBLCG Grid Deployment\\fP Team/g' -e 's/CASTOR/LCG/g' -e 's/\\fBC\\fPASTOR/LCG/g' $ACTUAL_COMMAND_NAME | nroff -man -i > $3/$COMMAND_NAME.$4

fi


