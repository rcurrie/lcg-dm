/*
 * Copyright (C) 1999-2004 by CERN/IT/PDP/DM
 * All rights reserved
 */

/** Takes the path to a filename which has been extracted from the EDG
 ** RLS and constructs the appropriate directory structure in the LFC
 ** namespace, giving each directory a GUID.
 ** If the given pathname starts with /grid, it is assumed to be the
 ** full pathname and any argument given by the -vo option is ignored.
 ** UID and GID are set depending on the experiment specified in
 ** -vo. If none is chosen, ATLAS is the default.
 **/

/** Usage: 
 ** migrate_path path [-vo VO_NAME]
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <errno.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "lfc_api.h"
#include "serrno.h"

main(argc, argv)
int argc;
char **argv;
{
  char path[CA_MAXPATHLEN+1];
  char base[5];
  char guid[CA_MAXGUIDLEN+1];
  char vo[6];
  struct Cns_filestat statbuf;
  char *p;
  char *endp;
  int c;
  int errflg = 0;
  uuid_t uuid;
  char uidbuf[5];
  char gidbuf[6];
  uid_t uid;
  gid_t gid;

  if (argc < 2 || (argc > 2 && strcmp(argv[2], "-vo") != 0)) {
    printf("Usage: migrate_path PATHNAME [-vo VO_NAME]\n
                   where VO_NAME is one of 'atlas', 'cms', 'alice, 'lhcb'\n");
    exit(1);
  }

#if defined(_WIN32)
  if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
    fprintf (stderr, "WSAStartup unsuccessful\n");
    exit (SYERR);
  }
#endif

  /* set up directory name according to given arguments */
  sprintf(vo, "atlas");
  strncpy(base, argv[1], 5);
  printf("Base is %s\n", base);
  if (strcmp(base, "/grid") != 0) {
    if (argv[2]) {
      sprintf(vo, "%s", argv[3]);
      sprintf(path, "/grid/%s/%s", vo, argv[1]);
    }
    else {
      sprintf(path, "/grid/%s", argv[1]);
    }
  }
  else {
    printf("Full path has been given; ignoring -vo option\n");
    strcpy(path, argv[1]);
  }

  /* set gid and uid to correct values for this VO */
  if (!strcmp(vo, "alice")) {
    sprintf(gidbuf, "1395"); 
    sprintf(uidbuf, "10417");
  }
  else if (!strcmp(vo, "lhcb")) {
    sprintf(gidbuf, "1470");
    sprintf(uidbuf, "12238");
  }
  else if (!strcmp(vo, "cms")) {
    sprintf(gidbuf, "1399");
    sprintf(uidbuf, "11410");
  }
  else if (!strcmp(vo, "atlasbis")) {
    sprintf(gidbuf,"1307");
    sprintf(uidbuf,"10761");
  }
  else {
    //default is atlas
    sprintf(gidbuf, "1307");
    sprintf(uidbuf, "10761");
  }
  uid = strtol(uidbuf, NULL, 10);
  gid = strtol(gidbuf, NULL, 10);

  /* check if exists and if not, create with guid */
  if (lfc_stat (path, &statbuf) < 0) {
    if (serrno == ENOENT) {
      endp = strrchr (path, '/');
      p = endp;
      while (p > path) {
	*p = '\0';
	c = lfc_access (path, F_OK);
	if (c == 0) break;
	p = strrchr (path, '/');
      }
      while (p <= endp) {
	*p = '/';
	uuid_generate(uuid);
	uuid_unparse(uuid, guid);
	c = lfc_mkdirg (path, guid, 0777);
	if (c < 0 && serrno != EEXIST) {
	  fprintf (stderr, "cannot create %s: %s\n",
		   path, sstrerror(serrno));
	  errflg++;
	  break;
	}
	if (lfc_chown(path, uid, gid) < 0) {
	  fprintf(stderr, "Cannot chown %s: %s\n", path, sstrerror(serrno));
	}
	p += strlen (p);
      }
    } else {
      fprintf (stderr, "%s: %s\n", path, sstrerror(serrno));
      errflg++;
    }    
  }

#if defined(_WIN32)
  WSACleanup();
#endif

  if (errflg)
    exit (USERR);

  printf("Path %s successfully created\n", path); 
  exit (0);
}
