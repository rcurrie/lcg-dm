#
# Query the RLS, parse the Aliases and create the corresponding directories and files in the LFC
#

package QueryAndUpdate;

use DBI;

sub queryFromRLSAndUpdateLFC($$$$) {
    my ($dbh_rmc, $lrc_user, $rmc_user, $path) = @_;

    unless (defined($path)) {
	$path = "/";
    }

    my $select;
    my $count = 0;

    my ($guid_guid, $alias_alias, $pfn_pfn);
    $select= $dbh_rmc->prepare("
	SELECT grmc.guid_guid, alias_alias, pfn_pfn 
        FROM $rmc_user.guid grmc, $lrc_user.guid glrc, alias, $lrc_user.pfn
        WHERE grmc.guid_id = alias_gid
	AND grmc.alias_id = 524
        AND glrc.guid_id = pfn_gid
        AND grmc.guid_guid = glrc.guid_guid");
    $select->execute();
    while(($guid_guid, $alias_alias, $pfn_pfn) = $select->fetchrow_array()) {

    # if $guid_guid starts with "guid:",  remove this part
    if ($guid_guid =~ /guid:/) {
        $guid_guid =~ s/guid://;
    }

    if ($alias_alias !=~ /\//) {
    
	# create the guid (if it doesn't exist), the alias and the pfn
        print "./migrate_files -a $alias_alias -g $guid_guid -r $pfn_pfn -p $path\n";
        `./migrate_files -a $alias_alias -g $guid_guid -r $pfn_pfn -p $path`;

    } else {

        #parse the alias
	my $directory;
        ($directory, $alias_alias) = parseAlias($alias_alias);

        # create the directories where to put the file
        print "./migrate_path /grid/$path/$directory\n";
        `./migrate_path /grid/$path/$directory`;
        # create the guid (if it doesn't exist), the alias and the pfn
        print "./migrate_files -p /$path/$directory -a $alias_alias -g $guid_guid -r $pfn_pfn\n";
        `./migrate_files -p /$path/$directory -a $alias_alias -g $guid_guid -r $pfn_pfn`;

    }


    $count = $count+1;
    if ($count%1000==0) {
        $time = localtime();
        print "$time : $count entries migrated\n";
    }

}

    $select->finish;
    return $count;

} 


sub parseAlias($) {

    my ($alias_alias) = @_;
    my $directory, $index;

    print "alias before parsing = $alias_alias\n";

    # find the position of the last occurence of "/"
    $index = rindex($alias_alias, "/");
    
    $directory = substr($alias_alias, 0, $index);
    $alias_alias = substr($alias_alias, $index+1); 
    print "directory = $directory\nalias after parsing = $alias_alias\n";
    return ($directory, $alias_alias); 

}


1;
