----------------------------------------------
---- Author : Lana Abadie (lana.abadie@cern.ch)
---- ReadMe for the UpdateCnsTab PL/SQL package for LFC DB
---- PL/SQL is not case-sensitive
----------------------------------------------

1 Objectives of this package
-----------------------------
- allow updating the gid and acl attributes for all the files and directories located under a given directory path.
- in this readme, we take the example of the use case of LHCb.
- They want to set the gid of all the directories and files located under 'grid/lhcb/production' and under 'grid/lhcb/test' to 'lhcb/lcgprod'. As a consequence the acl needs to be updated too. 
- As there is a replication of the DB, there is a need to slow down the updates. hence the use of dbms_lock.sleep()

2 How to use this package
----------------------------
- Connect to the LFC database via sqlplus
- type the following commands
	variable res varchar2(3900);
	exec :res:=updatecnstab_pck.UpdateColCnsfileMetadata('grid/lhcb/production', 'lhcb/lcgprod', 'lhcb',20);
	print res;
- the input arguments are :
	1. root directory path: all the files and directories under it will be updated
	2. the new gid to be set, should be already in cns_groupinfo tables. it will update the group_obj (C) of the acl attribute.
	3. the new group which will be set in the group (D) of the acl.
	4. the max rows nb of rows per second which can be updated due to replication. By default it is 0 which means no replication. In the case of LHCb and the replication with CNAF, it is 20 rows/s.
- the output of the last command should be 'No Error: Successful update';

3 if the package needs to be recompiled or is unknown in the database
----------------------------------------------------------------------
- Connect to the LFC database via sqlplus
- go to file/open. Point to the location of where the pl/sql files are stored. First open the ora_updatecnstab_interface.sql. It should be successful. Otherwise correct the errors. then do the same thing for ora_updatecnstab.sql


4 Package content
-----------------
In this version, there are 9 functions

5 New changes
--------------
the maximun nb of rows which can updated is now a input parameter.
