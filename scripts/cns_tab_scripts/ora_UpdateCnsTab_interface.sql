create or replace package  updatecnstab_pck  AUTHID CURRENT_USER is
TYPE element_list IS TABLE OF NUMBER; 
function UpdateColCnsfileMetadata(root_name varchar2, new_groupname_gid varchar2, new_groupname_acl varchar2,repli_update_limit number default 0) return varchar2;
function GetGID(groupname varchar2) return number;
function GetAllDirectoriesUnder(root_fileid number) return element_list;
function GetAllFilesUnder(root_fileid number) return element_list;
function EncodeACLForDirectory( new_gid number, new_acl_gid number, fileid number) return varchar2;
function GetFileIDByTreeAddress(full_directory_path varchar2) return number;
function EncodeACLForFile( new_gid number, new_acl_gid number, fileid number) return varchar2;
function UpdateColCnsfileMetadataForDir(root_fileid number, new_gid number, new_gid_acl number,repli_update_limit number default 0) return varchar2;
function UpdateColCnsfileMetadataForFil(root_fileid number, new_gid number, new_gid_acl number,repli_update_limit number default 0) return varchar2;


end updatecnstab_pck;
/
