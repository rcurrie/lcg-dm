#
# Query the DPM database and updates the hostname
#

package UpdateDpmDatabase;

use DBI;
use Common;

#
# Update the 'Cns_file_replica' table
#
sub updateDpmCnsFileReplicaTable($$$) {
    my ($dbh_dpns, $old_disk_server, $new_disk_server) = @_;
    my $count = 0;

    my ($fileid, $host, $sfn);
    $select = $dbh_dpns->prepare("
        SELECT fileid, host, sfn
        FROM Cns_file_replica
        WHERE sfn LIKE '$old_disk_server:/\%'
	AND host = '$old_disk_server'");
    $select->execute();

    while (($fileid, $host, $sfn) = $select->fetchrow_array()) {

        my $new_sfn = $sfn;
        my $new_host = $host;

	$new_sfn =~ s/$old_disk_server/$new_disk_server/;
	$new_host =~ s/$old_disk_server/$new_disk_server/;

        Common::updateFieldInDpmWhereStringAndFileId($dbh_dpns, "Cns_file_replica", "host", $new_host, "host", $host, "fileid", $fileid);
        Common::updateFieldInDpmWhereStringAndFileId($dbh_dpns, "Cns_file_replica", "sfn", $new_sfn, "sfn", $sfn, "fileid", $fileid);

        $count = $count+1;
    }

    $select->finish;
    return $count;

}



#
# Update the 'dpm_fs' table
#
sub updateDpmFsTable($$$) {
    my ($dbh_dpm, $old_disk_server, $new_disk_server) = @_;
    my $count = 0;

    my ($server, $new_server);
    $select = $dbh_dpm->prepare("
        SELECT server
        FROM dpm_fs
	WHERE server = '$old_disk_server'");
    $select->execute();

    while (($server) = $select->fetchrow_array()) {

	$new_server = $server;
        $new_server =~ s/$old_disk_server/$new_disk_server/;
        Common::updateFieldInDpmWhereString($dbh_dpm, "dpm_fs", "server", $new_server, "server", $server);

        $count = $count+1;
    }

    $select->finish;
    return $count;

}


#
# Update the 'dpm_copy_filereq' table
#
sub updateDpmCopyFileReqTable($$$) {
    my ($dbh_dpm, $old_srm, $new_srm) = @_;
    my $count = 0;

    my ($r_token, $f_ordinal, $from_surl, $to_surl);
    $select1 = $dbh_dpm->prepare("
        SELECT r_token, f_ordinal, from_surl
        FROM dpm_copy_filereq
        WHERE from_surl LIKE '\%$old_srm\%'");
    $select1->execute();

    while (($r_token, $f_ordinal, $from_surl) = $select1->fetchrow_array()) {

        my $new_from_surl = $from_surl;
        $new_from_surl =~ s/$old_srm/$new_srm/;

        Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_copy_filereq", "from_surl",
			$new_from_surl, "from_surl", $from_surl, "r_token", $r_token, "f_ordinal", $f_ordinal);

        $count = $count+1;
    }

    $select2 = $dbh_dpm->prepare("
        SELECT r_token, f_ordinal, to_surl
        FROM dpm_copy_filereq
        WHERE to_surl LIKE '\%$old_srm\%'");
    $select2->execute();

    while (($r_token, $f_ordinal, $to_surl) = $select2->fetchrow_array()) {

        my $new_to_surl = $to_surl;
        $new_to_surl =~ s/$old_srm/$new_srm/;

        Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_copy_filereq", "to_surl",
				$new_to_surl, "to_surl", $to_surl, "r_token", $r_token, "f_ordinal", $f_ordinal);

        $count = $count+1;
    }

    $select1->finish;
    $select2->finish;
    return $count;


}


#
# Update the 'dpm_get_filereq' table
#
sub updateDpmGetFileReqTable($$$$$) {
    my ($dbh_dpm, $old_srm, $new_srm, $old_disk_server, $new_disk_server) = @_;
    my $count = 0;

    my ($r_token, $f_ordinal, $from_surl, $server, $pfn);
    $select = $dbh_dpm->prepare("
        SELECT r_token, f_ordinal, from_surl, server, pfn
        FROM dpm_get_filereq
        WHERE from_surl LIKE '\%$old_srm\%'
	OR server = '$old_disk_server'
	OR pfn LIKE '\%$old_disk_server\%'");
    $select->execute();

    while (($r_token, $f_ordinal, $from_surl, $server, $pfn) = $select->fetchrow_array()) {

	if (defined($from_surl)) {
		my $new_from_surl = $from_surl;
        	$new_from_surl =~ s/$old_srm/$new_srm/;

        	Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_get_filereq", "from_surl", 
				$new_from_surl, "from_surl", $from_surl, "r_token", $r_token, "f_ordinal", $f_ordinal);
	}

	if (defined($server)) {
		my $new_server = $server;
		$new_server =~ s/$old_disk_server/$new_disk_server/;

	        Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_get_filereq", "server", 
				$new_server, "server", $server, "r_token", $r_token, "f_ordinal", $f_ordinal);
	}

        if (defined($pfn)) {
                my $new_pfn = $pfn;
                $new_pfn =~ s/$old_disk_server/$new_disk_server/;

                Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_get_filereq", "pfn", 
				$new_pfn, "pfn", $pfn, "r_token", $r_token, "f_ordinal", $f_ordinal);
        }


        $count = $count+1;
    }

    $select->finish;
    return $count;

}


#
# Update the 'dpm_put_filereq' table
#
sub updateDpmPutFileReqTable($$$$$) {
    my ($dbh_dpm, $old_srm, $new_srm, $old_disk_server, $new_disk_server) = @_;
    my $count = 0;

    my ($r_token, $f_ordinal, $to_surl, $server, $pfn);
    $select = $dbh_dpm->prepare("
        SELECT r_token, f_ordinal, to_surl, server, pfn
        FROM dpm_put_filereq
        WHERE to_surl LIKE '\%$old_srm\%'
	OR server = '$old_disk_server'
	OR pfn LIKE '\%$old_disk_server\%'");
    $select->execute();

    while (($r_token, $f_ordinal, $to_surl, $server, $pfn) = $select->fetchrow_array()) {

	if (defined($to_surl)) {
	        my $new_to_surl = $to_surl;
        	$new_to_surl =~ s/$old_srm/$new_srm/;
	
        	Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_put_filereq", "to_surl",
				$new_to_surl, "to_surl", $to_surl, "r_token", $r_token, "f_ordinal", $f_ordinal);
	}

        if (defined($server)) {
                my $new_server = $server;
                $new_server =~ s/$old_disk_server/$new_disk_server/;

                Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_put_filereq", "server",
				$new_server, "server", $server, "r_token", $r_token, "f_ordinal", $f_ordinal);
        }

        if (defined($pfn)) {
                my $new_pfn = $pfn;
                $new_pfn =~ s/$old_disk_server/$new_disk_server/;

                Common::updateFieldInDpmWhereTwoStringsAndOneNumber($dbh_dpm, "dpm_put_filereq", "pfn",
				$new_pfn, "pfn", $pfn, "r_token", $r_token, "f_ordinal", $f_ordinal);
        }


        $count = $count+1;
    }

    $select->finish;
    return $count;


}


1;
