#!/usr/bin/perl -w
###########################################################
#
# update-LFC-after-SE-rename
# 	-> rename old entries after storage element upgrade
#	   (only works for Oracle !)
#
# authors:    James Casey <james.casey@cern.ch>, 
#             Sophie Lemaitre <sophie.lemaitre@cern.ch>
# date:       01/03/2006
#
###########################################################

use strict;
use warnings;
use Getopt::Long;
use DBI;
use Env qw(ORACLE_HOME);

# JC - the default NLS_LANG set for CERN AFS install via setoraenv sets
# the NLS_LANG to a value for proddb, which doesn't work on pdb01
# We undef it here.
use Env qw(NLS_LANG);
undef $NLS_LANG;

sub connectToDatabase($$$) {
    my ($db, $user, $pwd) = @_;
        return DBI->connect("DBI:Oracle:$db", $user, $pwd, { RaiseError
=> 1, PrintError => 0, AutoCommit => 0 })
        || die "Can't open database $db:$user: $DBI::errstr\n";
}

sub usage ($) {
    my $reason = shift(@_);
    print << "EOF" and die "\nWrong usage of the script: $reason\n";
usage: $0 --db db_sid --user lfc_username --pwd lfc_password --oldSE
old_SE_name --newSE new_SE_name [--dummy]

 db            The db name as it is in tnsnames.ora
 user          The name of the user
 pwd           The password given to the user
 oldSE         The old full hostname of the Storage Element
 newSE         The new full hostname of the Storage Element
 oldPath       The old path, ex: "sfn://old-se.cern.ch/my/old/path"
 newPath       The new path, ex: "srm://new-se.cern.ch/my/new/path"
 dummy         run in dummy mode - don't do anything - just print the
new SQL
EOF
}

# Create arguments variables...
my ($db, $user, $pwd, $oldSE, $newSE, $oldPath, $newPath, $dummy);

# ... and read the arguments
GetOptions("db:s", => \$db,
           "user:s", => \$user,
           "pwd:s", => \$pwd,
           "oldSE:s", => \$oldSE,
           "newSE:s", => \$newSE,
	   "oldPath:s", => \$oldPath,
	   "newPath:s", => \$newPath,
           "dummy", => \$dummy);

# Check CLI consistency
usage("The database Oracle SID must be specified.\n") unless(defined
$db);
usage("The username must be specified.\n") unless(defined $user);
usage("The password must be specified.\n") unless(defined $pwd);
usage("The old Storage Element full hostname must be specified.\n")
unless(defined $oldSE);
usage("The new Storage Element full hostname must be specified.\n")
unless(defined $newSE);
usage("The old path must be specified.\n")
unless(defined $oldPath);
usage("The new path must be specified.\n")
unless(defined $newPath);


#eval {

#
# Check ORACLE_HOME is defined
#
if (!defined($ORACLE_HOME) ) {
        print STDERR "Error: ORACLE_HOME is not set! Check your Oracle
installation.\n";
        exit(1);
}

my @drivers = DBI->available_drivers;
if ((my $result = grep  /Oracle/ , @drivers) == 0){
        print STDERR "Error: Oracle DBD Module is not installed.\n";
        exit(1);
}


# Connect to the database.
my $dbh = connectToDatabase($db, $user, $pwd);


#
# do all replicas containing the old SE full host name :
#
my $ah = $dbh->prepare("SELECT fileid, host, sfn FROM Cns_file_replica WHERE sfn
LIKE '\%$oldPath\%' AND host = '$oldSE'");
$ah->execute();

DBI::trace(1);
my $rowCount=0;
while (my $ref = $ah->fetchrow_hashref()) {
    # oracle has the nice habit of capitalizing column names..
    my $sfn = $ref->{'SFN'};
    my $host = $ref->{'HOST'};
    my $fileid = $ref->{'FILEID'};

    my $new_sfn = $sfn;
    my $new_host = $host;

    $new_sfn =~ s/$oldPath/$newPath/;
    $new_host =~ s/$oldSE/$newSE/;

    my $sql1 = "UPDATE Cns_file_replica SET host='$new_host' WHERE fileid=$fileid and sfn='$sfn'";
    my $sql2 = "UPDATE Cns_file_replica SET sfn='$new_sfn' WHERE fileid=$fileid and sfn='$sfn'";

    if($dummy) {
        print "SQL : $sql1\n";
        print "SQL : $sql2\n";
    } else {
        my $check = $dbh->prepare("SELECT fileid from Cns_file_replica where sfn='$new_sfn'");
        $check->execute();
        if (my $ref2 = $check->fetchrow_hashref()) {
                print "Duplicate : $new_sfn\n";
		print "Removing $sfn\n";
		$dbh->do("DELETE FROM Cns_file_replica where sfn='$sfn' and fileid=$fileid");
        } else {
                $dbh->do($sql1);
                $dbh->do($sql2);
        }
    }
    $rowCount++;
}
$ah->finish();
$dbh->commit;

# Disconnect from the database.
$dbh->disconnect();

if($dummy) {
    print "Dummy run completed. $rowCount rows to change.\n";
} else {
    print "Update complete. $rowCount rows changed.\n";
}

#};
#die ("Failed to replace $oldSE with $newSE in the LFC database.");

