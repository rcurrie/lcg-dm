--PL/SQL written L.ABADIE (IT/GD) on 23 november 2007 v1.0 : allows to update the host column and or the sfn column
-- v.1.1  handle the case where the user has already registered the file with the new SE: it deletes the old one and keeps the new one

create or replace function update_host_name(host_to_replace varchar2,host_name_replacing varchar2) return varchar2 AUTHID CURRENT_USER is
i number:=0;
max_rows_to_update number:=8000;
rescode varchar2(1000);
nb_updated number:=1;
begin
while (nb_updated>0) loop
	execute immediate 'update cns_file_replica set host=:1 where host=:2 and rownum<:3' using host_name_replacing,host_to_replace,max_rows_to_update;
	nb_updated:=SQL%ROWCOUNT;
	i:=i+nb_updated;
	commit;
end loop;

commit;
dbms_output.put_line('value of rows_updated for the host='||i);

rescode:='SUCCESSFUL_UPDATE';
return rescode;
exception
	when NO_DATA_FOUND then
rescode:='ERROR : NO host MATCHES the given elt_to replace' ;
			return rescode;
		when OTHERS then
rescode:='ERROR : '||sqlerrm;
			return rescode;
end update_host_name;
/

create or replace function update_sfn(sfn_prefix_to_replace varchar2,sfn_prefix_replacing varchar2, host_cond varchar2 default NULL) return varchar2 AUTHID CURRENT_USER is
j number:=0;
max_rows_to_update number:=8000;
rescode varchar2(1000);
nb_updated number:=1;
elt_to_replace varchar2(4000);
begin
elt_to_replace:=sfn_prefix_to_replace||'%';

while (nb_updated>0) loop
	if host_cond is NULL then
		execute immediate 'delete from cns_file_replica where sfn like :1 and replace(sfn,:2,:3) in (select sfn from cns_file_replica) and rownum<:4' using elt_to_replace, sfn_prefix_to_replace,sfn_prefix_replacing, max_rows_to_update;
	else
		execute immediate 'delete from cns_file_replica where sfn like :1 and replace(sfn,:2,:3) in (select sfn from cns_file_replica) and rownum<:4 and host=:5' using elt_to_replace, sfn_prefix_to_replace,sfn_prefix_replacing, max_rows_to_update, host_cond;
	end if;
	
	nb_updated:=SQL%ROWCOUNT;
	j:=j+nb_updated;
	commit;
end loop;
commit;
dbms_output.put_line('value of rows_deleted  for the sfn='||j);
nb_updated:=1;
while (nb_updated>0) loop
	begin
		if host_cond is NULL then
			execute immediate 'update cns_file_replica set sfn=replace(sfn,:1,:2) where sfn like :3 and rownum<:4' using sfn_prefix_to_replace,sfn_prefix_replacing,elt_to_replace, max_rows_to_update;
		else
			execute immediate 'update cns_file_replica set sfn=replace(sfn,:1,:2) where host=:3 and sfn like :4 and rownum<:5' using sfn_prefix_to_replace,sfn_prefix_replacing, host_cond, elt_to_replace, max_rows_to_update;	
		end if;
		exception
			when DUP_VAL_ON_INDEX then
				execute immediate 'delete from cns_file_replica where sfn like :1 and replace(sfn,:2,:3) in (select sfn from cns_file_replica) and rownum<:4' using elt_to_replace, sfn_prefix_to_replace,sfn_prefix_replacing, max_rows_to_update;
		commit;
	end;
	nb_updated:=SQL%ROWCOUNT;
	j:=j+nb_updated;
	--commit;
end loop;
commit;
dbms_output.put_line('value of rows_updated  for the sfn='||j);

rescode:='SUCCESSFUL_UPDATE';
return rescode;
exception
		when NO_DATA_FOUND then
rescode:='ERROR : NO SFN MATCHES the given elt_to replace' ;
			return rescode;
		when OTHERS then
rescode:='ERROR : '||sqlerrm;
			return rescode;
end update_sfn;
/

