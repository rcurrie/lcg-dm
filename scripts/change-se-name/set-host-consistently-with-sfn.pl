#!/usr/bin/perl -w
###############################################################
#
# set-host-consistently-with-sfn
# 	-> set the 'host' field, consistently with the hostname 
#          appearing in 'sfn' (only works for Oracle !)
#
# authors:    Sophie Lemaitre <sophie.lemaitre@cern.ch>
# date:       24/07/2006
#
###############################################################

use strict;
use warnings;
use Getopt::Long;
use DBI;
use Env qw(ORACLE_HOME);

# JC - the default NLS_LANG set for CERN AFS install via setoraenv sets
# the NLS_LANG to a value for proddb, which doesn't work on pdb01
# We undef it here.
use Env qw(NLS_LANG);
undef $NLS_LANG;

sub connectToDatabase($$$) {
    my ($db, $user, $pwd) = @_;
        return DBI->connect("DBI:Oracle:$db", $user, $pwd, { RaiseError
=> 1, PrintError => 0, AutoCommit => 0 })
        || die "Can't open database $db:$user: $DBI::errstr\n";
}

sub usage ($) {
    my $reason = shift(@_);
    print << "EOF" and die "\nWrong usage of the script: $reason\n";
usage: $0 --db db_sid --user lfc_username --pwd-file password_file --SE se [--dummy]

 db            The db name as it is in tnsnames.ora
 user          The name of the user
 pwd-file      The file containing the user password
 SE            The hostname in 'host'
 dummy         run in dummy mode - don't do anything - just print the
new SQL
EOF
}

sub parseSfn($) {

    my ($sfn) = @_;
    my ($host, $index);

    $sfn =~ s/sfn:\/\///;	# for sfn protocol
    $sfn =~ s/srm:\/\///;	# for srm protocol

    # find the position of the first occurence of "/"
    $index = index($sfn, "/");

    $host = substr($sfn, 0, $index);
    return $host;

}

# Create arguments variables...
my ($db, $user, $pwd_file, $pwd, $SE, $dummy);

# ... and read the arguments
GetOptions("db:s", => \$db,
           "user:s", => \$user,
           "pwd-file:s", => \$pwd_file,
           "SE:s", => \$SE,
           "dummy", => \$dummy);

# Check CLI consistency
usage("The database Oracle SID must be specified.\n") unless(defined $db);
usage("The username must be specified.\n") unless(defined $user);
usage("The file containing the user password must be specified.\n") unless(defined $pwd_file);
usage("The Storage Element full hostname must be specified.\n") unless(defined $SE);

#eval {

#
# Check ORACLE_HOME is defined
#
if (!defined($ORACLE_HOME) ) {
        print STDERR "Error: ORACLE_HOME is not set! Check your Oracle
installation.\n";
        exit(1);
}

my @drivers = DBI->available_drivers;
if ((my $result = grep  /Oracle/ , @drivers) == 0){
        print STDERR "Error: Oracle DBD Module is not installed.\n";
        exit(1);
}

#
# read database password from file
#
open(FILE, $pwd_file) or die("Unable to open password file");
my @data = <FILE>;
$pwd = $data[0];
$pwd =~ s/\n//;
close(FILE);


# Connect to the database.
my $dbh = connectToDatabase($db, $user, $pwd);


#
# replace the given 'host' with the correct value
#
my $ah = $dbh->prepare("SELECT fileid, host, sfn FROM Cns_file_replica WHERE host = '$SE'");
$ah->execute();

DBI::trace(1);
my $rowCount=0;
while (my $ref = $ah->fetchrow_hashref()) {
    # oracle has the nice habit of capitalizing column names..
    my $sfn = $ref->{'SFN'};
    my $host = $ref->{'HOST'};
    my $fileid = $ref->{'FILEID'};

    my $new_host = parseSfn($sfn);

    if ($new_host eq $SE) {
	# ignore
    } else {
	# change 'host' field
	my $sql1 = "UPDATE Cns_file_replica SET host='$new_host' WHERE fileid=$fileid and sfn='$sfn'";

	if($dummy) {
		print "SQL : $sql1\n";
	} else {
		$dbh->do($sql1);
	}
	$rowCount++;
    }
}
$ah->finish();
$dbh->commit;

# Disconnect from the database.
$dbh->disconnect();

if($dummy) {
    print "Dummy run completed. $rowCount rows to change.\n";
} else {
    print "Update complete. $rowCount rows changed.\n";
}


#};
#die ("Failed to replace $SE in the LFC database.");
