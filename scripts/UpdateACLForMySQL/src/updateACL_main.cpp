/********************************************************************************/
// Author: L.Abadie
// version:v1.0
/********************************************************************************/
#include "updateACL_core.h"
#define PATCH_UPDATE_ACL_VERSION 1.1


extern char _mysql_db_name[100];
 extern char _mysql_user_name[100];
 extern char _mysql_pwd[100];
 extern char _mysql_host_name[100];
 extern int _mysql_port_nb;
 extern char _mysql_socket_path[500];

 extern char _new_gid_entry[500];
 extern char _new_acl_gid[500];
 extern char _new_directory_path[3000];
 extern MYSQL* conn_handler;
int main()
{
char config_file[20];
int status=0;
int flags=0;
int rescode=0;
int rescode1=0;
  int root_owner_uid=0;
  int root_gid=0;
char ErrMess[1000];
char ErrMess_bis[1000];
char appliName[100];
int root_fileid=0;
int new_gid_val=0;
int new_acl_gid_val=0;
  std::cout<<"Updating the ACL for both files and directory : "<<PATCH_UPDATE_ACL_VERSION<<std::endl;

//get info about the conn for oracle and mysql
strcpy(config_file,"config_file.dat");
rescode=GetParamInfo(config_file, ErrMess);
  strcpy(appliName,"GetParamInfo");
if(rescode!=0)
{
	
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess);

	std::cout<<ErrMess_bis<<std::endl;
	return -1;
}
else
	WriteToLogFile(appliName,  ErrMess);



rescode1=mysql_DBConnect(_mysql_host_name,_mysql_user_name,_mysql_pwd, _mysql_db_name,_mysql_port_nb, _mysql_socket_path, flags, ErrMess);

if(rescode1!=0 )
{
	strcpy(appliName,"mysql_DBConnect");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess);
	return -1;
	//std::cout<<ErrMess_bis<<std::endl;
}
	
WriteToLogFile("main", "Connected to  MySQL server successfully");



rescode=GetFileID_ByPath( conn_handler,_mysql_db_name,_new_directory_path,root_fileid,root_owner_uid,root_gid, ErrMess);
if(rescode!=0)
{
	sprintf(ErrMess_bis,"Getting the root_fileid for the path %s failed, errno=%d : %s ",_new_directory_path,rescode,ErrMess);
	std::cout<<ErrMess_bis<<std::endl;
	WriteToLogFile("main", ErrMess_bis);
	mysql_disconnect();
	return -1;
}
else
{
	sprintf(ErrMess_bis,"Getting the root_fileid  for %s was successful and is equal to %d ",_new_directory_path, root_fileid);
	WriteToLogFile("main", ErrMess_bis);
}
//std::cout<<"after get the fileid rootfile_id="<<root_fileid<<" and owner="<<root_owner_uid<<std::endl;
rescode=mysql_autocommit(conn_handler, 0) ;
if(rescode!=0)
{
 	sprintf(ErrMess_bis,"Could not disable the autocommit mode",ErrMess);
	std::cout<<ErrMess_bis<<std::endl;
	WriteToLogFile("main", ErrMess_bis);
	mysql_disconnect();
	return rescode;

}

rescode=mysql_GetGID(conn_handler, _mysql_db_name,_new_acl_gid,new_acl_gid_val,ErrMess);
if(rescode!=0)
{
 	sprintf(ErrMess_bis,"the given new_gid_acl doe not exist",ErrMess);
	std::cout<<ErrMess_bis<<std::endl;
	WriteToLogFile("main", ErrMess_bis);
	mysql_disconnect();
	return rescode;

}


if(strncmp(_new_gid_entry,"none",4)==0)
{

rescode=GetAllDirectoriesAndFilesUnder_acl(conn_handler,_mysql_db_name, root_fileid, root_gid,new_acl_gid_val,  root_owner_uid, ErrMess);

  if(rescode!=0)
    {
	sprintf(ErrMess_bis,"Pb when updating the acl for path %s, errno=%d : %s ",_new_directory_path,rescode,ErrMess);
	std::cout<<ErrMess_bis<<std::endl;
	WriteToLogFile("main", ErrMess_bis);
	mysql_disconnect();
	return -1;
    }
  else
    {
	sprintf(ErrMess_bis,"Updating all the acl was successful",_new_directory_path, root_fileid);	
	std::cout<<ErrMess_bis<<std::endl;

	WriteToLogFile("main", ErrMess_bis);
	WriteToLogFile("main", "Application successfully terminated");
	
    }
}
else
{

  rescode=mysql_GetGID(conn_handler,_mysql_db_name,_new_gid_entry,new_gid_val,ErrMess);
  if(rescode!=0)
    {
 	sprintf(ErrMess_bis,"the given new_gid_entry doe not exist",ErrMess);
	WriteToLogFile("main", ErrMess_bis);

	mysql_disconnect();
	std::cout<<ErrMess_bis<<std::endl;
	return rescode;

    }

  rescode=GetAllDirectoriesAndFilesUnder(conn_handler,_mysql_db_name, root_fileid, new_gid_val,new_acl_gid_val,  root_owner_uid, ErrMess);

  if(rescode!=0)
    {
	sprintf(ErrMess_bis,"Pb when updating the acl for path %s, errno=%d : %s ",_new_directory_path,rescode,ErrMess);
	std::cout<<ErrMess_bis<<std::endl;
	WriteToLogFile("main", ErrMess_bis);
	mysql_disconnect();
	return -1;
    }
  else
    {
	sprintf(ErrMess_bis,"Updating all the acl was successful",_new_directory_path, root_fileid);	
	std::cout<<ErrMess_bis<<std::endl;

	WriteToLogFile("main", ErrMess_bis);
	WriteToLogFile("main", "Application successfully terminated");
	
    }
}
mysql_disconnect();

return 0;

}
