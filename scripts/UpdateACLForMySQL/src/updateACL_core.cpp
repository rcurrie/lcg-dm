#include "updateACL_core.h"
MYSQL* conn_handler;

#define PARENT_FILEID_ROOT 2
#define ACL_TYPE_LEN 5000
#define MAX_ROWS_UPDATED 1000
 char _mysql_db_name[100];
  char _mysql_user_name[100];
 char _mysql_pwd[100];
  char _mysql_host_name[100];
 int _mysql_port_nb;
char _mysql_socket_path[500];

char _new_directory_path[3000];

 char _new_acl_gid[500];
char _new_gid_entry[500];

int GetParamInfo(char* config_file,char* ErrMess)
{
	char appliName[100]="GetParamInfo";
	char* s=(char*)malloc(2001*sizeof(char));
	FILE* f=NULL;
	double next=1;
	int stop=0;
	int pos1=0;
	int pos2=0;
	int rescode=0;
	if(s==NULL)
	{
		GetErrorMess(appliName,"MALLOC UNSUCCESSFUL",ErrMess,1);
		return -1;
	}

	if ((f=fopen( config_file,"r"))== NULL)
	{
		GetErrorMess(appliName,"Cannot open the config file",ErrMess,1);
		free(s);
		return -1;
	}
	else
	{
		
		while (fgets(s,1000,f)!=NULL )
		{
			

			if(strstr(s,"mysql_db_name=")!=NULL)
			{
				next=next*2;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
				s[pos1+pos2]='\0';
				strcpy(_mysql_db_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql db name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"mysql_user_name=")!=NULL)
			{
				next=next*3;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_user_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql user name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"mysql_pwd=")!=NULL)
			{
				next=next*5;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_pwd,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql pwd entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"mysql_host_name=")!=NULL)
			{
				next=next*7;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_host_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql host name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"mysql_port_nb=")!=NULL)
			{
				next=next*11;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					_mysql_port_nb=atoi(s+pos1+1);
				}
				
			}
			if(strstr(s,"mysql_socket_path=")!=NULL)
			{
				next=next*13;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_socket_path,s+pos1+1);
				}
			}
		
			if(strstr(s,"new_gid_acl=")!=NULL)
			{
				next=next*17;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_new_acl_gid,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The new_gid_acl entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"new_directory_path=")!=NULL)
			{
				next=next*19;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_new_directory_path,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The directory path entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			


		
	
		

		if(strstr(s,"new_gid_entry=")!=NULL)
			{
				next=next*23;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_new_gid_entry,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The new_gid_entry entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
		}
		fclose(f);
		free(s);
		if(next==223092870)
		{
			GetErrorMess(appliName,"No_ERROR",ErrMess,0);
			rescode=RecreateConfigFile( config_file,  ErrMess);
			rescode=0;

		}
		else
		{
			GetErrorMess(appliName,"Invalid config file, not all the entries were filled",ErrMess,1);
			rescode=-1;
		}

		return rescode;
		
	}
}

int mysql_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess)
{
	char appliName[100]="mysql_DBConnexion";
	
	char errmessage[500];
	conn_handler=mysql_init(NULL);
	if(conn_handler==NULL)
	{
		GetErrorMess(appliName,"mysql_init() failed ",ErrMess,1);
		return -1;
	}
	if(mysql_real_connect (conn_handler,host_name, user_name,password,db_name,port_num,socket_name,flags)==NULL)
	{
		sprintf(errmessage,"mysql_real_connect() failed. Error %u (%s)",mysql_errno(conn_handler),mysql_error(conn_handler));
		GetErrorMess(appliName,errmessage,ErrMess,1);
		return -1;
	}
	


	return 0;
}

void mysql_disconnect()
{
mysql_close(conn_handler);
}







int mysql_GetGID(MYSQL * conn_temp,char* db_name, char* group_name,int &gid,char* ErrMess)
{
	char appliName[100]="mysql_GetGID";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;
	char colname[31]="id";
	char mysql_stmt[1000];
	std::cout<<"value of groupname="<<group_name<<std::endl;
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"select gid from %s.Cns_groupinfo where groupname='%s'",db_name,group_name);
	else
		sprintf(mysql_stmt,"select gid from Cns_groupinfo where groupname='%s'",group_name);

	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		
	}
	else
		res_set=mysql_store_result(conn_temp);



	if(res_set==NULL)
	{
		GetErrorMess(appliName,"mysql_store_result() failed",ErrMess,1);
		return rescode;
	}
	if((row=mysql_fetch_row(res_set))!=NULL)
	{
		gid=atoi(row[0]);
		
		
	}
	else
	  gid=-1;

	if (gid>0)
	  rescode=0;

	std::cout<<"value of gid="<<gid<<std::endl;	
	
	return rescode;
}

int mysql_GetFileID(MYSQL * conn_temp,char* db_name, char* filename_part, int parent_fileid,int &fileid,int &owner_uid,int &gid_value,char* ErrMess)
{
	char appliName[100]="mysql_GetFileID";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;
	char colname[31]="id";
	char mysql_stmt[1000];
	//std::cout<<"parent="<<parent_fileid<<std::endl;
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"select fileid,owner_uid,gid from %s.Cns_file_metadata where parent_fileid=%d and name='%s'",db_name,parent_fileid,filename_part);
	else
		sprintf(mysql_stmt,"select fileid,owner_uid,gid from Cns_file_metadata where parent_fileid=%d and name='%s'",parent_fileid,filename_part);
	
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		//std::cout<<mysql_error(conn_temp)<<std::endl;
		
	}
	else
		res_set=mysql_store_result(conn_temp);
	//std::cout<<"stmt="<<mysql_stmt<<std::endl;


	if(res_set==NULL)
	{
		GetErrorMess(appliName,"mysql_store_result() failed",ErrMess,1);
		std::cout<<mysql_error(conn_temp)<<std::endl;
		return rescode;
	}
	if((row=mysql_fetch_row(res_set))!=NULL)
	{
	  
		fileid=atoi(row[0]);
		
		owner_uid=atoi(row[1]);
		gid_value=atoi(row[2]);
	}
	else
	  {
	    GetErrorMess(appliName,"no fileid found for the given directory",ErrMess,1);
		std::cout<<mysql_error(conn_temp)<<std::endl;
		return rescode;

	  }
	//std::cout<<"row="<<fileid<<std::endl;
	if (fileid>0)
		rescode=0;

	return rescode;
}

int GetFileID_ByPath(MYSQL * conn_temp,char* db_name,char* path_file,int &fileid, int & owner_uid,int & gid_value,char *ErrMess)
{

	int path_len=0;
	int rescode=0;
	char appliName[100]="GetFileID_ByPath";
	char  file_part[250];
	int parent_fileid=0;
	int pos2=0;
	int pos1=0;
	int i=0;
	//int owner_uid=0;
	int fileid_temp=0;
	char ErrMess_temp[1000];
	if(path_file==NULL)
	{
		GetErrorMess(appliName,"The path is NULL",ErrMess,1);
		rescode=-1;
		return rescode;
	}
	
	
	if(path_file[0]=='/')
	{
		pos1=strcspn(path_file+1,"/");
		strncpy(file_part,path_file+1,pos1);
	        file_part[pos1]='\0';
		pos1++;
	}
	else
	{
		pos1=strcspn(path_file,"/");
		strncpy(file_part,path_file,pos1);
		file_part[pos1]='\0';	
	}
	

	//std::cout<<"part 1="<<file_part<<std::endl;
	rescode=mysql_GetFileID(conn_temp,db_name,file_part, PARENT_FILEID_ROOT,fileid_temp,owner_uid,gid_value, ErrMess_temp);
	if(rescode!=0)
	{
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
		rescode=-1;
		return rescode;
	}
	path_len=strlen(path_file);
	if(file_part[path_len-1]=='/')
	{
		file_part[path_len-1]=='\0';
		path_len--;
	}
	//std::cout<<"path_len="<<path_len<<" and fileid_temp="<<fileid_temp<<std::endl;

	while(pos1<path_len  )
	{
		pos1++;
	
		i++;
		parent_fileid=fileid_temp;
		pos2=strcspn(path_file+pos1,"/");
		//std::cout<<"pos2="<<pos2<<"path_file+pos1="<<path_file+pos1<<std::endl;
		strncpy(file_part,path_file+pos1,pos2);
		file_part[pos2]='\0';
		pos1+=pos2;
		//std::cout<<"file_part ="<<file_part<<std::endl;
		rescode=mysql_GetFileID(conn_temp,db_name,file_part, parent_fileid,fileid_temp, owner_uid,gid_value,ErrMess_temp);
		//std::cout<<"rescode ="<<rescode<<std::endl;

		if(rescode!=0 || fileid_temp<1)
		{
			GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
		
			rescode=-1;
			return rescode;
		}
		
	}
	fileid=fileid_temp;
	//std::cout<<"fileid="<<fileid<<" and parent="<<parent_fileid<<std::endl;
	//rescode=-1;
	return rescode;
}

//new_gid=lcgadmin and new_gid_acl=atlas 
int EncodeACL(int directory_type, int new_gid,int new_gid_acl,int owner_uid,int fileid, char* new_ACL, int &len_acl, char* ErrMess)
{
	char appliName[100]="EncodeACLForDirectory";
	
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=0;
	
	
	char* acl_temp=NULL;

	
	acl_temp=(char*)malloc(ACL_TYPE_LEN*sizeof(char));
	if(acl_temp==NULL)
	{
		GetErrorMess(appliName,"Malloc unsuccessful for acl_temp",ErrMess,1);
		rescode=-1;
		return rescode;
	}
	if(directory_type==1)
		sprintf(acl_temp,"A7%d,C7%d,D7%d,E70,F50,a7%d,c7%d,d7%d,e70,f50",owner_uid,new_gid,new_gid_acl,owner_uid,new_gid,new_gid_acl);
	if(directory_type==0)
		sprintf(acl_temp,"A6%d,C6%d,D6%d,E70,F40",owner_uid,new_gid,new_gid_acl);

	if(strlen(acl_temp)<len_acl)
	{
		strcpy(new_ACL,acl_temp);
		len_acl=strlen(acl_temp)+1;
	}
	else	
	{
		len_acl=strlen(acl_temp)+1;
		GetErrorMess(appliName,"BUFFER_TOO_SMALL",ErrMess,1);
		rescode=-1;
	}
	//std::cout<<"acl_temp="<<acl_temp<<std::endl;
	free(acl_temp);
	return rescode;
}

int mysql_UpdateACL(MYSQL * conn_temp,char* db_name,int fileid, int direc_type,int new_gid,int new_gid_acl,int owner_uid,  char* ErrMess)
{
	char appliName[100]="mysql_UpdateACL";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int i=0;
	int rescode=-1;
	
	char mysql_stmt[1000];
	int num_rows=0;
	char* acl_temp=(char*)malloc(ACL_TYPE_LEN*sizeof(char));
	int len_acl=ACL_TYPE_LEN;
	char ErrMess_temp[1000];
	if(acl_temp==NULL)
	{
		GetErrorMess(appliName,"unsuccessful malloc for acl_Temp",ErrMess,1);
		return rescode;
	}

	rescode=EncodeACL(direc_type, new_gid,new_gid_acl, owner_uid, fileid, acl_temp, len_acl, ErrMess_temp);
	if(rescode!=0)
	{
		free(acl_temp);
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
		return rescode;

	}
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"update %s.Cns_file_metadata set gid=%d,acl='%s' where fileid=%d",db_name,new_gid,acl_temp,fileid);
	else
		sprintf(mysql_stmt,"update Cns_file_metadata set gid=%d,acl='%s' where fileid=%d ", new_gid,acl_temp,fileid);
	
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		rescode=mysql_query(conn_temp,mysql_stmt);
		std::cout<<"error in getting acl for direc="<<mysql_error(conn_temp)<<std::endl;

	}
	else
	{
	  //std::cout<<"error when updating acl="<<mysql_error(conn_temp)<<std::endl;
	  rescode=0;
	}
	if(rescode==0)
	  {
	    direc_type=0;
	    //std::cout<<"updating acl for files"<<std::endl;
	    rescode=EncodeACL(direc_type, new_gid,new_gid_acl,-1, fileid, acl_temp, len_acl, ErrMess_temp);
	    if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"update %s.Cns_file_metadata set gid=%d, acl=replace('%s','-1',owner_uid) where parent_fileid=%d and filemode between 32768 and 33279",db_name,new_gid,acl_temp,fileid);
	    if(mysql_query(conn_temp,mysql_stmt)!=0)
	      {
		GetErrorMess(appliName,"mysql_query() failed ",ErrMess,1);
		rescode=mysql_query(conn_temp,mysql_stmt);
		std::cout<<"error="<<mysql_error(conn_temp)<<std::endl;

	      }
	     rescode=mysql_commit(conn_temp);
       
	  }
	free(acl_temp);
	return rescode;

}

int mysql_GetFileID_Filemode(MYSQL * conn_temp,char* db_name,  int parent_fileid,int* fileid_list,int* owner_uid_list, int& fileid_list_len,char* ErrMess)
{
	char appliName[100]="mysql_GetFileID_Filemode";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int i=0;
	int rescode=-1;
	char colname[31]="id";
	char mysql_stmt[1000];
	int num_rows=0;
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"select fileid,owner_uid from %s.Cns_file_metadata where parent_fileid=%d  and filemode between 16384 and 16895",db_name,parent_fileid);
	else
		sprintf(mysql_stmt,"select fileid,owner_uid from Cns_file_metadata where parent_fileid=%d  and filemode between 16384 and 16895",parent_fileid);
	
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		
	}
	else
		res_set=mysql_store_result(conn_temp);



	if(res_set==NULL)
	{
		GetErrorMess(appliName,"mysql_store_result() failed",ErrMess,1);
		return rescode;
	}
	
	while((row=mysql_fetch_row(res_set))!=NULL && i <fileid_list_len)
	{
		
		fileid_list[i]=atoi(row[0]);
		owner_uid_list[i]=atoi(row[1]);
		i++;
		
	}
	//std::cout<<"in get fileid filemode i="<<i<<std::endl;
	if(i>fileid_list_len-1)
	{
		GetErrorMess(appliName,"BUFFER_TOO_SMALL",ErrMess,1);
		fileid_list_len=i;
		return rescode;
	}
	fileid_list_len=i;
	if (i>0)
		rescode=0;
	if (i==0)
	  	GetErrorMess(appliName,"NO_ROWS_SELECTED",ErrMess,1);
	
	return rescode;
}


void CopyVector(int * path_vector_new,int *path_vector_old,int path_vector_old_len, int index_start_new)
{
	int k;
	for(k=0;k<path_vector_old_len;k++)
	{
		path_vector_new[k+index_start_new]=path_vector_old[k];
	}
	

}

int GetAllDirectoriesAndFilesUnder(MYSQL * conn_temp,char* db_name,int root_fileid,int root_gid,int root_gid_acl,int root_owner_uid, char* ErrMess)
{
	char appliName[100]="GetAllDirectoriesAndFilesUnder";

	int rescode=0;
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	
	int* fileid_prev=NULL;

	int* owner_uid_prev=NULL;
	int list_len_prev=10000;

	int* fileid_new=NULL;

	int* owner_uid_new=NULL;
	int list_len_new=10000;

	int* fileid_temp=NULL;

	int* owner_uid_temp=NULL;
	int list_len_temp=10000;
	int direct_type=-1;
	//int direct_list_len=10000;
	int fileid=0;
	int filemode=0;
	int gid=0;
	int i=0;
	int j=0;
	int stop=0;
	int nb_update=1;
	char ErrMess_temp[1000];
	int nb_rows_processed=0;
	int parent_fileid=0;
	int nb_rows_processed_prev=0;
	int total_nb_rows_updated=0;
	//std::cout<<"before update acl"<<std::endl;
	rescode=mysql_UpdateACL(conn_temp, db_name,root_fileid,1,root_gid,root_gid_acl,root_owner_uid,ErrMess_temp);
	rescode=mysql_commit(conn_temp);
	std::cout<<"after update acl and rescode="<<rescode<<std::endl;
	if(rescode!=0)
	  {
	    GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
	    rescode=-1;
	    return rescode;
	  }
	fileid_prev=(int*)realloc(fileid_prev,list_len_prev*sizeof(int));
	if(fileid_prev==NULL)
	{
		
		GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
		rescode=-1;
		return rescode;
	}

	

	owner_uid_prev=(int*)realloc(owner_uid_prev,list_len_prev*sizeof(int));
	if(owner_uid_prev==NULL)
	{
		
		if(fileid_prev!=NULL)
			free(fileid_prev);
       
		GetErrorMess(appliName,"Malloc unsuccessful for owner_uid_prev",ErrMess,1);
		rescode=-1;
		return rescode;
	}
	//std::cout<<"memmory for prev allocated"<<std::endl;

	fileid_new=(int*)realloc(fileid_new,list_len_new*sizeof(int));
	if(fileid_new==NULL)
	{
		if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		if(fileid_prev!=NULL)
			free(fileid_prev);
	
		GetErrorMess(appliName,"Malloc unsuccessful for fileid_new",ErrMess,1);
		rescode=-1;
		return rescode;
	}

	

	owner_uid_new=(int*)realloc(owner_uid_new,list_len_new*sizeof(int));
	if(owner_uid_new==NULL)
	{
		
		if(fileid_prev!=NULL)
			free(fileid_prev);
	
		if(fileid_new!=NULL)
			free(fileid_new);
		if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		
		GetErrorMess(appliName,"Malloc unsuccessful for owner_uid_new",ErrMess,1);
		rescode=-1;
		return rescode;
	}
	//std::cout<<"memmory for new allocated"<<std::endl;
	//initialization
	//std::cout<<"intialization root_fileid="<<root_fileid<<std::endl; 
	rescode=mysql_GetFileID_Filemode(conn_temp,db_name, root_fileid,fileid_prev,owner_uid_prev, list_len_prev,ErrMess_temp);


	if((strstr(ErrMess_temp,"BUFFER_TOO_SMALL"))!=NULL)
	{

		fileid_prev=(int*)realloc(fileid_prev,list_len_prev*sizeof(int));
		if(fileid_prev==NULL)
		{
			
		
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
		
			GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
			rescode=-1;
			return rescode;
		}

		

		owner_uid_prev=(int*)realloc(owner_uid_prev,list_len_prev*sizeof(int));
		if(owner_uid_prev==NULL)
		{
		
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_prev!=NULL)
			free(fileid_prev);
			GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
			rescode=-1;
			return rescode;
		}
		rescode=mysql_GetFileID_Filemode(conn_temp,db_name, root_fileid,fileid_prev,owner_uid_prev, list_len_prev,ErrMess_temp);
		if(rescode!=0)
		{
			
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_prev!=NULL)
			free(fileid_prev);
			GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
			rescode=-1;
			return rescode;
		}
	}
	else
	{
		if(rescode!=0)
		{
		
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
			
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_prev!=NULL)
			free(fileid_prev);
			GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
			rescode=-1;
			return rescode;

		}

	}
	std::cout<<"parent_fileid="<<fileid_prev[0]<<" and="<<list_len_prev<<std::endl;

	while(stop==0)
	{
		nb_rows_processed=0;
		for(i=0;i<list_len_prev;i++)
		{

			parent_fileid=fileid_prev[i];
			direct_type=-1;
			rescode=mysql_GetFileID_Filemode(conn_temp,db_name, parent_fileid,fileid_new,owner_uid_new, list_len_new,ErrMess_temp);
			//std::cout<<"value of fileid_new="<<fileid_new[0]<<" and rescode="<<rescode<<std::endl;
			if((strstr(ErrMess_temp,"BUFFER_TOO_SMALL"))!=NULL)
			{

				fileid_new=(int*)realloc(fileid_new,list_len_new*sizeof(int));
				if(fileid_new==NULL)
				{
			
				
					if(fileid_new!=NULL)
					free(fileid_new);
					if(owner_uid_prev!=NULL)
					free(owner_uid_prev);
				
					if(owner_uid_new!=NULL)
					free(owner_uid_new);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					
					GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
					rescode=-1;
					return rescode;
				}

				

				owner_uid_new=(int*)realloc(owner_uid_new,list_len_new*sizeof(int));
				if(owner_uid_new==NULL)
				{
					
					if(fileid_new!=NULL)
						free(fileid_new);
					if(owner_uid_prev!=NULL)
						free(owner_uid_prev);
				
					if(owner_uid_new!=NULL)
						free(owner_uid_new);
					if(fileid_prev!=NULL)
						free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
				
					GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
					rescode=-1;
					return rescode;
				}
				rescode=mysql_GetFileID_Filemode(conn_temp,db_name, parent_fileid,fileid_new,owner_uid_new, list_len_new,ErrMess_temp);
				//std::cout<<" after mysql_getfileid="<<rescode<<std::endl;
				if(rescode!=0)
				{
					
					if(fileid_new!=NULL)
						free(fileid_new);
					if(owner_uid_prev!=NULL)
						free(owner_uid_prev);
					
					if(owner_uid_new!=NULL)
						free(owner_uid_new);
					if(fileid_prev!=NULL)
						free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
				
					GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
					rescode=-1;
					return rescode;
				}
			}
			else
			{
			  //std::cout<<"Errmess="<<ErrMess_temp<<std::endl;
				if(rescode!=0 && strstr(ErrMess_temp,"NO_ROWS_SELECTED")==NULL)
				{
					
					if(fileid_new!=NULL)
					free(fileid_new);
					if(owner_uid_prev!=NULL)
					free(owner_uid_prev);
					
					if(owner_uid_new!=NULL)
					free(owner_uid_new);
					if(fileid_prev!=NULL)
					free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					
					GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
					rescode=-1;
					return rescode;
				}

			}
			//std::cout<<"nb_rows_processed="<<nb_rows_processed<<std::endl;
			nb_rows_processed_prev=nb_rows_processed;
			nb_rows_processed+=list_len_new;
			if(list_len_new>0)
			{
			  fileid_temp=(int*)realloc(fileid_temp,nb_rows_processed*sizeof(int));
			  
			  owner_uid_temp=(int*)realloc(owner_uid_temp,nb_rows_processed*sizeof(int));
			  if(fileid_temp==NULL ||owner_uid_temp==NULL)
			    {

				
				if(fileid_new!=NULL)
				free(fileid_new);
				if(owner_uid_prev!=NULL)
				free(owner_uid_prev);
			
				if(owner_uid_new!=NULL)
				free(owner_uid_new);
				if(fileid_temp!=NULL)
				free(fileid_temp);
			
				if(owner_uid_temp!=NULL)
				free(owner_uid_temp);
				if(fileid_prev!=NULL)
				free(fileid_prev);
				GetErrorMess(appliName,"Unsuccessful malloc for the temp arrays",ErrMess,1);
				rescode=-1;
				return rescode;

			}
			CopyVector(fileid_temp,fileid_new,list_len_new, nb_rows_processed_prev);
		
			CopyVector(owner_uid_temp,owner_uid_new,list_len_new, nb_rows_processed_prev);
			}
			list_len_new=10000;
			direct_type=1;
			if(direct_type==1)
			{
				rescode=mysql_UpdateACL(conn_temp, db_name,parent_fileid,  direct_type,root_gid,root_gid_acl, owner_uid_prev[i],   ErrMess_temp);
				total_nb_rows_updated++;
				if(total_nb_rows_updated%MAX_ROWS_UPDATED==0)
					rescode=mysql_commit(conn_temp);
				if(rescode!=0)
				{
				  //std::cout<<"update acl="<<ErrMess_temp<<std::endl;
					
					if(fileid_new!=NULL)
					free(fileid_new);
					if(owner_uid_prev!=NULL)
					free(owner_uid_prev);
				
					if(owner_uid_new!=NULL)
					free(owner_uid_new);
					if(fileid_prev!=NULL)
					free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
				
					sprintf(ErrMess_temp,"Pb when committing the %d and errno=%d ",nb_rows_processed,rescode);
					GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
					rescode=-1;
					return rescode;
				}
			}
			
		}
		if(nb_rows_processed>0)
		{
		fileid_prev=(int*)realloc(fileid_prev,nb_rows_processed*sizeof(int));
		owner_uid_prev=(int*)realloc(owner_uid_prev,nb_rows_processed*sizeof(int));
		if(fileid_prev==NULL ||owner_uid_prev==NULL)
		{

		
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_temp!=NULL)
			free(fileid_temp);
		
			if(owner_uid_temp!=NULL)
			free(owner_uid_temp);
			if(fileid_prev!=NULL)
			free(fileid_prev);
			GetErrorMess(appliName,"Unsuccessful malloc for the prev arrays",ErrMess,1);
			rescode=-1;
			return rescode;

		}
		}
		list_len_prev=nb_rows_processed;
		if(list_len_prev==0)
			stop=1;
		else
		  {
		CopyVector(fileid_prev,fileid_temp, nb_rows_processed,0);
		CopyVector(owner_uid_prev,owner_uid_temp, nb_rows_processed,0);
		  }

	}
	std::cout<<"end of loop"<<std::endl;
	rescode=mysql_commit(conn_temp);

	if(fileid_new!=NULL)
	free(fileid_new);
	if(owner_uid_prev!=NULL)
	free(owner_uid_prev);

	if(owner_uid_new!=NULL)
	free(owner_uid_new);
	if(fileid_temp!=NULL)
	free(fileid_temp);

	if(owner_uid_temp!=NULL)
	free(owner_uid_temp);
	if(fileid_prev!=NULL)
	free(fileid_prev);
	
	return 0;

}

int RecreateConfigFile(char* filename, char* ErrMess)
{
char appliName[100]="RecreateConfigFile";
FILE *f1;
int rescode=0;
f1 = fopen (filename, "w+t");  
char col_stmt[200];
if (f1==NULL)
{
	sprintf(col_stmt,"Could not open the config file  %s ",filename);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	
	rescode=-1;
	return rescode;
}

rescode=fprintf(f1,"mysql_host_name=%s;\nmysql_user_name=%s;\nmysql_pwd=\nmysql_db_name=%s;\nmysql_port_nb=\nmysql_socket_path=\nnew_gid_acl=%s;\nnew_gid_entry=%s;\nnew_directory_path=%s;",_mysql_host_name, _mysql_user_name,_mysql_db_name,_new_acl_gid,_new_gid_entry,_new_directory_path);
fclose (f1); 
GetErrorMess(appliName,"none",ErrMess,0);
return 0;
}







int mysql_UpdateACL_only(MYSQL * conn_temp,char* db_name,int fileid, int direc_type,int new_gid,int new_acl_gid,int owner_uid,  char* ErrMess)
{
	char appliName[100]="mysql_UpdateACL_only";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int i=0;
	int rescode=-1;
	
	char mysql_stmt[1000];
	int num_rows=0;
	char* acl_temp=(char*)malloc(ACL_TYPE_LEN*sizeof(char));
	int len_acl=ACL_TYPE_LEN;
	char ErrMess_temp[1000];
	if(acl_temp==NULL)
	{
		GetErrorMess(appliName,"unsuccessful malloc for acl_Temp",ErrMess,1);
		return rescode;
	}

	rescode=EncodeACL(direc_type, new_gid,  new_acl_gid, owner_uid, fileid, acl_temp, len_acl, ErrMess_temp);
	if(rescode!=0)
	{
		free(acl_temp);
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
		return rescode;

	}
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"update %s.Cns_file_metadata set acl='%s' where fileid=%d",db_name,acl_temp,fileid);
	else
		sprintf(mysql_stmt,"update Cns_file_metadata set acl='%s' where fileid=%d ", acl_temp,fileid);
	
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		rescode=mysql_query(conn_temp,mysql_stmt);
		std::cout<<"eroror="<<mysql_error(conn_temp)<<std::endl;

	}
	else
	{
	  rescode=0;
		

	}

	if(rescode==0)
	  {
	    direc_type=0;
	    //std::cout<<"updating acl for files"<<std::endl;
	    rescode=EncodeACL(direc_type, -2,new_acl_gid,-1, fileid, acl_temp, len_acl, ErrMess_temp);
	    if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"update %s.Cns_file_metadata set acl=replace(replace('%s','-1',owner_uid),'-2',gid) where parent_fileid=%d and filemode between 32768 and 33279",db_name,acl_temp,fileid);
	    if(mysql_query(conn_temp,mysql_stmt)!=0)
	      {
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		rescode=mysql_query(conn_temp,mysql_stmt);
		std::cout<<"error="<<mysql_error(conn_temp)<<std::endl;

	      }
	     rescode=mysql_commit(conn_temp);
       
	  }
	return rescode;

}

int mysql_GetFileID_Filemode_GID(MYSQL * conn_temp,char* db_name,  int parent_fileid,int* fileid_list,int * gid_list,int* owner_uid_list, int& fileid_list_len,char* ErrMess)
{
	char appliName[100]="mysql_GetFileID_Filemode_GID";
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	int i=0;
	int rescode=-1;
	char colname[31]="id";
	char mysql_stmt[1000];
	int num_rows=0;
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"select fileid,gid,owner_uid from %s.Cns_file_metadata where parent_fileid=%d  and filemode between 16384 and 16895",db_name,parent_fileid);
	else
		sprintf(mysql_stmt,"select fileid,gid,owner_uid from Cns_file_metadata where parent_fileid=%d  and filemode between 16384 and 16895",parent_fileid);
	
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		
	}
	else
		res_set=mysql_store_result(conn_temp);



	if(res_set==NULL)
	{
		GetErrorMess(appliName,"mysql_store_result() failed",ErrMess,1);
		return rescode;
	}
	
	while((row=mysql_fetch_row(res_set))!=NULL && i <fileid_list_len)
	{
		
		fileid_list[i]=atoi(row[0]);
		gid_list[i]=atoi(row[1]);
		owner_uid_list[i]=atoi(row[2]);
		i++;
		
	}
	//std::cout<<"in get fileid filemode i="<<i<<std::endl;
	if(i>fileid_list_len-1)
	{
		GetErrorMess(appliName,"BUFFER_TOO_SMALL",ErrMess,1);
		fileid_list_len=i;
		return rescode;
	}
	fileid_list_len=i;
	if (i>0)
		rescode=0;
	if (i==0)
	  	GetErrorMess(appliName,"NO_ROWS_SELECTED",ErrMess,1);
	
	return rescode;
}



int GetAllDirectoriesAndFilesUnder_acl(MYSQL * conn_temp,char* db_name,int root_fileid,int root_gid,int root_owner_uid, int new_acl_gid, char* ErrMess)
{
	char appliName[100]="GetAllDirectoriesAndFilesUnder_acl";

	int rescode=0;
	MYSQL_RES *res_set;
	MYSQL_ROW row;
	
	int* fileid_prev=NULL;
	int* gid_prev=NULL;
	int* owner_uid_prev=NULL;
	int list_len_prev=10000;

	int* fileid_new=NULL;
	int* gid_new=NULL;
	int* owner_uid_new=NULL;
	int list_len_new=10000;

	int* fileid_temp=NULL;
	int* gid_temp=NULL;
	int* owner_uid_temp=NULL;
	int list_len_temp=10000;
	int direct_type=-1;
	//int direct_list_len=10000;
	int fileid=0;
	int filemode=0;
	int gid=0;
	int i=0;
	int j=0;
	int stop=0;
	int nb_update=1;
	char ErrMess_temp[1000];
	int nb_rows_processed=0;
	int parent_fileid=0;
	int nb_rows_processed_prev=0;
	int total_nb_rows_updated=0;
	//std::cout<<"before update acl"<<std::endl;
	rescode=mysql_UpdateACL_only(conn_temp, db_name,root_fileid,1,root_gid,new_acl_gid, root_owner_uid,   ErrMess_temp);
	rescode=mysql_commit(conn_temp);
	std::cout<<"after update acl and rescode="<<rescode<<std::endl;
	if(rescode!=0)
	  {
	    GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
	    rescode=-1;
	    return rescode;
	  }
	fileid_prev=(int*)realloc(fileid_prev,list_len_prev*sizeof(int));
	if(fileid_prev==NULL)
	{
		
		GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
		rescode=-1;
		return rescode;
	}

	gid_prev=(int*)realloc(gid_prev,list_len_prev*sizeof(int));
	if(gid_prev==NULL)
	{
		
		if(fileid_prev!=NULL)
			free(fileid_prev);
		GetErrorMess(appliName,"Malloc unsuccessful for filemode_prev",ErrMess,1);
		rescode=-1;
		return rescode;
	}

	owner_uid_prev=(int*)realloc(owner_uid_prev,list_len_prev*sizeof(int));
	if(owner_uid_prev==NULL)
	{
		
		if(fileid_prev!=NULL)
			free(fileid_prev);
		if(gid_prev!=NULL)
			free(gid_prev);
		GetErrorMess(appliName,"Malloc unsuccessful for owner_uid_prev",ErrMess,1);
		rescode=-1;
		return rescode;
	}
	//std::cout<<"memmory for prev allocated"<<std::endl;

	fileid_new=(int*)realloc(fileid_new,list_len_new*sizeof(int));
	if(fileid_new==NULL)
	{
		if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		if(fileid_prev!=NULL)
			free(fileid_prev);
		if(gid_prev!=NULL)
			free(gid_prev);
		GetErrorMess(appliName,"Malloc unsuccessful for fileid_new",ErrMess,1);
		rescode=-1;
		return rescode;
	}

	gid_new=(int*)realloc(gid_new,list_len_new*sizeof(int));
	if(gid_new==NULL)
	{
		if(fileid_prev!=NULL)
			free(fileid_prev);
		if(gid_prev!=NULL)
			free(gid_prev);
		if(fileid_new!=NULL)
			free(fileid_new);
		if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		GetErrorMess(appliName,"Malloc unsuccessful for filemode_new",ErrMess,1);
		rescode=-1;
		return rescode;
	}

	owner_uid_new=(int*)realloc(owner_uid_new,list_len_new*sizeof(int));
	if(owner_uid_new==NULL)
	{
		
		if(fileid_prev!=NULL)
			free(fileid_prev);
		if(gid_prev!=NULL)
			free(gid_prev);
		if(fileid_new!=NULL)
			free(fileid_new);
		if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
		if(gid_new!=NULL)
			free(gid_new);
		GetErrorMess(appliName,"Malloc unsuccessful for owner_uid_new",ErrMess,1);
		rescode=-1;
		return rescode;
	}
	//std::cout<<"memmory for new allocated"<<std::endl;
	//initialization
	//std::cout<<"intialization root_fileid="<<root_fileid<<std::endl; 
	rescode=mysql_GetFileID_Filemode_GID(conn_temp,db_name, root_fileid,fileid_prev,gid_prev,owner_uid_prev, list_len_prev,ErrMess_temp);


	if((strstr(ErrMess_temp,"BUFFER_TOO_SMALL"))!=NULL)
	{

		fileid_prev=(int*)realloc(fileid_prev,list_len_prev*sizeof(int));
		if(fileid_prev==NULL)
		{
			
			if(gid_prev!=NULL)
			free(gid_prev);
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
			if(gid_new!=NULL)
			free(gid_new);
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
		
			GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
			rescode=-1;
			return rescode;
		}

		gid_prev=(int*)realloc(gid_prev,list_len_prev*sizeof(int));
		if(gid_prev==NULL)
		{
			if(gid_prev!=NULL)
			free(gid_prev);
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
			if(gid_new!=NULL)
			free(gid_new);
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_prev!=NULL)
				free(fileid_prev);
			GetErrorMess(appliName,"Malloc unsuccessful for filemode_prev",ErrMess,1);
			rescode=-1;
			return rescode;
		}

		owner_uid_prev=(int*)realloc(owner_uid_prev,list_len_prev*sizeof(int));
		if(owner_uid_prev==NULL)
		{
			if(gid_prev!=NULL)
			free(gid_prev);
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
			if(gid_new!=NULL)
			free(gid_new);
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_prev!=NULL)
				free(fileid_prev);
			GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
			rescode=-1;
			return rescode;
		}
		rescode=mysql_GetFileID_Filemode_GID(conn_temp,db_name, root_fileid,fileid_prev,gid_prev,owner_uid_prev, list_len_prev,ErrMess_temp);
		if(rescode!=0)
		{
			if(gid_prev!=NULL)
			free(gid_prev);
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
			if(gid_new!=NULL)
			free(gid_new);
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_prev!=NULL)
				free(fileid_prev);
			GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
			rescode=-1;
			return rescode;
		}
	}
	else
	{
		if(rescode!=0)
		{
			if(gid_prev!=NULL)
			free(gid_prev);
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
			if(gid_new!=NULL)
			free(gid_new);
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_prev!=NULL)
			free(fileid_prev);
			GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
			rescode=-1;
			return rescode;

		}

	}
	std::cout<<"parent_fileid="<<fileid_prev[0]<<" and="<<list_len_prev<<std::endl;

	while(stop==0)
	{
		nb_rows_processed=0;
		for(i=0;i<list_len_prev;i++)
		{

			parent_fileid=fileid_prev[i];
			direct_type=-1;
			rescode=mysql_GetFileID_Filemode_GID(conn_temp,db_name, parent_fileid,fileid_new,gid_new,owner_uid_new, list_len_new,ErrMess_temp);
			//std::cout<<"value of fileid_new="<<fileid_new[0]<<" and rescode="<<rescode<<std::endl;
			if((strstr(ErrMess_temp,"BUFFER_TOO_SMALL"))!=NULL)
			{

				fileid_new=(int*)realloc(fileid_new,list_len_new*sizeof(int));
				if(fileid_new==NULL)
				{
			
					if(gid_prev!=NULL)
					free(gid_prev);
					if(fileid_new!=NULL)
					free(fileid_new);
					if(owner_uid_prev!=NULL)
					free(owner_uid_prev);
					if(gid_new!=NULL)
					free(gid_new);
					if(owner_uid_new!=NULL)
					free(owner_uid_new);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					if(gid_temp!=NULL)
					free(gid_temp);
					GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
					rescode=-1;
					return rescode;
				}

				gid_new=(int*)realloc(gid_new,list_len_new*sizeof(int));
				if(gid_new==NULL)
				{
					if(gid_prev!=NULL)
					free(gid_prev);
					if(fileid_new!=NULL)
					free(fileid_new);
					if(owner_uid_prev!=NULL)
					free(owner_uid_prev);
					if(gid_new!=NULL)
					free(gid_new);
					if(owner_uid_new!=NULL)
					free(owner_uid_new);
					if(fileid_prev!=NULL)
					free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					if(gid_temp!=NULL)
					free(gid_temp);
					GetErrorMess(appliName,"Malloc unsuccessful for filemode_prev",ErrMess,1);
					rescode=-1;
					return rescode;
				}

				owner_uid_new=(int*)realloc(owner_uid_new,list_len_new*sizeof(int));
				if(owner_uid_new==NULL)
				{
					if(gid_prev!=NULL)
					       free(gid_prev);
					if(fileid_new!=NULL)
						free(fileid_new);
					if(owner_uid_prev!=NULL)
						free(owner_uid_prev);
					if(gid_new!=NULL)
						free(gid_new);
					if(owner_uid_new!=NULL)
						free(owner_uid_new);
					if(fileid_prev!=NULL)
						free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					if(gid_temp!=NULL)
					free(gid_temp);
					GetErrorMess(appliName,"Malloc unsuccessful for fileid_prev",ErrMess,1);
					rescode=-1;
					return rescode;
				}
				rescode=mysql_GetFileID_Filemode_GID(conn_temp,db_name, parent_fileid,fileid_new,gid_new,owner_uid_new, list_len_new,ErrMess_temp);
				//std::cout<<" after mysql_getfileid="<<rescode<<std::endl;
				if(rescode!=0)
				{
					if(gid_prev!=NULL)
						free(gid_prev);
					if(fileid_new!=NULL)
						free(fileid_new);
					if(owner_uid_prev!=NULL)
						free(owner_uid_prev);
					if(gid_new!=NULL)
						free(gid_new);
					if(owner_uid_new!=NULL)
						free(owner_uid_new);
					if(fileid_prev!=NULL)
						free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					if(gid_temp!=NULL)
					free(gid_temp);
					GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
					rescode=-1;
					return rescode;
				}
			}
			else
			{
			  //std::cout<<"Errmess="<<ErrMess_temp<<std::endl;
				if(rescode!=0 && strstr(ErrMess_temp,"NO_ROWS_SELECTED")==NULL)
				{
					if(gid_prev!=NULL)
					free(gid_prev);
					if(fileid_new!=NULL)
					free(fileid_new);
					if(owner_uid_prev!=NULL)
					free(owner_uid_prev);
					if(gid_new!=NULL)
					free(gid_new);
					if(owner_uid_new!=NULL)
					free(owner_uid_new);
					if(fileid_prev!=NULL)
					free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					if(gid_temp!=NULL)
					free(gid_temp);
					GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
					rescode=-1;
					return rescode;
				}

			}
			//std::cout<<"nb_rows_processed="<<nb_rows_processed<<std::endl;
			nb_rows_processed_prev=nb_rows_processed;
			nb_rows_processed+=list_len_new;
			if(list_len_new>0)
			{
			  fileid_temp=(int*)realloc(fileid_temp,nb_rows_processed*sizeof(int));
			  gid_temp=(int*)realloc(gid_temp,nb_rows_processed*sizeof(int));
			  owner_uid_temp=(int*)realloc(owner_uid_temp,nb_rows_processed*sizeof(int));
			  if(fileid_temp==NULL ||gid_temp==NULL ||owner_uid_temp==NULL)
			    {

				if(gid_prev!=NULL)
				free(gid_prev);
				if(fileid_new!=NULL)
				free(fileid_new);
				if(owner_uid_prev!=NULL)
				free(owner_uid_prev);
				if(gid_new!=NULL)
				free(gid_new);
				if(owner_uid_new!=NULL)
				free(owner_uid_new);
				if(fileid_temp!=NULL)
				free(fileid_temp);
				if(gid_temp!=NULL)
				free(gid_temp);
				if(owner_uid_temp!=NULL)
				free(owner_uid_temp);
				if(fileid_prev!=NULL)
				free(fileid_prev);
				GetErrorMess(appliName,"Unsuccessful malloc for the temp arrays",ErrMess,1);
				rescode=-1;
				return rescode;

			}
			CopyVector(fileid_temp,fileid_new,list_len_new, nb_rows_processed_prev);
			CopyVector(gid_temp,gid_new,list_len_new, nb_rows_processed_prev);
			CopyVector(owner_uid_temp,owner_uid_new,list_len_new, nb_rows_processed_prev);
			}
			list_len_new=10000;
			//if(filemode_prev[i]>16384 && filemode_prev[i]<16895)
			direct_type=1;
			//if(filemode_prev[i]>32768 && filemode_prev[i]<33279)
			//direct_type=0;
			//std::cout<<"direct_type="<<direct_type<<std::endl;
			if(direct_type==1)
			{
				rescode=mysql_UpdateACL_only(conn_temp, db_name,parent_fileid,  direct_type,gid_prev[i],new_acl_gid, owner_uid_prev[i],   ErrMess_temp);
				total_nb_rows_updated++;
				if(total_nb_rows_updated%MAX_ROWS_UPDATED==0)
					rescode=mysql_commit(conn_temp);
				if(rescode!=0)
				{
					if(gid_prev!=NULL)
					free(gid_prev);
					if(fileid_new!=NULL)
					free(fileid_new);
					if(owner_uid_prev!=NULL)
					free(owner_uid_prev);
					if(gid_new!=NULL)
					free(gid_new);
					if(owner_uid_new!=NULL)
					free(owner_uid_new);
					if(fileid_prev!=NULL)
					free(fileid_prev);
					if(fileid_temp!=NULL)
					free(fileid_temp);
					if(owner_uid_temp!=NULL)
					free(owner_uid_temp);
					if(gid_temp!=NULL)
					free(gid_temp);
					sprintf(ErrMess_temp,"Pb when committing the %d and errno=%d ",nb_rows_processed,rescode);
					GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
					rescode=-1;
					return rescode;
				}
			}
			
		}
		if(nb_rows_processed>0)
		{
		fileid_prev=(int*)realloc(fileid_prev,nb_rows_processed*sizeof(int));
		gid_prev=(int*)realloc(gid_prev,nb_rows_processed*sizeof(int));
		owner_uid_prev=(int*)realloc(owner_uid_prev,nb_rows_processed*sizeof(int));
		if(fileid_prev==NULL ||gid_prev==NULL ||owner_uid_prev==NULL)
		{

			if(gid_prev!=NULL)
			free(gid_prev);
			if(fileid_new!=NULL)
			free(fileid_new);
			if(owner_uid_prev!=NULL)
			free(owner_uid_prev);
			if(gid_new!=NULL)
			free(gid_new);
			if(owner_uid_new!=NULL)
			free(owner_uid_new);
			if(fileid_temp!=NULL)
			free(fileid_temp);
			if(gid_temp!=NULL)
			free(gid_temp);
			if(owner_uid_temp!=NULL)
			free(owner_uid_temp);
			if(fileid_prev!=NULL)
			free(fileid_prev);
			GetErrorMess(appliName,"Unsuccessful malloc for the prev arrays",ErrMess,1);
			rescode=-1;
			return rescode;

		}
		}
		list_len_prev=nb_rows_processed;
		if(list_len_prev==0)
			stop=1;
		else
		  {
		CopyVector(fileid_prev,fileid_temp, nb_rows_processed,0);
		CopyVector(gid_prev,gid_temp,nb_rows_processed,0);
		CopyVector(owner_uid_prev,owner_uid_temp, nb_rows_processed,0);
		  }

	}
	std::cout<<"end of loop"<<std::endl;
	rescode=mysql_commit(conn_temp);
	if(gid_prev!=NULL)
	free(gid_prev);
	if(fileid_new!=NULL)
	free(fileid_new);
	if(owner_uid_prev!=NULL)
	free(owner_uid_prev);
	if(gid_new!=NULL)
	free(gid_new);
	if(owner_uid_new!=NULL)
	free(owner_uid_new);
	if(fileid_temp!=NULL)
	free(fileid_temp);
	if(gid_temp!=NULL)
	free(gid_temp);
	if(owner_uid_temp!=NULL)
	free(owner_uid_temp);
	if(fileid_prev!=NULL)
	free(fileid_prev);
	
	return 0;

}

