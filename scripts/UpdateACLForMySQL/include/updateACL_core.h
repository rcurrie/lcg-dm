#include <stdio.h>
#include <mysql.h>
#include <stdlib.h>
#include "getError.h"


int mysql_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess);
void mysql_disconnect();
int GetParamInfo(char* config_file,char* ErrMess);
int mysql_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess);
void mysql_disconnect();
int mysql_GetGID(MYSQL * conn_temp,char* db_name, char* group_name,int &gid,char* ErrMess);
int mysql_GetFileID(MYSQL * conn_temp,char* db_name, char* filename_part, int parent_fileid,int &fileid,int &root_owner_uid,int &gid_val,char* ErrMess);
int GetFileID_ByPath(MYSQL * conn_temp,char* db_name,char* path_file,int &fileid,int& root_owner_uid,int &gid_val, char *ErrMess);
int EncodeACL(int directory_type, int new_gid,int new_gid_acl,int owner_uid,int fileid, char* new_ACL, int &len_acl, char* ErrMess);
int mysql_UpdateACL(MYSQL * conn_temp,char* db_name,int fileid, int direc_type,int new_gid,int new_gid_acl,int owner_uid,  char* ErrMess);
int mysql_GetFileID_Filemode(MYSQL * conn_temp,char* db_name,  int parent_fileid,int* fileid_list,int * filemode_list,int* owner_uid, int& fileid_list_len,char* ErrMess);
void CopyVector(int * path_vector_new,int *path_vector_old,int path_vector_old_len, int index_start_new);
int GetAllDirectoriesAndFilesUnder(MYSQL * conn_temp,char* db_name,int root_fileid,int root_gid,int root_gid_acl,int root_owner_uid, char* ErrMess);
int RecreateConfigFile(char* filename, char* ErrMess);

int GetAllDirectoriesAndFilesUnder_acl(MYSQL * conn_temp,char* db_name,int root_fileid,int root_gid,int root_owner_uid, int new_acl_gid, char* ErrMess);


int mysql_GetFileID_Filemode_GID(MYSQL * conn_temp,char* db_name,  int parent_fileid,int* fileid_list,int * gid_list,int* owner_uid_list, int& fileid_list_len,char* ErrMess);

int mysql_UpdateACL_only(MYSQL * conn_temp,char* db_name,int fileid, int direc_type,int new_gid,int new_acl_gid,int owner_uid,  char* ErrMess);
