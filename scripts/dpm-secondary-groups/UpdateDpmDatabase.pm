#
# Query the DPM database and updates the 's_uid' and 'protocol' fields
#

package UpdateDpmDatabase;

use DBI;
use Common;
use strict;
use warnings; 

sub update_mysql($) {
	my ($dbh_dpm) = @_;

	$dbh_dpm->do ("ALTER TABLE dpm_req ADD groups VARCHAR(255)");
	$dbh_dpm->do ("ALTER TABLE dpm_pending_req ADD groups VARCHAR(255)");

	$dbh_dpm->do ("ALTER TABLE dpm_pool ADD groups VARCHAR(255) BINARY");

	$dbh_dpm->do ("UPDATE dpm_pending_req SET groups=r_gid");
	$dbh_dpm->do ("UPDATE dpm_req SET groups=r_gid");
	$dbh_dpm->do ("UPDATE dpm_pool SET groups=gid");

	$dbh_dpm->do ("ALTER TABLE dpm_pool DROP gid");

	$dbh_dpm->do ("DROP TABLE schema_version");
	$dbh_dpm->do ("CREATE TABLE schema_version_dpm (major INTEGER NOT NULL, minor INTEGER NOT NULL, patch INTEGER NOT NULL) TYPE=INNODB");
	$dbh_dpm->do ("INSERT INTO schema_version_dpm VALUES(3, 1, 0)");

	$dbh_dpm->commit;
}

sub update_oracle($) {
        my ($dbh_dpm) = @_;

	$dbh_dpm->do ("ALTER TABLE dpm_req ADD groups VARCHAR2(255)");
	$dbh_dpm->do ("ALTER TABLE dpm_pending_req ADD groups VARCHAR2(255)");

	$dbh_dpm->do ("ALTER TABLE dpm_pool ADD groups VARCHAR2(255)");

	$dbh_dpm->do ("UPDATE dpm_pending_req SET groups=r_gid");
	$dbh_dpm->do ("UPDATE dpm_req SET groups=r_gid");
	$dbh_dpm->do ("UPDATE dpm_pool SET groups=gid");

	$dbh_dpm->do ("ALTER TABLE dpm_pool DROP COLUMN gid");

	$dbh_dpm->do ("CREATE TABLE schema_version_dpm (major NUMBER(1), minor NUMBER(1), patch NUMBER(1))");
	$dbh_dpm->do ("INSERT INTO schema_version_dpm VALUES(3, 1, 0)");

	$dbh_dpm->commit;
}

1;
