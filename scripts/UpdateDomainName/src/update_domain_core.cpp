#include "update_domain_core.h"
MYSQL* conn_handler;

#define PARENT_FILEID_ROOT 2
#define ACL_TYPE_LEN 5000
#define MAX_ROWS_UPDATED 1000
 char _mysql_db_name[100];
  char _mysql_user_name[100];
//char _mysql_pwd[100];
  char _mysql_host_name[100];
 int _mysql_port_nb;
char _mysql_socket_path[500];


 char _host_name[500];
 char _domain_name[500];

int GetParamInfo(char* config_file,char* ErrMess)
{
	char appliName[100]="GetParamInfo";
	char* s=(char*)malloc(2001*sizeof(char));
	FILE* f=NULL;
	double next=1;
	int stop=0;
	int pos1=0;
	int pos2=0;
	int rescode=0;
	if(s==NULL)
	{
		GetErrorMess(appliName,"MALLOC UNSUCCESSFUL",ErrMess,1);
		return -1;
	}

	if ((f=fopen( config_file,"r"))== NULL)
	{
		GetErrorMess(appliName,"Cannot open the config file",ErrMess,1);
		free(s);
		return -1;
	}
	else
	{
		
		while (fgets(s,1000,f)!=NULL )
		{
			

			if(strstr(s,"mysql_db_name=")!=NULL)
			{
				next=next*2;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
				s[pos1+pos2]='\0';
				strcpy(_mysql_db_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql db name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			if(strstr(s,"mysql_user_name=")!=NULL)
			{
				next=next*3;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_user_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql user name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}

			
			if(strstr(s,"mysql_host_name=")!=NULL)
			{
				next=next*5;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_host_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The mysql host name entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"mysql_port_nb=")!=NULL)
			{
				next=next*7;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					_mysql_port_nb=atoi(s+pos1+1);
				}
				
			}
			if(strstr(s,"mysql_socket_path=")!=NULL)
			{
				next=next*11;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_mysql_socket_path,s+pos1+1);
				}
			}
			if(strstr(s,"new_host_name=")!=NULL)
			{
				next=next*13;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
			
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_host_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The host_name entry is empty or you have forgotten the ';' at the end!!",ErrMess,1);
					free(s);
					return -1;
				}
			}
			if(strstr(s,"domain_name=")!=NULL)
			{
				next=next*17;
				pos1=strcspn(s,"=");
				pos2=strcspn(s+pos1,";");
				if(pos2>3 && pos2!=strlen(s+pos1))
				{
					s[pos1+pos2]='\0';
					strcpy(_domain_name,s+pos1+1);
				}
				else
				{
					GetErrorMess(appliName,"The domain entry entry is empty or you have forgotten the ';' at the end!",ErrMess,1);
					free(s);
					return -1;
				}
			}
		}
		
		fclose(f);
		free(s);
		if(next==510510)
		{
			GetErrorMess(appliName,"",ErrMess,0);
			rescode=RecreateConfigFile( config_file,  ErrMess);
			rescode=0;

		}
		else
		{
			GetErrorMess(appliName,"Invalid config file, not all the entries were filled",ErrMess,1);
			rescode=-1;
		}
		
		return rescode;
		
	}
}

int mysql_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess)
{
	char appliName[100]="mysql_DBConnexion";
	
	char errmessage[500];
	conn_handler=mysql_init(NULL);
	if(conn_handler==NULL)
	{
		GetErrorMess(appliName,"mysql_init() failed ",ErrMess,1);
		return -1;
	}
	if(mysql_real_connect (conn_handler,host_name, user_name,password,db_name,port_num,socket_name,flags)==NULL)
	{
		sprintf(errmessage,"mysql_real_connect() failed. Error %u (%s)",mysql_errno(conn_handler),mysql_error(conn_handler));
		GetErrorMess(appliName,errmessage,ErrMess,1);
		return -1;
	}
	


	return 0;
}

void mysql_disconnect()
{
mysql_close(conn_handler);
}








int mysql_UpdateHost(MYSQL * conn_temp,char* db_name, char* host_name, char* domain_name,char* ErrMess)
{
	char appliName[100]="mysql_UpdateHost";
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;

	char mysql_stmt[1000];
	char ErrMess_temp[1000];
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"update %s.Cns_file_replica set host=concat_ws('.',host,'%s') where host='%s'",db_name,domain_name,host_name);
	else
		sprintf(mysql_stmt,"update Cns_file_replica set host=concat_ws('.',host,'%s') where host='%s'",domain_name,host_name);
	
	
	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
	    GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
	    if(mysql_error(conn_temp)!=0)
	    {
		sprintf(ErrMess_temp,"mysql_query() failed : sqlerrno=%d :%s ",mysql_errno(conn_temp),mysql_error(conn_temp));
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
	    }
	  else
	    rescode=0;
		
	}
	else
	  rescode=0;

	

	return rescode;
}

int mysql_UpdateSFN(MYSQL * conn_temp,char* db_name, char* host_name, char* domain_name,char* ErrMess)
{
	char appliName[100]="mysql_UpdateHost";
	int pos1=0;
	int pos2=0;
	int pos3=0;
	int i=0;
	int rescode=-1;
	char ErrMess_temp[1000];
	char mysql_stmt[1000];
	
	if(strstr(db_name,"none")==NULL)
		sprintf(mysql_stmt,"update %s.Cns_file_replica set sfn=replace(sfn,'%s:','%s.%s:') where sfn like '%s:%%'",db_name,host_name,host_name,domain_name,host_name);
	else
		sprintf(mysql_stmt,"update Cns_file_replica set sfn=replace(sfn,'%s:','%s.%s:') where sfn like '%s:%%'",host_name,host_name,domain_name,host_name);
	

	if(mysql_query(conn_temp,mysql_stmt)!=0)
	{
		GetErrorMess(appliName,"mysql_query() failed",ErrMess,1);
		
	}
	
	if((long) mysql_affected_rows(conn_temp)>1)
	{
			rescode=0;
	}
	else
	{
		sprintf(ErrMess_temp,"mysql_query() failed : sqlerrno=%d :%s ",mysql_errno(conn_temp),mysql_error(conn_temp));
		GetErrorMess(appliName,ErrMess_temp,ErrMess,1);
	}

	

	return rescode;
}


int RecreateConfigFile(char* filename, char* ErrMess)
{
char appliName[100]="RecreateConfigFile";
FILE *f1;
int rescode=0;
f1 = fopen (filename, "w+t");  
char col_stmt[200];
if (f1==NULL)
{
	sprintf(col_stmt,"Could not open the config file  %s ",filename);
	GetErrorMess(appliName,col_stmt,ErrMess,1);
	
	rescode=-1;
	return rescode;
}

rescode=fprintf(f1,"mysql_host_name=%s;\nmysql_user_name=%s;\nmysql_db_name=%s;\nmysql_port_nb=\nmysql_socket_path=\nnew_host_name=%s;\ndomain_name=%s;",_mysql_host_name, _mysql_user_name,_mysql_db_name,_host_name,_domain_name);
fclose (f1); 
GetErrorMess(appliName,"none",ErrMess,0);
return 0;
}

