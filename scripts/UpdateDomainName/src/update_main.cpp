/********************************************************************************/
// Author: L.Abadie
// version:v1.0
/********************************************************************************/
#include "update_domain_core.h"
#include <curses.h> 
#include <pwd.h> 
#include <unistd.h> 

extern char _mysql_db_name[100];
 extern char _mysql_user_name[100];
 //extern char _mysql_pwd[100];
 extern char _mysql_host_name[100];
 extern int _mysql_port_nb;
 extern char _mysql_socket_path[500];

 extern char _host_name[500];
 extern char _domain_name[500];
 extern MYSQL* conn_handler;
int main()
{

char config_file[20];
int status=0;
int flags=0;
int rescode=0;
  int count=0;
int rescode1=0;
char ErrMess[1000];
char ErrMess_bis[1000];
char appliName[100];
  char* mysql_pwd=NULL;
char buffer[102];
  char domain_name[100];
  /*
  initscr();
  printw(" Enter pwd for the mysql DB \n");

//get info about the conn for oracle and mysql
  // std::cout<<"enter password "<<std::endl; 
  refresh(); // refresh screen with chars from printw

 noecho(); // no output of input
while((buffer[count] = getch()) != '\n') // read input until Enter is pressed
{
  printw("*");
  refresh();
count++;
}
 endwin();
 buffer[count]='\0';
strcpy(mysql_pwd,buffer); 
*/
  mysql_pwd=(char*)malloc(101*sizeof(char));
  if(mysql_pwd==NULL)
    {
      GetErrorMess(appliName,"Malloc unsuccesful",ErrMess,1);
	WriteToLogFile(appliName,  ErrMess);

    }
  mysql_pwd=getpass("Enter the DB password\n");

 

strcpy(config_file,"config_file.dat");
 
rescode=GetParamInfo(config_file, ErrMess);
 
if(rescode!=0)
{
	strcpy(appliName,"GetParamInfo");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess_bis);
	  free(mysql_pwd);
	std::cout<<ErrMess_bis<<std::endl;
	return -1;
}
else
	WriteToLogFile(appliName,  ErrMess);



rescode1=mysql_DBConnect(_mysql_host_name,_mysql_user_name,mysql_pwd, _mysql_db_name,_mysql_port_nb, _mysql_socket_path, flags, ErrMess);

if(rescode1!=0 )
{
	strcpy(appliName,"mysql_DBConnect");
	GetErrorMess(appliName,ErrMess,ErrMess_bis,1);
	WriteToLogFile(appliName,  ErrMess_bis);
	free(mysql_pwd);
	
	return -1;
	//std::cout<<ErrMess_bis<<std::endl;
}
free(mysql_pwd);	
WriteToLogFile("main", "Connected to Oracle and MySQL server successfully");

rescode=mysql_autocommit(conn_handler, 1) ;
if(rescode!=0)
{
	GetErrorMess(appliName,"could not enable the autocommit mode",ErrMess,1);
	return rescode;
}
if(_domain_name[0]=='.')
  strcpy(domain_name,_domain_name+1);
else
 strcpy(domain_name,_domain_name);

rescode=mysql_UpdateHost(conn_handler,_mysql_db_name, _host_name, domain_name, ErrMess);
if(rescode!=0)
{
	sprintf(ErrMess_bis,"Pb when updating the host column : %s",ErrMess);
	WriteToLogFile("main", ErrMess_bis);
	return -1;
}
else
{
	sprintf(ErrMess_bis,"Updating the host column was successful ");
	WriteToLogFile("main", ErrMess_bis);

	
}

rescode=mysql_UpdateSFN(conn_handler,_mysql_db_name, _host_name, domain_name, ErrMess);
if(rescode!=0)
{
	sprintf(ErrMess_bis,"Pb when updating the sfn column : %s",ErrMess);
	WriteToLogFile("main", ErrMess_bis);
	free(mysql_pwd);
	return -1;
}
else
{
	sprintf(ErrMess_bis,"Updating the sfn column was successful ");
	WriteToLogFile("main", "Application successfully terminated");
	WriteToLogFile("main", ErrMess_bis);
}


return 0;

}
