#include <stdio.h>
#include <mysql.h>
#include <stdlib.h>
#include "getError.h"


int mysql_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess);
void mysql_disconnect();
int GetParamInfo(char* config_file,char* ErrMess);
int RecreateConfigFile(char* filename, char* ErrMess);
int mysql_UpdateHost(MYSQL * conn_temp,char* db_name, char* host_name, char* domain_name,char* ErrMess);
int mysql_UpdateSFN(MYSQL * conn_temp,char* db_name, char* host_name, char* domain_name,char* ErrMess);


