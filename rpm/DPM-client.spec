Summary: APIs and CLIs for DPM/DPNS/RFIO
Name: DPM-client
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
License: Apache-2.0
Prefix: @PREFIX@
BuildRequires: @BUILDREQUIRES@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.

%package -n dpm
Summary: CLI for DPM/DPNS/RFIO
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0
AutoReqProv: yes
Obsoletes: DPM-client
%description -n dpm
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This provides the CLIs and the corresponding man pages.

%package -n dpm-libs
Summary: Client shared libraries for Disk Pool Manager
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0
AutoReqProv: yes
Obsoletes: DPM-client
%description -n dpm-libs
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This provides the client shared libraries. 

%package -n dpm-devel
Summary: Disk Pool Manager development libraries and header files
Group: grid/lcg
Requires: dpm-libs >= @VERSION@, lcgdm-devel >= 1.8.0
AutoReqProv: yes
Obsoletes: DPM-client
%description -n dpm-devel
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This provides the client header files, archive library and API man pages.

%package -n perl-dpm
Summary: Perl interface to Disk Pool Manager
Group: grid/lcg
BuildRequires: @REQUIRES.PERL@
Requires: dpm-libs >= @VERSION@, @REQUIRES.PERL@
AutoReqProv: yes
Obsoletes: DPM-interfaces, DPM-interfaces2
%description -n perl-dpm
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This is the perl interface to the DPM built with swig. 
It requires Perl 5.

%package -n python-dpm
Summary: Python interfaces to Disk Pool Manager
Group: grid/lcg
BuildRequires: python >= @PYTHON.VERSION@
Requires: dpm-libs >= @VERSION@, python >= @PYTHON.VERSION@
AutoReqProv: yes
Obsoletes: DPM-interfaces, DPM-interfaces2
%description -n python-dpm
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This is the Python interface to the DPM built with swig.
It requires Python >= 2.3.

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-client-only dpm ${EXTRA_CONFIGURE_OPTIONS}
make

%install 
rm -rf $RPM_BUILD_ROOT
# For client backward compatibility
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python
ln -sf ../python@PYTHON.VERSION@/site-packages/_dpm.so ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/_dpm.so
ln -sf ../python@PYTHON.VERSION@/site-packages/dpm.py ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/dpm.py

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

%clean

%files -n dpm
%defattr(-,root,root)
%{prefix}/bin/dpm-addfs
%{prefix}/bin/dpm-addpool
%{prefix}/bin/dpm-drain
%{prefix}/bin/dpm-getspacemd
%{prefix}/bin/dpm-getspacetokens
%{prefix}/bin/dpm-modifyfs
%{prefix}/bin/dpm-modifypool
%{prefix}/bin/dpm-ping
%{prefix}/bin/dpm-qryconf
%{prefix}/bin/dpm-register
%{prefix}/bin/dpm-releasespace
%{prefix}/bin/dpm-replicate
%{prefix}/bin/dpm-reservespace
%{prefix}/bin/dpm-rmfs
%{prefix}/bin/dpm-rmpool
%{prefix}/bin/dpm-updatespace
%{prefix}/bin/dpns-chgrp
%{prefix}/bin/dpns-chmod
%{prefix}/bin/dpns-chown
%{prefix}/bin/dpns-entergrpmap
%{prefix}/bin/dpns-enterusrmap
%{prefix}/bin/dpns-getacl
%{prefix}/bin/dpns-listgrpmap
%{prefix}/bin/dpns-listusrmap
%{prefix}/bin/dpns-ln
%{prefix}/bin/dpns-ls
%{prefix}/bin/dpns-mkdir
%{prefix}/bin/dpns-modifygrpmap
%{prefix}/bin/dpns-modifyusrmap
%{prefix}/bin/dpns-ping
%{prefix}/bin/dpns-rename
%{prefix}/bin/dpns-rm
%{prefix}/bin/dpns-rmgrpmap
%{prefix}/bin/dpns-rmusrmap
%{prefix}/bin/dpns-setacl
%{prefix}/bin/rfcat
%{prefix}/bin/rfchmod
%{prefix}/bin/rfcp
%{prefix}/bin/rfdf
%{prefix}/bin/rfdir
%{prefix}/bin/rfmkdir
%{prefix}/bin/rfrename
%{prefix}/bin/rfrm
%{prefix}/bin/rfstat
%{prefix}/share/man/man1/dpm-addfs.1
%{prefix}/share/man/man1/dpm-addpool.1
%{prefix}/share/man/man1/dpm-drain.1
%{prefix}/share/man/man1/dpm-getspacemd.1
%{prefix}/share/man/man1/dpm-getspacetokens.1
%{prefix}/share/man/man1/dpm-modifyfs.1
%{prefix}/share/man/man1/dpm-modifypool.1
%{prefix}/share/man/man1/dpm-ping.1
%{prefix}/share/man/man1/dpm-qryconf.1
%{prefix}/share/man/man1/dpm-register.1
%{prefix}/share/man/man1/dpm-releasespace.1
%{prefix}/share/man/man1/dpm-replicate.1
%{prefix}/share/man/man1/dpm-reservespace.1
%{prefix}/share/man/man1/dpm-rmfs.1
%{prefix}/share/man/man1/dpm-rmpool.1
%{prefix}/share/man/man1/dpm-updatespace.1
%{prefix}/share/man/man1/dpns-chgrp.1
%{prefix}/share/man/man1/dpns-chmod.1
%{prefix}/share/man/man1/dpns-chown.1
%{prefix}/share/man/man1/dpns-entergrpmap.1
%{prefix}/share/man/man1/dpns-enterusrmap.1
%{prefix}/share/man/man1/dpns-getacl.1
%{prefix}/share/man/man1/dpns-listgrpmap.1
%{prefix}/share/man/man1/dpns-listusrmap.1
%{prefix}/share/man/man1/dpns-ln.1
%{prefix}/share/man/man1/dpns-ls.1
%{prefix}/share/man/man1/dpns-mkdir.1
%{prefix}/share/man/man1/dpns-modifygrpmap.1
%{prefix}/share/man/man1/dpns-modifyusrmap.1
%{prefix}/share/man/man1/dpns-ping.1
%{prefix}/share/man/man1/dpns-rename.1
%{prefix}/share/man/man1/dpns-rm.1
%{prefix}/share/man/man1/dpns-rmgrpmap.1
%{prefix}/share/man/man1/dpns-rmusrmap.1
%{prefix}/share/man/man1/dpns-setacl.1
%{prefix}/share/man/man1/rfcat.1
%{prefix}/share/man/man1/rfchmod.1
%{prefix}/share/man/man1/rfcp.1
%{prefix}/share/man/man1/rfdf.1
%{prefix}/share/man/man1/rfdir.1
%{prefix}/share/man/man1/rfmkdir.1
%{prefix}/share/man/man1/rfrename.1
%{prefix}/share/man/man1/rfrm.1
%{prefix}/share/man/man1/rfstat.1

%files -n dpm-libs
%{prefix}/%{_lib}/libdpm.so.*

%files -n dpm-devel
%{prefix}/include/dpm/Castor_limits.h
%{prefix}/include/dpm/Cnetdb.h
%{prefix}/include/dpm/Cns_api.h
%{prefix}/include/dpm/Cns_constants.h
%{prefix}/include/dpm/Cns_struct.h
%{prefix}/include/dpm/dpm_api.h
%{prefix}/include/dpm/dpm_constants.h
%{prefix}/include/dpm/dpm_struct.h
%{prefix}/include/dpm/dpns_api.h
%{prefix}/include/dpm/osdep.h
%{prefix}/include/dpm/rfcntl.h
%{prefix}/include/dpm/rfio.h
%{prefix}/include/dpm/rfio_api.h
%{prefix}/include/dpm/rfio_constants.h
%{prefix}/include/dpm/rfio_errno.h
%{prefix}/include/dpm/serrno.h
%{prefix}/include/dpm/u64subr.h
%{prefix}/%{_lib}/libdpm.a
%{prefix}/%{_lib}/libdpm.so
%{prefix}/share/man/man3/dpm_abortfiles.3
%{prefix}/share/man/man3/dpm_abortreq.3
%{prefix}/share/man/man3/dpm_addfs.3
%{prefix}/share/man/man3/dpm_addpool.3
%{prefix}/share/man/man3/dpm_copy.3
%{prefix}/share/man/man3/dpm_delreplica.3
%{prefix}/share/man/man3/dpm_extendfilelife.3
%{prefix}/share/man/man3/dpm_get.3
%{prefix}/share/man/man3/dpm_getifcevers.3
%{prefix}/share/man/man3/dpm_getpoolfs.3
%{prefix}/share/man/man3/dpm_getpools.3
%{prefix}/share/man/man3/dpm_getprotocols.3
%{prefix}/share/man/man3/dpm_getreqid.3
%{prefix}/share/man/man3/dpm_getreqsummary.3
%{prefix}/share/man/man3/dpm_getspacemd.3
%{prefix}/share/man/man3/dpm_getspacetoken.3
%{prefix}/share/man/man3/dpm_getstatus_copyreq.3
%{prefix}/share/man/man3/dpm_getstatus_getreq.3
%{prefix}/share/man/man3/dpm_getstatus_putreq.3
%{prefix}/share/man/man3/dpm_modifyfs.3
%{prefix}/share/man/man3/dpm_modifypool.3
%{prefix}/share/man/man3/dpm_ping.3
%{prefix}/share/man/man3/dpm_put.3
%{prefix}/share/man/man3/dpm_putdone.3
%{prefix}/share/man/man3/dpm_releasespace.3
%{prefix}/share/man/man3/dpm_relfiles.3
%{prefix}/share/man/man3/dpm_reservespace.3
%{prefix}/share/man/man3/dpm_rm.3
%{prefix}/share/man/man3/dpm_rmfs.3
%{prefix}/share/man/man3/dpm_rmpool.3
%{prefix}/share/man/man3/dpm_seterrbuf.3
%{prefix}/share/man/man3/dpm_updatespace.3
%{prefix}/share/man/man3/dpns_aborttrans.3
%{prefix}/share/man/man3/dpns_access.3
%{prefix}/share/man/man3/dpns_accessr.3
%{prefix}/share/man/man3/dpns_addreplica.3
%{prefix}/share/man/man3/dpns_addreplicax.3
%{prefix}/share/man/man3/dpns_chdir.3
%{prefix}/share/man/man3/dpns_chmod.3
%{prefix}/share/man/man3/dpns_chown.3
%{prefix}/share/man/man3/dpns_closedir.3
%{prefix}/share/man/man3/dpns_creat.3
%{prefix}/share/man/man3/dpns_delete.3
%{prefix}/share/man/man3/dpns_delreplica.3
%{prefix}/share/man/man3/dpns_delreplicasbysfn.3
%{prefix}/share/man/man3/dpns_endsess.3
%{prefix}/share/man/man3/dpns_endtrans.3
%{prefix}/share/man/man3/dpns_entergrpmap.3
%{prefix}/share/man/man3/dpns_enterusrmap.3
%{prefix}/share/man/man3/dpns_getacl.3
%{prefix}/share/man/man3/dpns_getcwd.3
%{prefix}/share/man/man3/dpns_getgrpbygid.3
%{prefix}/share/man/man3/dpns_getgrpbygids.3
%{prefix}/share/man/man3/dpns_getgrpbynam.3
%{prefix}/share/man/man3/dpns_getgrpmap.3
%{prefix}/share/man/man3/dpns_getidmap.3
%{prefix}/share/man/man3/dpns_getifcevers.3
%{prefix}/share/man/man3/dpns_getreplica.3
%{prefix}/share/man/man3/dpns_getreplicax.3
%{prefix}/share/man/man3/dpns_getusrbynam.3
%{prefix}/share/man/man3/dpns_getusrbyuid.3
%{prefix}/share/man/man3/dpns_getusrmap.3
%{prefix}/share/man/man3/dpns_lchown.3
%{prefix}/share/man/man3/dpns_listrep4gc.3
%{prefix}/share/man/man3/dpns_listreplica.3
%{prefix}/share/man/man3/dpns_listreplicax.3
%{prefix}/share/man/man3/dpns_listrepset.3
%{prefix}/share/man/man3/dpns_lstat.3
%{prefix}/share/man/man3/dpns_mkdir.3
%{prefix}/share/man/man3/dpns_modifygrpmap.3
%{prefix}/share/man/man3/dpns_modifyusrmap.3
%{prefix}/share/man/man3/dpns_modreplica.3
%{prefix}/share/man/man3/dpns_modreplicax.3
%{prefix}/share/man/man3/dpns_opendir.3
%{prefix}/share/man/man3/dpns_opendirxg.3
%{prefix}/share/man/man3/dpns_ping.3
%{prefix}/share/man/man3/dpns_readdir.3
%{prefix}/share/man/man3/dpns_readdirg.3
%{prefix}/share/man/man3/dpns_readdirx.3
%{prefix}/share/man/man3/dpns_readdirxp.3
%{prefix}/share/man/man3/dpns_readdirxr.3
%{prefix}/share/man/man3/dpns_readlink.3
%{prefix}/share/man/man3/dpns_registerfiles.3
%{prefix}/share/man/man3/dpns_rename.3
%{prefix}/share/man/man3/dpns_rewinddir.3
%{prefix}/share/man/man3/dpns_rmdir.3
%{prefix}/share/man/man3/dpns_rmgrpmap.3
%{prefix}/share/man/man3/dpns_rmusrmap.3
%{prefix}/share/man/man3/dpns_setacl.3
%{prefix}/share/man/man3/dpns_setatime.3
%{prefix}/share/man/man3/dpns_seterrbuf.3
%{prefix}/share/man/man3/dpns_setfsize.3
%{prefix}/share/man/man3/dpns_setfsizec.3
%{prefix}/share/man/man3/dpns_setptime.3
%{prefix}/share/man/man3/dpns_setratime.3
%{prefix}/share/man/man3/dpns_setrltime.3
%{prefix}/share/man/man3/dpns_setrstatus.3
%{prefix}/share/man/man3/dpns_setrtype.3
%{prefix}/share/man/man3/dpns_startsess.3
%{prefix}/share/man/man3/dpns_starttrans.3
%{prefix}/share/man/man3/dpns_stat.3
%{prefix}/share/man/man3/dpns_statg.3
%{prefix}/share/man/man3/dpns_statr.3
%{prefix}/share/man/man3/dpns_symlink.3
%{prefix}/share/man/man3/dpns_umask.3
%{prefix}/share/man/man3/dpns_undelete.3
%{prefix}/share/man/man3/dpns_unlink.3
%{prefix}/share/man/man3/dpns_utime.3
%{prefix}/share/man/man3/rfio_access.3
%{prefix}/share/man/man3/rfio_chmod.3
%{prefix}/share/man/man3/rfio_chown.3
%{prefix}/share/man/man3/rfio_close.3
%{prefix}/share/man/man3/rfio_closedir.3
%{prefix}/share/man/man3/rfio_fchmod.3
%{prefix}/share/man/man3/rfio_fclose.3
%{prefix}/share/man/man3/rfio_feof.3
%{prefix}/share/man/man3/rfio_ferror.3
%{prefix}/share/man/man3/rfio_fflush.3
%{prefix}/share/man/man3/rfio_fileno.3
%{prefix}/share/man/man3/rfio_fopen.3
%{prefix}/share/man/man3/rfio_fopen64.3
%{prefix}/share/man/man3/rfio_fread.3
%{prefix}/share/man/man3/rfio_fseek.3
%{prefix}/share/man/man3/rfio_fseeko64.3
%{prefix}/share/man/man3/rfio_fstat.3
%{prefix}/share/man/man3/rfio_fstat64.3
%{prefix}/share/man/man3/rfio_ftell.3
%{prefix}/share/man/man3/rfio_ftello64.3
%{prefix}/share/man/man3/rfio_fwrite.3
%{prefix}/share/man/man3/rfio_lockf.3
%{prefix}/share/man/man3/rfio_lockf64.3
%{prefix}/share/man/man3/rfio_lseek.3
%{prefix}/share/man/man3/rfio_lseek64.3
%{prefix}/share/man/man3/rfio_lstat.3
%{prefix}/share/man/man3/rfio_lstat64.3
%{prefix}/share/man/man3/rfio_mkdir.3
%{prefix}/share/man/man3/rfio_mstat.3
%{prefix}/share/man/man3/rfio_mstat64.3
%{prefix}/share/man/man3/rfio_msymlink.3
%{prefix}/share/man/man3/rfio_munlink.3
%{prefix}/share/man/man3/rfio_open.3
%{prefix}/share/man/man3/rfio_open64.3
%{prefix}/share/man/man3/rfio_opendir.3
%{prefix}/share/man/man3/rfio_pclose.3
%{prefix}/share/man/man3/rfio_perror.3
%{prefix}/share/man/man3/rfio_popen.3
%{prefix}/share/man/man3/rfio_pread.3
%{prefix}/share/man/man3/rfio_preseek.3
%{prefix}/share/man/man3/rfio_preseek64.3
%{prefix}/share/man/man3/rfio_pwrite.3
%{prefix}/share/man/man3/rfio_rcp.3
%{prefix}/share/man/man3/rfio_read.3
%{prefix}/share/man/man3/rfio_readdir.3
%{prefix}/share/man/man3/rfio_readlink.3
%{prefix}/share/man/man3/rfio_rename.3
%{prefix}/share/man/man3/rfio_rewinddir.3
%{prefix}/share/man/man3/rfio_rmdir.3
%{prefix}/share/man/man3/rfio_serror.3
%{prefix}/share/man/man3/rfio_setbufsize.3
%{prefix}/share/man/man3/rfio_stat.3
%{prefix}/share/man/man3/rfio_stat64.3
%{prefix}/share/man/man3/rfio_statfs.3
%{prefix}/share/man/man3/rfio_statfs64.3
%{prefix}/share/man/man3/rfio_symlink.3
%{prefix}/share/man/man3/rfio_unlink.3
%{prefix}/share/man/man3/rfio_write.3
%{prefix}/share/man/man3/rfioreadopt.3
%{prefix}/share/man/man3/rfiosetopt.3

%files -n perl-dpm
%defattr(-,root,root)
%attr(755,root,root) @PERLDIR@/dpm.so
%attr(755,root,root) @PERLDIR@/dpm.pm

%files -n python-dpm
%dir %{prefix}/%{_lib}/python
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_dpm.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/dpm.py
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_dpm2.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/dpm2.py
%attr(755,root,root) %{prefix}/%{_lib}/python/_dpm.so
%attr(755,root,root) %{prefix}/%{_lib}/python/dpm.py
%attr(755,root,root) %{prefix}/bin/dpm-listspaces
%attr(644,root,root) %{prefix}/share/man/man1/dpm-listspaces.1
%attr(644,root,root) %{prefix}/share/man/man3/dpm_python.3
%attr(644,root,root) %{prefix}/share/man/man3/dpm2_python.3

%post -n dpm-libs
if [ `uname -m` != x86_64 -o \( `uname -m` = x86_64 -a "%{_lib}" = lib64 \) ]; then
   if [ `grep -c ^%{prefix}/%{_lib} /etc/ld.so.conf` = 0 ]; then
      echo "%{prefix}/%{_lib}" >> /etc/ld.so.conf
   fi
fi

[ -x "/sbin/ldconfig" ] && /sbin/ldconfig

%postun -n dpm-libs
[ -x "/sbin/ldconfig" ] && /sbin/ldconfig
