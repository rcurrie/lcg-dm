Summary: LCG File Catalog
Name: LFC-client-py25
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
AutoReqProv: yes
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
BuildRequires: @REQUIRES.PERL@
BuildRequires: python >= 2.5
License: Apache-2.0
Prefix: @PREFIX@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
python 2.5 interface to LFC

%package -n python25-lfc
Summary: LCG File Catalog Interfaces
Group: grid/lcg
Requires: lfc-libs >= @VERSION@, python-lfc >= @VERSION@
AutoReqProv: yes
Obsoletes: LFC-interfaces-py25, LFC-interfaces2-py25
%description -n python25-lfc
The LCG File Catalog (LFC) allows to store files in a File System looking like structure.
It allows you to create symbolic links to any file or directory stored in the LFC, as well as replicas.
This is the Python interface to the LFC built with swig. 
It requires Python 2.5.

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-client-only lfc ${EXTRA_CONFIGURE_OPTIONS}
make

%install 
rm -rf $RPM_BUILD_ROOT

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

%clean

%files -n python25-lfc
%defattr(-,root,root)
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/_lfc.so
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/lfc.py
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/_lfcthr.so
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/lfcthr.py
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/_lfc2.so
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/lfc2.py
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/_lfc2thr.so
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/lfc2thr.py
