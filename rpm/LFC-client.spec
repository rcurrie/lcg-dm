Summary: Client for the LFC
Name: LFC-client
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
License: Apache-2.0
Prefix: @PREFIX@
BuildRequires: @BUILDREQUIRES@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
The LCG File Catalog (LFC) allows to store files in a File System looking like
structure.  It allows you to create symbolic links to any file or directory
stored in the LFC, as well as replicas.

%package -n lfc
Summary: CLI for LCG File Catalogue
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0
AutoReqProv: yes
Obsoletes: LFC-client
%description -n lfc
The LCG File Catalog (LFC) allows to store files in a File System looking like
structure.  It allows you to create symbolic links to any file or directory
stored in the LFC, as well as replicas.
This provides the CLIs and the corresponding man pages.

%package -n lfc-libs
Summary: Client shared libraries for LCG File Catalogue
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0
AutoReqProv: yes
Obsoletes: LFC-client
%description -n lfc-libs
The LCG File Catalog (LFC) allows to store files in a File System looking like
structure.  It allows you to create symbolic links to any file or directory
stored in the LFC, as well as replicas.
This provides the client shared libraries.

%package -n lfc-devel
Summary: LCG File Catalogue development libraries and header files
Group: grid/lcg
Requires: lfc-libs >= @VERSION@, lcgdm-devel >= 1.8.0
AutoReqProv: yes
Obsoletes: LFC-client
%description -n lfc-devel
The LCG File Catalog (LFC) allows to store files in a File System looking like
structure.  It allows you to create symbolic links to any file or directory
stored in the LFC, as well as replicas.
This provides the client header files, archive library and API man pages.

%package -n perl-lfc
Summary: Perl interface to LCG File Catalog
Group: grid/lcg
BuildRequires: @REQUIRES.PERL@
Requires: lfc-libs >= @VERSION@, @REQUIRES.PERL@
AutoReqProv: yes
Obsoletes: LFC-interfaces, LFC-interfaces2
%description -n perl-lfc
The LCG File Catalog (LFC) allows to store files in a File System looking like
structure. It allows you to create symbolic links to any file or directory
stored in the LFC, as well as replicas.
This is the perl interface to the LFC built with swig. 
It requires Perl 5.

%package -n python-lfc
Summary: Python interfaces to LCG File Catalog
Group: grid/lcg
BuildRequires: python >= @PYTHON.VERSION@
Requires: lfc-libs >= @VERSION@, python >= @PYTHON.VERSION@
AutoReqProv: yes
Obsoletes: LFC-interfaces, LFC-interfaces2
%description -n python-lfc
The LCG File Catalog (LFC) allows to store files in a File System looking like
structure. It allows you to create symbolic links to any file or directory
stored in the LFC, as well as replicas.
This is the python interface to the LFC built with swig. 
It requires Python >= 2.3.

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-client-only lfc ${EXTRA_CONFIGURE_OPTIONS}
make

%install 
rm -rf $RPM_BUILD_ROOT

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

mkdir -p ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python
ln -sf ../python@PYTHON.VERSION@/site-packages/_lfc.so ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/_lfc.so
ln -sf ../python@PYTHON.VERSION@/site-packages/lfc.py ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/lfc.py
ln -sf ../python@PYTHON.VERSION@/site-packages/_lfcthr.so ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/_lfcthr.so
ln -sf ../python@PYTHON.VERSION@/site-packages/lfcthr.py ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/lfcthr.py

mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/LFC/test
cp test/perl/lfc/UI_validation_tests.pl ${RPM_BUILD_ROOT}%{prefix}/share/LFC/test/perl-interface-validation.pl
cp test/python/lfc/UI_validation_tests.py ${RPM_BUILD_ROOT}%{prefix}/share/LFC/test/python-interface-validation.py

mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/doc/LFC-client-%{version}
cp doc/lfc/README ${RPM_BUILD_ROOT}%{prefix}/share/doc/LFC-client-%{version}
cp doc/lfc/INSTALL-client ${RPM_BUILD_ROOT}%{prefix}/share/doc/LFC-client-%{version}

%clean

%files -n lfc
%defattr(-,root,root)
%attr(755,root,root) %{prefix}/bin/lfc-chgrp
%attr(755,root,root) %{prefix}/bin/lfc-chmod
%attr(755,root,root) %{prefix}/bin/lfc-chown
%attr(755,root,root) %{prefix}/bin/lfc-delcomment
%attr(755,root,root) %{prefix}/bin/lfc-entergrpmap
%attr(755,root,root) %{prefix}/bin/lfc-enterusrmap
%attr(755,root,root) %{prefix}/bin/lfc-getacl
%attr(755,root,root) %{prefix}/bin/lfc-listgrpmap
%attr(755,root,root) %{prefix}/bin/lfc-listusrmap
%attr(755,root,root) %{prefix}/bin/lfc-ln
%attr(755,root,root) %{prefix}/bin/lfc-ls
%attr(755,root,root) %{prefix}/bin/lfc-mkdir
%attr(755,root,root) %{prefix}/bin/lfc-modifygrpmap
%attr(755,root,root) %{prefix}/bin/lfc-modifyusrmap
%attr(755,root,root) %{prefix}/bin/lfc-ping
%attr(755,root,root) %{prefix}/bin/lfc-rename
%attr(755,root,root) %{prefix}/bin/lfc-rm
%attr(755,root,root) %{prefix}/bin/lfc-rmgrpmap
%attr(755,root,root) %{prefix}/bin/lfc-rmusrmap
%attr(755,root,root) %{prefix}/bin/lfc-setacl
%attr(755,root,root) %{prefix}/bin/lfc-setcomment
%attr(644,root,root) %{prefix}/share/doc/LFC-client-%{version}/README
%attr(644,root,root) %{prefix}/share/doc/LFC-client-%{version}/INSTALL-client
%attr(644,root,root) %{prefix}/share/man/man1/lfc-chgrp.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-chmod.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-chown.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-delcomment.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-entergrpmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-enterusrmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-getacl.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-listgrpmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-listusrmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-ln.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-ls.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-mkdir.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-modifygrpmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-modifyusrmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-ping.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-rename.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-rm.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-rmgrpmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-rmusrmap.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-setacl.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-setcomment.1

%files -n lfc-libs
%{prefix}/%{_lib}/liblfc.so.*

%files -n lfc-devel
%attr(644,root,root) %{prefix}/include/lfc/Castor_limits.h
%attr(644,root,root) %{prefix}/include/lfc/Cnetdb.h
%attr(644,root,root) %{prefix}/include/lfc/Cns_api.h
%attr(644,root,root) %{prefix}/include/lfc/Cns_constants.h
%attr(644,root,root) %{prefix}/include/lfc/Cns_struct.h
%attr(644,root,root) %{prefix}/include/lfc/lfc_api.h
%attr(644,root,root) %{prefix}/include/lfc/osdep.h
%attr(644,root,root) %{prefix}/include/lfc/serrno.h
%attr(755,root,root) %{prefix}/%{_lib}/liblfc.a
%attr(755,root,root) %{prefix}/%{_lib}/liblfc.so
%attr(644,root,root) %{prefix}/share/man/man3/lfc_aborttrans.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_access.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_accessr.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_addreplica.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_addreplicax.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_chdir.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_chmod.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_chown.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_closedir.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_creatg.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delcomment.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delete.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delfilesbyguid.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delfilesbyname.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delfilesbypattern.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delreplica.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delreplicas.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_delreplicasbysfn.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_endsess.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_endtrans.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_entergrpmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_enterusrmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getacl.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getcomment.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getcwd.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getgrpbygid.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getgrpbygids.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getgrpbynam.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getgrpmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getidmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getifcevers.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getlinks.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getreplica.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getreplicas.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getreplicasl.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getreplicass.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getreplicax.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getusrbynam.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getusrbyuid.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_getusrmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_lchown.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_listlinks.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_listrep4gc.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_listreplica.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_listreplicax.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_listrepset.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_lstat.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_mkdir.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_mkdirg.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_modifygrpmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_modifyusrmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_modreplica.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_modreplicax.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_opendirg.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_opendirxg.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_ping.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_readdir.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_readdirc.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_readdirg.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_readdirxc.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_readdirxp.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_readdirxr.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_readlink.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_registerfiles.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_rename.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_rewinddir.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_rmdir.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_rmgrpmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_rmusrmap.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setacl.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setatime.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setcomment.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_seterrbuf.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setfsize.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setfsizec.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setfsizeg.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setptime.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setratime.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setrltime.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setrstatus.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_setrtype.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_startsess.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_starttrans.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_stat.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_statg.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_statr.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_symlink.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_umask.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_undelete.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_unlink.3
%attr(644,root,root) %{prefix}/share/man/man3/lfc_utime.3

%files -n perl-lfc
%defattr(-,root,root)
%attr(755,root,root) @PERLDIR@/lfc.so
%attr(755,root,root) @PERLDIR@/lfc.pm
%attr(644,root,root) %{prefix}/share/man/man3/lfc_perl.3
%attr(755,root,root) %{prefix}/share/LFC/test/perl-interface-validation.pl

%files -n python-lfc
%dir %{prefix}/%{_lib}/python
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_lfc.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/lfc.py
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_lfcthr.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/lfcthr.py
%attr(755,root,root) %{prefix}/%{_lib}/python/_lfc.so
%attr(755,root,root) %{prefix}/%{_lib}/python/lfc.py
%attr(755,root,root) %{prefix}/%{_lib}/python/_lfcthr.so
%attr(755,root,root) %{prefix}/%{_lib}/python/lfcthr.py
%attr(644,root,root) %{prefix}/share/man/man3/lfc_python.3
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_lfc2.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/lfc2.py
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_lfc2thr.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/lfc2thr.py
%attr(644,root,root) %{prefix}/share/man/man3/lfc2_python.3
%attr(755,root,root) %{prefix}/share/LFC/test/python-interface-validation.py

%post -n lfc-libs
if [ `uname -m` != x86_64 -o \( `uname -m` = x86_64 -a "%{_lib}" = lib64 \) ]; then
   if [ `grep -c ^%{prefix}/%{_lib} /etc/ld.so.conf` = 0 ]; then
      echo "%{prefix}/%{_lib}" >> /etc/ld.so.conf
   fi
fi

[ -x "/sbin/ldconfig" ] && /sbin/ldconfig

%postun -n lfc-libs
[ -x "/sbin/ldconfig" ] && /sbin/ldconfig
