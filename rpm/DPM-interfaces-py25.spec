Summary: Disk Pool Manager
Name: DPM-client-py25
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
AutoReqProv: yes
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
BuildRequires: @REQUIRES.PERL@
BuildRequires: python >= 2.5
License: Apache-2.0
Prefix: @PREFIX@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
python 2.5 interface to DPM/DPNS

%package -n python25-dpm
Summary: Disk Pool Manager Interfaces
Group: grid/lcg
Requires: dpm-libs >= @VERSION@, python-dpm >= @VERSION@
AutoReqProv: yes
Obsoletes: DPM-interfaces-py25, DPM-interfaces2-py25
%description -n python25-dpm
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This is the Python interface to the DPM built with swig. 
It requires Python 2.5.

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-client-only dpm ${EXTRA_CONFIGURE_OPTIONS}
make

%install 
rm -rf $RPM_BUILD_ROOT

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

%clean

%files -n python25-dpm
%defattr(-,root,root)
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/_dpm.so
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/dpm.py
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/_dpm2.so
%attr(755,root,root) %{prefix}/%{_lib}/python2.5/site-packages/dpm2.py
