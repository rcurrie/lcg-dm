#
# Copyright (c) Members of the EGEE Collaboration. 2006-2009.
# See http://public.eu-egee.org/partners/ for details on 
# the copyright holders.
# For license conditions see the license file or
# http://www.apache.org/licenses/LICENSE-2.0
#
# Authors: 
#      Akos Frohner <Akos.Frohner@cern.ch>
#
# Makefile to build all RPMs in one go.

default: all

OS=$(shell ../scripts/get-platform-os)

define emibuildrequires
BuildRequires:  globus-gssapi-gsi-devel%{?_isa}\n
BuildRequires:  globus-gss-assist-devel%{?_isa}\n
BuildRequires:  globus-gsi-credential-devel%{?_isa}\n
BuildRequires:  globus-gsi-callback-devel%{?_isa}\n
BuildRequires:  globus-gass-copy-devel%{?_isa}\n
BuildRequires:  globus-ftp-client-devel%{?_isa}\n
BuildRequires:  globus-common-devel%{?_isa}\n
BuildRequires:  voms-devel%{?_isa}\n
BuildRequires:  gsoap-devel%{?_isa}\n
BuildRequires:  CGSI-gSOAP-devel%{?_isa} >= 1.3.4.0\n
BuildRequires:  CGSI_gSOAP_2.7-voms%{?_isa}\n
BuildRequires:  mysql-devel%{?_isa}\n
BuildRequires:  argus-pep-api-c-devel%{?_isa}\n
BuildRequires:  swig%{?_isa}\n
BuildRequires:  python-devel%{?_isa}\n
endef

define glitebuildrequires
endef

ifndef BUILDREQUIRES
BUILDREQUIRES=$(shell if (echo "$(EXTRA_CONFIGURE_OPTIONS)" | egrep -e "--with-emi" > /dev/null 2>&1); then echo "$(emibuildrequires)"; else echo "$(glitebuildrequires)"; fi;)
endif

ifndef VERSION
include ../VERSION
endif

ifndef RELEASE_SUFFIX
RELEASE_SUFFIX=$(RC)sec.$(OS)
endif

ifndef PREFIX
PREFIX =$(shell if (echo "$(EXTRA_CONFIGURE_OPTIONS)" | egrep -e "--with-emi" > /dev/null 2>&1); then echo "/usr" ; else echo "/opt/lcg"; fi;)
endif

ifeq ($(GLOBUS_LOCATION),)
GLOBUS_LOCATION=$(shell awk '/^.define.*GlobusLocation/ { print $$3 }' ../config/site.def)
endif

LIBDIR='lib'
ifeq ($(shell uname -m),x86_64)
    LIBDIR='lib64'
endif
ifeq ($(PERLDIR),)
PERLDIR =$(shell if (echo "$(EXTRA_CONFIGURE_OPTIONS)" | egrep -e "--with-emi" > /dev/null 2>&1); then perl -MConfig -e 'print $$Config{vendorarchexp}'; else echo "$(PREFIX)/$(LIBDIR)/perl"; fi;)
endif

REQUIRES.VOMS =$(shell if (echo "$(EXTRA_CONFIGURE_OPTIONS)" | egrep -e "--with-emi" > /dev/null 2>&1); then echo "voms" ; else echo "glite-security-voms-api-cpp"; fi;)

ifndef ORACLE_INSTANTCLIENT_LOCATION
ORACLE_INSTANTCLIENT_LOCATION=/usr
endif
ifndef ORACLE_INSTANTCLIENT_VERSION
ORACLE_INSTANTCLIENT_VERSION=10.2.0.3
endif
ORACLE_INSTANTCLIENT_LIBDIR=$(ORACLE_INSTANTCLIENT_LOCATION)/lib/oracle/$(ORACLE_INSTANTCLIENT_VERSION)/client/lib/

PYTHON_VERSION=$(shell python -c "import sys; print sys.version[:3]")

#all: rpm-lcg-dm-common rpm-LFC-mysql rpm-DPM-mysql rpm-DPM-dicom-mysql
all: client
ifneq ($(PROC_INIT),)
all: rpm-LFC-oracle rpm-DPM-oracle
endif
all:
	mkdir -p ../RPMS
	cp build-*/RPMS/*/*.rpm ../RPMS/
	cp build-*/SRPMS/*.rpm ../RPMS/

install: install-lcg-dm-common install-LFC-mysql install-DPM-mysql

client: rpm-lcg-dm-common rpm-LFC-client rpm-DPM-client
	mkdir -p ../RPMS
	cp build-*/RPMS/*/*.rpm ../RPMS/
	cp build-*/SRPMS/*.rpm ../RPMS/

client-install: install-lcg-dm-common install-LFC-client install-DPM-client

py25: rpm-LFC-py25 rpm-DPM-py25
	mkdir -p ../RPMS
	cp build-*/RPMS/*/*.rpm ../RPMS/
	cp build-*/SRPMS/*.rpm ../RPMS/

py26: rpm-LFC-py26 rpm-DPM-py26
	mkdir -p ../RPMS
	cp build-*/RPMS/*/*.rpm ../RPMS/
	cp build-*/SRPMS/*.rpm ../RPMS/

check-proc:
	source $(PROC_INIT); which proc

SOURCE_DIR=LCG-DM-$(VERSION)
SOURCE_TAR=$(SOURCE_DIR).tar.gz

$(SOURCE_TAR):
	cd ..; \
	rm -rf $(SOURCE_DIR); \
	mkdir $(SOURCE_DIR); \
	cp -rp Imakefile Makefile.ini Makefile.ini.Win32 README VERSION \
		config imake test setosflags configure doc scripts \
		h lib shlib common ns rfio dpm srmv1 srmv2 srmv2.2 security \
		dli dicomcopy dpmcopy $(SOURCE_DIR); \
	find $(SOURCE_DIR) -name .svn -print0 | xargs -0 rm -rf ; \
	tar -czf $(SOURCE_TAR) $(SOURCE_DIR); \
	rm -rf $(SOURCE_DIR)
	mv ../$(SOURCE_TAR) .

source: $(SOURCE_TAR)

build-rpm: source
	mkdir -p build-rpm
	mkdir -p build-rpm/BUILD
	mkdir -p build-rpm/RPMS
	mkdir -p build-rpm/SRPMS
	mkdir -p build-rpm/SOURCES
	mkdir -p build-rpm/SPECS
	cp $(SOURCE_TAR) build-rpm/SOURCES
	for spec in *.spec; do \
		sed -e 's/@VERSION@/$(VERSION)/g;' \
			-e 's/@RELEASE@/$(RELEASE)/g;' \
			-e 's/@SECURITY@/$(RELEASE_SUFFIX)/g;' \
			-e 's#@PREFIX@#$(PREFIX)#g;' \
			-e 's/@PYTHON.VERSION@/$(PYTHON_VERSION)/g;' \
			-e 's/@REQUIRES.VOMS@/$(REQUIRES.VOMS)/g;' \
			-e 's,@PERLDIR@,$(PERLDIR),g;' \
			-e 's/BuildRequires: @BUILDREQUIRES@/$(BUILDREQUIRES)/g;' \
			-e 's#$${EXTRA_CONFIGURE_OPTIONS}#$(EXTRA_CONFIGURE_OPTIONS)#;' \
			-f $(OS).spec.requires $$spec >build-rpm/SPECS/$$spec; \
	done

###############################################################################
# common
###############################################################################

BUILD_COMMON=build-rpm/BUILD/lcg-dm-common-$(VERSION)

rpm-lcg-dm-common $(BUILD_COMMON): build-rpm 
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/lcg-dm-common.spec

install-lcg-dm-common: $(BUILD_COMMON)
	if [ ! -d "$(prefix)" ]; then mkdir -p "$(prefix)"; fi
	cd $(BUILD_COMMON); $(MAKE) prefix=$(prefix) install install.man

###############################################################################
# LFC client
###############################################################################

BUILD_LFC_CLIENT=build-rpm/BUILD/LFC-client-$(VERSION)

rpm-LFC-client $(BUILD_LFC_CLIENT): build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/LFC-client.spec

install-LFC-client: $(BUILD_LFC_CLIENT)
	if [ ! -d "$(prefix)" ]; then mkdir -p "$(prefix)"; fi
	cd $(BUILD_LFC_CLIENT); $(MAKE) prefix=$(prefix) install install.man

###############################################################################
# DPM client
###############################################################################

BUILD_DPM_CLIENT=build-rpm/BUILD/DPM-client-$(VERSION)

rpm-DPM-client $(BUILD_DPM_CLIENT): build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/DPM-client.spec

install-DPM-client: $(BUILD_DPM_CLIENT)
	if [ ! -d "$(prefix)" ]; then mkdir -p "$(prefix)"; fi
	cd $(BUILD_DPM_CLIENT); $(MAKE) prefix=$(prefix) install install.man

###############################################################################
# LFC MySQL
###############################################################################

BUILD_LFC_MYSQL=build-rpm/BUILD/LFC-mysql-$(VERSION)

rpm-LFC-mysql $(BUILD_LFC_MYSQL): build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/LFC-mysql.spec

install-LFC-mysql: $(BUILD_LFC_MYSQL)
	if [ ! -d "$(prefix)" ]; then mkdir -p "$(prefix)"; fi
	cd $(BUILD_LFC_MYSQL); $(MAKE) prefix=$(prefix) install install.man

###############################################################################
# DPM MySQL
###############################################################################

BUILD_DPM_MYSQL=build-rpm/BUILD/DPM-mysql-$(VERSION)

rpm-DPM-mysql $(BUILD_DPM_MYSQL): build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/DPM-mysql.spec

install-DPM-mysql: $(BUILD_DPM_MYSQL)
	if [ ! -d "$(prefix)" ]; then mkdir -p "$(prefix)"; fi
	cd $(BUILD_DPM_MYSQL); $(MAKE) prefix=$(prefix) install install.man

###############################################################################
# DPM-DICOM MySQL
###############################################################################

rpm-DPM-dicom-mysql: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/DPM-dicom.spec

###############################################################################
# LFC Oracle
###############################################################################

rpm-LFC-oracle: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(ORACLE_INSTANTCLIENT_LIBDIR):$(LD_LIBRARY_PATH); \
	source $(PROC_INIT); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/LFC-oracle.spec

###############################################################################
# DPM Oracle
###############################################################################

rpm-DPM-oracle: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(ORACLE_INSTANTCLIENT_LIBDIR):$(LD_LIBRARY_PATH); \
	source $(PROC_INIT); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/DPM-oracle.spec

###############################################################################
# LFC PostgreSQL
###############################################################################

rpm-LFC-postgres: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/LFC-postgres.spec

###############################################################################
# DPM PostgreSQL
###############################################################################

rpm-DPM-postgres: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/DPM-postgres.spec

###############################################################################
# LFC Python 2.5
###############################################################################

rpm-LFC-py25: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/LFC-interfaces-py25.spec

###############################################################################
# LFC Python 2.6
###############################################################################

rpm-LFC-py26: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/LFC-interfaces-py26.spec

###############################################################################
# DPM Python 2.5
###############################################################################

rpm-DPM-py25: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/DPM-interfaces-py25.spec

###############################################################################
# DPM Python 2.6
###############################################################################

rpm-DPM-py26: build-rpm
	export LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); \
	rpmbuild --nodeps --define "_topdir $(PWD)/build-rpm" -ba build-rpm/SPECS/DPM-interfaces-py26.spec

clean:
	-rm -rf ../RPMS build-rpm $(SOURCE_TAR)


