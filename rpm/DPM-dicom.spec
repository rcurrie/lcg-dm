Summary: Disk Pool Manager
Name: DPM-dicom
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
AutoReqProv: yes
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
License: Apache-2.0
Prefix: @PREFIX@
BuildRequires: @BUILDREQUIRES@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.

%package server-mysql
Summary: DPM MySQL Server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, @REQUIRES.MYSQL@
Provides: DPM-server-mysql
Conflicts: DPM-server-oracle
AutoReqProv: yes
%description server-mysql
DPM server with MySQL database back-end and
support for additional storage back-ends.

%package copyd-mysql
Summary: DPM DICOM storage back-end
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, @REQUIRES.MYSQL@
AutoReqProv: yes
%description copyd-mysql
DICOM storage back-ends for DPM.

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-mysql --with-dicom dpm ${EXTRA_CONFIGURE_OPTIONS}
make


%install 
rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpns
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpm
mkdir -p ${RPM_BUILD_ROOT}/var/log/dicomcopy
mkdir -p ${RPM_BUILD_ROOT}/etc/logrotate.d
mkdir -p ${RPM_BUILD_ROOT}/etc/init.d
mkdir -p ${RPM_BUILD_ROOT}/etc/sysconfig
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/etc
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/man/man3

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

# For the DPM
echo "<username>/<password>@<host>" >${RPM_BUILD_ROOT}%{prefix}/etc/DPMCONFIG.templ
cp dpm/dpm_mysql_tbl.sql ${RPM_BUILD_ROOT}%{prefix}/share/DPM/create_dpm_tables_mysql.sql
cp -p dpm/dpm.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dpm
cp -p dpm/dpm.scripts.mysql ${RPM_BUILD_ROOT}/etc/init.d/dpm
cp -p dpm/dpm.sysconfig.mysql ${RPM_BUILD_ROOT}/etc/sysconfig/dpm.templ

# For dicomcopyd
cp dicomcopy/dicomcopy_mysql_tbl.sql ${RPM_BUILD_ROOT}%{prefix}/share/DPM/create_dicomcopy_tables_mysql.sql
cp -p dicomcopy/dicomcopyd.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dicomcopyd
cp -p dicomcopy/dicomcopyd.scripts.mysql ${RPM_BUILD_ROOT}/etc/init.d/dicomcopyd
cp -p dicomcopy/dicomcopyd.sysconfig.mysql ${RPM_BUILD_ROOT}/etc/sysconfig/dicomcopyd.templ

%clean

%files server-mysql
%defattr(-,root,root)
/var/log/dpm
%attr(755, root, root) %{prefix}/bin/dpm
%attr(755, root, root) %{prefix}/bin/dpm-buildfsv
%attr(755, root, root) %{prefix}/bin/dpm-shutdown
%attr(600, root, root) %{prefix}/etc/DPMCONFIG.templ
%{prefix}/share/DPM/create_dpm_tables_mysql.sql
%{prefix}/share/man/man1/dpm.1
%{prefix}/share/man/man1/dpm-buildfsv.1
%{prefix}/share/man/man1/dpm-shutdown.1
%attr(644, root, root) /etc/logrotate.d/dpm
%attr(755, root, root) /etc/init.d/dpm
%attr(644, root, root) /etc/sysconfig/dpm.templ

%files copyd-mysql
%defattr(-,root,root)
/var/log/dicomcopy
%attr(755, root, root) %{prefix}/bin/dicomcopyd
%{prefix}/share/DPM/create_dicomcopy_tables_mysql.sql
%{prefix}/share/man/man1/dicomcopyd.1
%attr(644, root, root) /etc/logrotate.d/dicomcopyd
%attr(755, root, root) /etc/init.d/dicomcopyd
%attr(644, root, root) /etc/sysconfig/dicomcopyd.templ

%post server-mysql
/sbin/chkconfig --add dpm

echo "The DPM is now installed."
echo "Please use the <install_dir>/etc/DPMCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the DPM daemon, use /etc/sysconfig/dpm.templ to create the /etc/sysconfig/dpm file and modify it if necessary."
echo " "
echo "Then, to start/stop the DPM server, use the following command :"
echo " > service dpm start|stop"
echo " "

%post copyd-mysql
/sbin/chkconfig --add dicomcopyd

echo "The DPM DICOM back-end is now installed."
echo "Before running the DPM daemon, use /etc/sysconfig/dicomcopyd.templ to create the /etc/sysconfig/dicomcopyd file and modify it if necessary."
echo " "
echo "Then, to start/stop the DPM server, use the following command :"
echo " > service dicomcopyd start|stop"

