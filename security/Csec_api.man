.\" @(#)$RCSfile: Csec_api.man,v $ $Revision$ $Date$ CERN IT-ADC/CA Benjamin Couturier
.\" Copyright (C) 2004-2010 by CERN/IT/ADC/CA
.\" All rights reserved
.\"
.TH CSEC_API 3 "$Date$" CASTOR "CASTOR Security Functions"
.SH NAME
Csec_api \- Provides authentication in CASTOR services
.SH SYNOPSIS
Header file:
.sp
\fB#include <Csec_api.h>\fR
.sp
On the client side:
.sp
.BI "int Csec_client_initContext(Csec_context_t *" ctx,
.BI "int " service_type,
.BI "Csec_protocol *" protocols );
.br
.BI "int Csec_client_establishContext(Csec_context_t *" ctx,
.BI "int " socket );
.br
.BI "int Csec_client_setSecurityOpts(Csec_context_t *" ctx,
.BI "int " opt );
.br
.BI "int Csec_client_setAuthorizationId(Csec_context_t *" ctx,
.BI "const char *" mech, 
.BI "const char *" name );
.br
.BI "int Csec_client_setVOMS_data(Csec_context_t *" ctx,
.BI "const char *" voname,
.BI "char **" fqan,
.BI "int " nbfqan );
.sp
On the server side:
.sp
.BI "int Csec_server_initContext(Csec_context_t *" ctx,
.BI "int " service_type ,
.BI "Csec_protocol *" protocols );
.br
.BI "int Csec_server_reinitContext (Csec_context_t *" ctx,
.BI "int " service_type ,
.BI "Csec_protocol *" protocols );
.br
.BI "int Csec_server_establishContext (Csec_context_t *" ctx,
.BI "int " socket );
.br
.BI "int Csec_server_getClientId(Csec_context_t *" ctx,
.BI "char **" mech,
.BI "char **" name );
.br
.BI "int Csec_server_getAuthorizationId(Csec_context_t *" ctx,
.BI "char **" mech,
.BI "char **" name );
.br
.BI "int Csec_server_getDelegatedCredentials(Csec_context_t *" ctx,
.BI "char **" mech, " void ** " buf, " size_t *" size ");"
.br
.BI "int Csec_server_setSecurityOpts(Csec_context_t *" ctx,
.BI "int " opt );
.br
.BI "char *Csec_server_get_client_ca(Csec_context_t *" ctx);
.br
.BI "char *Csec_server_get_client_vo(Csec_context_t *" ctx);
.br
.BI "char **Csec_server_get_client_fqans(Csec_context_t *" ctx,
.BI "int *" nbfqan);
.sp
Common functions:
.sp
.BI "int Csec_clearContext(Csec_context_t *" ctx );
.br
.BI "int Csec_getErrorMessage();"
.br
.BI "int Csec_getErrorMessageSummary(size_t " maxlen );
.br
.BI "int Csec_mapToLocalUser(const char *" mech,
.BI "const char *" name,
.BI "char *" username,
.BI "size_t " username_size,
.BI "uid_t *" uid,
.BI "gid_t *" gid );
.br
.BI "Csec_context_t * Csec_get_default_context();"

.SH DESCRIPTION
.B Csec_api
functions allow for the implimentation of strong authentication mechanisms in CASTOR servers and clients. Csec_api is integrated with the CASTOR 
framework for errors.
.sp
.TP
.B Csec_client_initContext, Csec_server_initContext, Csec_server_reinitContext
allow to initialize the 
.B Csec_context_t
structure. The service type parameter defines which type of key will be used by the service. Its value can be:
.RS
.TP 
.B CSEC_SERVICE_TYPE_HOST 
A normal host key (e.g. host/machine_name@DOMAIN for KRB5) will be used
.TP 
.B
CSEC_SERVICE_TYPE_CENTRAL
A  CASTOR Central host type key (e.g. castorcentral/machine_name@DOMAIN for KRB5) will be used
.TP 
.B CSEC_SERVICE_TYPE_DISK
A  CASTOR disk host type key (e.g. castordisk/machine_name@DOMAIN for KRB5) will be used
.TP 
.B CSEC_SERVICE_TYPE_TAPE
A  CASTOR tape host type key (e.g. castortape/machine_name@DOMAIN for KRB5) will be used
.RE
.TP
.B Csec_client_establishContext, Csec_server_establishContext
Given an initialized context and an opened socket, establish a security context according to the chosen security mechanism.
.TP
.B Csec_client_setSecurityOpts, Csec_server_setSecurityOpts
Given an initialized context, but one that has not yet been used to establish a security context
these functions allow the selection of various options. Currently supported are:

    CSEC_OPT_DELEG_FLAG

      Requests that delegated credentials from the client are made available to the server. Either the client or server may set this option and it will automaticaly limit the selection of authentication method to one that supports delegation. (Currently only GSI)

    CSEC_OPT_NODELEG_FLAG

      This directs that client/server to disallow delegation. If the other side requests delegation the establishing of a security context will fail.

If neither side sets any options the default behaviour is to not delegate a credential.
.TP
.B Csec_server_getClientId
Allows a server to retrieve the authentication mechanism and identification name
(eg. principal or DN) from an established context, ctx. If either of mech or name
are NULL no pointer will be returned. The strings that are returned are associated
to the context, ctx, so should only be used while the context ctx is valid. If
required they should be copied before the context is reset or cleared.
.TP
.B Csec_client_setAuthorizationId, Csec_server_getAuthorizationId
On the client side an 'AuthorizationId' may be set against an initialized but
not yet established context. The AuthorizationId, consisting of the pair mech and name,
may be treated as any arbitrary pair of strings up to CA_MAXCSECPROTOLEN, CA_MAXCSECNAMELEN
in length. The strings will be made available to the server. mech is supposed to
represent the mechanism type and name should be an identifying string such as principal
or DN.

On the server side, the AuthorizationId may be retrieved after the security
context is established. If the client did not set any id the server will
receive an error when Csec_server_getAuthorizationId() is called. Pointers
to the mechanism and the name will be returned in mech and name. Either may
be set to NULL, in which case no pointer is returned. Upon successful return
the list of VOMS fqans and the VOMS voname available to the server will also
be reset to those which the client set manually, or will be emptied if the
client did not set any. The strings returned are associated with the context
and should be copied before the context is reset or cleared.
.TP
.B Csec_server_getDelegatedCredential
Allows a server to retrieve a copy of any delegated credential available
from an established context, ctx. The credential is returned in buf. The size
of the data in the buffer is returned in size. The data should be treated as an opaque structure, the meaning of
which depends on the authentication scheme that was used. The scheme name
is returned in mech. Currently only mech 'GSI' supports credential
delegation and the client or server must request delegation passing by
setting the appropriate flag with Csec_client_setSecurityOpts() or
Csec_server_setSecurityOpts(). The GSI credential data is suitable for passing to
gss_import_cred(). mech and buf will point to data conatined within the context, ctx,
and should only be used while ctx remains valid.
.TP
.B Csec_mapToLocalUser
This function determines whether an ID (mechanism, name pair) can be mapped to a local uid/gid and/or username. If the username
is wanted a buffer should be passed in username, the size of which is indicated with username_size. If the uid/gid are required
uid and gid should be passed. Any of username, uid or gid may be NULL in which case they are not returned. If both uid and gid
are NULL and username is non-NULL the mapped username is not required to exist on the local system. ie. the function will succeed
as long as a mapping exists. If either of uid or gid are non-NULL the mapping and local username must exist, otherwise
Csec_mapToLocalUser() will return an error.
.TP
.B Csec_clearContext 
Clears the context and deallocates the memory used.
.TP
.B Csec_get_default_context
Utility function that provides the applications with one default per thread security context that can be used by the security layer.

.SH ERROR HANDLING
In case of errors in the Csec_pai layer, the functions return -1 (or NULL for the functions returning strings), the serrno is set accordingly. It is possible to get the detailed error message by using the
.B Csec_getErrorMessage()
or
.B Csec_getErrorMessageSummary()
functions. The Csec_getErrorMessageSummary() function will return a summary message that should need at most maxlen bytes
of storage (including the terminating null). The detail of the message may be cut in various ways to reduce the length
to fix in the specified length.


.SH ENVIRONMENT
.TP
.B CSEC_MECH
environment variable or 
.B "CSEC MECH" 
entry in 
.B /etc/shift.conf 
.br
This environment variable contains the list of protocols to be used for authentication. The variable has precedence over the 
CASTOR configuration file.
.br
On the client side, this list is used to choose which security mechanism should be used to address the server.
On the server side, this is is of list of mechanisms that should be accepted by the server.
.TP
.B CSEC_TRACE
If defined switches the tracing on in the CASTOR security module. Tracing information is sent to stderr, unless
.B CSEC_TRACEFILE is defined.
.TP
.B CSEC_TRACEFILE
If defined, the CASTOR security tracing information is written to afile which name is the value of this variable.

.SH SECURITY MECHANISMS
The currently supported methods for authentication are:
.TP
.BI KRB5  
Kerberos 5 mechanism
.TP
.BI KRB4  
Kerberos 4 mechanism
.TP
.BI GSI
Globus Security Infrastructure
.TP
.BI ID
Unsecure protocol, should be used for testing only.

.SH NOTE ON THREAD SAFETY
If the Csec_api library was compiled thread safe (eg. was built defining the _THREAD_SAFE
macro, which is the standard way) then the library should be thread safe. If the
application using Csec_api also defines _THREAD_SAFE, Csec attempts to use thread safe
versions of any underlying security libraries that are used for the authentication service.

For instance, in the case of GSI the thread safe version of Globus may, in areas other
than security, sometimes create threads. If the application using Csec_api needs to link
to the GSI libraries for its own use then threading flavour should be consistent. Therefore
if the non threaded Globus libraries are required then do not define the _THREAD_SAFE macro.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
