How to set up an DPM nameserver with a mysql backend
====================================================

The DPM nameserver (DPNS) is run by the dpmmgr user. This user
requires a copy of the host certificate files:

  cp -p /etc/grid-security/hostcert.pem /etc/grid-security/dpmmgr/dpmcert.pem
  cp -p /etc/grid-security/hostkey.pem /etc/grid-security/dpmmgr/dpmkey.pem
  chown dpmmgr:dpmmgr /etc/grid-security/dpmmgr/dpm*

Create the DPNS database on the mysql server:

  mysql -u root < /usr/share/dpm/create_dpns_tables_mysql.sql

Select a username and password for this mysql database table. Replace
<user> and <password> in the commands below with your choice, also
replace <hostname> with the hostname of the DPNS server:

  mysql -u root
  mysql> use mysql
  mysql> grant all on cns_db.* to <user>@localhost identified by '<password>';
  mysql> grant all on cns_db.* to <user>@<hostname> identified by '<password>';
  mysql> quit

Let the DPNS server know about the account information and make sure
only the dpmmgr user can read this information:

  touch /etc/DPNSCONFIG
  chmod 600 /etc/DPNSCONFIG
  chown dpmmgr:dpmmgr /etc/DPNSCONFIG
  echo <user>/<password>@<dbserver>/<dbname> > /etc/DPNSCONFIG

If the /<dbname> part is omitted the database "cns_db" will be used.

If you have a firewall, open the DPNS server port (5010).

Once the configuration is completed, start the DPNS server:

  service dpm-mysql-nameserver start

If you want to start the service automatically at boot time:

  chkconfig --add dpm-mysql-nameserver
