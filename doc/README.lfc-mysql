How to set up an LFC server with a mysql backend
================================================

The LFC server is run by the lfcmgr user. This user requires a copy of
the host certificate files:

  cp -p /etc/grid-security/hostcert.pem /etc/grid-security/lfcmgr/lfccert.pem
  cp -p /etc/grid-security/hostkey.pem /etc/grid-security/lfcmgr/lfckey.pem
  chown lfcmgr:lfcmgr /etc/grid-security/lfcmgr/lfc*

Create the LFC database on the mysql server:

  mysql -u root < /usr/share/lfc/create_lfc_tables_mysql.sql

Select a username and password for this mysql database table. Replace
<user> and <password> in the commands below with your choice, also
replace <hostname> with the hostname of the LFC server:

  mysql -u root
  mysql> use mysql
  mysql> grant all on cns_db.* to <user>@localhost identified by '<password>';
  mysql> grant all on cns_db.* to <user>@<hostname> identified by '<password>';
  mysql> quit

Let the LFC server know about the account information and make sure
only the lfcmgr user can read this information:

  touch /etc/NSCONFIG
  chmod 600 /etc/NSCONFIG
  chown lfcmgr:lfcmgr /etc/NSCONFIG
  echo <user>/<password>@<dbserver>/<dbname> > /etc/NSCONFIG

If the /<dbname> part is omitted the database "cns_db" will be used.

If you have a firewall, open the LFC server port (5010).

Once the configuration is completed, start the LFC server:

  service lfc-mysql start

If you want to start the service automatically at boot time:

  chkconfig --add lfc-mysql
