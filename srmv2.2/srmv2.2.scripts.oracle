#!/bin/sh
#
# srmv2.2         Start/Stop srmv2.2
#
# chkconfig: - 94 16
# description: SRMV2.2 server daemon
#
# $Id$
#
# @(#)$RCSfile: srmv2.2.scripts.oracle,v $ $Revision$ $Date$ CERN/IT/ADC/CA Jean-Damien Durand
#

#
## PLEASE USE sysconfig FILE TO CHANGE THESE ENVIRONMENT VARIABLES
#
PREFIX=/opt/lcg
ORACLE_HOME=/usr/lib/oracle/10.2.0.1/client
export ORACLE_HOME
GRIDMAPDIR=/etc/grid-security/gridmapdir
export GRIDMAPDIR
GRIDMAP=/etc/grid-security/grid-mapfile
export GRIDMAP
CSEC_MECH=ID
export CSEC_MECH
DPMUSER=dpmmgr
DPMGROUP=dpmmgr
SRMV2DAEMONLOGFILE=/var/log/srmv2.2/log
DPMCONFIGFILE=/opt/lcg/etc/DPMCONFIG
NB_THREADS=20

sysname=`uname -s`

# source function library
if [ -r /etc/rc.d/init.d/functions ]; then
    . /etc/rc.d/init.d/functions
    DAEMON="daemon --check srmv2.2"
    FAILURE=failure
    ECHO_FAILURE=echo_failure
    SUCCESS=success
    ECHO_SUCCESS=echo_success
    ECHO_END=echo
    if [ $sysname = "Linux" ]; then
        ECHOOPT=-n
    fi
else
    DAEMON=eval
    FAILURE=
    ECHO_FAILURE=
    SUCCESS=
    ECHO_SUCCESS=
    ECHOOPT=
    ECHO_END=
fi

RETVAL=0
prog="srmv2.2"
PIDFILE=/var/run/srmv2.2.pid
PIDDIR=/var/run
SUBSYS=/var/lock/subsys/srmv2.2
SUBSYSDIR=/var/lock/subsys
[ -z "$SILENTSTOP" ] && SILENTSTOP=0
SRMV2DAEMON=$PREFIX/sbin/srmv2.2
SRMV2SHUTDOWN=$PREFIX/sbin/srmv2-shutdown
if [ -r $PREFIX/etc/srmv2.2.conf ]; then
    SYSCONFIGSRMV2DAEMON=$PREFIX/etc/srmv2.2.conf
elif [ -r /etc/sysconfig/srmv2.2 ]; then
    SYSCONFIGSRMV2DAEMON=/etc/sysconfig/srmv2.2
elif [ -r /etc/default/srmv2.2 ]; then
    SYSCONFIGSRMV2DAEMON=/etc/default/srmv2.2
fi

#
## Blindly attempt to create useful directories
#
# [ ! -d $PIDDIR ] && mkdir -p $PIDDIR > /dev/null 2>&1
# [ ! -d $SUBSYSDIR ] && mkdir -p $SUBSYSDIR > /dev/null 2>&1

if [ $sysname = "HP-UX" ]; then
    export UNIX95=1
fi
if [ $sysname = "SunOS" ]; then
    format4comm="fname"
elif [ $sysname = "Darwin" ]; then
    format4comm="ucomm"
else
    format4comm="comm"
fi

case "$1" in
  start)
    DPMUSERHOME=`finger $DPMUSER | grep -i directory: | cut -f 2 -d' '`
    TNS_ADMIN=$DPMUSERHOME/.tnsadmin
    export TNS_ADMIN
    X509_USER_CERT=/etc/grid-security/$DPMUSER/dpmcert.pem
    export X509_USER_CERT
    X509_USER_KEY=/etc/grid-security/$DPMUSER/dpmkey.pem
    export X509_USER_KEY

    netstat -an | egrep '[:.]8446' | egrep 'LISTEN *$' > /dev/null
    if [ $? -eq 0 ]; then
        echo $ECHOOPT "srmv2.2 already started: "
        [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "srmv2.2 already started: "
        RETVAL=0
    else
        if [ -n "$SYSCONFIGSRMV2DAEMON" ]; then
            #
            ## Source the configuration
            #
            . $SYSCONFIGSRMV2DAEMON
            if [ "${RUN_SRMV2DAEMON}" != "yes" ]; then
                echo $ECHOOPT "$SYSCONFIGSRMV2DAEMON says NO: "
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "$SYSCONFIGSRMV2DAEMON says NO: "
                RETVAL=0
                $ECHO_END
                exit $RETVAL
            fi
            export DPM_HOST
            export DPNS_HOST
            if [ -n "${ULIMIT_N}" ]; then
                ulimit -n ${ULIMIT_N}
            fi
        fi
        if [ ! -s "$DPMCONFIGFILE" ]; then
            echo $ECHOOPT "config file $DPMCONFIGFILE empty: "
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "config file $DPMCONFIGFILE empty: "
            RETVAL=1
        fi
        if [ ! -d $ORACLE_HOME ]; then
            echo $ECHOOPT "No Oracle Client found: "
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "No Oracle Client found: "
            RETVAL=1
        fi

        # get TWO_TASK or ORACLE_SID
        if [ "${DB_IS_LOCAL}" != "yes" ]; then
            TWO_TASK=`head -1 $DPMCONFIGFILE | cut -f2 -d@`
            export TWO_TASK
        else
            ORACLE_SID=`head -1 $DPMCONFIGFILE | cut -f2 -d@`
            export ORACLE_SID
        fi

        LD_LIBRARY_PATH=$ORACLE_HOME/lib:/opt/glite/lib:/opt/globus/lib:$LD_LIBRARY_PATH
        export LD_LIBRARY_PATH
        ORACLECLIENT=`ldconfig -p | grep libclntsh`
        if [ "x${ORACLECLIENT}" == "x" ]; then
            echo $ECHOOPT "No Oracle Client installed: "
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "No Oracle Client installed: "
            RETVAL=1
        fi

        if [ $RETVAL -eq 0 ]; then
            echo $ECHOOPT "Starting $prog: "
            cd /
            mkdir -p `dirname $SRMV2DAEMONLOGFILE`
            chown $DPMUSER:$DPMGROUP `dirname $SRMV2DAEMONLOGFILE`
            if [ "${ALLOW_COREDUMP}" != "yes" ]; then
                $DAEMON "su $DPMUSER -c \"LD_LIBRARY_PATH=$LD_LIBRARY_PATH $SRMV2DAEMON -t $NB_THREADS -c $DPMCONFIGFILE -l $SRMV2DAEMONLOGFILE\""
            else
                $DAEMON "su $DPMUSER -c \"mkdir -p $DPMUSERHOME/srmv2.2; cd $DPMUSERHOME/srmv2.2; hostname -f >> logstart; date >> logstart; ulimit -c unlimited; pwd >> logstart; ulimit -c >> logstart; echo DAEMON $SRMV2DAEMON \`hostname -f\` >> logstart; LD_LIBRARY_PATH=$LD_LIBRARY_PATH $SRMV2DAEMON -t $NB_THREADS -c $DPMCONFIGFILE -l $SRMV2DAEMONLOGFILE\""
            fi
            if [ $? -eq 0 ]; then
                [ -d $SUBSYSDIR ] && touch $SUBSYS
                if [ -d $PIDDIR ]; then
                    pid=`ps -eo pid,ppid,$format4comm | grep " 1 srmv2.2$" | awk '{print $1}'`
                    # The point of $PIDFILE is that it kills only
                    # the master daemon.
                    rm -f $PIDFILE
                    echo $pid > $PIDFILE
                fi
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Starting $prog: "
                RETVAL=0
            else
                [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Starting $prog: "
                RETVAL=1
            fi
        fi
    fi
    $ECHO_END
    ;;
  stop)
    if [ -f $PIDFILE ]; then
        echo $ECHOOPT "Stopping $prog: "
        if [ -x $SRMV2SHUTDOWN ]; then
            $SRMV2SHUTDOWN -f -h `hostname` > /dev/null 2>&1
            RETVAL=$?
            if [ $RETVAL -ne 0 ]; then
                kill -2 -`cat $PIDFILE` > /dev/null 2>&1
                RETVAL=$?
            fi
        else
            kill -2 -`cat $PIDFILE` > /dev/null 2>&1
            RETVAL=$?
        fi
        if [ $RETVAL -eq 0 ]; then
            rm -f $PIDFILE
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
        else
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
        fi
    else
        # srmv2.2 might have been started by hand
        pid=`ps -eo pid,ppid,$format4comm | grep " 1 srmv2.2$" | awk '{print $1}'`
        if [ -n "$pid" ]; then
            echo $ECHOOPT "Stopping $prog: "
            if [ -x $SRMV2SHUTDOWN ]; then
                $SRMV2SHUTDOWN -f -h `hostname` > /dev/null 2>&1
                RETVAL=$?
                if [ $RETVAL -ne 0 ]; then
                    kill -2 -$pid > /dev/null 2>&1
                    RETVAL=$?
                fi
            else
                kill -2 -$pid > /dev/null 2>&1
                RETVAL=$?
            fi
            if [ $RETVAL -eq 0 ]; then
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
            else
                [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
            fi
        else
            echo $ECHOOPT "srmv2.2 already stopped: "
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "srmv2.2 already stopped: "
            [ $SILENTSTOP -eq 0 ] && RETVAL=0 || RETVAL=1
        fi
    fi

    lpid="X"
    while [ "x$lpid" != "x" ]; do
        sleep 1
        #Does not seem to work on SunOS ;-)
        lpid=`ps -eo pid,ppid,$format4comm | grep " 1 srmv2.2$" | awk '{print $1}'`
    done

    [ -d $SUBSYSDIR ] && rm -f $SUBSYS
    $ECHO_END
    ;;
  restart | force-reload)
    $0 stop
    if [ $? -eq 0 -o $SILENTSTOP -eq 0 ]; then
        sleep 5
        $0 start
        RETVAL=$?
    else
        RETVAL=0
    fi
    ;;
  condrestart | try-restart)
    SILENTSTOP=1
    export SILENTSTOP
    $0 restart
    RETVAL=$?
    ;;
  reload)
    ;;
  status)
    pid=`ps -eo pid,ppid,$format4comm | grep " 1 srmv2.2$" | awk '{print $1}'`
    if [ -n "$pid" ]; then
        echo $ECHOOPT "srmv2.2 (pid $pid) is running..."
        $ECHO_SUCCESS
        $ECHO_END
        RETVAL=0
    else
        if [ -f $PIDFILE ]; then
            pid=`head -1 $PIDFILE`
            if [ "$pid" != "" ] ; then
                echo $ECHOOPT "srmv2.2 dead but pid file exists"
                $ECHO_FAILURE
                $ECHO_END
                RETVAL=1
            else
                echo $ECHOOPT "srmv2.2 dead"
                $ECHO_FAILURE
                $ECHO_END
                RETVAL=1
            fi
        else
            if [ -f $SUBSYS ]; then
                echo $ECHOOPT "srmv2.2 dead but subsys ($SUBSYS) locked"
                RETVAL=2
            else
                echo $ECHOOPT "srmv2.2 is stopped"
                RETVAL=3
            fi
            $ECHO_FAILURE
            $ECHO_END
        fi
    fi
    ;;
  *)
    echo "Usage: $0 {start|stop|status|restart|condrestart}"
    RETVAL=1
    ;;
esac

exit $RETVAL
