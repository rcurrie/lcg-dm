/*
 * Copyright (C) 2004-2011 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_spacereq.c,v $ $Revision$ $Date$ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <sys/types.h>
#include "Cnetdb.h"
#include "dpm.h"
#include "dpm_api.h"
#include "dpm_server.h"
#include "dpm_util.h"
#include "dpns_api.h"
#include "serrno.h"
#include "srm_server.h"
#include "srmv2H.h"
extern gid_t *Cdecode_groups (char *, int *);
extern int Cgroupmatch (gid_t, int, gid_t *);
extern char *decode_group(gid_t, gid_t *, char *);
extern char *decode_user(uid_t, uid_t *, char *);
static int na_key = -1;

/*			Space Management Functions			*/

int
ns1__srmReserveSpace (struct soap *soap, struct ns1__srmReserveSpaceRequest *req, struct ns1__srmReserveSpaceResponse_ *rep)
{
	char ac_latency;
	u_signed64 actual_g_space;
	time_t actual_lifetime;
	char actual_s_type;
	u_signed64 actual_t_space;
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int nbfqans;
	int nbgids;
	struct ns1__srmReserveSpaceResponse *repp;
	u_signed64 req_g_space;
	time_t req_lifetime;
	u_signed64 req_t_space;
	char ret_policy;
	char s_token[CA_MAXDPMTOKENLEN+1];
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "ReserveSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmReserveSpaceResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmReserveSpaceResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmReserveSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}
	if (req->userSpaceTokenDescription &&
	    strlen (req->userSpaceTokenDescription) >= 256) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation =
		    soap_strdup (soap, "Invalid user space token description");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}
	if (! req->retentionPolicyInfo) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation =
		    soap_strdup (soap, "retentionPolicyInfo is mandatory");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}
	if (req->retentionPolicyInfo->retentionPolicy == REPLICA)
		ret_policy = 'R';
	else if (req->retentionPolicyInfo->retentionPolicy == OUTPUT)
		ret_policy = 'O';
	else if (req->retentionPolicyInfo->retentionPolicy == CUSTODIAL)
		ret_policy = 'C';
	else {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation =
		    soap_strdup (soap, "Invalid retentionPolicy");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}
	if (req->retentionPolicyInfo->accessLatency) {
		if (*req->retentionPolicyInfo->accessLatency == ONLINE)
			ac_latency = 'O';
		else if (*req->retentionPolicyInfo->accessLatency == NEARLINE)
			ac_latency = 'N';
		else {
			repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repp->returnStatus->explanation =
			    soap_strdup (soap, "Invalid accessLatency");
			RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
		}
	} else
		ac_latency = 'O';
	if (req->desiredSizeOfTotalSpace)
		req_t_space = *req->desiredSizeOfTotalSpace;
	else
		req_t_space = 0;
	req_g_space = req->desiredSizeOfGuaranteedSpace;
	if (req->desiredLifetimeOfReservedSpace)
		req_lifetime = *req->desiredLifetimeOfReservedSpace;
	else
		req_lifetime = 0;
	if (req_lifetime < 0)
		req_lifetime = 0x7FFFFFFF;

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_reservespace ('-', req->userSpaceTokenDescription, ret_policy,
	    ac_latency, req_t_space, req_g_space, req_lifetime, 0, NULL, NULL,
	    &actual_s_type, &actual_t_space, &actual_g_space, &actual_lifetime,
	    s_token) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
	}
	if ((repp->sizeOfTotalReservedSpace = soap_malloc (soap, sizeof(ULONG64))))
		*repp->sizeOfTotalReservedSpace = actual_t_space;
	if ((repp->sizeOfGuaranteedReservedSpace = soap_malloc (soap, sizeof(ULONG64))))
		*repp->sizeOfGuaranteedReservedSpace = actual_g_space;
	if ((repp->lifetimeOfReservedSpace = soap_malloc (soap, sizeof(int))))
		*repp->lifetimeOfReservedSpace = (actual_lifetime != 0x7FFFFFFF) ?
			actual_lifetime : -1;
	repp->spaceToken = soap_strdup (soap, s_token);
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURNSC (SOAP_OK, SRM_USCORESUCCESS);
}

int
ns1__srmStatusOfReserveSpaceRequest (struct soap *soap, struct ns1__srmStatusOfReserveSpaceRequestRequest *req, struct ns1__srmStatusOfReserveSpaceRequestResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[28];
	gid_t gid;
	struct ns1__srmStatusOfReserveSpaceRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;

	strcpy (func, "StatusOfReserveSpaceRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmStatusOfReserveSpaceRequestResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmStatusOfReserveSpaceRequestResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	rep->srmStatusOfReserveSpaceRequestResponse = repp;
	RETURNSC (SOAP_OK, SRM_USCORENOT_USCORESUPPORTED);
}

int
ns1__srmReleaseSpace (struct soap *soap, struct ns1__srmReleaseSpaceRequest *req, struct ns1__srmReleaseSpaceResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	int force;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXDPMTOKENLEN+14];
	int nbfqans;
	int nbgids;
	struct ns1__srmReleaseSpaceResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "ReleaseSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmReleaseSpaceResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmReleaseSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}
	if (! req->spaceToken) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "spaceToken is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}
	if (strlen (req->spaceToken) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid space token");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}

	sprintf (logbuf, "ReleaseSpace %s", req->spaceToken);
	srm_logreq (func, logbuf);

	if (req->forceFileRelease)
		force = *req->forceFileRelease;
	else
		force = 0;

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_releasespace (req->spaceToken, force) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
	}
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURNSC (SOAP_OK, SRM_USCORESUCCESS);
}

int
ns1__srmUpdateSpace (struct soap *soap, struct ns1__srmUpdateSpaceRequest *req, struct ns1__srmUpdateSpaceResponse_ *rep)
{
	u_signed64 actual_g_space;
	time_t actual_lifetime;
	u_signed64 actual_t_space;
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXDPMTOKENLEN+13];
	int nbfqans;
	int nbgids;
	struct ns1__srmUpdateSpaceResponse *repp;
	u_signed64 req_g_space;
	time_t req_lifetime;
	u_signed64 req_t_space;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "UpdateSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmUpdateSpaceResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmUpdateSpaceResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmUpdateSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}
	if (! req->spaceToken) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "spaceToken is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}
	if (strlen (req->spaceToken) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid space token");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}

	sprintf (logbuf, "UpdateSpace %s", req->spaceToken);
	srm_logreq (func, logbuf);

	if (req->newSizeOfTotalSpaceDesired)
		req_t_space = *req->newSizeOfTotalSpaceDesired;
	else
		req_t_space = -1;
	if (req->newSizeOfGuaranteedSpaceDesired)
		req_g_space = *req->newSizeOfGuaranteedSpaceDesired;
	else
		req_g_space = -1;
	if (req->newLifeTime) {
		req_lifetime = *req->newLifeTime;
		if (req_lifetime < 0)
			req_lifetime = 0x7FFFFFFF;
	} else
		req_lifetime = -1;

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_updatespace (req->spaceToken, req_t_space, req_g_space, req_lifetime,
	    -1, NULL, &actual_t_space, &actual_g_space, &actual_lifetime) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
	}
	if ((repp->sizeOfTotalSpace = soap_malloc (soap, sizeof(ULONG64))))
		*repp->sizeOfTotalSpace = actual_t_space;
	if ((repp->sizeOfGuaranteedSpace = soap_malloc (soap, sizeof(ULONG64))))
		*repp->sizeOfGuaranteedSpace = actual_g_space;
	if ((repp->lifetimeGranted = soap_malloc (soap, sizeof(int))))
		*repp->lifetimeGranted = (actual_lifetime != 0x7FFFFFFF) ?
			actual_lifetime : -1;
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURNSC (SOAP_OK, SRM_USCORESUCCESS);
}

int
ns1__srmStatusOfUpdateSpaceRequest (struct soap *soap, struct ns1__srmStatusOfUpdateSpaceRequestRequest *req, struct ns1__srmStatusOfUpdateSpaceRequestResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[27];
	gid_t gid;
	struct ns1__srmStatusOfUpdateSpaceRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;

	strcpy (func, "StatusOfUpdateSpaceRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmStatusOfUpdateSpaceRequestResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmStatusOfUpdateSpaceRequestResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	rep->srmStatusOfUpdateSpaceRequestResponse = repp;
	RETURNSC (SOAP_OK, SRM_USCORENOT_USCORESUPPORTED);
}

int
ns1__srmGetSpaceMetaData (struct soap *soap, struct ns1__srmGetSpaceMetaDataRequest *req, struct ns1__srmGetSpaceMetaDataResponse_ *rep)
{
	static enum ns1__TAccessLatency al_types[] = {ONLINE, NEARLINE};
	char clientdn[256];
	const char *clienthost;
	time_t current_time;
	time_t curtime;
	struct dpm_space_reserv dpm_spcmd;
	char **fqan;
	char func[17];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	int nb_errors = 0;
	int nb_s_gids;
	int nbfqans;
	int nbgids;
	int nbtokens;
	struct ns1__TMetaDataSpace *repmdp;
	struct ns1__srmGetSpaceMetaDataResponse *repp;
	gid_t *s_gids;
	char *s_token;
	gid_t sav_gid = -1;
	char sav_gidstr[256];
	uid_t sav_uid = -1;
	char sav_uidstr[256];
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "GetSpaceMetaData");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetSpaceMetaDataResponse))) == NULL)
		RETURN (SOAP_EOM);
	memset (repp, 0, sizeof(struct ns1__srmGetSpaceMetaDataResponse));
	if ((repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmGetSpaceMetaDataResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}
	if (! req->arrayOfSpaceTokens) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSpaceTokens is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}
	nbtokens = req->arrayOfSpaceTokens->__sizestringArray;

	/* Allocate the array of space details */

	if ((repp->arrayOfSpaceDetails =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTMetaDataSpace))) == NULL ||
	    (repp->arrayOfSpaceDetails->spaceDataArray =
	    soap_malloc (soap, nbtokens * sizeof(struct ns1__TMetaDataSpace *))) == NULL)
		RETURN (SOAP_EOM);
	repp->arrayOfSpaceDetails->__sizespaceDataArray = nbtokens;
	for (i = 0; i < nbtokens; i++) {
		if ((repp->arrayOfSpaceDetails->spaceDataArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TMetaDataSpace))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfSpaceDetails->spaceDataArray[i], 0, sizeof(struct ns1__TMetaDataSpace));
		if ((repp->arrayOfSpaceDetails->spaceDataArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
	}

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->arrayOfSpaceDetails = NULL;
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURNSC (SOAP_OK, SRM_USCOREINTERNAL_USCOREERROR);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	for (i = 0; i < nbtokens; i++) {
		repmdp = repp->arrayOfSpaceDetails->spaceDataArray[i];
		repmdp->spaceToken = req->arrayOfSpaceTokens->stringArray[i];
		repmdp->status->explanation = NULL;
		if ( ! req->arrayOfSpaceTokens->stringArray[i]) {
			repmdp->status->statusCode = SRM_USCOREFAILURE;
			repmdp->status->explanation = soap_strdup (soap, "space token is required");
			nb_errors++;
			continue;
		}
		s_token = req->arrayOfSpaceTokens->stringArray[i];
		if (strlen (s_token) > CA_MAXDPMTOKENLEN) {
			repmdp->status->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repmdp->status->explanation = soap_strdup (soap, "Invalid space token");
			nb_errors++;
			continue;
		}
		if (dpm_get_spcmd_by_token (&thip->dbfd, s_token, &dpm_spcmd,
		    0, NULL) < 0) {
			if (serrno == ENOENT) {
				repmdp->status->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
				repmdp->status->explanation = soap_strdup (soap, "Unknown space token");
			} else {
				repmdp->status->statusCode = SRM_USCOREFAILURE;
				repmdp->status->explanation = soap_strdup (soap, "DB fetch error");
			}
			nb_errors++;
			continue;
		}
		if (dpm_spcmd.s_gid) {
			if ((s_gids = Cdecode_groups (dpm_spcmd.groups, &nb_s_gids)) == NULL) {
				repmdp->status->statusCode = SRM_USCOREFAILURE;
				repmdp->status->explanation =
				    soap_strdup (soap, "Could not allocate memory");
				nb_errors++;
				continue;
			}
			for (j = 0; j < nb_s_gids; j++) {
				if (Cgroupmatch (s_gids[j], nbgids, gids)) break;
			}
			free (s_gids);
			if (j >= nb_s_gids) {
				repmdp->status->statusCode = SRM_USCOREAUTHORIZATION_USCOREFAILURE;
				nb_errors++;
				continue;
			}
		}
		if (dpm_spcmd.s_uid && uid != dpm_spcmd.s_uid) {
			repmdp->status->statusCode = SRM_USCOREAUTHORIZATION_USCOREFAILURE;
			nb_errors++;
			continue;
		}
		current_time = time (0);
		if (dpm_spcmd.expire_time < current_time)
			repmdp->status->statusCode = SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED;
		else
			repmdp->status->statusCode = SRM_USCORESUCCESS;
		if ((repmdp->retentionPolicyInfo =
		    soap_malloc (soap, sizeof(struct ns1__TRetentionPolicyInfo)))) {
			if (dpm_spcmd.ret_policy == 'R')
				repmdp->retentionPolicyInfo->retentionPolicy = REPLICA;
			else if (dpm_spcmd.ret_policy == 'O')
				repmdp->retentionPolicyInfo->retentionPolicy = OUTPUT;
			else
				repmdp->retentionPolicyInfo->retentionPolicy = CUSTODIAL;
			if (dpm_spcmd.ac_latency == 'O')
				repmdp->retentionPolicyInfo->accessLatency = &al_types[ONLINE];
			else
				repmdp->retentionPolicyInfo->accessLatency = &al_types[NEARLINE];
		}
		if (dpm_spcmd.s_gid)
			repmdp->owner =
			    soap_strdup (soap, decode_group (dpm_spcmd.s_gid, &sav_gid, sav_gidstr));
		else
			repmdp->owner =
			    soap_strdup (soap, decode_user (dpm_spcmd.s_uid, &sav_uid, sav_uidstr));
		if ((repmdp->totalSize = soap_malloc (soap, sizeof(ULONG64))))
			*repmdp->totalSize = dpm_spcmd.t_space;
		if ((repmdp->guaranteedSize = soap_malloc (soap, sizeof(ULONG64))))
			*repmdp->guaranteedSize = dpm_spcmd.g_space;
		if ((repmdp->unusedSize = soap_malloc (soap, sizeof(ULONG64))))
			*repmdp->unusedSize = dpm_spcmd.u_space > 0 ? dpm_spcmd.u_space : 0;
		if ((repmdp->lifetimeAssigned = soap_malloc (soap, sizeof(int))))
			*repmdp->lifetimeAssigned = current_time - dpm_spcmd.assign_time;
		if ((repmdp->lifetimeLeft = soap_malloc (soap, sizeof(int))))
			*repmdp->lifetimeLeft = (dpm_spcmd.expire_time == 0x7FFFFFFF) ?
			    -1 : (dpm_spcmd.expire_time > current_time) ?
			    dpm_spcmd.expire_time - current_time : 0;
	}
	if (nb_errors == 0)
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	else if (nb_errors != nbtokens)
		repp->returnStatus->statusCode = SRM_USCOREPARTIAL_USCORESUCCESS;
	else {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Failed for all tokens");
	}
	RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
}

int
ns1__srmChangeSpaceForFiles (struct soap *soap, struct ns1__srmChangeSpaceForFilesRequest *req, struct ns1__srmChangeSpaceForFilesResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[20];
	struct ns1__srmChangeSpaceForFilesResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "ChangeSpaceForFiles");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmChangeSpaceForFilesResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	repp->requestToken = NULL;
	repp->estimatedProcessingTime = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmChangeSpaceForFilesResponse = repp;
	RETURNSC (SOAP_OK, SRM_USCORENOT_USCORESUPPORTED);
}

int
ns1__srmStatusOfChangeSpaceForFilesRequest (struct soap *soap, struct ns1__srmStatusOfChangeSpaceForFilesRequestRequest *req, struct ns1__srmStatusOfChangeSpaceForFilesRequestResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[35];
	struct ns1__srmStatusOfChangeSpaceForFilesRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "StatusOfChangeSpaceForFilesRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmStatusOfChangeSpaceForFilesRequestResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	repp->estimatedProcessingTime = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmStatusOfChangeSpaceForFilesRequestResponse = repp;
	RETURNSC (SOAP_OK, SRM_USCORENOT_USCORESUPPORTED);
}

int
ns1__srmExtendFileLifeTimeInSpace (struct soap *soap, struct ns1__srmExtendFileLifeTimeInSpaceRequest *req, struct ns1__srmExtendFileLifeTimeInSpaceResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	time_t current_time;
	time_t curtime;
	struct dpm_pool *dpm_pools = NULL;
	struct dpm_space_reserv dpm_spcmd;
	int found;
	char **fqan;
	char func[26];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	time_t lifetime = 0;
	char logbuf[CA_MAXSFNLEN+38];
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbpools = 0;
	int nbreplicas = 0;
	int nbsurls;
	int nerr;
	struct Cns_filereplicax *rep_entries = NULL;
	struct ns1__TSURLLifetimeReturnStatus *repfilep;
	struct ns1__srmExtendFileLifeTimeInSpaceResponse *repp;
	char *sfn;
	char *surl;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "ExtendFileLifeTimeInSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmExtendFileLifeTimeInSpaceResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmExtendFileLifeTimeInSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	
	}
	if (! req->spaceToken) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "spaceToken is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}
	if (strlen (req->spaceToken) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid space token");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}
	if (! req->arrayOfSURLs) {
		repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
		repp->returnStatus->explanation = soap_strdup (soap, "Current implementation requires SURLs");
		RETURNSC (SOAP_OK, SRM_USCORENOT_USCORESUPPORTED);
	}

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURNSC (SOAP_OK, SRM_USCOREINTERNAL_USCOREERROR);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	/* Get space lifetime */

	if (dpm_get_spcmd_by_token (&thip->dbfd, req->spaceToken, &dpm_spcmd,
	    0, NULL) < 0) {
		if (serrno == ENOENT) {
			repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repp->returnStatus->explanation = soap_strdup (soap, "Unknown space token");
		} else {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB fetch error");
		}
		RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
	}
	if ((dpm_spcmd.s_gid && ! Cgroupmatch (dpm_spcmd.s_gid, nbgids, gids)) ||
	    (dpm_spcmd.s_uid && uid != dpm_spcmd.s_uid)) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHORIZATION_USCOREFAILURE;
		RETURNSC (SOAP_OK, SRM_USCOREAUTHORIZATION_USCOREFAILURE);
	}
	current_time = time (0);
	if (req->newLifeTime && *req->newLifeTime) {
		if (*req->newLifeTime < 0)	/* infinite */
			lifetime = 0x7FFFFFFF;
		else
			lifetime = *req->newLifeTime + current_time;
	} else {
		lifetime = DEFAULT_LIFETIME;
		dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
		thip->errbuf[0] = '\0';
		dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
		if (voname && fqan)
			dpm_client_setVOMS_data (voname, fqan, nbfqans);

		if (dpm_getpools (&nbpools, &dpm_pools) == 0) {
			for (i = 0; i < nbpools; i++) {
				if (strcmp ((dpm_pools + i)->poolname, dpm_spcmd.poolname) == 0) {
					lifetime = (dpm_pools + i)->def_lifetime;
					break;
				}
			}
		}
		for (i = 0; i < nbpools; i++)
			free ((dpm_pools + i)->gids);
		free (dpm_pools);
		if (lifetime != 0x7FFFFFFF)
			lifetime += current_time;
	}
	if (lifetime > dpm_spcmd.expire_time)
		lifetime = dpm_spcmd.expire_time;
	sprintf (logbuf, "ExtendFileLifeTimeInSpace %s %d", req->spaceToken, lifetime);
	srm_logreq (func, logbuf);

	nbsurls = req->arrayOfSURLs->__sizeurlArray;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLLifetimeReturnStatus))) == NULL ||
	    (repp->arrayOfFileStatuses->statusArray =
		soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLLifetimeReturnStatus *))) == NULL)
		RETURN (SOAP_EOM);
        repp->arrayOfFileStatuses->__sizestatusArray = nbsurls;

	for (i = 0; i < nbsurls; i++) {
		if ((repp->arrayOfFileStatuses->statusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSURLLifetimeReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->statusArray[i], 0, sizeof(struct ns1__TSURLLifetimeReturnStatus));
		if ((repp->arrayOfFileStatuses->statusArray[i]->status =

		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
        }

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	for (i = 0; i < nbsurls; i++) {
		repfilep = repp->arrayOfFileStatuses->statusArray[i];
		surl = req->arrayOfSURLs->urlArray[i];
		repfilep->surl = surl;
		repfilep->status->explanation = NULL;
		if (! surl) {
			repfilep->status->statusCode = SRM_USCOREFAILURE;
			repfilep->status->explanation =
				soap_strdup (soap, "Pointer to SURL is NULL");
			nb_file_err++;
			continue;
		}
		if (strlen (surl) > CA_MAXSFNLEN) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		sprintf (logbuf, "ExtendFileLifeTimeInSpace %d %s", i, surl);
		srm_logreq (func, logbuf);

		/* find pfns of replicas in that space */

		if ((sfn = sfnfromsurl (surl)) == NULL) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		rep_entries = NULL;
		if (Cns_getreplicax (sfn, NULL, NULL, &nbreplicas, &rep_entries) < 0) {
			repfilep->status->statusCode = serrno2statuscode (serrno);
			free (rep_entries);
			nb_file_err++;
			continue;
		}
		found = 0;
		nerr = 0;
		for (j = 0; j < nbreplicas; j++) {
			if (strcmp ((rep_entries + j)->setname, req->spaceToken))
				continue;
			found++;
			if ((rep_entries + j)->ltime < current_time) {
				repfilep->status->statusCode =
				    SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED;
				nerr++;
				break;
			}
			if (Cns_setrltime ((rep_entries + j)->sfn, lifetime) < 0) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				if (*thip->errbuf) {
					thip->errbuf[strlen(thip->errbuf)-1] = '\0';
					repfilep->status->explanation =
					    soap_strdup (soap, thip->errbuf);
				}
				nerr++;
				continue;
			}
		}
		free (rep_entries);
		if (! found) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		} else if (nerr) {
			nb_file_err++;
			continue;
		}
		repfilep->status->statusCode = SRM_USCORESUCCESS;
		if ((repfilep->fileLifetime =
		    soap_malloc (soap, sizeof(int))) == NULL)
			RETURN (SOAP_EOM);
		*repfilep->fileLifetime =
		    (lifetime != 0x7FFFFFFF) ? lifetime - current_time : -1;
	}
	if (nb_file_err == 0)
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	else if (nb_file_err != nbsurls)
		repp->returnStatus->statusCode = SRM_USCOREPARTIAL_USCORESUCCESS;
	else {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Failed for all SURLs");
	}
	RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
}

int
ns1__srmPurgeFromSpace (struct soap *soap, struct ns1__srmPurgeFromSpaceRequest *req, struct ns1__srmPurgeFromSpaceResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	int found;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	char logbuf[CA_MAXSFNLEN+27];
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbreplicas = 0;
	int nbsurls;
	int nerr;
	int primary_replica;
	struct Cns_filereplicax *rep_entries = NULL;
	struct ns1__TSURLReturnStatus *repfilep;
	struct ns1__srmPurgeFromSpaceResponse *repp;
	char *sfn;
	char **surls = NULL;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "PurgeFromSpace");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmPurgeFromSpaceResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmPurgeFromSpaceResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}
	if (! req->spaceToken) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "spaceToken is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}
	if (strlen (req->spaceToken) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}

	sprintf (logbuf, "PurgeFromSpace %s", req->spaceToken);
	srm_logreq (func, logbuf);

	if (! req->arrayOfSURLs) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSiteURLs is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}
	nbsurls = req->arrayOfSURLs->__sizeurlArray;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLReturnStatus))) == NULL ||
	    (nbsurls > 0 && ((repp->arrayOfFileStatuses->statusArray =
		soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLReturnStatus *))) == NULL ||
		(surls = soap_malloc (soap, nbsurls * sizeof (char *))) == NULL)))
		RETURN (SOAP_EOM);
	repp->arrayOfFileStatuses->__sizestatusArray = nbsurls;

	for (i = 0; i < nbsurls; i++) {
		if ((repp->arrayOfFileStatuses->statusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSURLReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->statusArray[i], 0, sizeof(struct ns1__TSURLReturnStatus));
		if ((repp->arrayOfFileStatuses->statusArray[i]->status =

		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
        }

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);
	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	for (i = 0; i < nbsurls; i++) {
		repfilep = repp->arrayOfFileStatuses->statusArray[i];
		repfilep->surl = req->arrayOfSURLs->urlArray[i];
		repfilep->status->explanation = NULL;
		if (! repfilep->surl) {
			repfilep->status->statusCode = SRM_USCOREFAILURE;
			repfilep->status->explanation =
				soap_strdup (soap, "Pointer to SURL is NULL");
			nb_file_err++;
			continue;
		}
		if (strlen (repfilep->surl) > CA_MAXSFNLEN) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		sprintf (logbuf, "PurgeFromSpace %d %s", i, repfilep->surl);
		srm_logreq (func, logbuf);

		/* find pfns of replicas in that space */

		if ((sfn = sfnfromsurl (repfilep->surl)) == NULL) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		rep_entries = NULL;
		if (Cns_getreplicax (sfn, NULL, NULL, &nbreplicas, &rep_entries) < 0) {
			repfilep->status->statusCode = serrno2statuscode (serrno);
			free (rep_entries);
			nb_file_err++;
			continue;
		}
		found = 0;
		nerr = 0;
		primary_replica = -1;
		for (j = 0; j < nbreplicas; j++) {
			if (strcmp ((rep_entries + j)->setname, req->spaceToken))
				continue;
			found++;
			if ((rep_entries + j)->r_type == 'P') {
				primary_replica = j;
				continue;
			}
			if (dpm_delreplica ((rep_entries + j)->sfn) < 0) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				if (*thip->errbuf) {
					thip->errbuf[strlen(thip->errbuf)-1] = '\0';
					repfilep->status->explanation =
					    soap_strdup (soap, thip->errbuf);
				}
				nerr++;
				continue;
			}
		}
		free (rep_entries);
		if (! found) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		} else if (found == 1 && primary_replica >= 0) {
			repfilep->status->statusCode = SRM_USCORELAST_USCORECOPY;
			nb_file_err++;
			continue;
		} else if (nerr) {
			nb_file_err++;
			continue;
		}
		repfilep->status->statusCode = SRM_USCORESUCCESS;
	}
	if (nb_file_err == 0)
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	else if (nb_file_err != nbsurls)
		repp->returnStatus->statusCode = SRM_USCOREPARTIAL_USCORESUCCESS;
	else {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Failed for all SURLs");
	}
	RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
}

int
ns1__srmGetSpaceTokens (struct soap *soap, struct ns1__srmGetSpaceTokensRequest *req, struct ns1__srmGetSpaceTokensResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int nbfqans;
	int nbgids;
	int nbreplies = 0;
	struct ns1__srmGetSpaceTokensResponse *repp;
	char **s_tokens = NULL;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "GetSpaceToken");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetSpaceTokensResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfSpaceTokens = NULL;
	rep->srmGetSpaceTokensResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}
	if (req->userSpaceTokenDescription &&
	    strlen (req->userSpaceTokenDescription) > 255) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid user space token description");
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_getspacetoken (req->userSpaceTokenDescription, &nbreplies, &s_tokens) < 0) {
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		for (i = 0; i < nbreplies; i++)
			free (s_tokens[i]);
		free (s_tokens);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
	}

	/* Allocate the array of space tokens */

	if ((repp->arrayOfSpaceTokens =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfString))) == NULL ||
	    (repp->arrayOfSpaceTokens->stringArray =
	    soap_malloc (soap, nbreplies * sizeof(char *))) == NULL) {
		for (i = 0; i < nbreplies; i++)
			free (s_tokens[i]);
		free (s_tokens);
		RETURN (SOAP_EOM);
	}

	repp->arrayOfSpaceTokens->__sizestringArray = nbreplies;
	for (i = 0; i < nbreplies; i++) {
		if ((repp->arrayOfSpaceTokens->stringArray[i] =
		    soap_strdup (soap, s_tokens[i])) == NULL) {
			for ( ; i < nbreplies; i++)
				free (s_tokens[i]);
			free (s_tokens);
			RETURN (SOAP_EOM);
		}
		free (s_tokens[i]);
	}
	free (s_tokens);
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURNSC (SOAP_OK, SRM_USCORESUCCESS);
}
