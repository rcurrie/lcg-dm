/*
 * Copyright (C) 2006-2007 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_discovreq.c,v $ $Revision: 1.3 $ $Date: 2008/01/10 08:28:00 $ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <sys/types.h>
#include "Cnetdb.h"
#include "dpm.h"
#include "dpm_server.h"
#include "patchlevel.h"
#include "srm_server.h"
#include "srmv2H.h"
extern int nb_supported_protocols;
extern char **supported_protocols;
static int na_key = -1;

int
ns1__srmGetTransferProtocols (struct soap *soap, struct ns1__srmGetTransferProtocolsRequest *req, struct ns1__srmGetTransferProtocolsResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[21];
	int i;
	struct ns1__srmGetTransferProtocolsResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "GetTransferProtocols");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetTransferProtocolsResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL ||
	    (repp->protocolInfo = soap_malloc (soap, sizeof(struct ns1__ArrayOfTSupportedTransferProtocol))) == NULL ||
	    (repp->protocolInfo->protocolArray =
		soap_malloc (soap, nb_supported_protocols * sizeof(struct ns1__TSupportedTransferProtocol *))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->protocolInfo->__sizeprotocolArray = nb_supported_protocols;
	rep->srmGetTransferProtocolsResponse = repp;
	for (i = 0; i < nb_supported_protocols; i++) {
		if ((repp->protocolInfo->protocolArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSupportedTransferProtocol))) == NULL ||
		    (repp->protocolInfo->protocolArray[i]->transferProtocol =
		    soap_strdup (soap, supported_protocols[i])) == NULL)
			RETURN (SOAP_EOM);
		repp->protocolInfo->protocolArray[i]->attributes = NULL;
	}
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmPing (struct soap *soap, struct ns1__srmPingRequest *req, struct ns1__srmPingResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[16];
	char info[256];
	struct ns1__srmPingResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "Ping");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmPingResponse))) == NULL ||
	    (repp->versionInfo = soap_strdup (soap, "v2.2")) == NULL) {
		RETURN (SOAP_EOM);
	}
	if ((repp->otherInfo = soap_malloc (soap, sizeof(struct ns1__ArrayOfTExtraInfo))) == NULL ||
	    (repp->otherInfo->extraInfoArray =
	    soap_malloc (soap, 2 * sizeof(struct ns1__TExtraInfo *))) == NULL)
		repp->otherInfo = NULL;
	else {
		repp->otherInfo->__sizeextraInfoArray = 2;
		sprintf (info, "%s-%d", BASEVERSION, PATCHLEVEL);
		if ((repp->otherInfo->extraInfoArray[0] =
		    soap_malloc (soap, sizeof(struct ns1__TExtraInfo))) == NULL ||
		    (repp->otherInfo->extraInfoArray[0]->key =
		    soap_strdup (soap, "backend_type")) == NULL ||
		    (repp->otherInfo->extraInfoArray[0]->value =
		    soap_strdup (soap, "DPM")) == NULL ||
		    (repp->otherInfo->extraInfoArray[1] =
		    soap_malloc (soap, sizeof(struct ns1__TExtraInfo))) == NULL ||
		    (repp->otherInfo->extraInfoArray[1]->key =
		    soap_strdup (soap, "backend_version")) == NULL ||
		    (repp->otherInfo->extraInfoArray[1]->value =
		    soap_strdup (soap, info)) == NULL)
			repp->otherInfo = NULL;
	}
	rep->srmPingResponse = repp;
	RETURN (SOAP_OK);
}
