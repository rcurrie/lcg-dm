.\" @(#)$RCSfile: Cns_delreplica.man,v $ $Revision: 1.3 $ $Date: 2005/07/26 12:09:11 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2005 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_DELREPLICA 3 "$Date: 2005/07/26 12:09:11 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_delreplica \- delete a replica for a given file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_delreplica (const char *" guid ,
.BI "struct Cns_fileid *" file_uniqueid ,
.BI "const char *" sfn )
.SH DESCRIPTION
.B Cns_delreplica
deletes a given replica. An extra check may be obtained by specifying
.I guid
or
.IR file_uniqueid .
If both are given,
.I file_uniqueid
is used.
.TP
.I guid
specifies the Grid Unique IDentifier.
.TP
.I sfn
is either the Site URL or the Physical File Name for the replica.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file or replica does not exist.
.TP
.B EACCES
Search permission is denied on a component of the parent directory or
the effective user ID does not match the owner of the file or
write permission on the file entry itself is denied.
.TP
.B EFAULT
.I sfn
is a NULL pointer.
.TP
.B EINVAL
The length of
.I guid
exceeds
.BR CA_MAXGUIDLEN .
.TP
.B ENAMETOOLONG
The length of
.I sfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_addreplica(3) ,
.BR Cns_listreplica(3)
