.\" @(#)$RCSfile: nsenterusrmap.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006-2009 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH NSENTERUSRMAP 1 "$Date$" CASTOR "Cns Administrator Commands"
.SH NAME
nsenterusrmap \- define a new user entry in Virtual Id table
.SH SYNOPSIS
.B nsenterusrmap
[
.BI --uid " uid"
]
.BI --user " username"
.SH DESCRIPTION
.B nsenterusrmap
defines a new user entry in Virtual Id table.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.BI --uid " uid"
specifies the Virtual User Id.
If this parameter is absent, the next available id is allocated.
.TP
.BI --user " username"
specifies the new user name.
It must be at most 255 characters long.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.B Cns_enterusrmap(3)
