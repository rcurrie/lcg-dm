/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: nslistusrmap.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	nslistusrmap - query about a given user or list all existing users in virtual uid table */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "Cns_api.h"
#include "serrno.h"
extern	char	*optarg;
extern	int	optind;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	int errflg = 0;
	int i;
	static struct Coptions longopts[] = {
		{"uid", REQUIRED_ARGUMENT, 0, OPT_IDMAP_UID},
		{"user", REQUIRED_ARGUMENT, 0, OPT_IDMAP_USER},
		{0, 0, 0, 0}
	};
	int nbentries;
	char p_stat;
	uid_t uid = -1;
	char *username = NULL;
	struct Cns_userinfo *usr_entries;
	char vidstr[256];

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_IDMAP_UID:
			if ((uid = strtol (Coptarg, &dp, 10)) < 0 || *dp != '\0') {
				fprintf (stderr, "invalid uid: %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_IDMAP_USER:
			if (strlen (Coptarg) > 255) {
				fprintf (stderr,
				    "user name too long: %s\n", Coptarg);
				errflg++;
			} else
				username = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc)
		errflg++;
	if (errflg) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "[--uid uid] [--user username]\n");
		exit (USERR);
	}

	if (uid != -1) {
		if (Cns_getusrbyuid (uid, vidstr) < 0) {
			fprintf (stderr, "nslistusrmap %d: %s\n", uid,
			    (serrno == ENOENT) ? "No such user" : sstrerror(serrno));
			exit (USERR);
		}
		printf ("%8d %s\n", uid, vidstr);
	} else if (username) {
		if (Cns_getusrbynam (username, &uid) < 0) {
			fprintf (stderr, "nslistusrmap %s: %s\n", username,
			    (serrno == ENOENT) ? "No such user" : sstrerror(serrno));
			exit (USERR);
		}
		printf ("%8d %s\n", uid, username);
	} else {
		if (Cns_getusrmap (&nbentries, &usr_entries) < 0) {
			fprintf (stderr, "nslistusrmap: %s\n", sstrerror(serrno));
			exit (USERR);
		}
		for (i = 0; i < nbentries; i++) {
			p_stat = '\0';
			printf ("%8d %s", (usr_entries+i)->userid,
			    (usr_entries+i)->username);
			if ((usr_entries+i)->banned & ARGUS_BAN) {
				printf (p_stat ? "|ARGUS_BAN" : " ARGUS_BAN");
				p_stat = '|';
			}
			if ((usr_entries+i)->banned & LOCAL_BAN) {
				printf (p_stat ? "|LOCAL_BAN" : " LOCAL_BAN");
				p_stat = '|';
			}
			printf ("\n");
		}
	}
	exit (0);
}
