/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_delfilesbyguid.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_delfilesbyguid - delete entries associated with a list of GUIDs
	and associated replica entries if force is set to 1 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_delfilesbyguid(int nbguids, const char **guids, int force, int *nbstatuses, int **statuses)
{
	int argtype = 0;
	int c;
	char func[19];
	gid_t gid;
	int i;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[4];
	char *sbp;
	char *sendbuf;
	struct Cns_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "Cns_delfilesbyguid");
	if (Cns_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbguids <= 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (! guids || ! nbstatuses || ! statuses) {
		serrno = EFAULT;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 5 * LONGSIZE;
	msglen += WORDSIZE;
	msglen += WORDSIZE;
	msglen += LONGSIZE;
	for (i = 0; i < nbguids; i++) {
		msglen += strlen (guids[i]) + 1;
	}

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC);
	marshall_LONG (sbp, CNS_DELFILES);
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */
 
	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_WORD (sbp, argtype);
	marshall_WORD (sbp, force);
	marshall_LONG (sbp, nbguids);
	for (i = 0; i < nbguids; i++) {
		marshall_STRING (sbp, guids[i]);
	}
 
	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsdx (NULL, NULL, sendbuf,
	    msglen, repbuf, sizeof(repbuf), (void **)statuses, nbstatuses);
	free (sendbuf);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, *nbstatuses);
		if (*nbstatuses == 0) {
			*statuses = NULL;
			return (0);
		}
	}
	return (c);
}
