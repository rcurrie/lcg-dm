.\" @(#)$RCSfile: Cns_getreplicasl.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007-2010 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_GETREPLICASL 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_getreplicasl \- get the replica entries associated with a list of path names
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getreplicasl (int " nbfiles ,
.BI "const char **" paths ,
.BI "const char *" se ,
.BI "int *" nbentries ,
.BI "struct Cns_filereplicas **" rep_entries )
.SH DESCRIPTION
.B Cns_getreplicasl
gets the replica entries associated with a list of path names.
.TP
.I nbfiles
specifies the number of path names in the array
.IR paths .
.TP
.I paths
specifies the list of logical path names relative to the current CASTOR directory or
the list of full CASTOR path names.
.TP
.I se
allows to restrict the replica entries to a given SE.
.TP
.I nbentries
will be set to the number of entries in the array of replicas.
.TP
.I rep_entries
will be set to the address of an array of Cns_filereplicas structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct Cns_filereplicas {
	char		guid[CA_MAXGUIDLEN+1];
	int		errcode;
	u_signed64	filesize;
	time_t		ctime;		/* GUID creation time */
	char		csumtype[3];
	char		csumvalue[33];
	time_t		r_ctime;	/* replica creation time */
	time_t		r_atime;	/* last access to replica */
	char		status;
	char		host[CA_MAXHOSTNAMELEN+1];
	char		sfn[CA_MAXSFNLEN+1];
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file does not exist.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request or unmarshalling
the reply.
.TP
.B EFAULT
.IR paths ,
.I nbentries
or
.I rep_entries
is a NULL pointer.
.TP
.B EINVAL
.I nbfiles
is not strictly positive or the length of
.I se
exceeds
.BR CA_MAXHOSTNAMELEN .
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP 
.B SEINTERNAL 
Database error.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chdir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
