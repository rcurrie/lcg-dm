/*
 * Copyright (C) 2005-2010 by CERN/IT/GD/SC
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_getidmap.c,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

/*      Cns_getidmap - get uid/gids associated with a given dn/roles */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h> 
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_getidmapc (const char *username, const char *user_ca, int nbgroups, const char **groupnames, uid_t *userid, gid_t *gids)
{
	int c;
	char func[16];
	int i;
	int msglen;
	int n;
	char *q;
	char *rbp;
	char repbuf[REPBUFSZ];
	char *sbp;
	char *sendbuf;
	struct Cns_api_thread_info *thip;

	strcpy (func, "Cns_getidmap");
	if (Cns_apiinit (&thip))
		return (-1);

	if (! username || ! userid || ! gids) {
		serrno = EFAULT;
		return (-1);
	}
	if (nbgroups < 0) {
		serrno = EINVAL;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 3 * LONGSIZE;
	msglen += strlen (username) + 1;
	msglen += LONGSIZE;
	if (groupnames) {
		for (i = 0; i < nbgroups; i++)
			msglen += strlen (groupnames[i]) + 1;
	}
	if (user_ca)
		msglen += strlen (user_ca) + 1;
	else
		msglen++;

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC2);
	marshall_LONG (sbp, CNS_GETIDMAP);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_STRING (sbp, username);
	marshall_LONG (sbp, nbgroups);
	if (groupnames) {
		for (i = 0; i < nbgroups; i++) {
			marshall_STRING (sbp, groupnames[i]);
		}
	}
	if (user_ca) {
		marshall_STRING (sbp, user_ca);
	} else {
		marshall_STRING (sbp, "");
	}

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsd (NULL, NULL, sendbuf, msglen, repbuf, sizeof(repbuf));
	free (sendbuf);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, n);
		*userid = n;
		if (nbgroups == 0)
			nbgroups = 1;
		for (i = 0; i < nbgroups; i++) {
			unmarshall_LONG (rbp, n);
			*(gids+i) = n;
		}
	}
	return (c);
}

int DLL_DECL
Cns_getidmap (const char *username, int nbgroups, const char **groupnames, uid_t *userid, gid_t *gids)
{
	return (Cns_getidmapc (username, NULL, nbgroups, groupnames, userid, gids));
}
