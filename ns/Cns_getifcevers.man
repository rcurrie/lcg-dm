.\" @(#)$RCSfile: Cns_getifcevers.man,v $ $Revision: 1.1 $ $Date: 2007/05/09 06:42:05 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_GETIFCEVERS 3 "$Date: 2007/05/09 06:42:05 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_getifcevers \- get name server client version number
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getifcevers (char *" version )
.SH DESCRIPTION
.B Cns_getifcevers
gets the name server client version number.
.SH OPTIONS
.TP
.I version
points at a buffer to receive the version number.
The buffer must be at least 256 characters long.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I version
is a NULL pointer.
