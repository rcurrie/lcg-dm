.\" @(#)$RCSfile: Cns_startsess.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2005-2010 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_STARTSESS 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_startsess \- start session
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_startsess (char *" server ,
.BI "char *" comment );
.SH DESCRIPTION
.B Cns_startsess
starts session.
The connection to the Name Server is kept open until
.B Cns_endsess
is explicitly called.
Requests issued between
.B Cns_startsess
and
.B Cns_endsess
are independent from each other and are individually committed or rolled back.
The
.I comment
is logged in the server log.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EINVAL
The length of
.I comment
exceeds
.BR CA_MAXCOMMENTLEN .
.TP 
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_endsess(3)
