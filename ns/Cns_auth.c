/*
 * Copyright (C) 2005 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_auth.c,v $ $Revision: 1.3 $ $Date: 2006/08/28 07:47:57 $ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include "Castor_limits.h"
#include "Cns_api.h"
#include "serrno.h"

/*      Cns_client_getAuthorizationId - get the authorization id from the thread-specific structure */

int DLL_DECL
Cns_client_getAuthorizationId(uid_t *uid, gid_t *gid, char **mech, char **id)
{
	struct Cns_api_thread_info *thip;

#ifdef CSEC
	if (Cns_apiinit (&thip))
		return (-1);
	if (thip->use_authorization_id == 0)
		return (0);
	if (uid)
		*uid = thip->Csec_uid;
	if (gid)
		*gid = thip->Csec_gid;
	if (mech)
		*mech = thip->Csec_mech;
	if (id)
		*id = thip->Csec_auth_id;
#endif
	return (0);
}

/*      Cns_client_resetAuthorizationId - reset the authorization id in the thread-specific structure */

int DLL_DECL
Cns_client_resetAuthorizationId()
{
	char func[32];
	struct Cns_api_thread_info *thip;

#ifdef CSEC
	strcpy (func, "Cns_client_resetAuthorizationId");
	if (Cns_apiinit (&thip))
		return (-1);
	thip->use_authorization_id = 0;
#endif
	return (0);
}

/*      Cns_client_setAuthorizationId - set the authorization id in the thread-specific structure */

int DLL_DECL
Cns_client_setAuthorizationId(uid_t uid, gid_t gid, const char *mech, char *id)
{
	char func[30];
	struct Cns_api_thread_info *thip;

#ifdef CSEC
	strcpy (func, "Cns_client_setAuthorizationId");
	if (Cns_apiinit (&thip))
		return (-1);
	thip->Csec_uid = uid;
	thip->Csec_gid = gid;
	if (strlen (mech) > CA_MAXCSECPROTOLEN) {
		Cns_errmsg (func, "Supplied Csec protocol is too long\n");
		serrno = EINVAL;
		return (-1);
	}
	strcpy (thip->Csec_mech, mech);
	if (strlen (id) > CA_MAXCSECNAMELEN) {
		Cns_errmsg (func, "Supplied authorization id is too long\n");
		serrno = EINVAL;
		return (-1);
	}
	strcpy (thip->Csec_auth_id, id);
	thip->voname = NULL;
	thip->nbfqan = 0;
	thip->fqan = NULL;
	thip->use_authorization_id = 1;
#endif
	return (0);
}

/*      Cns_client_setVOMS_data - set the VOMS data in the thread-specific structure */

int DLL_DECL
Cns_client_setVOMS_data(char *voname, char **fqan, int nbfqan)
{
	struct Cns_api_thread_info *thip;
    static int                  voname_key = -1;
    static int                  fqans_key  = -1;
    int                         i, max_fqan_len, len;
    void                       *fqan_bucket;
    char                       *offset;

#ifdef CSEC
	if (Cns_apiinit (&thip))
		return (-1);

    /* VO name */
    if (Cglobals_get_dynamic(&voname_key, &thip->voname,
                             strlen(voname) + 1, 1) < 0) {
      serrno = ENOMEM;
      return (-1);
    }
    strcpy(thip->voname, voname);

	/* Fqans */
    thip->nbfqan = nbfqan;
    
    /* Search biggest one */
    max_fqan_len = 0;
    for (i = 0; i < nbfqan; ++i) {
      len = strlen(fqan[i]);
      if (len > max_fqan_len)
        max_fqan_len = len;
    }

    max_fqan_len++; /* We need \0 !*/

    /* Allocate enough for all of them */
    if (Cglobals_get_dynamic(&fqans_key, &fqan_bucket,
                             nbfqan * max_fqan_len + sizeof(char*) * nbfqan,
                             1) < 0) {
      serrno = ENOMEM;
      return (-1);
    }

    /* Point to the proper place and copy */
    thip->fqan = (char**)fqan_bucket; /* First bytes reserved for the pointer array */
    offset = (char*)(fqan_bucket + sizeof(char**) * nbfqan);
    
    for (i = 0; i < nbfqan; ++i) {
      thip->fqan[i] = offset;
      strcpy(thip->fqan[i], fqan[i]);
      /* Point to the next entry offset */
      offset += max_fqan_len;
    }
    
#endif
	return (0);
}

/*      Cns_client_setSecurityProtocols - set the security protocols that can be used */
int DLL_DECL
Cns_client_setSecurityProtocols(const char *proto_list)
{
  return Cns_setenv("CSEC_MECH", proto_list, 1);
}
