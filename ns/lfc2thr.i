/*********************************************
    SWIG input file for LFC
 (including typemaps for non-trivial functions
*********************************************/

%module lfc2thr

%{
#include "Python.h"
#include "lfc_api.h"
%}

%init %{
    Cthread_init ();
%}

%exception %{
      char serrbuf[ERRORLEN_MAX] = "";
      lfc_seterrbuf (serrbuf, ERRORLEN_MAX);
      Py_BEGIN_ALLOW_THREADS
      $action
      Py_END_ALLOW_THREADS
%}

%include "lfc_api.h"
%include "Cns.i"
