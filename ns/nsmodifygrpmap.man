.\" @(#)$RCSfile: nsmodifygrpmap.man,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2010 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH NSMODIFYGRPMAP 1 "$Date$" CASTOR "Cns Administrator Commands"
.SH NAME
nsmodifygrpmap \- modify group entry corresponding to a given virtual gid
.SH SYNOPSIS
.B nsmodifygrpmap
.BI --gid " gid"
.BI --group " newname"
[
.BI --status " status"
]
.SH DESCRIPTION
.B nsmodifygrpmap
modifies the group entry corresponding to a given virtual gid.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.BI --gid " gid"
specifies the Virtual Group Id.
.TP
.BI --group " newname"
specifies the new group name.
It must be at most 255 characters long.
.TP
.BI --status " status"
status can be set to 0 or a combination of ARGUS_BAN and LOCAL_BAN.
This can be either alphanumeric or the corresponding numeric value.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.B Cns_modifygrpmap(3)
