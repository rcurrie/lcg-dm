.\" @(#)$RCSfile: Cns_getgrpbynam.man,v $ $Revision: 1.1 $ $Date: 2005/10/19 11:30:57 $ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH CNS_GETGRPBYNAM 3 "$Date: 2005/10/19 11:30:57 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_getgrpbynam \- get virtual gid associated with a given group name
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getgrpbynam (char *" groupname ,
.BI "gid_t *" gid )
.SH DESCRIPTION
.B Cns_getgrpbynam
gets the virtual gid associated with a given group name.
.TP
.I groupname
specifies the group name.
It must be at most 255 characters long.
.TP
.I gid
specifies the address of a buffer to receive the Virtual Group Id.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I groupname
or
.I gid
is a NULL pointer.
.TP
.B EINVAL
This group name does not exist in the internal mapping table or the length of
.I groupname
exceeds 255.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
