/*
 * Copyright (C) 1999-2008 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cupv_check.c,v $ $Revision: 1.11 $ $Date: 2008/01/29 10:09:39 $ CERN IT-DS/HSM Ben Couturier";
#endif /* not lint */
 

#include <errno.h>
#ifndef USE_CUPV
#include <stdio.h>
#endif
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cupv_api.h"
#ifndef USE_CUPV
#ifndef _WIN32
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
#define strtok(X,Y) strtok_r(X,Y,&last)
#endif /* _REENTRANT || _THREAD_SAFE */
#endif /* _WIN32 */
#include "Cns.h"
#else
#include "Cupv.h"
#endif
#include "serrno.h"
extern char localdomain[CA_MAXHOSTNAMELEN+1];


Cupv_check(uid_t priv_uid, gid_t priv_gid, const char *src, const char *tgt, int priv)
{
	int c;
	char fqn[CA_MAXHOSTNAMELEN+1];
	char func[16];
	char *getconfent();
	gid_t gid;
	int l;
#ifndef USE_CUPV
#ifndef _WIN32
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
	char *last = NULL;
#endif  
#endif
#endif
	int msglen;
	char *p;
	char *q;
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct Cupv_api_thread_info *thip;
	uid_t uid;
	int lensrc, lentgt;

        strcpy (func, "Cupv_check");
#ifdef USE_CUPV
        if (Cupv_apiinit (&thip))
                return (-1);
        uid = geteuid();
        gid = getegid();
#if defined(_WIN32)
        if (uid < 0 || gid < 0) {
                Cupv_errmsg (func, CUP53);
                serrno = SENOMAPFND;
                return (-1);
        }
#endif
#endif
	if (priv_uid < 0 || priv_gid < 0 || priv < 0) {
	  serrno = EINVAL;
	  return (-1);
	}

	if (src == NULL) {
	  lensrc = 0;
	} else {
	  lensrc = strlen(src);
	}

	if (tgt == NULL) {
	  lentgt = 0;
	} else {
	  lentgt = strlen(tgt);
	}

	if (lensrc > CA_MAXREGEXPLEN || lentgt > CA_MAXREGEXPLEN) {
	  serrno = EINVAL;
	  return(-1);
	}

	/* Applying a first check to see if the request is for root */
	/* In this case just return without asking the server */

	if (priv_uid == 0) {
	  if (src == NULL && tgt == NULL) {
	    /* Both NULL, authorized */
	    return(0);
	  } else if (strcmp(src, tgt)==0) {
	    /* src == tmp */
	    return(0);
	  }
	} /* In other cases, a message is sent to the server for validation */

#ifndef USE_CUPV
	if (priv_uid != 0) {
		serrno = EACCES;
		return (-1);
	}
	if ((p = getconfent (CNS_SCE, "TRUST", 1)) == NULL) {
		serrno = EACCES;
		return (-1);
	}
	l = strlen (localdomain);
	for (q = strtok (p, "\t "); q; q = strtok (NULL, "\t ")) {
		if (strcmp (src, q) == 0)
			return (0);
		if (strchr (q, '.'))
			continue;
		if (strlen (q) + l + 1 > CA_MAXHOSTNAMELEN)
			continue;
		sprintf (fqn, "%s.%s", q, localdomain);
		if (strcmp (src, fqn) == 0)
			return (0);
	}
	serrno = EACCES;
	return (-1);
#else
 	/* Build request header */
	sbp = sendbuf;
	marshall_LONG (sbp, CUPV_MAGIC);
	marshall_LONG (sbp, CUPV_CHECK);
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG(sbp, msglen);

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_LONG (sbp, priv_uid);
	marshall_LONG (sbp, priv_gid);

	if (src == NULL) {
	  marshall_STRING (sbp, "");
	} else {
	  marshall_STRING(sbp, src);
	}

	if (tgt == NULL) {
	  marshall_STRING (sbp, "");
	} else {
	  marshall_STRING(sbp, tgt);
	}

	marshall_LONG (sbp, priv); 
 
	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	while ((c = send2Cupv (NULL, sendbuf, msglen, NULL, 0)) &&
	       serrno == ECUPVNACT)
	  sleep (RETRYI);
	return (c);
#endif
}

#ifndef USE_CUPV
Cupv_seterrbuf(char *buffer, int buflen)
{
	                return (0);
}
#endif

