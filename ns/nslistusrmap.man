.\" @(#)$RCSfile: nslistusrmap.man,v $ $Revision: 1.1 $ $Date: 2007/12/13 06:15:14 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH NSLISTUSRMAP 1 "$Date: 2007/12/13 06:15:14 $" CASTOR "Cns Administrator Commands"
.SH NAME
nslistusrmap \- query about a given user or list all existing users in virtual uid table
.SH SYNOPSIS
.B nslistusrmap
[
.BI --uid " uid"
] [
.BI --user " username"
]
.SH DESCRIPTION
.B nslistusrmap
queries about a given user or lists all existing users in virtual uid table.
.LP
This function requires ADMIN privilege.
.SH OPTIONS
.TP
.BI --uid " uid"
specifies the Virtual User Id.
.TP
.BI --user " username"
specifies the user name.
.SH EXAMPLES
.nf
.ft CW
nslistusrmap --uid 5678
    5678 /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=Jean Dupond
.sp
nslistusrmap --user "/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=Jean Dupond"
    5678 /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=Jean Dupond
.sp
nslistusrmap
    5678 /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=Jean Dupond
    6789 /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=Luc Martin
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_getusrbynam(3) ,
.B Cns_getusrbyuid(3)
