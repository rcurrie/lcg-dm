.\" @(#)$RCSfile: Cns_getusrmap.man,v $ $Revision: 1.1 $ $Date: 2007/12/13 06:15:14 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_GETUSRMAP 3 "$Date: 2007/12/13 06:15:14 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_getusrmap \- get all existing users from virtual uid table
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getusrmap (int *" nbentries ,
.BI "struct Cns_userinfo **" usr_entries )
.SH DESCRIPTION
.B Cns_getusrmap
gets all the existing users from the virtual uid table.
.TP
.I nbentries
will be set to the number of entries in the array of user infos.
.TP
.I usr_entries
will be set to the address of an array of Cns_userinfo structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.LP 
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B ENOMEM
Memory could not be allocated for unmarshalling the reply.
.TP
.B EFAULT
.I nbentries
or
.I usr_entries
is a NULL pointer.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
