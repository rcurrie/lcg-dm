/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_getreplicasl.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_getreplicasl - get replica entries associated with a list of path names */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_getreplicasl(int nbfiles, const char **paths, const char *se, int *nbentries, struct Cns_filereplicas **rep_entries)
{
	int c;
	char func[17];
	gid_t gid;
	int i;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[4];
	char *sbp;
	char *sendbuf;
	struct Cns_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "Cns_getreplicasl");
	if (Cns_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbfiles <= 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (! paths || ! nbentries || ! rep_entries) {
		serrno = EFAULT;
		return (-1);
	}

	if  (se && strlen (se) > CA_MAXHOSTNAMELEN) {
		serrno = EINVAL;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 5 * LONGSIZE;
	msglen += HYPERSIZE;
	msglen += LONGSIZE;
	if (se)
		msglen += strlen (se) + 1;
	else
		msglen++;
	for (i = 0; i < nbfiles; i++) {
		msglen += strlen (paths[i]) + 1;
	}

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC2);
	marshall_LONG (sbp, CNS_GETREPLICAL);
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */
 
	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_HYPER (sbp, thip->cwd);
	marshall_LONG (sbp, nbfiles);
	if (se) {
		marshall_STRING (sbp, se);
	} else {
		marshall_STRING (sbp, "");
	}
	for (i = 0; i < nbfiles; i++) {
		marshall_STRING (sbp, paths[i]);
	}
 
	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsdx (NULL, NULL, sendbuf,
	    msglen, repbuf, sizeof(repbuf), (void **)rep_entries, nbentries);
	free (sendbuf);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, *nbentries);
		if (*nbentries == 0) {
			*rep_entries = NULL;
			return (0);
		}
	}
	return (c);
}
