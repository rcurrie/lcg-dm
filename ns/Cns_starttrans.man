.\" @(#)$RCSfile: Cns_starttrans.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2010 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_STARTTRANS 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_starttrans \- start transaction mode
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_starttrans (char *" server ,
.BI "char *" comment );
.SH DESCRIPTION
.B Cns_starttrans
starts transaction mode.
The connection to the Name Server is kept open until one of the DB update
operation fails or
.B Cns_aborttrans
or
.B Cns_endtrans
is explicitly called.
If all the DB update operations between
.B Cns_starttrans
and
.B Cns_endtrans
are successful, a COMMIT is done.
If any DB update operation fails or if an explicit
.B Cns_aborttrans
is done, a ROLLBACK is done.
The
.I comment
is logged in the server log.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EINVAL
The length of
.I comment
exceeds
.BR CA_MAXCOMMENTLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_aborttrans(3) ,
.BR Cns_endtrans(3)
