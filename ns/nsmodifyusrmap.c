/*
 * Copyright (C) 2005-2010 by CERN/IT/GD/SC
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: nsmodifyusrmap.c,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

/*      nsmodifyusrmap - modify user entry corresponding to a given uid */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include "Cgetopt.h"
#include "Cns_api.h"
#include "serrno.h"
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	int errflg = 0;
	static struct Coptions longopts[] = {
		{"uid", REQUIRED_ARGUMENT, 0, OPT_IDMAP_UID},
		{"user", REQUIRED_ARGUMENT, 0, OPT_IDMAP_USER},
		{"status", REQUIRED_ARGUMENT, 0, OPT_IDMAP_STATUS},
		{0, 0, 0, 0}
	};
	char *p;
	int status = -1;
	char statusa[50];
	uid_t uid = 0;
	char *username = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_IDMAP_UID:
			if ((uid = strtol (Coptarg, &dp, 10)) < 0 || *dp != '\0') {
				fprintf (stderr, "invalid uid: %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_IDMAP_USER:
			if (strlen (Coptarg) > 255) {
				fprintf (stderr,
				    "user name too long: %s\n", Coptarg);
				errflg++;
			} else
				username = Coptarg;
			break;
		case OPT_IDMAP_STATUS:
			if (isdigit (*Coptarg)) {
				if ((status = strtol (Coptarg, &dp, 10)) < 0 ||
				    *dp != '\0') {
					fprintf (stderr,
					    "invalid status %s\n", Coptarg);
					errflg++;
				}
			} else {
				if (strlen (Coptarg) >= sizeof (statusa)) {
					fprintf (stderr,
					    "invalid status %s\n", Coptarg);
					errflg++;
					break;
				}
				status = 0;
				strcpy (statusa, Coptarg);
				p = strtok (statusa, "|");
				while (p) {
					if (strcmp (p, "ARGUS_BAN") == 0)
						status |= ARGUS_BAN;
					else if (strcmp (p, "LOCAL_BAN") == 0)
						status |= LOCAL_BAN;
					else {
						fprintf (stderr,
						    "invalid status %s\n", Coptarg);
						errflg++;
						break;
					}
					p = strtok (NULL, "|");
				}
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || uid <= 0 || username == NULL)
		errflg++;
	if (errflg) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "--uid uid --user username [--status status]\n");
		exit (USERR);
	}

	if (Cns_modifyusrmap (uid, username, status) < 0) {
		if (serrno == EEXIST)
			fprintf (stderr, "nsmodifyusrmap %s: %s\n", username,
			    "User exists already");
		else
			fprintf (stderr, "nsmodifyusrmap %d: %s\n", uid,
			    (serrno == ENOENT) ? "No such user" : sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
