.\" @(#)$RCSfile: nschgrp.man,v $ $Revision: 1.1 $ $Date: 2007/01/13 10:35:39 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 1999-2006 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH NSCHGRP 1 "$Date: 2007/01/13 10:35:39 $" CASTOR "Cns User Commands"
.SH NAME
nschgrp \- change group ownership of a CASTOR directory/file in the name server
.SH SYNOPSIS
.B nschgrp
.RB [ -h ]
.RB [ -R ]
.IR group
.IR path ...
.SH DESCRIPTION
.B nschgrp
sets the group ownership of a CASTOR directory/file in the name server to the
value of
.IR group .
.LP
To change the group ID, the effective user ID of the process must match the
owner ID of the file and the new group must be in the list of groups the caller
belong to or the caller must have ADMIN privilege in the Cupv database.
.TP
.I group
is either a valid group name or a valid numeric ID.
.TP
.I path
specifies the CASTOR pathname.
If
.I path
does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.SH OPTIONS
The following options are supported:
.TP
.B -h
If
.I path
is a symbolic link, changes the ownership of the link itself.
.TP
.B -R
Recursive mode.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chown(3) ,
.B Cupvlist(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
