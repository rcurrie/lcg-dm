--
--  Copyright (C) 2004-2006 by CERN/IT/GD/CT
--  All rights reserved
--
--       @(#)$RCSfile: Cns_mysql_drop.sql,v $ $Revision: 1.2 $ $Date: 2006/12/06 13:53:46 $ CERN IT-GD Sophie Lemaitre

--     Drop Name Server MySQL tables and constraints.


USE cns_db;
ALTER TABLE Cns_class_metadata
	DROP INDEX classid,
	DROP INDEX classname;
ALTER TABLE Cns_user_metadata
	DROP INDEX u_fileid,
	DROP FOREIGN KEY fk_u_fileid;
ALTER TABLE Cns_symlinks
	DROP INDEX fileid,
	DROP FOREIGN KEY fk_l_fileid;
ALTER TABLE Cns_file_replica
        DROP INDEX fileid,
        DROP INDEX host,
	DROP INDEX sfn,
	DROP FOREIGN KEY fk_r_fileid;
ALTER TABLE Cns_file_metadata
	DROP INDEX fileid,
	DROP INDEX file_full_id,
	DROP INDEX guid,
	DROP INDEX parent_fileid_idx;
ALTER TABLE Cns_groupinfo
	DROP INDEX groupname;
ALTER TABLE Cns_userinfo
        DROP INDEX username;

DROP TABLE Cns_unique_id;
DROP TABLE Cns_unique_gid;
DROP TABLE Cns_unique_uid;

DROP TABLE Cns_class_metadata;
DROP TABLE Cns_user_metadata;
DROP TABLE Cns_symlinks;
DROP TABLE Cns_file_replica;
DROP TABLE Cns_file_metadata;
DROP TABLE Cns_groupinfo;
DROP TABLE Cns_userinfo;
DROP TABLE schema_version;

USE mysql;
DROP DATABASE cns_db;
