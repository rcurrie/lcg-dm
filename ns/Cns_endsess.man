.\" @(#)$RCSfile: Cns_endsess.man,v $ $Revision: 1.1 $ $Date: 2005/06/30 05:26:29 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2005 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_ENDSESS 3 "$Date: 2005/06/30 05:26:29 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_endsess \- end session
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.B int Cns_endsess (void)
.SH DESCRIPTION
.B Cns_endsess
ends session.
The connection to the Name Server is closed.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_startsess(3)
