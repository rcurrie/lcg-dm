/*
 * @ (#)$RCSfile: Cns-python_typemaps.i,v $ $Revision$ $Date$ CERN IT-DM/SMD Remi Mollon
 */

/***********************************************
    SWIG input file for Cns Python interface
 (including typemaps for non-trivial functions)
***********************************************/


#ifdef SWIGPYTHON


/***************************************
   Helpers
***************************************/



%{
static int is_returncode = 0;

static PyObject* my_t_output_helper(PyObject* target, PyObject* o) {
    PyObject*   o2;
    PyObject*   o3;

    if (!target || (is_returncode && target == Py_None)) {
        target = o;
    } else {
        if (!PyTuple_Check(target)) {
            o2 = target;
            target = PyTuple_New(1);
            PyTuple_SetItem(target, 0, o2);
        }
        o3 = PyTuple_New(1);
        PyTuple_SetItem(o3, 0, o);

        o2 = target;
        target = PySequence_Concat(o2, o3);
        Py_DECREF(o2);
        Py_DECREF(o3);
    }

    is_returncode = 0;
    return target;
}

static PyObject* serrno2pyexc (int value) {
    PyObject *exc = NULL;
    switch (value) {
        case EFAULT:
        case ENOMEM:
            exc = PyExc_MemoryError;
            break;
        case EACCES:
        case EPERM:
            exc = PyExc_StandardError;
            break;
        case ENOENT:
        case EEXIST:
        case ENOTDIR:
        case EISDIR:
        case ENAMETOOLONG:
        case ERANGE:
        case SENOSHOST:
            exc = PyExc_ValueError;
            break;
        case EINVAL:
        case EBADF:
            exc = PyExc_SyntaxError;
            break;
        case SENOSSERV:
        case SECOMERR:
        case ENSNACT:
        case ENOSPC:
            exc = PyExc_SystemError;
            break;
        case SELOOP:
            exc = PyExc_RuntimeError;
            break;
        default:
            exc = PyExc_Exception;
    }
    return exc;
}
%}

%inline %{
PyObject *version () {
  char str[256];
  Cns_getifcevers (str);
  return (PyString_FromString (str));
}
%}


/***************************************
    Input (in) Typemaps 
***************************************/



%typemap(in) char * {
        if ($input == Py_None) {
                $1 = NULL;
        } else {
                $1 = PyString_AsString ($input);
                if ($1 && $1[0] == 0) $1 = NULL;
        }
}

/* Nothing to free, need to overload default typemap as of swig 1.3.28 */
%typemap(freearg) (char *) %{%}



%typemap(in) u_signed64 {
    if (PyInt_Check ($input))
        $1 = PyInt_AsUnsignedLongLongMask ($input);
    else if (PyLong_Check ($input))
        $1 = PyLong_AsUnsignedLongLong ($input);
    else {
        PyErr_SetString (PyExc_TypeError, "int or long expected");
        return NULL;
    }
}



%typemap(in, numinputs=0) (int *OUTPUT) {
   int tmp_int;
   $1 = &tmp_int;
}



%typemap(in, numinputs=0) (unsigned int *OUTPUT) {
   unsigned int tmp_int;
   $1 = &tmp_int;
}



%typemap(in, numinputs=0) (char *STRING256OUT) {
   char tmp_str[256];
   $1 = tmp_str;
}



%typemap(in, numinputs=0) (char *COMMENTOUT) {
   char tmp_str[CA_MAXCOMMENTLEN + 1];
   $1 = tmp_str;
}



%typemap(in, numinputs=0) (char *PATHOUT, int LENGTH) {
   char tmp_str[4096];
   $1 = tmp_str;
   $2 = 4096;
}



%typemap(in, numinputs=0) (int *LENGTH, struct Cns_filereplica **OUTPUT) {
   int tmp_int;
   struct Cns_filereplica *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}



%typemap(in) (char **INPUT, int LENGTH) {
  int i;

  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $2 = PyList_Size ($input);
  $1 = (char **) calloc ($2+1, sizeof (char *));
  for (i = 0; i < $2; i++) {
    PyObject *s = PyList_GetItem ($input,i);
    if (!PyString_Check (s)) {
        free ($1);
        PyErr_SetString (PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $1[i] = PyString_AsString (s);
  }
  $1[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (char **INPUT, int LENGTH) {
   if ($1) free ($1);
}



%typemap(in) (int LENGTH, const char **INPUT) {
  int i;

  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size ($input);
  $2 = (char **) calloc ($1+1, sizeof (char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem ($input,i);
    if (!PyString_Check (s)) {
        free ($2);
        PyErr_SetString (PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString (s);
  }
  $2[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (int LENGTH, const char **INPUT) {
   if ($2) free ($2);
}



%typemap(in) (const char **INPUTLIST2) {
  int i;

  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = (char **) calloc (arg1+1, sizeof (char *));
  for (i = 0; i < arg1; i++) {
    PyObject *s = PyList_GetItem ($input,i);
    if (!PyString_Check (s)) {
        free ($1);
        PyErr_SetString (PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $1[i] = PyString_AsString (s);
  }
  $1[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (const char **INPUTLIST2) {
   if ($1) free ($1);
}

%typemap(in, numinputs=0) (int *LENGTH, struct Cns_filereplicas **OUTPUT) {
   int tmp_int;
   struct Cns_filereplicas *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}



%typemap(in, numinputs=0) (int *LENGTH, struct Cns_filereplicax **OUTPUT) {
   int tmp_int;
   struct Cns_filereplicax *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}



%typemap(in, numinputs=0) (int *LENGTH, struct Cns_linkinfo **OUTPUT) {
   int tmp_int;
   struct Cns_linkinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}



%typemap(in) (int LENGTH, struct Cns_acl *INPUT) {

  /* Check the input list */
  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else {
    /* Length of the python list (how many Cns_acl structs we will need) */
    $1 = PyList_Size ($input);

    /* Reserve space to store all the list members in an array (after converting them) */
    $2 = (struct Cns_acl *) calloc ($1, sizeof (struct Cns_acl)) ;

    int i;
    for (i=0; i<$1; i++) {
      PyObject * aux_object = PyList_GetItem ($input, i);

      /* Temp pointer */
      struct Cns_acl * aux_p;

      /* This moves aux_p to point to a newly allocated Cns_acl struct representing the item in the python list */
      SWIG_ConvertPtr (aux_object, (void**) &aux_p,
                      $descriptor(struct Cns_acl *), SWIG_POINTER_EXCEPTION);

      /* Copy the value to the array */
      $2[i] = *aux_p;
    }

  }/* end of: No error */

}/* end of: typemap(in) */

/* Free the temporary Cns_acl array passed to the C function after the Cns_setacl function has returned */
%typemap(freearg) (int LENGTH, struct Cns_acl *INPUT) {
  if ($2) free ($2);
}



%typemap(in, numinputs=0) (int LENGTH, struct Cns_acl *OUTPUT) {
   $1 = 255;
   $2 = (struct Cns_acl *) calloc ($1, sizeof (struct Cns_acl));
}



%typemap(in) (int LENGTH, struct Cns_filereg *INPUT) {

  /* Check the input list */
  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else {
    /* Length of the python list (how many structs we will need) */
    $1 = PyList_Size ($input);

    /* Reserve space to store all the list members in an array (after converting them) */
    $2 = (struct Cns_filereg *) calloc ($1, sizeof (struct Cns_filereg)) ;

    int i;
    for (i=0; i<$1; i++) {
      PyObject * aux_object = PyList_GetItem ($input, i);

      /* Temp pointer */
      struct Cns_filereg * aux_p;

      /* This moves aux_p to point to a newly allocated Cns_filereg struct representing the item in the python list */
      SWIG_ConvertPtr (aux_object, (void**) &aux_p,
                      $descriptor(struct Cns_filereg *), SWIG_POINTER_EXCEPTION);

      /* Copy the value to the array */
      $2[i] = *aux_p;
    }

  }/* end of: No error */

}/* end of: typemap(in) */

/* Free the temporary Cns_filereg array passed to the C function after the Cns_registerfiles function has returned */
%typemap(freearg) (int LENGTH, struct Cns_filereg *INPUT) {
  if ($2) free ($2);
}



%typemap(in) (int LENGTH, gid_t *INPUT, char **OUTPUTS1) {
  int i;

  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size ($input);
  $2 = (gid_t *) calloc ($1, sizeof (gid_t));
  $3 = (char **) calloc ($1 + 1, sizeof (char *));

  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem ($input,i);
    if (!PyInt_Check (s)) {
        free ($2);
        PyErr_SetString (PyExc_ValueError, "List items must be integers");
        return NULL;
    }
    $2[i] = (gid_t) PyInt_AsLong (s);
  }
}

%typemap(freearg) (int LENGTH, gid_t *INPUT) {
  if ($2) free ($2);
}

%typemap(freearg) (char **OUTPUTS1) {
   if ($1) free ($1);
}

%typemap(freearg) (int LENGTH, gid_t *INPUT) {
  if ($2) free ($2);
}

%typemap(freearg) (char **OUTPUTS1) {
   if ($1) free ($1);
}



%typemap(in, numinputs=0) (int *LENGTH, int **OUTPUT) {
   int tmp_int;
   int *tmp_tab;
   $1 = &tmp_int;
   $2 = &tmp_tab;
}



%typemap(in, numinputs=0) (int *LENGTH, struct Cns_filestatus **OUTPUT) {
   int tmp_int;
   struct Cns_filestatus *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}



%typemap(in, numinputs=0) (int *LENGTH, struct Cns_userinfo **OUTPUT) {
   int tmp_int;
   struct Cns_userinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}



%typemap(in, numinputs=0) (int *LENGTH, struct Cns_groupinfo **OUTPUT) {
   int tmp_int;
   struct Cns_groupinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}



%typemap(in, numinputs=0) (struct Cns_filestat *STATOUT) {
   $1 = (struct Cns_filestat *) calloc (1, sizeof (struct Cns_filestat));
}



%typemap(in, numinputs=0) (struct Cns_filestatg *STATOUT) {
   $1 = (struct Cns_filestatg *) calloc (1, sizeof (struct Cns_filestatg));
}



%typemap(in, numinputs=1) (int LENGTH, const char **INPUT, uid_t *OUTPUT, gid_t *IDMAPOUTPUT) (uid_t tmp_uid){
  int i;

  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size ($input);
  $2 = (char **) calloc ($1+1, sizeof (char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem ($input,i);
    if (!PyString_Check (s)) {
        free ($2);
        PyErr_SetString (PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString (s);
  }
  $2[i] = 0;
 
  $3 = &tmp_uid;
  $4 = (gid_t *) calloc ($1 > 0 ? $1 : 1, sizeof (gid_t));
}

%typemap(freearg) (gid_t *IDMAPOUTPUT) {
   if ($1) free ($1);
}



%typemap(in, numinputs=1) (int LENGTH, const char **INPUT, uid_t *OUTPUT, gid_t *IDMAPOUTPUT1) (uid_t tmp_uid){
  int i;

  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size ($input);
  $2 = (char **) calloc ($1+1, sizeof (char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem ($input,i);
    if (!PyString_Check (s)) {
        free ($2);
        PyErr_SetString (PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString (s);
  }
  $2[i] = 0;
 
  $3 = &tmp_uid;
  $4 = (gid_t *) calloc ($1 > 0 ? $1 : 1, sizeof (gid_t));
}

%typemap(freearg) (gid_t *IDMAPOUTPUT) {
   if ($1) free ($1);
}



%typemap(in, numinputs=1) (int LENGTH, const char **INPUT, uid_t *OUTPUT, gid_t *IDMAPOUTPUT1) (uid_t tmp_uid){
  int i;

  if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size ($input);
  $2 = (char **) calloc ($1+1, sizeof (char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem ($input,i);
    if (!PyString_Check (s)) {
        free ($2);
        PyErr_SetString (PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString (s);
  }
  $2[i] = 0;
 
  $3 = &tmp_uid;
  $4 = (gid_t *) calloc ($1 > 0 ? $1 : 1, sizeof (gid_t));
}

%typemap(freearg) (gid_t *IDMAPOUTPUT1) {
   if ($1) free ($1);
}



/***************************************
    Output (out/argout) Typemaps 
***************************************/



%typemap(out,noblock=1) RETURNCODE {
  if ($1 < 0) {
      PyErr_SetString (serrno2pyexc (serrno), serrbuf);
      return (NULL);
  }

  is_returncode = 1;
  $result = Py_None;
}



%typemap(out,noblock=1) RETURNCODE_BOOL {
  if ($1 < 0) {
      PyErr_SetString (serrno2pyexc (serrno), serrbuf);
      return (NULL);
  }

  is_returncode = 0;
  $result = PyInt_FromLong ($1);
}



%typemap(out,noblock=1) int {
  $result = PyInt_FromLong ($1);
}



%typemap(out) char[ANY] {
      $result = PyString_FromString ($1);
}



%typemap(argout) (int *OUTPUT) {
  $result = my_t_output_helper($result, PyInt_FromLong (*$1));
}



%typemap(argout) (unsigned int *OUTPUT) {
  $result = my_t_output_helper($result, PyInt_FromLong (*$1));
}



%typemap(argout) (char *STRING256OUT), (char *COMMENTOUT), (char *PATHOUT, int LENGTH) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyString_FromString ($1);
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (int LENGTH, struct Cns_acl *OUTPUT) {
  PyObject *myresult;

  /* Error */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {

    /* No structs returned, only number of ACL entries */
    if ($2 == 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
    }

    /* Structs returned in array */
    else {
       int i, len = result;
       myresult = PyList_New (len);

       for (i = 0; i < len; i++) {
         /* Let python garbage-collect only the 1st one. That will already delete it all!
            Because the original var was Cns_al* and not Cns_acl** (only 1 pointer responsible) */
         int PYTHON_OWNED = i > 0 ? 0 : 1;

          /* Python object for each of the Cns_acl structs set by the C call */
         PyObject * aux_obj = SWIG_NewPointerObj (&$2[i], $descriptor(struct Cns_acl *), PYTHON_OWNED);

         PyList_SetItem (myresult, i, aux_obj);

       }/* end of for */

    }/* end of: Structs returned in array */

  }/* end of: No error */

  $result = my_t_output_helper($result, myresult);
}/* end of: typemap(argout) */



%typemap(out) struct Cns_direnrep * {
  PyObject *myresult;

  /* Don't let python garbage-collect these structures, because they are freed with Cns_closedir */
  int PYTHON_OWNED=0;

  /* Error, return None */
  if ($1 == 0) {
    Py_INCREF (Py_None);
    myresult = Py_None;
  }
  /* No error */
  else {
    /* Python object for the Cns_direnrep set by the C call */
    myresult = SWIG_NewPointerObj ($1, $descriptor(struct Cns_direnrep *), PYTHON_OWNED);
  }

  $result = my_t_output_helper(NULL, myresult);
}


%typemap(out) struct Cns_rep_info * {
  PyObject *myresult;
  int len = (arg1)->nbreplicas;

  /* Don't let python garbage-collect these structures, because they are freed with Cns_closedir */
  int PYTHON_OWNED=0;

  /* Error, return None */
  if ($1 == 0 || len < 0) {
    Py_INCREF (Py_None);
    myresult = Py_None;
  }
  /* No error */
  else {
      myresult = PyList_New (len);
      int i;

      for (i = 0; i < len; i++) {
          /* Python object for each of the Cns_rep_info structs set by the C call */
          PyObject * aux_rep = SWIG_NewPointerObj ($1 + i, $descriptor(struct Cns_rep_info *), PYTHON_OWNED);
          PyList_SetItem (myresult, i, aux_rep);
      }
  }

  $result = my_t_output_helper(NULL, myresult);
}



%typemap(argout) (int *LENGTH, struct Cns_filereplica **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyList_New (*$1);

    int i;
    for (i = 0; i < *$1; i++) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct Cns_filereplica * aux = (struct Cns_filereplica *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj (aux, $descriptor(struct Cns_filereplica *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem (myresult, i, aux_obj);

    }
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (int *LENGTH, struct Cns_filereplicas **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyList_New (*$1);

    int i;
    for (i = 0; i < *$1; i++) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct Cns_filereplicas * aux = (struct Cns_filereplicas *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj (aux, $descriptor(struct Cns_filereplicas *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem (myresult, i, aux_obj);

    }
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (int *LENGTH, struct Cns_filereplicax **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyList_New (*$1);

    int i;
    for (i = 0; i < *$1; i++) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct Cns_filereplicax * aux = (struct Cns_filereplicax *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj (aux, $descriptor(struct Cns_filereplicax *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem (myresult, i, aux_obj);

    }
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (int *LENGTH, struct Cns_linkinfo **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    myresult = PyList_New (*$1);

    int i;
    for (i = 0; i < *$1; i++) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct Cns_linkinfo * aux = (struct Cns_linkinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj (aux, $descriptor(struct Cns_linkinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem (myresult, i, aux_obj);

    }
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (int *LENGTH, int **OUTPUT) {
  PyObject *myresult;

  /* Error */
  if (result < 0 || *$1 < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
      int i;
      PyObject *aux_obj;
      myresult = PyList_New (*$1);

      for (i = 0; i < *$1; i++) {
          aux_obj = PyInt_FromLong ((long) (*$2)[i]);
          PyList_SetItem (myresult, i, aux_obj);
      }/* end of for */
  }/* end of: No error */

  $result = my_t_output_helper($result, myresult);
}/* end of: typemap(argout) */



%typemap(argout) (int *LENGTH, struct Cns_filestatus **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0 || *$1 < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int i;
    myresult = PyList_New (*$1);

    for (i = 0; i < *$1; i++) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct Cns_filestatus * aux = (struct Cns_filestatus *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj (aux, $descriptor(struct Cns_filestatus *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem (myresult, i, aux_obj);
    }
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (int *LENGTH, struct Cns_userinfo **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0 || *$1 < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int i;
    myresult = PyList_New (*$1);

    for (i = 0; i < *$1; i++) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct Cns_userinfo * aux = (struct Cns_userinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj (aux, $descriptor(struct Cns_userinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem (myresult, i, aux_obj);
    }
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (int *LENGTH, struct Cns_groupinfo **OUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0 || *$1 < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int i;
    myresult = PyList_New (*$1);

    for (i = 0; i < *$1; i++) {
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i > 0 ? 0 : 1;

      struct Cns_groupinfo * aux = (struct Cns_groupinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj (aux, $descriptor(struct Cns_groupinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyList_SetItem (myresult, i, aux_obj);
    }
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (struct Cns_filestat *STATOUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int PYTHON_OWNED = 1;

    struct Cns_filestat * aux = (struct Cns_filestat *) $1;

    /* Create a python object based on the C struct set by the C function */
    myresult = SWIG_NewPointerObj (aux, $descriptor(struct Cns_filestat *), PYTHON_OWNED);
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (struct Cns_filestatg *STATOUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int PYTHON_OWNED = 1;

    struct Cns_filestatg * aux = (struct Cns_filestatg *) $1;

    /* Create a python object based on the C struct set by the C function */
    myresult = SWIG_NewPointerObj (aux, $descriptor(struct Cns_filestatg *), PYTHON_OWNED);
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (char **OUTPUTS1) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int i;
    myresult = PyList_New (arg1);

    for (i = 0; i < arg1; ++i)
      PyList_SetItem (myresult, i, PyString_FromString ($1[i]));
  }

  $result = my_t_output_helper($result, myresult);
}



%typemap(argout) (gid_t *IDMAPOUTPUT) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int i;
    myresult = PyList_New (arg2);

    for (i = 0; i < arg2; ++i)
      PyList_SetItem (myresult, i, PyInt_FromLong ($1[i]));
  }

  $result = my_t_output_helper($result, myresult);
}


%typemap(argout) (gid_t *IDMAPOUTPUT1) {
  PyObject *myresult;

  /* Error, return None */
  if (result < 0) {
      Py_INCREF (Py_None); 
      myresult = Py_None;
  }
  /* No error */
  else {
    int i;
    myresult = PyList_New (arg3);

    for (i = 0; i < arg3; ++i)
      PyList_SetItem (myresult, i, PyInt_FromLong ($1[i]));
  }

  $result = my_t_output_helper($result, myresult);
}

#endif
