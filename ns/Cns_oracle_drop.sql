--
--  Copyright (C) 2004-2006 by CERN/IT/GD/CT
--  All rights reserved
--
--       @(#)$RCSfile: Cns_oracle_drop.sql,v $ $Revision: 1.4 $ $Date: 2006/12/06 14:06:12 $ CERN IT-GD Sophie Lemaitre

--     Drop Name Server Oracle tables and constraints.

ALTER TABLE Cns_class_metadata
       	DROP CONSTRAINT pk_classid
	DROP CONSTRAINT classname;
ALTER TABLE Cns_user_metadata 
	DROP CONSTRAINT fk_u_fileid
       	DROP CONSTRAINT pk_u_fileid;
ALTER TABLE Cns_symlinks
	DROP CONSTRAINT fk_l_fileid
	DROP CONSTRAINT pk_l_fileid;
ALTER TABLE Cns_file_replica
	DROP CONSTRAINT fk_r_fileid
       	DROP CONSTRAINT pk_repl_sfn;
ALTER TABLE Cns_groupinfo
       	DROP CONSTRAINT pk_map_groupname;
ALTER TABLE Cns_userinfo
       	DROP CONSTRAINT pk_map_username;
ALTER TABLE Cns_file_metadata
        DROP CONSTRAINT pk_fileid
        DROP CONSTRAINT file_full_id
        DROP CONSTRAINT file_guid;

DROP INDEX replica_id;
DROP INDEX parent_fileid_idx;
DROP INDEX replica_host;

DROP SEQUENCE Cns_unique_id;

DROP TABLE Cns_unique_gid;
DROP TABLE Cns_unique_uid;
DROP TABLE Cns_class_metadata;
DROP TABLE Cns_file_metadata;
DROP TABLE Cns_user_metadata;
DROP TABLE Cns_symlinks;
DROP TABLE Cns_file_replica;
DROP TABLE Cns_groupinfo;
DROP TABLE Cns_userinfo;
DROP TABLE schema_version;
