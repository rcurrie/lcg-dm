/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_delfilesbyname.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_delfilesbyname - delete entries associated with a list of paths 
	and associated replica entries if force is set to 1 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_delfilesbyname(int nbfiles, const char **paths, int force, int *nbstatuses, int **statuses)
{
	int argtype = 1;
	int c;
	char func[19];
	gid_t gid;
	int i;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[4];
	char *sbp;
	char *sendbuf;
	struct Cns_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "Cns_delfilesbyname");
	if (Cns_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbfiles <= 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (! paths || ! nbstatuses || ! statuses) {
		serrno = EFAULT;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 5 * LONGSIZE;
	msglen += WORDSIZE;
	msglen += WORDSIZE;
	msglen += HYPERSIZE;
	msglen += LONGSIZE;
	for (i = 0; i < nbfiles; i++) {
		msglen += strlen (paths[i]) + 1;
	}

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC);
	marshall_LONG (sbp, CNS_DELFILES);
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */
 
	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_WORD (sbp, argtype);
	marshall_WORD (sbp, force);
	marshall_HYPER (sbp, thip->cwd);
	marshall_LONG (sbp, nbfiles);
	for (i = 0; i < nbfiles; i++) {
		marshall_STRING (sbp, paths[i]);
	}
 
	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsdx (NULL, NULL, sendbuf,
	    msglen, repbuf, sizeof(repbuf), (void **)statuses, nbstatuses);
	free (sendbuf);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, *nbstatuses);
		if (*nbstatuses == 0) {
			*statuses = NULL;
			return (0);
		}
	}
	return (c);
}
