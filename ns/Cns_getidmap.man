.\" @(#)$RCSfile: Cns_getidmap.man,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2010 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH CNS_GETIDMAP 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_getidmap \- get virtual uid/gid(s) associated with a given dn/role(s)
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getidmap (const char *" username ,
.BI "int " nbgroups ,
.BI "const char **" groupnames ,
.BI "uid_t *" userid ,
.BI "gid_t *" gids )
.sp
.BI "int Cns_getidmapc (const char *" username ,
.BI "const char *" user_ca ,
.BI "int " nbgroups ,
.BI "const char **" groupnames ,
.BI "uid_t *" userid ,
.BI "gid_t *" gids )
.SH DESCRIPTION
.B Cns_getidmap
gets the virtual uid/gid(s) associated with a given dn/role(s).
If the dn or any of the roles does not exist yet in the Database an entry
is added to the mapping table.
.TP
.I username
specifies the user name (dn).
It must be at most 255 characters long.
.TP
.I user_ca
specifies the CA from the user proxy.
It must be at most 255 characters long.
.TP
.I nbgroups
is the number of group names.
.TP
.I groupnames
is the address of an array of group names (vo/role).
Each group name must be at most 255 characters long.
This pointer can be NULL.
In this case the vo is obtained from a static mapping file.
.TP
.I userid
specifies the address of a buffer to receive the Virtual User Id.
.TP
.I gids
specifies the address of a buffer to receive the Virtual Group Ids.
It must be big enough to receive
.I nbgroups
group ids, but at least one group id in case
.I nbgroups
is zero.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
Memory could not be allocated in the server to receive the group names or
to send back the gids.
.TP
.B EACCES
One of
.IR username ,
.I user_ca
or the primary (first) group is banned.
.TP
.B EFAULT
.IR username ,
.I userid
or
.I gids
is a NULL pointer.
.TP
.B EINVAL
.I nbgroups
is negative or the length of 
.IR username ,
.I user_ca
or of one of the group names exceeds 255.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B SENOMAPFND
.B DISABLEAUTOVIDS
is set and
.I username
or one of the groups does not exist.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
