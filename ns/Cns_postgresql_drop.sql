--
--  Copyright (C) 2004-2008 by CERN/IT/GD/CT
--  All rights reserved
--
--       @(#)$RCSfile: Cns_postgresql_drop.sql,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD Jean-Philippe Baud

--     Drop Name Server PostgreSQL tables and constraints.

ALTER TABLE Cns_class_metadata
       	DROP CONSTRAINT pk_classid;
ALTER TABLE Cns_class_metadata
	DROP CONSTRAINT classname;
ALTER TABLE Cns_user_metadata 
	DROP CONSTRAINT fk_u_fileid;
ALTER TABLE Cns_user_metadata 
       	DROP CONSTRAINT pk_u_fileid;
ALTER TABLE Cns_symlinks
	DROP CONSTRAINT fk_l_fileid;
ALTER TABLE Cns_symlinks
	DROP CONSTRAINT pk_l_fileid;
ALTER TABLE Cns_file_replica
	DROP CONSTRAINT fk_r_fileid;
ALTER TABLE Cns_file_replica
       	DROP CONSTRAINT pk_repl_sfn;
ALTER TABLE Cns_groupinfo
       	DROP CONSTRAINT pk_map_groupname;
ALTER TABLE Cns_userinfo
       	DROP CONSTRAINT pk_map_username;
ALTER TABLE Cns_file_metadata
        DROP CONSTRAINT pk_fileid;
ALTER TABLE Cns_file_metadata
        DROP CONSTRAINT file_full_id;
ALTER TABLE Cns_file_metadata
        DROP CONSTRAINT file_guid;

DROP INDEX replica_id;
DROP INDEX replica_host;
DROP INDEX parent_fileid_idx;

DROP SEQUENCE Cns_unique_id;

DROP TABLE Cns_unique_gid;
DROP TABLE Cns_unique_uid;
DROP TABLE Cns_class_metadata;
DROP TABLE Cns_file_metadata;
DROP TABLE Cns_user_metadata;
DROP TABLE Cns_symlinks;
DROP TABLE Cns_file_replica;
DROP TABLE Cns_groupinfo;
DROP TABLE Cns_userinfo;
DROP TABLE schema_version;
