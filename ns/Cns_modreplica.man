.\" @(#)$RCSfile: Cns_modreplica.man,v $ $Revision: 1.3 $ $Date: 2008/08/27 12:43:28 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_MODREPLICA 3 "$Date: 2008/08/27 12:43:28 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_modreplica \- modify information about a given replica
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_modreplica (const char *" sfn ,
.BI "const char *" setname ,
.BI "const char *" poolname ,
.BI "const char *" server )
.sp
.BI "int Cns_modreplicax (const char *" sfn ,
.BI "const char *" setname ,
.BI "const char *" poolname ,
.BI "const char *" server ,
.BI "const char *" fs ,
.BI "const char " r_type )
.SH DESCRIPTION
.B Cns_modreplica
modifies information about a given replica.
.TP
.I sfn
is either the Site URL or the Physical File Name for the replica.
.TP
.I setname
is either a replica set name or a space token.
.TP
.I server
is either the Storage Element fully qualified hostname or the disk server.
.TP
.I poolname
specifies the disk pool name (this argument is only meaningful for the Disk Pool
Manager).
.TP
.B Cns_modreplicax
is similar to
.B Cns_modreplica
but has the additional arguments:
.TP
.I fs
specifies the mount point of the dedicated filesystem (this
argument is only meaningful for the Disk Pool Manager).
.TP
.I r_type
may be one of the following:
.RS
.TP
.B P
Primary  
.TP
.B S
Secondary.
.RE
.SH RETURN VALUE
These routines return 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named replica does not exist.
.TP
.B EACCES
Search permission is denied on a component of the parent directory or
the effective user ID does not match the owner of the file or
read permission on the file entry itself is denied.
.TP
.B EFAULT
.I sfn
is a NULL pointer.
.TP
.B EINVAL
The length of
.I setname
exceeds 36,
the length of
.I poolname
exceeds
.B CA_MAXPOOLNAMELEN
or the length of
.I server
exceeds
.BR CA_MAXHOSTNAMELEN .
.TP
.B ENAMETOOLONG
The length of
.I sfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_addreplica(3) ,
.BR Cns_listreplica(3)
