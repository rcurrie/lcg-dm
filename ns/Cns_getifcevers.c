/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_getifcevers.c,v $ $Revision: 1.1 $ $Date: 2007/05/09 06:42:05 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*      Cns_getifcevers - get name server client version number */

#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include "Cns_api.h"
#include "patchlevel.h"
#include "serrno.h"

int DLL_DECL
Cns_getifcevers(char *version)
{
	if (! version) {
		serrno = EFAULT;
		return (-1);
	}
	sprintf (version, "%s-%d", BASEVERSION, PATCHLEVEL);
	return (0);
}
