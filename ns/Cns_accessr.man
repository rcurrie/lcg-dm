.\" @(#)$RCSfile: Cns_accessr.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007-2010 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_ACCESSR 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_accessr \- check existence/accessibility of a file replica
.SH SYNOPSIS
Under Unix:
.br
.B #include <sys/types.h>
.br
.B #include <unistd.h>
.br
\fB#include "Cns_api.h"\fR
.sp
Under Windows/NT:
.br
.B #include <sys/types.h>
.br
.B #define R_OK 4
.br
.B #define W_OK 2
.br
.B #define X_OK 1
.br
.B #define F_OK 0
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_accessr (const char *" sfn ,
.BI "int " amode );
.SH DESCRIPTION
.B Cns_accessr
checks in the name server database the existence or the accessibility of the
file replica
.I sfn
according to the bit pattern in
.I amode
using the real user ID.
.TP
.I sfn
is either the Site URL or the Physical File Name for the replica.
.TP
.I amode
the bit pattern is built by an OR of the constants defined in
.RB < unistd.h >
under Unix or to be explicitly defined under Windows/NT:
.RS
.TP
.B R_OK
test for read permission
.TP
.B W_OK
test for write permission
.TP
.B X_OK
test for search/execute permission
.TP
.B F_OK
test for existence of the file.
.RE
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file does not exist.
.TP
.B EACCES
Search permission is denied on a component of the parent file prefix,
the specified access to the file itself is denied or W_OK is requested but
the replica status is not 'P' (cannot modify an existing file).
.TP
.B EFAULT
.I sfn
is a NULL pointer.
.TP
.B EINVAL
.I amode
is invalid.
.TP
.B ENAMETOOLONG
The length of
.I sfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chmod(3) ,
.B Cns_statr(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
