.\" @(#)$RCSfile: Cns_readdirc.man,v $ $Revision$ $Date$ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 2011 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH CNS_READDIRC 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_readdirc \- read CASTOR directory opened by
.B Cns_opendir
in the name server
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "struct Cns_direncomm *Cns_readdirc (Cns_DIR *" dirp )
.SH DESCRIPTION
.B Cns_readdirc
reads the CASTOR directory opened by
.B Cns_opendir
in the name server.
This routine returns a pointer to a structure containing the current directory
entry including the comment associated.
.B Cns_readdirc
caches a variable number of such entries, depending on the filename size, to
minimize the number of requests to the name server.
.TP
.I dirp
specifies the pointer value returned by
.BR Cns_opendir .
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current directory
entry if the operation was successful or NULL if the end of the directory was
reached or if the operation failed. When the end of the directory is encountered,
serrno is not changed. If the operation failed,
.B serrno
is set appropriately.

As Cns_readdirc returns a null pointer
both at the end of the directory and on error, an application wishing to check
for error situations should set
.B serrno
to 0, then call Cns_readdirc, then check
.B serrno
and if it is non-zero, assume an error has occurred.
.SH ERRORS
.TP 1.3i
.B EBADF
File descriptor in DIR structure is invalid.
.TP
.B EFAULT
.I dirp
is a NULL pointer.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_closedir(3) ,
.BR Cns_opendir(3) ,
.BR Cns_rewinddir(3) ,
.B Cns_setcomment (3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
