.\" @(#)$RCSfile: nslistgrpmap.man,v $ $Revision: 1.1 $ $Date: 2007/12/13 06:15:14 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH NSLISTGRPMAP 1 "$Date: 2007/12/13 06:15:14 $" CASTOR "Cns Administrator Commands"
.SH NAME
nslistgrpmap \- query about a given group or list all existing groups in virtual gid table
.SH SYNOPSIS
.B nslistgrpmap
[
.BI --gid " gid"
] [
.BI --group " groupname"
]
.SH DESCRIPTION
.B nslistgrpmap
queries about a given group or lists all existing groups in virtual gid table.
.LP
This function requires ADMIN privilege.
.SH OPTIONS
.TP
.BI --gid " gid"
specifies the Virtual Group Id.
.TP
.BI --group " groupname"
specifies the group name.
.SH EXAMPLES
.nf
.ft CW
nslistgrpmap --gid 2688
    2688 dteam
.sp
nslistgrpmap --group dteam
    2688 dteam
.sp
nslistgrpmap
    2688 dteam
    2689 ops
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_getgrpbynam(3) ,
.B Cns_getgrpbygid(3)
