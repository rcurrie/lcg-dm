.\" @(#)$RCSfile: Cns_listrepset.man,v $ $Revision: 1.1 $ $Date: 2006/12/01 09:19:37 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_LISTREPSET 3 "$Date: 2006/12/01 09:19:37 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_listrepset \- list replica entries that belong to a given set
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "struct Cns_filereplicax *Cns_listrepset (const char *" setname ,
.BI "int " flags ,
.BI "Cns_list *" listp )
.SH DESCRIPTION
.B Cns_listrepset
lists replica entries that belong to a given set.
This routine returns a pointer to a structure containing the current replica
entry.
.PP
.nf
.ft CW
struct Cns_filereplicax {
	u_signed64	fileid;
	u_signed64	nbaccesses;
	time_t		ctime;		/* replica creation time */
	time_t		atime;		/* last access to replica */
	time_t		ptime;		/* replica pin time */
	time_t		ltime;		/* replica lifetime */
	char		r_type;		/* 'P' for Primary, 'S' for Secondary */
	char		status;
	char		f_type;		/* 'V' for Volatile, 'P' for Permanent */
	char		setname[37];
	char		poolname[CA_MAXPOOLNAMELEN+1];
	char		host[CA_MAXHOSTNAMELEN+1];
	char		fs[80];
	char		sfn[CA_MAXSFNLEN+1];
};
.ft
.fi
.TP
.I setname
is either the replica set name or the space token.
.TP
.I flags
may be one of the following constant:
.RS
.TP
.B CNS_LIST_BEGIN
the first call must have this flag set to allocate buffers and
initialize pointers.
.TP
.B CNS_LIST_CONTINUE
all the following calls must have this flag set.
.TP
.B CNS_LIST_END
final call to terminate the list and free resources.
.RE
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current replica
entry if the operation was successful or NULL if all entries have been returned
or if the operation failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
Memory could not be allocated for the output buffer.
.TP
.B EFAULT
.I setname
or
.I listp
is a NULL pointer.
.TP
.B EINVAL
The length of
.I setname
exceeds 36.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_addreplicax(3) ,
.B Cns_modreplica(3)
