.\" @(#)$RCSfile: lfc2_python.man,v $ $Revision$ $Date$ CERN IT-DM/SGT Remi Mollon
.\" Copyright (C) 2004-2010 by CERN/IT/DM/SGT
.\" All rights reserved
.\"
.TH lfc2_python 3 "$Date$" LFC "Python Reference"
.SH NAME
lfc2 \- New python interface to the LFC client API
.br
lfc2thr \- Thread enabled version of Python interface to the LFC
.SH SYNOPSIS
.B import lfc2 as lfc
.br

.br
.B import lfc2thr as lfc
.br
.B lfc.init()

.SH DESCRIPTION
The lfc module permits you to access the LFC client interface from python
programs. The lfc module is a swig wrapping of the standard C interface.
For detailed descriptions of each function see the individual man page of
each function.

The lfcthr module is a version of the lfc module supporting multi-threaded 
Python clients. Its usage is similar to the usage of the lfc module except
the obligatory initialisation call lfcthr.init() in the main program before 
threads are started.
 
.SH FUNCTIONS
.B  lfc_aborttrans ( )

.BI "lfc_access ( string " path ,
.BI "int " amode
.BI  ") -> bool " access

.BI "lfc_accessr ( string " sfn ,
.BI "int " amode
.BI ") -> bool " access

.BI "lfc_addreplica ( string " guid ,
.BI "struct lfc_fileid *" file_uniqueid ,
.BI "string " server ,
.BI "string " sfn ,
.BI "char " status ,
.BI "char " f_type ,
.BI "string " poolname ,
.BI "string " fs
.B  )

.BI "lfc_addreplicax ( string " guid ,
.BI "struct lfc_fileid *" file_uniqueid ,
.BI "string " server ,
.BI "string " sfn ,
.BI "char " status ,
.BI "char " f_type ,
.BI "string " poolname ,
.BI "string " fs ,
.BI "char " r_type ,
.BI "string " setname
.B  )

.BI "lfc_chdir ( string " path
.B  )

.BI "lfc_chmod ( string " path ,
.BI "mode_t " mode
.B  )

.BI "lfc_chown ( string "path ,
.BI "uid_t " new_uid ,
.BI "gid_t " new_gid
.B  )

.BI "lfc_client_setAuthorizationId ( uid_t " uid ,
.BI "gid_t " gid ,
.BI "string " mech ,
.BI "string " id
.B  )

.BI "lfc_client_setVOMS_data ( string " voname ,
.BI "stringlist " vomsdata
.B  )

.BI "lfc_closedir ( lfc_DIR " dirp
.B  )

.BI "lfc_creatg ( string " path ,
.BI "string " guid ,
.BI "mode_t " mode
.B  )

.BI "lfc_delcomment ( string " path
.B  )

.BI "lfc_delete ( string " path
.B  )

.BI "lfc_delfilesbyguid ( ListOfString " guids ,
.BI "int " force
.BI ") -> ListOfInt " result

.BI "lfc_delfilesbyname ( ListOfString " names ,
.BI int force
.BI ") -> ListOfInt " results

.BI "lfc_delfilesbypattern ( string " path ,
.BI "string " pattern ,
.BI "int " force
.BI ") -> ListOf struct lfc_filestatus " results

.BI "lfc_delreplica ( string " guid ,
.BI "struct lfc_fileid *" file_uniqueid ,
.BI "string " sfn
.B  )

.BI "lfc_delreplicas ( ListOfString " sfns ,
.BI "string " se
.BI ") -> ListOfInt " results

.BI "lfc_delreplicasbysfn ( ListOfString " sfns ,
.BI "ListOfString " guids
.BI ") -> ListOfInt " results

.B  lfc_endsess ( )

.B  lfc_endtrans ( )

.BI "lfc_getacl ( string " path ,
.BI ") -> ListOf struct lfc_acl " acls

.BI "lfc_getcomment ( string " path
.BI ") -> string " comment

.BI "lfc_getcwd ( ) -> string " dir

.BI "lfc_getifcevers ( ) -> string " version

.BI "lfc_getlinks ( string " path ,
.BI "string "guid
.BI ") -> ListOf struct lfc_linkinfo " links

.BI "lfc_getreplica ( string " path ,
.BI "string " guid ,
.BI "string " se
.BI ") -> ListOf struct lfc_filereplica " replicas

.BI "lfc_getreplicas ( ListOfString " guids ,
.BI "string " se
.BI ") -> ListOf struct lfc_filereplicas " replicas

.BI "lfc_getreplicasl ( ListOfString " paths ,
.BI "string " se
.BI ") -> ListOf struct lfc_filereplicas " replicas

.BI "lfc_getreplicass ( ListOfString " paths ,
.BI ") -> ListOf struct lfc_filereplicas " replicas

.BI "lfc_getreplicax ( string " path ,
.BI "string " guid ,
.BI "string " se
.BI ") -> ListOf struct lfc_filereplicax " replicas

.BI "lfc_lchown ( string " path ,
.BI "uid_t " new_uid ,
.BI "gid_t "new_gid
.B  )

.BI "lfc_listreplicax ( string " poolname ,
.BI "string " server ,
.BI "string " fs ,
.BI "int " flags ,
.BI "lfc_list * listp
.BI ") -> struct lfc_filereplica " replica

.BI "lfc_listrepset ( string " setname ,
.BI "int " flags ,
.BI "lfc_list *" listp
.BI ") -> struct lfc_filereplicax " replica

.BI "lfc_lstat ( string " path
.BI ") -> struct lfc_filestat " statbuf

.BI "lfc_mkdir ( string " path ,
.BI "mode_t " mode
.B  )

.BI "lfc_mkdirg ( string " path ,
.BI "string " guid ,
.BI "mode_t " mode
.B  )

.BI "lfc_modreplica ( string " sfn ,
.BI "string " setname ,
.BI "string " poolname ,
.BI "string " server
.B  )

.BI "lfc_modreplicax ( string " sfn ,
.BI "string " setname ,
.BI "string " poolname ,
.BI "string " server ,
.BI "string " fs ,
.BI "char " r_type
.B  )

.BI "lfc_opendir ( string " path
.BI ") -> lfc_DIR " dir

.BI "lfc_opendirg ( string " path ,
.BI "string " guid
.BI ") -> lfc_DIR " dir

.BI "lfc_opendirxg ( string " server ,
.BI "string " path ,
.BI "string " guid
.BI ") -> lfc_DIR " dir

.BI "lfc_ping ( string " server
.BI ") -> string " info

.BI "lfc_readdir ( lfc_DIR " dirp
.BI ") -> struct dirent " dirent

.BI "lfc_readdirc ( lfc_DIR " dirp
.BI ") -> struct lfc_direncomm " dirent

.BI "lfc_readdirg ( lfc_DIR " dirp
.BI ") -> struct lfc_direnstatg " dirent

.BI "lfc_readdirx ( lfc_DIR " dirp
.BI ") -> struct lfc_direnstat " dirent

.BI "lfc_readdirxc ( lfc_DIR " dirp
.BI ") -> struct lfc_direnstatc " dirent

.BI "lfc_readdirxp ( lfc_DIR " dirp ,
.BI "string " pattern ,
.BI "string " se
.BI "= None ) -> struct lfc_direnrep " dirent

.BI "lfc_readdirxr ( lfc_DIR " dirp ,
.BI "string " se
.BI "= None ) -> struct lfc_direnrep " dirent

.BI "lfc_readlink ( string " path
.BI ") -> string " link

.BI "lfc_registerfiles ( ListOf struct lfc_filereg " files
.BI ") -> ListOfInt " results

.BI "lfc_rename ( string " oldpath ,
.BI "string " newpath
.B  )

.BI "lfc_rewinddir ( lfc_DIR " dirp
.B  )

.BI "lfc_rmdir ( string " path
.B  )

.BI "lfc_setacl ( string " path ,
.BI "ListOf struct lfc_acl " acls
.B  )

.BI "lfc_setatime ( string " path ,
.BI "struct lfc_fileid " file_uniqueid
.B  )

.BI "lfc_setcomment ( string " path ,
.BI "string " comment
.B  )

.BI "lfc_setfsize ( string " path ,
.BI "struct lfc_fileid " file_uniqueid ,
.BI "long " filesize
.B  )

.BI "lfc_setfsizec ( string " path ,
.BI "struct lfc_fileid " file_uniqueid ,
.BI "long " filesize ,
.BI "string " csumtype ,
.BI "string " csumvalue
.B  )

.BI "lfc_setfsizeg ( string " guid ,
.BI "long " filesize ,
.BI "string " csumtype ,
.BI "string " csumvalue
.B  )

.BI "lfc_setptime ( string " sfn ,
.BI "long " ptime
.B  )

.BI "lfc_setratime ( string " sfn
.B  )

.BI "lfc_setrltime ( string " sfn ,
.BI "long " ltime
.B  )

.BI "lfc_setrstatus ( string " sfn ,
.BI "char " status
.B  )

.BI "lfc_setrtype ( string " sfn ,
.BI "char " type
.B  )

.BI "lfc_startsess ( string " server ,
.BI "string " comment
.BI )

.BI "lfc_starttrans ( string " server ,
.BI "string " comment
.BI )

.BI "lfc_stat ( string " path
.BI ") -> struct lfc_filestat " statbuf

.BI "lfc_statg ( string " path ,
.BI "string " guid
.BI ") -> struct lfc_filestatg " statbuf

.BI "lfc_statr ( string " sfn)
.BI ") -> struct lfc_filestatg " statbuf

.BI "lfc_symlink ( string " target ,
.BI "string " linkname
.B  )

.BI "lfc_umask ( mode_t " cmask
.BI ") -> mode_t " oldmask

.BI "lfc_undelete ( string " path
.B  )

.BI "lfc_unlink ( string " path
.B  )

.BI "lfc_utime ( string " path ,
.BI "struct utimbuf " times
.B  )

.BI "lfc_entergrpmap ( gid_t " gid ,
.BI "string " groupname
.B  )

.BI "lfc_enterusrmap ( uid_t " uid ,
.BI "string " username
.B  )

.BI "lfc_getgrpbygid ( gid_t " gid
.BI ") -> string " group

.BI "lfc_getgrpbygids ( ListOfgid_t " gids
.BI ") -> ListOfString " groups

.BI "lfc_getgrpbynam ( string " groupname
.BI ") -> gid_t " gid

.BI "lfc_getgrpmap ( ) -> ListOf struct lfc_groupinfo " infos

.BI "lfc_getidmap ( string " username ,
.BI "ListOfString " vo_roles 
.BI ") -> uid_t " uid ,
.BI " ListOf gid_t " gids

.BI "lfc_getusrbynam ( string " username
.BI ") -> uid_t " uid

.BI "lfc_getusrbyuid ( uid_t " uid
.BI ") -> string " user

.BI "lfc_getusrmap ( ) -> ListOf struct lfc_userinfo " userinfo

.BI "lfc_modifygrpmap ( gid_t " gid ,
.BI "string " newname
.B  )

.BI "lfc_modifyusrmap ( uid_t " uid ,
.BI "string " newname
.B  )

.BI "lfc_rmgrpmap ( gid_t " gid ,
.BI "string " groupname
.B  )

.BI "lfc_rmusrmap ( uid_t " uid ,
.BI "string " username
.B  )

.SH STRUCTURES
Sometimes you need to specify a structure as an input argument (eg.
.IR "struct lfc_acl" ,
.IR "struct lfc_list" ", etc.)."
For that purpose, the module includes structure constructors, named exactly as the structure.

Thus, to create
.IR "struct lfc_acl " or
.IR "struct lfc_list" ,
you have to do the following:

.nf
    lfcacl = lfc.lfc_acl()
    lfclist = lfc.lfc_list()
.fi

If you want to see how to use it in a real case, please have a look at examples.

.SH ERRORS
As you can see, no error code is returned. When an error occurs, an exception is raised
with a description of the error.

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# stat an existing entry in the LFC and print the GUID
"""

name = "/grid/dteam/my.test"

try:
   stat = lfc.lfc_statg(name,"")
except Exception:
   traceback.print_exc()
   sys.exit(1)

guid = stat.guid
print "The GUID for " + name + " is " + guid

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# setting and retrieving a comment on a file
"""

file = "/grid/dteam/my.test"
comment = "MyComment"

try:
   lfc.lfc_setcomment(file,comment)

   comment = lfc.lfc_getcomment(file)
   print "Read back comment " + comment
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

"""
# Using the lfc_readdirxr method
"""

import sys
import traceback
import lfc2 as lfc

name = "/grid/dteam/my.test"

try:
   dir = lfc.lfc_opendir(name)

   while 1:
      entry = lfc.lfc_readdirxr(dir)
      if entry == None:
         break
      print entry.d_name
      for i in range(entry.nbreplicas):
         print " ==> %s" % entry.rep[i].sfn

   lfc.lfc_closedir(dir)
except TypeError, x:
   print " ==> None"
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# Using the lfc_getreplica method
"""

file = "/grid/dteam/my.test"

try:
   list = lfc.lfc_getreplica(file, "", "")
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in list:
   print i.host
   print i.sfn

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# Using the lfc_getacl and lfc_setacl methods to add a user ACL
"""

file = "/grid/dteam/my.test"

try:
   acls_list = lfc.lfc_getacl(file)
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

# When adding a first ACL for a given user, you also need to add the mask
# When adding the second user ACL, it is not necessary anymore

acl_user = lfc.lfc_acl()
acl_mask = lfc.lfc_acl()

acl_user.a_type = lfc.CNS_ACL_USER
acl_user.a_id = 18701		# user id
acl_user.a_perm = lfc.S_IROTH | lfc.S_IXOTH

acl_mask.a_type = lfc.CNS_ACL_MASK
acl_mask.a_id = 0			# no user id specified
acl_mask.a_perm = lfc.S_IROTH | lfc.S_IXOTH

acls_list.append(acl_user)
acls_list.append(acl_mask)

try:
   lfc.lfc_setacl(file, acls_list)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# Using the lfc_getacl and lfc_setacl methods to remove a user ACL
"""

file = "/grid/dteam/my.test"

try:
   acls_list = lfc.lfc_getacl(file)
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

del acls_list[1]	# delete a given user ACL from the list of ACLs

try:
   lfc.lfc_setacl(file, acls_list)
except Exception:
   traceback.print_exc()
   sys.exit(1)

.SH EXAMPLE
.nf
#!/usr/bin/env python
import lfc2thr as lfc
import os
from threading import Thread

class slave(Thread):

   def __init__ (self):
      Thread.__init__(self)
      
   def run(self):
        ....
        result = lfc.lfc_getreplica("", guid, "")
        ....
        return
        
if __name__ == '__main__':

    os.environ['LFC_HOST'] = 'my_lfc.cern.ch'

    #	Threaded library initialisation
    lfc.init()

    ....
    #	Start up of threads
    for i in xrange(totalNumberOfSlaves):
        slv = slave(i)
        slv.start()
        
    ....

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# Using the lfc_getusrmap method
"""

try:
   list = lfc.lfc_getusrmap()
except Exception:
   traceback.print_exc()
   sys.exit(1)

for i in list:
   print i.userid + " " + i.username

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# Using the lfc_getgrpmap method
"""

try:
   list = lfc.lfc_getgrpmap()
except Exception:
   traceback.print_exc()
   sys.exit(1)
    
for i in list:
   print i.gid + " " + i.groupname

.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import traceback
import lfc2 as lfc

"""
# Using the lfc_getidmap method
"""

userdn = "user certificate DN"
vo_roles = ["dteam"]

try:
   uid, gids = lfc.lfc_getidmap (userdn, vo_roles)
except Exception:
   traceback.print_exc()
   sys.exit(1)

print "uid=", uid
print "gids=", gids

.SH SEE ALSO
.B LFC C interface man pages
