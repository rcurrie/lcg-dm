.\" @(#)$RCSfile: nsentergrpmap.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006-2009 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH NSENTERGRPMAP 1 "$Date$" CASTOR "Cns Administrator Commands"
.SH NAME
nsentergrpmap \- define a new group entry in Virtual Id table
.SH SYNOPSIS
.B nsentergrpmap
[
.BI --gid " gid"
]
.BI --group " groupname"
.SH DESCRIPTION
.B nsentergrpmap
defines a new group entry in Virtual Id table.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.BI --gid " gid"
specifies the Virtual Group Id.
If this parameter is absent, the next available id is allocated.
.TP
.BI --group " groupname"
specifies the new group name.
It must be at most 255 characters long.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.B Cns_entergrpmap(3)
