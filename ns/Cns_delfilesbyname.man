.\" @(#)$RCSfile: Cns_delfilesbyname.man,v $ $Revision: 1.1 $ $Date: 2007/12/13 11:59:47 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_DELFILESBYNAME 3 "$Date: 2007/12/13 11:59:47 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_delfilesbyname \- delete the file entries corresponding to a list of path names
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_delfilesbyname (int " nbfiles ,
.BI "const char **" paths ,
.BI "int " force ,
.BI "int *" nbstatuses ,
.BI "int **" statuses )
.SH DESCRIPTION
.B Cns_delfilesbyname
deletes the file entries corresponding to a list of path names. If replicas exist
and the force argument is not zero, all replicas are first removed.
.TP
.I nbfiles
specifies the number of path names in the array
.IR paths .
.TP
.I paths
specifies the list of logical path names relative to the current CASTOR directory or
the list of full CASTOR path names.
.TP
.I nbstatuses
will be set to the number of replies in the array of statuses.
.TP
.I statuses
will be set to the address of an array of integer statuses allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file does not exist.
.TP
.B EACCES
Search permission is denied on a component of the
.IR path
prefix or write permission is denied on the parent directory or
the parent has the sticky bit S_ISVTX set and
.RS 1.5i
.LP
the effective user ID of the requestor does not match the owner ID of the file and
.LP
the effective user ID of the requestor does not match the owner ID of the
directory and
.LP
the file is not writable by the requestor and
.LP
the requestor does not have ADMIN privilege in the Cupv database.
.RE
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request or unmarshalling
the reply.
.TP
.B EFAULT
.IR paths ,
.I nbstatuses
or
.I statuses
is a NULL pointer.
.TP
.B EEXIST
File has replicas and
.I force
is zero.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B EINVAL
.I nbfiles
is not strictly positive.
.TP
.B SENOSSERV
Service unknown.
.TP 
.B SEINTERNAL 
Database error.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_delreplica(3) ,
.BR Cns_unlink(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
