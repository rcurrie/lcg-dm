/*
 * Copyright (C) 2005-2010 by CERN/IT/GD/SC
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: nsmodifygrpmap.c,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

/*      nsmodifygrpmap - modify group entry corresponding to a given gid */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include "Cgetopt.h"
#include "Cns_api.h"
#include "serrno.h"
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	int errflg = 0;
	gid_t gid = 0;
	char *groupname = NULL;
	static struct Coptions longopts[] = {
		{"gid", REQUIRED_ARGUMENT, 0, OPT_IDMAP_GID},
		{"group", REQUIRED_ARGUMENT, 0, OPT_IDMAP_GROUP},
		{"status", REQUIRED_ARGUMENT, 0, OPT_IDMAP_STATUS},
		{0, 0, 0, 0}
	};
	char *p;
	int status = -1;
	char statusa[50];

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_IDMAP_GID:
			if ((gid = strtol (Coptarg, &dp, 10)) < 0 || *dp != '\0') {
				fprintf (stderr, "invalid gid: %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_IDMAP_GROUP:
			if (strlen (Coptarg) > 255) {
				fprintf (stderr,
				    "group name too long: %s\n", Coptarg);
				errflg++;
			} else
				groupname = Coptarg;
			break;
		case OPT_IDMAP_STATUS:
			if (isdigit (*Coptarg)) {
				if ((status = strtol (Coptarg, &dp, 10)) < 0 ||
				    *dp != '\0') {
					fprintf (stderr,
					    "invalid status %s\n", Coptarg);
					errflg++;
				}
			} else {
				if (strlen (Coptarg) >= sizeof (statusa)) {
					fprintf (stderr,
					    "invalid status %s\n", Coptarg);
					errflg++;
					break;
				}
				status = 0;
				strcpy (statusa, Coptarg);
				p = strtok (statusa, "|");
				while (p) {
					if (strcmp (p, "ARGUS_BAN") == 0)
						status |= ARGUS_BAN;
					else if (strcmp (p, "LOCAL_BAN") == 0)
						status |= LOCAL_BAN;
					else {
						fprintf (stderr,
						    "invalid status %s\n", Coptarg);
						errflg++;
						break;
					}
					p = strtok (NULL, "|");
				}
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || gid <= 0 || groupname == NULL)
		errflg++;
	if (errflg) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "--gid gid --group groupname [--status status]\n");
		exit (USERR);
	}

	if (Cns_modifygrpmap (gid, groupname, status) < 0) {
		if (serrno == EEXIST)
			fprintf (stderr, "nsmodifygrpmap %s: %s\n", groupname,
			    "Group exists already");
		else
			fprintf (stderr, "nsmodifygrpmap %d: %s\n", gid,
			    (serrno == ENOENT) ? "No such group" : sstrerror(serrno));
		exit (USERR);
	}
	exit (0);
}
