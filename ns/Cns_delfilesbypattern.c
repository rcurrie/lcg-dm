/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_delfilesbypattern.c,v $ $Revision: 1.1 $ $Date: 2007/12/13 11:59:47 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_delfilesbypattern - delete file entries selected by restricted
	pattern matching on basename; delete associated replica entries if force
	is set to 1 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_delfilesbypattern(const char *path, const char *pattern, int force, int *nbstatuses, struct Cns_filestatus **statuses)
{
	int c;
	char func[22];
	gid_t gid;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[4];
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct Cns_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "Cns_delfilesbypattern");
	if (Cns_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! path || !pattern || ! nbstatuses || ! statuses) {
		serrno = EFAULT;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC);
	marshall_LONG (sbp, CNS_DELFILESBYP);
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */
 
	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_HYPER (sbp, thip->cwd);
	marshall_STRING (sbp, path);
	marshall_STRING (sbp, pattern);
	marshall_WORD (sbp, force);
 
	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsdx (NULL, NULL, sendbuf,
	    msglen, repbuf, sizeof(repbuf), (void **)statuses, nbstatuses);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, *nbstatuses);
		if (*nbstatuses == 0) {
			*statuses = NULL;
			return (0);
		}
	}
	return (c);
}
