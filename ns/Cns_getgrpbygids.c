/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_getgrpbygids.c,v $ $Revision: 1.1 $ $Date: 2007/03/21 10:24:15 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*      Cns_getgrpbygids - get group names associated with given virtual gids */

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h> 
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_getgrpbygids(int nbgroups, gid_t *gids, char **groupnames)
{
	int c;
	char func[17];
	int i;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[1];
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct Cns_api_thread_info *thip;

	strcpy (func, "Cns_getgrpbygids");
	if (Cns_apiinit (&thip))
		return (-1);

	if (! gids || ! groupnames) {
		serrno = EFAULT;
		return (-1);
	}
	if (nbgroups <= 0) {
		serrno = EINVAL;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC);
	marshall_LONG (sbp, CNS_GETGRPNAMES);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, nbgroups);
	for (i = 0; i < nbgroups; i++)
		marshall_LONG (sbp, gids[i]);

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsdx (NULL, NULL, sendbuf, msglen, repbuf, sizeof(repbuf),
	    (void **)groupnames, NULL);

	return (c);
}
