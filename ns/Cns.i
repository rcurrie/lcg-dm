/*
 * @(#)$RCSfile: Cns.i,v $ $Revision$ $Date$ CERN IT-DM/SMD Remi Mollon
 */

/*********************************************
    SWIG input file for Cns

    ** Need to be included
    ** not a module by itself
    ** 'lfc_api.h' or 'dpns_api.h' must be
    **    included before this file
*********************************************/

%include "typemaps.i"

#define SWIGNOPROTO

%{
#define SWIGNOPROTO
#include <dirent.h>
#include "Cns_api.h"
#include "serrno.h"

#define ERRORLEN_MAX 4096

/* Used to have a different treatment by SWIG */
typedef int RETURNCODE;
typedef int RETURNCODE_BOOL;
%}

%include "Cns-python_typemaps.i"

/* We don't want default constructor/destructor for 'struct Cns_rep_info' */
%nodefault Cns_rep_info;

%include "Cns_api.h"
%include "Cns_constants.h"
%include "Cns_struct.h"


			/* function prototypes */

extern RETURNCODE_BOOL  Cns_access (const char *path, int amode);
extern RETURNCODE_BOOL  Cns_accessr (const char *sfn, int amode);
extern RETURNCODE  Cns_aborttrans ();
extern RETURNCODE  Cns_addreplica (const char *guid, struct Cns_fileid *file_uniqueid, const char *server,
        const char *sfn, const char status, const char f_type, const char *poolname, const char * fs);
extern RETURNCODE  Cns_addreplicax (const char *guid, struct Cns_fileid *file_uniqueid, const char *server,
        const char *sfn, const char status, const char f_type, const char *poolname, const char *fs,
        const char r_type, const char *setname);
extern RETURNCODE  Cns_chdir (const char *path);
extern RETURNCODE  Cns_chmod (const char *path, mode_t mode);
extern RETURNCODE  Cns_chown (const char *path, uid_t new_uid, gid_t new_gid);
extern RETURNCODE  Cns_client_setAuthorizationId (uid_t uid, gid_t gid, const char *mech, char *id);
extern RETURNCODE  Cns_client_setVOMS_data (char *voname, char **INPUT, int LENGTH);
extern RETURNCODE  Cns_closedir (Cns_DIR *dirp);
#if defined(NSTYPE_LFC)
extern RETURNCODE  Cns_creatg (const char *path, const char *guid, mode_t mode);
#else
extern RETURNCODE  Cns_creat (const char *path, mode_t mode);
#endif
extern RETURNCODE  Cns_delcomment (const char *path);
extern RETURNCODE  Cns_delete (const char *path);

#if defined(NSTYPE_LFC)
extern RETURNCODE  Cns_delfilesbyguid (int LENGTH, const char **INPUT, int force, int *LENGTH, int **OUTPUT);
extern RETURNCODE  Cns_delfilesbyname (int LENGTH, const char **INPUT, int force, int *LENGTH, int **OUTPUT);
extern RETURNCODE  Cns_delfilesbypattern (const char *path, const char *pattern, int force, int *LENGTH,
        struct Cns_filestatus **OUTPUT);
extern RETURNCODE  Cns_delreplicas (int LENGTH, const char **INPUT, char *se, int *LENGTH, int **OUTPUT);
#endif

extern RETURNCODE  Cns_delreplicasbysfn (int LENGTH, const char **INPUT, const char **INPUTLIST2, int *LENGTH, int **OUTPUT);
extern RETURNCODE  Cns_delreplica (const char *guid, struct Cns_fileid *file_uniqueid, const char *sfn);
extern RETURNCODE  Cns_endsess ();
extern RETURNCODE  Cns_endtrans ();
extern RETURNCODE  Cns_getacl (const char *path, int LENGTH, struct Cns_acl *OUTPUT);
extern RETURNCODE  Cns_getcomment (const char *path, char *COMMENTOUT);
extern char  *Cns_getcwd (char *PATHOUT, int LENGTH);
extern RETURNCODE  Cns_getifcevers (char *STRING256OUT);
extern RETURNCODE  Cns_getlinks (const char *path, const char *guid, int *LENGTH,
        struct Cns_linkinfo **OUTPUT);
extern RETURNCODE  Cns_getreplica (const char *path, const char *guid, const char *se,
                   int *LENGTH, struct Cns_filereplica **OUTPUT);

#if defined(NSTYPE_LFC)
extern RETURNCODE Cns_getreplicas (int LENGTH, const char **INPUT, const char *se,
                   int *LENGTH, struct Cns_filereplicas **OUTPUT);
extern RETURNCODE Cns_getreplicasl (int LENGTH, const char **INPUT, const char *se,
                   int *LENGTH, struct Cns_filereplicas **OUTPUT);
extern RETURNCODE Cns_getreplicass (int LENGTH, const char **INPUT,
                   int *LENGTH, struct Cns_filereplicas **OUTPUT);
#endif

extern RETURNCODE Cns_getreplicax (const char *path, const char *guid, const char *se,
                   int *LENGTH, struct Cns_filereplicax **OUTPUT);
extern RETURNCODE  Cns_lchown (const char *path, uid_t new_uid, gid_t new_gid);
/*
extern struct Cns_linkinfo  *Cns_listlinks (const char *path, const char *guid,
        int flags, Cns_list *listp);
extern struct Cns_filereplica  *Cns_listreplica (const char *path, const char *guid,
        int flags, Cns_list *listp);
*/
extern struct Cns_filereplica  *Cns_listreplicax (const char *poolname, const char *server, const char *fs, int flags, Cns_list *listp);
extern struct Cns_filereplicax  *Cns_listrepset (const char *setname, int flags, Cns_list *listp);
extern RETURNCODE  Cns_lstat (const char *path, struct Cns_filestat *STATOUT);
extern RETURNCODE  Cns_mkdir (const char *path, mode_t mode);
extern RETURNCODE  Cns_mkdirg (const char *path, const char *guid, mode_t mode);
extern RETURNCODE  Cns_modreplica (const char *sfn, const char *setname, const char *poolname, const char *server);
extern RETURNCODE  Cns_modreplicax (const char *sfn, const char *setname, const char *poolname, const char *server, const char *fs, const char r_type);
extern Cns_DIR  *Cns_opendir (const char *path);
extern Cns_DIR  *Cns_opendirg (const char *path, const char *guid);
extern Cns_DIR  *Cns_opendirxg (char *server, const char *path, const char *guid);
extern RETURNCODE  Cns_ping (char *server, char *STRING256OUT);
extern struct dirent  *Cns_readdir (Cns_DIR *dirp);
extern struct Cns_direncomm  *Cns_readdirc (Cns_DIR *dirp);
extern struct Cns_direnstatg  *Cns_readdirg (Cns_DIR *dirp);
extern struct Cns_direnstat  *Cns_readdirx (Cns_DIR *dirp);
extern struct Cns_direnstatc  *Cns_readdirxc (Cns_DIR *dirp);
extern struct Cns_direnrep  *Cns_readdirxp (Cns_DIR *dirp, char *pattern, char *se = NULL);
extern struct Cns_direnrep  *Cns_readdirxr (Cns_DIR *dirp, char *se = NULL);
extern RETURNCODE  Cns_readlink (const char *path, char *PATHOUT, size_t LENGTH);
extern RETURNCODE  Cns_rename (const char *oldpath, const char *newpath);
extern void  Cns_rewinddir (Cns_DIR *dirp);
extern RETURNCODE  Cns_registerfiles (int LENGTH, struct Cns_filereg *INPUT, int *LENGTH, int **OUTPUT);
extern RETURNCODE  Cns_rmdir (const char *path);
extern RETURNCODE  Cns_setacl (const char *path, int LENGTH, struct Cns_acl *INPUT);
extern RETURNCODE  Cns_setatime (const char *path, struct Cns_fileid *file_uniqueid);
extern RETURNCODE  Cns_setcomment (const char *path, char *comment);
extern RETURNCODE  Cns_setfsize (const char *path, struct Cns_fileid *file_uniqueid, u_signed64 filesize);
extern RETURNCODE  Cns_setfsizec (const char *path, struct Cns_fileid *file_uniqueid, u_signed64 filesize, const char *csumtype, char *csumvalue);
extern RETURNCODE  Cns_setfsizeg (const char *guid, u_signed64 filesize, const char *csumtype, char *csumvalue);
extern RETURNCODE  Cns_setptime (const char *sfn, time_t ptime);
extern RETURNCODE  Cns_setratime (const char *sfn);
extern RETURNCODE  Cns_setrltime (const char *sfn, time_t ltime);
extern RETURNCODE  Cns_setrstatus (const char *sfn, const char status);
extern RETURNCODE  Cns_setrtype (const char *sfn, const char type);
extern RETURNCODE  Cns_startsess (char *server, char *comment);
extern RETURNCODE  Cns_starttrans (char *server, char *comment);
extern RETURNCODE  Cns_stat (const char *path, struct Cns_filestat *STATOUT);
extern RETURNCODE  Cns_statg (const char *path, const char *guid, struct Cns_filestatg *STATOUT);
extern RETURNCODE  Cns_statr (const char *sfn, struct Cns_filestatg *STATOUT);
extern RETURNCODE  Cns_symlink (const char *target, const char *linkname);
extern mode_t  Cns_umask (mode_t cmask);
extern RETURNCODE  Cns_undelete (const char *path);
extern RETURNCODE  Cns_unlink (const char *path);
extern RETURNCODE  Cns_utime (const char *path, struct utimbuf *times);

			/* function protypes for ID tables */

extern RETURNCODE  Cns_entergrpmap (gid_t gid, char *groupname);
extern RETURNCODE  Cns_enterusrmap (uid_t uid, char *username);
extern RETURNCODE  Cns_getgrpbygid (gid_t gid, char *STRING256OUT);
extern RETURNCODE  Cns_getgrpbygids (int LENGTH, gid_t *INPUT, char **OUTPUTS1);
extern RETURNCODE  Cns_getgrpbynam (char *groupname, gid_t *OUTPUT);
extern RETURNCODE  Cns_getgrpmap (int *LENGTH, struct Cns_groupinfo **OUTPUT);
extern RETURNCODE  Cns_getidmap (const char *username, int LENGTH, const char **INPUT, uid_t *OUTPUT, gid_t *IDMAPOUTPUT);
extern RETURNCODE  Cns_getidmapc (const char *username, const char *user_ca, int LENGTH, const char **INPUT, uid_t *OUTPUT, gid_t *IDMAPOUTPUT1);
extern RETURNCODE  Cns_getusrbynam (char *username, uid_t *OUTPUT);
extern RETURNCODE  Cns_getusrbyuid (uid_t uid, char *STRING256OUT);
extern RETURNCODE  Cns_getusrmap (int *LENGTH, struct Cns_userinfo **OUTPUT);
extern RETURNCODE  Cns_modifygrpmap (gid_t gid, char *newname, int status);
extern RETURNCODE  Cns_modifyusrmap (uid_t uid, char *newname, int status);
extern RETURNCODE  Cns_rmgrpmap (gid_t gid, char *groupname);
extern RETURNCODE  Cns_rmusrmap (uid_t uid, char *username);

			/* function protypes for thread-safe environment variables */
extern RETURNCODE  Cns_setenv (const char *key, const char *value, int replace);
extern const char* Cns_getenv (const char *key);


/****************************
      Rest of declarations
****************************/

#define R_OK 4
#define W_OK 2
#define X_OK 1
#define F_OK 0
#define S_IROTH 4
#define S_IWOTH 2
#define S_IXOTH 1

typedef unsigned int gid_t;
typedef unsigned int mode_t;
typedef unsigned int size_t;
typedef long int time_t;
typedef unsigned int uid_t;
typedef unsigned long long u_signed64;
typedef int RETURNCODE;
typedef int RETURNCODE_BOOL;

struct dirent {
        long            d_ino;
        long            d_off;
        unsigned short  d_reclen;
        char            d_name[256];
};

struct utimbuf {
        time_t actime;
        time_t modtime;
};
