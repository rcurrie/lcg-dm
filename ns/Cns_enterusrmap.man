.\" @(#)$RCSfile: Cns_enterusrmap.man,v $ $Revision: 1.2 $ $Date: 2006/01/13 16:22:46 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_ENTERUSRMAP 3 "$Date: 2006/01/13 16:22:46 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_enterusrmap \- define a new user entry in Virtual Id table
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_enterusrmap (uid_t " uid ,
.BI "char *" username )
.SH DESCRIPTION
.B Cns_enterusrmap
defines a new user entry in Virtual Id table.
.TP
.I uid
specifies the Virtual User Id.
If the value is -1, the next available id is allocated.
.TP
.I username
specifies the user name.
.LP
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B EFAULT
.I username
is a NULL pointer.
.TP
.B EEXIST
User exists already.
.TP
.B EINVAL
The length of
.I username
exceeds 255.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
