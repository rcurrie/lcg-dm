.\" @(#)$RCSfile: Cns_getreplicass.man $ $Revision$ $Date$ CERN IT-GT/DMS Jean-Philippe Baud
.\" Copyright (C) 2010 by CERN/IT/GT/DMS
.\" All rights reserved
.\"
.TH CNS_GETREPLICASS 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_getreplicass \- get the replica entries associated with a list of sfns
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getreplicass (int " nbsfns ,
.BI "const char **" sfns ,
.BI "int *" nbentries ,
.BI "struct Cns_filereplicas **" rep_entries )
.SH DESCRIPTION
.B Cns_getreplicass
gets the replica entries associated with a list of sfns.
.TP
.I nbsfns
specifies the number of path names in the array
.IR sfns .
.TP
.I sfns
specifies the list of Site File Names.
.TP
.I nbentries
will be set to the number of entries in the array of replicas.
.TP
.I rep_entries
will be set to the address of an array of Cns_filereplicas structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct Cns_filereplicas {
	char		guid[CA_MAXGUIDLEN+1];
	int		errcode;
	u_signed64	filesize;
	time_t		ctime;		/* GUID creation time */
	char		csumtype[3];
	char		csumvalue[33];
	time_t		r_ctime;	/* replica creation time */
	time_t		r_atime;	/* last access to replica */
	char		status;
	char		host[CA_MAXHOSTNAMELEN+1];
	char		sfn[CA_MAXSFNLEN+1];
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file does not exist.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request or unmarshalling
the reply.
.TP
.B EFAULT
.IR sfns ,
.I nbentries
or
.I rep_entries
is a NULL pointer.
.TP
.B EINVAL
.I nbsfns
is not strictly positive.
.TP
.B ENAMETOOLONG
The length of
.I sfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP 
.B SEINTERNAL 
Database error.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chdir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
