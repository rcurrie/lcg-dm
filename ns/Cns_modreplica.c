/*
 * Copyright (C) 2006 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_modreplica.c,v $ $Revision: 1.3 $ $Date: 2008/08/27 12:43:28 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_modreplica - modify information about a given replica */

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_modreplica(const char *sfn, const char *setname, const char *poolname, const char *server)
{
	return (Cns_modreplicax (sfn, setname, poolname, server, NULL, '\0'));
}

int DLL_DECL
Cns_modreplicax(const char *sfn, const char *setname, const char *poolname, const char *server, const char *fs, const char r_type)
{
	int c;
	int extended = 0;
	char func[16];
	gid_t gid;
	int msglen;
	char *q;
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct Cns_api_thread_info *thip;
	uid_t uid;
	u_signed64 zero = 0;
 
	strcpy (func, "Cns_modreplica");
	if (Cns_apiinit (&thip))
		return (-1);
	uid = getuid();
	gid = getgid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! sfn) {
		serrno = EFAULT;
		return (-1);
	}

	if (strlen (sfn) > CA_MAXSFNLEN) {
		serrno = ENAMETOOLONG;
		return (-1);
	}
	if ((setname && strlen (setname) > 36) ||
	    (poolname && strlen (poolname) > CA_MAXPOOLNAMELEN) ||
	    (server && strlen (server) > CA_MAXHOSTNAMELEN)) {
		serrno = EINVAL;
		return (-1);
	}

	/* Build request header */

	if (r_type || (fs && strlen (fs) > 0))
		extended = 1;

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC);
	marshall_LONG (sbp, (extended ? CNS_MODREPLICAX : CNS_MODREPLICA));
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);
 
	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_STRING (sbp, sfn);
	if (setname) {
		marshall_STRING (sbp, setname);
	} else {
		marshall_STRING (sbp, "");
	}
	if (poolname) {
		marshall_STRING (sbp, poolname);
	} else {
		marshall_STRING (sbp, "");
	}
	if (server) {
		marshall_STRING (sbp, server);
	} else {
		marshall_STRING (sbp, "");
	}
	if (extended) {
		if (fs) {
			marshall_STRING (sbp, fs);
		} else {
			marshall_STRING (sbp, "");
		}
		marshall_BYTE (sbp, r_type);
	}

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsd (NULL, NULL, sendbuf, msglen, NULL, 0);

	if (c && serrno == SENAMETOOLONG) serrno = ENAMETOOLONG;
	return (c);
}
