#
#  Copyright (C) 1999-2004 by CERN/IT/PDP/DM
#  All rights reserved
#
#       @(#)$RCSfile: oralink.mk,v $ $Revision: 1.2 $ $Date: 2005/03/08 11:31:38 $ CERN IT-PDP/DM Jean-Philippe Baud
 
#    Link nsdaemon with Oracle libraries.


ifdef ORACLE_SHARE
include $(ORACLE_SHARE)/demo_proc_ic.mk
else
include $(ORACLE_HOME)/precomp/lib/env_precomp.mk
endif

ifdef ORACLE_LIB
LIBHOME=$(ORACLE_LIB)
endif

PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL) $(STATICTTLIBS)
PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL)

dpnsdaemon: $(NSDAEMON_OBJS)
	$(CC) -o dpnsdaemon $(CLDFLAGS) $(NSDAEMON_OBJS) $(LIBS) -L$(LIBHOME) $(PROLDLIBS)
lfcdaemon: $(NSDAEMON_OBJS)
	$(CC) -o lfcdaemon $(CLDFLAGS) $(NSDAEMON_OBJS) $(LIBS) -L$(LIBHOME) $(PROLDLIBS)
nsdaemon: $(NSDAEMON_OBJS)
	$(CC) -o nsdaemon $(CLDFLAGS) $(NSDAEMON_OBJS) $(LIBS) -L$(LIBHOME) $(PROLDLIBS)
