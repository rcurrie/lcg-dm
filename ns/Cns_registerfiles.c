/*
 * Copyright (C) 2009-2010 by CERN/IT/GS
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_registerfiles.c,v $ $Revision$ $Date$ CERN IT-GS Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_registerfiles - register list of files with their corresponding replica entry */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_registerfiles(int nbfiles, struct Cns_filereg *files, int *nbstatuses, int **statuses)
{
	int c;
	char func[18];
	gid_t gid;
	int i;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[4];
	char *sbp;
	char *sendbuf;
	struct Cns_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "Cns_registerfiles");
	if (Cns_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbfiles <= 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (! files || ! nbstatuses || ! statuses) {
		serrno = EFAULT;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 5 * LONGSIZE;
	msglen += WORDSIZE;
	msglen += HYPERSIZE;
	msglen += LONGSIZE;
	for (i = 0; i < nbfiles; i++) {
		msglen += strlen ((files+i)->lfn) + 1;
		if ((files+i)->guid) {
			msglen += strlen ((files+i)->guid) + 1;
		} else {
			msglen++;
		}
		msglen += LONGSIZE;
		msglen += HYPERSIZE;
		if ((files+i)->csumtype) {
			msglen += strlen ((files+i)->csumtype) + 1;
		} else {
			msglen++;
		}
		if ((files+i)->csumvalue) {
			msglen += strlen ((files+i)->csumvalue) + 1;
		} else {
			msglen++;
		}
		msglen += strlen ((files+i)->server) + 1;
		msglen += strlen ((files+i)->sfn) + 1;
	}

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC);
	marshall_LONG (sbp, CNS_REGFILES);
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */
 
	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_WORD (sbp, thip->mask);
	marshall_HYPER (sbp, thip->cwd);
	marshall_LONG (sbp, nbfiles);
	for (i = 0; i < nbfiles; i++) {
		marshall_STRING (sbp, (files+i)->lfn);
		if ((files+i)->guid) {
			marshall_STRING (sbp, (files+i)->guid);
		} else {
			marshall_STRING (sbp, "");
		}
		marshall_LONG (sbp, (files+i)->mode);
		marshall_HYPER (sbp, (files+i)->size);
		if ((files+i)->guid) {
			marshall_STRING (sbp, (files+i)->csumtype);
		} else {
			marshall_STRING (sbp, "");
		}
		if ((files+i)->guid) {
			marshall_STRING (sbp, (files+i)->csumvalue);
		} else {
			marshall_STRING (sbp, "");
		}
		marshall_STRING (sbp, (files+i)->server);
		marshall_STRING (sbp, (files+i)->sfn);
	}
 
	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsdx (NULL, NULL, sendbuf,
	    msglen, repbuf, sizeof(repbuf), (void **)statuses, nbstatuses);
	free (sendbuf);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, *nbstatuses);
		if (*nbstatuses == 0) {
			*statuses = NULL;
			return (0);
		}
	}
	return (c);
}
