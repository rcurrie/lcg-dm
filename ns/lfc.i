/*********************************************
    SWIG input file for LFC
 (including typemaps for non-trivial functions
*********************************************/

%module lfc
%include "typemaps.i"
%{
#include <dirent.h>
#include "lfc_api.h"
#include "serrno.h"
%}

%init %{
Cthread_init();
%}

/* Typemaps are only valid for python */
#ifdef SWIGPYTHON



/***************************************
    Global typemaps definition
***************************************/


%typemap(in) u_signed64 {
  if (PyInt_Check ($input))
      $1 = PyInt_AsUnsignedLongLongMask ($input);
  else if (PyLong_Check ($input))
      $1 = PyLong_AsUnsignedLongLong ($input);
  else {
      PyErr_SetString (PyExc_TypeError, "int or long expected");
      return NULL;
  }
}


%typemap(out) char[ANY] {
      $result = PyString_FromString ($1);
}


/***************************************
    Typemaps definition for getlinks
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct lfc_linkinfo **OUTPUT){
   int tmp_int;
   struct lfc_linkinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct lfc_linkinfo **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct lfc_linkinfo * aux = (struct lfc_linkinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct lfc_linkinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of links */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct lfc_linkinfo **OUTPUT) {(int *nbentries, struct lfc_linkinfo **linkinfos)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getlinks (const char *path, const char *guid, int *nbentries, struct lfc_linkinfo **linkinfos);

%clear (int *nbentries, struct lfc_linkinfo **linkinfos);


/***************************************
    Typemaps definition for getreplica
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct lfc_filereplica **OUTPUT){
   int tmp_int;
   struct lfc_filereplica *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct lfc_filereplica **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;	

      struct lfc_filereplica * aux = (struct lfc_filereplica *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct lfc_filereplica *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of replicas */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct lfc_filereplica **OUTPUT) {(int *return_value, struct lfc_filereplica **rep_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getreplica(const char *path, const char *guid, const char *se,
                   int *return_value, struct lfc_filereplica **rep_entries);

%clear (int *return_value, struct lfc_filereplica **rep_entries);




/***************************************
    Typemaps definition for getreplicas
***************************************/


%typemap(in) (int LENGTH, const char **INPUT) {
  int i;
  if (!PyList_Check($input)) {
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size($input);
  $2 = (char **) malloc(($1+1)*sizeof(char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem($input,i);
    if (!PyString_Check(s)) {
        free($2);
        PyErr_SetString(PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString(s);
  }
  $2[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (int LENGTH, const char **INPUT) {
   if ($2) free($2);
}

%typemap(in, numinputs=0) (int *LENGTH, struct lfc_filereplicas **OUTPUT){
   int tmp_int;
   struct lfc_filereplicas *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct lfc_filereplicas **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;	

      struct lfc_filereplicas * aux = (struct lfc_filereplicas *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct lfc_filereplicas *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of replicas */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int LENGTH, const char **INPUT) {(int nbguids, const char **guids)};
%apply (int *LENGTH, struct lfc_filereplicas **OUTPUT) {(int *return_value, struct lfc_filereplicas **rep_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getreplicas(int nbguids, const char **guids, const char *se,
                   int *return_value, struct lfc_filereplicas **rep_entries);

%clear (int nbguids, const char **guids);
%clear (int *return_value, struct lfc_filereplicas **rep_entries);




/***************************************
    Typemaps definition for getreplicasl
***************************************/


/* C/C++ declarations and apply statement */
/* Reuse of the typemaps from the previous function getreplicas */

%apply (int LENGTH, const char **INPUT) {(int nbfiles, const char **paths)};
%apply (int *LENGTH, struct lfc_filereplicas **OUTPUT) {(int *return_value, struct lfc_filereplicas **rep_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getreplicasl(int nbfiles, const char **paths, const char *se,
                   int *return_value, struct lfc_filereplicas **rep_entries);

%clear (int nbfiles, const char **paths);
%clear (int *return_value, struct lfc_filereplicas **rep_entries);




/***************************************
    Typemaps definition for getreplicass
***************************************/


/* C/C++ declarations and apply statement */
/* Reuse of the typemaps from the previous function getreplicas */

%apply (int LENGTH, const char **INPUT) {(int nbsfns, const char **sfns)};
%apply (int *LENGTH, struct lfc_filereplicas **OUTPUT) {(int *return_value, struct lfc_filereplicas **rep_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getreplicass(int nbsfns, const char **sfns,
                   int *return_value, struct lfc_filereplicas **rep_entries);

%clear (int nbsfns, const char **sfns);
%clear (int *return_value, struct lfc_filereplicas **rep_entries);




/***************************************
    Typemaps definition for getreplicax
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct lfc_filereplicax **OUTPUT){
   int tmp_int;
   struct lfc_filereplicax *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct lfc_filereplicax **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;	

      struct lfc_filereplicax * aux = (struct lfc_filereplicax *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct lfc_filereplicax *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of replicas */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct lfc_filereplicax **OUTPUT) {(int *return_value, struct lfc_filereplicax **rep_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getreplicax(const char *path, const char *guid, const char *se,
                   int *return_value, struct lfc_filereplicax **rep_entries);

%clear (int *return_value, struct lfc_filereplicax **rep_entries);




/****************************
   Typemap for readdirxp
****************************/

%typemap(out) struct lfc_direnrep *OUTPUT{

  /* Don't let python garbage-collect these structures, because they are freed with lfc_closedir */
  int PYTHON_OWNED=0;

  /* Error, return None */
  if($1 == 0){
    Py_INCREF(Py_None);
    $result = Py_None;
  }
  /* No error */
  else{
    $result = PyTuple_New(2);
    /* Python object for the lfc_direnrep set by the C call */
    PyObject * res = SWIG_NewPointerObj($1, $descriptor(struct lfc_direnrep *), PYTHON_OWNED);
    /* First element of the returned tuple is this direnrep object */
    PyTuple_SetItem($result, 0, res);

    if($1->nbreplicas > 0){
       PyObject *tuple = PyTuple_New($1->nbreplicas);
       int i;
       for(i=0; i< $1->nbreplicas; i++){
          /* Python object for each of the lfc_rep_info structs set by the C call */
          PyObject * aux_rep = SWIG_NewPointerObj(i+($1->rep), $descriptor(struct lfc_rep_info *), PYTHON_OWNED);
          PyTuple_SetItem(tuple, i, aux_rep);
       }
       /* Second element of the returned tuple is a tuple of these lfc_rep_info objects */
       PyTuple_SetItem($result, 1, tuple);
    }
    /* If there are no replicas, set the second element to None */
    else{
       Py_INCREF(Py_None);
       PyTuple_SetItem($result, 1, Py_None);
    }
  }
}


/* C/C++ declarations and apply statement */

%apply (struct lfc_direnrep *OUTPUT) {(struct lfc_direnrep *)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
struct lfc_direnrep *lfc_readdirxp (lfc_DIR *dirp, char *pattern, char *se);

%clear (struct lfc_direnrep *);




/****************************
   Typemap for readdirxr
****************************/

%typemap(out) struct lfc_direnrep *OUTPUT{

  /* Don't let python garbage-collect these structures, because they are freed with lfc_closedir */
  int PYTHON_OWNED=0;

  /* Error, return None */
  if($1 == 0){
    Py_INCREF(Py_None);
    $result = Py_None;
  }
  /* No error */
  else{
    $result = PyTuple_New(2);
    /* Python object for the lfc_direnrep set by the C call */
    PyObject * res = SWIG_NewPointerObj($1, $descriptor(struct lfc_direnrep *), PYTHON_OWNED);
    /* First element of the returned tuple is this direnrep object */
    PyTuple_SetItem($result, 0, res);

    if($1->nbreplicas > 0){
       PyObject *tuple = PyTuple_New($1->nbreplicas);
       int i;
       for(i=0; i< $1->nbreplicas; i++){
          /* Python object for each of the lfc_rep_info structs set by the C call */
          PyObject * aux_rep = SWIG_NewPointerObj(i+($1->rep), $descriptor(struct lfc_rep_info *), PYTHON_OWNED);
          PyTuple_SetItem(tuple, i, aux_rep);
       }
       /* Second element of the returned tuple is a tuple of these lfc_rep_info objects */
       PyTuple_SetItem($result, 1, tuple);
    }
    /* If there are no replicas, set the second element to None */
    else{
       Py_INCREF(Py_None);
       PyTuple_SetItem($result, 1, Py_None);
    }
  }
}


/* C/C++ declarations and apply statement */

%apply (struct lfc_direnrep *OUTPUT) {(struct lfc_direnrep *)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
struct lfc_direnrep *lfc_readdirxr (lfc_DIR *dirp, char *se);

%clear (struct lfc_direnrep *);






/*************************************
    Typemaps definition for setacl
*************************************/

%typemap(in) (int LENGTH, struct lfc_acl *TUPLE){

  /* Check the input list */
  if(!PyList_Check($input)){
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else{
    /* Length of the python list (how many lfc_acl structs we will need) */
    $1 = PyList_Size($input);

    /* Reserve space to store all the list members in an array (after converting them) */
    $2 = (struct lfc_acl *) malloc($1 * sizeof(struct lfc_acl)) ;

    int i;
    for(i=0; i<$1; i++){
      PyObject * aux_object = PyList_GetItem($input, i);

      /* Temp pointer */
      struct lfc_acl * aux_p;

      /* This moves aux_p to point to a newly allocated lfc_acl struct representing the item in the python list */
      SWIG_ConvertPtr(aux_object, (void**) &aux_p,
                      $descriptor(struct lfc_acl *), SWIG_POINTER_EXCEPTION);

      /* Copy the value to the array */
      $2[i] = *(aux_p);
    }

  }/* end of: No error */

}/* end of: typemap(in) */



/* Free the temporary lfc_acl array passed to the C function after the lfc_setacl function has returned */
%typemap(freearg) (int LENGTH, struct lfc_acl *TUPLE) {
  if($2){
     free($2);
   }
}


/* C/C++ declarations and apply statement */

%apply (int LENGTH, struct lfc_acl *TUPLE) {(int nentries, struct lfc_acl *acl)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_setacl(const char *path, int nentries, struct lfc_acl *acl);

%clear (int nentries, struct lfc_acl *acl);




/***************************************
    Typemaps definition for getacl
***************************************/

%typemap(in) (int LENGTH, struct lfc_acl *OUTPUT){
   int len = PyInt_AsLong($input);
   $1 = len;
   $2 = (struct lfc_acl *) malloc(len * sizeof(struct lfc_acl));
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int LENGTH, struct lfc_acl *OUTPUT) {

  PyObject *tuple;
  int len = PyInt_AsLong($result);

  /* Error */
  if(len < 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
  }

  /* No error */
  else{

    /* No structs returned, only number of ACL entries */
    if($1 == 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
    }

    /* Structs returned in array */
    else{
       tuple = PyList_New(len);
       int i;

       for(i=0; i<len; i++){
         /* Let python garbage-collect only the 1st one. That will already delete it all!
            Because the original var was lfc_al* and not lfc_acl** (only 1 pointer responsible) */
         int PYTHON_OWNED = i>0 ? 0 : 1;

          /* Python object for each of the lfc_acl structs set by the C call */
         PyObject * aux_obj = SWIG_NewPointerObj(&$2[i], $descriptor(struct lfc_acl *), PYTHON_OWNED);

         PyList_SetItem(tuple, i, aux_obj);

       }/* end of for */

    }/* end of: Structs returned in array */

  }/* end of: No error */

/* Return the results (homogeneous for every case) */
  PyObject * old_result = $result;
  $result = PyList_New(2);
  /* First element of the returned tuple is the C call return value */
  PyList_SetItem($result, 0, old_result);
  /* Second element of the returned tuple is a tuple of the python lfc_acl objects */
  PyList_SetItem($result, 1, tuple);


}/* end of: typemap(argout) */



/* C/C++ declarations and apply statement */

%apply (int LENGTH, struct lfc_acl *OUTPUT) {(int nentries, struct lfc_acl *acl)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getacl(const char *path, int nentries, struct lfc_acl *acl);

%clear (int nentries, struct lfc_acl *acl);




/*****************************************
    Typemaps definition for delfilesbyguid
*****************************************/


%typemap(in) (int LENGTH, const char **INPUT) {
  int i;
  if (!PyList_Check($input)) {
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size($input);
  $2 = (char **) malloc(($1+1)*sizeof(char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem($input,i);
    if (!PyString_Check(s)) {
        free($2);
        PyErr_SetString(PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString(s);
  }
  $2[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (int LENGTH, const char **INPUT) {
   if ($2) free($2);
}

%typemap(in, numinputs=0) (int *LENGTH, int **OUTPUT){
   int tmp_int;
   int *tmp_tab;
   $1 = &tmp_int;
   $2 = &tmp_tab;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, int **OUTPUT) {

  PyObject *tuple;
  int len = PyInt_AsLong($result);

  /* Error */
  if(len < 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
  }

  /* No error */
  else{

      /* No statuses returned */
      if(*$1 < 1){
          tuple = PyList_New(1);
          Py_INCREF(Py_None);
          PyList_SetItem(tuple, 0, Py_None);
      }

      /* Statuses returned in array */
      else{
          int i;
          tuple = PyList_New (*$1);

          for (i=0; i<*$1; i++) {
              PyObject *aux_obj = PyInt_FromLong ((long) (*$2)[i]);
              PyList_SetItem (tuple, i, aux_obj);
          }/* end of for */

      }/* end of: Statuses returned in array */

  }/* end of: No error */

  /* Return the results (homogeneous for every case) */
  PyObject * old_result = $result;
  $result = PyList_New(2);
  /* First element of the returned tuple is the C call return value */
  PyList_SetItem($result, 0, old_result);
  /* Second element of the returned tuple is a tuple of the python lfc_acl objects */
  PyList_SetItem($result, 1, tuple);


}/* end of: typemap(argout) */

/* C/C++ declarations and apply statement */

%apply (int LENGTH, const char **INPUT) {(int nbguids, const char **guids)};
%apply (int *LENGTH, int **OUTPUT) {(int *nbstatuses, int **statuses)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_delfilesbyguid(int nbguids, const char **guids, int force, int *nbstatuses, int **statuses);

%clear (int nbguids, const char **guids);
%clear (int *nbstatuses, int **statuses);




/*****************************************
    Typemaps definition for delfilesbyname
*****************************************/


/* C/C++ declarations and apply statement */
/* Reuse of the typemaps from the previous function delfilesbyguid */

%apply (int LENGTH, const char **INPUT) {(int nbfiles, const char **paths)};
%apply (int *LENGTH, int **OUTPUT) {(int *nbstatuses, int **statuses)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_delfilesbyname(int nbfiles, const char **paths, int force, int *nbstatuses, int **statuses);

%clear (int nbfiles, const char **paths);
%clear (int *nbstatuses, int **statuses);



/********************************************
    Typemaps definition for delfilesbypattern
********************************************/



%typemap(in, numinputs=0) (int *LENGTH, struct lfc_filestatus **OUTPUT){
   int tmp_int;
   struct lfc_filestatus *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct lfc_filestatus **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct lfc_filestatus * aux = (struct lfc_filestatus *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct lfc_filestatus *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of file statuses */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct lfc_filestatus **OUTPUT) {(int *nbstatuses, struct lfc_filestatus **statuses)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_delfilesbypattern (const char *path, const char *pattern, int force, int *nbstatuses, struct lfc_filestatus **statuses);

%clear (int *nbstatuses, struct lfc_filestatus **statuses);




/**************************************
    Typemaps definition for delreplicas
**************************************/


%typemap(in) (int nbguids, const char **guids) {
  int i;
  if (!PyList_Check($input)) {
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size($input);
  $2 = (char **) malloc(($1+1)*sizeof(char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem($input,i);
    if (!PyString_Check(s)) {
        free($2);
        PyErr_SetString(PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString(s);
  }
  $2[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (int nbguids, const char **guids) {
   if ($2) free($2);
}

%typemap(in, numinputs=0) (int *LENGTH, int **OUTPUT){
   int tmp_int;
   int *tmp_tab;
   $1 = &tmp_int;
   $2 = &tmp_tab;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, int **OUTPUT) {

  PyObject *tuple;
  int len = PyInt_AsLong($result);

  /* Error */
  if(len < 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
  }

  /* No error */
  else{

      /* No statuses returned */
      if(*$1 < 1){
          tuple = PyList_New(1);
          Py_INCREF(Py_None);
          PyList_SetItem(tuple, 0, Py_None);
      }

      /* Statuses returned in array */
      else{
          int i;
          tuple = PyList_New (*$1);

          for (i=0; i<*$1; i++) {
              PyObject *aux_obj = PyInt_FromLong ((long) (*$2)[i]);
              PyList_SetItem (tuple, i, aux_obj);
          }/* end of for */

      }/* end of: Statuses returned in array */

  }/* end of: No error */

  /* Return the results (homogeneous for every case) */
  PyObject * old_result = $result;
  $result = PyList_New(2);
  /* First element of the returned tuple is the C call return value */
  PyList_SetItem($result, 0, old_result);
  /* Second element of the returned tuple is a tuple of the python lfc_acl objects */
  PyList_SetItem($result, 1, tuple);


}/* end of: typemap(argout) */


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, int **OUTPUT) {(int *nbstatuses, int **statuses)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_delreplicas(int nbguids, const char **guids, char *se, int *nbstatuses, int **statuses);

%clear (int *nbstatuses, int **statuses);



/***************************************
    Typemaps definition for getusrmap
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct lfc_userinfo **OUTPUT){
   int tmp_int;
   struct lfc_userinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct lfc_userinfo **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;	

      struct lfc_userinfo * aux = (struct lfc_userinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct lfc_userinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of user entries */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct lfc_userinfo **OUTPUT) {(int *nbentries, struct lfc_userinfo **usr_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getusrmap (int *nbentries, struct lfc_userinfo **usr_entries);

%clear (int *nbentries, struct lfc_userinfo **usr_entries);



/***************************************
    Typemaps definition for getgrpmap
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct lfc_groupinfo **OUTPUT){
   int tmp_int;
   struct lfc_groupinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct lfc_groupinfo **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;	

      struct lfc_groupinfo * aux = (struct lfc_groupinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct lfc_groupinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of group entries */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct lfc_groupinfo **OUTPUT) {(int *nbentries, struct lfc_groupinfo **grp_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_getgrpmap (int *nbentries, struct lfc_groupinfo **grp_entries);

%clear (int *nbentries, struct lfc_groupinfo **grp_entries);




/*****************************************
    Typemaps definition for delreplicasbysfn
*****************************************/


%typemap(in) (int LENGTH, const char **INPUT) {
  int i;
  if (!PyList_Check($input)) {
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = PyList_Size($input);
  $2 = (char **) malloc(($1+1)*sizeof(char *));
  for (i = 0; i < $1; i++) {
    PyObject *s = PyList_GetItem($input,i);
    if (!PyString_Check(s)) {
        free($2);
        PyErr_SetString(PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $2[i] = PyString_AsString(s);
  }
  $2[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (int LENGTH, const char **INPUT) {
   if ($2) free($2);
}

%typemap(in, numinputs=0) (int *LENGTH, int **OUTPUT){
   int tmp_int;
   int *tmp_tab;
   $1 = &tmp_int;
   $2 = &tmp_tab;
}


%typemap(in) (const char **INPUTLIST2) {
  int i;
  if (!PyList_Check($input)) {
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $1 = (char **) calloc (arg1+1, sizeof(char *));
  for (i = 0; i < arg1; i++) {
    PyObject *s = PyList_GetItem($input,i);
    if (!PyString_Check(s)) {
        free($1);
        PyErr_SetString(PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $1[i] = PyString_AsString(s);
  }
  $1[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (const char **INPUTLIST2) {
   if ($1) free($1);
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, int **OUTPUT) {

  PyObject *tuple;
  int len = PyInt_AsLong($result);

  /* Error */
  if(len < 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
  }

  /* No error */
  else{

      /* No statuses returned */
      if(*$1 < 1){
          tuple = PyList_New(1);
          Py_INCREF(Py_None);
          PyList_SetItem(tuple, 0, Py_None);
      }

      /* Statuses returned in array */
      else{
          int i;
          tuple = PyList_New (*$1);

          for (i=0; i<*$1; i++) {
              PyObject *aux_obj = PyInt_FromLong ((long) (*$2)[i]);
              PyList_SetItem (tuple, i, aux_obj);
          }/* end of for */

      }/* end of: Statuses returned in array */

  }/* end of: No error */

  /* Return the results (homogeneous for every case) */
  PyObject * old_result = $result;
  $result = PyList_New(2);
  /* First element of the returned tuple is the C call return value */
  PyList_SetItem($result, 0, old_result);
  /* Second element of the returned tuple is a tuple of the python lfc_acl objects */
  PyList_SetItem($result, 1, tuple);


}/* end of: typemap(argout) */

/* C/C++ declarations and apply statement */

%apply (int LENGTH, const char **INPUT) {(int nbfiles, const char **sfns)};
%apply (const char **INPUTLIST2) {(const char **guids)};
%apply (int *LENGTH, int **OUTPUT) {(int *nbstatuses, int **statuses)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_delreplicasbysfn (int nbfiles, const char **sfns, const char **guids, int *nbstatuses, int **statuses);

%clear (int nbfiles, const char **sfns);
%clear (const char **guids);
%clear (int *nbstatuses, int **statuses);






/*****************************************
    Typemaps definition for registerfiles
*****************************************/

%typemap(in) (int LENGTH, struct lfc_filereg *INPUT){

  /* Check the input list */
  if(!PyList_Check($input)){
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else{
    /* Length of the python list (how many lfc_filereg structs we will need) */
    $1 = PyList_Size($input);

    /* Reserve space to store all the list members in an array (after converting them) */
    $2 = (struct lfc_filereg *) calloc($1, sizeof(struct lfc_filereg)) ;

    int i;
    for(i=0; i<$1; i++){
      PyObject * aux_object = PyList_GetItem($input, i);

      /* Temp pointer */
      struct lfc_filereg * aux_p;

      /* This moves aux_p to point to a newly allocated lfc_filereg struct representing the item in the python list */
      SWIG_ConvertPtr(aux_object, (void**) &aux_p,
                      $descriptor(struct lfc_filereg *), SWIG_POINTER_EXCEPTION);

      /* Copy the value to the array */
      $2[i] = *(aux_p);
    }

  }/* end of: No error */

}/* end of: typemap(in) */

/* Free the temporary lfc_filereg array passed to the C function after the lfc_setfilereg function has returned */
%typemap(freearg) (int LENGTH, struct lfc_filereg *INPUT) {
  if($2){
     free($2);
   }
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, int **OUTPUT) {

  PyObject *tuple;
  int len = PyInt_AsLong($result);

  /* Error */
  if(len < 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
  }

  /* No error */
  else{

      /* No statuses returned */
      if(*$1 < 1){
          tuple = PyList_New(1);
          Py_INCREF(Py_None);
          PyList_SetItem(tuple, 0, Py_None);
      }

      /* Statuses returned in array */
      else{
          int i;
          tuple = PyList_New (*$1);

          for (i=0; i<*$1; i++) {
              PyObject *aux_obj = PyInt_FromLong ((long) (*$2)[i]);
              PyList_SetItem (tuple, i, aux_obj);
          }/* end of for */

      }/* end of: Statuses returned in array */

  }/* end of: No error */

  /* Return the results (homogeneous for every case) */
  PyObject * old_result = $result;
  $result = PyList_New(2);
  /* First element of the returned tuple is the C call return value */
  PyList_SetItem($result, 0, old_result);
  /* Second element of the returned tuple is a tuple of the python lfc_acl objects */
  PyList_SetItem($result, 1, tuple);


}/* end of: typemap(argout) */


/* C/C++ declarations and apply statement */

%apply (int LENGTH, struct lfc_filereg *INPUT) {(int nbfiles, struct lfc_filereg *files)};
%apply (int *LENGTH, int **OUTPUT) {(int *nbstatuses, int **statuses)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_registerfiles (int nbfiles, struct lfc_filereg *files, int *nbstatuses, int **statuses);

%clear (int nbfiles, struct lfc_filereg *files);
%clear (int *nbstatuses, int **statuses);


/**************************************************
    Typemaps definition for lfc_client_setVOMS_data
***************************************************/
%typemap(in) (char **INPUT, int LENGTH) {
  int i;
  if (!PyList_Check($input)) {
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }
  $2 = PyList_Size($input);
  $1 = (char **) malloc(($2+1)*sizeof(char *));
  for (i = 0; i < $2; i++) {
    PyObject *s = PyList_GetItem($input,i);
    if (!PyString_Check(s)) {
        free($1);
        PyErr_SetString(PyExc_ValueError, "List items must be strings");
        return NULL;
    }
    $1[i] = PyString_AsString(s);
  }
  $1[i] = 0;
}

// Free the char** used as input for the C function, if that is not used to build the return value
%typemap(freearg) (char **INPUT, int LENGTH) {
   if ($1) free($1);
}


%apply (char **INPUT, int LENGTH) {(char **fqans, int nbfqans)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int lfc_client_setVOMS_data (char *vo, char **fqans, int nbfqans);

%clear (char **fqans, int nbfqans);

#endif




/****************************
      Rest of declarations
****************************/

%include "lfc_api.h"
%include "Castor_limits.h"
%include "Cns_constants.h"
%include "Cns_struct.h"
struct dirent {
        long            d_ino;
        long            d_off;
        unsigned short  d_reclen;
        char            d_name[256];
};
typedef unsigned int gid_t;
typedef unsigned int mode_t;
typedef long int time_t;
typedef unsigned int uid_t;
typedef unsigned long long u_signed64;
struct utimbuf {
        time_t actime;
        time_t modtime;
};
#define R_OK 4
#define W_OK 2
#define X_OK 1
#define F_OK 0
#define _PROTO(a) a
#define EXTERN_C extern
#define DLL_DECL
%include "Cns_api.h"
EXTERN_C int DLL_DECL serrno;
EXTERN_C char DLL_DECL *sstrerror _PROTO((int));

