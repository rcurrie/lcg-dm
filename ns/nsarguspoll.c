/*
 * Copyright (C) 2010-2011 by CERN/IT/GT/DMS
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: nsarguspoll.c$ $Revision$ $Date$ CERN IT-GT/DMS Jean-Philippe Baud";
#endif /* not lint */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cns_api.h"
#include "argus/pep.h"
#include "serrno.h"
char *getenv();
xacml_action_t *create_xacml_action_id (const char *);
xacml_request_t *create_xacml_request (xacml_subject_t *, xacml_resource_t *, xacml_action_t *);
xacml_resource_t *create_xacml_resource_id (const char *);
xacml_subject_t *create_xacml_subject_dn_ca (const char *, const char *);
xacml_subject_t *create_xacml_subject_vo_fqan (const char *, const char *);
int merge_xacml_subject_attrs_into (xacml_subject_t *, xacml_subject_t *);
static int show_xacml_request(xacml_request_t *);
main(int argc, char **argv)
{
	xacml_action_t *action;
	xacml_decision_t decision;
	char fqan[256];
	struct Cns_groupinfo *grp_entries;
	int i;
	int nbentries;
	char *p;
	PEP *pep;
	pep_error_t pep_rc;
	char *pep_url;
	xacml_request_t *request;
	xacml_resource_t *resource;
	char *resourceid;
	xacml_response_t *response;
	xacml_result_t *result;
	xacml_subject_t *subject;
	struct Cns_userinfo *usr_entries;
	char voname[256];

	if (argc < 3) {
		fprintf (stderr, "usage: %s resourceid pep_url\n", argv[0]);
		exit (USERR);
	}
	pep = pep_initialize();
	if (pep == NULL) {
		fprintf (stderr, "failed to initialize PEP client\n");
		exit (SYERR);
	}

	resourceid = argv[1];
	for (i = 2; i < argc; i++) {
		pep_url = argv[i];
		pep_rc = pep_setoption (pep, PEP_OPTION_ENDPOINT_URL, pep_url);
		if (pep_rc != PEP_OK) {
			fprintf (stderr, "failed to set PEPd url: %s: %s\n", pep_url, pep_strerror(pep_rc));
			exit (CONFERR);
		}
	}
	if ((p = getenv ("X509_USER_CERT")) == NULL)
		p = "/etc/grid-security/hostcert.pem";
	pep_setoption (pep, PEP_OPTION_ENDPOINT_CLIENT_CERT, p);
	if ((p = getenv ("X509_USER_KEY")) == NULL)
		p = "/etc/grid-security/hostkey.pem";
	pep_setoption (pep, PEP_OPTION_ENDPOINT_CLIENT_KEY, p);
	if ((p = getenv ("X509_CERT_DIR")) == NULL)
		p = "/etc/grid-security/certificates";
	pep_setoption (pep, PEP_OPTION_ENDPOINT_SERVER_CAPATH, p);

	/* get user names from Cns_userinfo table */

	if (Cns_getusrmap (&nbentries, &usr_entries) < 0) {
		fprintf (stderr, "getusrmap: %s\n", sstrerror(serrno));
		exit (SYERR);
	}

	/* update status of each entry */

	for (i = 0; i < nbentries; i++) {
		subject = create_xacml_subject_dn_ca ((usr_entries+i)->username,
		    (usr_entries+i)->user_ca);
		resource = create_xacml_resource_id (resourceid);
		action = create_xacml_action_id ("ANY");
		request = create_xacml_request (subject, resource, action);
		response = NULL;
		if (request == NULL) {
			fprintf (stderr, "failed to create XACML request\n");
			goto freerr1;
		}
		show_xacml_request (request);
		pep_rc = pep_authorize (pep, &request, &response);
		if (pep_rc != PEP_OK) {
			fprintf (stderr, "failed to authorize XACML request: %s\n",
			    pep_strerror(pep_rc));
			goto freerr1;
		}
		if (xacml_response_results_length (response) <= 0)
			goto freerr1;
		result = xacml_response_getresult (response, 0);
		decision = xacml_result_getdecision (result);
		if (((usr_entries+i)->banned & ARGUS_BAN) &&
		    (decision == XACML_DECISION_PERMIT ||
		    decision == XACML_DECISION_NOT_APPLICABLE))
			Cns_modifyusrmap ((usr_entries+i)->userid,
			    (usr_entries+i)->username,
			    (usr_entries+i)->banned & ~ARGUS_BAN);
		else if (((usr_entries+i)->banned & ARGUS_BAN) == 0 &&
		    decision == XACML_DECISION_DENY)
			Cns_modifyusrmap ((usr_entries+i)->userid,
			    (usr_entries+i)->username,
			    (usr_entries+i)->banned | ARGUS_BAN);
freerr1:
		xacml_request_delete (request);
		xacml_response_delete (response);
	}
	free (usr_entries);

	/* get group names from Cns_groupinfo table */

	if (Cns_getgrpmap (&nbentries, &grp_entries) < 0) {
		fprintf (stderr, "getgrpmap: %s\n", sstrerror(serrno));
		exit (SYERR);
	}

	/* update status of each entry */

	for (i = 0; i < nbentries; i++) {
		strcpy (voname, (grp_entries+i)->groupname);
		if ((p = strchr (voname, '/')))
			*p = '\0';
		fqan[0] = '/';
		strcpy (&fqan[1], (grp_entries+i)->groupname);
		subject = create_xacml_subject_vo_fqan (voname, fqan);
		resource = create_xacml_resource_id (resourceid);
		action = create_xacml_action_id ("ANY");
		request = create_xacml_request (subject, resource, action);
		response = NULL;
		if (request == NULL) {
			fprintf (stderr, "failed to create XACML request\n");
			goto freerr2;
		}
		show_xacml_request (request);
		pep_rc = pep_authorize (pep, &request, &response);
		if (pep_rc != PEP_OK) {
			fprintf (stderr, "failed to authorize XACML request: %s\n",
			    pep_strerror(pep_rc));
			goto freerr2;
		}
		if (xacml_response_results_length (response) <= 0)
			goto freerr2;
		result = xacml_response_getresult (response, 0);
		decision = xacml_result_getdecision (result);
		if (((grp_entries+i)->banned & ARGUS_BAN) &&
		    (decision == XACML_DECISION_PERMIT ||
		    decision == XACML_DECISION_NOT_APPLICABLE))
			Cns_modifygrpmap ((grp_entries+i)->gid,
			    (grp_entries+i)->groupname,
			    (grp_entries+i)->banned & ~ARGUS_BAN);
		else if (((grp_entries+i)->banned & ARGUS_BAN) == 0 &&
		    decision == XACML_DECISION_DENY)
			Cns_modifygrpmap ((grp_entries+i)->gid,
			    (grp_entries+i)->groupname,
			    (grp_entries+i)->banned | ARGUS_BAN);
freerr2:
		xacml_request_delete (request);
		xacml_response_delete (response);
	}
	free (grp_entries);

	pep_destroy (pep);
	exit (0);
}

xacml_action_t *
create_xacml_action_id (const char *actionid)
{
	xacml_action_t *action;
	xacml_attribute_t *action_attr_id;

	if (actionid == NULL)
		return (NULL);
	action = xacml_action_create();
	if (action == NULL) {
		fprintf (stderr, "can not allocate XACML Action\n");
		return (NULL);
	}
	action_attr_id = xacml_attribute_create (XACML_ACTION_ID);
	if (action_attr_id == NULL) {
		fprintf (stderr, "can not allocate XACML Action Attribute: %s\n",
		    XACML_ACTION_ID);
		xacml_action_delete (action);
		return (NULL);
	}
	xacml_attribute_addvalue (action_attr_id, actionid);
	xacml_action_addattribute (action, action_attr_id);
	return (action);
}

xacml_resource_t *
create_xacml_resource_id (const char *resourceid)
{
	xacml_resource_t *resource;
	xacml_attribute_t *resource_attr_id;

	if (resourceid == NULL)
		return (NULL);
	resource = xacml_resource_create();
	if (resource == NULL) {
		fprintf (stderr, "can not allocate XACML Resource\n");
		return (NULL);
	}
	resource_attr_id = xacml_attribute_create (XACML_RESOURCE_ID);
	if (resource_attr_id == NULL) {
		fprintf (stderr, "can not allocate XAMCL Resource Attribute: %s\n",
		    XACML_RESOURCE_ID);
		xacml_resource_delete (resource);
		return (NULL);
	}
	xacml_attribute_addvalue (resource_attr_id, resourceid);
	xacml_resource_addattribute (resource, resource_attr_id);
	return (resource);
}

xacml_request_t *
create_xacml_request (xacml_subject_t *subject, xacml_resource_t *resource, xacml_action_t *action)
{
	xacml_request_t * request;

	request = xacml_request_create();
	if (request == NULL) {
		fprintf (stderr, "can not allocate XACML Request\n");
		xacml_subject_delete (subject);
		xacml_resource_delete (resource);
		xacml_action_delete (action);
		return (NULL);
	}
	if (subject != NULL)
		xacml_request_addsubject (request, subject);
	if (resource != NULL)
		xacml_request_addresource (request, resource);
	if (action != NULL)
		xacml_request_setaction (request, action);
	return (request);
}

xacml_subject_t *
create_xacml_subject (const char *id, const char *datatype, const char *value) {
	xacml_subject_t *subject;
	xacml_attribute_t *subject_attr_id; 

	subject= xacml_subject_create();
	if (subject == NULL) {
		fprintf (stderr, "can not allocate XACML Subject\n");
		return (NULL);
	}
	subject_attr_id = xacml_attribute_create (id);
	if (subject_attr_id == NULL) {
		fprintf (stderr, "can not allocate XACML Subject Attribute: %s\n", id);
		xacml_subject_delete (subject);
		return (NULL);
	}
	xacml_attribute_setdatatype (subject_attr_id, datatype);
	xacml_attribute_addvalue (subject_attr_id, value);
	xacml_subject_addattribute (subject, subject_attr_id);
	return (subject);
}

xacml_subject_t *
create_xacml_subject_dn_ca (const char *user_dn, const char *user_ca)
{
	xacml_subject_t *subject;
	xacml_subject_t *subject_ca;
	xacml_subject_t *subject_id;

	subject= xacml_subject_create();
	subject_id = create_xacml_subject (XACML_SUBJECT_ID,
	    XACML_DATATYPE_STRING, user_dn);
	if (subject_id == NULL) {
		xacml_subject_delete (subject);
		return (NULL);
	}
	merge_xacml_subject_attrs_into (subject_id, subject);
	if (! user_ca || ! *user_ca)
		return (subject);
	subject_ca = create_xacml_subject (XACML_GRIDWN_ATTRIBUTE_SUBJECT_ISSUER,
	    XACML_DATATYPE_STRING, user_ca);
	if (subject_ca == NULL) {
		xacml_subject_delete (subject);
		return (NULL);
	}
	merge_xacml_subject_attrs_into (subject_ca, subject);
	return (subject);
}

xacml_subject_t *
create_xacml_subject_vo_fqan (const char *voname, const char *fqan)
{
	xacml_subject_t *subject;
	xacml_subject_t *subject_fqan;
	xacml_subject_t *subject_vo;

	subject= xacml_subject_create();
	subject_vo = create_xacml_subject (XACML_GRIDWN_ATTRIBUTE_VIRTUAL_ORGANIZATION,
	    XACML_DATATYPE_STRING, voname);
	if (subject_vo == NULL) {
		xacml_subject_delete (subject);
		return (NULL);
	}
	merge_xacml_subject_attrs_into (subject_vo, subject);
	subject_fqan = create_xacml_subject (XACML_GRIDWN_ATTRIBUTE_FQAN,
	    XACML_GRIDWN_DATATYPE_FQAN, fqan);
	if (subject_fqan == NULL) {
		xacml_subject_delete (subject);
		return (NULL);
	}
	merge_xacml_subject_attrs_into (subject_fqan, subject);
	return (subject);
}

int
merge_xacml_subject_attrs_into (xacml_subject_t *from_subject, xacml_subject_t *to_subject)
{
	xacml_attribute_t *attr;
	int i;
	size_t l;

	if (to_subject == NULL) {
		fprintf (stderr, "destination XACML Subject is NULL\n");
		return (-1);
	}
	if (from_subject == NULL)
		return (0);
	l = xacml_subject_attributes_length (from_subject);
	for (i= 0; i < l; i++) {
		attr = xacml_subject_getattribute (from_subject, i);
		if (xacml_subject_addattribute (to_subject, attr) != PEP_XACML_OK) {
			fprintf (stderr, "failed to merge attribute %d into Subject\n",i);
			return (-2);
		}
	}
	return (0);
}

static int show_xacml_request(xacml_request_t * request) {
	if (request == NULL) {
		fprintf(stderr, "show_request: request is NULL\n");
		return 1;
	}
	size_t subjects_l= xacml_request_subjects_length(request);
	fprintf(stderr, "request: %d subjects\n", (int)subjects_l);
	int i= 0;
	for (i= 0; i<subjects_l; i++) {
		xacml_subject_t * subject= xacml_request_getsubject(request,i);
		const char * category= xacml_subject_getcategory(subject);
		if (category)
			fprintf(stderr, "request.subject[%d].category= %s\n", i, category);
		size_t attrs_l= xacml_subject_attributes_length(subject);
		fprintf(stderr, "request.subject[%d]: %d attributes\n", i, (int)attrs_l);
		int j= 0;
		for(j= 0; j<attrs_l; j++) {
			xacml_attribute_t * attr= xacml_subject_getattribute(subject,j);
			const char * attr_id= xacml_attribute_getid(attr);
			if (attr_id)
				fprintf(stderr, "request.subject[%d].attribute[%d].id= %s\n", i,j,attr_id);
			const char * attr_datatype= xacml_attribute_getdatatype(attr);
			if (attr_datatype)
				fprintf(stderr, "request.subject[%d].attribute[%d].datatype= %s\n", i,j,attr_datatype);
			const char * attr_issuer= xacml_attribute_getissuer(attr);
			if (attr_issuer)
				fprintf(stderr, "request.subject[%d].attribute[%d].issuer= %s\n", i,j,attr_issuer);
			size_t values_l= xacml_attribute_values_length(attr);
			//fprintf("request.subject[%d].attribute[%d]: %d values", i,j,(int)values_l);
			int k= 0;
			for (k= 0; k<values_l; k++) {
				const char * attr_value= xacml_attribute_getvalue(attr,k);
				fprintf(stderr, "request.subject[%d].attribute[%d].value[%d]= %s\n", i,j,k,attr_value);
			}
		}
	}
	size_t resources_l= xacml_request_resources_length(request);
	fprintf(stderr, "request: %d resources\n", (int)resources_l);
	for (i= 0; i<resources_l; i++) {
		xacml_resource_t * resource= xacml_request_getresource(request,i);
		const char * res_content= xacml_resource_getcontent(resource);
		if (res_content)
			fprintf(stderr, "request.resource[%d].content= %s\n", i, res_content);
		size_t attrs_l= xacml_resource_attributes_length(resource);
		fprintf(stderr, "request.resource[%d]: %d attributes\n", i, (int)attrs_l);
		int j= 0;
		for(j= 0; j<attrs_l; j++) {
			xacml_attribute_t * attr= xacml_resource_getattribute(resource,j);
			const char * attr_id= xacml_attribute_getid(attr);
			if (attr_id)
				fprintf(stderr, "request.resource[%d].attribute[%d].id= %s\n", i,j,attr_id);
			const char * attr_datatype= xacml_attribute_getdatatype(attr);
			if (attr_datatype)
				fprintf(stderr, "request.resource[%d].attribute[%d].datatype= %s\n", i,j,attr_datatype);
			const char * attr_issuer= xacml_attribute_getissuer(attr);
			if (attr_issuer)
				fprintf(stderr, "request.resource[%d].attribute[%d].issuer= %s\n", i,j,attr_issuer);
			size_t values_l= xacml_attribute_values_length(attr);
			//fprintf("request.resource[%d].attribute[%d]: %d values", i,j,(int)values_l);
			int k= 0;
			for (k= 0; k<values_l; k++) {
				const char * attr_value= xacml_attribute_getvalue(attr,k);
				if (attr_value)
					fprintf(stderr, "request.resource[%d].attribute[%d].value[%d]= %s\n", i,j,k,attr_value);
			}
		}
	}
	int j= 0;
	xacml_action_t * action= xacml_request_getaction(request);
	if (action) {
		size_t act_attrs_l= xacml_action_attributes_length(action);
		fprintf(stderr, "request.action: %d attributes\n",(int)act_attrs_l);
		for (j= 0; j<act_attrs_l; j++) {
			xacml_attribute_t * attr= xacml_action_getattribute(action,j);
			const char * attr_id= xacml_attribute_getid(attr);
			if (attr_id)
				fprintf(stderr, "request.action.attribute[%d].id= %s\n", j,attr_id);
			const char * attr_datatype= xacml_attribute_getdatatype(attr);
			if (attr_datatype)
				fprintf(stderr, "request.action.attribute[%d].datatype= %s\n", j,attr_datatype);
			const char * attr_issuer= xacml_attribute_getissuer(attr);
			if (attr_issuer)
				fprintf(stderr, "request.action.attribute[%d].issuer= %s\n", j,attr_issuer);
			size_t values_l= xacml_attribute_values_length(attr);
			//fprintf("request.action.attribute[%d]: %d values", j,(int)values_l);
			int k= 0;
			for (k= 0; k<values_l; k++) {
				const char * attr_value= xacml_attribute_getvalue(attr,k);
				if (attr_value)
					fprintf(stderr, "request.action.attribute[%d].value[%d]= %s\n",j,k,attr_value);
			}
		}
	}
	xacml_environment_t * env= xacml_request_getenvironment(request);
	if (env) {
		size_t env_attrs_l= xacml_environment_attributes_length(env);
		fprintf(stderr, "request.environment: %d attributes\n",(int)env_attrs_l);
		for (j= 0; j<env_attrs_l; j++) {
			xacml_attribute_t * attr= xacml_environment_getattribute(env,j);
			const char * attr_id= xacml_attribute_getid(attr);
			if (attr_id)
				fprintf(stderr, "request.environment.attribute[%d].id= %s\n", j,attr_id);
			const char * attr_datatype= xacml_attribute_getdatatype(attr);
			if (attr_datatype)
				fprintf(stderr, "request.environment.attribute[%d].datatype= %s\n", j,attr_datatype);
			const char * attr_issuer= xacml_attribute_getissuer(attr);
			if (attr_issuer)
				fprintf(stderr, "request.environment.attribute[%d].issuer= %s\n", j,attr_issuer);
			size_t values_l= xacml_attribute_values_length(attr);
			//fprintf("request.environment.attribute[%d]: %d values", j,(int)values_l);
			int k= 0;
			for (k= 0; k<values_l; k++) {
				const char * attr_value= xacml_attribute_getvalue(attr,k);
				if (attr_value)
					fprintf(stderr, "request.environment.attribute[%d].value[%d]= %s\n",j,k,attr_value);
			}
		}
	}
	return 0;
}
