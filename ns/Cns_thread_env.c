/*
 * Copyright (C) 2011 by CERN/IT/GT/DMS
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_thread_env.c,v $ $Revision: 1.0 $ $Date: 2011/11/07 14:17:00 $ CERN IT-GT/DMS Alejandro Álvarez Ayllón";
#endif /* not lint */

/*	Cns_thread_env - getenv/setenv equivalent calls, but thread safe */

#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include "Cglobals.h"
#include "Cns_api.h"
#include "Cthread_env.h"


int DLL_DECL
Cns_setenv(const char *key, const char *value, int replace)
{
  return Cthread_setenv(key, value, replace);
}

char* DLL_DECL
Cns_getenv(const char *key)
{
  return Cthread_getenv(key);
}
