.\" @(#)$RCSfile: Cns_listreplica.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2010 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_LISTREPLICA 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_listreplica \- list replica entries for a given file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "struct Cns_filereplica *Cns_listreplica (const char *" path ,
.BI "const char *" guid ,
.BI "int " flags ,
.BI "Cns_list *" listp )
.SH DESCRIPTION
.B Cns_listreplica
lists replica entries for a given file.
This routine returns a pointer to a structure containing the current replica
entry.
.PP
.nf
.ft CW
struct Cns_filereplica {
	u_signed64	fileid;
	u_signed64	nbaccesses;
	time_t		atime;		/* last access to replica */
	time_t		ptime;		/* replica pin time */
	char		status;
	char		f_type;		/* 'V' for Volatile, 'P' for Permanent */
	char		poolname[CA_MAXPOOLNAMELEN+1];
	char		host[CA_MAXHOSTNAMELEN+1];
	char		fs[80];
	char		sfn[CA_MAXSFNLEN+1];
};
.ft
.fi
.TP
.I path
specifies the logical pathname.
.TP
.I guid
specifies the Grid Unique IDentifier.
.TP
.I flags
may be one of the following constant:
.RS
.TP
.B CNS_LIST_BEGIN
the first call must have this flag set to allocate buffers and
initialize pointers.
.TP
.B CNS_LIST_CONTINUE
all the following calls must have this flag set.
.TP
.B CNS_LIST_END
final call to terminate the list and free resources.
.RE
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current replica
entry if the operation was successful or NULL if all entries have been returned
or if the operation failed. In the latter case,
.B serrno
is set appropriately.
.SH EXAMPLES
A C program listing the replicas fo a given sfn could look like:
.sp
.nf
.ft CW
	int flags;
	Cns_list list;
	struct Cns_filereplica *lp;

	flags = CNS_LIST_BEGIN;
	while ((lp = Cns_listreplica (sfn, NULL, flags, &list)) != NULL) {
		flags = CNS_LIST_CONTINUE;
		/* process the entry */
		.....
	}
	(void) Cns_listreplica (sfn, NULL, CNS_LIST_END, &list);
.ft
.fi
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file does not exist.
.TP
.B EACCES
Search permission is denied on a component of the parent directory.
.TP
.B ENOMEM
Memory could not be allocated for the output buffer.
.TP
.B EFAULT
.I path
and
.I guid
are NULL pointers or
.I listp
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B EINVAL
The length of
.I guid
exceeds
.B CA_MAXGUIDLEN
or path and guid are both given and they point at a different file.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.BR CA_MAXPATHNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_addreplica(3)
