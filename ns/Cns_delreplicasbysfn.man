.\" @(#)$RCSfile: Cns_delreplicasbysfn.man,v $ $Revision: 1.1 $ $Date: 2009/04/06 08:30:13 $ CERN IT-GS Jean-Philippe Baud
.\" Copyright (C) 2009 by CERN/IT/GS
.\" All rights reserved
.\"
.TH CNS_DELREPLICASBYSFN 3 "$Date: 2009/04/06 08:30:13 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_delreplicasbysfn \- delete entries associated with a list of sfns and corresponding lfn if last replica
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_delreplicasbysfn (int " nbfiles ,
.BI "const char **" sfns ,
.BI "const char **" guids ,
.BI "int *" nbstatuses ,
.BI "int **" statuses )
.SH DESCRIPTION
.B Cns_delreplicasbysfn
deletes entries associated with a list of sfns and corresponding lfn if last replica.
An extra check may be obtained by specifying a list of
.IR guids .
.TP
.I nbfiles
specifies the number of file names in the array
.IR sfns .
.TP
.I sfns
specifies either the Site URLs or the Physical File Names for the list of replicas.
.TP
.I guids
specifies the list of Grid Unique IDentifiers (optional). Can be NULL.
.TP
.I nbstatuses
will be set to the number of replies in the array of statuses.
.TP
.I statuses
will be set to the address of an array of integer statuses allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
Individual statuses are 0 if the operation was successful or set to the
serrno value in case of error.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file or replica does not exist.
.TP
.B E2BIG
Request too large (max 1 MB).
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request or unmarshalling
the reply.
.TP
.B EACCES
Search permission is denied on a component of the parent directory or
write permission is denied on the parent directory or
the parent has the sticky bit S_ISVTX set and
.RS 1.5i
.LP
the effective user ID of the requestor does not match the owner ID of the file and
.LP
the effective user ID of the requestor does not match the owner ID of the
directory and
.LP
the file is not writable by the requestor and
.LP
the requestor does not have ADMIN privilege in the Cupv database.
.RE
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request or unmarshalling
the reply.
.TP
.B EFAULT
.IR sfns ,
.I nbstatuses
or
.I statuses
is a NULL pointer.
.TP
.B EINVAL
.I nbfiles
is not strictly positive or the length of one of the
.I guids
exceeds
.BR CA_MAXGUIDLEN .
.TP
.B ENAMETOOLONG
The length of
.I sfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSSERV
Service unknown.
.TP 
.B SEINTERNAL 
Database error.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_delreplica(3) ,
.BR Cns_unlink(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
