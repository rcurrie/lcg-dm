.\" @(#)$RCSfile: lfc_python.man,v $ $Revision$ $Date$ CERN IT-GD/CT David Smith
.\" Copyright (C) 2004-2007 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH lfc_python 3 "$Date$" LFC "Python Reference"
.SH NAME
lfc \- Python interface to the LFC
.br
lfcthr \- Thread enabled version of Python interface to the LFC
.SH SYNOPSIS
.B import lfc
.br

.br
.B import lfcthr
.br
.B lfcthr.init()

.SH DESCRIPTION
The lfc module permits you to access the LFC client interface from python
programs. The lfc module is a swig wrapping of the standard C interface.
For detailed descriptions of each function see the individual man page of
each function.

The lfcthr module is a version of the lfc module supporting multi-threaded 
Python clients. Its usage is similar to the usage of the lfc module except
the obligatory initialisation call lfcthr.init() in the main program before 
threads are started.
 
There follows a series of examples of how to use selected functions and how
to retrieve the information returned by them: Examples are finding the
GUID of an existing entry, listing the replicas of a given GUID and
setting and retrieving the comment associated with an entry.
.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import lfc

"""
# stat an existing entry in the LFC and print the GUID
"""

name = "/grid/dteam/my.test"

stat = lfc.lfc_filestatg()
res = lfc.lfc_statg(name,"",stat)

if res == 0:
   guid = stat.guid
   print "The GUID for " + name + " is " + guid
else:
   err_num = lfc.cvar.serrno
   err_string = lfc.sstrerror(err_num)
   print "There was an error while looking for " + name + ": Error " + str(err_num) \\
		+ " (" + err_string + ")"
   sys.exit(1)
.fi
.SH EXAMPLE
.nf
#!/usr/bin/python

import lfc

"""
# list the replicas of a given entry, starting from the GUID
"""

guid = "6a3164e0-a4d7-4abe-9f76-e3b8882735d1"

listp = lfc.lfc_list()
flag = lfc.CNS_LIST_BEGIN

print "Listing replicas for GUID " + guid

num_replicas=0

while(1):
   res = lfc.lfc_listreplica("",guid,flag,listp)
   flag = lfc.CNS_LIST_CONTINUE

   if res == None:
      break
   else:
      rep_name = res.sfn
      print "Replica: " + rep_name
      num_replicas = num_replicas + 1

lfc.lfc_listreplica("",guid,lfc.CNS_LIST_END,listp)
print "Found " + str(num_replicas) + " replica(s)"
.fi
.SH EXAMPLE
.nf
#!/usr/bin/python

import sys
import lfc
import re

"""
# setting and retrieving a comment on a file
"""

file = "/grid/dteam/my.test"

comment = "MyComment"
res = lfc.lfc_setcomment(file,comment)

if res != 0:
   err_num = lfc.cvar.serrno
   err_string = lfc.sstrerror(err_num)
   print "Problem while setting comment for " + file + ": Error " + str(err_num) \\
		+ " (" + err_string + ")"
   sys.exit(1)

buffer=""
for i in range(0,lfc.CA_MAXCOMMENTLEN+1):
  buffer=buffer + " "
  
res = lfc.lfc_getcomment(file,buffer)

if res != 0:
   err_num = lfc.cvar.serrno
   err_string = lfc.sstrerror(err_num)
   print "Problem while reading the comment for " + file + ": Error " + str(err_num) \\
		+ " (" + err_string + ")"
   sys.exit(1)

r = re.compile("(.*?)\0", re.DOTALL)
comment = r.findall(buffer)[0]

print "Read back comment " + comment

.SH EXAMPLE
.nf
#!/usr/bin/python

"""
# Using the lfc_readdirxr method
"""

import sys
import lfc

name = "/grid/dteam/my.test"

dir = lfc.lfc_opendirg(name,"")
if (dir == None) or (dir == 0):
        err_num = lfc.cvar.serrno
        err_string = lfc.sstrerror(err_num)
        print "Error while looking for " + name + ": Error " + str(err_num) \\
			+ " (" + err_string + ")"
        sys.exit(1)

while 1:
        read_pt = lfc.lfc_readdirxr(dir,"")
        if (read_pt == None) or (read_pt == 0):
                break
        entry, list = read_pt
        print entry.d_name
        try:
                for i in range(len(list)):
                        print " ==> %s" % list[i].sfn
        except TypeError, x:
                print " ==> None"

lfc.lfc_closedir(dir)
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import lfc

"""
# Using the lfc_getlinks method
"""

result, list = lfc.lfc_getlinks("/grid/dteam/antotests/extratests/dir2/f105", "")
print result
print len(list)
if (result == 0):
	for i in list:
		print i.path
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import lfc

"""
# Using the lfc_getreplica method
"""

result, list = lfc.lfc_getreplica("/grid/dteam/antotests/extratests/dir2/f105", "", "")
print result
print len(list)
if (result == 0):
	for i in list:
		print i.host
		print i.sfn
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import lfc

"""
# Using the lfc_getacl and lfc_setacl methods to add a user ACL
"""

nentries, acls_list = lfc.lfc_getacl("/grid/dteam/tests_sophie3", lfc.CA_MAXACLENTRIES)

print nentries
print len(acls_list)

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

# When adding a first ACL for a given user, you also need to add the mask
# When adding the second user ACL, it is not necessary anymore

acl_user = lfc.lfc_acl()
acl_mask = lfc.lfc_acl()

acl_user.a_type=2		# 2 corresponds to CNS_ACL_USER
acl_user.a_id=18701		# user id
acl_user.a_perm=5

acl_mask.a_type=5		# 5 corresponds to CNS_ACL_MASK
acl_mask.a_id=0			# no user id specified
acl_mask.a_perm=5

acls_list.append(acl_user)
acls_list.append(acl_mask)

res = lfc.lfc_setacl("/grid/dteam/tests_sophie3", acls_list)

if res == 0:
        print "OK"
else:
        err_num = lfc.cvar.serrno
        err_string = lfc.sstrerror(err_num)
        print "There was an error : Error " + str(err_num) + " (" + err_string + ")"
        sys.exit(1)
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import lfc

"""
# Using the lfc_getacl and lfc_setacl methods to remove a user ACL
"""

nentries, acls_list = lfc.lfc_getacl("/grid/dteam/tests_sophie3", lfc.CA_MAXACLENTRIES)

# Note : you cannot remove the owner ACL (i.e. for CNS_ACL_USER_OBJ type) if ACLs
# ====== for other users exist. Ff all the other user ACLs are deleted, the owner
# ====== ACL is automatically removed.

for i in acls_list:
        print i.a_type
        print i.a_id
        print i.a_perm

del acls_list[1]	# delete a given user ACL from the list of ACLs

res = lfc.lfc_setacl("/grid/dteam/tests_sophie3", acls_list)

if res == 0:
        print "OK"
else:
        err_num = lfc.cvar.serrno
        err_string = lfc.sstrerror(err_num)
        print "There was an error : Error " + str(err_num) + " (" + err_string + ")"
        sys.exit(1)
.fi

.SH EXAMPLE
.nf
#!/usr/bin/env python
import lfcthr 
import os
from threading import Thread

class slave(Thread):

   def __init__ (self):
      Thread.__init__(self)
      
   def run(self):
        ....
        result = lfcthr.lfc_getreplica("", guid, "")
        ....
        return
        
if __name__ == '__main__':

    os.environ['LFC_HOST'] = 'my_lfc.cern.ch'

    #	Threaded library initialisation
    lfcthr.init()

    ....
    #	Start up of threads
    for i in xrange(totalNumberOfSlaves):
        slv = slave(i)
        slv.start()
        
    ....

.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import lfc

"""
# Using the lfc_getusrmap method
"""

result, list = lfc.lfc_getusrmap()
print result
print len(list)
if (result == 0):
	for i in list:
		print i.userid + " " + i.username
.fi

.SH EXAMPLE
.nf
#!/usr/bin/python

import lfc

"""
# Using the lfc_getgrpmap method
"""

result, list = lfc.lfc_getgrpmap()
print result
print len(list)
if (result == 0):
	for i in list:
		print i.gid + " " + i.groupname
.fi

.SH KNOWN BUGS
The current interface to the \fBlfc_getcomment(3)\fP, \fBlfc_getcwd(3)\fP,
\fBlfc_readlink(3)\fP, \fBlfc_seterrbuf(3)\fP requires the passing of str object which is
modified to contain the result (in a similar way to the C functions, which accept a buffer).
However this breaks the immutability of python str. This will be changed in the future.
.SH SEE ALSO
.B LFC C interface man pages
