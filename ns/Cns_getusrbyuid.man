.\" @(#)$RCSfile: Cns_getusrbyuid.man,v $ $Revision: 1.1 $ $Date: 2005/10/19 11:30:57 $ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH CNS_GETUSRBYUID 3 "$Date: 2005/10/19 11:30:57 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_getusrbyuid \- get user name associated with a given virtual uid
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getusrbyuid (uid_t " uid ,
.BI "char *" username )
.SH DESCRIPTION
.B Cns_getusrbyuid
gets the user name associated with a given virtual uid.
.TP
.I uid
specifies the Virtual User Id.
.TP
.I username
points at a buffer to receive the associated user name.
The buffer must be at least 256 characters long.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I username
is a NULL pointer.
.TP
.B EINVAL
This uid does not exist in the internal mapping table.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
