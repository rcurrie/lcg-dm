.\" @(#)$RCSfile: nschown.man,v $ $Revision: 1.2 $ $Date: 2007/01/15 08:05:17 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2007 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSCHOWN 1 "$Date: 2007/01/15 08:05:17 $" CASTOR "Cns User Commands"
.SH NAME
nschown \- change owner and group of a CASTOR directory/file in the name server
.SH SYNOPSIS
.B nschown
.RB [ -h ]
.RB [ -R ]
.IR owner [: group ]
.IR path ...
.LP
.B nschown
.RB [ -h ]
.RB [ -R ]
.RI : group
.IR path ...
.SH DESCRIPTION
.B nschown
sets the owner and/or the group of a CASTOR directory/file in the name
server to the values in
.I owner
and
.I group
respectively.
.LP
To change the owner ID, if the group ID does not change and if the caller and
the new owner ID belong to that group, GRP_ADMIN privilege is needed, otherwise
the caller must have ADMIN privilege in the Cupv database.
To change the group ID, the effective user ID of the process must match the
owner ID of the file and the new group must be in the list of groups the caller
belong to or the caller must have ADMIN privilege in the Cupv database.
.TP
.I owner
is either a valid username or a valid numeric ID.
.TP
.I group
is either a valid group name or a valid numeric ID.
.TP
.I path
specifies the CASTOR pathname.
If
.I path
does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.SH OPTIONS
The following options are supported:
.TP
.B -h
If
.I path
is a symbolic link, changes the ownership of the link itself.
.TP
.B -R
Recursive mode.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chown(3) ,
.B Cupvlist(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
