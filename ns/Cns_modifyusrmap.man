.\" @(#)$RCSfile: Cns_modifyusrmap.man,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2010 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH CNS_MODIFYUSRMAP 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_modifyusrmap \- modify user entry corresponding to a given virtual uid
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_modifyusrmap (uid_t " uid ,
.BI "char *" newname ,
.BI "int " status)
.SH DESCRIPTION
.B Cns_modifyusrmap
modifies the user entry corresponding to a given virtual uid.
.TP
.I uid
specifies the Virtual User Id.
.TP
.I newname
specifies the new user name.
It must be at most 255 characters long.
.TP
.I status
status can be set to 0 or a combination of ARGUS_BAN and LOCAL_BAN.
.LP
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B EFAULT
.I newname
is a NULL pointer.
.TP
.B EEXIST
This user exists already.
.TP
.B EINVAL
This uid does not exist in the internal mapping table or the length of
.I newname
exceeds 255.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
