.\" @(#)$RCSfile: nsdaemon.man,v $ $Revision$ $Date$ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2011 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSDAEMON 8 "$Date$" CASTOR "Cns Administrator Commands"
.SH NAME
nsdaemon \- start the name server
.SH SYNOPSIS
.B nsdaemon
[
.BI -c " config_file"
] [
.BI -l " log_file"
] [
.BI -m " map_file"
] [
.B -n
] [
.B -r
] [
.B -s 0|1
] [
.BI -t " nbthreads"
] [
.BI -4
|
.BI -6
]
.SH DESCRIPTION
.LP
The
.B nsdaemon
command starts the name server.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will read the name server configuration file,
create the "/" entry in the database if it does not exist yet,
create a pool of threads and look for requests.
Each of them is processed in a thread which opens a connection to the
database server if necessary.
When a request has been completed, the thread becomes idle until it is allocated
to another request.
The connection to the database server is kept open between 2 requests.
If the name server is being shutdown or not active, the requests are
automatically retried by the client API.
.LP
All error messages and statistical information are kept in a log.
.LP
The name server listen port number can be defined on client hosts and
on the name server itself in either of the following ways:
.RS
.LP
setting an environment variable CNS_PORT
.RS
.HP
setenv CNS_PORT 5010
.RE
.LP
an entry in
.B /etc/shift.conf
like:
.RS
.HP
CNS	PORT	5010
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of CNS_PORT in Cns_constants.h.
.LP
The name server host name can be defined on client hosts
in either of the following ways:
.RS
.LP
setting an environment variable CNS_HOST, for example:
.RS
.HP
setenv CNS_HOST castor5
.RE
.LP
an entry in
.B /etc/shift.conf
for example:
.RS
.HP
CNS	HOST	castor5
.RE
.RE
.LP
If none of these methods is used, the default host is taken from the
definition of NsHost in site.def.
.LP
The name server database keeps the metadata information: filename, file size,
access time, permissions and migration status.
.LP
The name server configuration file contains password information for the
database and must be readable/writable only by root.
It contains a single line in the format:
.HP
.RS
username/password@server
.RE
or
.RS
username/password@server/dbname
.RE
.sp
where 'username' and 'password' are the credentials to login to the database
instance identified by 'server'. If 'dbname' is not specified, "cns_db" is used.
.LP
In the log each entry has a timestamp.
All entries corresponding to one request have the same request id.
For each user command there is one message NS092 giving information about
the requestor (hostname, uid, gid) and one message NS098 giving the command
itself.
The completion code of the command is also logged.
.SH OPTIONS
.TP
.BI -c " config_file"
Specifies a different path for the Name Server configuration file.
.TP
.BI -l " log_file"
Specifies a different path for the Name Server log file.
The special value
.B syslog
will send the log messages to the system logger syslogd.
.TP
.BI -m " map_file"
Specifies a different path for the Name Server mapping file.
.TP
.B -n
disables automatic creation of virtual ids.
.TP
.B -r
read-only mode. Only query operations are allowed.
.TP
.B -s " 0|1"
replica restrict mode. If the argument is 1 then commands which change replicas 
will only be accepted from trusted connections. The dpnsaemon will default to
this mode, other instances of the name server service do not.
.TP
.BI -t " nbthreads"
specifies the number of threads. The value must be lower than CNS_MAXNBTHREADS
(currently 100). Default is 20.
.TP
.B -4
only try to listen on IPv4 addresses
.TP
.B -6
only try to listen on IPv6 addresses
.SH FILES
.TP 1.5i
.B /etc/NSCONFIG
configuration file
.TP
.B /usr/spool/ns/log
.SH EXAMPLES
.TP
Here is a small log:
.nf
09/13 14:36:35  6089 Cns_serv: started
09/13 14:36:36  6089 Cns_serv: creating /
09/13 14:37:02  6089 Cns_srv_mkdir: NS092 - mkdir request by 0,0 from castor5.cern.ch
09/13 14:37:02  6089 Cns_srv_mkdir: NS098 - mkdir /castor 777 22
09/13 14:37:02  6089 Cns_srv_mkdir: returns 0
09/13 14:37:18  6089 Cns_srv_stat: NS092 - stat request by 0,0 from castor5.cern.ch
09/13 14:37:18  6089 Cns_srv_stat: NS098 - stat /castor
09/13 14:37:18  6089 Cns_srv_stat: returns 0
.fi
.SH SEE ALSO
.BR Clogit(3) ,
.B nsshutdown(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
