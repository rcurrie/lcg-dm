/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: nslistgrpmap.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	nslistgrpmap - query about a given group or list all existing groups in virtual gid table */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "Cns_api.h"
#include "serrno.h"
extern	char	*optarg;
extern	int	optind;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char *dp;
	int errflg = 0;
	uid_t gid = -1;
	char *groupname = NULL;
	struct Cns_groupinfo *grp_entries;
	int i;
	static struct Coptions longopts[] = {
		{"gid", REQUIRED_ARGUMENT, 0, OPT_IDMAP_GID},
		{"group", REQUIRED_ARGUMENT, 0, OPT_IDMAP_GROUP},
		{0, 0, 0, 0}
	};
	int nbentries;
	char p_stat;
	char vidstr[256];

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_IDMAP_GID:
			if ((gid = strtol (Coptarg, &dp, 10)) < 0 || *dp != '\0') {
				fprintf (stderr, "invalid gid: %s\n", Coptarg);
				errflg++;
			}
			break;
		case OPT_IDMAP_GROUP:
			if (strlen (Coptarg) > 255) {
				fprintf (stderr,
				    "group name too long: %s\n", Coptarg);
				errflg++;
			} else
				groupname = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc)
		errflg++;
	if (errflg) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "[--gid gid] [--group groupname]\n");
		exit (USERR);
	}

	if (gid != -1) {
		if (Cns_getgrpbygid (gid, vidstr) < 0) {
			fprintf (stderr, "nslistgrpmap %d: %s\n", gid,
			    (serrno == ENOENT) ? "No such group" : sstrerror(serrno));
			exit (USERR);
		}
		printf ("%8d %s\n", gid, vidstr);
	} else if (groupname) {
		if (Cns_getgrpbynam (groupname, &gid) < 0) {
			fprintf (stderr, "nslistgrpmap %s: %s\n", groupname,
			    (serrno == ENOENT) ? "No such group" : sstrerror(serrno));
			exit (USERR);
		}
		printf ("%8d %s\n", gid, groupname);
	} else {
		if (Cns_getgrpmap (&nbentries, &grp_entries) < 0) {
			fprintf (stderr, "nslistgrpmap: %s\n", sstrerror(serrno));
			exit (USERR);
		}
		for (i = 0; i < nbentries; i++) {
			p_stat = '\0';
			printf ("%8d %s", (grp_entries+i)->gid,
			    (grp_entries+i)->groupname);
			if ((grp_entries+i)->banned & ARGUS_BAN) {
				printf (p_stat ? "|ARGUS_BAN" : " ARGUS_BAN");
				p_stat = '|';
			}
			if ((grp_entries+i)->banned & LOCAL_BAN) {
				printf (p_stat ? "|LOCAL_BAN" : " LOCAL_BAN");
				p_stat = '|';
			}
			printf ("\n");
		}
	}
	exit (0);
}
