/*
 * Copyright (C) 1999-2008 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_readdir.c,v $ $Revision: 1.5 $ $Date: 2008/02/04 12:08:52 $ CERN IT-PDP/DM Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_readdir - read a directory entry */

#include <errno.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

#if (defined(sun) || defined(linux))
struct dirent64 DLL_DECL *
#else
struct dirent DLL_DECL *
#endif
Cns_readdir64(Cns_DIR *dirp)
{
	int c;
	int direntsz;
#if (defined(sun) || defined(linux))
	struct dirent64 *dp;
#else
	struct dirent *dp;
#endif
	char func[16];
	int getattr = 0;
	gid_t gid;
	int msglen;
	int n;
	int nbentries;
	char *q;
	char *rbp;
	char repbuf[DIRBUFSZ+4];
	char *sbp;
	char sendbuf[REQBUFSZ];
	uid_t uid;
 
	strcpy (func, "Cns_readdir");
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (NULL);
	}
#endif

	if (! dirp) {
		serrno = EFAULT;
		return (NULL);
	}
	/* compute size of client machine dirent structure excluding d_name */

#if (defined(sun) || defined(linux))
	dp = (struct dirent64 *) dirp->dd_buf;
#else
	dp = (struct dirent *) dirp->dd_buf;
#endif
	direntsz = &dp->d_name[0] - (char *) dp;

	if (dirp->dd_size == 0) {	/* no data in the cache */
		if (dirp->eod)
			return (NULL);

		/* Build request header */

		sbp = sendbuf;
		marshall_LONG (sbp, CNS_MAGIC);
		marshall_LONG (sbp, CNS_READDIR);
		q = sbp;        /* save pointer. The next field will be updated */
		msglen = 3 * LONGSIZE;
		marshall_LONG (sbp, msglen);
	 
		/* Build request body */

		marshall_LONG (sbp, uid);
		marshall_LONG (sbp, gid);
		marshall_WORD (sbp, getattr);
		marshall_WORD (sbp, direntsz);
		marshall_HYPER (sbp, dirp->fileid);
		marshall_WORD (sbp, dirp->bod);

		msglen = sbp - sendbuf;
		marshall_LONG (q, msglen);	/* update length field */

		c = send2nsd (&dirp->dd_fd, NULL, sendbuf, msglen,
		    repbuf, sizeof(repbuf));

		if (c < 0)
			return (NULL);
		rbp = repbuf;
		unmarshall_WORD (rbp, nbentries);
		if (nbentries == 0)
			return (NULL);		/* end of directory */

		/* unmarshall reply into the dirent structures */

#if (defined(sun) || defined(linux))
		dp = (struct dirent64 *) dirp->dd_buf;
#else
		dp = (struct dirent *) dirp->dd_buf;
#endif
		while (nbentries--) {
			dp->d_ino = 0;
#if defined(linux) || defined(sgi) || defined(SOLARIS)
			dp->d_off = 0;
#endif
#if defined(_AIX)
			dp->d_offset = 0;
#endif
#if defined(linux) || defined(__APPLE__)
			dp->d_type = 0;
#endif
			unmarshall_STRING (rbp, dp->d_name);
			n = strlen (dp->d_name);
#if defined(_AIX) || (defined(__alpha) && defined(__osf__)) || defined(hpux) || defined(__APPLE__)
			dp->d_namlen = n;
#endif
			dp->d_reclen = ((direntsz + n + 8) / 8) * 8;
#if (defined(sun) || defined(linux))
			dp = (struct dirent64 *) ((char *) dp + dp->d_reclen);
#else
			dp = (struct dirent *) ((char *) dp + dp->d_reclen);
#endif
		}
		dirp->bod = 0;
		unmarshall_WORD (rbp, dirp->eod);
		dirp->dd_size = (char *) dp - dirp->dd_buf;
	}
#if (defined(sun) || defined(linux))
	dp = (struct dirent64 *) (dirp->dd_buf + dirp->dd_loc);
#else
	dp = (struct dirent *) (dirp->dd_buf + dirp->dd_loc);
#endif
	dirp->dd_loc += dp->d_reclen;
	if (dirp->dd_loc >= dirp->dd_size) {	/* must refill next time */
		dirp->dd_loc = 0;
		dirp->dd_size = 0;
	}
	return (dp);
}

struct dirent DLL_DECL *
Cns_readdir(Cns_DIR *dirp)
{
#if !defined(SOLARIS) && !defined(linux)
	return (Cns_readdir64 (dirp));
#else
	struct dirent64 *dp64;
	struct dirent *dp32;
	short namlen;

	if ((dp64 = Cns_readdir64 (dirp)) == NULL)
		return (NULL);

	namlen = strlen (dp64->d_name);
	dp32 = (struct dirent *) dp64;
	dp32->d_ino = dp64->d_ino;
	dp32->d_off = dp64->d_off;
#if defined(linux)
	dp32->d_type = dp64->d_type;
#endif
	strcpy (dp32->d_name, dp64->d_name);
	dp32->d_reclen = ((&dp32->d_name[0] - (char *) dp32 + namlen + 8) / 8) * 8;
	return (dp32);
#endif
}
