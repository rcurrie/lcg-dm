/*
 * Copyright (C) 1999-2011 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: nslogit.c,v $ $Revision$ $Date$ CERN IT-PDP/DM Jean-Philippe Baud";
#endif /* not lint */

#include <stdarg.h>
#include <syslog.h>
#include "Clogit.h"

nslogit(char *func, char *msg, ...)
{
	va_list args;

	va_start (args, msg);
	Cvlogit (LOG_INFO, func, msg, args);
	va_end (args);
	return (0);
}
