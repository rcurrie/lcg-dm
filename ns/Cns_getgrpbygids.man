.\" @(#)$RCSfile: Cns_getgrpbygids.man,v $ $Revision: 1.1 $ $Date: 2007/03/21 10:24:15 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_GETGRPBYGIDS 3 "$Date: 2007/03/21 10:24:15 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_getgrpbygids \- get group names associated with given virtual gids
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_getgrpbygids (int " nbgroups ,
.BI "gid_t *" gids ,
.BI "char **" groupnames )
.SH DESCRIPTION
.B Cns_getgrpbygids
gets the group names associated with given virtual gids.
.TP
.I nbgroups
specifies the number of gids.
.TP
.I gids
specifies the Virtual Group Ids.
.TP
.I groupnames
specifies a buffer to receive the pointers to the associated group names.
The buffer must be big enough to hold
.I nbgroups
pointer values.
The client application is responsible for freeing the array elements when not
needed anymore.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
Memory could not be allocated for unmarshalling the reply.
.TP
.B EFAULT
.I gids
or
.I groupnames
is a NULL pointer.
.TP
.B EINVAL
.I nbgroups
is not strictly positive or one of the gids does not exist in the internal
mapping table.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
