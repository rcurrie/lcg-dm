#!/bin/sh
#
# dpmcopyd         Start/Stop dpmcopyd
#
# chkconfig: - 93 17
# description: DPM COPY server daemon
#
# $Id$
#
# @(#)$RCSfile: dpmcopyd.scripts.mysql,v $ $Revision$ $Date$ CERN/IT/ADC/CA Jean-Damien Durand
#

#
## PLEASE USE sysconfig FILE TO CHANGE THESE ENVIRONMENT VARIABLES
#
PREFIX=/opt/lcg
GRIDMAPDIR=/etc/grid-security/gridmapdir
export GRIDMAPDIR
GRIDMAP=/etc/grid-security/grid-mapfile
export GRIDMAP
LD_LIBRARY_PATH=/opt/glite/lib:/opt/globus/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
CSEC_MECH=ID
export CSEC_MECH
DPMCOPYDLOGFILE=/var/log/dpmcopy/log
DPMCONFIGFILE=/opt/lcg/etc/DPMCONFIG
DPMUSER=dpmmgr
DPMGROUP=dpmmgr

sysname=`uname -s`

# source function library
if [ -r /etc/rc.d/init.d/functions ]; then
    . /etc/rc.d/init.d/functions
    DAEMON="daemon --check dpmcopyd"
    FAILURE=failure
    ECHO_FAILURE=echo_failure
    SUCCESS=success
    ECHO_SUCCESS=echo_success
    ECHO_END=echo
    if [ $sysname = "Linux" ]; then
        ECHOOPT=-n
    fi
else
    DAEMON=eval
    FAILURE=
    ECHO_FAILURE=
    SUCCESS=
    ECHO_SUCCESS=
    ECHOOPT=
    ECHO_END=
fi

RETVAL=0
prog="dpmcopyd"
PIDFILE=/var/run/dpmcopyd.pid
PIDDIR=/var/run
SUBSYS=/var/lock/subsys/dpmcopyd
SUBSYSDIR=/var/lock/subsys
[ -z "$SILENTSTOP" ] && SILENTSTOP=0
DPMCOPYD=$PREFIX/sbin/dpmcopyd
DPMCOPYSHUTDOWN=$PREFIX/sbin/dpmcopy-shutdown
if [ -r $PREFIX/etc/dpmcopyd.conf ]; then
    SYSCONFIGDPMCOPYD=$PREFIX/etc/dpmcopyd.conf
elif [ -r /etc/sysconfig/dpmcopyd ]; then
    SYSCONFIGDPMCOPYD=/etc/sysconfig/dpmcopyd
elif [ -r /etc/default/dpmcopyd ]; then
    SYSCONFIGDPMCOPYD=/etc/default/dpmcopyd
fi

#
## Blindly attempt to create useful directories
#
# [ ! -d $PIDDIR ] && mkdir -p $PIDDIR > /dev/null 2>&1
# [ ! -d $SUBSYSDIR ] && mkdir -p $SUBSYSDIR > /dev/null 2>&1

if [ $sysname = "HP-UX" ]; then
    export UNIX95=1
fi
if [ $sysname = "SunOS" ]; then
    format4comm="fname"
elif [ $sysname = "Darwin" ]; then
    format4comm="ucomm"
else
    format4comm="comm"
fi

case "$1" in
  start)
    DPMUSERHOME=`finger $DPMUSER | grep -i directory: | cut -f 2 -d' '`
    X509_USER_CERT=/etc/grid-security/$DPMUSER/dpmcert.pem
    export X509_USER_CERT
    X509_USER_KEY=/etc/grid-security/$DPMUSER/dpmkey.pem
    export X509_USER_KEY

    netstat -an | egrep '[:.]5017' | egrep 'LISTEN *$' > /dev/null
    if [ $? -eq 0 ]; then
        echo $ECHOOPT "dpmcopyd already started: "
        [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "dpmcopyd already started: "
        RETVAL=0
    else
        if [ -n "$SYSCONFIGDPMCOPYD" ]; then
            #
            ## Source the configuration
            #
            . $SYSCONFIGDPMCOPYD
            if [ "${RUN_DPMCOPYDAEMON}" != "yes" ]; then
                echo $ECHOOPT "$SYSCONFIGDPMCOPYD says NO: "
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "$SYSCONFIGDPMCOPYD says NO: "
                RETVAL=0
                $ECHO_END
                exit $RETVAL
            fi
            export DPM_HOST
            export DPNS_HOST
            if [ -n "${ULIMIT_N}" ]; then
                ulimit -n ${ULIMIT_N}
            fi
        fi
        if [ ! -s "$DPMCONFIGFILE" ]; then
            echo $ECHOOPT "config file $DPMCONFIGFILE empty: "
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "config file $DPMCONFIGFILE empty: "
            RETVAL=1
        fi
        if [ $RETVAL -eq 0 ]; then
            echo $ECHOOPT "Starting $prog: "
            cd /
            mkdir -p `dirname $DPMCOPYDLOGFILE`
            chown $DPMUSER:$DPMGROUP `dirname $DPMCOPYDLOGFILE`
            if [ "${ALLOW_COREDUMP}" != "yes" ]; then
                $DAEMON "su $DPMUSER -c \"LD_LIBRARY_PATH=$LD_LIBRARY_PATH $DPMCOPYD -c $DPMCONFIGFILE -l $DPMCOPYDLOGFILE\""
            else
                $DAEMON "su $DPMUSER -c \"mkdir -p $DPMUSERHOME/dpmcopy; cd $DPMUSERHOME/dpmcopy; hostname -f >> logstart; date >> logstart; umask 0; umask >> logstart; ulimit -c unlimited; pwd >> logstart; ulimit -c >> logstart; echo DAEMON $DPMCOPYD \`hostname -f\` >> logstart; LD_LIBRARY_PATH=$LD_LIBRARY_PATH $DPMCOPYD -c $DPMCONFIGFILE -l $DPMCOPYDLOGFILE\""
            fi
            if [ $? -eq 0 ]; then
                [ -d $SUBSYSDIR ] && touch $SUBSYS
                if [ -d $PIDDIR ]; then
                    pid=`ps -eo pid,ppid,$format4comm | grep " 1 dpmcopyd$" | awk '{print $1}'`
                    # The point of $PIDFILE is that it kills only
                    # the master daemon.
                    rm -f $PIDFILE
                    echo $pid > $PIDFILE
                fi
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Starting $prog: "
                RETVAL=0
            else
                [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Starting $prog: "
                RETVAL=1
            fi
        fi
    fi
    $ECHO_END
    ;;
  stop)
    if [ -f $PIDFILE ]; then
        echo $ECHOOPT "Stopping $prog: "
        if [ -x $DPMCOPYSHUTDOWN ]; then
            $DPMCOPYSHUTDOWN -f -h `hostname` > /dev/null 2>&1
            RETVAL=$?
            if [ $RETVAL -ne 0 ]; then
                kill -2 -`cat $PIDFILE` > /dev/null 2>&1
                RETVAL=$?
            fi
        else
            kill -2 -`cat $PIDFILE` > /dev/null 2>&1
            RETVAL=$?
        fi
        if [ $RETVAL -eq 0 ]; then
            rm -f $PIDFILE
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
        else
            [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
        fi
    else
        # dpmcopyd might have been started by hand
        pid=`ps -eo pid,ppid,$format4comm | grep " 1 dpmcopyd$" | awk '{print $1}'`
        if [ -n "$pid" ]; then
            echo $ECHOOPT "Stopping $prog: "
            if [ -x $DPMCOPYSHUTDOWN ]; then
                $DPMCOPYSHUTDOWN -f -h `hostname` > /dev/null 2>&1
                RETVAL=$?
                if [ $RETVAL -ne 0 ]; then
                    kill -2 -$pid > /dev/null 2>&1
                    RETVAL=$?
                fi
            else
                kill -2 -$pid > /dev/null 2>&1
                RETVAL=$?
            fi
            if [ $RETVAL -eq 0 ]; then
                [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "Stopping $prog: "
            else
                [ -n "$ECHO_FAILURE" ] && $ECHO_FAILURE "Stopping $prog: "
            fi
        else
            echo $ECHOOPT "dpmcopyd already stopped: "
            [ -n "$ECHO_SUCCESS" ] && $ECHO_SUCCESS "dpmcopyd already stopped: "
            [ $SILENTSTOP -eq 0 ] && RETVAL=0 || RETVAL=1
        fi
    fi

    lpid="X"
    while [ "x$lpid" != "x" ]; do
        sleep 1
        #Does not seem to work on SunOS ;-)
        lpid=`ps -eo pid,ppid,$format4comm | grep " 1 dpmcopyd$" | awk '{print $1}'`
    done

    [ -d $SUBSYSDIR ] && rm -f $SUBSYS
    $ECHO_END
    ;;
  restart | force-reload)
    $0 stop
    if [ $? -eq 0 -o $SILENTSTOP -eq 0 ]; then
        sleep 1
        $0 start
        RETVAL=$?
    else
        RETVAL=0
    fi
    ;;
  condrestart | try-restart)
    SILENTSTOP=1
    export SILENTSTOP
    $0 restart
    RETVAL=$?
    ;;
  reload)
    ;;
  status)
    pid=`ps -eo pid,ppid,$format4comm | grep " 1 dpmcopyd$" | awk '{print $1}'`
    if [ -n "$pid" ]; then
        echo $ECHOOPT "dpmcopyd (pid $pid) is running..."
        $ECHO_SUCCESS
        $ECHO_END
        RETVAL=0
    else
        if [ -f $PIDFILE ]; then
            pid=`head -1 $PIDFILE`
            if [ "$pid" != "" ] ; then
                echo $ECHOOPT "dpmcopyd dead but pid file exists"
                $ECHO_FAILURE
                $ECHO_END
                RETVAL=1
            else
                echo $ECHOOPT "dpmcopyd dead"
                $ECHO_FAILURE
                $ECHO_END
                RETVAL=1
            fi
        else
            if [ -f $SUBSYS ]; then
                echo $ECHOOPT "dpmcopyd dead but subsys ($SUBSYS) locked"
                RETVAL=2
            else
                echo $ECHOOPT "dpmcopyd is stopped"
                RETVAL=3
            fi
            $ECHO_FAILURE
            $ECHO_END
        fi
    fi
    ;;
  *)
    echo "Usage: $0 {start|stop|status|restart|condrestart}"
    RETVAL=1
    ;;
esac

exit $RETVAL
