#
#  Copyright (C) 2007 by CERN/IT/GD/ITR
#  All rights reserved
#
#       @(#)$RCSfile: oralink.mk,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud
 
#    Link dpmcopyd with Oracle libraries.

ifdef ORACLE_SHARE
include $(ORACLE_SHARE)/demo_proc_ic.mk
else
include $(ORACLE_HOME)/precomp/lib/env_precomp.mk
endif

ifdef ORACLE_LIB
LIBHOME=$(ORACLE_LIB)
endif

PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL) $(STATICTTLIBS)
PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL)

dpmcopyd: $(SRV_OBJS)
	$(CC) -o dpmcopyd $(CLDFLAGS) $(SRV_OBJS) $(LIBS) -L$(LIBHOME) $(PROLDLIBS)
