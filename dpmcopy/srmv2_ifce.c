/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_ifce.c,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <string.h>
#include "cgsi_plugin.h"
#include "dpm.h"
#include "dpm_api.h"
#include "dpm_server.h"
#include "dpmcopy_server.h"
#include "serrno.h"
#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
static enum ns1__TAccessLatency al_types[] = {ONLINE, NEARLINE};
static enum ns1__TOverwriteMode o_modes[] = {NEVER, ALWAYS};
static enum ns1__TFileStorageType s_types[] = {VOLATILE, DURABLE, PERMANENT};
#define SRM_SERVICE_PATH "/srm/managerv2"

int
get_srm_endpoint(char *surl, char *epbuf, int epbufsz)
{
	char *p;

	if ((p = strstr (surl + 6, "?SFN="))) {	/* full endpoint is provided */
		if (p - surl + 3 > epbufsz)
			return (-1);
		strcpy (epbuf, "httpg");
		strncpy (epbuf + 5, surl + 3, p - surl - 3);
		*(epbuf + (p - surl + 2)) = '\0';
	} else {
		if ((p = strchr (surl + 6, '/')) == NULL)
			return (-1);
		if (p - surl + 2 + sizeof(SRM_SERVICE_PATH) > epbufsz)
			return (-1);
		strcpy (epbuf, "httpg");
		strncpy (epbuf + 5, surl + 3, p - surl - 3);
		*(epbuf + (p - surl + 2)) = '\0';
		strcat (epbuf, SRM_SERVICE_PATH);
	}
	return (0);
}

int
soap_error2serrno(struct soap *soap)
{
	if (soap->error == SOAP_EOM)
		return (ENOMEM);
	else if (soap->error == SOAP_EOF)
		return (SETIMEDOUT);
	else {
		if (soap->fault && soap->fault->faultstring) {
			dpm_errmsg (NULL, "%s\n", soap->fault->faultstring);
			if (strstr (soap->fault->faultstring, "credential"))
				return (ESEC_BAD_CREDENTIALS);
			else
				return (EINVAL);
		} else
			return (SEINTERNAL);
	}
}

int
statuscode2serrno(int statuscode)
{
	switch (statuscode) {
	case SRM_USCOREAUTHENTICATION_USCOREFAILURE:
		return (ESEC_BAD_CREDENTIALS);
	case SRM_USCOREAUTHORIZATION_USCOREFAILURE:
		return (EACCES);
	case SRM_USCOREINVALID_USCOREREQUEST:
		return (EINVAL);
	case SRM_USCOREINVALID_USCOREPATH:
		return (ENOENT);
	case SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED:
		dpm_errmsg (NULL, "Space lifetime expired\n");
		return (EINVAL);
	case SRM_USCORENO_USCOREFREE_USCORESPACE:
		return (ENOSPC);
	case SRM_USCOREDUPLICATION_USCOREERROR:
		return (EEXIST);
	case SRM_USCOREINTERNAL_USCOREERROR:
		return (SEINTERNAL);
	case SRM_USCORENOT_USCORESUPPORTED:
		return (SEOPNOTSUP);
	default:
		return (EINVAL);
	}
}

int statuscode2dpmstatus(int statuscode, int isfile)
{
	switch (statuscode) {
	case SRM_USCORESUCCESS:
		return (DPM_SUCCESS);
	case SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED:
		return (DPM_EXPIRED);
	case SRM_USCOREREQUEST_USCOREQUEUED:
		return (DPM_QUEUED);
	case SRM_USCOREREQUEST_USCOREINPROGRESS:
		return (DPM_ACTIVE);
	case SRM_USCOREABORTED:
		return (DPM_ABORTED);
	case SRM_USCORERELEASED:
		return (DPM_RELEASED);
	case SRM_USCOREFILE_USCOREPINNED:
		return (DPM_READY);
	case SRM_USCOREFILE_USCOREIN_USCORECACHE:
		return (DPM_SUCCESS);
	case SRM_USCORESPACE_USCOREAVAILABLE:
		return (DPM_READY);
	case SRM_USCOREPARTIAL_USCORESUCCESS:
		return (DPM_DONE);
	default:
		if (isfile)
			return (DPM_FAILED | statuscode2serrno (statuscode));
		serrno = statuscode2serrno (statuscode);
		return (-1);
	}
}

int
srmv2_init(struct soap *soap, int timeout)
{
	int flags;

	soap_init (soap);
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (soap, client_cgsi_plugin, &flags);
	soap->connect_timeout = timeout ;
	soap->send_timeout = timeout ;
	soap->recv_timeout = timeout ;
	return (0);
}

int
srmv2_abortfiles(char *r_token, int nbsurls, char **surls, int *nbreplies, struct dpm_filestatus **filestatuses, int timeout)
{
	int i;
	struct ns1__srmAbortFilesResponse_ rep;
	struct ns1__ArrayOfTSURLReturnStatus *repfs;
	struct ns1__srmAbortFilesRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	char srm_endpoint[256];
	int status;

	if (get_srm_endpoint (surls[0], srm_endpoint, sizeof(srm_endpoint)) < 0) {
		dpm_errmsg (NULL, "SRM endpoint too long\n");
		return (-1);
	}

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	req.requestToken = r_token;
	if ((req.arrayOfSURLs =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
	    (req.arrayOfSURLs->urlArray =
	    soap_malloc (&soap, nbsurls * sizeof(char *))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}

	req.arrayOfSURLs->__sizeurlArray = nbsurls;
	for (i = 0; i < nbsurls; i++) {
		req.arrayOfSURLs->urlArray[i] = surls[i];
	}

	if (soap_call_ns1__srmAbortFiles (&soap, srm_endpoint, "AbortFiles", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmAbortFilesResponse->returnStatus;
	repfs = rep.srmAbortFilesResponse->arrayOfFileStatuses;
	if (! repfs || repfs->__sizestatusArray <= 0 || ! repfs->statusArray) {
		*filestatuses = NULL;
	} else if ((*filestatuses = malloc (repfs->__sizestatusArray * sizeof(struct dpm_filestatus)))) {
		*nbreplies = repfs->__sizestatusArray;
		for (i = 0; i < *nbreplies; i++) {
			(*filestatuses+i)->surl = strdup (repfs->statusArray[i]->surl);
			(*filestatuses+i)->status =
			    statuscode2dpmstatus (repfs->statusArray[i]->status->statusCode, 1);
			if (repfs->statusArray[i]->status->explanation)
				(*filestatuses+i)->errstring =
				    strdup (repfs->statusArray[i]->status->explanation);
			else
				(*filestatuses+i)->errstring = NULL;
		}
	}
	if (reqstatp->explanation)
		dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
	status = statuscode2dpmstatus (reqstatp->statusCode, 0);
	soap_end (&soap);
	soap_done (&soap);
	return (status);
}

int
srmv2_get(int nbfiles, char **surls, int nbprotocols, char **protocols, int timeout, char **r_token, int *nbreplies, struct dpm_getfilestatus **filestatuses)
{
	int i;
	struct ns1__srmPrepareToGetResponse_ rep;
	struct ns1__ArrayOfTGetRequestFileStatus *repfs;
	struct ns1__srmPrepareToGetRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	char srm_endpoint[256];
	int status;

	if (get_srm_endpoint (surls[0], srm_endpoint, sizeof(srm_endpoint)) < 0) {
		dpm_errmsg (NULL, "SRM endpoint too long\n");
		return (-1);
	}

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFileRequests =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOfTGetFileRequest))) == NULL ||
	    (req.arrayOfFileRequests->requestArray =
	    soap_malloc (&soap, nbfiles * sizeof(struct ns1__TGetFileRequest *))) == NULL ||
	    (req.transferParameters =
	    soap_malloc (&soap, sizeof(struct ns1__TTransferParameters))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}

	req.arrayOfFileRequests->__sizerequestArray = nbfiles;
	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFileRequests->requestArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TGetFileRequest))) == NULL) {
			serrno = ENOMEM;
			soap_end (&soap);
			soap_done (&soap);
			return (-1);
		}
		req.arrayOfFileRequests->requestArray[i]->sourceSURL = (char *) surls[i];
		req.arrayOfFileRequests->requestArray[i]->dirOption = NULL;
	}

	memset (req.transferParameters, 0, sizeof(struct ns1__TTransferParameters));
	if ((req.transferParameters->arrayOfTransferProtocols =
	    soap_malloc (&soap, nbprotocols * sizeof(struct ns1__ArrayOfString))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	req.transferParameters->arrayOfTransferProtocols->__sizestringArray = nbprotocols;
	req.transferParameters->arrayOfTransferProtocols->stringArray = protocols;

	if (soap_call_ns1__srmPrepareToGet (&soap, srm_endpoint, "PrepareToGet", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmPrepareToGetResponse->returnStatus;
	if (rep.srmPrepareToGetResponse->requestToken)
		*r_token = strdup (rep.srmPrepareToGetResponse->requestToken);
	else
		*r_token = NULL;
	repfs = rep.srmPrepareToGetResponse->arrayOfFileStatuses;
	if (! repfs || repfs->__sizestatusArray <= 0 || ! repfs->statusArray) {
		*filestatuses = NULL;
	} else if ((*filestatuses = calloc (repfs->__sizestatusArray, sizeof(struct dpm_getfilestatus)))) {
		*nbreplies = repfs->__sizestatusArray;
		for (i = 0; i < *nbreplies; i++) {
			(*filestatuses+i)->from_surl = strdup (repfs->statusArray[i]->sourceSURL);
			if (repfs->statusArray[i]->transferURL)
				(*filestatuses+i)->turl = strdup (repfs->statusArray[i]->transferURL);
			if (repfs->statusArray[i]->fileSize)
				(*filestatuses+i)->filesize = *repfs->statusArray[i]->fileSize;
			(*filestatuses+i)->status =
			    statuscode2dpmstatus (repfs->statusArray[i]->status->statusCode, 1);
			if (repfs->statusArray[i]->status->explanation)
				(*filestatuses+i)->errstring =
				    strdup (repfs->statusArray[i]->status->explanation);
			if (repfs->statusArray[i]->remainingPinTime)
				(*filestatuses+i)->pintime = *repfs->statusArray[i]->remainingPinTime;
		}
	}
	if (reqstatp->explanation)
		dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
	status = statuscode2dpmstatus (reqstatp->statusCode, 0);
	soap_end (&soap);
	soap_done (&soap);
	return (status);
}

int
srmv2_getfilesize(char *surl, u_signed64 *filesize, int timeout)
{
	int nblevel = 0;
	struct ns1__srmLsResponse_ rep;
	struct ns1__ArrayOfTMetaDataPathDetail *repfs;
	struct ns1__srmLsRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	char srm_endpoint[256];

	if (get_srm_endpoint (surl, srm_endpoint, sizeof(srm_endpoint)) < 0) {
		dpm_errmsg (NULL, "SRM endpoint too long\n");
		return (-1);
	}

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfSURLs = soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	req.arrayOfSURLs->__sizeurlArray = 1;
	req.arrayOfSURLs->urlArray = &surl;
	req.numOfLevels = &nblevel;

	if (soap_call_ns1__srmLs (&soap, srm_endpoint, "Ls", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmLsResponse->returnStatus;
	repfs = rep.srmLsResponse->details;
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREFAILURE) {
		serrno = statuscode2serrno (reqstatp->statusCode);
		if (reqstatp->explanation)
			dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	if (! repfs || repfs->__sizepathDetailArray <= 0 || ! repfs->pathDetailArray) {
		dpm_errmsg (NULL, "srmLs returned empty response\n");
		serrno = SEINTERNAL;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	if (repfs->pathDetailArray[0]->status &&
	    repfs->pathDetailArray[0]->status->statusCode != SRM_USCORESUCCESS) {
		serrno = statuscode2serrno (repfs->pathDetailArray[0]->status->statusCode);
		if (repfs->pathDetailArray[0]->status->explanation)
			dpm_errmsg (NULL, "%s\n", repfs->pathDetailArray[0]->status->explanation);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	if (! repfs->pathDetailArray[0]->size) {
		dpm_errmsg (NULL, "srmLs returned empty size\n");
		serrno = SEINTERNAL;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	*filesize = *repfs->pathDetailArray[0]->size;
	soap_end (&soap);
	soap_done (&soap);
	return (0);
}

int
srmv2_getstatus_getreq(char *r_token, int nbfiles, char **surls, char *srm_endpoint, int *nbreplies, struct dpm_getfilestatus **filestatuses, int timeout)
{
	int i;
	struct ns1__srmStatusOfGetRequestResponse_ rep;
	struct ns1__ArrayOfTGetRequestFileStatus *repfs;
	struct ns1__srmStatusOfGetRequestRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	int status;

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	req.requestToken = r_token;
	if (surls) {
		if ((req.arrayOfSourceSURLs =
		    soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
		    (req.arrayOfSourceSURLs->urlArray =
		    soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL) {
			serrno = ENOMEM;
			soap_end (&soap);
			soap_done (&soap);
			return (-1);
		}

		req.arrayOfSourceSURLs->__sizeurlArray = nbfiles;
		for (i = 0; i < nbfiles; i++) {
			req.arrayOfSourceSURLs->urlArray[i] = surls[i];
		}
	}

	if (soap_call_ns1__srmStatusOfGetRequest (&soap, srm_endpoint,
	    "StatusOfGetRequest", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmStatusOfGetRequestResponse->returnStatus;
	repfs = rep.srmStatusOfGetRequestResponse->arrayOfFileStatuses;
	if (! repfs || repfs->__sizestatusArray <= 0 || ! repfs->statusArray) {
		*filestatuses = NULL;
	} else if ((*filestatuses = calloc (repfs->__sizestatusArray, sizeof(struct dpm_getfilestatus)))) {
		*nbreplies = repfs->__sizestatusArray;
		for (i = 0; i < *nbreplies; i++) {
			(*filestatuses+i)->from_surl = strdup (repfs->statusArray[i]->sourceSURL);
			if (repfs->statusArray[i]->transferURL)
				(*filestatuses+i)->turl = strdup (repfs->statusArray[i]->transferURL);
			if (repfs->statusArray[i]->fileSize)
				(*filestatuses+i)->filesize = *repfs->statusArray[i]->fileSize;
			(*filestatuses+i)->status =
			    statuscode2dpmstatus (repfs->statusArray[i]->status->statusCode, 1);
			if (repfs->statusArray[i]->status->explanation)
				(*filestatuses+i)->errstring =
				    strdup (repfs->statusArray[i]->status->explanation);
			if (repfs->statusArray[i]->remainingPinTime)
				(*filestatuses+i)->pintime = *repfs->statusArray[i]->remainingPinTime;
		}
	}
	if (reqstatp->explanation)
		dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
	status = statuscode2dpmstatus (reqstatp->statusCode, 0);
	soap_end (&soap);
	soap_done (&soap);
	return (status);
}

int
srmv2_getstatus_putreq(char *r_token, int nbfiles, char **surls, char *srm_endpoint, int *nbreplies, struct dpm_putfilestatus **filestatuses, int timeout)
{
	time_t current_time;
	int i;
	struct ns1__srmStatusOfPutRequestResponse_ rep;
	struct ns1__ArrayOfTPutRequestFileStatus *repfs;
	struct ns1__srmStatusOfPutRequestRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	int status;

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	req.requestToken = r_token;
	if (surls) {
		if ((req.arrayOfTargetSURLs =
		    soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
		    (req.arrayOfTargetSURLs->urlArray =
		    soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL) {
			serrno = ENOMEM;
			soap_end (&soap);
			soap_done (&soap);
			return (-1);
		}

		req.arrayOfTargetSURLs->__sizeurlArray = nbfiles;
		for (i = 0; i < nbfiles; i++) {
			req.arrayOfTargetSURLs->urlArray[i] = surls[i];
		}
	}

	if (soap_call_ns1__srmStatusOfPutRequest (&soap, srm_endpoint,
	    "StatusOfPutRequest", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmStatusOfPutRequestResponse->returnStatus;
	repfs = rep.srmStatusOfPutRequestResponse->arrayOfFileStatuses;
	if (! repfs || repfs->__sizestatusArray <= 0 || ! repfs->statusArray) {
		*filestatuses = NULL;
	} else if ((*filestatuses = calloc (repfs->__sizestatusArray, sizeof(struct dpm_putfilestatus)))) {
		*nbreplies = repfs->__sizestatusArray;
		for (i = 0; i < *nbreplies; i++) {
			(*filestatuses+i)->to_surl = strdup (repfs->statusArray[i]->SURL);
			if (repfs->statusArray[i]->transferURL)
				(*filestatuses+i)->turl = strdup (repfs->statusArray[i]->transferURL);
			if (repfs->statusArray[i]->fileSize)
				(*filestatuses+i)->filesize = *repfs->statusArray[i]->fileSize;
			(*filestatuses+i)->status =
			    statuscode2dpmstatus (repfs->statusArray[i]->status->statusCode, 1);
			if (repfs->statusArray[i]->status->explanation)
				(*filestatuses+i)->errstring =
				    strdup (repfs->statusArray[i]->status->explanation);
			if (repfs->statusArray[i]->remainingPinLifetime)
				(*filestatuses+i)->pintime = *repfs->statusArray[i]->remainingPinLifetime;
			if (repfs->statusArray[i]->remainingFileLifetime) {
				(*filestatuses+i)->f_lifetime =
				    *repfs->statusArray[i]->remainingFileLifetime;
				if ((*filestatuses+i)->f_lifetime < 0)
					(*filestatuses+i)->f_lifetime = 0x7FFFFFFF;
				else if ((*filestatuses+i)->f_lifetime > 0)
					(*filestatuses+i)->f_lifetime += time (0);
			}
		}
	}
	if (reqstatp->explanation)
		dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
	status = statuscode2dpmstatus (reqstatp->statusCode, 0);
	soap_end (&soap);
	soap_done (&soap);
	return (status);
}

int
srmv2_ping(char *endpoint, int timeout)
{
	struct ns1__srmPingResponse_ rep;
	struct ns1__srmPingRequest req;
	struct soap soap;

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));

	if (soap_call_ns1__srmPing (&soap, endpoint, "Ping", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	soap_end (&soap);
	soap_done (&soap);
	return (0);
}

int
srmv2_put(int nbfiles, struct dpm_putfilereq *reqfiles, int nbprotocols, char **protocols, int overwrite, int timeout, char **r_token, int *nbreplies, struct dpm_putfilestatus **filestatuses)
{
	int i;
	struct ns1__srmPrepareToPutResponse_ rep;
	struct ns1__ArrayOfTPutRequestFileStatus *repfs;
	struct ns1__srmPrepareToPutRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	char srm_endpoint[256];
	int status;

	if (get_srm_endpoint (reqfiles->to_surl, srm_endpoint, sizeof(srm_endpoint)) < 0) {
		dpm_errmsg (NULL, "SRM endpoint too long\n");
		return (-1);
	}

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFileRequests =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOfTPutFileRequest))) == NULL ||
	    (req.arrayOfFileRequests->requestArray =
	    soap_malloc (&soap, nbfiles * sizeof(struct ns1__TPutFileRequest *))) == NULL ||
	    (req.transferParameters =
	    soap_malloc (&soap, sizeof(struct ns1__TTransferParameters))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}

	req.arrayOfFileRequests->__sizerequestArray = nbfiles;

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFileRequests->requestArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TPutFileRequest))) == NULL) {
			serrno = ENOMEM;
			soap_end (&soap);
			soap_done (&soap);
			return (-1);
		}
		req.arrayOfFileRequests->requestArray[i]->targetSURL = (reqfiles+i)->to_surl;
		req.arrayOfFileRequests->requestArray[i]->expectedFileSize = &(reqfiles+i)->requested_size;
	}

	if (overwrite)
		req.overwriteOption = &o_modes[ALWAYS];
	if (reqfiles->f_lifetime) {
		if ((req.desiredFileLifeTime = soap_malloc (&soap, sizeof(int))) == NULL) {
			serrno = ENOMEM;
			soap_end (&soap);
			soap_done (&soap);
			return (-1);
		}
		if (reqfiles->f_lifetime == 0x7FFFFFFF)
			*req.desiredFileLifeTime = -1;
		else
			*req.desiredFileLifeTime = reqfiles->f_lifetime;
	}
	if (reqfiles->f_type == 'V')
		req.desiredFileStorageType = &s_types[VOLATILE];
	else if (reqfiles->f_type == 'D')
		req.desiredFileStorageType = &s_types[DURABLE];
	else if (reqfiles->f_type == 'P')
		req.desiredFileStorageType = &s_types[PERMANENT];
	if (*reqfiles->s_token)
		req.targetSpaceToken = reqfiles->s_token;
	if (reqfiles->ret_policy != '\0') {
		if ((req.targetFileRetentionPolicyInfo =
		    soap_malloc (&soap, sizeof(struct ns1__TRetentionPolicyInfo))) == NULL) {
			serrno = ENOMEM;
			soap_end (&soap);
			soap_done (&soap);
			return (-1);
		}
		if (reqfiles->ret_policy == 'R')
			req.targetFileRetentionPolicyInfo->retentionPolicy = REPLICA;
		else if (reqfiles->ret_policy == 'O')
			req.targetFileRetentionPolicyInfo->retentionPolicy = OUTPUT;
		else
			req.targetFileRetentionPolicyInfo->retentionPolicy = CUSTODIAL;
		if (reqfiles->ac_latency == 'O')
			req.targetFileRetentionPolicyInfo->accessLatency = &al_types[ONLINE];
		else if (reqfiles->ac_latency == 'N')
			req.targetFileRetentionPolicyInfo->accessLatency = &al_types[NEARLINE];
	}
	memset (req.transferParameters, 0, sizeof(struct ns1__TTransferParameters));
	if ((req.transferParameters->arrayOfTransferProtocols =
	    soap_malloc (&soap, nbprotocols * sizeof(struct ns1__ArrayOfString))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	req.transferParameters->arrayOfTransferProtocols->__sizestringArray = nbprotocols;
	req.transferParameters->arrayOfTransferProtocols->stringArray = protocols;

	if (soap_call_ns1__srmPrepareToPut (&soap, srm_endpoint, "PrepareToPut", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmPrepareToPutResponse->returnStatus;
	if (rep.srmPrepareToPutResponse->requestToken)
		*r_token = strdup (rep.srmPrepareToPutResponse->requestToken);
	else
		*r_token = NULL;
	repfs = rep.srmPrepareToPutResponse->arrayOfFileStatuses;
	if (! repfs || repfs->__sizestatusArray <= 0 || ! repfs->statusArray) {
		*filestatuses = NULL;
	} else if ((*filestatuses = calloc (repfs->__sizestatusArray, sizeof(struct dpm_putfilestatus)))) {
		*nbreplies = repfs->__sizestatusArray;
		for (i = 0; i < *nbreplies; i++) {
			(*filestatuses+i)->to_surl = strdup (repfs->statusArray[i]->SURL);
			if (repfs->statusArray[i]->transferURL)
				(*filestatuses+i)->turl = strdup (repfs->statusArray[i]->transferURL);
			if (repfs->statusArray[i]->fileSize)
				(*filestatuses+i)->filesize = *repfs->statusArray[i]->fileSize;
			(*filestatuses+i)->status =
			    statuscode2dpmstatus (repfs->statusArray[i]->status->statusCode, 1);
			if (repfs->statusArray[i]->status->explanation)
				(*filestatuses+i)->errstring =
				    strdup (repfs->statusArray[i]->status->explanation);
			if (repfs->statusArray[i]->remainingPinLifetime)
				(*filestatuses+i)->pintime = *repfs->statusArray[i]->remainingPinLifetime;
			if (repfs->statusArray[i]->remainingFileLifetime)
				(*filestatuses+i)->f_lifetime =
				    *repfs->statusArray[i]->remainingFileLifetime;
		}
	}
	if (reqstatp->explanation)
		dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
	status = statuscode2dpmstatus (reqstatp->statusCode, 0);
	soap_end (&soap);
	soap_done (&soap);
	return (status);
}

int
srmv2_putdone(char *r_token, int nbsurls, char **surls, int *nbreplies, struct dpm_filestatus **filestatuses, int timeout)
{
	int i;
	struct ns1__srmPutDoneResponse_ rep;
	struct ns1__ArrayOfTSURLReturnStatus *repfs;
	struct ns1__srmPutDoneRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	char srm_endpoint[256];
	int status;

	if (get_srm_endpoint (surls[0], srm_endpoint, sizeof(srm_endpoint)) < 0) {
		dpm_errmsg (NULL, "SRM endpoint too long\n");
		return (-1);
	}

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	req.requestToken = r_token;
	if ((req.arrayOfSURLs =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
	    (req.arrayOfSURLs->urlArray =
	    soap_malloc (&soap, nbsurls * sizeof(char *))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}

	req.arrayOfSURLs->__sizeurlArray = nbsurls;
	for (i = 0; i < nbsurls; i++) {
		req.arrayOfSURLs->urlArray[i] = surls[i];
	}

	if (soap_call_ns1__srmPutDone (&soap, srm_endpoint, "PutDone", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmPutDoneResponse->returnStatus;
	repfs = rep.srmPutDoneResponse->arrayOfFileStatuses;
	if (! repfs || repfs->__sizestatusArray <= 0 || ! repfs->statusArray) {
		*filestatuses = NULL;
	} else if ((*filestatuses = malloc (repfs->__sizestatusArray * sizeof(struct dpm_filestatus)))) {
		*nbreplies = repfs->__sizestatusArray;
		for (i = 0; i < *nbreplies; i++) {
			(*filestatuses+i)->surl = strdup (repfs->statusArray[i]->surl);
			(*filestatuses+i)->status =
			    statuscode2dpmstatus (repfs->statusArray[i]->status->statusCode, 1);
			if (repfs->statusArray[i]->status->explanation)
				(*filestatuses+i)->errstring =
				    strdup (repfs->statusArray[i]->status->explanation);
			else
				(*filestatuses+i)->errstring = NULL;
		}
	}
	if (reqstatp->explanation)
		dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
	status = statuscode2dpmstatus (reqstatp->statusCode, 0);
	soap_end (&soap);
	soap_done (&soap);
	return (status);
}

int
srmv2_relfiles(char *r_token, int nbsurls, char **surls, int *nbreplies, struct dpm_filestatus **filestatuses, int timeout)
{
	int i;
	struct ns1__srmReleaseFilesResponse_ rep;
	struct ns1__ArrayOfTSURLReturnStatus *repfs;
	struct ns1__srmReleaseFilesRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct soap soap;
	char srm_endpoint[256];
	int status;

	if (get_srm_endpoint (surls[0], srm_endpoint, sizeof(srm_endpoint)) < 0) {
		dpm_errmsg (NULL, "SRM endpoint too long\n");
		return (-1);
	}

	srmv2_init (&soap, timeout);

	memset (&req, 0, sizeof(req));
	req.requestToken = r_token;
	if ((req.arrayOfSURLs =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
	    (req.arrayOfSURLs->urlArray =
	    soap_malloc (&soap, nbsurls * sizeof(char *))) == NULL) {
		serrno = ENOMEM;
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}

	req.arrayOfSURLs->__sizeurlArray = nbsurls;
	for (i = 0; i < nbsurls; i++) {
		req.arrayOfSURLs->urlArray[i] = surls[i];
	}

	if (soap_call_ns1__srmReleaseFiles (&soap, srm_endpoint, "ReleaseFiles", &req, &rep)) {
		serrno = soap_error2serrno (&soap);
		soap_end (&soap);
		soap_done (&soap);
		return (-1);
	}
	reqstatp = rep.srmReleaseFilesResponse->returnStatus;
	repfs = rep.srmReleaseFilesResponse->arrayOfFileStatuses;
	if (! repfs || repfs->__sizestatusArray <= 0 || ! repfs->statusArray) {
		*filestatuses = NULL;
	} else if ((*filestatuses = malloc (repfs->__sizestatusArray * sizeof(struct dpm_filestatus)))) {
		*nbreplies = repfs->__sizestatusArray;
		for (i = 0; i < *nbreplies; i++) {
			(*filestatuses+i)->surl = strdup (repfs->statusArray[i]->surl);
			(*filestatuses+i)->status =
			    statuscode2dpmstatus (repfs->statusArray[i]->status->statusCode, 1);
			if (repfs->statusArray[i]->status->explanation)
				(*filestatuses+i)->errstring =
				    strdup (repfs->statusArray[i]->status->explanation);
			else
				(*filestatuses+i)->errstring = NULL;
		}
	}
	if (reqstatp->explanation)
		dpm_errmsg (NULL, "%s\n", reqstatp->explanation);
	status = statuscode2dpmstatus (reqstatp->statusCode, 0);
	soap_end (&soap);
	soap_done (&soap);
	return (status);
}
