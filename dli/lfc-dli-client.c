/*
 * Copyright (C) 2005 by CERN
 * All rights reserved
 */

#include <string.h>
#include <stdio.h>
#include <getopt.h>

#include "dliH.h"
#include "DataLocationInterface.nsmap"

#define DEFAULT_PORT "8085"

int helpflg = 0;
int verbose = 0;

main(int argc, char **argv)
{
	int c;
	char *data_type;
	char *endpoint = NULL;
	char lfc_endpoint[80];
	int errflg = 0;
	int i;
	static struct option longopts[] = {
		{"endpoint", required_argument, 0, 'e'},
		{"help", no_argument, &helpflg, 1},
		{"verbose", no_argument, &verbose, 1},
		{0, 0, 0, 0}
	};
	struct ns1__listReplicasResponse out;
	char *p;
	char *port = NULL;
	char *src_file;
	struct soap soap;
	int verbose = 0;

	opterr = 0;
	while((c = getopt_long(argc, argv, "e:hv", longopts, NULL)) != EOF) {
	  switch(c) {
	  case 'e':
	    endpoint = optarg;
	    break;
	  case 'h':
	    helpflg = 1;
	    break;
	  case 'v':
	    verbose = 1;
	    break;
	  case '?':
	    errflg++;
	    break;
	  default:
	    break;
	  }
	}

	if(optind >= argc)
	  errflg++;

	/* Try and create endpoint from LFC_HOST if not supplied */
	if(endpoint == NULL) {
	  if((p = getenv ("DLI_PORT")) != NULL) {
	    port = p;
	  }
	  if((p = getenv ("LFC_HOST")) == NULL) {
	    fprintf(stderr, "Error : No endpoint supplied, and LFC_HOST not set\n");
	    exit (1);
	  }
	  
	  sprintf(lfc_endpoint, "http://%s:%s/", p, port == NULL ? DEFAULT_PORT : port );
	  endpoint = lfc_endpoint;
	}

	if(errflg || helpflg) {
	  fprintf (stderr, "usage: %s %s%s", argv[0],
		   "[-v | --verbose] [-h | --help]\n",
		   "            [-e | --endpoint endpoint] logical_file\n");
	  exit (errflg ? 1 : 0);
	}

	src_file = argv[optind];
	if (strncmp (src_file, "guid:", 5) == 0)
		data_type = "guid";
	else if (strncmp (src_file, "lfn:", 4) == 0)
		data_type = "lfn";
	else {
		fprintf (stderr, "Bad URL syntax\n");
		exit (1);
	}

	if(verbose)
	  printf("Contacting endpoint : %s\n", endpoint);

	soap_init (&soap);
	if (soap_call_ns1__listReplicas (&soap, endpoint, "listReplicas",
	    data_type, src_file, &out)) {
		soap_print_fault (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < out.urlList->__size; i++)
		printf ("%s\n", out.urlList->__ptritem[i]);
	soap_end (&soap);
	exit (0);
}
