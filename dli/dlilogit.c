/*
 * Copyright (C) 2005-2011 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dlilogit.c,v $ $Revision$ $Date$ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <stdarg.h>
#include <syslog.h>
#include "Clogit.h"

dlilogit(char *func, char *msg, ...)
{
	va_list args;

	va_start (args, msg);
	Cvlogit (LOG_INFO, func, msg, args);
	va_end (args);
	return (0);
}
