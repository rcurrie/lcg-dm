#include "LcgdmDbPool.hh"

LcgdmDbPool *LcgdmDbPool::instance = 0;



LcgdmDbPool *LcgdmDbPool::GetInstance() {
                if (!instance)
                        instance = new LcgdmDbPool();

                return instance;
        }


void LcgdmDbPool::AddDbfd(void *dbfd) {
		dbfdcond.Lock();
		dbfdrepo.push(dbfd);
		dbfdcond.UnLock();
		dbfdcond.Signal();
	}

void *LcgdmDbPool::GetDbfd() {
		int r = 0;
		void *res = 0;
		dbfdcond.Lock();

		while (!r && (dbfdrepo.size() <= 0))
			r = dbfdcond.WaitMS(30000);

		if (!r) {
			if (dbfdrepo.size() > 0) {
				res = dbfdrepo.front();
				dbfdrepo.pop();
			}
		}

		dbfdcond.UnLock();

		return res;
	}
