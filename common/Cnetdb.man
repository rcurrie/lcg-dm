.\"
.\" $Id: Cnetdb.man,v 1.3 2008/02/13 17:29:10 dhsmith Exp $
.\"
.TH CNETDB "3" "$Date: 2008/02/13 17:29:10 $" "CASTOR" "Common Library Functions"
.SH NAME
\fBCnetdb\fP \- \fBC\fPASTOR netdb utilities
.SH SYNOPSIS
.B #include <Cnetdb.h>
.P
.BI "char *Cgetnetaddress(int " sock ", const void *" sa ", size_t " salen ", int *" skey ", const char **" numeric_out ", const char **" name_out ", int " flags ", int " cflags ");"

.SH DESCRIPTION

\fBCnetdb\fP provides several netdb like functions including Cgethostbyname, Cgethostbyaddr, Cgetservbyname,
Cgetnameinfo, Cgetaddrinfo, Cgai_strerror
which behave in a somewhat similar way to their system netdb counterparts. In addition Cnetdb provides a function
called Cgetnetaddress which is currently the only
one documented here.
.P
Cgetnetaddress returns a DNS domain name (often called a hostname) given either a network socket, in which case the hostname corresponds to the peer address
of the socket, or a sockaddr structure. The name will be returned as a numeric IP address and/or optionaly a DNS domain name.
.P
Exactly one of either \fBsock\fP or \fBsa\fP must be specified, the other should be set to -1 or NULL respectively. If
both are set the behaviour should be considered undefined and may change in the future. If \fBskey\fP
is NULL the memory for pointers returned by Cgetnetaddress will be dynamically allocated and it is the responsibility of the
caller to free them when done. If not NULL \fBskey\fP should point to an int which is initially set to a value of -1. The
memory used to store the results returned by Cgetnetaddress will be allocated internally and will be reused during subsequent
calls to Cgetnetaddress from the same thread and that share the same value of \fBskey\fP.
.P
If \fBnumeric_out\fP or \fBname_out\fP are not NULL the char pointer which they reference will be reset to a pointer to a
character array containig the numeric or domain name
address to be returned by the function. The output will be NULL if the function is unable to find an IP or domain name.
\fBflags\fP accepts the same bitmaped flags that can be set for getnameinfo(). \fBcflags\fP accepts bitmaped flags which are
unique to Cgetnetaddress.
.P
If \fBNI_NUMERICHOST\fP is specified in \fBflags\fP no attempt is made to obtain a domain name. If \fBNI_NAMEREQD\fP
is specified nothing is returned unless a domain name address can be determined. \fBNI_NUMERICHOST\fP and \fBNI_NAMEREQD\fP
are mutually exclusive. Other \fBflags\fP options may be set and will have the effect described for getnameinfo().
.P
\fBcflags\fP may take the bitmaped flags \fBCNA_WANTLOOPBACK\fP, \fBCNA_NOFWDLOOKUP\fP and \fBCNA_FWDLOOKUP\fP.
.P
By default if the address to be
returned is determined to be the loopback address any domain name returned will be that of the local machine.
If \fBCNA_WANTLOOPBACK\fP is specified any domain name returned will be that associated with the loopback address, usually
localhost.localdomain. 
.P
A forward lookup verification of the domain name may be optionally specified with the \fBCNA_FWDLOOKUP\fP flag.
The forward lookup means that one of the address to which a domain name resolves
must match the address passed to Cgetnetaddress. If the match failes the domain name will be considered to not
be available. By default, or if the option \fBCNA_NOFWDLOOKUP\fP is passed in \fBcflags\fP, the forward lookup check is
not performed or required. The behavior if both \fBCNA_FWDLOOKUP\fP and \fBCNA_NOFWDLOOKUP\fP are specified is undefined.
.SH RETURN VALUE
Cgetnetaddress() returns a pointer to the domain name, if available, or otherwise to the numeric IP address, if available.
If neither is available NULL is returned.
.SH SEE ALSO
\fBgethostbyname\fP(3), \fBgethostbyaddr\fP(3), \fBgetservbyname\fP(3), \fBgetnameinfo\fP(3),
\fBgetaddrinfo\fP(3), \fBgai_strerror\fP(3)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch> and
\fBDPM\fP Team <hep-service-dpm@cern.ch>
