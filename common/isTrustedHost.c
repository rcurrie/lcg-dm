/*
 * Copyright (C) 2005-2008 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: isTrustedHost.c,v $ $Revision: 1.8 $ $Date: 2008/01/31 11:41:39 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <stdio.h>
#include <string.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <netdb.h>
#include <netinet/in.h>
#endif
#include "Castor_limits.h"
#include "Cnetdb.h"

#ifndef _WIN32
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
#define strtok(X,Y) strtok_r(X,Y,&last)
#endif /* _REENTRANT || _THREAD_SAFE */
#endif /* _WIN32 */

extern int DLL_DECL CDoubleDnsLookup _PROTO((int, char *));

/* Check in configuration file if a given hostname
   is trusted for a given operation
   Example of configuration file entries:
   RFIOD	RTRUST	host1 host2
   DPNS		TRUST	host3 host4
 */

int DLL_DECL
isTrustedHost2 (char *hostname, char *localhost, char *localdomain, char *svc_name, char *perm)
{
	char fqn[CA_MAXHOSTNAMELEN+1];
	char *getconfent();
	int l;
#ifndef _WIN32
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
	char *last = NULL;
#endif
#endif
	char *p;
	char *q;

	if (! hostname)
		return (0);
	if (localhost && strcmp (hostname, localhost) == 0)
		return (1);
	if ((p = getconfent (svc_name, perm, 1)) == NULL)
		return (0);
	l = strlen (localdomain);
	for (q = strtok (p, "\t "); q; q = strtok (NULL, "\t ")) {
		if (strcmp ("*", q) == 0)
			return (1); 
		if (strcmp (hostname, q) == 0)
			return (1);
		if (strchr (q, '.'))
			continue;
		if (strlen (q) + l + 1 > CA_MAXHOSTNAMELEN)
			continue;
		sprintf (fqn, "%s.%s", q, localdomain);
		if (strcmp (hostname, fqn) == 0)
			return (1);
	}
	return (0);
}

/* Check in configuration file if a client connected on the socket s
   is trusted for a given operation
   Example of configuration file entries:
   RFIOD	RTRUST	host1 host2
   DPNS		TRUST	host3 host4
 */

int DLL_DECL
isTrustedHost (int s, char *localhost, char *localdomain, char *svc_name, char *perm)
{
	char hostname[CA_MAXHOSTNAMELEN+1];

	if (CDoubleDnsLookup (s, hostname))
		return (0);

	return (isTrustedHost2 (hostname, localhost, localdomain, svc_name, perm));
}
