/*
 * $Id: Cthread_env.c,v 1.0 2011/11/07 Alejandro Alvarez $
 */

/*
 * Copyright (C) 2011 by CERN/IT/GT/DMS
 * All rights reserved
 */
#include "Cthread_env.h"
#include <stdlib.h>
#include <string.h>


/* Structure to hold key/value pairs */
struct Cthread_env_entry {
  char key  [64];
  char value[1024]; /* Should be able to hold a path */
};

/* Number of entries */
#define CTHREAD_ENV_NENTRIES 20

/* Key used for per-thread memory */
static int Cthread_env_key = -1;

int DLL_DECL Cthread_setenv (const char *key, const char *value, int replace)
{
  struct Cthread_env_entry *env_register;
  int                       i, insert;

  if (Cglobals_get(&Cthread_env_key,
                   (void **)&env_register,
                   sizeof(struct Cthread_env_entry) * CTHREAD_ENV_NENTRIES) < 0)
  {
    return -1;
  }

  /* Search for a match or for first free */
  insert = -1;
  for (i = 0; i < CTHREAD_ENV_NENTRIES && insert == -1; ++i) {
    if (strcmp(env_register[i].key, key) == 0) {
      if (replace)
        insert = i;
      else
        return 0; /* Stop here */
    }
    else if (env_register[i].key[0] == '\0')
      insert = i;
  }

  if (insert < 0)
    return -1;

  /* Copy and return */
  strncpy(env_register[insert].key,   key,   64);
  strncpy(env_register[insert].value, value, 1024);

  return 0;
}

char* DLL_DECL Cthread_getenv (const char *key)
{
  struct Cthread_env_entry *env_register;
  int                       i;

  if (Cglobals_get(&Cthread_env_key,
                   (void **)&env_register,
                   sizeof(struct Cthread_env_entry) * CTHREAD_ENV_NENTRIES) < 0)
  {
    return NULL;
  }

  
  for (i = 0; i < CTHREAD_ENV_NENTRIES && env_register[i].key[0] != '\0'; ++i) {
    if (strcmp(env_register[i].key, key) == 0) {
      return env_register[i].value;
    }
  }

  /* Fall-back to the environment */
  return getenv(key);
}

