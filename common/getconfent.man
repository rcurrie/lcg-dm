.\" @(#)@(#)$RCSfile: getconfent.man,v $ $Revision: 1.1 $ $Date: 2005/03/29 09:27:19 $ CERN IT-PDP/DC Frederic Hemmer
.\" Copyright (C) 1990-2000 by CERN/IT/PDP/DC
.\" All rights reserved
.\"
.TH GETCONFENT 3 "$Date: 2005/03/29 09:27:19 $" CASTOR "Common Library Functions"
.SH NAME
getconfent, getconfent_r \- Get configuration entry
.SH SYNOPSIS
.nf
.LP
.BI "char * getconfent ("
.br
.BI "               const char *" category , 
.br
.BI "               const char *" name , 
.br
.BI "               int " flags );
.PP
.BI "char * getconfent_r ("
.br
.BI "               const char *" category ,
.br
.BI "               const char *" name ,
.br
.BI "               int " flags , 
.br
.BI "               char *" buffer ,
.br
.BI "               int " bufsiz );
.fi
.IX  "getconfent function"  ""  "\fLget\fP \(em configuration entry"
.SH DESCRIPTION
.LP
.B getconfent(\|)
get the
.I name
entry from the  configuration file.
Every entry in the configuration file is categorized. The
.I category
parameter is its specifier. The
.B getconfent_r(\|)
function is a re-entrant version of 
.B getconfent(\|)
using the passed
.I buffer
parameter instead of an internal static buffer. 
.PP
If threads are created usint the
.B Cthread_create(\|)
interface, it is not mandatory to call 
.B getconfent_r(\|)
in order to assure thread-safe code. In that case
.B getconfent(\|)
calls the re-entrant version internally using a buffer allocated
in thread local storage. 
.SH "RETURN VALUE"
.B getconfent(\|)
and
.B getconfent_r(\|)
return NULL if the entry is not found. Otherwise,
it returns the matched entry.
If
.B flags
is not null, the return value is a pointer to the full string matched
in the configuration file. Otherwise, only the pointer to the first
blank separated token is returned.
.SH "ERRORS"
.PP
If the
.B getconfent(\|), getconfent_r(\|)
function fail, 
.B serrno
may be set to one of the following values:
.TP
.B SENOCONFIG
configuration file not found.
.SH "SEE ALSO"
.BR shift.conf(4),
.BR Cthread_create(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
