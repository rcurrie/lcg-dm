/*
 * Copyright (C) 1999-2007 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: u64subr.c,v $ $Revision$ $Date$ CERN IT-PDP/DM Jean-Philippe Baud";
#endif /* not lint */
 
#include <stdio.h>       /* For sprintf */
#include <string.h>      /* For memset */
#include <ctype.h>       /* For isspace and al. */
#include <osdep.h>
#include "u64subr.h"

/* strtoi64 - convert a string to an signed 64 bits integer */
signed64 DLL_DECL strtoi64(str)
	CONST char *str;
{
	char *p = (char *) str;
	signed64 i64 = 0;
	int negative = 0;

	while (isspace (*p)) p++;	/* skip leading spaces */
	if (*p == '-') {
		negative++;
		p++;
	}
	while (*p) {
		if (! isdigit (*p)) break;
			i64 = i64 * 10 + (*p++ - '0');
	}
	return (negative ? -i64 : i64);
}

/* i64tostr - convert a signed 64 bits integer to a string */
/*	buf must be long enough to contain the result
 *	if fldsize <= 0, the result is left adjusted in buf
 *	if fldsize > 0, the result is right adjusted
 *		leading spaces are inserted if needed
 */
char DLL_DECL *i64tostr(i64, buf, fldsize)
	signed64 i64;
	char *buf;
	int fldsize;
{
	char *p;
	u_signed64 u64;

	if (i64 >= 0)
		return (u64tostr ((u_signed64)i64, buf, fldsize));
	u64 = -i64;
	if (fldsize <= 0) {
		*buf = '-';
		(void) u64tostr (u64, buf + 1, fldsize);
	} else {
		(void) u64tostr (u64, buf, fldsize);
		p = buf;
		while (*p == ' ') p++;
		if (p > buf)
			*(p - 1) = '-';
	}
	return (buf);
}

/* strtou64 - convert a string to an unsigned 64 bits integer */
u_signed64 DLL_DECL strtou64(str)
	CONST char *str;
{
	char *p = (char *) str;
	u_signed64 u64 = 0;

	while (isspace (*p)) p++;	/* skip leading spaces */
	while (*p) {
		if (! isdigit (*p)) break;
			u64 = u64 * 10 + (*p++ - '0');
	}
	return (u64);
}

/* u64tostr - convert an unsigned 64 bits integer to a string */
/*	buf must be long enough to contain the result
 *	if fldsize <= 0, the result is left adjusted in buf
 *	if fldsize > 0, the result is right adjusted
 *		leading spaces are inserted if needed
 */
char DLL_DECL *u64tostr(u64, buf, fldsize)
	u_signed64 u64;
	char *buf;
	int fldsize;
{
	int n;
	u_signed64 t64;
	char *p;
	char tmpbuf[21];

	p = tmpbuf + 20;
	*p-- = '\0';

	do {
		t64 = u64 / 10;
		*p-- = '0' + (u64 - t64 * 10);
	} while ((u64 = t64));

	if (fldsize <= 0) {
		strcpy (buf, p + 1);
	} else {
		n = fldsize - (tmpbuf + 19 - p);
		memset (buf, ' ', n);
		strcpy (buf + n, p + 1);
	}
		
	return (buf);
}

/* strutou64 - convert a string with unit to an unsigned 64 bits integer */
u_signed64 DLL_DECL strutou64(str)
	CONST char *str;
{
	char *p = (char *) str;
	u_signed64 u64 = 0;

	while (isspace (*p)) p++;	/* skip leading spaces */
	while (*p) {
		if (! isdigit (*p)) break;
			u64 = u64 * 10 + (*p++ - '0');
	}
	if (*p && ! *(p + 1))
		if (*p == 'k') u64 *= ONE_KB;
		else if (*p == 'M') u64 *= ONE_MB;
		else if (*p == 'G') u64 *= ONE_GB;
		else if (*p == 'T') u64 *= ONE_TB;
		else if (*p == 'P') u64 *= ONE_PB;
	return (u64);
}

/* u64tostru - convert an unsigned 64 bits integer to a string  with unit*/
/*	buf must be long enough to contain the result
 *	if fldsize <= 0, the result is left adjusted in buf
 *	if fldsize > 0, the result is right adjusted
 *		leading spaces are inserted if needed
 */
char DLL_DECL *u64tostru(u64, buf, fldsize)
	u_signed64 u64;
	char *buf;
	int fldsize;
{
	float fnum;
	int inum;
	int n;
	signed64 t64;
	char *p;
	char tmpbuf[21];
	char unit;

	t64 = u64;
	if (u64 > ONE_PB) {
		fnum = (double) t64 / (double) ONE_PB;
		unit = 'P';
	} else if (u64 > ONE_TB) {
		fnum = (double) t64 / (double) ONE_TB;
		unit = 'T';
	} else if (u64 > ONE_GB) {
		fnum = (double) t64 / (double) ONE_GB;
		unit = 'G';
	} else if (u64 > ONE_MB) {
		fnum = (double) t64 / (double) ONE_MB;
		unit = 'M';
	} else if (u64 > ONE_KB) {
		fnum = (double) t64 / (double) ONE_KB;
		unit = 'k';
	} else {
		inum = (int) u64;
		unit = ' ';
	}
	if (unit != ' ')
		sprintf (tmpbuf, "%.2f%c", fnum, unit);
	else
		sprintf (tmpbuf, "%d", inum);

	if (fldsize <= 0) {
		strcpy (buf, tmpbuf);
	} else {
		n = fldsize - strlen (tmpbuf);
		memset (buf, ' ', n);
		strcpy (buf + n, tmpbuf);
	}
		
	return (buf);
}

u_signed64 DLL_DECL strsitou64(str)
	CONST char *str;
{
	char *p = (char *) str;
	u_signed64 u64 = 0;

	while (isspace (*p)) p++;	/* skip leading spaces */
	while (*p) {
		if (! isdigit (*p)) break;
			u64 = u64 * 10 + (*p++ - '0');
	}
	if (*p && ! *(p + 1))
		if (*p == 'k') u64 *= 1000;
		else if (*p == 'M') u64 *= 1000000;
		else if (*p == 'G') u64 *= 1000000000;
		else if (*p == 'T') u64 *= CONSTLL(1000000000000);
		else if (*p == 'P') u64 *= CONSTLL(1000000000000000);
	return (u64);
}

char DLL_DECL *u64tostrsi(u64, buf, fldsize)
	u_signed64 u64;
	char *buf;
	int fldsize;
{
	float fnum;
	int inum;
	int n;
	signed64 t64;
	char *p;
	char tmpbuf[21];
	char unit;

	t64 = u64;
	if (u64 > CONSTLL(1000000000000000)) {
		fnum = (double) t64 / (double) CONSTLL(1000000000000000);
		unit = 'P';
	} else if (u64 > CONSTLL(1000000000000)) {
		fnum = (double) t64 / (double) CONSTLL(1000000000000);
		unit = 'T';
	} else if (u64 > 1000000000) {
		fnum = (double) t64 / (double) 1000000000;
		unit = 'G';
	} else if (u64 > 1000000) {
		fnum = (double) t64 / (double) 1000000;
		unit = 'M';
	} else if (u64 > 1000) {
		fnum = (double) t64 / (double) 1000;
		unit = 'k';
	} else {
		inum = (int) u64;
		unit = ' ';
	}
	if (unit != ' ')
		sprintf (tmpbuf, "%.2f%c", fnum, unit);
	else
		sprintf (tmpbuf, "%d", inum);

	if (fldsize <= 0) {
		strcpy (buf, tmpbuf);
	} else {
		n = fldsize - strlen (tmpbuf);
		memset (buf, ' ', n);
		strcpy (buf + n, tmpbuf);
	}
		
	return (buf);
}
