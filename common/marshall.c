/*
 * Copyright (C) 2000-2001 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: marshall.c,v $ $Revision: 1.1 $ $Date: 2005/03/29 09:27:19 $ CERN IT-PDP/DM Jean-Philippe Baud";
#endif /* not lint */

/*
 * marshall.c - wrappers on top of marshall macros
 */

#include <string.h> 
#include "marshall.h"
#include "osdep.h"

int DLL_DECL
_unmarshall_STRINGN(ptr, str, n)
char **ptr;
char *str;
int n;
{
	char *p;

	(void) strncpy (str, *ptr, n);
	if ((p = memchr (str, 0, n)) != NULL) {
		*ptr += (p - str + 1);
		return (0);
	}
	*(str + n - 1) = '\0';
	*ptr += strlen(*ptr) + 1;
	return (-1);
}

int DLL_DECL
_unmarshall_NSTRINGN(ptr, end, str, n)
char **ptr;
char *end;
char *str;
int n;
{
	size_t l;
	char *p;

	if (*ptr >= end || n<=0) {
		if (n>0)
			*str = '\0';
		return (-1);
	}
	if ((p = memchr (*ptr, 0, end - *ptr)) == NULL) {
		*str = '\0';
		*ptr = end;
		return (-1);
	} else p++;
	l = p - *ptr;
	if (l > (size_t)n) l = n;
	(void) memcpy (str, *ptr, l);
	*ptr = p;
	if (!str[l-1])
		return (0);
	str[l-1] = '\0';
	return (-1);
}
