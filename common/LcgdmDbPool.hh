#ifndef LCGDM_TCP_SERVER_H
#define LCGDM_TCP_SERVER_H


//
// LcgdmTcpServer
// Class that instantiates a TCP server for the various lcgdm daemons
// Gives also the basic functionalities of a waiting pool for
// the "thip" contexts used in lcgdm to store the context of a thread
//
// Author: Fabrizio Furano (CERN, Sept 2011)
//
//



#include "UtilsPthread.hh"
#include <queue>


class LcgdmDbPool {
private:

	// This is our simple but effective db pool
	// We store the pointers to the thips as opaque things
	// as we don't want to incorporate those definitions here
	// Has to be synced by a condvar, this is a waiting pool!
        std::queue<void *> dbfdrepo;
	UptCondVar dbfdcond;

	UptMutex mtx;
	int nclients;

	static LcgdmDbPool *instance;
protected:
	// We treat thip as a completely opaque pointer here.
	LcgdmDbPool():						   dbfdcond(0) {}
public:
   
	// We treat thip as a completely opaque pointer here.
	// We give to this class the behavior of a singleton
	static LcgdmDbPool *GetInstance();

	virtual ~LcgdmDbPool() {};

	// Puts a dbfd pointer in the pool
	void AddDbfd(void *dbfd);

	// Retrieves a dbfd pointer from the pool
	// Before failing, wait for some time for a dbfd to be available
	void *GetDbfd();
};

#endif
