/******************************************************************************
 *UtilsPthread
 *A set of utilities that wrap the pthread library for use with C++
 *By Fabrizio Furano (CERN IT/GT, Sept 2011)
 *
 *Originally taken from XrdSysPthread, by A.Hanushevsky, with a few additions
 *by Fabrizio Furano
 ******************************************************************************/


 
#include <errno.h>
#include <pthread.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#else
#undef ETIMEDOUT       // Make sure that the definition from Winsock2.h is used ... 
#include <Winsock2.h>
#include <time.h>
#include "XrdSys/XrdWin32.hh"
#endif
#include <sys/types.h>

#include "UtilsPthread.hh"



struct UptThreadArgs
       {
        pthread_key_t numKey;
        const char   *tDesc;
        void         *(*proc)(void *);
        void         *arg;

        UptThreadArgs(pthread_key_t nk, const char *td,
                         void *(*p)(void *), void *a)
                        {numKey=nk; tDesc=td, proc=p; arg=a;}
       ~UptThreadArgs() {}
       };

/******************************************************************************/
/*                           G l o b a l   D a t a                            */
/******************************************************************************/
  
pthread_key_t UptThread::threadNumkey;

size_t        UptThread::stackSize = 0;

int           UptThread::initDone  = 0;

/******************************************************************************/
/*             T h r e a d   I n t e r f a c e   P r o g r a m s              */
/******************************************************************************/
  
extern "C"
{
void *UptThread_Xeq(void *myargs)
{
   UptThreadArgs *ap = (UptThreadArgs *)myargs;
   unsigned long myNum;
   void *retc;

#if   defined(__linux__)
   myNum = static_cast<unsigned int>(getpid());
#elif defined(__solaris__)
   myNum = static_cast<unsigned int>(pthread_self());
#elif defined(__macos__)
   myNum = static_cast<unsigned int>(pthread_mach_thread_np(pthread_self()));
#else
   static UptMutex   numMutex;
   static unsigned long threadNum = 1;
   numMutex.Lock(); threadNum++; myNum = threadNum; numMutex.UnLock();
#endif

   pthread_setspecific(ap->numKey, reinterpret_cast<const void *>(myNum));

   retc = ap->proc(ap->arg);
   delete ap;
   return retc;
}
}
  

  
int UptCondVar::Wait()
{
 int retc;

// Wait for the condition
//
   if (relMutex) Lock();
   retc = pthread_cond_wait(&cvar, &cmut);
   if (relMutex) UnLock();
   return retc;
}

/******************************************************************************/
  
int UptCondVar::Wait(int sec)
{
 struct timespec tval;
 int retc;

// Get the mutex before calculating the time
//
   if (relMutex) Lock();

// Simply adjust the time in seconds
//
   tval.tv_sec  = time(0) + sec;
   tval.tv_nsec = 0;

// Wait for the condition or timeout
//
   do {retc = pthread_cond_timedwait(&cvar, &cmut, &tval);}
   while (retc && (retc != ETIMEDOUT));

   if (relMutex) UnLock();
   return retc == ETIMEDOUT;
}

/******************************************************************************/
/*                                W a i t M S                                 */
/******************************************************************************/
  
int UptCondVar::WaitMS(int msec)
{
 int sec, retc, usec;
 struct timeval tnow;
 struct timespec tval;

// Adjust millseconds
//
   if (msec < 1000) sec = 0;
      else {sec = msec / 1000; msec = msec % 1000;}
   usec = msec * 1000;

// Get the mutex before getting the time
//
   if (relMutex) Lock();

// Get current time of day
//
   gettimeofday(&tnow, 0);

// Add the second and microseconds
//
   tval.tv_sec  = tnow.tv_sec  +  sec;
   tval.tv_nsec = tnow.tv_usec + usec;
   if (tval.tv_nsec > 1000000)
      {tval.tv_sec += tval.tv_nsec / 1000000;
       tval.tv_nsec = tval.tv_nsec % 1000000;
      }
   tval.tv_nsec *= 1000;


// Now wait for the condition or timeout
//
   do {retc = pthread_cond_timedwait(&cvar, &cmut, &tval);}
   while (retc && (retc != ETIMEDOUT));

   if (relMutex) UnLock();
   return retc == ETIMEDOUT;
}
 









  
#ifdef __macos__

int UptSemaphore::CondWait()
{
   int rc;

// Get the semaphore only we can get it without waiting
//
   semVar.Lock();
   if ((rc = (semVal > 0) && !semWait)) semVal--;
   semVar.UnLock();
   return rc;
}

/******************************************************************************/
/*                                  P o s t                                   */
/******************************************************************************/
  
void UptSemaphore::Post()
{
// Add one to the semaphore counter. If we the value is > 0 and there is a
// thread waiting for the sempagore, signal it to get the semaphore.
//
   semVar.Lock();
   semVal++;
   if (semVal && semWait) semVar.Signal();
   semVar.UnLock();
}

/******************************************************************************/
/*                                  W a i t                                   */
/******************************************************************************/
  
void UptSemaphore::Wait()
{

// Wait until the sempahore value is positive. This will not be starvation
// free is the OS implements an unfair mutex;
//
   semVar.Lock();
   if (semVal < 1 || semWait)
      while(semVal < 1)
           {semWait++;
            semVar.Wait();
            semWait--;
           }

// Decrement the semaphore value and return
//
   semVal--;
   semVar.UnLock();
}
#endif
 












/******************************************************************************/
/*                        T h r e a d   M e t h o d s                         */
/******************************************************************************/
/******************************************************************************/
/*                                d o I n i t                                 */
/******************************************************************************/
  
void UptThread::doInit()
{
   static UptMutex initMutex;

   initMutex.Lock();
   if (!initDone)
      {pthread_key_create(&threadNumkey, 0);
       pthread_setspecific(threadNumkey, (const void *)1);
       initDone = 1;
      }
   initMutex.UnLock();
}
  
/******************************************************************************/
/*                                   R u n                                    */
/******************************************************************************/

int UptThread::Run(pthread_t *tid, void *(*proc)(void *), void *arg, 
                      int opts, const char *tDesc)
{
   pthread_attr_t tattr;
   UptThreadArgs *myargs;

   if (!initDone) doInit();
   myargs = new UptThreadArgs(threadNumkey, tDesc, proc, arg);

   pthread_attr_init(&tattr);
   if (  opts & UPTTHREAD_BIND)
      pthread_attr_setscope(&tattr, PTHREAD_SCOPE_SYSTEM);
   if (!(opts & UPTTHREAD_HOLD))
      pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);
   if (stackSize)
       pthread_attr_setstacksize(&tattr, stackSize);
   return pthread_create(tid, &tattr, UptThread_Xeq,
   			 static_cast<void *>(myargs));
}

/******************************************************************************/
/*                                  W a i t                                   */
/******************************************************************************/
  
int UptThread::Wait(pthread_t tid)
{
   int retc, *tstat;
   if ((retc = pthread_join(tid, reinterpret_cast<void **>(&tstat)))) return retc;
   return *tstat;
}



/******************************************************************************/
/*                         X r d S y s R e c M u t e x                        */
/******************************************************************************/

UptRecMutex::UptRecMutex() {

   int rc;
   pthread_mutexattr_t attr;

   rc = pthread_mutexattr_init(&attr);

   if (!rc) {
      rc = pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
      if (!rc)
	 pthread_mutex_init(&cs, &attr);
   }

   pthread_mutexattr_destroy(&attr);

 }












//_____________________________________________________________________________
void * UptThreadFancyDispatcher(void * arg)
{
   // This function is launched by the thread implementation. Its purpose
   // is to call the actual thread body, passing to it the original arg and
   // a pointer to the thread object which launched it.

   UptThreadFancy::UptThreadFancyArgs *args = (UptThreadFancy::UptThreadFancyArgs *)arg;

   args->threadobj->SetCancelDeferred();
   args->threadobj->SetCancelOn();

   if (args->threadobj->ThreadFunc)
      return args->threadobj->ThreadFunc(args->arg, args->threadobj);

   return 0;

}


//_____________________________________________________________________________
int UptThreadFancy::MaskSignal(int snum, bool block)
{
   // Modify masking for signal snum: if block is true the signal is blocked,
   // else is unblocked. If snum <= 0 (default) all the allowed signals are
   // blocked / unblocked.
#ifndef WIN32
   sigset_t mask;
   int how = block ? SIG_BLOCK : SIG_UNBLOCK;
   if (snum <= 0)
      sigfillset(&mask);
      else sigaddset(&mask, snum);
   return pthread_sigmask(how, &mask, 0);
#else
   return 0;
#endif
}


