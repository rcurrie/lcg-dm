/*
 * Copyright (C) 1991-2008 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: getifnam.c,v $ $Revision: 1.5 $ $Date: 2008/02/15 07:40:39 $ CERN/IT/PDP/DM Frederic Hemmer";
#endif /* not lint */

/* getifnam.c   Get connected socket interface name                     */

#include <stdio.h>                      /* Standard input/output        */
#include <sys/types.h>                  /* Standard data types          */
#if !defined(_WIN32)
#include <sys/socket.h>                 /* Socket interface             */
#include <netinet/in.h>                 /* Internet data types          */
#else
#include <winsock2.h>
#endif
#if defined(SOLARIS)
#include <sys/sockio.h>
#endif
#include <errno.h>                      /* Error numbers                */
#include <serrno.h>                     /* Special error numbers        */
#include <log.h>                        /* Generalized error logger     */
#if defined(_AIX)
#include <sys/time.h>                   /* needed for if.h              */
#endif /* AIX */
#include <trace.h>                      /* tracing definitions          */
#include <string.h>                     /* For strlen                   */
#include <Cglobals.h>                   /* Cglobals prototypes          */

#include <net.h>                        /* Networking specifics         */

static int ifname_key = -1;             /* Key to interface name global */

EXTERN_C char *getifnam_sa(const struct sockaddr *sa, char *ifname, size_t ifnamelen);

/*
 *  Identifies the local interface being used for the socket descriptor s
 */

char DLL_DECL  *getifnam_r(s,ifname,ifnamelen)
SOCKET     s;
char    *ifname;
size_t  ifnamelen;
{
    struct sockaddr_storage ss;
#if defined(_WIN32)
    int addrlen;
#else
    socklen_t addrlen;
#endif

    if ( ifname == NULL || ifnamelen <= 0) return(NULL);
    INIT_TRACE("COMMON_TRACE");
    TRACE(1,"getifnam_r", "getifnam_r(%d) entered",s);

    addrlen = sizeof(ss);
    if (getsockname(s, (struct  sockaddr *)&ss, &addrlen) == SOCKET_ERROR ) {
        TRACE(2,"getifnam_r", "getsockname returned %d", errno);
        log(LOG_ERR, "getsockname: %s\n",strerror(errno));
        END_TRACE();
        return(NULL);
    }

    if (getifnam_sa((struct sockaddr *)&ss, ifname, ifnamelen) == NULL) {
#if defined(_WIN32)
        static char dummy_ifce[4]="???";
        TRACE(2,"getifnam_r", "returning dummy value on windows");
        END_TRACE();
        return (dummy_ifce);
#else
        serrno = SEBADIFNAM;
        TRACE(2,"getifnam_r", "returning NULL");
        END_TRACE();
        return((char *) NULL);
#endif
    }

    TRACE(2,"getifnam_r", "returning %s", ifname);
    END_TRACE();

    return(ifname);
}

char DLL_DECL *getifnam(s)
SOCKET s;
{
    char *ifname = NULL;
    size_t ifnamelen = 16;

    Cglobals_get(&ifname_key,(void **)&ifname,ifnamelen);

    return(getifnam_r(s,ifname,ifnamelen));
}
