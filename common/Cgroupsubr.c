#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include "Csnprintf.h"

gid_t *
Cdecode_groups(char *groups, int *nbgids)
{
	char *dp;
	gid_t *gids;
	int i = 0;
	char *p;

	p = groups;
	while (*p) {
		if (*p == ',') i++;
		p++;
	}
	i++;
	if ((gids = calloc (i, sizeof(gid_t))) == NULL)
		return (NULL);
	*nbgids = i;
	i = 0;
	p = groups;
	while (1) {
		gids[i++] = strtol (p, &dp, 10);
		if ((dp = strchr (dp, ',')) == NULL) break;
		p = dp + 1;
	}
	return (gids);
}

char *
Cencode_groups(int nbgids, gid_t *gids, char *groups, size_t bufsize)
{
	int i;
	int n;
	char *p;

	p = groups;
	for (i = 0; i < nbgids; i++) {
		if (p != groups) {
			if (1 >= bufsize - (p - groups))
				return (NULL);
			*p++ = ',';
		}
		n = Csnprintf (p, bufsize - (p - groups), "%d", gids[i]);
		if (n >= bufsize - (p - groups))
			return (NULL);
		p += n;
	}
	return (groups);
}

Cgroupmatch(gid_t gid, int nbgids, gid_t *gids)
{
	int i;

	for (i = 0; i < nbgids; i++) {
		if (gid == gids[i])
			return (1);
	}
	return (0);
}
