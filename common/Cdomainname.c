/*
 * Copyright (C) 2002-2005 by CERN/IT/DS/HSM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cdomainname.c,v $ $Revision: 1.4 $ $Date: 2008/02/15 07:40:38 $ CERN IT-DS/HSM Jean-Philippe Baud";
#endif /* not lint */

#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#endif
#include "Castor_limits.h"
#include "Cnetdb.h"
#include "serrno.h"
#include "Cmutex.h"

static int _domainnamefound = 0;
static char _domainname[CA_MAXHOSTNAMELEN+1];

/* Cdomainname - get domain name */

int DLL_DECL Cdomainname(char *name, int namelen)
{
	char hostname[CA_MAXHOSTNAMELEN+1];
	struct addrinfo hints, *ai, *aitop;
	int gaierrno;
	char *p;

#ifndef _WIN32
	FILE *fd;
	/*
	 * try looking in /etc/resolv.conf
	 * putting this here and assuming that it is correct, eliminates
	 * calls to gethostbyname, and therefore DNS lookups. This helps
	 * those on dialup systems.
	 */
	if ((fd = fopen ("/etc/resolv.conf", "r")) != NULL) {
		char line[300];
		char *q;
		while (fgets (line, sizeof(line), fd) != NULL) {
			if (strncmp (line, "domain", 6) == 0 ||
			    strncmp (line, "search", 6) == 0) {
				p = line + 6;
				while (*p == ' ' || *p == '\t')
					p++;
				if (*p == '\0' || *p == '\n')
					continue;
				fclose (fd);
				q = p + strlen (p) - 1;
				if (*q == '\n')
					*q = '\0';
				q = p;
				while (*q != '\0' && *q != ' ' && *q != '\t')
					q++;
				if (*q)
					*q = '\0';
				if (strlen (p) > namelen) {
					serrno = EINVAL;
					return (-1);
				}
				strcpy (name, p);
				return (0);
			}
		}
		fclose (fd);
	}
#endif

	Cmutex_lock(&_domainnamefound, -1);
	if (_domainnamefound) {
		strcpy(name, _domainname);
        	Cmutex_unlock(&_domainnamefound);
		return (0);
	}
        Cmutex_unlock(&_domainnamefound);

	/* Try gethostname */

	gethostname (hostname, CA_MAXHOSTNAMELEN+1);
	memset(&hints, '\0', sizeof(hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags |= AI_CANONNAME;
	gaierrno = Cgetaddrinfo(hostname, NULL, &hints, &aitop);
	if (gaierrno != 0) aitop = NULL;
	for(ai=aitop;ai;ai=ai->ai_next) {
		gaierrno = -1;
		if (ai == aitop && ai->ai_canonname) {
			struct addrinfo *res;
			memset(&hints, '\0', sizeof(hints));
			hints.ai_family = PF_UNSPEC;
			hints.ai_flags |= AI_NUMERICHOST;
			gaierrno = Cgetaddrinfo(ai->ai_canonname, NULL, &hints, &res);
			if (gaierrno == 0) {
				freeaddrinfo(res);
				gaierrno = -1;
			} else {
				if (strchr(ai->ai_canonname,'.') != NULL &&
						strlen(ai->ai_canonname)<sizeof(hostname)) {
					strcpy(hostname, ai->ai_canonname);
					gaierrno = 0;
				}
			}
		}

		if (gaierrno != 0)			
			gaierrno = Cgetnameinfo(ai->ai_addr, ai->ai_addrlen, hostname, 
							sizeof(hostname), NULL, 0, NI_NAMEREQD);
		if (gaierrno != 0) continue;
		if ((p = strchr(hostname,'.'))) {
			p++;
			freeaddrinfo(aitop);
			if (strlen (p) > namelen) {
				serrno = EINVAL;
				return (-1);
			}
			strcpy (name, p);
			if (strlen(name)<=CA_MAXHOSTNAMELEN) {
				Cmutex_lock(&_domainnamefound, -1);
				strcpy(_domainname, name);
				_domainnamefound = 1;
				Cmutex_unlock(&_domainnamefound);
			}
			return (0);
		}
	}
	if (aitop)
		freeaddrinfo(aitop);

	serrno = SEINTERNAL;
	return (-1);
}
