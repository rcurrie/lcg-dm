
extern "C" {
#include "LcgdmDbPool_C.h"
}

#include "LcgdmDbPool.hh"


extern "C" void LcgdmDbPool_AddDbfd(void *dbfd){
  LcgdmDbPool::GetInstance()->AddDbfd(dbfd);
};

extern "C" void *LcgdmDbPool_GetDbfd(){
  return LcgdmDbPool::GetInstance()->GetDbfd();
};
