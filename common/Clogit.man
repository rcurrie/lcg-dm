.\" @(#)$RCSfile: Clogit.man$ $Revision$ $Date$ CERN IT-GT Jean-Philippe Baud
.\" Copyright (C) 2011 by CERN/IT/GT
.\" All rights reserved
.\"
.TH CLOGIT 3 "$Date$" CASTOR "Common Library Functions"
.SH NAME
Clogit \- log server messages in local log or in system logger
.SH SYNOPSIS
\fB#include "Clog.h"\fR
.sp
.BI "int Cinitlog (char *" cmd ,
.BI "char *" logfile ")"
.sp
.BI "int Clogit (int " level ,
.BI "char *" func ,
.BI "char *" msg ,
.B "...)"
.sp
.BI "int Cvlogit (int " level ,
.BI "char *" func ,
.BI "char *" msg ,
.BI "va_list " ap )
.SH DESCRIPTION
.B Cinitlog
initializes the server logging routines.
.sp
.B Clogit
logs server messages in a local log or in the system logger.
.sp
.B Cvlogit
is the same as
.B Clogit
but takes a va_list instead of a variable number of arguments.
.TP
.I cmd
specifies a string to be prepended to the syslog messages.
.TP
.I logfile
specifies a path for the server log file. If set to
.BR syslog ,
the system logger will be used.
By default, only messages with level at least as important as
LOG_INFO will be logged. The level threshold can be changed with the environment
variable LOG_PRIORITY.
.TP
.I level
is associated with the message.
The possible values, in order of decreasing importance are:
.RS
.TP
.B LOG_EMERG
.TP
.B LOG_ALERT
.TP
.B LOG_CRIT
.TP
.B LOG_ERR
.TP
.B LOG_WARNING
.TP
.B LOG_NOTICE
.TP
.B LOG_INFO
.TP
.B LOG_DEBUG
.RE
.TP
.I func
is the name of the function that issues the message.
.TP
.I msg
is a format as in printf.
.SH RETURN VALUE
These routines return 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I logfile
is a NULL pointer.
.TP
.B ENAMETOOLONG
The length of
.I logfile
exceeds
.BR CA_MAXPATHLEN .
