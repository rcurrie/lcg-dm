/*
 * Copyright (C) 1999-2011 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Clogit.c$ $Revision$ $Date$ CERN IT-GT/DMS Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include "Castor_limits.h"
#include "Cglobals.h"
#include "Clogit.h"
#include "Csnprintf.h"
#include "serrno.h"
#define LOGBUFSZ CA_MAXLINELEN+1
static int jid;
static char logfile[CA_MAXPATHLEN+1];
static int loglevel = -1;
static int syslogflag;

int
Cinitlog (char *cmd, char *logfilename)
{
	char *p;

	jid = getpid();
	if (loglevel < 0) {
		if ((p = getenv ("LOG_PRIORITY")) != NULL)
			loglevel = atoi (p);
		else
			loglevel = LOG_INFO;
	}
	if (! logfilename) {
		serrno = EFAULT;
		return (-1);
	}
	if (strcmp (logfilename, "syslog") == 0) {
		syslogflag = 1;
		openlog (cmd, 0, LOG_USER);
	} else if (strlen (logfilename) > CA_MAXPATHLEN) {
		serrno = ENAMETOOLONG;
		return (-1);
	} else
		strcpy (logfile, logfilename);
	return (0);
}

int
Clogit(int level, char *func, char *msg, ...)
{
	va_list args;

	va_start (args, msg);
	Cvlogit (level, func, msg, args);
	va_end (args);
	return (0);
}

int
Cvlogit(int level, char *func, char *msg, va_list args)
{
	int l;
	char prtbuf[LOGBUFSZ];
	int save_errno;
	int Tid = 0;
	struct tm *tm;
#if defined(_REENTRANT) || defined(_THREAD_SAFE)
	struct tm tmstruc;
#endif
	struct timeval tv;
	int fd_log;

	if (level > loglevel)
		return (0);
	save_errno = errno;
	if (! syslogflag) {
		(void) gettimeofday (&tv, NULL);
#if (defined(_REENTRANT) || defined(_THREAD_SAFE)) && !defined(_WIN32)
		(void) localtime_r (&tv.tv_sec, &tmstruc);
		tm = &tmstruc;
#else
		tm = localtime (&tv.tv_sec);
#endif
		Csnprintf (prtbuf, LOGBUFSZ, "%02d/%02d %02d:%02d:%02d.%03d ",
		    tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
		    tv.tv_usec / 1000);
		l = 19;
	} else {
		prtbuf[0] = '\0';
		l = 0;
	}
	Cglobals_getTid (&Tid);
	if (Tid < 0) {	/* main thread */
		if (! syslogflag)
			Csnprintf (prtbuf + l, LOGBUFSZ - l, "%5d %s: ",
			    jid, func);
		else
			Csnprintf (prtbuf + l, LOGBUFSZ - l, "[%d] %s: ",
			    jid, func);
	} else {
		if (! syslogflag)
			Csnprintf (prtbuf + l, LOGBUFSZ - l, "%5d,%d %s: ",
			    jid, Tid, func);
		else
			Csnprintf (prtbuf + l, LOGBUFSZ - l, "[%d,%d] %s: ",
			    jid, Tid, func);
	}
	l = strlen (prtbuf);
	Cvsnprintf (prtbuf + l, LOGBUFSZ - l, msg, args);
	if (prtbuf[LOGBUFSZ-2] != '\n') {
		prtbuf[LOGBUFSZ-2] = '\n';
		prtbuf[LOGBUFSZ-1] = '\0';
	}
	if (! syslogflag) {
#ifdef O_LARGEFILE
		fd_log = open (logfile, O_WRONLY | O_CREAT | O_APPEND | O_LARGEFILE, 0664);
#else
		fd_log = open (logfile, O_WRONLY | O_CREAT | O_APPEND, 0664);
#endif
		write (fd_log, prtbuf, strlen(prtbuf));
		close (fd_log);
	} else {
		syslog (level, "%s", prtbuf);
	}
	errno = save_errno;
	return (0);
}
