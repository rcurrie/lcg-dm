.\" @(#)$RCSfile: rfio_chown.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:02 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_CHOWN 3 "$Date: 2005/03/31 13:13:02 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_chown \- change owner and group of a directory/file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_chown (const char *" path ,
.BI "uid_t " new_uid ,
.BI "gid_t " new_gid )
.SH DESCRIPTION
.B rfio_chown
sets the owner and the group of a directory/file to the numeric values in
.I owner
and
.I group
respectively.
If
.I owner
or
.I group
is specified as -1,
.B rfio_chown()
does not change the corresponding ID of the file.
.TP
.I path
specifies the logical pathname relative to the current directory or
the full pathname.
.LP
The effective user ID of the process must match the owner of the file or be
super-user.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EPERM
The effective user ID does not match the owner of the file and is not super-user.
.TP
.B ENOENT
The named file/directory does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix or write permission on the file itself is denied.
.TP
.B EFAULT
.I path
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B EINVAL
.I new_uid
or
.I new_gid
is invalid.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SETIMEDOUT
Timed out.
.TP
.B SECONNDROP
Connection closed by remote end.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR Castor_limits(4)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
