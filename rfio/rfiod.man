.\"
.\" $Id$
.\"
.\" @(#)$RCSfile: rfiod.man,v $ $Revision$ $Date$ CERN/IT/PDP/DM
.\" Copyright (C) 1990-2010 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIOD 8 "$Date$" CASTOR "Rfio Administrator Commands"
.SH NAME
rfiod\- Remote file access daemon
.SH SYNOPSIS
.B rfiod
[
.B -d
] [
.BI -f " filename"
] [
.B -l
] [
.BI -p " port"
] [
.B -s
] [
.B -t
] [
.B -4
|
.B -6
] [
.BI -L " log_level"
]
.SH DESCRIPTION
.IX "\fLrfiod\fR"
.IX  tcp
rfiod start the daemon handling remote file access requests.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
rfiod uses for the streaming mode (v3) one control port and one data port.
The range of data ports to be used by rfiod can be set with the environment
variable RFIO_PORT_RANGE.
.SH OPTIONS
.TP
.B \-d
Turn on printing of debug messages. Default output device is stderr.
.TP
.BI \-f " filename"
Output filename for error, logging and debugging messages.
Normal error messages are sent to syslog unless otherwise specified.
Two filenames are special : "stderr", which will send messages to standard
error and "syslog" which will send to syslog.
.TP
.B \-l
Turn on printing of log messages. Default output device is in
.BR /var/log/rfio/log .
.TP
.BI \-p " port"
Specify a network port for the service to listen on. If this option
is not given the value RFIO_PORT defined in rfio_constants.h is used.
.TP
.B \-s
Turn on Standalone mode.
This option should always be set when rfiod is not started by inetd.
.TP
.B \-t
Turn on single threaded mode. Useful for debugging as no processes
are created.
.TP
.B -4
only try to listen on IPv4 addresses
.TP
.B -6
only try to listen on IPv6 addresses
.TP
.BI \-L " log_level"
Specify the logging level. Default 6 (LOG_INFO).
.SH EXAMPLE
.RS
.HP
/usr/local/bin/rfiod -sl
.RE
.SH "SEE ALSO"
.BR syslog(8) ,
.B tcp(4)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
