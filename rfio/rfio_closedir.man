.\"
.\" $Id: rfio_closedir.man,v 1.1 2005/03/31 13:13:02 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_closedir.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:02 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_CLOSEDIR 3 "$Date: 2005/03/31 13:13:02 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_closedir \- close directory opened by
.B rfio_opendir
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_closedir (RDIR *" dirp ");"
.SH DESCRIPTION
.B rfio_closedir
closes the directory opened by
.B rfio_opendir
and associated with the
.B RDIR
structure pointed by
.IR dirp .
.TP
.I dirp
specifies the pointer value returned by
.BR rfio_opendir .
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
File descriptor in DIR structure is invalid.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SETIMEDOUT
Timed out.
.TP
.B SEBADVERSION
Version ID mismatch.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR rfio_opendir(3) ,
.BR rfio_readdir(3) ,
.BR rfio_rewinddir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
