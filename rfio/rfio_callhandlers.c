/*
 * $Id$
 */

/*
 * Copyright (C) 2005-2007 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rfio_callhandlers.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#if defined(_WIN32)
#define MAXHOSTNAMELEN 64
#else
#include <netdb.h>
#include <sys/param.h>          /* For MAXHOSTNAMELEN definition  */
#endif
#include <sys/stat.h>
#if defined(_WIN32)
#define R_OK 4
#define W_OK 2
#define X_OK 1
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Csec_api.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "log.h"
#include "rfio_api.h"
#include "serrno.h"
extern Csec_context_t sec_ctx;
extern int set_authorization_id;
extern char data_interface[MAXHOSTNAMELEN];
extern char localdomain[MAXHOSTNAMELEN];
extern struct passwd stagersuperuser;

static struct {
	char *principal;
	uid_t uid;
	gid_t gid;
	char **fqan;
	char *voname;
	char *user_ca;
	int nbfqans;
} stored;

int rfio_handle_store_virtualid(const char *principal)
{
#ifdef CSEC
	if (set_authorization_id) {
#ifdef VIRTUAL_ID
		stored.user_ca = Csec_server_get_client_ca (&sec_ctx);
#ifdef USE_VOMS
		stored.voname = Csec_server_get_client_vo (&sec_ctx);
		stored.fqan = Csec_server_get_client_fqans (&sec_ctx, &stored.nbfqans);
#endif
		if (Cns_getidmapc (principal, stored.user_ca, stored.nbfqans > 1 ? 1 : stored.nbfqans,
		    (const char **)stored.fqan, &stored.uid, &stored.gid)) {
			log (LOG_ERR, "Could not verify %s by getting a virtual id from the dpm: %s !\n", principal, sstrerror (serrno));
			return (-1);
		}

		stored.principal = strdup (principal);
#endif
	}
#endif
	return (0);
}

int rfio_handle_access(const char *sfn, 
		     int mode,
		     const char *mech,
		     const char *principal,
		     uid_t uid,
		     gid_t gid,
		     int *need_user_check)
{
	char **fqan = NULL;
	int nbfqans = 0;
	char *p;
	char sfn1[CA_MAXSFNLEN+1];
	struct stat64 st;
	char *user_ca;
	char *voname = NULL;

	if (stat64 (sfn, &st) < 0) {
		serrno = errno;
		return (-1);
	}
	if ((st.st_mode & S_IFMT) != S_IFREG ||
	    (st.st_uid != stagersuperuser.pw_uid && st.st_gid != stagersuperuser.pw_gid)) {
		/* not a file managed by DPM */
		*need_user_check = 1;
		return (0);
	}
	strcpy (sfn1, data_interface);
	if (strchr (sfn1, '.') == NULL) {
		strcat (sfn1, ".");
		strcat (sfn1, localdomain);
	}
	p = sfn1 + strlen (sfn1);
	*p++ = ':';
	strcpy (p, sfn);

#ifdef CSEC
	if (stored.principal != NULL && !strcmp(stored.principal, principal)) {
		dpm_client_setAuthorizationId (stored.uid, stored.gid, (char *) mech, (char *) principal);
		Cns_client_setAuthorizationId (stored.uid, stored.gid, (char *) mech, (char *) principal);
		if (stored.voname && stored.fqan) {
			dpm_client_setVOMS_data (stored.voname, stored.fqan, stored.nbfqans);
			Cns_client_setVOMS_data (stored.voname, stored.fqan, stored.nbfqans);
		}
	} else if (set_authorization_id) {
#ifdef VIRTUAL_ID
		user_ca = Csec_server_get_client_ca (&sec_ctx);
#ifdef USE_VOMS
		voname = Csec_server_get_client_vo (&sec_ctx);
		fqan = Csec_server_get_client_fqans (&sec_ctx, &nbfqans);
#endif
		if (Cns_getidmapc (principal, user_ca, nbfqans > 1 ? 1 : nbfqans,
		    (const char **)fqan, &uid, &gid)) {
			log (LOG_ERR, "Could not get virtual id: %s !\n", sstrerror (serrno));
			return (-1);
		}
#endif
		dpm_client_setAuthorizationId (uid, gid, (char *) mech, (char *) principal);
		Cns_client_setAuthorizationId (uid, gid, (char *) mech, (char *) principal);
		if (voname && fqan) {
			dpm_client_setVOMS_data (voname, fqan, nbfqans);
			Cns_client_setVOMS_data (voname, fqan, nbfqans);
		}
	}
#endif
	Cns_set_selectsrvr (CNS_SSRV_NOTPATH);
	if (dpm_accessr (sfn1, mode) < 0) {
		if (serrno != SEOPNOTSUP || Cns_accessr (sfn1, mode) < 0)
			return (-1);
	}
	*need_user_check = 0;
	return (0);
}

int rfio_handle_open(const char *sfn, 
		     int flags,
		     int mode,
		     const char *mech,
		     const char *principal,
		     uid_t uid,
		     gid_t gid,
		     char **pfn, 
		     void **ctx,
		     int *need_user_check)
{
	int amode;
	char **fqan = NULL;
	int nbfqans = 0;
	char *p;
	char *parent;
	char sfn1[CA_MAXSFNLEN+1];
	struct stat64 st;
	char *user_ca;
	char *voname = NULL;

	if (stat64 (sfn, &st) < 0) {
		if (errno != ENOENT || (flags & O_CREAT) == 0) {
			serrno = errno;
			return (-1);
		}
		if ((parent = strdup (sfn)) == NULL) {
			serrno = errno;
			return (-1);
		}
		if ((p = strrchr (parent, '/')) == NULL) {
			free (parent);
			serrno = EINVAL;
			return (-1);
		}
		if (p != parent) {
			*p = '\0';
			p = parent;
		} else
			p = "/";
		if (stat64 (p, &st) < 0) {
			serrno = errno;
			free (parent);
			return (-1);
		}
		free (parent);
	}
	if (st.st_uid != stagersuperuser.pw_uid && st.st_gid != stagersuperuser.pw_gid) {
		/* not a file managed by DPM */
		*pfn = strdup(sfn);
		*need_user_check = 1;
		return (0);
	}
	strcpy (sfn1, data_interface);
	if (strchr (sfn1, '.') == NULL) {
		strcat (sfn1, ".");
		strcat (sfn1, localdomain);
	}
	p = sfn1 + strlen (sfn1);
	*p++ = ':';
	strcpy (p, sfn);
#ifdef CSEC
	if (stored.principal != NULL && !strcmp(stored.principal, principal)) {
		dpm_client_setAuthorizationId (stored.uid, stored.gid, (char *) mech, (char *) principal);
		Cns_client_setAuthorizationId (stored.uid, stored.gid, (char *) mech, (char *) principal);
		if (stored.voname && stored.fqan) {
			dpm_client_setVOMS_data (stored.voname, stored.fqan, stored.nbfqans);
			Cns_client_setVOMS_data (stored.voname, stored.fqan, stored.nbfqans);
		}
	} else if (set_authorization_id) {
#ifdef VIRTUAL_ID
		user_ca = Csec_server_get_client_ca (&sec_ctx);
#ifdef USE_VOMS
		voname = Csec_server_get_client_vo (&sec_ctx);
		fqan = Csec_server_get_client_fqans (&sec_ctx, &nbfqans);
#endif
		if (Cns_getidmapc (principal, user_ca, nbfqans > 1 ? 1 : nbfqans,
		    (const char **)fqan, &uid, &gid)) {
			log (LOG_ERR, "Could not get virtual id: %s !\n", sstrerror (serrno));
			return (-1);
		}
#endif
		dpm_client_setAuthorizationId (uid, gid, (char *) mech, (char *) principal);
		Cns_client_setAuthorizationId (uid, gid, (char *) mech, (char *) principal);
		if (voname && fqan) {
			dpm_client_setVOMS_data (voname, fqan, nbfqans);
			Cns_client_setVOMS_data (voname, fqan, nbfqans);
		}
	}
#endif
	Cns_set_selectsrvr (CNS_SSRV_NOTPATH);
	amode = (flags & (O_WRONLY|O_TRUNC|O_CREAT)) ? W_OK : R_OK;
	if (dpm_accessr (sfn1, amode) < 0) {
		if (serrno != SEOPNOTSUP || Cns_accessr (sfn1, amode) < 0)
			return (-1);
	}
	*pfn = strdup(sfn);
	*need_user_check = 0;
	return (0);
}

int rfio_handle_firstwrite(void *ctx)
{
	return (0);
}

int rfio_handle_close(void *ctx,
		      struct stat64 *filestat,
		      int close_status)
{
	return (0);
}

int rfio_handle_remote_open(const char *sfn, 
		     const char *mech,
		     const char *principal,
		     uid_t uid,
		     gid_t gid)
{
	char **fqan = NULL;
	int nbfqans = 0;
	char *user_ca;
	char *voname = NULL;

#ifdef CSEC
	if (stored.principal != NULL && !strcmp(stored.principal, principal)) {
		rfio_client_setAuthorizationId (stored.uid, stored.gid, (char *) mech, (char *) principal);
		if (stored.voname && stored.fqan)
			rfio_client_setVOMS_data (stored.voname, stored.fqan, stored.nbfqans);
	} else if (set_authorization_id) {
#ifdef VIRTUAL_ID
		user_ca = Csec_server_get_client_ca (&sec_ctx);
#ifdef USE_VOMS
		voname = Csec_server_get_client_vo (&sec_ctx);
		fqan = Csec_server_get_client_fqans (&sec_ctx, &nbfqans);
#endif
		if (Cns_getidmapc (principal, user_ca, nbfqans > 1 ? 1 : nbfqans,
		    (const char **)fqan, &uid, &gid)) {
			log (LOG_ERR, "Could not get virtual id: %s !\n", sstrerror (serrno));
			return (-1);
		}
#endif
		rfio_client_setAuthorizationId (uid, gid, (char *) mech, (char *) principal);
		if (voname && fqan)
			rfio_client_setVOMS_data (voname, fqan, nbfqans);
	}
#endif
	return (0);
}

int rfio_handle_stat(const char *sfn, 
		     int lflag,
		     const char *mech,
		     const char *principal,
		     uid_t uid,
		     gid_t gid,
		     struct stat *st,
		     int *need_user_check)
{
	char **fqan = NULL;
	int nbfqans = 0;
	char *p;
	char sfn1[CA_MAXSFNLEN+1];
	struct Cns_filestatg statbuf;
	char *user_ca;
	char *voname = NULL;

#if !defined(_WIN32)
	if (lflag) {
		if (lstat (sfn, st) < 0) {
			serrno = errno;
			return (-1);
		}
	} else
#endif
		if (stat (sfn, st) < 0) {
			serrno = errno;
			return (-1);
		}
	if ((st->st_mode & S_IFMT) != S_IFREG ||
	    (st->st_uid != stagersuperuser.pw_uid && st->st_gid != stagersuperuser.pw_gid)) {
		/* not a file managed by DPM */
		*need_user_check = 1;
		return (0);
	}
	strcpy (sfn1, data_interface);
	if (strchr (sfn1, '.') == NULL) {
		strcat (sfn1, ".");
		strcat (sfn1, localdomain);
	}
	p = sfn1 + strlen (sfn1);
	*p++ = ':';
	strcpy (p, sfn);
#ifdef CSEC
	if (stored.principal != NULL && !strcmp(stored.principal, principal)) {
		Cns_client_setAuthorizationId (stored.uid, stored.gid, (char *) mech, (char *) principal);
		if (stored.voname && stored.fqan)
			Cns_client_setVOMS_data (stored.voname, stored.fqan, stored.nbfqans);
	} else if (set_authorization_id) {
#ifdef VIRTUAL_ID
		user_ca = Csec_server_get_client_ca (&sec_ctx);
#ifdef USE_VOMS
		voname = Csec_server_get_client_vo (&sec_ctx);
		fqan = Csec_server_get_client_fqans (&sec_ctx, &nbfqans);
#endif
		if (Cns_getidmapc (principal, user_ca, nbfqans > 1 ? 1 : nbfqans,
		    (const char **)fqan, &uid, &gid)) {
			log (LOG_ERR, "Could not get virtual id: %s !\n", sstrerror (serrno));
			return (-1);
		}
#endif
		Cns_client_setAuthorizationId (uid, gid, (char *) mech, (char *) principal);
		if (voname && fqan)
			Cns_client_setVOMS_data (voname, fqan, nbfqans);
	}
#endif
	Cns_set_selectsrvr (CNS_SSRV_NOTPATH);
	if (Cns_statr (sfn1, &statbuf) < 0)
		return (-1);
	st->st_uid = statbuf.uid;
	st->st_gid = statbuf.gid;
	st->st_mode = statbuf.filemode;
	*need_user_check = 0;
	return (0);
}

int rfio_handle_stat64(const char *sfn, 
		     int lflag,
		     const char *mech,
		     const char *principal,
		     uid_t uid,
		     gid_t gid,
		     struct stat64 *st,
		     int *need_user_check)
{
	char **fqan = NULL;
	int nbfqans = 0;
	char *p;
	char sfn1[CA_MAXSFNLEN+1];
	struct Cns_filestatg statbuf;
	char *user_ca;
	char *voname = NULL;

#if !defined(_WIN32)
	if (lflag) {
		if (lstat64 (sfn, st) < 0) {
			serrno = errno;
			return (-1);
		}
	} else
#endif
		if (stat64 (sfn, st) < 0) {
			serrno = errno;
			return (-1);
		}
	if ((st->st_mode & S_IFMT) != S_IFREG ||
	    (st->st_uid != stagersuperuser.pw_uid && st->st_gid != stagersuperuser.pw_gid)) {
		/* not a file managed by DPM */
		*need_user_check = 1;
		return (0);
	}
#ifdef CSEC
	if (uid == 0 && gid == 0) {
		*need_user_check = 0;
		return (0);
	}
#endif
	strcpy (sfn1, data_interface);
	if (strchr (sfn1, '.') == NULL) {
		strcat (sfn1, ".");
		strcat (sfn1, localdomain);
	}
	p = sfn1 + strlen (sfn1);
	*p++ = ':';
	strcpy (p, sfn);
#ifdef CSEC
	if (stored.principal != NULL && !strcmp(stored.principal, principal)) {
		Cns_client_setAuthorizationId (stored.uid, stored.gid, (char *) mech, (char *) principal);
		if (stored.voname && stored.fqan)
			Cns_client_setVOMS_data (stored.voname, stored.fqan, stored.nbfqans);
	} else if (set_authorization_id) {
#ifdef VIRTUAL_ID
		user_ca = Csec_server_get_client_ca (&sec_ctx);
#ifdef USE_VOMS
		voname = Csec_server_get_client_vo (&sec_ctx);
		fqan = Csec_server_get_client_fqans (&sec_ctx, &nbfqans);
#endif
		if (Cns_getidmapc (principal, user_ca, nbfqans > 1 ? 1 : nbfqans,
		    (const char **)fqan, &uid, &gid)) {
			log (LOG_ERR, "Could not get virtual id: %s !\n", sstrerror (serrno));
			return (-1);
		}
#endif
		Cns_client_setAuthorizationId (uid, gid, (char *) mech, (char *) principal);
		if (voname && fqan)
			Cns_client_setVOMS_data (voname, fqan, nbfqans);
	}
#endif
	Cns_set_selectsrvr (CNS_SSRV_NOTPATH);
	if (Cns_statr (sfn1, &statbuf) < 0)
		return (-1);
	st->st_uid = statbuf.uid;
	st->st_gid = statbuf.gid;
	st->st_mode = statbuf.filemode;
	*need_user_check = 0;
	return (0);
}
