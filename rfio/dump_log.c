/*
 * $Id: dump_log.c,v 1.1 2005/03/31 13:13:00 baud Exp $
 */

/*
 * Copyright (C) 1993-1999 by CERN/IT/PDP/DM
 * All rights reserved
 */
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dump_log.c,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:00 $ CERN/IT/PDP/DM Fabien Collin" ;
#endif /* not lint */

/* dump_log.c : A command to dump a client log */

#include <stdio.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <unistd.h>
#if ( defined(__osf__) && defined(__alpha) )
#include <stdlib.h>
#endif
#include "logging.h"

#define ERR_RECORD_FULL 1

#define MAX_LSEEK 7000
#define MAX_READ 7000
#define MAX_WRITE 7000
#define MAX_OP MAX_LSEEK+MAX_READ+MAX_WRITE

static OPEN_BUFFER rec_open;
static ERROR_BUFFER rec_error;
static RECORD_BUFFER rec_record;
static OPERATION rec_op[MAX_OP];
static int cur_op;
static int record_table[MAX_RECORD_TABLE];
static int length_record_table;
static int lf;

void rec_read(buffer,size)
     char *buffer;
     int size;
{
  int n;

  if ((n = read(lf,buffer,size)) == -1)
    {
      fprintf(stderr,"Error reading logging file\n");
      exit(1);
    }

  /* It's possible for size to be equal to 0 if the client has made an open
     with a TRUNC flag : The record table is then empty, but the dump log
     is not terminated */
  if ((n == 0) && (size != 0))
    {
      fprintf(stderr,"Dump terminated\n");
      exit(0);
    }
}

void main(argc,argv)
     int argc;
     char *argv[];
{
  static char *log_filename;
  char *getconfent();
  char date[26];
  int i;
  int j;
  char buf[10];
  WRITE_BUFFER wb;
  READ_BUFFER rb;
  LSEEK_BUFFER lb;
  char *name;
  short name_length;

  if (argc != 2)
    {
	log_filename= (char *) getconfent("RFIO", "CLIENTLOG", 0);
	if (log_filename == NULL ) {
		fprintf(stderr,"No file specified\n") ;
		exit(1) ;
	}
    }
  else
  /*
    Open the logging file
    See RFIO/CLIENT_LOG variable in shift.conf
    */

  	log_filename = argv[1];

  if ((lf = open(log_filename,O_RDONLY)) == -1)
    {
      fprintf(stderr,"Cannot open %s\n",log_filename);
      exit(1);
    }

  while (1)
    {
      rec_read((char *)&rec_open,sizeof(rec_open));

      if (rec_open.command != LOG_OPEN)
	{
	  fprintf(stderr,"Panic : OPEN record not found\n");
	  exit(1);
	}
      strcpy(date,ctime(&rec_open.date));
      date[24] = '\0';

      rec_read((char *)&name_length,sizeof(name_length));

      if ((name = malloc(name_length)) == NULL)
	{
	  fprintf(stderr,"Not enough memory\n");
	  exit(1);
	}

      rec_read(name,name_length);

      printf("%08X %04X %s OPEN : Uid=%05u Local=%1d Flags=%08X Size=%08X File = %s Machine=%s Rtime=%08X Utime=%08X Stime=%08X\n",
	     rec_open.incarnation_date,
	     rec_open.incarnation_pid,
	     date,
	     rec_open.uid,
	     rec_open.local,
	     rec_open.flags,
	     rec_open.size,
	     name,
	     rec_open.machine,
	     rec_open.session_real_time,rec_open.session_user_time,rec_open.session_sys_time);

      rec_read((char *)&rec_error,sizeof(rec_error));

      if (rec_error.command != LOG_ERRORS)
	{
	  fprintf(stderr,"Panic : ERROR record not found\n");
	  exit(1);
	}

      if (rec_error.num)
	printf("%08X %04X %s ERROR : Num=%05u\n",
	       rec_open.incarnation_date,
	       rec_open.incarnation_pid,
	       date,
	       rec_error.num);
      
      rec_read((char *)&rec_record,sizeof(rec_record));

      if (rec_record.command != LOG_RECORD)
	{
	  fprintf(stderr,"Panic : RECORD TABLE record not found\n");
	  exit(1);
	}

      length_record_table = rec_record.length;
      
      rec_read((char *)record_table,rec_record.length * sizeof(long));

      rec_read((char *)&cur_op,sizeof(int));

      rec_read((char *)rec_op,cur_op * sizeof(LSEEK_BUFFER));

      for (i=0;i<cur_op;i++)
	{
	  switch(rec_op[i].lseek.command)
	    {
	    case LOG_LSEEK_SET:
	      /* Here we use a temporary variable lb because the HP-UX 9.01
		 C compiler is broken */
	      lb = rec_op[i].lseek;

	      printf("%08X %04X %s LSEEK_SET : Offset=%08X\n",
		     rec_open.incarnation_date,
		     rec_open.incarnation_pid,
		     date,
		     lb.offset);
	      break;
	    case LOG_LSEEK_CUR:
	      /* Here we use a temporary variable lb because the HP-UX 9.01
		 C compiler is broken */
	      lb = rec_op[i].lseek;

	      printf("%08X %04X %s LSEEK_CUR : Offset=%08X\n",
		     rec_open.incarnation_date,
		     rec_open.incarnation_pid,
		     date,
		     lb.offset);
	      break;
	    case LOG_LSEEK_END:
	      /* Here we use a temporary variable lb because the HP-UX 9.01
		 C compiler is broken */
	      lb = rec_op[i].lseek;

	      printf("%08X %04X %s LSEEK_END : Offset=%08X\n",
		     rec_open.incarnation_date,
		     rec_open.incarnation_pid,
		     date,
		     lb.offset);
	      break;
	    case LOG_READ:
	      /* Here we use a temporary variable rb because the HP-UX 9.01
		 C compiler is broken */
	      rb = rec_op[i].read;

	      printf("%08X %04X %s READ : Count=%08X Record=%08X\n",
		     rec_open.incarnation_date,
		     rec_open.incarnation_pid,
		     date,
		     rb.count,
		     record_table[rb.record]);
	      break;
	    case LOG_WRITE:
	      /* Here we use a temporary variable wb because the HP-UX 9.01
		 C compiler is broken */
	      wb = rec_op[i].write;

	      printf("%08X %04X %s WRITE : Count=%08X Record=%08X\n",
		     rec_open.incarnation_date,
		     rec_open.incarnation_pid,
		     date,
		     wb.count,
		     record_table[wb.record]);

	      break;
	    default:
	      printf("Error : Unknown operation\n");
	    }
	}
    }
}
