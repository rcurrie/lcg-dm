.\"
.\" $Id: rfio_rename.man,v 1.1 2005/03/31 13:13:03 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_rename.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:03 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_RENAME 3 "$Date: 2005/03/31 13:13:03 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_rename \- rename a file or directory
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_rename (const char *" oldpath ,
.BI "const char *" newpath )
.SH DESCRIPTION
.B rfio_rename
renames a file or directory.
.LP
.I oldpath
and
.I newpath
must be of the same type, i.e. both regular files or both directories.
.LP
If
.I newpath
exists already, it will be removed before the rename takes place. If
.I newpath
is a directory, it must be empty.
.LP
When renaming a directory,
.I newpath
must not be a descendant of
.IR oldpath ,
i.e.
.I newpath
must not contain a path prefix that names
.IR oldpath .
.LP
Write permission is required on both parents. If
.I oldpath
is a directory, write permission is required on it and if
.I newpath
is an existing directory, write permission is also required on it.
.LP
If any of the parents has the sticky bit S_ISVTX set, either
.RS
.LP
the effective user ID of the requestor must match the owner ID of the file or
.LP
the effective user ID of the requestor must match the owner ID of the directory or
.LP
the file must be writable by the requestor or
.LP
the requestor must be super-user.
.RE
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
A component of
.I oldpath
prefix does not exist or
.I oldpath
or
.I newpath
is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the path prefixes or
write permission on the parent directories is denied or
.I oldpath
is a directory and write permission is denied on
.I oldpath
or
.IR newpath .
.TP
.B EFAULT
.I oldpath
or
.I newpath
is a NULL pointer.
.TP
.B EEXIST
.I newpath
is an existing directory and is not empty.
.TP
.B ENOTDIR
A component of the path prefixes is not a directory or
.I oldpath
is a directory and
.I newpath
is an existing regular file.
.TP
.B EISDIR
.I newpath
is a directory while
.I oldpath
is a regular file.
.TP
.B EINVAL
.I newpath
is a descendant of
.IR oldpath .
.TP
.B ENAMETOOLONG
The length of
.I oldpath
or
.I newpath
exceeds
.B CA_MAXPATHLEN
or the length of a path component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ERFXHOST
Cross-host rename is not supported.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR rfio_chmod(3) ,
.BR rfio_unlink(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
