.\"
.\" $Id: rfdf.man,v 1.1 2008/06/13 14:32:22 dhsmith Exp $
.\"
.\" @(#)$RCSfile: rfdf.man,v $ $Revision: 1.1 $ $Date: 2008/06/13 14:32:22 $ CERN IT-DM David Smith
.\" Copyright (C) 2008 by CERN/IT/DM
.\" All rights reserved
.\"
.TH RFDF 1 "$Date: 2008/06/13 14:32:22 $" CASTOR "Rfio User Commands"
.SH NAME
rfdf \- print information about a mounted filesystem
.SH SYNOPSIS
.B rfdf
.IR filename \ ...
.SH DESCRIPTION
.IX "\fLrfdf\fR"
.B rfdf
provides an interface to the
.B shift
remote file I/O daemon (rfiod) for returning information about one or more mounted filesystems.
The
.IR filename s
may refer to one ore more remote files or directories, each having the form:
.IP
.IB hostname : path
.LP
or a local file (not containing the :/ character combination). The information
for the filesystem on which each file or directory resides is returned.
.P
The following information may be returned
.P
Path (as specified on the command line)
.IP
blocksize
.IP
total blocks
.IP
free blocks - number of blocks available to non-superuser
.IP
total inodes
.IP
free inodes 
.LP
.SH "SEE ALSO"
.BR rfio_statfs64(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch> and \fBDPM\fP Team <hep-service-dpm@cern.ch>
