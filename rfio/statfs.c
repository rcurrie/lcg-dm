/*
 * $Id: statfs.c,v 1.3 2007/12/04 11:25:43 dhsmith Exp $
 */

/*
 * Copyright (C) 1990-2005 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: statfs.c,v $ $Revision: 1.3 $ $Date: 2007/12/04 11:25:43 $ CERN/IT/PDP/DM Felix Hassine";
#endif /* not lint */

/* statfs.c       Remote File I/O - get file system status     */

#define RFIO_KERNEL 1
#include "rfio.h"               /* Remote File I/O general definitions  */

int DLL_DECL rfio_statfs(path, statfsbuf)  
char    *path;              	/* remote file path                     */
struct rfstatfs *statfsbuf;     /* status buffer (subset of local used) */
{
#if (defined(__alpha) && defined(__osf__)) || defined(IRIX64) || defined(__ia64__) || defined(__x86_64) || defined(__ppc64__)
   return (rfio_statfs64(path,(struct rfstatfs64 *)statfsbuf));
#else
   char     buf[BUFSIZ];       /* General input/output buffer          */
   register int    s;      /* socket descriptor            */
   int      status;
   int      len;
   char     *host, *filename;
   char     *p=buf;
   int 	 rt ;
   int	 rcode, parserc ;

   INIT_TRACE("RFIO_TRACE");
   TRACE(1, "rfio", "rfio_statfs(%s, %x)", path, statfsbuf);

   if (!(parserc = rfio_parse(path,&host,&filename))) {
      /* if not a remote file, must be local  */
      TRACE(1, "rfio", "rfio_statfs:  using local statfs(%s, %x)",
	    filename, statfsbuf);

      END_TRACE();
      rfio_errno = 0;
      return(rfstatfs(filename , statfsbuf));
   }
   if (parserc < 0) {
	   END_TRACE();
	   return(-1);
   }

   len = strlen(path)+1;
   if ( RQSTSIZE+len > BUFSIZ ) {
     TRACE(2,"rfio","rfio_statfs: request too long %d (max %d)",
           RQSTSIZE+len,BUFSIZ);
     END_TRACE();
     serrno = E2BIG;
     return(-1);
   }

   s = rfio_connect(host,&rt);
   if (s < 0)      {
      END_TRACE();
      return(-1);
   }

   marshall_WORD(p, RFIO_MAGIC);
   marshall_WORD(p, RQST_STATFS);
   marshall_LONG(p, len);
   p= buf + RQSTSIZE;
   marshall_STRING(p, filename);
   TRACE(2,"rfio","rfio_statfs: sending %d bytes",RQSTSIZE+len) ;
   if (netwrite_timeout(s,buf,RQSTSIZE+len,RFIO_CTRL_TIMEOUT) != (RQSTSIZE+len)) {
      TRACE(2, "rfio", "rfio_statfs: write(): ERROR occured (errno=%d)", errno);
      (void) close(s);
      END_TRACE();
      return(-1);
   }
   p = buf;
   TRACE(2, "rfio", "rfio_statfs: reading %d bytes", 7*LONGSIZE);
   if (netread_timeout(s, buf, 7*LONGSIZE, RFIO_CTRL_TIMEOUT) != (7*LONGSIZE))  {
      TRACE(2, "rfio", "rfio_statfs: read(): ERROR occured (errno=%d)", errno);
      (void) close(s);
      END_TRACE();
      return(-1);
   }
   unmarshall_LONG( p, statfsbuf->bsize ) ;
   unmarshall_LONG( p, statfsbuf->totblks ) ;
   unmarshall_LONG( p, statfsbuf->freeblks ) ;
   unmarshall_LONG( p, statfsbuf->totnods ) ;
   unmarshall_LONG( p, statfsbuf->freenods ) ;
   unmarshall_LONG( p, status ) ;
   unmarshall_LONG( p, rcode ) ;

   TRACE(1, "rfio", "rfio_statfs: return %d",status);
   rfio_errno = rcode ;
   (void) close(s);
   END_TRACE();
   return (status);
#endif
}

int DLL_DECL rfio_statfs64(path, statfsbuf)  
char    *path;              	/* remote file path                     */
struct rfstatfs64 *statfsbuf;   /* status buffer (subset of local used) */
{
   char     buf[BUFSIZ];       /* General input/output buffer          */
   register int    s;      /* socket descriptor            */
   int      status;
   int      len;
   char     *host, *filename;
   char     *p=buf;
   int 	 rc ;
   int save_errno, save_serrno;
   int 	 rt ;
   int	 rcode, parserc ;
   struct rfstatfs statfsb32;

   INIT_TRACE("RFIO_TRACE");
   TRACE(1, "rfio", "rfio_statfs64(%s, %x)", path, statfsbuf);

   if (!(parserc = rfio_parse(path,&host,&filename))) {
      /* if not a remote file, must be local  */
      TRACE(1, "rfio", "rfio_statfs64:  using local statfs64(%s, %x)",
	    filename, statfsbuf);

      END_TRACE();
      rfio_errno = 0;
      return(rfstatfs64(filename , statfsbuf));
   }
   if (parserc < 0) {
	   END_TRACE();
	   return(-1);
   }

   len = strlen(path)+1;
   if ( RQSTSIZE+len > BUFSIZ ) {
     TRACE(2,"rfio","rfio_statfs64: request too long %d (max %d)",
           RQSTSIZE+len,BUFSIZ);
     END_TRACE();
     serrno = E2BIG;
     return(-1);
   }

   s = rfio_connect(host,&rt);
   if (s < 0)      {
      END_TRACE();
      return(-1);
   }

   marshall_WORD(p, RFIO_MAGIC);
   marshall_WORD(p, RQST_STATFS64);
   marshall_LONG(p, len);
   p= buf + RQSTSIZE;
   marshall_STRING(p, filename);
   TRACE(2,"rfio","rfio_statfs64: sending %d bytes",RQSTSIZE+len) ;
   if (netwrite_timeout(s,buf,RQSTSIZE+len,RFIO_CTRL_TIMEOUT) != (RQSTSIZE+len)) {
      TRACE(2, "rfio", "rfio_statfs64: write(): ERROR occured (errno=%d)", errno);
      (void) close(s);
      END_TRACE();
      return(-1);
   }
   p = buf;
   TRACE(2, "rfio", "rfio_statfs64: reading %d bytes", 3*LONGSIZE+4*HYPERSIZE);
   if ((rc=netread_timeout(s, buf, 3*LONGSIZE+4*HYPERSIZE, RFIO_CTRL_TIMEOUT)) != (3*LONGSIZE+4*HYPERSIZE))  {
      save_errno = errno; save_serrno = serrno;
#if !defined(_WIN32)
      if (rc == 0 || (rc<0 && errno == ECONNRESET))
#else
      if (rc == 0 || (rc<0 && serrno == SETIMEDOUT))
#endif
         TRACE(2, "rfio", "rfio_statfs64: read(): The connection with the server has been broken");
      else 
         TRACE(2, "rfio", "rfio_statfs64: read(): ERROR occured (errno=%d)", errno);
      (void) close(s);
      END_TRACE();
      errno = save_errno; serrno = save_serrno;
      return(-1);
   }
   unmarshall_LONG( p, statfsbuf->bsize ) ;
   unmarshall_HYPER( p, statfsbuf->totblks ) ;
   unmarshall_HYPER( p, statfsbuf->freeblks ) ;
   unmarshall_HYPER( p, statfsbuf->totnods ) ;
   unmarshall_HYPER( p, statfsbuf->freenods ) ;
   unmarshall_LONG( p, status ) ;
   unmarshall_LONG( p, rcode ) ;

   TRACE(1, "rfio", "rfio_statfs64: return %d",status);
   rfio_errno = rcode ;
   (void) close(s);
   END_TRACE();
   return (status);
}
