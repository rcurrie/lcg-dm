/*
 * Copyright (C) 2005 by CERN/IT/GD/SC
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rfio_auth.c,v $ $Revision: 1.2 $ $Date: 2006/02/01 15:14:58 $ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include "Castor_limits.h"
#include "rfio_api.h"
#include "serrno.h"
#include "trace.h"

/*      rfio_client_getAuthorizationId - get the authorization id from the thread-specific structure */

int DLL_DECL
rfio_client_getAuthorizationId(uid_t *uid, gid_t *gid, char **mech, char **id)
{
	struct rfio_api_thread_info *thip;

#ifdef CSEC
	if (rfio_apiinit (&thip))
		return (-1);
	if (thip->use_authorization_id == 0)
		return (0);
	if (uid)
		*uid = thip->Csec_uid;
	if (gid)
		*gid = thip->Csec_gid;
	if (mech)
		*mech = thip->Csec_mech;
	if (id)
		*id = thip->Csec_auth_id;
#endif
	return (0);
}

/*      rfio_client_setAuthorizationId - set the authorization id in the thread-specific structure */

int DLL_DECL
rfio_client_setAuthorizationId(uid_t uid, gid_t gid, const char *mech, char *id)
{
	struct rfio_api_thread_info *thip;

#ifdef CSEC
	INIT_TRACE ("RFIO_TRACE");
	if (rfio_apiinit (&thip))
		return (-1);
	thip->Csec_uid = uid;
	thip->Csec_gid = gid;
	if (strlen (mech) > CA_MAXCSECPROTOLEN) {
		TRACE (1, "rfio", "setAuthorizationId: Supplied Csec protocol is too long");
		END_TRACE ();
		serrno = EINVAL;
		return (-1);
	}
	strcpy (thip->Csec_mech, mech);
	if (strlen (id) > CA_MAXCSECNAMELEN) {
		TRACE (1, "rfio", "setAuthorizationId: Supplied authorization id is too long");
		END_TRACE ();
		serrno = EINVAL;
		return (-1);
	}
	strcpy (thip->Csec_auth_id, id);
	thip->voname = NULL;
	thip->nbfqan = 0;
	thip->fqan = NULL;
	thip->use_authorization_id = 1;
#endif
	return (0);
}

/*      rfio_client_setVOMS_data - set the VOMS data in the thread-specific structure */

int DLL_DECL
rfio_client_setVOMS_data(char *voname, char **fqan, int nbfqan)
{
	struct rfio_api_thread_info *thip;

#ifdef CSEC
	if (rfio_apiinit (&thip))
		return (-1);
	thip->voname = voname;
	thip->nbfqan = nbfqan;
	thip->fqan = fqan;
#endif
	return (0);
}
