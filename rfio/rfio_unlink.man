.\"
.\" $Id: rfio_unlink.man,v 1.1 2005/03/31 13:13:04 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_unlink.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:04 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_UNLINK 3 "$Date: 2005/03/31 13:13:04 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_unlink \- remove a file entry
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_unlink (const char *" path ");"
.br
.BI "int rfio_munlink (const char *" path ");"
.br
.BI "int rfio_munlink_reset ();"
.br
.BI "int rfio_unend ();"
.br
.SH DESCRIPTION
.B rfio_unlink
removes a file entry.
.TP
.I path
specifies the logical pathname relative to the current directory or
the full pathname.
.LP
.B rfio_munlink
is identical to
.B rfio_unlink
but keeps the connection open to the server unless there are more than MAXMCON
connections already opened. This is useful when issuing a series of unlink calls.
The last
.B rfio_munlink
call should be followed by a call to
.BR rfio_unend .
.LP
.B rfio_munlink_reset
is to be used when your program is forking. In such a case the permanent connections opened with
.B rfio_munlink
become shared between the parent and the child. Use
.B rfio_munlink_reset
to perform the necessary reset and close of the socket file descriptor in the parent or the child in order to be sure that only of them will receice an answer from the RFIO daemon.
.LP
.B rfio_munlink_reset
is to be used when your program is forking. In such a case the permanent connections opened with
.B rfio_munlink
become shared between the parent and the child. Use
.B rfio_munlink_reset
to perform the necessary reset and close of the socket file descriptor in the parent or the child in order to be sure that only of them will receice an answer from the RFIO daemon.
.P
See NOTES section below.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH NOTES
Multiple connections using rfio_munlink are thread-safe but not process-wide, therefore a forked child cann share file descriptors opened with rfio_munlink by its parent. Use
.B rfio_msymlink_reset
in such case.
.P
Multiple connections behaviour is undefined if you work in a multi-threaded environment and with threads \fBnot\fP created using the CASTOR's \fBCthread\fP interface.
.SH ERRORS
.TP 1.3i
.B EPERM
.I path
is a directory.
.TP
.B ENOENT
The named file does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.IR path
prefix or write permission is denied on the parent directory or
the parent has the sticky bit S_ISVTX set and
.RS 1.5i
.LP
the effective user ID of the requestor does not match the owner ID of the file and
.LP
the effective user ID of the requestor does not match the owner ID of the
directory and
.LP
the file is not writable by the requestor and
.LP
the requestor is not super-user.
.RE
.TP
.B EFAULT
.I path
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR rfio_rmdir(3) ,
.BR Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
