/*
 * $Id$
 */

/*
 * Copyright (C) 1994-2010 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rfcp.c,v $ $Revision$ $Date$ CERN/IT/DS/HSM Felix Hassine, Olof Barring, Jean-Damien Durand";
#endif /* not lint */

#include <signal.h>
#include <fcntl.h>
#define RFIO_KERNEL 1
#include <rfio.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <osdep.h>
#include <u64subr.h>
#ifndef _WIN32
#include <unistd.h>
#endif
#include <Castor_limits.h>

#ifndef TRANSFER_UNIT
#define TRANSFER_UNIT   131072 
#endif

#define SYERR 2
#define USERR 1
#define OK    0

extern int serrno ;
extern int rfio_errno ;
extern char *getconfent() ;
#if sgi
extern char *strdup _PROTO((CONST char *));
#endif

void usage();
off64_t copyfile _PROTO((int, int, char *, u_signed64));
#ifdef CNS_ROOT
void copyfile_stgcleanup _PROTO((int));
#endif /* CNS_ROOT */
int have_maxsize;
u_signed64 maxsize;
int v2 = 0;
u_signed64 inpfile_size = 0;

#ifndef _WIN32
/* Signal handler - Simplify the POSIX sigaction calls */
#ifdef __STDC__
typedef void    Sigfunc(int);
Sigfunc *_rfio_signal(int, Sigfunc *);
#else
typedef void    Sigfunc();
Sigfunc *_rfio_signal();
#endif
#else
#define _rfio_signal(a,b) signal(a,b)
#endif

int main(argc, argv)
	int argc;
	char *argv[];
{
	int argvindx;		/* argument index in program argv */
#if defined(_WIN32)
	int binmode = O_BINARY;
#else
	int binmode = 0;
#endif
	int c;
	int cfargc;		/* number of arguments in command file */
	char **cfargv;	/* arguments in command file */
	int cfargvindx;	/* argument index in command file */
	char *curargv;	/* current argv */
	int  fd1, fd2;
	int incmdfile = 0;	/* 1 = processing command file */
	struct stat64 sbuf, sbuf2;
	off64_t size;
	time_t starttime, endtime;
	int rc ;
	int rc2 ;
	char filename[BUFSIZ], filename_sav[BUFSIZ], inpfile[BUFSIZ], outfile[BUFSIZ] ;
	char *cp , *ifce ;
	char ifce1[16] ;
	char ifce2[16] ;
	char *host1, *host2, *path1, *path2 ;
	char shost1[32];
	int l1, l2 ;
	int v;
	extern char * getifnam() ;
#if defined(_WIN32)
	WSADATA wsadata;
#endif
	int input_is_local = 1;

	have_maxsize = -1;
	maxsize = 0;
	cfargc = 0;
	cfargvindx = 0;
	inpfile[0] = '\0';
	outfile[0] = '\0';
	memset(&sbuf,'\0',sizeof(sbuf));
	memset(&sbuf2,'\0',sizeof(sbuf2));
	for (argvindx = 1; argvindx < argc || cfargvindx < cfargc; ) {
		if (cfargvindx >= cfargc) {
			incmdfile = 0;
		}
		curargv = incmdfile ? cfargv[cfargvindx++] : argv[argvindx++];
#if defined(_WIN32)
		if (strcmp (curargv, "-a") == 0) {
			binmode = O_TEXT;
			continue;
		} else if (strcmp (curargv, "-b") == 0) {
			binmode = O_BINARY;
			continue;
		} else if (*curargv == '@') {
			if ((cfargc = cmdf2argv (curargv+1, &cfargv)) < 0) {
				exit (USERR);
			}
			if (cfargc == 0) {
				continue;
			}
			incmdfile = 1;
			cfargvindx = 0;
			continue;
		}
#endif
		if (strcmp (curargv, "-s") == 0) {
			if (argvindx >= argc && cfargvindx >= cfargc) {
				usage();
			}
			if (cfargvindx >= cfargc) {
				incmdfile = 0;
			}
			curargv = incmdfile ? cfargv[cfargvindx++] : argv[argvindx++];
			/* We verify that curargv do not contain other characters but digits */
			if (strspn(curargv,"0123456789") != strlen(curargv)) {
				fprintf(stderr,"-s option value should contain only digits\n");
				exit(USERR);
			}
			maxsize = strtou64(curargv);
			if (maxsize == 0) {
				usage();
			}
			have_maxsize = 1;
			continue;
		} else if (strcmp(curargv, "-v2") == 0) {
			if (v2) {
				usage(); /* Option yet parsed */
			}
			v2 = 1;
			continue;
		}

		/* Special treatment for filenames starting with /scratch/... */
		if (inpfile[0] == '\0') {
			if (!strncmp ("/scratch/", curargv, 9) &&
				(cp = getconfent ("SHIFT", "SCRATCH", 0)) != NULL) {
				strcpy (inpfile, cp);
				strcat (inpfile, curargv+9);
			} else
				strcpy (inpfile, curargv);
		} else {
			if (!strncmp ("/scratch/", curargv, 9) &&
				(cp = getconfent ("SHIFT", "SCRATCH", 0)) != NULL) {
				strcpy (outfile, cp);
				strcat (outfile, curargv+9);
			} else
				strcpy (outfile, curargv);
		}
	}

	if (! outfile[0]) {
		usage();
	}

	/* Check that files are not identical ! */
#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif
	
	if ( rfio_stat64( outfile, &sbuf2) == 0 &&  
		 S_ISDIR(sbuf2.st_mode) ) {
		/* Not allowed if inpfile is stdin */
		if (strcmp(inpfile,"-") == 0) {
			fprintf (stderr, "Not valid output when input is the stdin\n");
			exit (USERR);
		}
		if ( (cp = strrchr(inpfile,'/'))  != NULL  ) {
			cp++;
		} else {
			cp = inpfile;
		}
		sprintf(filename, "%s/%s", outfile, cp);
	} else {
		strcpy(filename,outfile);
	}
	strcpy(filename_sav,filename);

	l1 = rfio_parseln( inpfile , &host1, &path1, NORDLINKS ) ;
	if (l1 < 0) {
		fprintf(stderr,"%s\n",sstrerror(serrno));
		exit (USERR);
	}

	if ( l1 ) {
		strcpy(shost1,host1) ;
		input_is_local = 0;
	}
	strcpy( filename, path1 );

	l2 = rfio_parseln( filename_sav , &host2, &path2, NORDLINKS ) ;
	if (l2 < 0) {
		fprintf(stderr,"%s\n",sstrerror(serrno));
		exit(USERR);
	}


	/* Command is of the form cp f1 f2. */
	serrno = rfio_errno = 0;
	if (strcmp(inpfile,"-") != 0) {
		rc = rfio_stat64(inpfile, &sbuf);
		if ( rc == 0 && ( S_ISDIR(sbuf.st_mode) || S_ISCHR(sbuf.st_mode)
#if !defined(_WIN32)
						  || S_ISBLK(sbuf.st_mode)
#endif
				 ) ) {
			fprintf(stderr,"file %s: Not a regular file\n",inpfile);
#if defined(_WIN32)
			WSACleanup();
#endif
			exit(USERR) ;
		} else if (rc == 0) {
			inpfile_size = (u_signed64) sbuf.st_size;
		} else {
			rfio_perror(inpfile);
#if defined(_WIN32)
			WSACleanup();
#endif
			exit(USERR) ;
		}
	} else {
		sbuf.st_mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH; /* Default open mode */
	}
	strcpy(filename,filename_sav);

	if ( ( l1 == 0 && l2 == 0 && (!memcmp(&sbuf,&sbuf2,sizeof(sbuf))) ) ||
	     ( l1 && l2 && !strcmp( shost1, host2 ) && (!memcmp(&sbuf,&sbuf2,sizeof(sbuf))) ) ) {
		fprintf(stderr,"files are identical \n");
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR) ;
	}

	/* Activate new transfer mode for source file */
	if (! v2) {
		v = RFIO_STREAM;
		rfiosetopt(RFIO_READOPT,&v,4); 
	}
	
	serrno = rfio_errno = 0;
#ifdef CNS_ROOT
	if (rfio_HsmIf_IsHsmFile(inpfile)) {
		/* The input is a CASTOR file - we need a signal handler because rfio_open() calls the stager */
#if ! defined(_WIN32)
		_rfio_signal (SIGHUP, copyfile_stgcleanup);
		_rfio_signal (SIGQUIT, copyfile_stgcleanup);
#endif
		_rfio_signal (SIGINT, copyfile_stgcleanup);
		_rfio_signal (SIGTERM, copyfile_stgcleanup);
	}
	if (rfio_HsmIf_IsHsmFile(inpfile) && (have_maxsize > 0) && (inpfile_size > 0)) {
		/* User specified a CASTOR file in input with a limited size in bytes */
		/* We cannot use rfio_open() directly because then the stager will try to */
		/* stage the full file */
		serrno = rfio_errno = 0;
		fd1 = rfio_HsmIf_open_limbysz(inpfile,O_RDONLY|binmode ,0644, (maxsize < inpfile_size) ? maxsize : inpfile_size, 1);
	} else {
#endif /* CNS_ROOT */
		serrno = rfio_errno = 0;
		if (strcmp(inpfile,"-") == 0) {
			fd1 = fileno(stdin);
		} else {
			fd1 = rfio_open64(inpfile,O_RDONLY|binmode ,0644);
		}
#ifdef CNS_ROOT
	}
#endif /* CNS_ROOT */
	if (fd1 < 0) {
		if (serrno) {
			rfio_perror(inpfile);
#ifdef CNS_ROOT
			c = (serrno == EACCES || serrno == EINVAL || serrno == ENOENT) ? USERR : SYERR;
#else
			c = SYERR;
#endif
		} else {
			switch (rfio_errno) {
			case EACCES:
			case EISDIR:
			case ENOENT:
			case EPERM :
				rfio_perror(inpfile);
				c = USERR;
				break ;
			case ENOSPC :
				rfio_perror(inpfile);
				c = ENOSPC;
				break ;
			case EBUSY :
				rfio_perror(inpfile);
				c = EBUSY;
				break ;
			case 0:
				switch(errno) {
				case EACCES:
				case EISDIR:
				case ENOENT:
				case EPERM :
					rfio_perror(inpfile);
					c = USERR;
					break;
				case ENOSPC :
					rfio_perror(inpfile);
					c = ENOSPC;
					break;
				case EBUSY :
					rfio_perror(inpfile);
					c = EBUSY;
					break;
				default:
					rfio_perror(inpfile);
					c = SYERR;
				}
				break;
			default:
				rfio_perror(inpfile);
				c = SYERR;
			}
		}
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (c);
	}
	if ( (ifce=getifnam(fd1))==NULL ) {
		strcpy(ifce1,"local");
	} else {
		strcpy(ifce1,ifce);
	}

	/* Activate new transfer mode for target file */
	if (! v2) {
		v = RFIO_STREAM;
		rfiosetopt(RFIO_READOPT,&v,4); 
	}

	serrno = rfio_errno = 0;
	fd2 = rfio_open64(filename , O_WRONLY|O_CREAT|O_TRUNC|binmode ,sbuf.st_mode & 0777);
	if (fd2 < 0) {
		if (serrno) {
			rfio_perror(outfile);
#ifdef CNS_ROOT
			c = (serrno == EACCES || serrno == EINVAL || serrno == ENOENT) ? USERR : SYERR;
#else
			c = SYERR;
#endif
		} else {
			switch (rfio_errno) {
			case EACCES:
			case EISDIR:
			case ENOENT:
			case EPERM :
				rfio_perror(outfile); 
				c = USERR;
				break;
			case ENOSPC :
				rfio_perror(outfile); 
				c = ENOSPC;
				break;
			case EBUSY :
				rfio_perror(outfile); 
				c = EBUSY;
				break;
			case 0:
				switch(errno) {
				case EACCES:
				case EISDIR:
				case ENOENT:
				case EPERM :
					rfio_perror(outfile); 
					c = USERR;
					break;
				case ENOSPC :
					rfio_perror(outfile); 
					c = ENOSPC;
					break;
				case EBUSY :
					rfio_perror(outfile); 
					c = EBUSY;
					break;
				default:
					rfio_perror(outfile);
					c = SYERR;
				}
				break;
			default:
				rfio_perror(outfile);
				c = SYERR;
			}
		}
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (c);
	}
	/*
	 * Get interfaces names
	 */
	if ( (ifce=getifnam(fd2))==NULL ) {
		strcpy(ifce2,"local");
	} else {
		strcpy(ifce2,ifce);
	}

	time(&starttime);
	size = copyfile(fd1, fd2, outfile, maxsize);

	if ( rfio_HsmIf_GetHsmType(fd1,NULL) == RFIO_HSM_CNS ) {
		if (rfio_close(fd1) < 0) {
			rfio_perror("close source");
#if defined(_WIN32)
			WSACleanup();
#endif
			exit(SYERR);
		}
	}
	if ( rfio_HsmIf_GetHsmType(fd2,NULL) == RFIO_HSM_CNS ) {
		if (rfio_close(fd2) < 0) {
			struct stat64 sbuf;
			mode_t mode;
			rfio_perror("close target");
			rfio_stat64(outfile, &sbuf);	/* check for special files */
			mode = sbuf.st_mode & S_IFMT;
			if (mode == S_IFREG) {
				rfio_unlink(filename);
			}
#if defined(_WIN32)
			WSACleanup();
#endif
			exit(SYERR);
		}
	}

	time(&endtime);
	if (size>0)  {
		char tmpbuf[21];
		char tmpbuf2[21];
		fprintf(stdout,"%s bytes in %d seconds through %s (in) and %s (out)", u64tostr(size, tmpbuf, 0), (int) (endtime-starttime),ifce1,ifce2);
		l1 = endtime-starttime ;
		if ( l1 > 0) {
			fprintf(stdout," (%d KB/sec)\n",(int) (size/(1024*l1)) );
		} else {
			fprintf(stdout,"\n");
		}
		if (have_maxsize < 0) {
			rc2 = (inpfile_size == size ? OK : ((strcmp(inpfile,"-") == 0) ? OK : SYERR));
			if (rc2 == SYERR) {
#ifdef _WIN32
				if (binmode != O_BINARY) {
					fprintf(stderr,"%s : got %s bytes instead of %s bytes\n", "Warning", u64tostr(size, tmpbuf, 0), u64tostr(inpfile_size, tmpbuf2, 0));
					rc = OK;
				} else {
#endif
				fprintf(stderr,"%s : got %s bytes instead of %s bytes\n", sstrerror(SESYSERR), u64tostr(size, tmpbuf, 0), u64tostr(inpfile_size, tmpbuf2, 0));
#ifdef _WIN32
				}
#endif
			}
		} else {
			if (maxsize < inpfile_size) { /* If input is "-", inpfile_size is zero */
				rc2 = (maxsize == size ? OK : SYERR);
				if (rc2 == SYERR) {
#ifdef _WIN32
					if (binmode != O_BINARY) {
						fprintf(stderr,"%s : got %s bytes instead of %s bytes\n", "Warning", u64tostr(size, tmpbuf, 0), u64tostr(maxsize, tmpbuf2, 0));
						rc = OK;
					} else {
#endif
						fprintf(stderr,"%s : got %s bytes instead of %s bytes\n", sstrerror(SESYSERR), u64tostr(size, tmpbuf, 0), u64tostr(maxsize, tmpbuf2, 0));
#ifdef _WIN32
					}
#endif
				}
			} else {
				rc2 = (inpfile_size == size ? OK : ((strcmp(inpfile,"-") == 0) ? OK : SYERR));
				if (rc2 == SYERR) {
#ifdef _WIN32
					if (binmode != O_BINARY) {
						fprintf(stderr,"%s : got %s bytes instead of %s bytes\n", "Warning", u64tostr(size, tmpbuf, 0), u64tostr(inpfile_size, tmpbuf2, 0));
						rc = OK;
					} else {
#endif
						fprintf(stderr,"%s : got %s bytes instead of %s bytes\n", sstrerror(SESYSERR), u64tostr(size, tmpbuf, 0), u64tostr(inpfile_size, tmpbuf2, 0));
#ifdef _WIN32
					}
#endif
				}
			}
		}
	} else if (size < 0) {
		rc2 = SYERR;
	} else {
		char tmpbuf[21];
		fprintf(stdout,"%s bytes transferred !!\n",u64tostr(size, tmpbuf, 0));
		rc2 = (inpfile_size == 0 ? OK : SYERR);
	}
#if defined(_WIN32)
	WSACleanup();
#endif
	exit(rc2);
}

off64_t copyfile(fd1, fd2, name, maxsize)
	int fd1, fd2;
	u_signed64 maxsize;
	char *name;
{
	int n;
	mode_t mode;
	int m = -1;
	struct stat64 sbuf;
	off64_t effsize=0;
	char  *p;
	int bufsize = TRANSFER_UNIT;
	char * cpbuf;
	extern char *getenv();		/* External declaration */
	u_signed64 total_bytes = 0;
	
	if ((p=getenv("RFCPBUFSIZ")) == NULL) {
		if ((p=getconfent("RFCP","BUFFERSIZE",0)) == NULL) {
			bufsize=TRANSFER_UNIT;
		} else  {
			bufsize=atoi(p);
		}
	} else {
		bufsize=atoi(p);
	}
	if (bufsize<=0) bufsize=TRANSFER_UNIT;
	
	if ( ( cpbuf = malloc(bufsize) ) == NULL ) {
		perror("malloc");
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (SYERR);
	}

	do {
		if (have_maxsize != -1) {
			if ((maxsize - total_bytes) < bufsize)
				bufsize = maxsize - total_bytes;
		}
		serrno = rfio_errno = 0;
		n = rfio_read(fd1, cpbuf, bufsize);
		if (n > 0) {
			int count = 0;

			total_bytes += n;
			effsize += n;
			serrno = rfio_errno = 0;
			while (count != n && (m = rfio_write(fd2, cpbuf+count, n-count)) > 0) {
				count += m;
			}
			if (m < 0) {
				int save_err = rfio_serrno();
				/* Write failed.  Don't keep truncated regular file. */
				rfio_perror("rfcp");
				if ( rfio_HsmIf_GetHsmType(fd2,NULL) != RFIO_HSM_CNS ) {
					if (rfio_close(fd2) < 0) {
						rfio_perror("close target");
					}
				}
				rfio_stat64(name, &sbuf);	/* check for special files */
				mode = sbuf.st_mode & S_IFMT;
				if (mode == S_IFREG) rfio_unlink(name);
#if defined(_WIN32)
				WSACleanup();
#endif
				free(cpbuf);
				exit((save_err == ENOSPC) ? ENOSPC : ((serrno == EBUSY) ? EBUSY : SYERR));
			}
		} else {
			if (n < 0) {
				rfio_perror("cp: read error");
				break;
			}
		}
	} while ((total_bytes != maxsize) && (n > 0));
	serrno = rfio_errno = 0;
	if (rfio_HsmIf_GetHsmType(fd1,NULL) != RFIO_HSM_CNS && rfio_close(fd1) < 0) {
		rfio_perror("close source");
#if defined(_WIN32)
		WSACleanup();
#endif
		free(cpbuf);
		exit(SYERR);
	}

	serrno = rfio_errno = 0;
	if (rfio_HsmIf_GetHsmType(fd2,NULL) != RFIO_HSM_CNS && rfio_close(fd2) < 0) {
		rfio_perror("close target");
		rfio_stat64(name, &sbuf);	/* check for special files */
		mode = sbuf.st_mode & S_IFMT;
		if (mode == S_IFREG) {
			rfio_unlink(name);
		}
#if defined(_WIN32)
		WSACleanup();
#endif
		free(cpbuf);
		exit(SYERR);
	}
	
	free(cpbuf);
	return(effsize);
}

void usage()
{
	fprintf(stderr,"Usage:  rfcp [-s maxsize] [-v2] f1 f2; or rfcp f1 <dir2>\n");
	exit(USERR);
}

#ifdef CNS_ROOT
void copyfile_stgcleanup(sig)
	int sig;
{
	TRACE(2,"rfio","copyfile_stgcleanup: Received signal No %d", sig);
}
#endif /* CNS_ROOT */

#ifndef _WIN32
Sigfunc *_rfio_signal(signo, func)
	int signo;
	Sigfunc *func;
{
	struct sigaction	act, oact;
	int n = 0;

	act.sa_handler = func;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	if (signo == SIGALRM) {
#ifdef	SA_INTERRUPT
		act.sa_flags |= SA_INTERRUPT;	/* SunOS 4.x */
#endif
	} else {
#ifdef	SA_RESTART
		act.sa_flags |= SA_RESTART;		/* SVR4, 44BSD */
#endif
	}
	n = sigaction(signo, &act, &oact);
	if (n < 0) {
		return(SIG_ERR);
	}
	switch (signo) {
#if ! defined(_WIN32)
	case SIGHUP:
		TRACE(2,"rfio","_rfio_signal: Trapping SIGHUP");
		break;
	case SIGQUIT:
		TRACE(2,"rfio","_rfio_signal: Trapping SIGQUIT");
		break;
#endif
	case SIGINT:
		TRACE(2,"rfio","_rfio_signal: Trapping SIGINT");
		break;
	case SIGTERM:
		TRACE(2,"rfio","_rfio_signal: Trapping SIGTERM");
		break;
	default:
		TRACE(2,"rfio","_rfio_signal: Trapping signal No %d", signo);
		break;
	}
	return(oact.sa_handler);
}
#endif /* #ifndef _WIN32 */
