/*
 * $Id$
 */

/*
 * Copyright (C) 2001-2011 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: getc.c,v $ $Revision$ $Date$ CERN/IT/PDP/DM Jean-Philippe Baud";
#endif /* not lint */

/* get.c        Remote File I/O - input of one character                */

/*
 * System remote file I/O definitions
 */
#define RFIO_KERNEL     1 
#include "rfio.h"    
#include "rfio_rfilefdt.h"
#include <stdlib.h>

/*
 * Remote file read
 */
int DLL_DECL rfio_getc(fp)  
	RFILE   *fp;                    /* remote file pointer          */
{
	unsigned char	c ;
	int	rc ;

	INIT_TRACE("RFIO_TRACE");
	TRACE(1, "rfio", "rfio_getc(%x)", fp);

	/*
	 * Checking fp validity
	 */
	if ( fp == NULL ) {
		errno = EBADF ;
		TRACE(2,"rfio","rfio_getc() : FILE ptr is NULL ") ;
		END_TRACE() ;
		return EOF ;
	}

	if (rfio_rfilefdt_findptr(fp,FINDRFILE_WITH_SCAN) == -1) {
		TRACE(2,"rfio","rfio_getc() : using local getc() ") ;
		rfio_errno = 0;
		rc= getc((FILE *)fp) ;
		if ( rc == EOF ) serrno = 0;
		END_TRACE() ; 
		return rc ;
	}

	TRACE(2,"rfio","rfio_getc() : ------------>2") ;

	/*
	 * Checking magic number
	 */
	if ( fp->magic != RFIO_MAGIC) {
		int fps = fp->s;
		serrno = SEBADVERSION ; 
		TRACE(2,"rfio","rfio_getc() : Bad magic number  ") ;
		free((char *)fp);
		(void) close(fps) ;
		END_TRACE();
		return EOF ;
	}

	/*
	 * The file is remote 
	 */
	rc= rfio_read(fp->s,&c,1) ;
	switch(rc) {
		case -1:
#ifdef linux
			((RFILE *)fp)->eof |= _IO_ERR_SEEN ;
#else
#if defined( __APPLE__)
			((RFILE *)fp)->eof |= __SERR ;
#else
#ifdef __Lynx__
			((RFILE *)fp)->eof |= _ERR ;
#else
			((RFILE *)fp)->eof |= _IOERR ;
#endif
#endif
#endif
			rc= EOF ; 
			break ; 
		case 0:
#ifdef linux
			((RFILE *)fp)->eof |= _IO_EOF_SEEN ; 
#else
#if defined( __APPLE__)
			((RFILE *)fp)->eof |= __SEOF ; 
#else
#ifdef __Lynx__
			((RFILE *)fp)->eof |= _EOF ; 
#else
			((RFILE *)fp)->eof |= _IOEOF ; 
#endif
#endif
#endif
			rc= EOF ; 
			break ; 
		default:
			rc= (int) c ;
			break ; 
	}
	END_TRACE() ;
	return rc ; 
}
