.\"
.\" $Id: rfio_lockf.man,v 1.1 2005/03/31 13:13:03 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_lockf.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:03 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_LOCKF 3 "$Date: 2005/03/31 13:13:03 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_lockf \- provide record locking on files
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_lockf (int " sd ", int " op ", long " siz ");"
.sp
Under Linux, for large files:
.br
.B #define _LARGEFILE64_SOURCE
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_lockf64 (int " sd ", int " op ", off64_t " siz ");"
.br
.sp
For large files, under other systems:
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_lockf64 (int " sd ", int " op ", off64_t " siz ");"
.SH DESCRIPTION
.B rfio_lockf
restricts access to regions of files to the calling process.
.LP
.I sd
is the file descriptor returned by a previous
.BR rfio_open .
.LP
.I op
specifies the action to be taken:
.RS
.TP 1.3i
.B F_ULOCK
unlock a region.
.TP
.B F_LOCK
lock a region.
.TP
.B F_TLOCK
test and lock a region.
.TP
.B F_TEST
test a region for other locks.
.RE
.LP
.I siz
gives the size of the region to be locked, starting at the current file offset.
.sp
.B rfio_lockf64 
must be used for large files and uses an off64_t
instead of a long to specify the size of the region to be locked.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
.I sd
is not a valid descriptor.
.TP
.B EACCES
The file region is locked and F_TEST or F_TLOCK was specified.
.TP
.B EINVAL
incorrect value for
.I op
or
.IR siz .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SETIMEDOUT
Timed out.
.TP
.B SECONNDROP
Connection closed by remote end.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR rfio_open(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
