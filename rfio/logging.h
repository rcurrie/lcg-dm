/*
 * $Id: logging.h,v 1.1 2005/03/31 13:13:01 baud Exp $
 *
 * $Log: logging.h,v $
 * Revision 1.1  2005/03/31 13:13:01  baud
 * imported from CASTOR
 *
 * Revision 1.2  1999/07/20 12:48:01  jdurand
 * 20-JUL-1999 Jean-Damien Durand
 *   Timeouted version of RFIO. Using netread_timeout() and netwrite_timeout
 *   on all control and data sockets.
 *
 */

/*
 * Copyright (C) 1993 by CERN/CN/SW/DC
 * All rights reserved
 */
#ifndef lint
/* $static char sccsid[] = "@(#)logging.h	1.1 11/15/93 CERN CN-SW/DC F. Collin" ; */
#endif /* not lint */


/* logging.h : Some definitions for logging.c
 A facility for logging client I/O primitives 
 The logging filename is defined by RFIO/CLIENTLOG in shift.conf 
 If this variable is not defined, no client logging is done 
 To dump a client log, use dump_log */

#define FALSE 0
#define TRUE 1

#define LOG_OPEN       0
#define LOG_RECORD     1
#define LOG_LSEEK_SET  2
#define LOG_LSEEK_CUR  3
#define LOG_LSEEK_END  4
#define LOG_READ       5
#define LOG_WRITE      6
#define LOG_ERRORS     7

#define ERR_RECORD_FULL 1

#define SYSLOG_OPEN 1
#define SYSLOG_WRONG_FD 2
#define SYSLOG_WRITE 3
#define SYSLOG_CLOSE 4
#define SYSLOG_MALLOC 5
#define SYSLOG_OVERFLOW 6


typedef struct {
  unsigned command:3;         /* LOG_OPEN */
  unsigned uid:16;            /* user id */
  unsigned local:1;           /* local or remote file operation */
  int flags;                  /* open flags */
  char machine[9];            /* Machine name if remote operation */
  long date;                  /* Date at which open occured */
  long incarnation_date;      /* Unique number identifying the client application */
  short incarnation_pid;
  int size;                   /* Size of the file beeing opened */
  time_t session_real_time;   /* Real execution time of the session */
  clock_t session_user_time;  /* User CPU time of the session */
  clock_t session_sys_time;   /* System CPU time of the session */
  } OPEN_BUFFER;

typedef struct {
  unsigned command:3;         /* LOG_LSEEK_SET or CUR or END */
  unsigned offset:29;         /* Lseek offset */
  } LSEEK_BUFFER;

typedef struct {
  unsigned command:3;         /* LOG_READ */
  unsigned count:16;          /* Number of successive read performed with the same record size */
  unsigned record:13;         /* Record size index (see record size table) */
  } READ_BUFFER;

typedef struct {
  unsigned command:3;         /* LOG_WRITE */
  unsigned count:16;          /* Number of successive write performed with the same record size */
  unsigned record:13;         /* Record size index (see record size table) */
  } WRITE_BUFFER;

typedef union {
  LSEEK_BUFFER lseek;         /* One item of the operation table */
  READ_BUFFER read;
  WRITE_BUFFER write;
  } OPERATION;

typedef struct {
  unsigned command:3;         /* LOG_ERROR */
  unsigned num:29;            /* Error numbers */
  } ERROR_BUFFER;

typedef struct {
  unsigned command:3;         /* LOG_RECORD */
  unsigned length:29;         /* Size of record size table */
  } RECORD_BUFFER;

#define MAX_RECORD_TABLE 128  /* The record size table is limited to 128 items */








