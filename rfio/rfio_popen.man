.\"
.\" $Id: rfio_popen.man,v 1.2 2008/09/24 11:25:01 dhsmith Exp $
.\"
.\" @(#)$RCSfile: rfio_popen.man,v $ $Revision: 1.2 $ $Date: 2008/09/24 11:25:01 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2008 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_POPEN 3 "$Date: 2008/09/24 11:25:01 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_popen, rfio_pclose \- start a process and open a pipe to it
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "FILE *rfio_popen (const char *" command ", const char *" type ");"
.sp
.BI "int rfio_pclose (FILE *" fs ");"
.SH DESCRIPTION
.B rfio_popen
starts a process and opens a pipe to it.
.LP
.I command
is a pointer to a string specifying the shell command to be executed.
.P
.I type
is a mode indicator for the pipe. One of the characters "r" or "w".
.LP
.B rfio_pclose
waits for the forked process to terminate and returns the exit status of the
command.
.SH EXAMPLE
.nf
.ft CW
	int c;
	FILE *rf;

	rf = rfio_popen (command, "r");
	if (rf == NULL) {
		rfio_perror ("rfio_popen");
		exit (1);
	}
	while ((c = rfio_pread (buf, 1, sizeof(buf), rf)) > 0) {
		...
	}
	c = rfio_pclose (rf);
.ft
.fi
.SH RETURN VALUE
This routine returns NULL if the operation failed or a non-NULL pointer to a FILE structure if the operation was successful. If it fails, \fBserrno\fP variable is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
Not enough memory.
.TP
.B EINVAL
The mode provided is invalid.
.TP
.B ECONNRESET
Connection reset by peer
.TP
.B ETIMEDOUT
Connection timed out
.TP
.B ECONNREFUSED
Connection refused
.TP
.B EHOSTUNREACH
No route to host
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SEUMSG2LONG
Command string too long.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR rfio_pread(3) ,
.BR rfio_pwrite(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
