/*
 * $Id$
 */

/*
 * Copyright (C) 1990-2010 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rfstatfs.c,v $ $Revision$ $Date$ CERN/IT/PDP/DM Felix Hassine";
#endif /* not lint */

#define RFIO_KERNEL 1
#if !defined(_WIN32)
#include <sys/param.h>
#if defined(SOLARIS)
#include <sys/types.h>
#include <sys/statvfs.h>
#endif
#if defined(_AIX) || defined(__Lynx__)
#include <sys/types.h>
#endif
#if defined(sgi) || defined( _AIX ) || defined(__Lynx__)
#include <sys/statfs.h>
#endif
#if (defined(__osf__) && defined(__alpha)) || defined( __APPLE__)
#include <sys/mount.h>
#endif
#if defined( hpux ) || defined(linux)
#include <sys/vfs.h>
#endif
#endif
#include <rfio.h>

int DLL_DECL rfstatfs(path, statfsbuf )
char *path ;
struct rfstatfs *statfsbuf ;
{
	int status = 0   ;

#if defined(_WIN32)
	DWORD bps, nfc, spc, tnc;
	char *p, *rootpath;
	char pathbuf[256];
#else
#if defined( SOLARIS )
	static struct statvfs fsbuffer ;
#else
        static struct statfs fsbuffer;
#endif
#endif
#if defined(__osf__) && defined(__alpha)
            if ( statfs(path,&fsbuffer,(int)sizeof(struct statfs)) < 0) {
                status = -1;
            }
#endif
#if defined( sgi )
            if ( statfs(path,&fsbuffer,(int)sizeof(struct statfs),(int)0) < 0) {
                status = -1;
            }
#endif
#if defined( _AIX ) || defined( hpux ) || defined(linux) || defined( __APPLE__)
            if ( statfs(path,&fsbuffer) < 0 ) {
                status = -1;
            }
#endif
#if defined ( SOLARIS )
	    if (statvfs (  path, &fsbuffer ) < 0) {
		status = -1;
	    }
#endif
#if defined(__Lynx__)
            if ( statfs(path,&fsbuffer,sizeof(struct statfs),0) < 0 ) {
                status = -1;
            }
#endif
#if defined(_WIN32)
	    if (*(path+1) == ':') {		/* drive name */
		pathbuf[0] = *path;
		pathbuf[1] = *(path+1);
		pathbuf[2] = '\\';
		pathbuf[3] = '\0';
		rootpath = pathbuf;
	    } else if (*path == '\\' && *(path+1) == '\\') {	/* UNC name */
		if ((p = strchr (path+2, '\\')) == 0)
		    return (-1);
		if (p = strchr (p+1, '\\'))
		    strncpy (pathbuf, path, p-path+1);
		else {
		    strcpy (pathbuf, path);
		    strcat (pathbuf, "\\");
		}
		rootpath = pathbuf;
	    } else
		rootpath = NULL;
	    if (GetDiskFreeSpace (rootpath, &spc, &bps, &nfc, &tnc) == 0) {
		status = -1;
	    }
#endif

        /*
         * Affecting variables
         */

	if  ( status == 0 ) {
#if defined( sgi ) || defined( __Lynx__ )
           statfsbuf->freeblks = (long)fsbuffer.f_bfree;
           statfsbuf->bsize = (long)fsbuffer.f_bsize ;
           statfsbuf->totblks = (long)fsbuffer.f_blocks;
           statfsbuf->totnods = (long)fsbuffer.f_files ;
           statfsbuf->freenods = (long)fsbuffer.f_ffree ;
#endif
#if defined(SOLARIS)
           statfsbuf->freeblks = (long)fsbuffer.f_bavail;
           statfsbuf->bsize = (long)fsbuffer.f_frsize ;
           statfsbuf->totblks = (long)fsbuffer.f_blocks;
           statfsbuf->totnods = (long)fsbuffer.f_files ;
           statfsbuf->freenods = (long)fsbuffer.f_ffree ;
#endif
#if defined( hpux ) || defined( _AIX ) || defined(linux) || defined( __APPLE__)
           statfsbuf->totblks = (long)fsbuffer.f_blocks;
           statfsbuf->freeblks = (long)fsbuffer.f_bavail;
           statfsbuf->totnods = (long)fsbuffer.f_files ;
           statfsbuf->freenods = (long)fsbuffer.f_ffree ;
           statfsbuf->bsize = (long)fsbuffer.f_bsize;
#endif
#if defined(__osf__) && defined(__alpha)
           statfsbuf->totblks = (long)fsbuffer.f_blocks;
           statfsbuf->freeblks = (long)fsbuffer.f_bavail;
           statfsbuf->totnods = (long)fsbuffer.f_files ;
           statfsbuf->freenods = (long)fsbuffer.f_ffree ;
           statfsbuf->bsize = (long)fsbuffer.f_fsize ;
#endif
#if defined(_WIN32)
           statfsbuf->totblks = (long)tnc;
           statfsbuf->freeblks = (long)nfc;
           statfsbuf->totnods = (long)-1;
           statfsbuf->freenods = (long)-1;
           statfsbuf->bsize = (long)(spc * bps);
#endif
 	}
	return status ;

}

int DLL_DECL rfstatfs64(path, statfsbuf )
char *path ;
struct rfstatfs64 *statfsbuf ;
{
	int status = 0   ;

#if defined(_WIN32)
	DWORD bps, nfc, spc, tnc;
	char *p, *rootpath;
	char pathbuf[256];
#else
#if defined( SOLARIS )
	static struct statvfs64 fsbuffer ;
#else
#if defined(AIX) || defined(linux)
        static struct statfs64 fsbuffer;
#else
        static struct statfs fsbuffer;
#endif
#endif
#endif
#if defined(__osf__) && defined(__alpha)
            if ( statfs(path,&fsbuffer,(int)sizeof(struct statfs)) < 0) {
                status = -1;
            }
#endif
#if defined( sgi )
            if ( statfs(path,&fsbuffer,(int)sizeof(struct statfs),(int)0) < 0) {
                status = -1;
            }
#endif
#if defined( _AIX ) || defined(linux)
            if ( statfs64(path,&fsbuffer) < 0 ) {
                status = -1;
            }
#endif
#if defined( hpux ) || defined( __APPLE__)
            if ( statfs(path,&fsbuffer) < 0 ) {
                status = -1;
            }
#endif
#if defined ( SOLARIS )
	    if (statvfs64 (  path, &fsbuffer ) < 0) {
		status = -1;
	    }
#endif
#if defined(__Lynx__)
            if ( statfs(path,&fsbuffer,sizeof(struct statfs),0) < 0 ) {
                status = -1;
            }
#endif
#if defined(_WIN32)
	    if (*(path+1) == ':') {		/* drive name */
		pathbuf[0] = *path;
		pathbuf[1] = *(path+1);
		pathbuf[2] = '\\';
		pathbuf[3] = '\0';
		rootpath = pathbuf;
	    } else if (*path == '\\' && *(path+1) == '\\') {	/* UNC name */
		if ((p = strchr (path+2, '\\')) == 0)
		    return (-1);
		if (p = strchr (p+1, '\\'))
		    strncpy (pathbuf, path, p-path+1);
		else {
		    strcpy (pathbuf, path);
		    strcat (pathbuf, "\\");
		}
		rootpath = pathbuf;
	    } else
		rootpath = NULL;
	    if (GetDiskFreeSpace (rootpath, &spc, &bps, &nfc, &tnc) == 0) {
		status = -1;
	    }
#endif

        /*
         * Affecting variables
         */

	if  ( status == 0 ) {
#if defined( sgi ) || defined( __Lynx__ )
           statfsbuf->freeblks = (signed64)fsbuffer.f_bfree;
           statfsbuf->bsize = (long)fsbuffer.f_bsize ;
           statfsbuf->totblks = (signed64)fsbuffer.f_blocks;
           statfsbuf->totnods = (signed64)fsbuffer.f_files ;
           statfsbuf->freenods = (signed64)fsbuffer.f_ffree ;
#endif
#if defined(SOLARIS)
           statfsbuf->freeblks = (signed64)fsbuffer.f_bavail;
           statfsbuf->bsize = (long)fsbuffer.f_frsize ;
           statfsbuf->totblks = (signed64)fsbuffer.f_blocks;
           statfsbuf->totnods = (signed64)fsbuffer.f_files ;
           statfsbuf->freenods = (signed64)fsbuffer.f_ffree ;
#endif
#if defined( hpux ) || defined( _AIX ) || defined(linux) || defined( __APPLE__)
           statfsbuf->totblks = (signed64)fsbuffer.f_blocks;
           statfsbuf->freeblks = (signed64)fsbuffer.f_bavail;
           statfsbuf->totnods = (signed64)fsbuffer.f_files ;
           statfsbuf->freenods = (signed64)fsbuffer.f_ffree ;
           statfsbuf->bsize = (long)fsbuffer.f_bsize;
#endif
#if defined(__osf__) && defined(__alpha)
           statfsbuf->totblks = (signed64)fsbuffer.f_blocks;
           statfsbuf->freeblks = (signed64)fsbuffer.f_bavail;
           statfsbuf->totnods = (signed64)fsbuffer.f_files ;
           statfsbuf->freenods = (signed64)fsbuffer.f_ffree ;
           statfsbuf->bsize = (long)fsbuffer.f_fsize ;
#endif
#if defined(_WIN32)
           statfsbuf->totblks = (signed64)tnc;
           statfsbuf->freeblks = (signed64)nfc;
           statfsbuf->totnods = (signed64)-1;
           statfsbuf->freenods = (signed64)-1;
           statfsbuf->bsize = (long)(spc * bps);
#endif
 	}
	return status ;

}
