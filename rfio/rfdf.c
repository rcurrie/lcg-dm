#include <stdio.h>
#include <stdlib.h>
#include "rfio_api.h"
#include "u64subr.h"
main(int argc, char **argv)
{
	int errflg = 0;
	int i;
	struct rfstatfs64 statfsbuf;
	char tmpbuf[21];

	if (argc == 1) {
		fprintf (stderr, "usage: %s path ...\n", argv[0]);
		exit (1);
	}
	for (i = 1; i < argc; i++) {
		if (rfio_statfs64 (argv[i], &statfsbuf) < 0) {
			fprintf (stderr, "%s: '%s': %s\n", argv[0], argv[i],
			    rfio_serror());
			errflg++;
		} else {
			printf ("%s\n", argv[i]);
			printf ("\tblocksize    : %d\n", statfsbuf.bsize);
			printf ("\ttotal blocks : %s\n", i64tostr (statfsbuf.totblks, tmpbuf, 0));
			printf ("\tfree blocks  : %s\n", i64tostr (statfsbuf.freeblks, tmpbuf, 0));
			printf ("\ttotal inodes : %s\n", i64tostr (statfsbuf.totnods, tmpbuf, 0));
			printf ("\tfree inodes  : %s\n", i64tostr (statfsbuf.freenods, tmpbuf, 0));
		}
	}
	exit (errflg ? 1 : 0);
}
