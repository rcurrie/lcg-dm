/*
 * $Id$
 */

/*
 * Copyright (C) 1990-2010 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: parse.c,v $ $Revision$ $Date$ CERN/IT/PDP/DM Frederic Hemmer";
#endif /* not lint */

/* parse.c      Remote File I/O - parse file path                       */

#define RFIO_KERNEL     1       /* system part of Remote File I/O       */

#if !defined(_WIN32)
#include <sys/param.h>		/* system dependent parameters		*/
#endif
#include <string.h>
#include <Castor_limits.h>
#include "rfio.h"               /* remote file I/O definitions          */
#include <Cglobals.h>
#include <dpns_api.h>
extern int Cdomainname (char *, int);
extern char *getconfent();

static int name0_key = -1;
static int name1_key = -1;
static int buffer_key = -1;

static int islocalhost(localhost, host)
char *localhost;
char *host;
{
   char *cp1, *cp2;
   char localdomain[CA_MAXHOSTNAMELEN+1];

   if (strcmp(localhost, host) == 0 || strcmp("localhost", host) == 0)
      return(1);
   cp1 = strchr(localhost, '.');
   cp2 = strchr(host, '.');
   if ((cp1 == NULL && cp2 == NULL) || (cp1 && cp2))
      return(0);
   if (cp1) {
      if (strncmp(localhost, host, cp1-localhost) == 0 &&
        strlen(host) == (cp1-localhost))
         return(1);
      else
         return(0);
   }
   if (strncmp(localhost, host, cp2-host) || strlen(localhost) != (cp2-host))
      return(0);
   if (Cdomainname(localdomain, sizeof(localdomain)) < 0)
      return(-1);
   if (strcmp(cp2+1, localdomain) == 0)
      return(1);
   return(0);
}

int DLL_DECL rfio_parseln(name, host, path, ln) /* parse name to host and path  */
char    *name;
char    **host;
char    **path;
int   	ln ; 	/* solve links or not ? */
{
   char    *cp1, *cp2, *cp3;
   char *dp;
   register int i;
   char	localhost[CA_MAXHOSTNAMELEN+1];
   int 	n = 0;
   char 	*name_0 = NULL;
   char 	*name_1 = NULL;
   char  	*buffer = NULL;    /* To hold temporary strings */
   char		*cwd_server;       /* Current CASTOR working directory server */

   if ( name == NULL ) {
     serrno = EINVAL;
     return(-1);
   }
   
   /* check if it is a TURL */

   if (strncmp (name, "rfio://", 7) == 0) {
      name += 7;
      if (*name != '/') {	/* Hostname is present */
         if ((cp1 = strchr (name, '/')) == NULL || (cp1 - name ) >= CA_MAXHOSTNAMELEN+6) {
            serrno = EINVAL;
            return (-1);
         }
         Cglobals_get (&name0_key, (void**)&name_0, CA_MAXHOSTNAMELEN+6);
         strncpy (name_0, name, cp1-name);
         *(name_0+(cp1-name)) = '\0';
         cp2 = strrchr (name_0, ':');
         /* checks for validy of port in the hostname
          * The port number is expect to follow a hostname or IP address
          * IPv4 [IPv4] [IPv6]
          */
         if (cp2 && (strchr (name_0, ':')==cp2 ||
                      (cp2-name_0>4 && *(cp2-1) == ']'))) {	/* There is a port number */
            if (strtol (cp2+1, &dp, 10) < 0 || *dp != '\0') {
               serrno = EINVAL;
               return (-1);
            }
         }
         if (*(cp1+1) == '/')
            cp1++;
         *host = name_0;
         *path = cp1;
         return (1);
      } else {
         if (*(name+1) == '/')
            name++;
      }
   }

/*
 * forms recognized: host:<unix path name>
 *                   /<prefix>/host/<unix path name>
 *
 * where prefix is defined in /etc/shift.conf :
 *      RFIO  NFS_ROOT          /.....
 */

   if ( rfioreadopt(RFIO_CONNECTOPT) == RFIO_FORCELOCAL )  {
      TRACE (2,"rfio","rfio_parseln(): Forcing local calls");
      *host = NULL;
      *path = name;
      return(0) ;
   }

   if (strlen(name) > CA_MAXPATHLEN) {
     /* Not enough space */
     serrno = SENAMETOOLONG;
     return(-1);
   }
   if (gethostname(localhost, sizeof(localhost)) < 0)	{
      return(-1);
   }

   Cglobals_get(&name1_key, (void**)&name_1, CA_MAXPATHLEN+1);
   Cglobals_get(&buffer_key, (void**)&buffer, CA_MAXPATHLEN+1);
   
   if (ln != NORDLINKS ) {
      int countln = 0 ;
      char     buffer1[CA_MAXPATHLEN+1],buffer2[CA_MAXPATHLEN+1];

      strcpy ( buffer1, name );
#if defined(FollowRtLinks)
      while ( (n= rfio_readlink(buffer1,buffer,CA_MAXPATHLEN+1)) > 0  && countln < 4 ) 
#else
#if !defined(_WIN32)
	 while ( (n= readlink(buffer1,buffer,CA_MAXPATHLEN+1)) > 0  && countln < 4 ) 
#else
	    if (countln) 	/* i.e. never, because links are not impl. */
#endif
#endif /* FollowRtLinks */
	    {
	       /* save original filename */
			if (strlen(buffer1) > CA_MAXPATHLEN) {
				/* Not enough space */
				serrno = SENAMETOOLONG;
				return(-1);
			}
	       strcpy(buffer2,buffer1);
	       countln ++ ;
	       buffer[n]='\0';
	       /* Is it a relative path ? */
	       if (buffer[0] != '/')  {
		  char *last_slash;
		  /* Is the relative link of the form host:/path ? */
		  if (strstr(buffer,":/") != NULL) {
		     /* Simply replace name */
		     strcpy(buffer1,buffer);
		  }  else  {
		     /* Host name is NOT like XXXX:/YYYYY */
		     if (strchr(buffer,'$') != NULL &&
			 (cp3 = (char *)strchr(buffer,':')) != NULL ) {
			/* This is a VMS syntax ! */
			strcpy(buffer1,buffer);
		     }
		     if ((last_slash =  strrchr(buffer1,'/')) == NULL) {
			/* Simply replace name */
			strcpy(buffer1,buffer);
		     }  else  {
			/* Concat basename of old name with new name */
			strcpy(last_slash + 1,buffer);
		     }
		  }
	       }    else
		  strcpy(buffer1, buffer);
	       TRACE (3,"rfio","rfio_parseln(): %s converted to %s",buffer2,buffer1);	
	    }
      strcpy(name_1,buffer1);
   }
   else
      strcpy(name_1,name);

   if (((cp1 = (char *)strstr(name_1,":/")) == NULL) || (strchr(name_1,'/') < (cp1+1))  ){
      /* Host name is NOT like XXXX:/YYYYY */
	   /* The other case: it contains ":/". So we a sure that a strchr() of character "/" will not */
	   /* return NULL - we then demand that there is no other "/" character before the one at cp1+1... */
      cp1 = name_1;
      if ( strchr(name_1,'$') != NULL && (cp3=(char *)strchr(name_1,':')) != NULL ) {
	 /* This is a VMS syntax ! */
	 *host= name_1 ;
	 *path= cp3+1 ;
	 cp3[0]='\0' ;
	 return (1) ;
      }
#if defined(CNS_ROOT)
      TRACE(3,"rfio","rfio_parseln() check %s against root %s",
            name_1,CNS_ROOT);
      if ( rfio_HsmIf_IsCnsFile(name_1) > 0 ) {
         cwd_server = rfio_HsmIf_GetCwdServer();
         *host = NULL;
         *path = NULL;
         *buffer = '\0';
         TRACE(3,"rfio","rfio_parseln() call Cns_selectsrvr(%s,0x%lx,0x%lx,0x%lx)",
               name_1,cwd_server,buffer,path);
               
	 if ( Cns_selectsrvr(name_1,cwd_server,buffer,path) == -1 ) {
             TRACE(3,"rfio","rfio_parseln() Cns_selectsrvr(): %s",
                   sstrerror(serrno));
	    *host = NULL;
	    *path = name_1;
	    return(-1);
	 }
         *host = buffer;
         TRACE(3,"rfio","rfio_parseln() Cns_selectsrvr() returns host=%s, path=%s",
               *host,*path);
	 return(0);
      }
#endif /* CNS_ROOT */
	  {
		  int sav_serrno = serrno;
		  cp2 = getconfent("RFIO","NFS_ROOT", 0);
#ifdef NFSROOT
		  if (cp2 == NULL) {
			  TRACE(3,"rfio","rfio_parseln() Using site-wide NFS ROOT \"%s\"", NFSROOT);
			  cp2 = NFSROOT;
			  /* Restore serrno */
			  serrno = sav_serrno;
		  }
#endif
	  }
	  if (cp2 == NULL)     {
	 *host = NULL;
	 *path = name_1;
	 serrno=0; /* reset error */
	 return(0);
      }
      /* compare the 2 strings        */
      for (i=0;i< (int)strlen(cp2);i++)     {
	 if (*(cp1+i) != *(cp2+i))       {
	    *host = NULL;
	    *path = name_1;
	    return(0);
	 }
      }
      cp1 += strlen(cp2);
      /* cp2 may be matching cp1 but being shorted */
      if (*cp1 != '/')        {
	 *host = NULL;
	 *path = name_1;
	 return(0);
      }
      /* cp1 now points to rooted nfs_path_name     */
      /* next token must be the node name prefixed by '/' */
      if ((cp2 = strchr(++cp1,'/')) == NULL)   {
	 /* incomplete name: /prefix/node  */
	 *host = NULL;
	 *path = name_1;
	 return(0);
      }

      strncpy(buffer,cp1,(int)(cp2-cp1));
      buffer[cp2-cp1] = '\0';
      *host = buffer;
      *path = name_1;
      /* is it localhost ? */
      if (islocalhost(localhost, buffer)) {
	 *host = NULL;
	 return(0);
      }
      else    {
	 return(1);
      }
   }
   else    {
      char *cp4, *cp5;
      /* first check if the path is in DOS format, e.g. hostname:x:/path/name
       * hostname may also be an IP address of the form:
       * IPv4 [IPv4] [IPv6]
       * port specification is not handled
       */
      cp2 = NULL; cp3 = NULL; cp4 = strchr(name_1, ':');
      while(cp4 && cp4<cp1) {
         cp2 = cp3; cp3 = cp4;
         cp4 = strchr(cp4+1, ':');
      }
      if (cp2) cp5 = strchr(cp2+1, ']');
      if( cp3 && cp1 == cp3+2 && (cp2 == NULL || (cp5 && cp5<cp3))) {
	 strncpy(buffer, name_1, cp3-name_1);
	 buffer[cp3-name_1] = '\0';
	 *host = buffer;
	 *path = cp3+1;
      }  else  {
	 strncpy(buffer,name_1,cp1-name_1);
	 buffer[cp1-name_1] = '\0';
	 *host = buffer;
	 *path = cp1+1;
      }
	 /* is it localhost ? */
	 if (islocalhost(localhost, buffer)) {
	    *host = NULL;
	    return(0);
	 }
	 else    {
	    return(1);
	 }
   }
}

int DLL_DECL rfio_parse(name, host, path)  /* parse name to host and path  */
char    *name;
char    **host;
char    **path;
{
   return ( rfio_parseln(name, host, path, RDLINKS) );
}
