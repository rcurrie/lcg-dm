/*
 * $Id: sleep.c,v 1.1 2005/04/13 17:00:30 baud Exp $
 */

/*
 * Copyright (C) 1997 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: sleep.c,v $ $Revision: 1.1 $ $Date: 2005/04/13 17:00:30 $ CERN/IT/PDP/DM Jean-Philippe Baud";
#endif /* not lint */

#include <windows.h>
unsigned int
sleep(seconds)
unsigned int seconds;
{
	DWORD ms = 1000 * seconds;
	Sleep (ms);
	return (0);
}
