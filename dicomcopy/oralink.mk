#
#  Copyright (C) 2007 by CERN/IT/GD/ITR
#  All rights reserved
#
#       @(#)$RCSfile: oralink.mk,v $ $Revision: 1.1 $ $Date: 2008/01/14 10:06:10 $ CERN IT-GD/ITR Jean-Philippe Baud
 
#    Link dicomcopyd with Oracle libraries.

ifdef ORACLE_SHARE
include $(ORACLE_SHARE)/demo_proc_ic.mk
else
include $(ORACLE_HOME)/precomp/lib/env_precomp.mk
endif

ifdef ORACLE_LIB
LIBHOME=$(ORACLE_LIB)
endif

PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL) $(STATICTTLIBS)
PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL)

dicomcopyd: $(SRV_OBJS)
	$(CC) -o dicomcopyd $(CLDFLAGS) $(SRV_OBJS) $(LIBS) -L$(LIBHOME) $(PROLDLIBS)
