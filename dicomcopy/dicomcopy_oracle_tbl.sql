--
--  Copyright (C) 2007 by CERN/IT/GD/ITR
--  All rights reserved
--
--       @(#)$RCSfile: dicomcopy_oracle_tbl.sql,v $ $Revision: 1.1 $ $Date: 2008/01/14 10:06:10 $ CERN IT-GD/ITR Jean-Philippe Baud

--     Create Disk Pool Manager Oracle tables for DICOM.

CREATE TABLE dpm_dicom_req (
       r_ordinal NUMBER(10),
       r_token VARCHAR2(36),
       r_uid NUMBER(10),
       r_gid NUMBER(10),
       client_dn VARCHAR2(255),
       groups VARCHAR2(255),
       clienthost VARCHAR2(63),
       retrytime NUMBER(10),
       lifetime NUMBER(10),
       s_token CHAR(36),
       ret_policy CHAR(1),
       f_type CHAR(1),
       nbreqfiles NUMBER(10),
       ctime NUMBER(10),
       status NUMBER(10));

CREATE TABLE dpm_dicom_filereq (
       r_token VARCHAR(36) BINARY,
       f_ordinal NUMBER(10),
       surl VARCHAR2(1103),
       dicom_fn VARCHAR2(255),
       status NUMBER(10));

ALTER TABLE dpm_dicom_req
       ADD CONSTRAINT pk_d_token PRIMARY KEY (r_token);
ALTER TABLE dpm_dicom_filereq
       ADD CONSTRAINT pk_d_fullid PRIMARY KEY (r_token, f_ordinal);

CREATE INDEX D_SURL_IDX ON dpm_dicom_filereq(SURL);
