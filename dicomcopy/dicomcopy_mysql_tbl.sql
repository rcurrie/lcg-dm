--
--  Copyright (C) 2007 by CERN/IT/GD/ITR
--  All rights reserved
--
--       @(#)$RCSfile: dicomcopy_mysql_tbl.sql,v $ $Revision: 1.1 $ $Date: 2008/01/14 10:06:10 $ CERN IT-GD/ITR Jean-Philippe Baud

--     Create Disk Pool Manager MySQL tables for DICOM.

USE dpm_db;
CREATE TABLE dpm_dicom_req (
       rowid BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       r_ordinal INTEGER,
       r_token VARCHAR(36) BINARY,
       r_uid INTEGER,
       r_gid INTEGER,
       client_dn VARCHAR(255) BINARY,
       groups VARCHAR(255) BINARY,
       clienthost VARCHAR(63) BINARY,
       retrytime INTEGER,
       lifetime INTEGER,
       s_token CHAR(36) BINARY,
       ret_policy CHAR(1),
       f_type CHAR(1) BINARY,
       nbreqfiles INTEGER,
       ctime INTEGER,
       status INTEGER)
	TYPE = InnoDB;

CREATE TABLE dpm_dicom_filereq (
       rowid BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       r_token VARCHAR(36) BINARY,
       f_ordinal INTEGER,
       surl BLOB,
       dicom_fn VARCHAR(255),
       status INTEGER)
	TYPE = InnoDB;

ALTER TABLE dpm_dicom_req
       ADD UNIQUE (r_token);
ALTER TABLE dpm_dicom_filereq
       ADD UNIQUE pk_d_fullid (r_token, f_ordinal);

CREATE INDEX D_SURL_IDX ON dpm_dicom_filereq(surl(255));
