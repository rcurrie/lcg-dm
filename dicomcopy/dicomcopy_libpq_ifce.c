/*
 * Copyright (C) 2008 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dicomcopy_libpq_ifce.c,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <sys/types.h>
#include "dicomcopy_server.h"
#include "dpm.h"
#include "dpm_backend.h"
#include "dpm_server.h"
#include "serrno.h"
extern void dpm_libpq_error(char *, char *, PGresult *res, struct dpm_dbfd *);

dpm_decode_dfr_entry(res, row, lock, rec_addr, dfr_entry)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_dicom_filereq *dfr_entry;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	strcpy (dfr_entry->r_token, PQgetvalue (res, row, i++));
	dfr_entry->f_ordinal = atoi (PQgetvalue (res, row, i++));
	strcpy (dfr_entry->surl, PQgetvalue (res, row, i++));
	strcpy (dfr_entry->dicom_fn, PQgetvalue (res, row, i++));
	dfr_entry->status = atoi (PQgetvalue (res, row, i));
}

dpm_decode_dicomreq_entry(res, row, lock, rec_addr, dpm_req)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_backend_req *dpm_req;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	dpm_req->r_ordinal = atoi (PQgetvalue (res, row, i++));
	strcpy (dpm_req->r_token, PQgetvalue (res, row, i++));
	dpm_req->r_uid = atoi (PQgetvalue (res, row, i++));
	dpm_req->r_gid = atoi (PQgetvalue (res, row, i++));
	strcpy (dpm_req->client_dn, PQgetvalue (res, row, i++));
	strcpy (dpm_req->groups, PQgetvalue (res, row, i++));
	strcpy (dpm_req->clienthost, PQgetvalue (res, row, i++));
	dpm_req->retrytime = atoi (PQgetvalue (res, row, i++));
	dpm_req->lifetime = atoi (PQgetvalue (res, row, i++));
	strcpy (dpm_req->s_token, PQgetvalue (res, row, i++));
	dpm_req->ret_policy = *(PQgetvalue (res, row, i++));
	dpm_req->f_type = *(PQgetvalue (res, row, i++));
	dpm_req->nbreqfiles = atoi (PQgetvalue (res, row, i++));
	dpm_req->ctime = atoi (PQgetvalue (res, row, i++));
	dpm_req->status = atoi (PQgetvalue (res, row, i));
}

dpm_delete_dfr_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_dicom_filereq WHERE ROWID = %s";
	char func[21];
	PGresult *res;
	char sql_stmt[70];

	strcpy (func, "dpm_delete_dfr_entry");
	sprintf (sql_stmt, delete_stmt, *rec_addr);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_dicomreq_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_dicom_req WHERE ROWID = %s";
	char func[26];
	PGresult *res;
	char sql_stmt[70];

	strcpy (func, "dpm_delete_dicomreq_entry");
	sprintf (sql_stmt, delete_stmt, *rec_addr);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_get_dfr_by_fullid(dbfd, r_token, f_ordinal, dfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token; 
int f_ordinal;
struct dpm_dicom_filereq *dfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[22]; 
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_dfr_by_fullid");
	sprintf (sql_stmt, query4upd, r_token, f_ordinal);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dfr_entry (res, 0, lock, rec_addr, dfr_entry);
	PQclear (res);
	return (0);
}

dpm_get_dfr_by_surl(dbfd, r_token, surl, dfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *surl;
struct dpm_dicom_filereq *dfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[20];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE r_token = '%s' AND surl = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[2378];

	strcpy (func, "dpm_get_dfr_by_surl");
	PQescapeStringConn (dbfd->Pconn, escaped_name, surl,
	    strlen (surl), NULL);
	sprintf (sql_stmt, query4upd, r_token, escaped_name);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dfr_entry (res, 0, lock, rec_addr, dfr_entry);
	PQclear (res);
	return (0);
}

dpm_get_dicomreq_by_token(dbfd, token, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *token;
struct dpm_backend_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[26];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS \
		FROM dpm_dicom_req \
		WHERE r_token = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_dicomreq_by_token");
	sprintf (sql_stmt, query4upd, token);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dicomreq_entry (res, 0, lock, rec_addr, dpm_req);
	PQclear (res);
	return (0);
}

dpm_get_next_dicomreq(dbfd, status, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
int status;
struct dpm_backend_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[22];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS \
		FROM dpm_dicom_req \
		WHERE status = %d \
		ORDER BY ctime, r_ordinal \
		LIMIT 1 \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_next_dicomreq");
	sprintf (sql_stmt, query4upd, status);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dicomreq_entry (res, 0, lock, rec_addr, dpm_req);
	PQclear (res);
	return (0);
}

dpm_insert_dfr_entry(dbfd, dfr_entry)
struct dpm_dbfd *dbfd;
struct dpm_dicom_filereq *dfr_entry;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_dicom_filereq \
		(R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS) \
		VALUES \
		('%s', %d, '%s', '%s', %d)";
	PGresult *res;
	char sql_stmt[2625];

	strcpy (func, "dpm_insert_dfr_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, dfr_entry->surl,
	    strlen (dfr_entry->surl), NULL);
	sprintf (sql_stmt, insert_stmt,
	    dfr_entry->r_token, dfr_entry->f_ordinal, escaped_name,
	    dfr_entry->dicom_fn, dfr_entry->status);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_dicomreq_entry(dbfd, dpm_req)
struct dpm_dbfd *dbfd;
struct dpm_backend_req *dpm_req;
{
	char escaped_name[255*2+1];
	char func[26];
	static char insert_stmt[] =
		"INSERT INTO dpm_dicom_req \
		(R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS) \
		VALUES \
		(%d, '%s', %d, %d, '%s', '%s', '%s', %d, %d, '%s', '%c', '%c', %d, %d, %d)";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_insert_dicomreq_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, dpm_req->client_dn,
	    strlen (dpm_req->client_dn), NULL);
	sprintf (sql_stmt, insert_stmt,
	    dpm_req->r_ordinal, dpm_req->r_token, dpm_req->r_uid,
	    dpm_req->r_gid, escaped_name, dpm_req->groups,
	    dpm_req->clienthost, dpm_req->retrytime, dpm_req->lifetime,
	    dpm_req->s_token, dpm_req->ret_policy, dpm_req->f_type,
	    dpm_req->nbreqfiles, dpm_req->ctime, dpm_req->status);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_list_dfr_by_surl(dbfd, bol, surl, dfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *surl;
struct dpm_dicom_filereq *dfr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[21];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE surl = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[2325];

	strcpy (func, "dpm_list_dfr_by_surl");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		PQescapeStringConn (dbfd->Pconn, escaped_name, surl,
		    strlen (surl), NULL);
		sprintf (sql_stmt, lock ? query4upd : query, escaped_name);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1); 
		}
		dblistptr->res = res; 
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_dfr_entry (dblistptr->res, dblistptr->row, lock, rec_addr, dfr_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_pending_dicomreq(dbfd, bol, dpm_req, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_backend_req *dpm_req; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[26];
	static char query[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS \
		FROM dpm_dicom_req \
		WHERE status = %d";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_pending_dicomreq");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, query, DPM_QUEUED);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1); 
		}
		dblistptr->res = res; 
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_dicomreq_entry (dblistptr->res, dblistptr->row, 0, NULL, dpm_req);
	dblistptr->row++;
	return (0);
}

dpm_update_dfr_entry (dbfd, rec_addr, dfr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_dicom_filereq *dfr_entry;
{
	char func[21];
	PGresult *res;
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_dicom_filereq SET \
		STATUS = %d \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_dfr_entry");
	sprintf (sql_stmt, update_stmt, dfr_entry->status, *rec_addr);

	 res = PQexec (dbfd->Pconn, sql_stmt);
	 if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		 dpm_libpq_error (func, "UPDATE", res, dbfd);
		 return (-1);
	 }
	 PQclear (res);
	return (0);
}

dpm_update_dicomreq_entry (dbfd, rec_addr, dpm_req)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_backend_req *dpm_req;
{
	char func[26];
	PGresult *res;
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_dicom_req SET \
		STATUS = %d \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_dicomreq_entry");
	sprintf (sql_stmt, update_stmt, dpm_req->status, *rec_addr);

	 res = PQexec (dbfd->Pconn, sql_stmt);
	 if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		 dpm_libpq_error (func, "UPDATE", res, dbfd);
		 return (-1);
	 }
	 PQclear (res);
	return (0);
}
