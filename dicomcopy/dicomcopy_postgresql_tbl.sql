--
--  Copyright (C) 2007-2008 by CERN/IT/GD/ITR
--  All rights reserved
--
--       @(#)$RCSfile: dicomcopy_postgresql_tbl.sql,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud

--     Create Disk Pool Manager PostgreSQL tables for DICOM.

CREATE TABLE dpm_dicom_req (
       rowid BIGSERIAL PRIMARY KEY,
       r_ordinal INTEGER,
       r_token VARCHAR(36),
       r_uid INTEGER,
       r_gid INTEGER,
       client_dn VARCHAR(255),
       groups VARCHAR(255),
       clienthost VARCHAR(63),
       retrytime INTEGER,
       lifetime INTEGER,
       s_token CHAR(36),
       ret_policy CHAR(1),
       f_type CHAR(1),
       nbreqfiles INTEGER,
       ctime INTEGER,
       status INTEGER);

CREATE TABLE dpm_dicom_filereq (
       rowid BIGSERIAL PRIMARY KEY,
       r_token VARCHAR(36),
       f_ordinal INTEGER,
       surl VARCHAR(1103),
       dicom_fn VARCHAR(255),
       status INTEGER);

ALTER TABLE dpm_dicom_req
       ADD CONSTRAINT pk_d_token UNIQUE (r_token);
ALTER TABLE dpm_dicom_filereq
       ADD CONSTRAINT pk_d_fullid UNIQUE (r_token, f_ordinal);

CREATE INDEX D_SURL_IDX ON dpm_dicom_filereq (surl);
