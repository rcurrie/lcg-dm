.\" @(#)$RCSfile: dicomcopyd.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007-2011 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH DICOMCOPYD 1 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dicomcopyd \- start the Disk Pool Manager DICOM backend
.SH SYNOPSIS
.B dicomcopyd
[
.BI -c " config_file"
] [
.BI -l " log_file"
] [
.BI -4
|
.BI -6
]
.SH DESCRIPTION
.LP
The
.B dicomcopyd
command starts the Disk Pool Manager DICOM backend.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will load a DICOM plugin, read the Disk Pool Manager "request" database
configuration file and look for requests.
Each of them is processed in a child process which opens a connection to the
database server if necessary.
.LP
All error messages and statistical information are kept in a log.
.LP
The maximum number of child processes is set by default to 20.
If there are more concurrent requests, they remain in the queue until a
worker process is available.
The maximum number of child processes can be changed by putting an entry in
.B /etc/shift.conf
for example:
.RS
.HP
DICOMCOPYD	MAX_WORKERS	30
.RE
.LP
The Disk Pool Manager DICOM backend listen port number can be defined on client
hosts and on the server itself in either of the following ways:
.RS
.LP
setting an environment variable DICOMCOPYD_PORT
.RS
.HP
setenv DICOMCOPYD_PORT 5016
.RE
.LP
an entry in
.B /etc/shift.conf
like:
.RS
.HP
DICOMCOPYD	PORT	5016
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of DICOMCOPYD_PORT in dicomcopy_constants.h.
.LP
The Disk Pool Manager host name can be defined on client hosts
in either of the following ways:
.RS
.LP
setting an environment variable DICOMCOPYD_HOST, for example:
.RS
.HP
setenv DICOMCOPYD_HOST sehost
.RE
.LP
an entry in
.B /etc/shift.conf
for example:
.RS
.HP
DICOMCOPYD	HOST	sehost
.RE
.RE
.LP
If none of these methods is used, the default host is taken from the
definition of DpmHost in site.def.
.LP
The Disk Pool Manager DICOM backend "request" database keeps the requests and
their status even after completion.
.LP
The Disk Pool Manager configuration file contains password information for the
database and must be readable/writable only by root.
It contains a single line in the format:
.HP
.RS
username/password@server
.RE
or
.RS
username/password@server/dbname
.RE
.sp
where 'username' and 'password' are the credentials to login to the database
instance identified by 'server'. If 'dbname' is not specified, "dpm_db" is used.
.LP
In the log each entry has a timestamp.
All entries corresponding to one request have the same process id.
For each user command there is one message giving information about
the requestor DN and several lines when getting the TURL for the output file
and when doing the copy itself.
The completion code of the command is also logged.
.SH OPTIONS
.TP
.BI -c " config_file"
Specifies a different path for the Disk Pool Manager configuration file.
.TP
.BI -l " log_file"
Specifies a different path for the Disk Pool Manager log file.
The special value
.B syslog
will send the log messages to the system logger syslogd.
.TP
.BI -4
only try to listen on IPv4 addresses
.TP
.BI -6
only try to listen on IPv6 addresses
.SH FILES
.TP 1.8i
.B libdpm_dicom.so
DICOM plugin
.TP
.B /etc/DPMCONFIG
configuration file
.TP
.B /var/log/dicomcopy/log
.SH EXAMPLES
.TP
Here is a small log:
.nf
12/18 12:53:26 22886 dicomcopyd: started
12/18 17:53:42 22886 dicomcopyd: inc_reqctr request from lxb0987v2.cern.ch
12/18 17:53:42 22886 dicomcopyd: decrementing reqctr
12/18 17:53:42 12038 dicomcopyd: processing request 860778d4-e50c-4007-9888-55aed93b45e9 from /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=knienart/CN=589336/CN=Krzysztof Nienartowicz
12/18 17:53:42 12038 dicomcopyd: getting turl for srm://lxb0987v2.cern.ch:8446/srm/managerv2?SFN=/dpm/cern.ch/home/biomed/mdm/1.2.826.0.1.3680043.2.1143..20060202124502298.29/1.2.826.0.1.3680043.2.1143..20060202124502298.29/1.2.826.0.1.3680043.2.1143..20060202124504468.51
12/18 17:53:43 12038 dicomcopyd: copying /1.2.826.0.1.3680043.2.1143..20060202124502298.29/1.2.826.0.1.3680043.2.1143..20060202124502298.29/1.2.826.0.1.3680043.2.1143..20060202124504468.51 to rfio://lxb0987v2.cern.ch//dpm_data/dteam/2007-12-18/1.2.826.0.1.3680043.2.1143..20060202124504468.51.11330.0
12/18 17:53:45 12038 dicomcopyd: dpm_dicomcopyfile returned 0, errcode = 0
12/18 17:53:45 12038 dicomcopyd: calling dpm_updatefilestatus
12/18 17:53:46 22886 dicomcopyd: process 12038 exiting with status 0
.fi
.SH SEE ALSO
.B Clogit(3)
