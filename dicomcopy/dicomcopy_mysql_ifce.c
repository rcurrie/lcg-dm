/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dicomcopy_mysql_ifce.c,v $ $Revision: 1.2 $ $Date: 2009/11/13 10:03:11 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <errmsg.h>
#include <mysqld_error.h>
#include "dicomcopy_server.h"
#include "dpm.h"
#include "dpm_backend.h"
#include "dpm_server.h"
#include "serrno.h"
extern void dpm_mysql_error(char *, char *, struct dpm_dbfd *);
extern int dpm_exec_query(char *, struct dpm_dbfd *, char *, MYSQL_RES **);

static inline unsigned long
dm_escape_string(mysql, to, from, length, max)
MYSQL *mysql;
char *to;
const char *from;
unsigned long length;
unsigned long max;
{
	unsigned long num = 0;
	if (max == 0)
		return 0;
	num = (max-1)/2;
	if (length < num)
		num = length;
	return mysql_real_escape_string (mysql, to, from, num);
}

static int
dm_snprintf(char *str, size_t size, const char *format, ...)
{  
	va_list args;
	int ret;

	va_start (args, format);

	ret = vsnprintf(str, size, format, args);
	if (size && (ret < 0 || (size_t)ret >= size))
		*str = '\0';

	va_end (args);
	return ret;   
}

dpm_decode_dfr_entry(row, lock, rec_addr, dfr_entry)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_dicom_filereq *dfr_entry;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, row[i++]);
	strcpy (dfr_entry->r_token, row[i++]);
	dfr_entry->f_ordinal = atoi (row[i++]);
	strcpy (dfr_entry->surl, row[i++]);
	strcpy (dfr_entry->dicom_fn, row[i++]);
	dfr_entry->status = atoi (row[i]);
}

dpm_decode_dicomreq_entry(row, lock, rec_addr, dpm_req)
MYSQL_ROW row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_backend_req *dpm_req;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, row[i++]);
	dpm_req->r_ordinal = atoi (row[i++]);
	strcpy (dpm_req->r_token, row[i++]);
	dpm_req->r_uid = atoi (row[i++]);
	dpm_req->r_gid = atoi (row[i++]);
	strcpy (dpm_req->client_dn, row[i++]);
	strcpy (dpm_req->groups, row[i++]);
	strcpy (dpm_req->clienthost, row[i++]);
	dpm_req->retrytime = atoi (row[i++]);
	dpm_req->lifetime = atoi (row[i++]);
	strcpy (dpm_req->s_token, row[i++]);
	dpm_req->ret_policy = *row[i++];
	dpm_req->f_type = *row[i++];
	dpm_req->nbreqfiles = atoi (row[i++]);
	dpm_req->ctime = atoi (row[i++]);
	dpm_req->status = atoi (row[i]);
}

dpm_delete_dfr_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_dicom_filereq WHERE ROWID = %s";
	char func[21];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_dfr_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, *rec_addr);
	if (mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_delete_dicomreq_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_dicom_req WHERE ROWID = %s";
	char func[26];
	char sql_stmt[1024];

	strcpy (func, "dpm_delete_dicomreq_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), delete_stmt, *rec_addr);
	if (mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "DELETE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_get_dfr_by_fullid(dbfd, r_token, f_ordinal, dfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token; 
int f_ordinal;
struct dpm_dicom_filereq *dfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[22]; 
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_dfr_by_fullid");
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), query4upd, escaped_token,
	    f_ordinal);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dfr_entry (row, lock, rec_addr, dfr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_dfr_by_surl(dbfd, r_token, surl, dfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *surl;
struct dpm_dicom_filereq *dfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[20];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE r_token = '%s' AND surl = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_get_dfr_by_surl");
	dm_escape_string (&dbfd->mysql, escaped_name, surl,
	    strlen (surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_token, r_token,
	    strlen (r_token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), query4upd, escaped_token,
	    escaped_name);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dfr_entry (row, lock, rec_addr, dfr_entry);
	mysql_free_result (res);
	return (0);
}

dpm_get_dicomreq_by_token(dbfd, token, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *token;
struct dpm_backend_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char func[26];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS \
		FROM dpm_dicom_req \
		WHERE r_token = '%s' \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_dicomreq_by_token");
	dm_escape_string (&dbfd->mysql, escaped_token, token,
	    strlen (token), sizeof(escaped_token));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), query4upd, escaped_token);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dicomreq_entry (row, lock, rec_addr, dpm_req);
	mysql_free_result (res);
	return (0);
}

dpm_get_next_dicomreq(dbfd, status, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
int status;
struct dpm_backend_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[22];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS \
		FROM dpm_dicom_req \
		WHERE status = %d \
		ORDER BY ctime, r_ordinal \
		LIMIT 1 \
		FOR UPDATE";
	MYSQL_RES *res;
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_next_dicomreq");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), query4upd, status);
	if (dpm_exec_query (func, dbfd, sql_stmt, &res))
		return (-1);
	if ((row = mysql_fetch_row (res)) == NULL) {
		mysql_free_result (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_dicomreq_entry (row, lock, rec_addr, dpm_req);
	mysql_free_result (res);
	return (0);
}

dpm_insert_dfr_entry(dbfd, dfr_entry)
struct dpm_dbfd *dbfd;
struct dpm_dicom_filereq *dfr_entry;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_dicomfn[255*2+1];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_dicom_filereq \
		(R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS) \
		VALUES \
		('%s', %d, '%s', '%s', %d)";
	char sql_stmt[4096];

	strcpy (func, "dpm_insert_dfr_entry");
	dm_escape_string (&dbfd->mysql, escaped_name, dfr_entry->surl,
	    strlen (dfr_entry->surl), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_token, dfr_entry->r_token,
	    strlen (dfr_entry->r_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_dicomfn, dfr_entry->dicom_fn,
	    strlen (dfr_entry->dicom_fn), sizeof(escaped_dicomfn));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    escaped_token, dfr_entry->f_ordinal, escaped_name,
	    escaped_dicomfn, dfr_entry->status);

	if (mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_insert_dicomreq_entry(dbfd, dpm_req)
struct dpm_dbfd *dbfd;
struct dpm_backend_req *dpm_req;
{
	char escaped_ftype[3];
	char escaped_groups[255*2+1];
	char escaped_host[CA_MAXHOSTNAMELEN*2+1];
	char escaped_name[255*2+1];
	char escaped_retpolicy[3];
	char escaped_token[CA_MAXDPMTOKENLEN*2+1];
	char escaped_token2[CA_MAXDPMTOKENLEN*2+1];
	char func[26];
	static char insert_stmt[] =
		"INSERT INTO dpm_dicom_req \
		(R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS) \
		VALUES \
		(%d, '%s', %d, %d, '%s', '%s', '%s', %d, %d, '%s', '%s', '%s', %d, %d, %d)";
	char sql_stmt[2048];

	strcpy (func, "dpm_insert_dicomreq_entry");
	dm_escape_string (&dbfd->mysql, escaped_name, dpm_req->client_dn,
	    strlen (dpm_req->client_dn), sizeof(escaped_name));
	dm_escape_string (&dbfd->mysql, escaped_ftype, &dpm_req->f_type, 1,
	    sizeof(escaped_ftype));
	dm_escape_string (&dbfd->mysql,	escaped_groups, dpm_req->groups,
	    strlen (dpm_req->groups), sizeof(escaped_groups));
	dm_escape_string (&dbfd->mysql, escaped_host, dpm_req->clienthost,
	    strlen (dpm_req->clienthost), sizeof(escaped_host));
	dm_escape_string (&dbfd->mysql, escaped_retpolicy,
	    &dpm_req->ret_policy, 1, sizeof(escaped_retpolicy));
	dm_escape_string (&dbfd->mysql, escaped_token, dpm_req->r_token,
	    strlen (dpm_req->r_token), sizeof(escaped_token));
	dm_escape_string (&dbfd->mysql, escaped_token2, dpm_req->s_token,
	    strlen (dpm_req->s_token), sizeof(escaped_token2));
	dm_snprintf (sql_stmt, sizeof(sql_stmt), insert_stmt,
	    dpm_req->r_ordinal, escaped_token, dpm_req->r_uid,
	    dpm_req->r_gid, escaped_name, escaped_groups,
	    escaped_host, dpm_req->retrytime, dpm_req->lifetime,
	    escaped_token2, escaped_retpolicy, escaped_ftype,
	    dpm_req->nbreqfiles, dpm_req->ctime, dpm_req->status);

	if (mysql_query (&dbfd->mysql, sql_stmt)) {
		if (mysql_errno (&dbfd->mysql) == ER_DUP_ENTRY)
			serrno = EEXIST;
		else {
			dpm_mysql_error (func, "INSERT", dbfd);
		}
		return (-1);
	}
	return (0);
}

dpm_list_dfr_by_surl(dbfd, bol, surl, dfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *surl;
struct dpm_dicom_filereq *dfr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[21];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, SURL, \
		 DICOM_FN, STATUS \
		FROM dpm_dicom_filereq \
		WHERE surl = '%s' \
		FOR UPDATE";
	MYSQL_ROW row;
	char sql_stmt[4096];

	strcpy (func, "dpm_list_dfr_by_surl");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_escape_string (&dbfd->mysql, escaped_name, surl,
		    strlen (surl), sizeof(escaped_name));
		dm_snprintf (sql_stmt, sizeof(sql_stmt), lock ? query4upd : query,
		    escaped_name);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_dfr_entry (row, lock, rec_addr, dfr_entry);
	return (0);
}

dpm_list_pending_dicomreq(dbfd, bol, dpm_req, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_backend_req *dpm_req; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[26];
	static char query[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, GROUPS, \
		 CLIENTHOST, RETRYTIME, LIFETIME, \
		 S_TOKEN, RET_POLICY, F_TYPE, \
		 NBREQFILES, CTIME, STATUS \
		FROM dpm_dicom_req \
		WHERE status = %d";
	MYSQL_ROW row;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_pending_dicomreq");
	if (endlist) {
		if (*dblistptr)
			mysql_free_result (*dblistptr);
		return (1);
	}
	if (bol) {
		dm_snprintf (sql_stmt, sizeof(sql_stmt), query, DPM_QUEUED);
		if (dpm_exec_query (func, dbfd, sql_stmt, dblistptr))
			return (-1);
	}
	if ((row = mysql_fetch_row (*dblistptr)) == NULL)
		return (1);
	dpm_decode_dicomreq_entry (row, 0, NULL, dpm_req);
	return (0);
}

dpm_update_dfr_entry (dbfd, rec_addr, dfr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_dicom_filereq *dfr_entry;
{
	char func[21];
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_dicom_filereq SET \
		STATUS = %d \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_dfr_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt, dfr_entry->status,
	    *rec_addr);

	if (mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}

dpm_update_dicomreq_entry (dbfd, rec_addr, dpm_req)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_backend_req *dpm_req;
{
	char func[26];
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_dicom_req SET \
		STATUS = %d \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_dicomreq_entry");
	dm_snprintf (sql_stmt, sizeof(sql_stmt), update_stmt, dpm_req->status,
	    *rec_addr);

	if (mysql_query (&dbfd->mysql, sql_stmt)) {
		dpm_mysql_error (func, "UPDATE", dbfd);
		return (-1);
	}
	return (0);
}
