/*
 * Copyright (C) 2007-2008 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: send2dicomcopyd.c,v $ $Revision: 1.6 $ $Date: 2009/01/11 00:43:26 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#endif
#include "Cnetdb.h"
#ifdef CSEC
#include "Csec_api.h"
#endif
#include "dicomcopy_api.h"
#include "dpm.h"
#include "dpm_api.h"
#include "marshall.h"
#include "net.h"
#include "serrno.h"
#if defined(_WIN32)
extern char *ws_strerr;
#endif

/* send2dicomcopyd - send a request to the DPM DICOM backend and wait for the reply */

send2dicomcopyd(host, reqp, reql, user_repbuf, user_repbuf_len)
char *host;
char *reqp;
int reql;
char *user_repbuf;
int user_repbuf_len;
{
	int actual_replen = 0;
	struct addrinfo *ai;
	struct addrinfo *aitop;
	int c;
#ifdef CSEC
	Csec_context_t ctx;
#endif
	char dicomcopydhost[CA_MAXHOSTNAMELEN+1];
	char func[16];
	int gaierrno;
	char *getconfent();
	char *getenv();
	struct addrinfo hints;
	int isconnected;
	int magic;
	int n;
	char *neterrstr = NULL;
	char *p;
	char prtbuf[PRTBUFSZ];
	int rep_type;
	char repbuf[REPBUFSZ];
	int s;
	int save_serrno;
	char strport[NI_MAXSERV];

	strcpy (func, "send2dicomcopyd");
	if ((p = getenv ("DICOMCOPYD_PORT")) || (p = getconfent ("DICOMCOPYD", "PORT", 0))) {
		strncpy (strport, p, sizeof(strport));
		strport[sizeof(strport)-1] = '\0';
	} else {
		snprintf (strport, sizeof(strport), "%u", DICOMCOPYD_PORT);
		serrno = 0;
	}
	if (host && *host)
		strcpy (dicomcopydhost, host);
	else if ((p = getenv ("DICOMCOPYD_HOST")) || (p = getconfent ("DICOMCOPYD", "HOST", 0)))
		strcpy (dicomcopydhost, p);
	else {
#if defined(DPM_HOST)
		strcpy (dicomcopydhost, DPM_HOST);
#else
		gethostname (dicomcopydhost, sizeof(dicomcopydhost));
#endif
		serrno = 0;
	}

	memset (&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
 #ifdef AI_ADDRCONFIG
	hints.ai_flags |= AI_ADDRCONFIG;
#endif
	gaierrno = Cgetaddrinfo (dicomcopydhost, strport, &hints, &aitop);

	if (gaierrno !=0 && serrno == 0)
		serrno = SENOSHOST;

	if (gaierrno == EAI_NONAME) {
		dpm_errmsg (func, DP009, "Host unknown:", dicomcopydhost);
		return (-1);
	} else if (gaierrno != 0) {
		dpm_errmsg (func, "Error during lookup of %s: %s\n",
		    dicomcopydhost, Cgai_strerror (gaierrno));
		return (-1);
	}

	isconnected = 0;
	save_serrno = 0;
	for (ai = aitop; ai && !isconnected; ai = ai->ai_next) {
		if (ai->ai_family != PF_INET && ai->ai_family != PF_INET6)
			continue;
		if ((s = socket (ai->ai_family, ai->ai_socktype, ai->ai_protocol))<0)
			continue;

		if (connect (s, ai->ai_addr, ai->ai_addrlen) < 0) {
#if defined(_WIN32)
			if (WSAGetLastError() == WSAEAFNOSUPPORT) {
#else
			if (errno == EAFNOSUPPORT) {
#endif
				(void) netclose (s);
				continue;
			}
#if defined(_WIN32)
			if (WSAGetLastError() == WSAECONNREFUSED) {
#else
			if (errno == ECONNREFUSED) {
#endif
				save_serrno = EDPMNACT;
			} else {
				save_serrno = SECOMERR;
			}
			p = neterror ();
			if (neterrstr)
				free (neterrstr);
			neterrstr = strdup (p);
			(void) netclose (s);
		} else {
			isconnected = 1;
		}
	}
	freeaddrinfo (aitop);
	if (!isconnected) {
		serrno = (save_serrno) ? save_serrno : SECOMERR;
		switch(serrno) {
			case EDPMNACT:
				dpm_errmsg (func, DP000, dicomcopydhost);
				break;
			default:
				if (neterrstr) {
					dpm_errmsg (func, DP002, "connect", neterrstr);
				} else {
					dpm_errmsg (func, "Could not create "
					    "an outgoing connection\n");
				}
				break;
		}
		if (neterrstr)
			free (neterrstr);
		return (-1);
	}
	if (neterrstr) {
		free (neterrstr);
		neterrstr = NULL;
	}

#ifdef CSEC
	Csec_client_initContext (&ctx, CSEC_SERVICE_TYPE_HOST, NULL);
	if (Csec_client_establishContext (&ctx, s) < 0) {
		if (serrno != SECOMERR && serrno != SETIMEDOUT)
			dpm_errmsg (func, DP002, "send", Csec_getErrorMessageSummary (PRTBUFSZ-48));
		(void) netclose (s);
		Csec_clearContext (&ctx);
		return (-1);
	}
	Csec_clearContext (&ctx);
#endif

	/* send request to DPM DICOM server */

	if ((n = netwrite (s, reqp, reql)) <= 0) {
		if (n == 0)
			dpm_errmsg (func, DP002, "send", sys_serrlist[SERRNO]);
		else
			dpm_errmsg (func, DP002, "send", neterror());
		(void) netclose (s);
		serrno = SECOMERR;
		return (-1);
	}

	/* get reply */

	while (1) {
		if ((n = netread (s, repbuf, 3 * LONGSIZE)) <= 0) {
			if (n == 0)
				dpm_errmsg (func, DP002, "recv", sys_serrlist[SERRNO]);
			else
				dpm_errmsg (func, DP002, "recv", neterror());
			(void) netclose (s);
			serrno = SECOMERR;
			return (-1);
		}
		p = repbuf;
		unmarshall_LONG (p, magic) ;
		unmarshall_LONG (p, rep_type) ;
		unmarshall_LONG (p, c) ;
		if (rep_type == DPM_RC) {
			(void) netclose (s);
			if (c) {
				serrno = c;
				c = -1;
			}
			break;
		}
		if (c > REPBUFSZ) {
			dpm_errmsg (func, "reply too large\n");
			serrno = SEINTERNAL;
			return (-1);
		}
		if ((n = netread (s, repbuf, c)) <= 0) {
			if (n == 0)
				dpm_errmsg (func, DP002, "recv", sys_serrlist[SERRNO]);
			else
				dpm_errmsg (func, DP002, "recv", neterror());
			(void) netclose (s);
			serrno = SECOMERR;
			return (-1);
		}
		p = repbuf;
		if (rep_type == MSG_ERR) {
			unmarshall_STRING (p, prtbuf);
			dpm_errmsg (NULL, "%s", prtbuf);
		} else if (user_repbuf) {
			if (actual_replen + c <= user_repbuf_len)
				n = c;
			else
				n = user_repbuf_len - actual_replen;
			if (n) {
				memcpy (user_repbuf + actual_replen, repbuf, n);
				actual_replen += n;
			}
		}
	}
	return (c);
}
