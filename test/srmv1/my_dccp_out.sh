#!/bin/bash

# This script is a part of SRM test suite. This is a wrapper script around
# rfcp, used to copy files to the remote machine (to TURL got from SRM
# server).

# $Id: my_dccp_out.sh,v 1.5 2005/11/29 11:04:19 jamesc Exp $


echo 'TURL:' $2
TURL_RFCP=$2
echo 'TURL_DCCP:  ' $TURL_RFCP
/opt/d-cache/dcap/bin/dccp $1 $TURL_RFCP
exit $?

