#!/bin/bash
#
# This script is a part of SRM test suite. This is a wrapper script around
# rfcp, used to copy files from the remote machine (from TURL got from SRM
# server) and store it locally.
#

# $Id: my_gsiftp_in.sh,v 1.5 2005/11/29 11:04:19 jamesc Exp $

echo 'TURL:' $1
TURL_RFCP=$1  ##  24/05/05
echo 'TURL_gsiftp:  ' $TURL_RFCP
globus-url-copy $TURL_RFCP file://`pwd`/$2

exit $?

