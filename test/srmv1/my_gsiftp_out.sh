#!/bin/bash
#
# This script is a part of SRM test suite. This is a wrapper script around
# rfcp, used to copy files to the remote machine (to TURL got from SRM
# server).
#

# $Id: my_gsiftp_out.sh,v 1.5 2005/11/29 11:04:19 jamesc Exp $

echo 'TURL:' $2
TURL_RFCP=$2  ##  24/05/05
echo 'TURL_gsiftp:  ' $TURL_RFCP
globus-url-copy file://`pwd`/$1 $TURL_RFCP

exit $?

