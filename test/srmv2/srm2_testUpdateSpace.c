/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testUpdateSpace.c,v 1.3 2005/11/09 16:16:00 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmUpdateSpaceResponse_ rep;
	//struct ArrayOfTSURLPermissionReturn *repfs;
	struct ns1__srmUpdateSpaceRequest req;
	//struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__srmUpdateSpaceResponse *repp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	if (argc < 7) {
		fprintf (stderr, "usage: %s endPoint s_token storage_system_info newSizeOfTotalSpaceDesired newSizeOfGuaranteedSpaceDesired newLifeTimeFromCallingTime\n", argv[0]);
		exit (1);
	}

#if 0
	if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

	    if ((req.spaceToken =
		 soap_malloc (&soap, sizeof(struct ns1__TSpaceToken))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    req.spaceToken->value = argv[2];

	    if ((req.storageSystemInfo =
		 soap_malloc (&soap, sizeof(struct ns1__TStorageSystemInfo))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    req.storageSystemInfo->value = argv[3];

	if ((req.newSizeOfTotalSpaceDesired =
		soap_malloc (&soap, sizeof(struct ns1__TSizeInBytes))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.newSizeOfTotalSpaceDesired->value = atoll(argv[4]);

	if ((req.newSizeOfGuaranteedSpaceDesired =
		soap_malloc (&soap, sizeof(struct ns1__TSizeInBytes))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.newSizeOfGuaranteedSpaceDesired->value = atoll(argv[5]);

	if ((req.newLifeTimeFromCallingTime =
		soap_malloc (&soap, sizeof(struct ns1__TLifeTimeInSeconds))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.newLifeTimeFromCallingTime->value = atoi(argv[6]);

	/* To send the request ... */

#if 0
	if (soap_call_ns1__srmUpdateSpace (&soap, argv[1], "SrmUpdateSpace",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	srm_endpoint = argv[1];
	SOAP_CALL_NS1(UpdateSpace);
#endif

	reqstatp = rep.srmUpdateSpaceResponse->returnStatus;
	//repfs = rep.srmCheckPermissionResponse->arrayOfPermissions;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	repp = rep.srmUpdateSpaceResponse;
/*  	printf ("srmUpdateSpace provided actual_s_type: %d actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", (*repp->typeOfReservedSpace), repp->sizeOfTotalReservedSpace->value, repp->sizeOfGuaranteedReservedSpace->value, repp->lifetimeOfReservedSpace->value ); */
	printf ("srmUpdateSpace provided actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", repp->sizeOfTotalSpace->value, repp->sizeOfGuaranteedSpace->value, repp->lifetimeGranted->value );

	soap_end (&soap);
	exit (0);
}
