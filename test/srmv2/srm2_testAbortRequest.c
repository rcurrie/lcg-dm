/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testAbortRequest.c,v 1.5 2006/01/27 09:37:00 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmAbortRequestResponse_ rep;
	struct ns1__ArrayOfTSURLReturnStatus *repfs;
	struct ns1__srmAbortRequestRequest req;
	struct ns1__TSURL *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;

	if (argc < 3) {
		fprintf (stderr, "usage: %s endPoint reqid\n", argv[0]);
		exit (1);
	}
	//nbfiles = argc - 2;

#if 0
	if (parsesurl (argv[2], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.requestToken =
		soap_malloc (&soap, sizeof(struct ns1__TRequestToken))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	req.requestToken->value = argv[2];

	//if (soap_call_ns1__srmAbortRequest (&soap, srm_endpoint, "AbortRequest",
	if (soap_call_ns1__srmAbortRequest (&soap, argv[1], "AbortRequest",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmAbortRequestResponse->returnStatus;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}

	soap_end (&soap);
	exit (0);
}

