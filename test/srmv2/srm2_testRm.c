/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testRm.c,v 1.6 2006/01/27 09:37:00 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmRmResponse_ rep;
	struct ns1__ArrayOfTSURLReturnStatus *repfs;
	struct ns1__srmRmRequest req;
	struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 1;

	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFilePaths =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTSURLInfo))) == NULL ||
	    (req.arrayOfFilePaths->surlInfoArray =
	        soap_malloc (&soap, nbfiles * sizeof(struct ns1__TSURLInfo *))) == NULL ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFilePaths->surlInfoArray[i] = 
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfFilePaths->__sizesurlInfoArray = nbfiles;

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfFilePaths->surlInfoArray[i];
		if ((reqfilep->SURLOrStFN =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->SURLOrStFN->value = argv[i+1];
		reqfilep->storageSystemInfo = NULL;
	}

	//req.arrayOfFilePaths = 0;  //  TEST0  CRASH
	//req.arrayOfFilePaths->surlInfoArray = 0;  //  TEST1  CRASH
	//req.arrayOfFilePaths->surlInfoArray[0] = 0;  //  TEST2  CRASH
	//req.arrayOfFilePaths->surlInfoArray[0]->SURLOrStFN = 0;  //  TEST4  CRASH

	if (soap_call_ns1__srmRm (&soap, srm_endpoint, "SrmRm",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmRmResponse->returnStatus;
	repfs = rep.srmRmResponse->arrayOfFileStatuses;

	/* wait for file "ready" */

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	for (i = 0; i < repfs->__sizesurlReturnStatusArray; i++) {
		if ((repfs->surlReturnStatusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->surlReturnStatusArray[i])->status->statusCode,
			    (repfs->surlReturnStatusArray[i])->status->explanation);
		else
			printf ("state[%d] = %d\n", i,
			    (repfs->surlReturnStatusArray[i])->status->statusCode);
	}
	soap_end (&soap);
	exit (0);
}
