/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGet.c,v 1.7 2006/01/27 09:37:00 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
	int nbproto = 0;
	char** lprotos;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio    ",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap ",
#endif
		""
	};
	int r = 0;
	char *r_token;
	struct ns1__srmPrepareToGetResponse_ rep;
	struct ns1__ArrayOfTGetRequestFileStatus *repfs;
	struct ns1__srmPrepareToGetRequest req;
	struct ns1__TGetFileRequest *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;

	if (argc < 4) {
		fprintf (stderr, "usage: %s proto u_token SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 3;

	if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
	
    nbproto = 1;
    /*
    lprotos = calloc (nbproto, sizeof(char*));
    lprotos[0] = strdup(argv[1]);
    */

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFileRequests =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTGetFileRequest))) == NULL ||
	    (req.arrayOfFileRequests->getRequestArray =
		soap_malloc (&soap, nbfiles * sizeof(struct ns1__TGetFileRequest *))) == NULL ||
	    (req.arrayOfTransferProtocols =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOf_USCORExsd_USCOREstring))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFileRequests->getRequestArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TGetFileRequest))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfFileRequests->__sizegetRequestArray = nbfiles;

	req.arrayOfTransferProtocols->stringArray = &argv[1];
	req.arrayOfTransferProtocols->__sizestringArray = nbproto;
	if ( strcmp(argv[2], "-") )
	  req.userRequestDescription = argv[2];

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfFileRequests->getRequestArray[i];
		memset (reqfilep, 0, sizeof(*reqfilep));
		if ((reqfilep->fromSURLInfo =
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL ||
		    (reqfilep->fromSURLInfo->SURLOrStFN =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->fromSURLInfo->SURLOrStFN->value = argv[i+3];
		reqfilep->fromSURLInfo->storageSystemInfo = NULL;

		/* GG special tests */
		/*
		if ( i == 0 )
		  //CRASHreqfilep->fromSURLInfo = NULL;
		//CRASHreqfilep->fromSURLInfo->SURLOrStFN = NULL;
		//CRASHreqfilep->fromSURLInfo->SURLOrStFN->value = NULL;
		*/
	}

#if 0
	if (soap_call_ns1__srmPrepareToGet (&soap, srm_endpoint, "PrepareToGet",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(PrepareToGet);
#endif

	reqstatp = rep.srmPrepareToGetResponse->returnStatus;
	repfs = rep.srmPrepareToGetResponse->arrayOfFileStatuses;
	if (rep.srmPrepareToGetResponse->requestToken) {
		r_token = rep.srmPrepareToGetResponse->requestToken->value;
		printf ("soap_call_ns1__srmPrepareToGet returned r_token %s\n",
		    r_token);
	}

	memset (&sreq, 0, sizeof(sreq));

	/* GG special tests */
	//GOODsreq.requestToken = NULL;
	/* Good token = 049672c4-0f64-471b-a393-c8fbeac9101c */
	//BADstrcpy(sreq.requestToken, special_token);
	//BADsreq.requestToken = strdup(special_token);
	//GOODstrcat(rep.srmPrepareToGetResponse->requestToken->value, "uu");
	//CRASHrep.srmPrepareToGetResponse->requestToken->value = NULL;
	//GOODstrcpy(rep.srmPrepareToGetResponse->requestToken->value, r_token);
	//GOODstrcpy(rep.srmPrepareToGetResponse->requestToken->value, strfry(r_token));

	sreq.requestToken = rep.srmPrepareToGetResponse->requestToken;

	/* wait for file "ready" */

	while (reqstatp->statusCode == SRM_USCOREREQUEST_USCOREQUEUED ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCOREINPROGRESS ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCORESUSPENDED) {
		printf("request state %d\n", reqstatp->statusCode);
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);

#if 0
		if (soap_call_ns1__srmStatusOfGetRequest (&soap, srm_endpoint,
		    "StatusOfGetRequest", &sreq, &srep)) {
			soap_print_fault (&soap, stderr);
			soap_end (&soap);
			exit (1);
		}
#else
	SOAP_CALL_NS1R(StatusOfGetRequest);
#endif

		reqstatp = srep.srmStatusOfGetRequestResponse->returnStatus;
		repfs = srep.srmStatusOfGetRequestResponse->arrayOfFileStatuses;
	}
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < repfs->__sizegetStatusArray; i++) {
		if (repfs->getStatusArray[i]->transferURL)
			printf ("state[%d] = %d, TURL = %s\n", i,
			    (repfs->getStatusArray[i])->status->statusCode,
			    (repfs->getStatusArray[i])->transferURL->value);
		else if ((repfs->getStatusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->getStatusArray[i])->status->statusCode,
			    (repfs->getStatusArray[i])->status->explanation);
		else
			printf ("state[%d] = %d\n", i,
			    (repfs->getStatusArray[i])->status->statusCode);
	}
	soap_end (&soap);
	exit (0);
}
