/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testReassignToUser.c,v 1.3 2005/11/22 10:39:09 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmReassignToUserResponse_ rep;
	//struct ArrayOfTSURLPermissionReturn *repfs;
	struct ns1__srmReassignToUserRequest req;
	struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	if (argc < 4) {
		fprintf (stderr, "usage: %s userID lifeT SURL\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 3;

	if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

#if 0
	if ((req.arrayOfSiteURLs =
		soap_malloc (&soap, sizeof(struct ArrayOfTSURLInfo))) == NULL ||
	    (req.arrayOfSiteURLs->__ptr =
	        soap_malloc (&soap, nbfiles * sizeof(struct ns1__TSURLInfo *))) == NULL ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfSiteURLs->__ptr[i] = 
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfSiteURLs->__size = nbfiles;

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfSiteURLs->__ptr[i];
		if ((reqfilep->SURLOrStFN =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->SURLOrStFN->value = argv[i+1];
	}
#endif

	    if ((req.assignedUser =
		 soap_malloc (&soap, sizeof(struct ns1__TUserID))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    req.assignedUser->value = argv[1];

	if ((req.lifeTimeOfThisAssignment =
		soap_malloc (&soap, sizeof(struct ns1__TLifeTimeInSeconds))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.lifeTimeOfThisAssignment->value = atoi(argv[2]);

	/* To set the SURL path ... */

	if ((req.path =
	    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL ||
	    (req.path->SURLOrStFN =
	    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.path->SURLOrStFN->value = argv[3];

	/* To send the request ... */

	if (soap_call_ns1__srmReassignToUser (&soap, srm_endpoint, "SrmReassignToUser",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmReassignToUserResponse->returnStatus;
	//repfs = rep.srmCheckPermissionResponse->arrayOfPermissions;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}

#if 0
	/* code not ready FIXME */
	if (! repfs) {
		printf ("arrayOfPermissions is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	for (i = 0; i < repfs->__size; i++) {
		if ((repfs->__ptr[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->__ptr[i])->status->statusCode,
			    (repfs->__ptr[i])->status->explanation);
		else
			printf ("state[%d] = %d, Perm = %d, SURL = %s\n", i,
			    (repfs->__ptr[i])->status->statusCode,
				(repfs->__ptr[i])->userPermission,
				(repfs->__ptr[i])->surl->value);
	}
#endif

	soap_end (&soap);
	exit (0);
}
