/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetSpaceMetaData.c,v 1.4 2005/11/22 10:39:09 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmGetSpaceMetaDataResponse_ rep;
	struct ns1__ArrayOfTMetaDataSpace *repfs;
	struct ns1__TMetaDataSpace *repmdp;
	struct ns1__srmGetSpaceMetaDataRequest req;
	struct ns1__TSpaceToken *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;
	char *s_type;

	enum ns1__TSpaceType {Volatile = 0, Durable = 1, Permanent = 2};

	if (argc < 3) {
		fprintf (stderr, "usage: %s endPoint SpaceTokenS\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 2;

#if 0
	if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

	if ((req.arrayOfSpaceTokens =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTSpaceToken))) == NULL||
	    (req.arrayOfSpaceTokens->tokenArray =
		soap_malloc (&soap, nbfiles * sizeof(struct ns1__TSpaceToken*))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfSpaceTokens->tokenArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TSpaceToken))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfSpaceTokens->__sizetokenArray = nbfiles;

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfSpaceTokens->tokenArray[i];
		reqfilep->value = argv[i+2];
	}


	/* To send the request ... */

#if 0
	if (soap_call_ns1__srmGetSpaceMetaData (&soap, argv[1], "SrmGetSpaceMetaData",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	srm_endpoint = argv[1];
	SOAP_CALL_NS1(GetSpaceMetaData);
#endif

	reqstatp = rep.srmGetSpaceMetaDataResponse->returnStatus;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}

        repfs = rep.srmGetSpaceMetaDataResponse->arrayOfSpaceDetails;

	if (! repfs) {
		printf ("arrayOfSpaceDetails is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	for (i = 0; i < repfs->__sizespaceDetailArray; i++) {
	  repmdp = repfs->spaceDetailArray[i];
/*  	  if ( (*repmdp->type) == Volatile ) */
/*  	    s_type = "Volatile"; */
	  
	  /*
	  printf ("state[%d] => s_token: %s s_type: %d client_dn: %s\n",
		  i,
		  repmdp->spaceToken->value,
		  (*repmdp->type),
		  repmdp->owner->value
		  );
	  
	  printf ("state[%d] => t_space: %llu g_space: %llu u_space: %llu a_lifetime: %llu r_lifetime: %llu client_dn: %s\n",
		  i,
		  repmdp->totalSize->value,
		  repmdp->guaranteedSize->value,
		  repmdp->unusedSize->value,
		  repmdp->lifetimeAssigned->value,
		  repmdp->lifetimeLeft->value,
		  repmdp->owner->value
		  );
	  */

	  printf ("state[%d] => s_token: %s s_type: %d t_space: %llu g_space: %llu u_space: %llu a_lifetime: %llu r_lifetime: %llu client_dn: %s\n",
		  i,
		  repmdp->spaceToken->value,
		  (*repmdp->type),
		  repmdp->totalSize->value,
		  repmdp->guaranteedSize->value,
		  repmdp->unusedSize->value,
		  repmdp->lifetimeAssigned->value,
		  repmdp->lifetimeLeft->value,
		  repmdp->owner->value
		  );

	}

	soap_end (&soap);
	exit (0);
}
