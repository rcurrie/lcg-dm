/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetSpaceToken.c,v 1.4 2005/11/22 10:39:09 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmGetSpaceTokenResponse_ rep;
	struct ns1__srmGetSpaceTokenRequest req;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__ArrayOfTSpaceToken *repfs;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	if (argc < 3) {
		fprintf (stderr, "usage: %s endPoint user_space_token_description\n", argv[0]);
		exit (1);
	}

#if 0
	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

	req.userSpaceTokenDescription = argv[2];
#if 0
	if (soap_call_ns1__srmGetSpaceToken (&soap, srm_endpoint, "GetSpaceToken",
#else
	if (soap_call_ns1__srmGetSpaceToken (&soap, argv[1], "GetSpaceToken",
#endif
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmGetSpaceTokenResponse->returnStatus;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}

        repfs = rep.srmGetSpaceTokenResponse->arrayOfPossibleSpaceTokens;

        if (! repfs) {
                printf ("arrayOfPossibleSpaceTokens is NULL\n");
                soap_end (&soap);
                exit (1);
        }

	printf("=== RETURNED SPACE TOKENS ===\n");
        for (i = 0; i < repfs->__sizetokenArray; i++) 
                printf("s_token[%d]: %s\n", i, repfs->tokenArray[i]->value);
	
	soap_end (&soap);
	exit (0);
}
