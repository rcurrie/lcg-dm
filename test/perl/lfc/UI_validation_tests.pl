#!/usr/bin/perl -w
use strict;
use lfc;

# stat an existing entry in the LFC and print the FILEID
my ($name,$stat,$fileid,$res);
$name = "/grid/dteam";

# $stat = new lfc::lfc_filestatg();
$stat = lfcc::new_lfc_filestatg();
$res = lfc::lfc_statg($name,undef,$stat);

if ($res == 0) {
	$fileid = lfcc::lfc_filestatg_fileid_get($stat);
	print "[OK] The FILEID for $name is $fileid\n";
} else {
	my $err_num = $lfc::serrno;
	my $err_string = lfc::sstrerror($err_num);
	print "[ERROR] There was an error while looking for $name: Error $err_num ($err_string)\n";
	exit(1);
}
lfcc::delete_lfc_filestatg($stat);
