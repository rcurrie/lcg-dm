/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetTransferProtocols.c,v 1.1 2006/12/19 20:05:25 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmGetTransferProtocolsResponse_ rep;
	struct ns1__srmGetTransferProtocolsRequest req;
#define SUP1 1	
#ifdef SUP1
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__ArrayOfTSupportedTransferProtocol *repfs;
#endif
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	int j;
	int nbprots;

	if (argc < 2) {
		fprintf (stderr, "usage: %s endPoint\n", argv[0]);
		exit (1);
	}

#if 0
	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

	//req.userTransferProtocolsDescription = argv[2];
#if 0
	if (soap_call_ns1__srmGetTransferProtocols (&soap, srm_endpoint, "GetTransferProtocols",
#else
	if (soap_call_ns1__srmGetTransferProtocols (&soap, argv[1], "GetTransferProtocols",
#endif
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}

#ifdef SUP1
	reqstatp = rep.srmGetTransferProtocolsResponse->returnStatus;
	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}

        repfs = rep.srmGetTransferProtocolsResponse->protocolInfo;

        if (! repfs) {
                printf ("protocolInfo is NULL\n");
                soap_end (&soap);
                exit (1);
        }

						    nbprots = repfs->__sizeprotocolArray;
	printf ("request protocols %d\n", nbprots);
						    for (i = 0; i < repfs->__sizeprotocolArray; i++) {
						      printf ("protocol[%d]: ExtraInfo: %s\n", i, repfs->protocolArray[i]->transferProtocol);
						      //printf ("protocol[%s]: ExtraInfo: %d\n", repfs->protocolArray[i]->transferProtocol, i);
							      //repfs->protocolArray[i]->attributes->__sizeextraInfoArray);
						      /*
						      if ( repfs->protocolArray[i]->attributes->__sizeextraInfoArray ) {
							for (j = 0; j < repfs->protocolArray[i]->attributes->__sizeextraInfoArray; j++) {
							  printf ("  Extra Info[%s]= %s\n", repfs->protocolArray[i]->attributes->extraInfoArray[j]->key,
								  repfs->protocolArray[i]->attributes->extraInfoArray[j]->value);
							}
						      }
						      */
						    }

#if 0
	printf("=== RETURNED SPACE TOKENS ===\n");
        for (i = 0; i < repfs->__sizetokenArray; i++) 
                printf("s_token[%d]: %s\n", i, repfs->tokenArray[i]->value);
#endif
#endif

#define SUP2 1	
#ifdef SUP2
	soap_end (&soap);
	exit (0);
#endif

}
