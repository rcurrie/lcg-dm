/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testPut.c,v 1.4 2007/07/19 08:06:24 grodid Exp $
// Last mod by GG (16/11/2006)

//Humm...#include "u64subr.h"
#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
	int nbproto = 0;
	static char *protocols[] = {
	  "gsiftp",
		"rfio",
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
	int r = 0;

	//OLD enum ns1__TRequestType f_type; // = 0;
	enum ns1__TStorageType {Volatile=0, Durable=1, Permanent=2};
	static enum ns1__TStorageType ftypes[] = {Volatile, Durable, Permanent};

	enum ns1__TRetentionPolicy {Replica = 0, Output = 1, Custodial = 2};
	enum ns1__TAccessLatency {Online = 0, Nearline = 1};
	static enum ns1__TRetentionPolicy rtypes[] = {Replica, Output, Custodial};
	static enum ns1__TAccessLatency atypes[] = {Online, Nearline};

	//struct ns1__TSizeInBytes f_size;
	//struct ns1__TLifeTimeInSeconds f_lifet;
	char *r_token;
	struct ns1__srmPrepareToPutResponse_ rep;
	struct ns1__ArrayOfTPutRequestFileStatus *repfs;
	struct ns1__srmPrepareToPutRequest req;
	struct ns1__TPutFileRequest *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfPutRequestResponse_ srep;
	struct ns1__srmStatusOfPutRequestRequest sreq;
	char *srm_endpoint;

	//struct ns1__TSURL *sreqfilep;
	////char *sreqfilep;

	char *special_token = "049672c4-0f64-471b-a393-c8fbeac9101c";

	//userRequestDescription
	//char *u_token;
	enum ns1__TOverwriteMode iovw = 2;

	char *ct;

	// Below, overW is overWrite flag   (-|0|1|2) for (Never|Always|WhenFilesAreDifferent) (defaults to 2 when '-')
	// protocol can be either '-' or 'rfio'                                                (defaults to gsiftp when '-')
	// storType is storageType flag     (-|0|1|2) for (Volatile|Durable|Permanent)         (defaults to 0 when '-')
	// retPol   is retentionPolicy flag (-|0|1|2) for (Replica|Output|Custodial)           (defaults to 0 when '-')
	// acLat    is accessLatency flag   (-|0|1)   for (Online|Nearline)                    (defaults to 0 when '-')

	//(void) fflush(NULL);
	(void) setbuf(stdout, NULL);
	(void) setbuf(stderr, NULL);

	if (argc < 13) {
		fprintf (stderr, "usage: %s [-|u_token] nbSURLs [-|overW] [-|pinTime] [-|lifeTime] [-|protocol] [-|spaceToken] [-|storType] [-|retPol] [-|acLat] SURL1 size1 [[SURL2 size2] [ ... ]]\n", argv[0]);
		exit (1);
	}
	//nbfiles = argc - 3;
	nbfiles = atoi(argv[2]);
	if (argc < (11+nbfiles*2)) {
	  //fprintf (stderr, "usage: %s u_token nbSURLs overW SURL1 pintime1 type1 size1 [SURL2 pintime2 type2 size2] [ ... ] [protocol] [spaceToken]\n", argv[0]);
		fprintf (stderr, "usage: %s [-|u_token] nbSURLs [-|overW] [-|pinTime] [-|lifeTime] [-|protocol] [-|spaceToken] [-|storType] [-|retPol] [-|acLat] SURL1 size1 [[SURL2 size2] [ ... ]]\n", argv[0]);
		exit (1);
	}

	if (parsesurl (argv[11], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK ;
	//printf ( " CGSI_TEST value: %d \n", flags);
	// Special BestMan - LBL
	////flags |= CGSI_OPT_DELEG_FLAG;
	//1807flags |= CGSI_OPT_DELEG_FLAG;
	//printf ( " CGSI_TEST value: %d \n", flags);
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	if ( (ct = getenv ("CRASH_TEST")) )
	//ct = getenv ("CRASH_TEST");
	  printf ( " CRASH_TEST value: %s \n", ct);
	else {
	  ct = NULL;
	  printf ( " srm_endpoint value: %s \n", srm_endpoint);
	}
	  //ct = strdup(" ");

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFileRequests =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTPutFileRequest))) == NULL ||
	    (req.arrayOfFileRequests->requestArray =
		soap_malloc (&soap, nbfiles * sizeof(struct ns1__TPutFileRequest *))) == NULL /* ||
	    (req.arrayOfTransferProtocols =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOf_USCORExsd_USCOREstring))) == NULL */ ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	req.authorizationID = NULL;

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFileRequests->requestArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TPutFileRequest))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfFileRequests->__sizerequestArray = nbfiles;
	/*
	req.arrayOfTransferProtocols->stringArray = protocols;
	req.arrayOfTransferProtocols->__sizestringArray = nbproto;
	*/
	if ( strcmp(argv[1], "-") )
	  req.userRequestDescription = argv[1];

	if ( strcmp(argv[3], "-") )
	  iovw = (enum ns1__TOverwriteMode) atoi(argv[3]);
	req.overwriteOption = &iovw; 

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfFileRequests->requestArray[i];
		memset (reqfilep, 0, sizeof(*reqfilep));
		/*
		if ((reqfilep->targetSURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->targetSURL->value = argv[i*4+4];
		*/
		reqfilep->targetSURL = argv[i*2+11];

		/* FIXME: replaced by something else ? */
		//reqfilep->targetSURL->storageSystemInfo = NULL;

#if 0  //  FIXME to move outside of the nbfiles loop
	if ((reqfilep->desiredFileLifeTime =
		soap_malloc (&soap, sizeof(struct ns1__TLifeTimeInSeconds))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
		//f_lifet = (ns1__TLifeTimeInSeconds) atoi(argv[i*4+4]);
		//f_lifet = atoul(argv[i*4+4]);

		/* FIXME: to be added in the input parameters */
		//reqfilep->desiredFileLifeTime->value = atol(argv[i*4+5]);

		reqfilep->desiredPinLifeTime->value = atol(argv[i*4+5]);

		/* OLD
		f_type = (enum ns1__TRequestType) atoi(argv[i*4+5]);
		reqfilep->fileStorageType = &f_type;
		*/
		reqfilep->fileStorageType = &ftypes[atoi(argv[i*4+6])];

	if ((reqfilep->knownSizeOfThisFile =
		soap_malloc (&soap, sizeof(struct ns1__TSizeInBytes))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	        //f_size = (ns1__TSizeInBytes) atoul(argv[i*4+6]);
	//2Gb		reqfilep->knownSizeOfThisFile->value = atol(argv[i*4+7]);
	//Humm...		reqfilep->knownSizeOfThisFile->value = strtou64(argv[i*4+7]);
		reqfilep->knownSizeOfThisFile->value = atoll(argv[i*4+7]);
#endif

	if ((reqfilep->expectedFileSize =
		soap_malloc (&soap, sizeof(ULONG64))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	*reqfilep->expectedFileSize = atoll(argv[i*2+12]);

	}
	//--------------------------- End of loop

	// reqfilep->fileStorageType = &ftypes[atoi(argv[8])];

	if ((req.desiredPinLifeTime =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	/* FIXME: to be added in the input parameters */
	if ( strcmp(argv[4], "-") )
	  *(req.desiredPinLifeTime) = atol(argv[4]);

	if ((req.desiredFileLifeTime =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	/* FIXME: to be added in the input parameters */
	if ( strcmp(argv[5], "-") )
	  *(req.desiredFileLifeTime) = atol(argv[5]);

	    if ((req.transferParameters =
		 soap_malloc (&soap, sizeof(struct ns1__TTransferParameters))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    //req.transferParametersHint->__sizesizeArray = 0;
	    //req.transferParametersHint->sizeArray = NULL;
	    req.transferParameters->accessPattern = NULL;
	    req.transferParameters->connectionType = NULL;
	    req.transferParameters->arrayOfClientNetworks = NULL;
	    req.transferParameters->arrayOfTransferProtocols = NULL;
	    //printf("Step 8-0\n");
	    if ((req.transferParameters->arrayOfTransferProtocols =
		 soap_malloc (&soap, nbproto*sizeof(struct ns1__ArrayOfString))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }

	    //printf("Step 8-1 nbproto: %d\n", nbproto);
	    // In any case, only one protocol is specified ...
	    req.transferParameters->arrayOfTransferProtocols->__sizestringArray = 1;

	    //printf("Step 8-2\n");
	//strcpy(req.transferParametersHint->arrayOfTransferProtocols->stringArray[0], protocols[0]);
	//req.transferParametersHint->arrayOfTransferProtocols->stringArray[0] = strdup(protocols[0]);
	//req.transferParametersHint->arrayOfTransferProtocols->stringArray[0] = protocols[0];
	//req.transferParametersHint->arrayOfTransferProtocols->stringArray[0] = soap_strdup(&soap, protocols[0]);

	    // Default case is to use "gsiftp"
	req.transferParameters->arrayOfTransferProtocols->stringArray = protocols;

	// Unless "rfio" is specified as the 'protocol' option
	//if ( argc > nbfiles*4+4 && argv[nbfiles*4+4] && ! strcmp(argv[nbfiles*4+4], "rfio") )
	if ( ! strcmp(argv[6], "rfio") )
	  req.transferParameters->arrayOfTransferProtocols->stringArray = &protocols[1];
	//printf("Step 8\n");

	/* Deprecated ...
	if ( argc > nbfiles*4+4 && argv[nbfiles*4+4] )
	  printf (" Protocol arg found: %s %d \n", argv[nbfiles*4+4], argc);
	if ( argc > nbfiles*4+5 && argv[nbfiles*4+5] )
	  printf (" spaceTok arg found: %s %d \n", argv[nbfiles*4+5], argc);
	*/

	/* Space Token if provided */
	
	//if ( strcmp(argv[7], "--") )
	if ( strncmp(argv[7], "-", 1) )
	  req.targetSpaceToken = argv[7];

	/* Storage Type if provided */
	
	if ( strcmp(argv[8], "-") )
	  req.desiredFileStorageType = &ftypes[atoi(argv[8])];
	else 
	  req.desiredFileStorageType = &ftypes[0];

#if 1
	if ((req.targetFileRetentionPolicyInfo =
		soap_malloc (&soap, sizeof(struct ns1__TRetentionPolicyInfo))) == NULL) {
		perror ("mallocR");
		soap_end (&soap);
		exit (1);
	}
	//req.retentionPolicyInfo->retentionPolicy = atoi(argv[7]);

	/* Retention Policy when provided */
	
	if ( strcmp(argv[9], "-") )
	  req.targetFileRetentionPolicyInfo->retentionPolicy = atoi(argv[9]);
	else 
	  req.targetFileRetentionPolicyInfo->retentionPolicy = 0;

	/* Access Latency when provided */
	
	if ( strcmp(argv[10], "-") )
	  req.targetFileRetentionPolicyInfo->accessLatency = &atypes[atoi(argv[10])];
	else 
	  req.targetFileRetentionPolicyInfo->accessLatency = &atypes[0];       
	//*req.targetFileRetentionPolicyInfo->accessLatency = 0;
#else
	// Test 29/12/06
	req.targetFileRetentionPolicyInfo = NULL;
#endif	

	// Special for tests ...
	////memset (&req, 0, sizeof(req));             //  CRASHes as well
	// Special for tests ...

	//req.arrayOfFileRequests->requestArray = 0;          //  Diag: Too many errors, NO CRASH0
	//req.arrayOfFileRequests->requestArray[0] = 0;       //  CRASH1
	//req.arrayOfFileRequests = 0;                 //  CRASH2
	//req.arrayOfTransferProtocols->stringArray = 0;     //  CRASH3
	//req.arrayOfTransferProtocols->stringArray[0] = 0;  //  CRASH4
	// If '&req' replaced with 0 below ==>             CRASH5
	//req.arrayOfTransferProtocols = 0;            //  CRASH6

	if ( ct ) {
	switch (ct[0]) {
	case 'a':
	  printf (" Case a \n");
	  req.arrayOfFileRequests->requestArray[0] = NULL;
	  //req.arrayOfFileRequests->requestArray = NULL;
	  break;
	case 'b':
	  printf (" Case b \n");
	  req.arrayOfFileRequests = NULL;
	  break;
	  /*
	case 'c':
	  printf (" Case c \n");
	  req.arrayOfTransferProtocols->stringArray = NULL;
	  break;
	case 'd':
	  printf (" Case d \n");
	  req.arrayOfTransferProtocols->stringArray[0] = NULL;
	  break;
	case 'f':
	  printf (" Case f \n");
	  req.arrayOfTransferProtocols = NULL;
	  break;
	  */
	case 'g':
	  /* Weird, this one does not crash ... */
	  printf (" Case g \n");
	  memset (&req, 0, sizeof(req));
	  break;
	default:
	  printf (" NO Action1 Case >%c< \n", ct[0]);
	  break;
	}
	}

#if 0

	//if ( ct[0] == 'e' )	  
	if ( ct && ! strncmp(ct, "e", 1) ) {  
	  printf (" Case e \n");
	  /* normal test */
	  if (soap_call_ns1__srmPrepareToPut (&soap, srm_endpoint, "PrepareToPut",
	    0, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	  /* special JPB value */
	  /*
	  if (soap_call_ns1__srmPrepareToPut (&soap, srm_endpoint, "PrepareToPut",
	    &req, 0)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	  */
	}
	else
	  if (soap_call_ns1__srmPrepareToPut (&soap, srm_endpoint, "PrepareToPut",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(PrepareToPut, re);
#endif

	/* FIXME: arrayOfFileStatuses when wsdl agreed */

	reqstatp = rep.srmPrepareToPutResponse->returnStatus;
	repfs = rep.srmPrepareToPutResponse->arrayOfFileStatuses;
	if (rep.srmPrepareToPutResponse->requestToken) {
		r_token = rep.srmPrepareToPutResponse->requestToken;
		printf ("soap_call_ns1__srmPrepareToPut returned r_token %s\n",
		    r_token);
		//printf("request state 00 %d\n", reqstatp->statusCode);
	}

	// New
	if (! repfs) 
	  printf ("PTP: arrayOfFileStatuses is NULL\n");
	printFileStatus(&soap, repfs);

	memset (&sreq, 0, sizeof(sreq));
	if ((sreq.arrayOfTargetSURLs =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
	    (sreq.arrayOfTargetSURLs->urlArray =
		soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	sreq.arrayOfTargetSURLs->__sizeurlArray = nbfiles;

#if 1
	for (i = 0; i < nbfiles; i++) {
	  ////sreqfilep = sreq.arrayOfTargetSURLs->urlArray[i];
		reqfilep = req.arrayOfFileRequests->requestArray[i];
		////sreqfilep = reqfilep->targetSURL;
		sreq.arrayOfTargetSURLs->urlArray[i] = reqfilep->targetSURL;
		/* GG special tests */
		/*
		if ( i == 0 )
		  //GOODsreq.arrayOfTargetSURLs = NULL;
		  //GOODsreq.arrayOfTargetSURLs->surlArray[i] = NULL;
		  //CRASHsreqfilep->value = NULL;
		  */
	}
#else
	sreq.arrayOfTargetSURLs->urlArray = req.arrayOfFileRequests->requestArray;
#endif

	sreq.requestToken = rep.srmPrepareToPutResponse->requestToken;

#if 0
	if (ct) {
	switch (ct[0]) {
	case 'h':
	  printf (" Case h \n");
	  sreq.requestToken = NULL;
	  break;
	case 'i':
	  printf (" Case i \n");
	  strcat(rep.srmPrepareToPutResponse->requestToken, "uu");
	  sreq.requestToken = rep.srmPrepareToPutResponse->requestToken;
	  break;
	case 'j':
	  printf (" Case j \n");
	  rep.srmPrepareToPutResponse->requestToken = NULL;
	  sreq.requestToken = rep.srmPrepareToPutResponse->requestToken;
	  break;
	case 'k':
	  printf (" Case k \n");
	  /* in fact the standard case ... for double-checking ... */
	  strcpy(rep.srmPrepareToPutResponse->requestToken, r_token);
	  sreq.requestToken = rep.srmPrepareToPutResponse->requestToken;
	  break;
	case 'l':
	  printf (" Case l \n");
	  strcpy(rep.srmPrepareToPutResponse->requestToken, strfry(r_token));
	  sreq.requestToken = rep.srmPrepareToPutResponse->requestToken;
	  break;
	default:
	  printf (" NO Action2 Case >%c< \n", ct[0]);
	  sreq.requestToken = rep.srmPrepareToPutResponse->requestToken;
	  break;
	}
	} else 
	  sreq.requestToken = rep.srmPrepareToPutResponse->requestToken;
#endif

	/* wait for file "ready" */

	while (reqstatp->statusCode == SRM_USCOREREQUEST_USCOREQUEUED ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCOREINPROGRESS ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCORESUSPENDED) {
	  printf ("request status0 %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
		printf("request state0 %d\n", reqstatp->statusCode);
	  if (reqstatp->explanation) 
	    printf ("explanation: %s\n", reqstatp->explanation);
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);

#if 0
		if (soap_call_ns1__srmStatusOfPutRequest (&soap, srm_endpoint,
		    "StatusOfPutRequest", &sreq, &srep)) {
			soap_print_fault (&soap, stderr);
			soap_end (&soap);
			exit (1);
		}
#else
	SOAP_CALL_NS1(StatusOfPutRequest, sre);
#endif

		reqstatp = srep.srmStatusOfPutRequestResponse->returnStatus;
		//printf("request state 2 %d\n", reqstatp->statusCode);
		//FSrepfs = srep.srmStatusOfPutRequestResponse->arrayOfFileStatuses;
	}
	//printf ( " After the srmStatusOfPutRequest Call ... \n");
	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREPARTIAL_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
	  if (reqstatp->explanation) {
	    printf ("explanation: %s\n", reqstatp->explanation);
// 	    if ( ! r ) {
// 	      soap_end (&soap);
// 	      exit (1);
// 	    }
	  } 
	    if ( ! r ) {
	      soap_end (&soap);
	      exit (1);
	    }
	  /*
	    soap_end (&soap);
	    exit (1);
	  */
	}

	//(void) fflush(NULL);

#define FS 1
#ifdef FS
	repfs = srep.srmStatusOfPutRequestResponse->arrayOfFileStatuses;
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if (repfs->statusArray[i]->transferURL)
#if 0
			printf ("state[%d] = %d, %s, TURL = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
			    (repfs->statusArray[i])->transferURL);
#else
		if ( (repfs->statusArray[i])->remainingPinLifetime && (repfs->statusArray[i])->remainingFileLifetime )
			printf ("state[%d] = %d, %s, TURL = %s pinT = %d, lifeT = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->transferURL,
				*(repfs->statusArray[i])->remainingPinLifetime,
				*(repfs->statusArray[i])->remainingFileLifetime);
		else
			printf ("state[%d] = %d, %s, TURL = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->transferURL);
			/*
				soap_dateTime2s (&soap, (time_t) ((repfs->statusArray[i])->remainingPinLifetime)),
				soap_dateTime2s (&soap, (time_t) ((repfs->statusArray[i])->remainingFileLifetime)));
			*/
#endif
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
#if 0
			printf ("state[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
#else
			printf ("state[%d] = %d, %s\n", i,
				(repfs->statusArray[i])->status->statusCode, 
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode));
#endif
	}
#endif


	soap_end (&soap);
	exit (0);
}

int
printFileStatus (struct soap* soap, struct ns1__ArrayOfTPutRequestFileStatus *repfs) {
  int i;

	if (! repfs) {
		printf ("SUB: arrayOfFileStatuses is NULL\n");
		/*
		soap_end (soap);
		exit (0);
		*/
		return;
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if (repfs->statusArray[i]->transferURL)
			printf ("pstate[%d] = %d, %s, TURL = %s pinT = %d, lifeT = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->transferURL,
				*(repfs->statusArray[i])->remainingPinLifetime,
				*(repfs->statusArray[i])->remainingFileLifetime);
			/*
				soap_dateTime2s (&soap, (time_t) ((repfs->statusArray[i])->remainingPinLifetime)),
				soap_dateTime2s (&soap, (time_t) ((repfs->statusArray[i])->remainingFileLifetime)));
			*/
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("pstate[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
#if 0
			printf ("pstate[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
#else
			printf ("pstate[%d] = %d, %s\n", i,
				(repfs->statusArray[i])->status->statusCode, 
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode));
#endif
	}

	return;

}
