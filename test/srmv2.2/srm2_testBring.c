/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testBring.c,v 1.1 2006/12/19 20:05:24 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
	int nbproto = 0;
	char** lprotos;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio    ",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap ",
#endif
		""
	};
	int r = 0;
	char *r_token;
	struct ns1__srmBringOnlineResponse_ rep;
	struct ns1__ArrayOfTBringOnlineRequestFileStatus *repfs;
	struct ns1__srmBringOnlineRequest req;
	struct ns1__TGetFileRequest *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfBringOnlineRequestResponse_ srep;
	struct ns1__srmStatusOfBringOnlineRequestRequest sreq;
	char *srm_endpoint;

	if (argc < 3) {
		fprintf (stderr, "usage: %s u_token SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 2;

	if (parsesurl (argv[2], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
	
	//nbproto = 1;
    /*
    lprotos = calloc (nbproto, sizeof(char*));
    lprotos[0] = strdup(argv[1]);
    */

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFileRequests =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTGetFileRequest))) == NULL ||
	    (req.arrayOfFileRequests->requestArray =
		soap_malloc (&soap, nbfiles * sizeof(struct ns1__TGetFileRequest *))) == NULL /* ||
	    (req.arrayOfTransferProtocols =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOf_USCORExsd_USCOREstring))) == NULL */ ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFileRequests->requestArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TGetFileRequest))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfFileRequests->__sizerequestArray = nbfiles;

	/*
	req.arrayOfTransferProtocols->stringArray = &argv[1];
	req.arrayOfTransferProtocols->__sizestringArray = nbproto;
	*/
	if ( strcmp(argv[1], "-") )
	  req.userRequestDescription = argv[1];

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfFileRequests->requestArray[i];
		memset (reqfilep, 0, sizeof(*reqfilep));
		/*
		if ((reqfilep->sourceSURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->sourceSURL->value = argv[i+2];
		*/
		reqfilep->sourceSURL = argv[i+2];

		/* FIXME: still existing ? */
		//??reqfilep->sourceSURL->storageSystemInfo = NULL;
		/* FIXME: new input parameter to fill in */
		reqfilep->dirOption = NULL;

		/* FIXME: many new file parameters to fill in */

		/* GG special tests */
		/*
		if ( i == 0 )
		  //CRASHreqfilep->fromSURLInfo = NULL;
		//CRASHreqfilep->fromSURLInfo->SURLOrStFN = NULL;
		//CRASHreqfilep->fromSURLInfo->SURLOrStFN->value = NULL;
		*/
	}

#if 0
	if (soap_call_ns1__srmBringOnline (&soap, srm_endpoint, "BringOnline",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(BringOnline, re);
#endif

	/* FIXME: arrayOfFileStatuses when wsdl agreed NONEED ? */

	reqstatp = rep.srmBringOnlineResponse->returnStatus;
	//repfs = rep.srmBringOnlineResponse->arrayOfFileStatuses;
	if (rep.srmBringOnlineResponse->requestToken) {
		r_token = rep.srmBringOnlineResponse->requestToken;
		printf ("soap_call_ns1__srmBringOnline returned r_token %s\n",
		    r_token);
	}

	memset (&sreq, 0, sizeof(sreq));

	/* GG special tests */
	//GOODsreq.requestToken = NULL;
	/* Good token = 049672c4-0f64-471b-a393-c8fbeac9101c */
	//BADstrcpy(sreq.requestToken, special_token);
	//BADsreq.requestToken = strdup(special_token);
	//GOODstrcat(rep.srmBringOnlineResponse->requestToken->value, "uu");
	//CRASHrep.srmBringOnlineResponse->requestToken->value = NULL;
	//GOODstrcpy(rep.srmBringOnlineResponse->requestToken->value, r_token);
	//GOODstrcpy(rep.srmBringOnlineResponse->requestToken->value, strfry(r_token));

	sreq.requestToken = rep.srmBringOnlineResponse->requestToken;

	/* wait for file "ready" */

	while (reqstatp->statusCode == SRM_USCOREREQUEST_USCOREQUEUED ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCOREINPROGRESS ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCORESUSPENDED) {
	printf ("request status0 %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
		printf("request state0 %d\n", reqstatp->statusCode);
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);

#if 0
		if (soap_call_ns1__srmStatusOfBringOnlineRequest (&soap, srm_endpoint,
		    "StatusOfBringOnlineRequest", &sreq, &srep)) {
			soap_print_fault (&soap, stderr);
			soap_end (&soap);
			exit (1);
		}
#else
	SOAP_CALL_NS1(StatusOfBringOnlineRequest, sre);
#endif

		reqstatp = srep.srmStatusOfBringOnlineRequestResponse->returnStatus;
		repfs = srep.srmStatusOfBringOnlineRequestResponse->arrayOfFileStatuses;

		/* TEMPORARY, because not fully implemented ... */
		if ( r>3 ) {
		  printf ("request state 00\n");
			soap_end (&soap);
			exit (1);
		}
	}

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (&soap);
		exit (1);
		*/
	}
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if (repfs->statusArray[i]->sourceSURL)
			printf ("state[%d] = %d, %s, TURL = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
			    (repfs->statusArray[i])->sourceSURL);
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
			printf ("state[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
	}
	soap_end (&soap);
	exit (0);
}
