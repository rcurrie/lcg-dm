/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testRmdir.c,v 1.2 2007/04/25 09:40:29 grodid Exp $

/* TODO: let the user choose if recursive flag should be set or not */
#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
	static enum xsd__boolean trueoption = true_;  //  JPB
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	int isurl = 1; // pointer to the 1st actual SURL in the arg list
	char *r_token;
	struct ns1__srmRmdirResponse_ rep;
	struct ns1__srmRmdirRequest req;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;
	int boolR = 0;

	if (argc < 2) {
		fprintf (stderr, "usage: %s [-R] SURL\n", argv[0]);
		exit (1);
	}

	if ( strchr(argv[1], '-' ) == argv[1] ) {
	  isurl = 2;
          if ( strchr(argv[1], 'R' ) )
            boolR = 1 ;
	}

	if (parsesurl (argv[isurl], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

#if 0
	if ((req.directoryPath =
	    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL /* ||
	    //(req.directoryPath->SURLOrStFN =
	    (req.directoryPath->SURL =
	    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL */ ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
#endif
	//req.directoryPath->SURLOrStFN->value = argv[1];
	//OLD2req.directoryPath = argv[1];
	req.SURL = argv[isurl];
	//req.directoryPath->storageSystemInfo = NULL;
	req.storageSystemInfo = NULL;
	if ( boolR ) 
	  req.recursive = &trueoption;  //  GG new
	printf("Step 1: %s\n", req.SURL);

#if 0
	if (soap_call_ns1__srmRmdir (&soap, srm_endpoint, "Rmdir",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(Rmdir, re);
#endif

	//printf("Step 2\n");
	reqstatp = rep.srmRmdirResponse->returnStatus;
	//printf("Step 3\n");

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}

	soap_end (&soap);
	exit (0);
}
