/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testCheckPermission.c,v 1.1 2006/12/19 20:05:24 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmCheckPermissionResponse_ rep;
	struct ns1__ArrayOfTSURLPermissionReturn *repfs;
	struct ns1__srmCheckPermissionRequest req;
	struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	enum ns1__TPermissionMode {None=0, X=1, W=2, WX=3, R=4, RX=5, RW=6, RWX=7};
	static enum ns1__TPermissionMode f_modes[] = {None, X, W, WX, R, RX, RW, RWX};
	char* sf_modes[] = {"None", "X", "W", "WX", "R", "RX", "RW", "RWX"};

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 1;

	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfSURLs =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
	    (req.arrayOfSURLs->urlArray =
	        soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	/*
	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfSURLs->surlInfoArray[i] = 
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	*/
	req.arrayOfSURLs->__sizeurlArray = nbfiles;

	for (i = 0; i < nbfiles; i++) {
	  //reqfilep = req.arrayOfSURLs->surlInfoArray[i];
		/*
		if ((reqfilep->SURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		*/
		//reqfilep->SURL = argv[i+1];
	  req.arrayOfSURLs->urlArray[i] = argv[i+1];
	}

	if (soap_call_ns1__srmCheckPermission (&soap, srm_endpoint, "CheckPermission",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmCheckPermissionResponse->returnStatus;
	repfs = rep.srmCheckPermissionResponse->arrayOfPermissions;

	/* wait for file "ready" */

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE &&
	    reqstatp->statusCode != SRM_USCOREPARTIAL_USCORESUCCESS
	    ) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	if (! repfs) {
		printf ("arrayOfPermissions is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request surlPermissionArray %d\n", repfs->__sizesurlPermissionArray);
	for (i = 0; i < repfs->__sizesurlPermissionArray; i++) {
		if ((repfs->surlPermissionArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->surlPermissionArray[i])->status->statusCode,
			    (repfs->surlPermissionArray[i])->status->explanation);
		else
#if 0
			printf ("state[%d] = %d, %s, Perm = %s, SURL = %s\n", i,
			    (repfs->surlPermissionArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->surlPermissionArray[i])->status->statusCode),
				//(repfs->surlPermissionArray[i])->userPermission,
				sf_modes[*((repfs->surlPermissionArray[i])->permission)],
				(repfs->surlPermissionArray[i])->surl);
#else
		printf ("state[%d] = %d, %s", i,
			    (repfs->surlPermissionArray[i])->status->statusCode,
			soap_ns1__TStatusCode2s (&soap, (repfs->surlPermissionArray[i])->status->statusCode));
		if ( (repfs->surlPermissionArray[i])->permission )
		  printf (", Perm = %s", 
			  sf_modes[*((repfs->surlPermissionArray[i])->permission)]);
		printf (", SURL = %s\n",
			(repfs->surlPermissionArray[i])->surl);
			
#endif
	}
	soap_end (&soap);
	exit (0);
}
