/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testExtendFileLifeTime.c,v 1.3 2007/03/20 17:31:12 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmExtendFileLifeTimeResponse_ rep;
	//struct ns1__ArrayOfTSURLReturnStatus *repfs;
	struct ns1__ArrayOfTSURLLifetimeReturnStatus *repfs;
	struct ns1__srmExtendFileLifeTimeRequest req;
	struct ns1__TSURL *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;

	if (argc < 5) {
		fprintf (stderr, "usage: %s [-|reqid] [-|pinlifetime|-1] [newlifetime|-|-1] SURLs\n", argv[0]);
		exit (1);
	}

	/* Case where [ r p n ] -> must be [ r p - ] */
	if ( strncmp(argv[1], "-", 1) )
	  if ( strcmp(argv[2], "-") )
	    if ( strcmp(argv[3], "-") ) {
	      fprintf (stderr, "usage: %s reqid pinlifetime - SURLs\n", argv[0]);
		exit (1);
	}

	/* Case where [ - - - ] -> must be [ - - n ] */
	if ( ! strncmp(argv[1], "-", 1) )
	  if ( ! strcmp(argv[2], "-") )
	    if ( ! strcmp(argv[3], "-") && strncmp(argv[3], "-1", 2) ) {
	      fprintf (stderr, "usage: %s - - newlifetime SURLs\n", argv[0]);
		exit (1);
	}

	/* Case where [ r - ? ] -> must be [ r p - ] */
	if ( strncmp(argv[1], "-", 1) )
	  if ( ! strcmp(argv[2], "-") ) {
	    fprintf (stderr, "usage: %s reqid pinlifetime - SURLs\n", argv[0]);
		exit (1);
	}

	/* Case where [ - p ? ] -> must be [ - - n ] */
	if ( ! strncmp(argv[1], "-", 1) )
	  if ( strcmp(argv[2], "-") ) {
	    fprintf (stderr, "usage: %s - - newlifetime SURLs\n", argv[0]);
		exit (1);
	}

	nbfiles = argc - 4;

	if (parsesurl (argv[4], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
#if 0
	if ((req.requestToken =
		soap_malloc (&soap, sizeof(struct ns1__TRequestToken))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.requestToken->value = argv[1];

	if ((req.arrayOfSURLs =
		soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	/* FIXME: it's now an array to fill in ! DONE */
	req.arrayOfSURLs->value = argv[3];
#else
	if ( /* (req.requestToken =
		soap_malloc (&soap, sizeof(struct ns1__TRequestToken))) == NULL || */
	    (req.arrayOfSURLs =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL || 
	    (req.arrayOfSURLs->urlArray =
		soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	/*
	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfSURLs->surlArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	*/
	if ( strncmp(argv[1], "-", 1) ) {
	  req.requestToken = argv[1];
	  //printf("Step 0\n");
	} else
	  req.requestToken = NULL;

	req.arrayOfSURLs->__sizeurlArray = nbfiles;

	for (i = 0; i < nbfiles; i++) {
	  //reqfilep = req.arrayOfSURLs->urlArray[i];
	  //reqfilep->value = argv[i+4];
	  req.arrayOfSURLs->urlArray[i] = argv[i+4];
		//NOreqfilep->storageSystemInfo = NULL;
	}
#endif

	
	if ((req.newFileLifeTime =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	
	if ( strcmp(argv[3], "-") || ! strncmp(argv[3], "-1", 2) ) {
	  //req.newFileLifeTime = atoi(argv[2]);
	  *(req.newFileLifeTime) = atoi(argv[3]);
	  printf("Step 1\n");
	} else
	  req.newFileLifeTime = NULL;


	/* FIXME: add a new argument newPinLifeTime DONE */

	
	if ((req.newPinLifeTime =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	
	//if ( strchr(argv[3], '-' ) == argv[3] ) 
	if ( strcmp(argv[2], "-") || ! strncmp(argv[2], "-1", 2) ) {
	  *(req.newPinLifeTime) = atoi(argv[2]);
	  printf("Step 2\n");
	} else
	  req.newPinLifeTime = NULL;

	/*
	else
	  req.newPinLifeTime = NULL;
	*/
	
	/* GG special tests */
	//CRASHreq.requestToken->value = NULL;
	//GOODreq.requestToken = NULL;
	//CRASHreq.siteURL->value = NULL;
	//GOODreq.siteURL = NULL;
	//GOODreq.newLifeTime->value = 0;
	//GOODreq.newLifeTime = NULL;

#if 0
	if (soap_call_ns1__srmExtendFileLifeTime (&soap, srm_endpoint, "ExtendFileLifeTime",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(ExtendFileLifeTime, re);
#endif

	reqstatp = rep.srmExtendFileLifeTimeResponse->returnStatus;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (&soap);
		exit (1);
		*/
	}
	
#if 0
	//if (rep.srmExtendFileLifeTimeResponse->fileLifetime) 
	if (rep->fileLifetime) 
		printf("time extended to: %d\n", rep.srmExtendFileLifeTimeResponse->fileLifetime);

	/* FIXME: add a new print newPinLifeTime DONE */
	if (rep.srmExtendFileLifeTimeResponse->pinLifetime) 
		printf("pintime extended to: %d\n", rep.srmExtendFileLifeTimeResponse->pinLifetime);
#endif

	/* FIXME: add a new print for filestatuses DONE */
	repfs = rep.srmExtendFileLifeTimeResponse->arrayOfFileStatuses;
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if ((repfs->statusArray[i])->status->explanation)
			printf ("state[%d] = %d, %s, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
			    (repfs->statusArray[i])->status->explanation);
		else {
#if 0
			printf ("state[%d] = %d, %s, pinLifetime: %d, fileLifetime: %d, SURL = %s\n", 
				i,
				(repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
				*(repfs->statusArray[i])->pinLifetime,
				*(repfs->statusArray[i])->fileLifetime,
				(repfs->statusArray[i])->surl
				);
#else
			printf ("state[%d] = %d, %s", 
				i,
				(repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode));
			if ( (repfs->statusArray[i])->pinLifetime )
			  /* FIXME */
			  //printf (", pinLifetime: %s", soap_dateTime2s (&soap, (time_t) *(repfs->statusArray[i])->pinLifetime));
			  printf (", pinLifetime: %d", *(repfs->statusArray[i])->pinLifetime);
			if ( (repfs->statusArray[i])->fileLifetime )
			  printf (", fileLifetime: %d", *(repfs->statusArray[i])->fileLifetime);
			printf (", SURL = %s\n", 
				(repfs->statusArray[i])->surl);
#endif
		}
	}

	soap_end (&soap);
	exit (0);
}

