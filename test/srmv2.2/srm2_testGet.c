/*
 * Copyright (C) 2004-2008 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGet.c,v 1.3 2008/02/05 05:44:30 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
	int nbproto = 0;
	char** lprotos;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio    ",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap ",
#endif
		""
	};
	int r = 0;
	char *r_token;
	struct ns1__srmPrepareToGetResponse_ rep;
	struct ns1__ArrayOfTGetRequestFileStatus *repfs;
	struct ns1__srmPrepareToGetRequest req;
	struct ns1__TGetFileRequest *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;
	int pptonly = 0;

	(void) setbuf(stdout, NULL);
	(void) setbuf(stderr, NULL);

	/* GG : '+' in place of 'u_token' arg means 'PrepareToGet step only' 09/01/08 */
	if (argc < 5) {
		fprintf (stderr, "usage: %s [-|protocol] [+|-|u_token] [-|pinTime] SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 4;

	if (parsesurl (argv[4], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
	
    nbproto = 1;
    
    lprotos = calloc (nbproto, sizeof(char*));
    if ( strcmp(argv[1], "-") )
      lprotos[0] = strdup(argv[1]);
    else
      nbproto = 0;
    

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFileRequests =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTGetFileRequest))) == NULL ||
	    (req.arrayOfFileRequests->requestArray =
		soap_malloc (&soap, nbfiles * sizeof(struct ns1__TGetFileRequest *))) == NULL /* ||
	    (req.arrayOfTransferProtocols =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOf_USCORExsd_USCOREstring))) == NULL */ ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFileRequests->requestArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TGetFileRequest))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfFileRequests->__sizerequestArray = nbfiles;

	/*
	req.arrayOfTransferProtocols->stringArray = &argv[1];
	req.arrayOfTransferProtocols->__sizestringArray = nbproto;
	*/
	if ( ! strcmp(argv[2], "+") )
	  pptonly = 1;
	else 
	  if ( strcmp(argv[2], "-") )
	    req.userRequestDescription = argv[2];

	if ((req.desiredPinLifeTime =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	if ( strcmp(argv[3], "-") )
	  *(req.desiredPinLifeTime) = atol(argv[3]);

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfFileRequests->requestArray[i];
		memset (reqfilep, 0, sizeof(*reqfilep));
		/*
		if ((reqfilep->sourceSURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->sourceSURL->value = argv[i+3];
		*/
		reqfilep->sourceSURL = argv[i+4];
		/* FIXME: still existing ? */
		//??reqfilep->sourceSURL->storageSystemInfo = NULL;
		/* FIXME: new input parameter to fill in */
		reqfilep->dirOption = NULL;

		/* FIXME: many new file parameters to fill in */

		/* GG special tests */
		/*
		if ( i == 0 )
		  //CRASHreqfilep->fromSURLInfo = NULL;
		//CRASHreqfilep->fromSURLInfo->SURLOrStFN = NULL;
		//CRASHreqfilep->fromSURLInfo->SURLOrStFN->value = NULL;
		*/
	}

	//printf("Step 7\n");
	    if ((req.transferParameters =
		 soap_malloc (&soap, sizeof(struct ns1__TTransferParameters))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    //req.transferParametersHint->__sizesizeArray = 0;
	    //req.transferParametersHint->sizeArray = NULL;
	    req.transferParameters->accessPattern = NULL;
	    req.transferParameters->connectionType = NULL;
	    req.transferParameters->arrayOfClientNetworks = NULL;
	    req.transferParameters->arrayOfTransferProtocols = NULL;
	    //printf("Step 8-0\n");
	    if ((req.transferParameters->arrayOfTransferProtocols =
		 soap_malloc (&soap, nbproto*sizeof(struct ns1__ArrayOfString))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    //printf("Step 8-1 nbproto: %d\n", nbproto);
	    req.transferParameters->arrayOfTransferProtocols->__sizestringArray = 1;
	    //printf("Step 8-2\n");
	req.transferParameters->arrayOfTransferProtocols->stringArray = lprotos;
	//printf("Step 8\n");

#if 0
	if (soap_call_ns1__srmPrepareToGet (&soap, srm_endpoint, "PrepareToGet",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(PrepareToGet, re);
#endif

	/* FIXME: arrayOfFileStatuses when wsdl agreed */

	reqstatp = rep.srmPrepareToGetResponse->returnStatus;
	repfs = rep.srmPrepareToGetResponse->arrayOfFileStatuses;
	if (rep.srmPrepareToGetResponse->requestToken) {
		r_token = rep.srmPrepareToGetResponse->requestToken;
		printf ("soap_call_ns1__srmPrepareToGet returned r_token %s\n",
		    r_token);
	}

	// New
	if (! repfs) 
	  printf ("PTG: arrayOfFileStatuses is NULL\n");
	printFileStatus(&soap, repfs);

	if (pptonly) {
	      soap_end (&soap);
	      exit (0);
	}

	memset (&sreq, 0, sizeof(sreq));

	/* GG special tests */
	//GOODsreq.requestToken = NULL;
	/* Good token = 049672c4-0f64-471b-a393-c8fbeac9101c */
	//BADstrcpy(sreq.requestToken, special_token);
	//BADsreq.requestToken = strdup(special_token);
	//GOODstrcat(rep.srmPrepareToGetResponse->requestToken->value, "uu");
	//CRASHrep.srmPrepareToGetResponse->requestToken->value = NULL;
	//GOODstrcpy(rep.srmPrepareToGetResponse->requestToken->value, r_token);
	//GOODstrcpy(rep.srmPrepareToGetResponse->requestToken->value, strfry(r_token));

	sreq.requestToken = rep.srmPrepareToGetResponse->requestToken;

	/* wait for file "ready" */

	while (reqstatp->statusCode == SRM_USCOREREQUEST_USCOREQUEUED ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCOREINPROGRESS ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCORESUSPENDED) {
	  printf ("request status0 %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	  printf("request state0 %d\n", reqstatp->statusCode);

		sleep ((r++ == 0) ? 1 : DEFPOLLINT);

#if 0
		if (soap_call_ns1__srmStatusOfGetRequest (&soap, srm_endpoint,
		    "StatusOfGetRequest", &sreq, &srep)) {
			soap_print_fault (&soap, stderr);
			soap_end (&soap);
			exit (1);
		}
#else
	SOAP_CALL_NS1(StatusOfGetRequest, sre);
#endif

		reqstatp = srep.srmStatusOfGetRequestResponse->returnStatus;
		//FSrepfs = srep.srmStatusOfGetRequestResponse->arrayOfFileStatuses;

	  repfs = srep.srmStatusOfGetRequestResponse->arrayOfFileStatuses;
	  printFileStatus(&soap, repfs);

	}
	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREPARTIAL_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
	  if (reqstatp->explanation) {
	    printf ("explanation: %s\n", reqstatp->explanation);
	  } 
	    if ( ! r ) {
	      soap_end (&soap);
	      exit (1);
	    }
	  /*
	    soap_end (&soap);
	    exit (1);
	  */
	}

#define FS 1
#ifdef FS
	repfs = srep.srmStatusOfGetRequestResponse->arrayOfFileStatuses;
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if (repfs->statusArray[i]->transferURL)
			printf ("state[%d] = %d, %s, TURL = %s\n", i,
				(repfs->statusArray[i])->status->statusCode, 
			    soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
			    (repfs->statusArray[i])->transferURL);
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
#if 0
			printf ("state[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
#else
			printf ("state[%d] = %d, %s\n", i,
				(repfs->statusArray[i])->status->statusCode, 
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode));
#endif
	}
#endif

	soap_end (&soap);
	exit (0);
}



int
printFileStatus (struct soap* soap, struct ns1__ArrayOfTGetRequestFileStatus *repfs) {
  int i;

	if (! repfs) {
		printf ("SUB: arrayOfFileStatuses is NULL\n");
		/*
		soap_end (soap);
		exit (0); 
		*/
		return;
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if (repfs->statusArray[i]->transferURL)
			printf ("pstate[%d] = %d, %s, TURL = %s\n", i,
				(repfs->statusArray[i])->status->statusCode, 
			    soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode),
			    (repfs->statusArray[i])->transferURL);
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("pstate[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
#if 0
			printf ("pstate[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
#else
			printf ("pstate[%d] = %d, %s\n", i,
				(repfs->statusArray[i])->status->statusCode, 
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode));
#endif
	}

	return;

}
