/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testReserveSpace.c,v 1.1 2006/12/19 20:05:28 grodid Exp $

/*  #include "u64subr.h" */
#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 1
	int nbproto = 0;
	static char *protocols[] = {
	  "gsiftp",
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;

	static enum ns1__TRetentionPolicy retpol[] = { REPLICA, OUTPUT, CUSTODIAL, };
	char* s_retpol[] = { "REPLICA", "OUTPUT", "CUSTODIAL", };

	char *s_token;
	struct ns1__srmReserveSpaceResponse_ rep;
	//struct ArrayOfTSURLPermissionReturn *repfs;
	struct ns1__srmReserveSpaceRequest req;
	//struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__srmReserveSpaceResponse *repp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;
	char tmpbut[25];
	char tmpbug[25];

	if (argc < 8) {
		fprintf (stderr, "usage: %s endPoint user_space_token_description storage_system_info sizeOfTotalSpaceDesired sizeOfGuaranteedSpaceDesired lifetimeOfSpaceToReserve typeOfSpace\n", argv[0]);
		exit (1);
	}
	//nbfiles = argc - 3;

#if 0
	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	} else
	  printf ("request endpoint %s\n", srm_endpoint);
#endif

	while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	//printf("Step 0\n");

#if 0
	/*
	    if ((req.spaceToken =
		 soap_malloc (&soap, sizeof(struct ns1__TSpaceToken))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	*/
	    req.spaceToken = argv[2];
#endif

#if 0
	    if ((req.storageSystemInfo =
		 soap_malloc (&soap, sizeof(struct ns1__TExtraInfo))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    //req.storageSystemInfo->value = argv[3];
	    /*
	    req.storageSystemInfo->__sizeextraInfoArray = 1;
	    req.storageSystemInfo->extraInfoArray[0]->key = "0";
	    req.storageSystemInfo->extraInfoArray[0]->value = argv[3];
	    */
	    req.storageSystemInfo->__sizeextraInfoArray = 0;
	    req.storageSystemInfo->extraInfoArray = NULL;
#else
	    req.storageSystemInfo = NULL;
#endif
	//printf("Step 1\n");

	
	if ((req.desiredSizeOfTotalSpace=
	     //soap_malloc (&soap, sizeof(struct ns1__TSizeInBytes))) == NULL) {
		soap_malloc (&soap, sizeof(ULONG64 *))) == NULL) {
		perror ("malloc0");
		soap_end (&soap);
		exit (1);
	}
	
	*(req.desiredSizeOfTotalSpace) = atoll(argv[4]);
	//printf("Step 2\n");

	/*
	if ((req.desiredSizeOfGuaranteedSpace =
		soap_malloc (&soap, sizeof(struct ns1__TSizeInBytes))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	*/
	req.desiredSizeOfGuaranteedSpace = atoll(argv[5]);
	//printf("Step 3\n");

	
	if ((req.desiredLifetimeOfReservedSpace =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc1");
		soap_end (&soap);
		exit (1);
	}
	
	*(req.desiredLifetimeOfReservedSpace) = atoi(argv[6]);
	//printf("Step 4\n");

	    if ((req.arrayOfExpectedFileSizes =
		 soap_malloc (&soap, sizeof(struct ns1__ArrayOfUnsignedLong))) == NULL) {
	      perror ("malloc2");
	      soap_end (&soap);
	      exit (1);
	    }
	    req.arrayOfExpectedFileSizes->__sizeunsignedLongArray = 0;
	    req.arrayOfExpectedFileSizes->unsignedLongArray = NULL;
	    //req.arrayOfExpectedFileSizes->extraInfoArray[0]->key = "0";
	    //req.arrayOfExpectedFileSizes->extraInfoArray[0]->value = argv[3];
	//printf("Step 5\n");

	req.userSpaceTokenDescription = argv[2];
	//printf("Step 6\n");

	if ((req.retentionPolicyInfo =
		soap_malloc (&soap, sizeof(struct ns1__TRetentionPolicyInfo))) == NULL) {
		perror ("malloc3");
		soap_end (&soap);
		exit (1);
	}
	////TOFIXreq.typeOfSpace = atoi(argv[7]);
	req.retentionPolicyInfo->retentionPolicy = atoi(argv[7]);
	//req.retentionPolicyInfo->retentionPolicy = &retpol[atoi(argv[7])];
	//printf("Step 7\n");

	    if ((req.transferParameters =
		 soap_malloc (&soap, sizeof(struct ns1__TTransferParameters))) == NULL) {
	      perror ("malloc4");
	      soap_end (&soap);
	      exit (1);
	    }
	    //req.transferParameters->__sizesizeArray = 0;
	    //req.transferParameters->sizeArray = NULL;
	    req.transferParameters->accessPattern = NULL;
	    req.transferParameters->connectionType = NULL;
	    req.transferParameters->arrayOfClientNetworks = NULL;
	    req.transferParameters->arrayOfTransferProtocols = NULL;
	//printf("Step 8-0\n");
	//printf("Step 8-00 nbproto: %d\n", nbproto);
	    if ((req.transferParameters->arrayOfTransferProtocols =
		 soap_malloc (&soap, nbproto*sizeof(struct ns1__ArrayOfString))) == NULL) {
	      perror ("malloc5");
	      soap_end (&soap);
	      exit (1);
	    }
	//printf("Step 8-1 nbproto: %d\n", nbproto);
	    req.transferParameters->arrayOfTransferProtocols->__sizestringArray = 1;
	//printf("Step 8-2\n");
	req.transferParameters->arrayOfTransferProtocols->stringArray = protocols;
	//printf("Step 8\n");

	/* To send the request ... */

#if 0
	if (soap_call_ns1__srmReserveSpace (&soap, argv[1], "SrmReserveSpace",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	srm_endpoint = argv[1];
	SOAP_CALL_NS1(ReserveSpace, re);
#endif

	reqstatp = rep.srmReserveSpaceResponse->returnStatus; 
	//repfs = rep.srmCheckPermissionResponse->arrayOfPermissions;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	repp = rep.srmReserveSpaceResponse;
	if (repp->spaceToken) {
		s_token = repp->spaceToken;
		printf ("soap_call_ns1__srmReserveSpace returned s_token: %s\n",
		    s_token);
	}
	/*
	if ( repp->retentionPolicy && repp->retentionPolicy->retentionPolicy )
	  printf ("srmReserveSpace provided actual_s_type: %s\n", s_retpol[*(repp->retentionPolicy->retentionPolicy)]);
	*/
	//printf ("srmReserveSpace provided actual_s_type: %s actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", s_retpol[*(repp->retentionPolicy->retentionPolicy)] /* (*repp->typeOfReservedSpace) */, repp->sizeOfTotalReservedSpace->value, repp->sizeOfGuaranteedReservedSpace->value, repp->lifetimeOfReservedSpace->value );
        printf ("srmReserveSpace provided actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", 
		//s_retpol[*(repp->retentionPolicy->retentionPolicy)] , 
		*(repp->sizeOfTotalReservedSpace), 
		*(repp->sizeOfGuaranteedReservedSpace), 
		*(repp->lifetimeOfReservedSpace) 
		);

	soap_end (&soap);
	exit (0);
}
