/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetPermission.c,v 1.2 2007/01/17 14:18:38 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

struct soap soap;
//static enum ns1__TPermissionMode permod[] = { NONE, X, W, WX, R, RX, RW, RWX, };
char* s_permod[] = { "NONE", "X", "W", "WX", "R", "RX", "RW", "RWX", };

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i, j;
	int isurl = 1; // pointer to the 1st actual SURL in the arg list
	int nbfiles;
	int nbproto = 0;
	static enum xsd__boolean trueoption = true_;  //  JPB
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
	int r = 0;
	char *r_token;
	struct ns1__srmGetPermissionResponse_ rep;
//////#define PRET 1
//////#ifdef PRET
	struct ns1__ArrayOfTPermissionReturn *repfs;
//////#endif
	struct ns1__srmGetPermissionRequest req;
//////#define INIT 1
//////#ifdef INIT
	//struct ns1__TSURLInfo;;;xsd__anyURI *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
//////#endif
	char *sfn;
	//struct soap soap;
	//struct ns1__srmStatusOfGetRequestResponse_ srep;
	//struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;
	int booll = 0;
	int boolR = 0;
        int nolv = 0;

	//static enum ns1__TPermissionMode permod[] = { NONE, X, W, WX, R, RX, RW, RWX, };

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURLs\n", argv[0]);
		exit (1);
	}

	/*
	if ( strchr(argv[1], '-' ) == argv[1] ) {
	  isurl = 2;
	  if ( strchr(argv[1], 'l' ) )
	    booll = 1 ;
          if ( strchr(argv[1], 'R' ) )
            boolR = 1 ;
	  if ( strchr(argv[1], '2' ) )
            nolv = 1 ;
	}
	*/

	nbfiles = argc - isurl;

	if (parsesurl (argv[isurl], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

//////#ifdef INIT
	memset (&req, 0, sizeof(req));
	if ((req.arrayOfSURLs =
	     soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
	    (req.arrayOfSURLs->urlArray =
	     soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	/*
	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfSURLs->surlInfoArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	*/
	req.arrayOfSURLs->__sizeurlArray = nbfiles;

	/*
	if ( booll ) 
	  req.fullDetailedList  = &trueoption;  //  GG
	if ( boolR ) 
	  req.allLevelRecursive = &trueoption;  //  GG new

        req.numOfLevels = &nolv;
	*/

	for (i = 0; i < nbfiles; i++) {
	  //reqfilep = req.arrayOfSURLs->surlInfoArray[i];
	  /*
		if ((req.arrayOfSURLs->surlInfoArray[i]->SURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	  */
		req.arrayOfSURLs->urlArray[i] = argv[i+isurl];
		//req.arrayOfSURLs->surlInfoArray[i]->storageSystemInfo = NULL;
	}

#if 0
	    if ((req.storageSystemInfo =
		 soap_malloc (&soap, sizeof(struct ns1__TExtraInfo))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    //req.storageSystemInfo->value = argv[3];
	    /*
	    req.storageSystemInfo->__sizeextraInfoArray = 1;
	    req.storageSystemInfo->extraInfoArray[0]->key = "0";
	    req.storageSystemInfo->extraInfoArray[0]->value = argv[3];
	    */
	    req.storageSystemInfo->__sizeextraInfoArray = 0;
	    req.storageSystemInfo->extraInfoArray = NULL;
	    //printf("Step 1\n");
#else
	    //req.storageSystemInfo = NULL;
#endif
//////#else
/*
	    soap_default_ns1__srmGetPermissionRequest (&soap, &req);
	    req.storageSystemInfo = NULL;
	req.arrayOfSURLs->__sizesurlInfoArray = nbfiles;
	if ( booll ) 
	  req.fullDetailedList  = &trueoption;  //  GG
	if ( boolR ) 
	  req.allLevelRecursive = &trueoption;  //  GG new

        req.numOfLevels = &nolv;
*/
//////#endif

	//printf("Step 8-0\n");

	if (soap_call_ns1__srmGetPermission (&soap, srm_endpoint, "GetPermission",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}

//////#ifdef PRET
	reqstatp = rep.srmGetPermissionResponse->returnStatus;
	/*
	if (rep.srmGetPermissionResponse->requestToken) {
		r_token = rep.srmGetPermissionResponse->requestToken;
		printf ("soap_call_ns1__srmGetPermission returned l_token %s\n",
		    r_token);
	}
	*/
	repfs = rep.srmGetPermissionResponse->arrayOfPermissionReturns;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREPARTIAL_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (&soap);
		exit (1);
		*/
	}
	
	if (! repfs) {
		printf ("arrayOfPermissionReturns is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request permissionArray %d\n", repfs->__sizepermissionArray);  //  GG
	for (i = 0; i < repfs->__sizepermissionArray; i++) {

	  printABranch(repfs, i);

	}
	soap_end (&soap); 
	exit (0);
//////#endif

}

//////#if 0
printABranch (struct ns1__ArrayOfTPermissionReturn *repfs, int i) {

  //struct ns1__arrayOfPermissionReturns *repfsl;
  int j, il, nsub;

  printf("Stat: %d, %s\n", repfs->permissionArray[i]->status->statusCode,
	 soap_ns1__TStatusCode2s (&soap, repfs->permissionArray[i]->status->statusCode)
	 );  //  GG

  /*
  if ( repfs->pathDetailArray[i]->arrayOfSubPaths )
    printf("Path: %s Subpath: %d\n", repfs->pathDetailArray[i]->surl, repfs->pathDetailArray[i]->arrayOfSubPaths->__sizepathDetailArray);
  else
  */
    printf("Path: %s\n", repfs->permissionArray[i]->surl);

  if ( repfs->permissionArray[i]->status->statusCode ) {
    printf("----=============\n");
    return; // continue;
  }
  //4printf("Size: %d\n", repfs->permissionArray[i]->size);
  //4printf("Type: %d\n", (*repfs->permissionArray[i]->type));
  //4if ( repfs->permissionArray[i]->fileStorageType )
  //4  printf("FStT: %d\n", (*repfs->permissionArray[i]->fileStorageType));
  if ( repfs->permissionArray[i]->owner )
    printf("OwnR: %s\n", repfs->permissionArray[i]->owner);
  //4if ( repfs->permissionArray[i]->checkSumType )
  //4  printf("ChST: %s\n", repfs->permissionArray[i]->checkSumType);
  //4if ( repfs->permissionArray[i]->createdAtTime )
  //4  printf("Crea: %s\n", repfs->permissionArray[i]->createdAtTime);
  //printf("Crea: %s\n", soap_dateTime2s (&soap, (time_t) (repfs->pathDetailArray[i]->createdAtTime->value)));
  //4if ( repfs->permissionArray[i]->lastModificationTime )
  //4  printf("Last: %s\n", repfs->permissionArray[i]->lastModificationTime);
  if ( repfs->permissionArray[i]->ownerPermission )
    printf("OwnP: %s\n", s_permod[*(repfs->permissionArray[i]->ownerPermission)]);
  //4if ( repfs->permissionArray[i]->clientPermission )
  //4  printf("CliP: %s\n", s_permod[*(repfs->permissionArray[i]->clientPermission)]);
  //printf("OwnP: %d\n", repfs->pathDetailArray[i]->ownerPermission->mode);
  if ( repfs->permissionArray[i]->otherPermission )
    printf("OthP: %s\n", s_permod[*(repfs->permissionArray[i]->otherPermission)]);
  // Warning: there should be a loop here if __size>1
  
  if ( repfs->permissionArray[i]->arrayOfUserPermissions ) {
    printf("UsrP: %d",
	   repfs->permissionArray[i]->arrayOfUserPermissions->__sizeuserPermissionArray);
    for (j = 0; j < repfs->permissionArray[i]->arrayOfUserPermissions->__sizeuserPermissionArray; j++ ) {
      printf(" %s:%s",
	     repfs->permissionArray[i]->arrayOfUserPermissions->userPermissionArray[j]->userID,
	     s_permod[repfs->permissionArray[i]->arrayOfUserPermissions->userPermissionArray[j]->mode]);
    }
    printf("\n");
  }

  if ( repfs->permissionArray[i]->arrayOfGroupPermissions ) {
    printf("GrpP: %d",
	   repfs->permissionArray[i]->arrayOfGroupPermissions->__sizegroupPermissionArray);
    for (j = 0; j < repfs->permissionArray[i]->arrayOfGroupPermissions->__sizegroupPermissionArray; j++ ) {
      printf(" %s:%s",
	     repfs->permissionArray[i]->arrayOfGroupPermissions->groupPermissionArray[j]->groupID,
	     s_permod[repfs->permissionArray[i]->arrayOfGroupPermissions->groupPermissionArray[j]->mode]);
    }
    printf("\n");
  }
  

  /*
  if ( repfs->permissionArray[i]->lifetimeAssigned )
    printf("LifA: %d\n", repfs->permissionArray[i]->lifetimeAssigned);
  if ( repfs->permissionArray[i]->lifetimeLeft )
    printf("LifL: %d\n", repfs->permissionArray[i]->lifetimeLeft);
  */

  /* FIXME: check if there are new fileds to be printed with 2.2 */

  printf("=================\n");

  /*
  //Loop on subpaths ...
  if ( ! repfs->pathDetailArray[i]->arrayOfSubPaths )
    return;
  repfsl = repfs->pathDetailArray[i]->arrayOfSubPaths;
  nsub = repfsl->__sizepathDetailArray;
  if ( ! nsub ) 
    return;
  for (il = 0; il < nsub; il++) {
    printf("================= Branch: %d\n", il);
    printABranch(repfsl, il);
  }
  */

  return;

}
//////#endif
