/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetRequestTokens.c,v 1.1 2006/12/19 20:05:25 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmGetRequestTokensResponse_ rep;
	struct ns1__srmGetRequestTokensRequest req;
	struct ns1__TReturnStatus *reqstatp;
	//struct ns1__ArrayOfTRequestTokens *repfs;
	struct ns1__ArrayOfTRequestTokenReturn *repfs;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	if (argc < 3) {
		fprintf (stderr, "usage: %s endPoint [user_request_description|-]\n", argv[0]);
		exit (1);
	}

#if 0
	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

	if ( strncmp(argv[2], "-", 1) ) 
	  req.userRequestDescription = argv[2];
	else
	  req.userRequestDescription = NULL;

	/* GG special tests */
	//GOODreq.userRequestDescription = NULL; // OUTPUTS EVERYTHING from the DB !

#if 0
	if (soap_call_ns1__srmGetRequestTokens (&soap, srm_endpoint, "GetRequestTokens",
#else
	if (soap_call_ns1__srmGetRequestTokens (&soap, argv[1], "GetRequestTokens",
#endif
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmGetRequestTokensResponse->returnStatus;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (&soap);
		exit (1);
		*/
	}

        repfs = rep.srmGetRequestTokensResponse->arrayOfRequestTokens;

        if (! repfs) {
                printf ("arrayOfRequestTokens is NULL\n");
                soap_end (&soap);
                exit (0);
        }
	printf ("request tokenArray %d\n", repfs->__sizetokenArray);
						
	printf("=== RETURNED REQUEST TOKENS ===\n");
        for (i = 0; i < repfs->__sizetokenArray; i++) 
                printf("%s createdAt: %s\n", 
		       repfs->tokenArray[i]->requestToken, 
		       repfs->tokenArray[i]->createdAtTime
		       /*
		       soap_dateTime2s (&soap, 
					(time_t*) (repfs->tokenArray[i]->createdAtTime->value)
					)
		       */
		       );
	
	soap_end (&soap);
	exit (0);
}
