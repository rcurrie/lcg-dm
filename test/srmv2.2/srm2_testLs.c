/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testLs.c,v 1.7 2007/07/19 08:06:24 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

struct soap soap;
//static enum ns1__TPermissionMode permod[] = { NONE, X, W, WX, R, RX, RW, RWX, };
char* s_permod[] = { "NONE", "X", "W", "WX", "R", "RX", "RW", "RWX", };

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i, j;
	int isurl = 1; // pointer to the 1st actual SURL in the arg list
	int nbfiles;
	int nbproto = 0;
	static enum xsd__boolean trueoption = true_;  //  JPB
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
	int r = 0;
	char *r_token;
	struct ns1__srmLsResponse_ rep;
#define PRET 1
#ifdef PRET
	struct ns1__ArrayOfTMetaDataPathDetail *repfs;
#endif
	struct ns1__srmLsRequest req;
#define INIT 1
#ifdef INIT
	//struct ns1__TSURLInfo;;;xsd__anyURI *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
#endif
	char *sfn;
	//struct soap soap;
	//struct ns1__srmStatusOfGetRequestResponse_ srep;
	//struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;
	int booll = 0;
	int boolR = 0;
        int nolv = -1;
	int boolV = 0;
	int boolP = 0;
	//int booll = 0;
	//int boolR = 0;
	int boolO = 0;  //  to provide pair offset/count

	//static enum ns1__TPermissionMode permod[] = { NONE, X, W, WX, R, RX, RW, RWX, };
	enum ns1__TStorageType {Volatile=0, Durable=1, Permanent=2};
	static enum ns1__TStorageType ftypes[] = {Volatile, Durable, Permanent};

	if (argc < 2) {
		fprintf (stderr, "usage: %s [-R|-l|-d|-2|-V|-P|-O] [offset count] SURLs\n", argv[0]);
		exit (1);
	}

	if ( strchr(argv[1], '-' ) == argv[1] ) {
	  isurl = 2;
	  if ( strchr(argv[1], 'l' ) )
	    booll = 1 ;
          if ( strchr(argv[1], 'R' ) )
            boolR = 1 ;
	  if ( strchr(argv[1], '2' ) )
            nolv = 1 ;
	  if ( strchr(argv[1], 'd' ) )
            nolv = 0 ;
          if ( strchr(argv[1], 'V' ) )
            boolV = 1 ;
          if ( strchr(argv[1], 'P' ) )
            boolP = 1 ;
          if ( strchr(argv[1], 'O' ) ) {
            boolO = 1 ;
	    isurl = 4;
	  }
	}

	nbfiles = argc - isurl;

	if (parsesurl (argv[isurl], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	// Special BestMan - LBL
	//1807flags |= CGSI_OPT_DELEG_FLAG;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

#ifdef INIT
	memset (&req, 0, sizeof(req));
	if ((req.arrayOfSURLs =
	     soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL ||
	    (req.arrayOfSURLs->urlArray =
	     soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL ) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	/*
	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfSURLs->surlInfoArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	*/
	req.arrayOfSURLs->__sizeurlArray = nbfiles;
	if ( booll ) 
	  req.fullDetailedList  = &trueoption;  //  GG
	if ( boolR ) 
	  req.allLevelRecursive = &trueoption;  //  GG new
	if ( boolV ) 
	  req.fileStorageType = &ftypes[0];  //  GG new2
	if ( boolP ) 
	  req.fileStorageType = &ftypes[2];  //  GG new2

	if ( nolv > -1 )
	  req.numOfLevels = &nolv;

	if ( boolO ) {
	if ((req.offset =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	if ((req.count =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	  *(req.offset) = atoi(argv[2]);
	  *(req.count)  = atoi(argv[3]);
	} else {
	  req.offset = NULL;
	  req.count  = NULL;
	}

	for (i = 0; i < nbfiles; i++) {
	  //reqfilep = req.arrayOfSURLs->urlArray[i];
		/*
		if ((reqfilep->SURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		*/
		req.arrayOfSURLs->urlArray[i] = argv[i+isurl];
		//req.arrayOfSURLs->urlArray[i]->storageSystemInfo = NULL;
	}

#if 0
	    if ((req.storageSystemInfo =
		 soap_malloc (&soap, sizeof(struct ns1__TExtraInfo))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    //req.storageSystemInfo->value = argv[3];
	    /*
	    req.storageSystemInfo->__sizeextraInfoArray = 1;
	    req.storageSystemInfo->extraInfoArray[0]->key = "0";
	    req.storageSystemInfo->extraInfoArray[0]->value = argv[3];
	    */
	    req.storageSystemInfo->__sizeextraInfoArray = 0;
	    req.storageSystemInfo->extraInfoArray = NULL;
	    //printf("Step 1\n");
#else
	    req.storageSystemInfo = NULL;
#endif
#else
	    soap_default_ns1__srmLsRequest (&soap, &req);
	    req.storageSystemInfo = NULL;
	req.arrayOfSURLs->__sizesurlInfoArray = nbfiles;
	if ( booll ) 
	  req.fullDetailedList  = &trueoption;  //  GG
	if ( boolR ) 
	  req.allLevelRecursive = &trueoption;  //  GG new

        req.numOfLevels = &nolv;
#endif

	//printf("Step 8-0\n");

#if 0
	if (soap_call_ns1__srmLs (&soap, srm_endpoint, "Ls",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(Ls, re);
#endif

#ifdef PRET
	reqstatp = rep.srmLsResponse->returnStatus;

	if (rep.srmLsResponse->requestToken) {
		r_token = rep.srmLsResponse->requestToken;
		printf ("soap_call_ns1__srmLs returned l_token %s\n",
		    r_token);
	}

	repfs = rep.srmLsResponse->details;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREPARTIAL_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (&soap);
		exit (1);
		*/
	}
	if (! repfs) {
		printf ("arrayOfTMetaDataPathDetail is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request pathDetailArray %d\n", repfs->__sizepathDetailArray);  //  GG
	for (i = 0; i < repfs->__sizepathDetailArray; i++) {

	  printABranch(repfs, i);

	}
	soap_end (&soap); 
	exit (0);
#endif

}

printABranch (struct ns1__ArrayOfTMetaDataPathDetail *repfs, int i) {

  struct ns1__ArrayOfTMetaDataPathDetail *repfsl;
  struct ns1__ArrayOfString *repfst;
  int j, il, it, nsub;

  //printf("Stat: %d\n", repfs->pathDetailArray[i]->status->statusCode);  //  GG
  printf("Stat: %d, %s\n", repfs->pathDetailArray[i]->status->statusCode,
	 soap_ns1__TStatusCode2s (&soap, repfs->pathDetailArray[i]->status->statusCode)
	 );  //  GG

  if ( repfs->pathDetailArray[i]->arrayOfSubPaths )
    //OLD2printf("Path: %s Subpath: %d\n", repfs->pathDetailArray[i]->surl, repfs->pathDetailArray[i]->arrayOfSubPaths->__sizepathDetailArray);
    printf("Path: %s Subpath: %d\n", repfs->pathDetailArray[i]->path, repfs->pathDetailArray[i]->arrayOfSubPaths->__sizepathDetailArray);
  else
    //OLD2printf("Path: %s\n", repfs->pathDetailArray[i]->surl);
    printf("Path: %s\n", repfs->pathDetailArray[i]->path);

  if ( repfs->pathDetailArray[i]->status->statusCode ) {
    printf("----=============\n");
    return; // continue;
  }
  printf("Size: %d\n", *(repfs->pathDetailArray[i]->size));
  if ( repfs->pathDetailArray[i]->type )
  printf("Type: %d, %s\n", (*repfs->pathDetailArray[i]->type),
	 soap_ns1__TFileType2s(&soap, (*repfs->pathDetailArray[i]->type))
	 );

  if ( repfs->pathDetailArray[i]->fileStorageType )
    printf("FStT: %d, %s\n", (*repfs->pathDetailArray[i]->fileStorageType),
	   soap_ns1__TFileStorageType2s(&soap, (*repfs->pathDetailArray[i]->fileStorageType))
	   );

  if ( repfs->pathDetailArray[i]->fileLocality )
    printf("FLoc: %d, %s\n", (*repfs->pathDetailArray[i]->fileLocality),
	   soap_ns1__TFileLocality2s(&soap, (*repfs->pathDetailArray[i]->fileLocality))
	   );

  if ( repfst = repfs->pathDetailArray[i]->arrayOfSpaceTokens ) {
    printf ("request tokenArray %d\n", repfst->__sizestringArray);
    printf("=== Space Tokens ===\n");
    for (it = 0; it < repfst->__sizestringArray; it++) 
      printf("s_token[%d]: %s\n", it, repfst->stringArray[it]);
  }

#if 0
  if ( repfs->pathDetailArray[i]->ownerPermission )
    printf("OwnR: %s\n", repfs->pathDetailArray[i]->ownerPermission);
  if ( repfs->pathDetailArray[i]->groupPermission )
    printf("GrpP: %s\n", repfs->pathDetailArray[i]->groupPermission);
#else
  if ( repfs->pathDetailArray[i]->ownerPermission )
    printf("OwnP: %s %s\n", 
	   repfs->pathDetailArray[i]->ownerPermission->userID,
	   s_permod[(repfs->pathDetailArray[i]->ownerPermission->mode)]);
  if ( repfs->pathDetailArray[i]->groupPermission )
    printf("GrpP: %s %s\n", 
	   repfs->pathDetailArray[i]->groupPermission->groupID,
	   s_permod[(repfs->pathDetailArray[i]->groupPermission->mode)]);
#endif

  if ( repfs->pathDetailArray[i]->otherPermission )
    printf("OthP: %s\n", s_permod[*(repfs->pathDetailArray[i]->otherPermission)]);
  if ( repfs->pathDetailArray[i]->checkSumType )
    printf("ChST: %s\n", repfs->pathDetailArray[i]->checkSumType);
  if ( repfs->pathDetailArray[i]->createdAtTime )
    printf("Crea: %s\n", repfs->pathDetailArray[i]->createdAtTime);
  //printf("Crea: %s\n", soap_dateTime2s (&soap, (time_t) (repfs->pathDetailArray[i]->createdAtTime->value)));
  if ( repfs->pathDetailArray[i]->lastModificationTime )
    printf("Last: %s\n", repfs->pathDetailArray[i]->lastModificationTime);

  //3if ( repfs->pathDetailArray[i]->ownerPermission )
  //3  printf("OwnP: %s\n", s_permod[*(repfs->pathDetailArray[i]->ownerPermission->mode)]);

  //7if ( repfs->pathDetailArray[i]->clientPermission )
  //7  printf("CliP: %s\n", s_permod[*(repfs->pathDetailArray[i]->clientPermission)]);
  //printf("OwnP: %d\n", repfs->pathDetailArray[i]->ownerPermission->mode);

  //3if ( repfs->pathDetailArray[i]->otherPermission )
  //3  printf("OthP: %s\n", s_permod[*(repfs->pathDetailArray[i]->otherPermission->mode)]);

  // Warning: there should be a loop here if __size>1
  /*
  if ( repfs->pathDetailArray[i]->arrayOfUserPermissions ) {
    printf("UsrP: %d",
	   repfs->pathDetailArray[i]->arrayOfUserPermissions->__sizeuserPermissionArray);
    for (j = 0; j < repfs->pathDetailArray[i]->arrayOfUserPermissions->__sizeuserPermissionArray; j++ ) {
      printf(" %s:%s",
	     repfs->pathDetailArray[i]->arrayOfUserPermissions->userPermissionArray[j]->userID->value,
	     s_permod[*(repfs->pathDetailArray[i]->arrayOfUserPermissions->userPermissionArray[j]->mode)]);
    }
    printf("\n");
  }

  if ( repfs->pathDetailArray[i]->arrayOfGroupPermissions ) {
    printf("GrpP: %d",
	   repfs->pathDetailArray[i]->arrayOfGroupPermissions->__sizegroupPermissionArray);
    for (j = 0; j < repfs->pathDetailArray[i]->arrayOfGroupPermissions->__sizegroupPermissionArray; j++ ) {
      printf(" %s:%s",
	     repfs->pathDetailArray[i]->arrayOfGroupPermissions->groupPermissionArray[j]->groupID->value,
	     s_permod[*(repfs->pathDetailArray[i]->arrayOfGroupPermissions->groupPermissionArray[j]->mode)]);
    }
    printf("\n");
  }
  */

  if ( repfs->pathDetailArray[i]->lifetimeAssigned )
    printf("LifA: %d\n", *(repfs->pathDetailArray[i]->lifetimeAssigned));
  if ( repfs->pathDetailArray[i]->lifetimeLeft )
    printf("LifL: %d\n", *(repfs->pathDetailArray[i]->lifetimeLeft));

  /* FIXME: check if there are new fields to be printed with 2.2 */

  printf("=================\n");

  //Loop on subpaths ...
  if ( ! repfs->pathDetailArray[i]->arrayOfSubPaths )
    return;
  repfsl = repfs->pathDetailArray[i]->arrayOfSubPaths;
  nsub = repfsl->__sizepathDetailArray;
  if ( ! nsub ) 
    return;
  for (il = 0; il < nsub; il++) {
    printf("================= Branch: %d\n", il);
    printABranch(repfsl, il);
  }

  return;

}

