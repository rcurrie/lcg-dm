#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the folder given as parameter, this script recursively lists the files
#  and their replicas, minimizing the number of simultaneous sessions.
#
#  It uses the lfc_opendirg, lfc_readdirg and lfc_getreplica methods.
#
#=============================================================================

import sys
import os
import lfc


#=============================================================================
#  Function readdirg_recurse
#=============================================================================
def readdirg_recurse(*args):
  
  if len(args) < 1:
    folder = ''
    prefix = ''
  else:
    folder = args[0]
    prefix = folder + '/'
  
  if (folder == '') or (folder[0] != '/'):
    if 'LFC_HOME' in os.environ:
      folder = os.environ['LFC_HOME'] + '/' + prefix
    else:
      sys.exit('Relative folder path requires LFC_HOME to be set and exported')
  
  #---------------------------------------------------------------------------
  # Open the folder
  #---------------------------------------------------------------------------
  dir = lfc.lfc_opendirg(folder, '')
  if dir == None:
    err_num    = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    sys.exit('Error ' + str(err_num) + ' on folder ' + folder + ': ' + err_string)
  
  files = []
  listp = lfc.lfc_list()
  
  #---------------------------------------------------------------------------
  # Loop on the entries of the folder to build the list of files
  #---------------------------------------------------------------------------
  while 1:
    entry = lfc.lfc_readdirg(dir)
    if entry == None:
      break
    files.append({'name': prefix + entry.d_name,
                  'mode': entry.filemode,
                  'guid': entry.guid})
  
  lfc.lfc_closedir(dir)
  
  #---------------------------------------------------------------------------
  # Loop on the current file list :  If the entry is a folder, recurse
  #---------------------------------------------------------------------------
  for myfile in files[:]:                            # Local copy is mandatory
    if myfile['mode'] & 040000:
      files.extend(readdirg_recurse(myfile['name']))
  
  return files


#=============================================================================
#  Function get_replicas
#=============================================================================
def get_replicas(files):
    
  #---------------------------------------------------------------------------
  # Loop on the list of files
  #---------------------------------------------------------------------------
  lfc.lfc_startsess('', '')
  
  for myfile in files:
    print myfile['name']
    
    #-------------------------------------------------------------------------
    # If the entry is a regular file, list its replicas using its GUID
    #-------------------------------------------------------------------------
    if not (myfile['mode'] & 060000):     # Exclude folders and symbolic links
      (res, replicas) = lfc.lfc_getreplica('', myfile['guid'], '')
      if res == 0:
        for replica in replicas:
          print '   ==>', replica.sfn
  
  lfc.lfc_endsess()


#=============================================================================
#  Main program
#=============================================================================
if __name__ == '__main__':
  get_replicas(readdirg_recurse(*sys.argv[1:]))
