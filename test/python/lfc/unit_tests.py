#!/usr/bin/python

import sys
import lfc
import commands
import os 


#
# test entry
#
directory = "/grid/dteam/my_test_dir/"
name = "/grid/dteam/my_test_dir/my.test"
replica1 = "sfn://my_se.in2p3.fr/hpss/in2p3.fr/group/sophie/tests_python/dir/my.test"
replica2 = "srm://my_other_se.cern.ch/castor/cern.ch/grid/sophie/tests_python/dir/my.test"
replica3 = "sfn://my_se.in2p3.fr/hpss/in2p3.fr/group/sophie/tests_python/dir/my.test2"
status = '-'
f_type = 'D'

#
# delete test entry, if exists
#
stat1 = lfc.lfc_filestatg()

if (lfc.lfc_statg(name,"",stat1)) == 0:
	if (lfc.lfc_delreplica(stat1.guid, None, replica1)) != 0:
		err_num = lfc.cvar.serrno
		err_string = lfc.sstrerror(err_num)
		print "error"+ str(err_num) + " (" + err_string + ")" 
	lfc.lfc_delreplica(stat1.guid, None, replica2)
	lfc.lfc_delreplica(stat1.guid, None, replica3)
	lfc.lfc_unlink(name)
	lfc.lfc_rmdir(directory)

#
# create entry in LFC for following tests
#
guid = commands.getoutput('uuidgen').split('\n')[0]
print guid

lfc.lfc_mkdir(directory, 0755)

if (lfc.lfc_creatg(name, guid, 0644)) != 0:
	err_num = lfc.cvar.serrno
	err_string = lfc.sstrerror(err_num)
	print "Error when creating " + name + ": Error " + str(err_num) + " (" + err_string + ")"
	#sys.exit(1)

#
# stat this entry in the LFC and print the GUID
#
stat3 = lfc.lfc_filestatg()

if (lfc.lfc_statg(name,"",stat3)) == 0:
	guid = stat3.guid
	print "The GUID for " + name + " is " + guid
else:
	err_num = lfc.cvar.serrno
	err_string = lfc.sstrerror(err_num)
	print  "There was an error while looking for " + name + ": Error " + str(err_num) + " (" + err_string + ")"
	#sys.exit(1)

#
# add replicas
#

lfc.lfc_addreplica(guid, None, os.getenv("LFC_HOST"), replica1, status, f_type, "", "")
lfc.lfc_addreplica(guid, None, os.getenv("LFC_HOST"), replica2, status, f_type, "", "")
lfc.lfc_addreplica(guid, None, os.getenv("LFC_HOST"), replica3, status, f_type, "", "")

#
# Using the lfc_getacl and lfc_setacl methods to add a user ACL
#
nentries, acls_list = lfc.lfc_getacl(name, lfc.CA_MAXACLENTRIES)

print "nentries = " + str(nentries)
print len(acls_list)

for i in acls_list:
	print i.a_type
	print i.a_id
	print i.a_perm

# When adding a first ACL for a given user, you also  need to add the mask
# When  adding  the  second user ACL, it is not necessary anymore

acl_user = lfc.lfc_acl()
acl_mask = lfc.lfc_acl()

acl_user.a_type=2        # 2 corresponds to CNS_ACL_USER
acl_user.a_id=18701      # user id
acl_user.a_perm=5

acl_mask.a_type=5        # 5 corresponds to CNS_ACL_MASK
acl_mask.a_id=0               # no user id specified
acl_mask.a_perm=5

acls_list.append(acl_user)
acls_list.append(acl_mask)

if (lfc.lfc_setacl(name, acls_list)) == 0:
	print "OK"
else:
	err_num = lfc.cvar.serrno
	err_string = lfc.sstrerror(err_num)
	print "Error " + str(err_num) + " (" + err_string + ")"
	#sys.exit(1)

#
# Using the lfc_getacl and lfc_setacl methods to remove a user ACL
#
nentries, acls_list = lfc.lfc_getacl(name, lfc.CA_MAXACLENTRIES)

# Note: you cannot remove the owner ACL (i.e. for CNS_ACL_USER_OBJ type) if ACLs for other users exist.
# ====== if all the other user ACLs are deleted, the owner ACL is automatically removed.

print "nentries 2 =" + str(nentries)

for i in acls_list:
	print i.a_type
	print i.a_id
	print i.a_perm

del acls_list[1]    # delete a given user ACL from the list of ACLs

if (lfc.lfc_setacl(name, acls_list)) == 0:
	print "OK"
else:
	err_num = lfc.cvar.serrno
	err_string = lfc.sstrerror(err_num)
	print "Error " + str(err_num) + " (" + err_string + ")"
	#sys.exit(1)


#
# list replicas, starting from the GUID
#
listp = lfc.lfc_list()
flag = lfc.CNS_LIST_BEGIN

print "Listing replicas for GUID " + guid

num_replicas=0

while(1):
	res = lfc.lfc_listreplica("",guid,flag,listp)
	flag = lfc.CNS_LIST_CONTINUE

	if res == None:
		break
	else:
		rep_name = res.sfn
		print "Replica: " + rep_name
		num_replicas = num_replicas + 1

lfc.lfc_listreplica("",guid,lfc.CNS_LIST_END,listp)
print "Found " + str(num_replicas) + " replica(s)"

#
# using the lfc_getreplica method
#

result, list = lfc.lfc_getreplica(name, "", "")
print "result = " + str(result)
print "len(list) = " + str(len(list)) + " replicas found"

if (result == 0):
	for i in list:
		print i.host
		print i.sfn

#
# using lfc_readdirxr method
#

dir = lfc.lfc_opendirg(directory,"")

while 1:
	read_pt = lfc.lfc_readdirxr(dir,"")
	if (read_pt == None) or (read_pt == 0):
		break
	entry,list=read_pt
	print entry.d_name
	try:
		for i in range(len(list)):
			print "   ==> %s" % list[i].sfn
	except TypeError, x:
		print "   ==> None"

lfc.lfc_closedir(dir)

#
# delete test entry
#
stat4 = lfc.lfc_filestatg()

if (lfc.lfc_statg(name,"",stat4)) == 0:
        lfc.lfc_delreplica(stat4.guid, None, replica1)
        lfc.lfc_delreplica(stat4.guid, None, replica2)
	lfc.lfc_delreplica(stat4.guid, None, replica3)
        lfc.lfc_unlink(name)
	lfc.lfc_rmdir(directory)

