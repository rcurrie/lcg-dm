#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the file given as first parameter, this script creates symbolic links
#  in the folder given as second parameter.  The number of symbolic links is
#  the third parameter.
#
#  It uses the lfc_symlink method.
#
#=============================================================================

import sys
import os
import lfc

if len(sys.argv) <= 3:
  sys.exit('Usage: ' + sys.argv[0] + '  source_path  folder_path  number_of_symlinks')

(source, folder, nb_symlinks) = (sys.argv[1], sys.argv[2], int(sys.argv[3]))
if nb_symlinks < 1:
  sys.exit(folder + ' :  0 symbolic links created')

nb_char = len(str(nb_symlinks-1))

if source[0] != '/':
  if 'LFC_HOME' in os.environ:
    source = os.environ['LFC_HOME'] + '/' + source
  else:
    sys.exit('Relative folder path requires LFC_HOME to be set and exported')

if folder[0] != '/':
  if 'LFC_HOME' in os.environ:
    folder = os.environ['LFC_HOME'] + '/' + folder
  else:
    sys.exit('Relative folder path requires LFC_HOME to be set and exported')

pos1 = source.rfind('/')
sourcename = source[pos1+1:]

pos2 = sourcename.rfind('.')
if pos2 > -1:
  targetname = folder + '/' + sourcename[:pos2]
  suffix     = sourcename[pos2:]
else:
  targetname = folder + '/' + sourcename
  suffix     = ''

if lfc.lfc_startsess('', '') != 0:
  err_num = lfc.cvar.serrno
  err_string = lfc.sstrerror(err_num)
  sys.exit('Error ' + str(err_num) + ' starting LFC session: ' + err_string)

for number in range(nb_symlinks):
  name = targetname + str(number).zfill(nb_char) + suffix
  res  = lfc.lfc_symlink(source, name)
  if res != 0:
    err_num = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    print 'Error ' + str(err_num) + ' while creating ' + name + ': ' + err_string
    number -= 1
    break

lfc.lfc_endsess()
print folder, ': ', (number+1), 'symbolic links created'
