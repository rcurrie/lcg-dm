#!/usr/bin/perl -w

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  This script uses the 'lfc-perf-symlinks.jdl.template' file to create a JDL,
#  then submits it to all computing elements in the same domain as the LFC
#  hosts listed in the file given as second parameter or else specified in the
#  'LFC_HOSTS_FILE' environment variable.
#
#  Optional first  parameter :  Number of steps to test  (by default 8)
#  Optional second parameter :  File containing the list of LFC hosts
#
#=============================================================================
use strict ;

#-----------------------------------------------------------------------------
#  Constants
#-----------------------------------------------------------------------------
my $jdlTemplate  = 'lfc-perf-symlinks.jdl.template' ;

my $vo           = $ENV{'LCG_GFAL_VO'}  or
                   die "Environment variable 'LCG_GFAL_VO' must be set\n" ;

my @linePatterns =
( 'Connecting\s+to\s+the\s+service\s+'.
      'https://[^:/]+:7443/glite_wms_wmproxy_server',
  '=+\s+glite-wms-job-submit\s+Success\s+=+',
  'The\s+job\s+has\s+been\s+successfully\s+submitted\s+to\s+the\s+WMProxy',
  'Your\s+job\s+identifier\s+is\s*:',
  '=*' ) ;

#-----------------------------------------------------------------------------
#  Variables to be substituded inside the template
#-----------------------------------------------------------------------------
my %vars ;
  
$vars{'NB_STEPS'}       = ( scalar(@ARGV) ?
                            shift @ARGV :
                            8 ) ;
  
$vars{'LFC_HOSTS_FILE'} = ( scalar(@ARGV) ?
                            shift @ARGV :
                            ( defined($ENV{'LFC_HOSTS_FILE'}) ?
                              $ENV{'LFC_HOSTS_FILE'} :
      die "Usage:  $0  [Nb_Steps  [file_containing_list_of_LFC_hosts]]\n",
          "        By default,  8  \$LFC_HOSTS_FILE\n" ) ) ;

print "\nLFC performance test with ".$vars{'NB_STEPS'}.' steps :  Max '.
      (4**($vars{'NB_STEPS'}-1))." symbolic links for '",
      $vars{'LFC_HOSTS_FILE'}, "'\n\n" ;

#-----------------------------------------------------------------------------
#  Read the template and substitute variables
#-----------------------------------------------------------------------------
my @jdlLines ;

open(TEMPLATE, $jdlTemplate)  or  die $! ;
while  ( <TEMPLATE> )
{
  if  ( ($_ ne "\n") and (substr($_, 0, 1) ne '#') )
  {
    s/\$([A-Za-z][0-9A-Za-z_]*)/defined($vars{$1}) ? $vars{$1} : '$'.$1/ge ;
    push(@jdlLines, $_) ;
  }
}
close(TEMPLATE) ;

#-----------------------------------------------------------------------------
#  Submission of the JDL to all computing elements in the same domain as the
#  LFC hosts listed in the LFC file.
#-----------------------------------------------------------------------------
my $jdl  =  $jdlTemplate ;
$jdl     =~ s/\.[^.]*$// ;

my @lfcTokens ;
my $country ;
my $maxToken ;
my @ceTokens ;
my $numToken ;
my @domainTokens ;
my $command ;

open(LFC_FILE, $vars{'LFC_HOSTS_FILE'})  or  die $! ;
while  ( <LFC_FILE> )
{
  if  ( ($_ eq "\n") or (substr($_, 0, 1) eq '#') )
      { next }
  #-----------------------------------------------------------------------------
  #  Calculation of the domain of the computing element
  #-----------------------------------------------------------------------------
  chomp ;
  @lfcTokens = reverse(split('\.', $_)) ;
  $country   = $lfcTokens[0] ;
  $maxToken  = 0 ;
  
  open(LCG_INFO, "lcg-info  --vo $vo  --list-ce  '--query=CE=*.$country:*'|")
      or die $! ;
  while  ( <LCG_INFO> )
  {
    if  ( m/^-? *CE *: *([^:]+):/ )
        {
          @ceTokens = reverse(split('\.', $1)) ;
          $numToken = 1 ;
          while ( ($numToken <= $#ceTokens) and ($numToken <= $#lfcTokens) and
                  ($ceTokens[$numToken] eq $lfcTokens[$numToken]) )
                { $numToken ++ }
          if  ( $numToken > $maxToken )
              { $maxToken = $numToken }
        }
  }
  close(LCG_INFO) ;
  
  @domainTokens = reverse(@lfcTokens[0..($maxToken-1)]) ;
  
  #-----------------------------------------------------------------------------
  #  Creation of the JDL file with the template
  #-----------------------------------------------------------------------------
  open(JDL, ">$jdl")  or  die $! ;
  print JDL @jdlLines ;
  print JDL 'Requirements      = RegExp("\\\\.', join('\\\\.', @domainTokens),
            ':",other.GlueCEUniqueID);', "\n" ;
  close(JDL) ;
  
  #-----------------------------------------------------------------------------
  #  Submission of the JDL file
  #-----------------------------------------------------------------------------
  $command = 'glite-wms-job-submit  -d '.$vo.'  '.$jdl ;
  print $command, '   (for ', join('.', @domainTokens), ")\n" ;
  open(GLITE_WMS_JOB_SUBMIT, "$command|")  or  die $! ;
  glite_wms_job_submit:
  while  ( <GLITE_WMS_JOB_SUBMIT> )
  {
    foreach  my $linePattern  ( @linePatterns )
    {
      if  ( m/^\s*$linePattern\s*$/ )
          { next glite_wms_job_submit }
    }
    s/^\s*-+\s*(https:)/-->  $1/ ;
    print ;
  }
  close(GLITE_WMS_JOB_SUBMIT) ;
  print "\n" ;
}
close(LFC_FILE) ;
