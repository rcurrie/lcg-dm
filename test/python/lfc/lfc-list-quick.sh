#!/bin/bash

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the LFC hosts listed in the file given as first parameter or else
#  specified in the LFC_HOSTS_FILE environment variable, this script quickly
#  lists the content of all folders at the second level.
#
#  It handles correcly paths containing blanks.
#
#=============================================================================

#-----------------------------------------------------------------------------
#  If the file containing the list of the LFC hosts is not given as first
#  parameter, it should be in the LFC_HOSTS_FILE environment variable.
#-----------------------------------------------------------------------------
if [[ "$1" == "" ]]
then
  if [[ "$LFC_HOSTS_FILE" == "" ]]
  then
    echo "Usage:  $0  [file_containing_list_of_LFC_hosts]"
    echo '        By default, $LFC_HOSTS_FILE'
    exit 1
  else
    echo "LFC_HOSTS_FILE=$LFC_HOSTS_FILE"
  fi
else
  LFC_HOSTS_FILE="$1"
fi

#-----------------------------------------------------------------------------
#  Extract the user's VO from his proxy
#-----------------------------------------------------------------------------
export LCG_GFAL_VO="$(voms-proxy-info -vo)"
if [[ "$LCG_GFAL_VO" == "" ]]
then
  echo "HOSTNAME=$HOSTNAME"  >> /dev/stderr
  exit 1
fi

#-----------------------------------------------------------------------------
#  Extract the user CN from his proxy
#-----------------------------------------------------------------------------
SUBMITTER="$(voms-proxy-info -subject)"
SUBMITTER="${SUBMITTER//CN=limited proxy/}"
SUBMITTER="${SUBMITTER//CN=proxy/}"
SUBMITTER="${SUBMITTER#*CN=}"
SUBMITTER="${SUBMITTER%%/*}"
SUBMITTER="${SUBMITTER// /_}"

export LFC_HOME="/grid/$LCG_GFAL_VO/$SUBMITTER"


#-----------------------------------------------------------------------------
#  Loop on the LFC hosts listed in the file
#-----------------------------------------------------------------------------
while read LFC_HOST
do
  if [[ ("$LFC_HOST" != "") && ("${LFC_HOST:0:1}" != "#") ]]
  then
    export LFC_HOST
    echo
    echo "LFC_HOST=$LFC_HOST"
    #-------------------------------------------------------------------------
    #  Folders at the first level
    #-------------------------------------------------------------------------
    lfc-ls  |  while read FOLDER
    do
      echo
      echo "    FOLDER=$FOLDER"
      #-----------------------------------------------------------------------
      #  Second level
      #-----------------------------------------------------------------------
      lfc-ls "$FOLDER"  |  while read FILE
      do
        echo "       $FILE"
      done
    done
    echo
  fi
done  < "$LFC_HOSTS_FILE"  ||  exit 1
