#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the folder given as parameter, this script recursively lists the files
#  and their replicas with optimized performance.
#
#  It uses the lfc_opendirg, lfc_readdirg and lfc_getreplicas methods.
#
#=============================================================================

import sys
import os
import lfc


#=============================================================================
#  Function readdirg_recurse
#=============================================================================
def readdirg_recurse(*args):
  
  if len(args) < 1:
    folder = ''
    prefix = ''
  else:
    folder = args[0]
    prefix = folder + '/'
  
  if (folder == '') or (folder[0] != '/'):
    if 'LFC_HOME' in os.environ:
      folder = os.environ['LFC_HOME'] + '/' + prefix
    else:
      sys.exit('Relative folder path requires LFC_HOME to be set and exported')
  
  #---------------------------------------------------------------------------
  # Open the folder
  #---------------------------------------------------------------------------
  dir = lfc.lfc_opendirg(folder, '')
  if dir == None:
    err_num    = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    sys.exit('Error ' + str(err_num) + ' on folder ' + folder + ': ' + err_string)
  
  files = []
  listp = lfc.lfc_list()
  
  #---------------------------------------------------------------------------
  # Loop on the entries of the folder to build the list of files
  #---------------------------------------------------------------------------
  while 1:
    entry = lfc.lfc_readdirg(dir)
    if entry == None:
      break
    files.append({'name': prefix + entry.d_name,
                  'mode': entry.filemode,
                  'guid': entry.guid})
  
  lfc.lfc_closedir(dir)
  
  #---------------------------------------------------------------------------
  # Loop on the current file list :  If the entry is a folder, recurse
  #---------------------------------------------------------------------------
  for myfile in files[:]:                            # Local copy is mandatory
    if myfile['mode'] & 040000:
      files.extend(readdirg_recurse(myfile['name']))
  
  return files


#=============================================================================
#  Function get_all_replicas
#=============================================================================
def get_all_replicas(files):
    
  #---------------------------------------------------------------------------
  # Build the list of GUID for all regular files
  #---------------------------------------------------------------------------
  guids = [ myfile['guid']  \
            for myfile in files  \
            if not (myfile['mode'] & 060000) ]
            # Exclude folders and symbolic links
  
  #---------------------------------------------------------------------------
  # Get all replicas at once and build dictionaries per GUID
  #---------------------------------------------------------------------------
  guidSizes    = {}
  guidReplicas = {}
  (res, rep_entries) = lfc.lfc_getreplicas(guids, '')
  if res != 0:
    print 'lfc_getreplicas :  Error ' + str(res) + "\n"
  else:
    for entry in rep_entries:
      if entry.errcode == 0:
        if entry.guid in guidReplicas:
          guidReplicas[entry.guid].append(entry.sfn) 
        else:
          guidSizes[entry.guid]    = entry.filesize
          guidReplicas[entry.guid] = [entry.sfn]
    
  #---------------------------------------------------------------------------
  # Loop on the list of files to print their Filemode, Name and Replicas
  #---------------------------------------------------------------------------
  for myfile in files:
    print myfile['name']
    guid = myfile['guid']
    if guid in guidReplicas:
      for replica in guidReplicas[guid]:
        if replica != '':
          print '   ==>', replica


#=============================================================================
#  Main program
#=============================================================================
if __name__ == '__main__':
  get_all_replicas(readdirg_recurse(*sys.argv[1:]))
