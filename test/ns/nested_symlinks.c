/*
 * Copyright (C) 1999-2004 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
	/* 
	 * Creates one file and recursively adds a specified number of
	 * symlinks per thread. Each thread then recursively reads the
	 * symlinks until the original file is reached. The time (in
	 * microseconds) for
	 * each thread to reach the file is printed to stdout as well
	 * as the total time for all threads to finish. 
	 *
	 * Usage: ./nested_symlinks.c -d [directory name] -n [nesting
	 * level of symlinks] -t [number of threads]
	 *
	 */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <uuid/uuid.h>
#include <sys/times.h>
#include <sys/time.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Cns_api.h"
#include "Cns.h"
#include "Cthread_api.h"
#include "serrno.h"
#define NLINKS 10
extern	char	*getenv();
extern	char	*optarg;

char Cnsdir[CA_MAXPATHLEN+1];
char filename[CA_MAXPATHLEN+1];

int nb_links = NLINKS;
int nb_threads = 1;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnshost[CA_MAXHOSTNAMELEN+1];
	char target[CA_MAXPATHLEN+1];
	char linkname[CA_MAXPATHLEN+1];
	char fnbuf[CA_MAXPATHLEN+1];
	time_t current_time;
	char dir[CA_MAXPATHLEN+1];
	void *doit(void *);
	char *dp;
	char *endp;
	int errflg = 0;
	int i,j;
	char *p;
	char pid4print[11];
	struct Cns_filestat statbuf;
	int *tid;
	struct tm *tm = NULL;
	int depth = 0;
    	char append[CA_MAXPATHLEN+1];
	char thread_dir[CA_MAXPATHLEN+1];
	char guid[CA_MAXGUIDLEN+1];
	uuid_t uuid;
	struct timeval utime;
	long start_time_us, end_time_us;
#if defined(_WIN32)
	WSADATA wsadata;
#endif
	/* get command line options */
	while ((c = getopt (argc, argv, "d:n:t:")) != EOF) {
		switch (c) {
		case 'd':
			strcpy(dir, optarg);
			break;
		case 'n':
			nb_links = strtol (optarg, &dp, 10);
			if (*dp != '\0') {
				fprintf (stderr, "invalid value for option -n\n", nb_links);
				errflg++;
			}
			if (nb_links > 999999){
			  fprintf(stderr, "Maximum number of links is 999999\n");
			  errflg++;
			}
			break;
		case 't':
			nb_threads = strtol (optarg, &dp, 10);
			if (*dp != '\0' || nb_threads <= 0) {
				fprintf (stderr, "invalid value for option -t\n");
				errflg++;
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif
	
	/* set up directory name according to command line options */
	sprintf (pid4print, "%d", getpid());
	if (dir) {
		if (*dir != '/') {
			if ((p = getenv ("CASTOR_HOME")) == NULL ||
			    strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				sprintf (Cnsdir, "%s/%s", p, dir);
		} else {
			if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				strcpy (Cnsdir, dir);
		}
	} else {
		gethostname (Cnshost, sizeof(Cnshost));
		if ((p = getenv ("CASTOR_HOME")) == NULL ||
		    strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
			fprintf (stderr, "cannot set dir name\n");
			errflg++;
		} else {
			(void) time (&current_time);
			tm = localtime (&current_time);
			sprintf (Cnsdir, "%s/Cnstest/%s/%d%02d%02d", p, Cnshost,
			    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday);
		}
	}
	if (errflg) {
		fprintf (stderr, "usage: %s %s\n", argv[0],
		    "[-d dir] [-t number_of_threads] [-n nesting_level]");
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}

	/* create the directory if not there already */

	if (Cns_stat (Cnsdir, &statbuf) < 0) {
		if (serrno == ENOENT) {
			endp = strrchr (Cnsdir, '/');
			p = endp;
			while (p > Cnsdir) {
				*p = '\0';
				c = Cns_access (Cnsdir, F_OK);
				if (c == 0) break;
				p = strrchr (Cnsdir, '/');
			}
			while (p <= endp) {
				*p = '/';
				c = Cns_mkdir (Cnsdir, 0777);
				if (c < 0 && serrno != EEXIST) {
					fprintf (stderr, "cannot create %s: %s\n",
					    Cnsdir, sstrerror(serrno));
					errflg++;
					break;
				}
				p += strlen (p);
			}
		} else {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
		}
	}

	/* add an extra directory for each thread */
	
	if (nb_threads > 1) {
	  for (i=0; i< nb_threads; i++) {
	    sprintf(thread_dir, "%s", Cnsdir);
	    sprintf(append, "/thread-%d", i);
	    strcat(thread_dir, append);
	    if (Cns_stat (thread_dir, &statbuf) <0) {
	      if (Cns_mkdir (thread_dir, 0777) < 0) {
		fprintf(stderr, "cannot create %s: %s\n",								thread_dir, sstrerror(serrno));
		errflg++;
		break;		
	      }			
	    }		
	  }
 	}

	if (tm == NULL) {
		(void) time (&current_time);
		tm = localtime (&current_time);
	}

	/* generate the LFN */
	sprintf (filename, "%02d%02d%02d_%d_%d",
		   tm->tm_hour, tm->tm_min, tm->tm_sec, getpid(), lrand48());
	sprintf (fnbuf, "%s/%s", Cnsdir, filename);
	uuid_generate(uuid);
	uuid_unparse(uuid, guid);
	if (Cns_creatg (fnbuf, guid, 0666) < 0) {
	  fprintf (stderr, "Error in creatg: %s: %s\n", fnbuf, sstrerror(serrno));
	}

	/* generate all the symlinks */
	for (j=0; j < nb_threads; j++) { 
	  sprintf(linkname, "%s_link", filename);
	  strcpy(dir, Cnsdir);
	  strcat(dir, "/");
	  if (nb_threads > 1) 
	    sprintf(thread_dir, "thread-%d/",j);
	  else sprintf(thread_dir, ""); 
	  strcat(dir,thread_dir);
	  strcpy(fnbuf, dir);
	  strcat(fnbuf, linkname);
	  p = fnbuf + strlen (fnbuf);
	  strcpy(target, filename);

	  for (i = 0; i < nb_links; i++) {
	    sprintf(p, "%d", i);
	    if (Cns_symlink (target, fnbuf) < 0) {
	      fprintf (stderr, "Error in symlink: %s: %s\n", fnbuf, sstrerror(serrno));
	      break;
	    }
	    strcpy(target, fnbuf);
	  }
	}

	/* set each thread going */
	gettimeofday( &utime, NULL);
	start_time_us = utime.tv_sec*1000000+utime.tv_usec;
	if (! errflg) {
		if ((tid = calloc (nb_threads, sizeof(int))) == NULL) {
			fprintf (stderr, "malloc error\n");
			errflg++;
		} else {
			for (i = 0; i < nb_threads; i++) {
			  if ((tid[i] = Cthread_create (&doit, &i)) < 0) {
			      fprintf (stderr, " error creating thread %d\n", i);
			      errflg++;
			    }
			}
			for (i = 0; i < nb_threads; i++) {
				(void)Cthread_join (tid[i], NULL);
			}
		}
	}
	gettimeofday( &utime, NULL);
	end_time_us = utime.tv_sec*1000000+utime.tv_usec;
#if defined(_WIN32)
	WSACleanup();
#endif
	if (errflg)
		exit (USERR);
	printf ("TOTAL: %d\n", end_time_us-start_time_us);
	exit (0);
}

void *
doit(arg)
void *arg;
{
  char thread_dir[CA_MAXPATHLEN+1];
  char dir[CA_MAXPATHLEN+1];
  char link_buf[CA_MAXPATHLEN+1];
  char target[CA_MAXPATHLEN+1];
  struct Cns_filestat stat;
  struct timeval utime;
  long start_time_us, end_time_us;

  strcpy(dir, Cnsdir);
  strcat(dir, "/");

  if (nb_threads > 1 ){
	  sprintf(thread_dir, "thread-%d/",*(int *)arg);
	  strcat(dir,thread_dir);
  }
  strcat(dir, filename);
  sprintf(target, "%s_link%d", dir, nb_links-1);

  gettimeofday( &utime, NULL);
  start_time_us = utime.tv_sec*1000000+utime.tv_usec;

  /* recursive read of symlinks until original file is found */
  while ( strcmp(link_buf, filename) != 0) {
    if (Cns_readlink(target, link_buf, CA_MAXPATHLEN+1) < 0) {
      fprintf(stderr, "Cannot readlink %s  : %s\n", target,
	      sstrerror(serrno));
    }
     sprintf(target, "%s", link_buf);
  }
  
  gettimeofday( &utime, NULL);
  end_time_us = utime.tv_sec*1000000+utime.tv_usec;

  printf ("%d \t", end_time_us-start_time_us);

  return (NULL);
}
