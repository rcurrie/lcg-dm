#!/usr/bin/perl


#
# Performance test : gets average insert times for 
# varying number of entries. 
#
use strict;
use warnings;
use Getopt::Long;

use FindBin;

# run the "create_files" command with the different number of files
# and get the times back in a file

my @files = (1, 10, 100, 1000, 10000, 100000);
my ($num_files, $result);

my $file = "results/insert/insert-times-inc-files.dat";
open(OUTFILE, '>', "$file") or die "Can't open $file: $!\n";

print OUTFILE "num_files, total insert time (ms), ave insert time(ms), mean insert time (ms), thread_ave (ms), thread_file_ave (ms) \n";

my($sec, $min, $hour, $day, $mon, $year, @rest) = localtime(time);
$year += 1900;
my $timestamp = "$year-$mon-$day-$hour:$min:$sec";
my @data;
my $num_threads = 1;

foreach $num_files (@files) {

	my $filename = "results/insert/insert-$num_files-files.dat";
	`./create_files -d /grid/dteam/caitriana/test2/insert/$timestamp-$num_files -f $num_files > $filename`;
  	open(INFILE, "$filename") or die "Can't open $filename: $!\n";
        my $run_time = 0;
        my $thread_time = 0;
        my $j = 0;
        while (<INFILE>) {
        	chomp;
                @data = split /\s+/, $_;
                if ($data[1] eq "TOTAL") {
                	$result = $data[2]/1000;
                }
               	elsif ($data[1] eq "THREAD") {
                	$thread_time += $data[2]/1000;
                }
               	else {
                 	$run_time += $data[1]/1000;
                        $j++;   
              	}
        }
        close INFILE;
	my $ave_run = $run_time / $j;
	my $average = $result / $num_files;
	my $ave_thread = $thread_time / $num_threads;  
	my $ave_file_perthread = $ave_thread / $num_files;
	$ave_run = (sprintf("%.2f",$ave_run));
	$average = (sprintf("%.2f",$average));
	$ave_thread = (sprintf("%.2f",$ave_thread));
	$ave_file_perthread = (sprintf("%.2f",$ave_file_perthread));
	print OUTFILE "$num_files \t $result \t $average \t $ave_run \t $ave_thread \t $ave_file_perthread\n";

  # delete the files that have just been produced, before testing with a different number of files
  `nsrm -rf /grid/dteam/caitriana/test2/insert/`;
}

close OUTFILE;
