#!/usr/bin/perl

#
# Performance test : creates then deletes files for different number
# of files and threads. A fixed number
# of files (currently set to 5000) are created/deleted and the average delete
# time calculated. This is repeated 5 times for each reading.
#

use strict;
use warnings;
use Getopt::Long;

use FindBin;

# First do it using absolute pathnames
my $outfile = "results/delete/delete-5000-abs.dat";
get_results($outfile, "no", 0);

# then with relative pathnames
$outfile = "results/delete/delete-5000-rel.dat";
get_results($outfile, "yes", 0);

# the subroutine for running the command with the given parameters.

sub get_results {

  my($file, $relative, $depth) = @_;
  
  my $optargs = "";
  if ($relative eq "yes") {
    $optargs = $optargs."-r ";
  }
  
  # run the "delete_files" command with the different number of files and threads
  # and get the times back in a file
  
  my @threads = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 50);
  my ($num_threads, $result);
  
  open(OUTFILE, '>', "$file") or die "Can't open $file: $!\n";
  
  print OUTFILE "num_threads, per_thread, total_time (ms), average_time(ms), ave_run_time (ms), ave_thread_time (ms), ave_ins_time_perthread \n";
  
  my($sec, $min, $hour, $day, $mon, $year, @rest) = localtime(time);
  $year += 1900;
  my $timestamp = "$year-$mon-$day-$hour:$min:$sec";
  my $i = 0;
  my $total_files = 5000;
  my @data;
  
  foreach $num_threads (@threads) {
    $i = 0;
    my $per_thread = $total_files/$num_threads;
    while ($i < 5) {
      my $filename =  "results/delete/$num_threads-thread-5000-deletes-$i.dat";
      `./delete_files -d /grid/dteam/caitriana/test2/delete/$timestamp-$num_threads -f $per_thread -t $num_threads $optargs > $filename`;
      open(INFILE, "$filename") or die "Can't open $filename: $!\n";
      my $run_time = 0;
      my $thread_time = 0;
      my $j = 0;
      while (<INFILE>) {
	chomp;
	@data = split /\s+/, $_;
	if ($data[1] eq "TOTAL") {
	  $result = $data[2]/1000;
	}
	elsif ($data[1] eq "THREAD") {
	  $thread_time += $data[2]/1000;
	}
	else {
	  $run_time += $data[1]/1000;
	  $j++;
	}
      }
      close INFILE;
      my $ave_run = $run_time / $j;
      my $ave_thread = $thread_time / $num_threads;
      my $ave_total = $result / $total_files;
      my $ave_file_perthread = $ave_thread / $per_thread;
      $ave_run = (sprintf("%.2f",$ave_run));
      $ave_thread = (sprintf("%.2f",$ave_thread));
      $ave_total = (sprintf("%.2f",$ave_total));
      $ave_file_perthread = (sprintf("%.2f",$ave_file_perthread));
      print OUTFILE "$num_threads \t $per_thread \t $result \t $ave_total \t $ave_run \t $ave_thread \t $ave_file_perthread\n";
      `nsrm -rf /grid/dteam/caitriana/test2/delete/$timestamp-$num_threads`;
      $i+=1;
    }
    # delete the files that have just been produced, before testing with a different number of threads
    `nsrm -rf /grid/dteam/caitriana/test2/delete/`;
  }
  close OUTFILE;
}
