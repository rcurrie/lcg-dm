/*
 * Copyright (C) 1999-2004 by CERN/IT/PDP/DM
 * All rights reserved
 */

	/* create a given number of files, make a given number of
	 * queries for one guid, and calculate the query rate. Default
	 * creates 10 files; call with -f 0 to not create any new files.
	 */

	/* If dir is specified on the command line but does not start with
	 * a slash, it is prefixed by $CASTOR_HOME..
	 * Cnshost is set to the value of the environment variable CNS_HOST.
	 * If not set, the value is taken from shift.conf.
	 * If not set there either, use localhost.
	 * Command syntax is:
	 *	query_rate [-d dir] [-f num_files] [-t num_threads]
	 *	[-q num_queries] [-x use transactions] [-c num queries
	 *	per transaction]
	 */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <uuid/uuid.h>
#include <sys/times.h>
#include <sys/time.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Cns.h"
#include "Cns_api.h"
#include "Cthread_api.h"
#include "serrno.h"
#define NFILES 10
extern	char	*getenv();
extern	char	*optarg;
char Cnsdir[CA_MAXPATHLEN+1];
int nb_queries = 0;
int total_queries = 0;
int transactions = 0;
int commit_interval = 1;
int thread_num = 0;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnshost[CA_MAXHOSTNAMELEN+1];
	char dir[CA_MAXPATHLEN+1];
	void *doit(void *);
	char *dp;
	char *endp;
	int errflg = 0;
	int i,j;
	int dir_flag = 0;
	char *p;
	char pid4print[11];
	struct Cns_filestat statbuf;
	struct Cns_filestatg statg;
	int *tid;
	struct tm *tm = NULL;
	char filename[CA_MAXPATHLEN+1];
	char append[CA_MAXPATHLEN+1];
	int nb_files = NFILES;
	int nb_threads = 1;
	char guid[CA_MAXGUIDLEN+1];
	uuid_t uuid;
	time_t current_time;
	struct timeval utime;
	long start_time_us, end_time_us;

#if defined(_WIN32)
	WSADATA wsadata;
#endif

	/* get command-line options */
	while ((c = getopt (argc, argv, "d:f:t:q:c:x")) != EOF) {
		switch (c) {
		case 'd':
			strcpy(dir, optarg);
			break;
		case 'f':
			nb_files = strtol (optarg, &dp, 10);
			if (*dp != '\0') {
			  fprintf (stderr, "invalid value for option -f\n");
			  errflg++;
			}
			break;
		case 't':
		  nb_threads = strtol (optarg, &dp, 10);
		  if (*dp != '\0' || nb_threads <= 0) {
		    fprintf (stderr, "invalid value for option -t\n");
		    errflg++;
		  }
		  break;
		case 'q':
		  total_queries =  strtol (optarg, &dp, 10);
		  if (*dp != '\0' || total_queries <= 0) {
		    fprintf (stderr, "invalid value for option -q\n");
		    errflg++;
		  }
		  break;
		case 'x':
		        transactions = 1;
			break;
		case 'c':
		        commit_interval = strtol (optarg, &dp, 10);
			if (*dp != '\0' || commit_interval <= 0) {
				fprintf (stderr, "invalid value for option -c\n");
				errflg++;
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;  
		}
	}

#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif

	/* set up directory name according to command line options */
	sprintf (pid4print, "%d", getpid());
	if (dir) {
		if (*dir != '/') {
			if ((p = getenv ("CASTOR_HOME")) == NULL ||
			    strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				sprintf (Cnsdir, "%s/%s", p, dir);
		} else {
			if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				strcpy (Cnsdir, dir);
		}
	} else {
		gethostname (Cnshost, sizeof(Cnshost));
		if ((p = getenv ("CASTOR_HOME")) == NULL ||
		    strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
			fprintf (stderr, "cannot set dir name\n");
			errflg++;
		} else {
			(void) time (&current_time);
			tm = localtime (&current_time);
			sprintf (Cnsdir, "%s/Cnstest/%s/%d%02d%02d", p, Cnshost,
			    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday);
		}
	}

	if (errflg) {
		fprintf (stderr, "usage: %s %s %s\n", argv[0],
		    "[-d dir] [-f num_files] [-t num_threads]");

#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}
	
	/* create the directory if not there already */
	if (Cns_stat (Cnsdir, &statbuf) < 0) {
		if (serrno == ENOENT) {
			endp = strrchr (Cnsdir, '/');
			p = endp;
			while (p > Cnsdir) {
				*p = '\0';
				c = Cns_access (Cnsdir, F_OK);
				if (c == 0) break;
				p = strrchr (Cnsdir, '/');
			}
			while (p <= endp) {
				*p = '/';
				c = Cns_mkdir (Cnsdir, 0777);
				if (c < 0 && serrno != EEXIST) {
					fprintf (stderr, "cannot create %s: %s\n",
					    Cnsdir, sstrerror(serrno));
					errflg++;
					break;
				}
				dir_flag = 1;
				p += strlen (p);
			}
		} else {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
		}
	}

	(void) time (&current_time);
	tm = localtime (&current_time);

	sprintf (filename, "%s/%02d%02d%02d_%d_",
		 Cnsdir, tm->tm_hour, tm->tm_min, tm->tm_sec, getpid());
	p = filename + strlen (filename);

	/* create the files */
	for (i = 0; i < nb_files; i++) {
	  uuid_generate(uuid);
	  uuid_unparse(uuid, guid);
	  sprintf (p, "%d", lrand48());
	  if (Cns_creatg (filename, guid, 0666) < 0) {
	    fprintf (stderr, "%s: %s\n", filename, sstrerror(serrno));
	    break;
	  }
	}

	/* get all the threads going */
	gettimeofday( &utime, NULL);
        start_time_us = utime.tv_sec*1000000+utime.tv_usec;
	if (! errflg) {
	  if ((tid = calloc (nb_threads, sizeof(int))) == NULL) {
	    fprintf (stderr, "malloc error\n");
	    errflg++;
	  } else {
	    for (i = 0; i < nb_threads; i++) {
	      if ((tid[i] = Cthread_create (&doit, &i)) < 0) {
		fprintf (stderr, " error creating thread %d\n", i);
		errflg++;
	      }
	    }
	    for (i = 0; i < nb_threads; i++) {
	      (void)Cthread_join (tid[i], NULL);
	    }
	  }
	}
	gettimeofday( &utime, NULL);
        end_time_us = utime.tv_sec*1000000+utime.tv_usec;

#if defined(_WIN32)
	WSACleanup();
#endif
	if (errflg)
		exit (USERR);

	printf ("%8.4f\n", 1000000.*nb_queries/(end_time_us - start_time_us));
	/*	printf(" %lu \t %lu \t %d\n", start_time_us, end_time_us, nb_queries); */
	exit (0);
}


void *
doit(arg)
void *arg;
{
  char filename[CA_MAXPATHLEN+1];
  char guid[CA_MAXGUIDLEN+1];
  struct Cns_filestatg statg;
  int thread_queries = 0;
  uuid_t uuid;
  int j;
  int i = *(int *)arg;

  /* put in the guid we are going to query for */
  uuid_generate(uuid);
  uuid_unparse(uuid, guid);
  sprintf(filename, "%s/lfn-%d", Cnsdir, lrand48());
  if (Cns_creatg(filename, guid, 0666) <0) {
    fprintf (stderr, "%s: %s\n", guid, sstrerror(serrno));
  }
  
  /* query for this guid until the specified number of queries is
     reached */
  /* with > 1 thread this will actually query slightly more than the
     specified number, but as it is a calculation of rate this is ok */ 
  while (thread_queries < total_queries) {
    if(transactions) Cns_starttrans (NULL, "query_rate");
    for (j = 0; j < commit_interval && j < total_queries; j++) {
      if (Cns_statg(filename, guid, &statg) < 0) {
	fprintf (stderr, "%s: %s\n", guid, sstrerror(serrno));
	if (transactions) Cns_aborttrans();
      }
      thread_queries++;
    }
    if (transactions) Cns_endtrans();
  }

  nb_queries+=thread_queries;

  if (Cns_unlink (filename) < 0) {
    fprintf (stderr, "%s: %s\n", filename, sstrerror(serrno));
  }
  
  return (NULL);
}
