#!/usr/bin/perl


#
# Performance test : time to read all files in a directory for increasing
# number of files.
#

use strict;
use warnings;
use Getopt::Long;

use FindBin;

# First do it using absolute pathnames
my $outfile = "results/READ-ABS-NONEST.dat";
get_results($outfile, "no", 0);

# then with relative pathnames
$outfile = "results/READ-REL-NONEST.dat";
get_results($outfile, "yes", 0);

# the subroutine for running the command with the given parameters.

sub get_results {

my($file, $relative, $depth) = @_;

my $optargs = "";
if ($relative eq "yes") {
  $optargs = $optargs."-r ";
}
if ($depth > 0) {
  $optargs = $optargs."-n $depth ";
}

# run the "read_files" command with the different number of files and threads
# and get the times back in a file

my @threads = (1, 2, 3, 4, 5, 6, 7, 8, 9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 50);
my ($num_threads, $result);

open(OUTFILE, '>', "$file") or die "Can't open $file: $!\n";

print OUTFILE "num_threads, num_files, total_time (ms), per_thread (ms), average_per_thread(ms)\n";

my($sec, $min, $hour, $day, $mon, $year, @rest) = localtime(time);
$year += 1900;
my $timestamp = "$year-$mon-$day-$hour:$min:$sec";
my $num_files = 1;
my $i = 0;
my $max_files = 10000;
my $increment = 1;
foreach $num_threads (@threads) {
      $num_files = 1;
      $i = 0;
      $increment = 1;	
      while ($num_files <= $max_files) {
	if ($i == 9) {
		$increment*=10;
		$i = 0;
	}	
	$result = `./read_files -d /grid/dteam/caitriana/test2/read/$timestamp-$num_threads -f $num_files -t $num_threads $optargs`;
	chop $result;
	my $per_thread = $result/$num_threads;
	my $average = $per_thread/$num_files;
	print OUTFILE "$num_threads \t $num_files \t $result \t $per_thread \t $average\n";
	`nsrm -rf /grid/dteam/caitriana/test2/read/$timestamp-$num_threads`;
	$i+=1;
	$num_files+=$increment;
	}
	`nsrm -rf /grid/dteam/caitriana/test2/read/`;
}

close OUTFILE;

}
