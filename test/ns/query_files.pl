#!/usr/bin/perl


#
# Performance test : time to query for a guid for increasing
# number of files in the LFC, for different numbers of threads.
#

use strict;
use warnings;
use Getopt::Long;

use FindBin;

my $outfile = "results/query/query-times-inc-files.dat";
get_results($outfile);

# the subroutine for running the command with the given parameters.

sub get_results {

my($file) = @_;

# run the "query_files" command with the different number of files and threads
# and get the times back in a file

my @threads = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 50);
my ($num_threads, $result);

open(OUTFILE, '>', "$file") or die "Can't open $file: $!\n";

print OUTFILE "num_threads, num_files, total_thread_time (ms), thread_average (ms) \n";

my($sec, $min, $hour, $day, $mon, $year, @rest) = localtime(time);
$year += 1900;
my $timestamp = "$year-$mon-$day-$hour:$min:$sec";
my $num_files = 1;
my $i = 0;
my $j;
my $max_files = 100000000;
my $increment = 1;
$result = 0;

while ($num_files <= $max_files) {

  if ($i == 9) {
    $increment*=10;
    $i = 0;
  }	
  
  `./create_files -d /grid/dteam/caitriana/test/$timestamp-$num_files -f $increment`;
  foreach $num_threads (@threads) {
    $result = 0;
    my $filename = "results/query/$num_threads-thread-query-$num_files-files.dat";
    `./query_files -d /grid/dteam/caitriana/test2/query/$timestamp-$num_threads -f 1 -t $num_threads > $filename`;
    open(INFILE, "$filename") or die "Can't open $filename: $!\n";
    while(<INFILE>) {
      chomp;
      my @data = split /\s+/, $_;	
      for($j=0; $j<$num_threads; $j++) {
	$result += $data[$j]/1000;
      }
    }
    close INFILE;
    my $average = $result/$num_threads;
    print OUTFILE "$num_threads \t $num_files \t $result \t $average\n";
    `nsrm -rf /grid/dteam/caitriana/test2/query/$timestamp-$num_threads`;
  }
  $i+=1;
  $num_files+=$increment;
}

close OUTFILE;

}
