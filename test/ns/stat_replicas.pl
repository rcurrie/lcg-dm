#!/usr/bin/perl


#
# Performance test : time to list all replicas of a file for increasing
# number of replicas.
#

use strict;
use warnings;
use Getopt::Long;

use FindBin;

my $outfile = "results/REP-ABS-NONEST.dat";
get_results($outfile);

# the subroutine for running the command with the given parameters.

sub get_results {

my($file) = @_;

# run the "stat_replicas" command with the different number of files and threads
# and get the times back in a file

my @threads = (1, 2, 5, 10, 20, 50);
my ($num_threads, $result);

open(OUTFILE, '>', "$file") or die "Can't open $file: $!\n";

print OUTFILE "num_threads, num_replicas, total_time (ms), thread_average (ms), replica_average(ms)\n";

my($sec, $min, $hour, $day, $mon, $year, @rest) = localtime(time);
$year += 1900;
my $timestamp = "$year-$mon-$day-$hour:$min:$sec";
my $num_replicas = 1;
my $i = 0;
my $max_replicas = 10000;
my $increment = 1;
foreach $num_threads (@threads) {
      $num_replicas = 1;
      $i = 0;
      $increment = 1;	
      while ($num_replicas <= $max_replicas) {
	if ($i == 9) {
		$increment*=10;
		$i = 0;
	}	
	my $j = 0;
        my $total = 0;
        $result = 0;
	my $filename = "results/$num_threads-THREAD-$num_replicas-REPLICAS.dat";
	`./stat_replicas -d /grid/dteam/caitriana/test2/replication/$timestamp-$num_threads -r $num_replicas -t $num_threads > $filename`;
	open(INFILE, "$filename") or die "Can't open $filename: $!\n";
        while (<INFILE>) {
                chomp;
                my @data = split /\s+/, $_;
                for($j=0; $j<$num_threads; $j++) {
                        $result += $data[$j]/1000;
                }
                $total = $data[$j+1]/1000;
        }
	close INFILE;
	my $per_thread = $result/$num_threads;
	my $average = $per_thread/$num_replicas;
	print OUTFILE "$num_threads \t $num_replicas \t $result \t $per_thread \t $average\n";
	`nsrm -rf /grid/dteam/caitriana/test2/replication/$timestamp-$num_threads`;
	$i+=1;
	$num_replicas+=$increment;
	}
	`nsrm -rf /grid/dteam/caitriana/test2/replication/`;
}

close OUTFILE;

}
