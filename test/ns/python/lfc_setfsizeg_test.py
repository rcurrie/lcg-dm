import os, lfc, sys, commands
from testClass import _test

global testHome 

class test_setfsize_ok(_test):
    def info(self):
        return "Set file size on existing file guid"
    def prepare(self):
        self.name = "/grid/dteam/python_setfsize_test"
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        ret = lfc.lfc_creatg(self.name, self.guid, 0664)
    def clean(self):
        lfc.lfc_unlink(self.name)
    def test(self):
        self.size=123456789L
        csumtype=""
        csumvalue=""
        ret=lfc.lfc_setfsizeg(self.guid,self.size, csumtype, csumvalue)
        stat=lfc.lfc_filestat()
        lfc.lfc_stat(self.name, stat)
        return (stat,ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.filesize=self.size
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.filesize == ret.filesize )
        else:
            retval = False
        return retval


class test_nonexisting_guid(_test):
    def __init__(self):
        self.retVal = -1
    def info(self):
	return "Set filesize on nonexisting guid: "
    def test(self):
        self.size=987654321L
        csumtype=""
        csumvalue=""
        self.guid="-----------------"
        ret=lfc.lfc_setfsizeg(self.guid,self.size,csumtype,csumvalue)
        stat=lfc.lfc_filestatg()
        lfc.lfc_statg("",self.guid, stat)
        return (stat,ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.filesize=0L
        return retval

    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.filesize == ret.filesize )
        else:
            retval = False
        return retval


class lfc_setfsizeg_test:
    def __init__(self):
	self.name = "lfc_setfsize_test"
        self.tests=[test_setfsize_ok,test_nonexisting_guid]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_setfsizeg_test().run()
