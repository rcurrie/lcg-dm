import os,lfc, commands
from testClass import _test 

class test_create_dir_ok(_test):
    def info(self):
        return "Directory creation"
    def test(self):
        self.name = "/grid/dteam/python_mkdir_test"
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        ret = lfc.lfc_mkdirg(self.name, self.guid, 0755)
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg("",self.guid, statg)
        return (statg,ret)
    def ret(self):
        statg=lfc.lfc_filestatg()
        statg.guid = self.guid
        return statg
    def compare(self,testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal != testRetVal):
            retval = False
        retval = retval & (ret.guid == test.guid)
        return retval
    def clean(self):
        lfc.lfc_rmdir(self.name)



class test_create_dir_pd(_test):
    def __init__(self):
        self.retVal=-1
    def info(self):
        return "Directory creation (permission denied)"
    def test(self):
        self.name = "/python_mkdir_test"
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        ret = lfc.lfc_mkdirg(self.name, self.guid, 0755)
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg("",self.guid, statg)
        return (statg,ret)
    def ret(self):
        statg=lfc.lfc_filestatg()
        statg.guid = ""
        return statg
    def clean(self):
        lfc.lfc_rmdir(self.name)
    def compare(self,testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal != testRetVal):
            retval = False
        retval = retval & (ret.guid == test.guid)
        return retval

class lfc_mkdirg_test:
    def __init__(self):
        self.name = "lfc_mkdirg_test"
        self.tests=[test_create_dir_ok, test_create_dir_pd]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'

lfc_mkdirg_test().run()
