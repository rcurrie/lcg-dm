import os, lfc, sys, commands, errno
from testClass import _test, _ntest

global testHome 

class test_ok(_test):
    def info(self):
	return "lfc_chdir OK "
    def prepare(self):
        self.path = "/grid/dteam"
    def test(self):
        ret = lfc.lfc_chdir(self.path)
        path = "                     "
        lfc.lfc_getcwd(path,len(path))
        return (path.strip(),ret)
    def ret(self):
        return self.path+"\0"
    def compare(self, testVal, retVal):
        (ret1, retRetVal) = retVal
        (test1, testRetVal) = testVal
        if ((ret1 == test1) & (retRetVal == testRetVal)):
            retval = True
        else:
            retval = False
        return retval

class test_ENOENT(_ntest):
    def info(self):
        return "lfc_chdir to non-existing directory (ENOENT)"
    def test(self):
        ret = lfc.lfc_chdir("/for_sure_non_existing_directory")
        return ((None,lfc.cvar.serrno), ret)
    def ret(self):
        return (None,errno.ENOENT)
    def compare(self, testVal, retVal):
        ((ret1, reterr), retRetVal) = retVal
        ((test1, testerr), testRetVal) = testVal
        if ((ret1 == test1) & (reterr == testerr)):
            retval = True
        else:
            retval = False
        return retval

class test_ENAMETOOLONG(_ntest):
    def info(self):
        return "The length of path exceeds CA_MAXPATHLEN (ENAMETOOLONG): "
    def test(self):
        path = ""
        for i in xrange(0,1023+1):
            path=path + "a"
        ret=lfc.lfc_chdir(path)
        return ("", lfc.cvar.serrno, ret)
    def ret(self):
        return ("", errno.ENAMETOOLONG)

    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class lfc_getcwd_test:
    def __init__(self):
	self.name = "lfc_getcwd_test"
        self.tests=[test_ok, test_ENOENT, test_ENAMETOOLONG]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_getcwd_test().run()
