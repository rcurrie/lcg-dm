import os,lfc

class lfc_stat_test:
    def __init__(self):
	self.name = "lfc_stat_test"
    def test(self):
        stat=lfc.lfc_filestat()
        statPtr=lfc.lfc_filestatPtr(stat)
        ret=lfc.lfc_stat("/",statPtr)
        return stat,ret

    def ret(self):
        retval=lfc.lfc_filestat()
        retval.fileid=2L
        retval.filemode=16877
        retval.nlink=1
        retval.uid=0
        retval.gid=0
        retval.filesize=0L
        retval.atime=1184059742
        retval.mtime=1171381061
        retval.ctime=1171381061
        retval.fileclass=0
        retval.status='-'
        return retval

    def ntest(self):
        stat=lfc.lfc_filestat()
        statPtr=lfc.lfc_filestatPtr(stat)
        ret=lfc.lfc_stat("/nonexisting",statPtr)
        return stat,ret

    def nret(self):
        retval=lfc.lfc_filestat()
        retval.fileid=0L
        retval.filemode=0
        retval.nlink=0
        retval.uid=0
        retval.gid=0
        retval.filesize=0L
        retval.atime=0
        retval.mtime=0
        retval.ctime=0
        retval.fileclass=0
        retval.status=' '
        return retval

    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
        else:
            retval = False
        return retval

    def run(self):
        print "%s:                     " %self.name
        ret1 = self.compare(self.test(), (self.ret(),0))
        if ret1:
            print "[OK]"
        else:
            print "[FAILED]"
        ret2 = self.compare(self.ntest(), (self.nret(),-1))
        if ret2:
            print "[OK]"
        else:
            print "[FAILED]"
        return ret1 & ret2


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'

lfc_stat_test().run()
