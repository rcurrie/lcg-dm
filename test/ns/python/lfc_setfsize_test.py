import os, lfc, sys, commands
from testClass import _test

global testHome 

class test_setfsize_ok(_test):
    def info(self):
        return "Set file size on existing file name"
    def prepare(self):
        guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_setfsize_test"
        ret = lfc.lfc_creatg(self.name,guid,0664)
    def clean(self):
        lfc.lfc_unlink(self.name)
    def test(self):
        self.size=123456789L
        ret=lfc.lfc_setfsize(self.name,None,self.size)
        stat=lfc.lfc_filestat()
        lfc.lfc_stat(self.name, stat)
        return (stat,ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.filesize=self.size
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.filesize == ret.filesize )
        else:
            retval = False
        return retval


class test_setfsize_fileid_ok(_test):
    def info(self):
	return "Set filesize on existing fileid: "
    def prepare(self):
        guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_setfsize_test"
        ret = lfc.lfc_creatg(self.name,guid,0664)
        stat=lfc.lfc_filestat()
        self.fileid=lfc.lfc_fileid()
        ret=lfc.lfc_statx(self.name,self.fileid, stat)
    def clean(self):
        lfc.lfc_unlink(self.name)
    def test(self):
        self.size=987654321L
        ret=lfc.lfc_setfsize("",self.fileid,self.size)
        stat=lfc.lfc_filestat()
        lfc.lfc_stat(self.name, stat)
        return (stat,ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.filesize=self.size
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
	    retval = retval & ( test.filesize == ret.filesize )
        else:
            retval = False
        return retval

class test_setfsize_preference_fileid_ok(_test):
    def info(self):
        return "Set filesize on preference on fileid (provided incorrect name): "
    def prepare(self):
        guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_setfsize_test"
        ret = lfc.lfc_creatg(self.name,guid,0664)
        stat=lfc.lfc_filestat()
        self.fileid=lfc.lfc_fileid()
        ret=lfc.lfc_statx(self.name,self.fileid, stat)
    def clean(self):
        lfc.lfc_unlink(self.name)
    def test(self):
        self.size=987654321L
        ret=lfc.lfc_setfsize("/jhgsdjfsg",self.fileid,self.size)
        stat=lfc.lfc_filestat()
        lfc.lfc_stat(self.name, stat)
        return (stat,ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.filesize=self.size
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.filesize == ret.filesize )
        else:
            retval = False
        return retval


class test_nonexisting_name(_test):
    def __init__(self):
        self.retVal = -1
    def info(self):
	return "Set filesize on nonexisting file name: "
    def test(self):
        self.size=987654321L
        self.name="/jhgsdjfsg"
        ret=lfc.lfc_setfsize(self.name,None,self.size)
        stat=lfc.lfc_filestat()
        lfc.lfc_stat(self.name, stat)
        return (stat,ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.filesize=0L
        return retval

    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.filesize == ret.filesize )
        else:
            retval = False
        return retval


class lfc_setfsize_test:
    def __init__(self):
	self.name = "lfc_setfsize_test"
        self.tests=[test_setfsize_ok,test_setfsize_fileid_ok,test_setfsize_preference_fileid_ok,test_nonexisting_name]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_setfsize_test().run()
