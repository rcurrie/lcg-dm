####
#
#	NOTE: Removes also the file which is not writeable.
#	NOTE: Error only if the parent directory is not writeable
#
####


import os, time, lfc, sys, commands, errno
from testClass import _test, _ntest

global testHome 
global name

class test_ok(_test):
    def info(self):
	return "lfc_delete"
    def clean(self):
        lfc.lfc_unlink(name)
    def prepare(self):
        lfc.lfc_mkdir(os.path.dirname(name),0755)
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        ret = lfc.lfc_creatg(name,self.guid,0664)
        if (ret != 0):
            print "Error: cannot create testing file"
            sys.exit(1)


    def test(self):
        ret = lfc.lfc_delete(name) 
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(name,"", statg)
        return (statg,ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.status='D'
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.status == ret.status )
        else:
            retval = False
        return retval

class test_EACCES(_ntest):
    def info(self):
	return "lfc_delete in permission denied (EACCES)"
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        lfc.lfc_mkdir(os.path.dirname(name),0755)
        ret = lfc.lfc_creatg(name,self.guid,0664)
        if (ret != 0):
            print "Error: cannot create testing file"
            sys.exit(1)
        lfc.lfc_chmod(os.path.dirname(name),0500)
    def clean(self):
        lfc.lfc_chmod(os.path.dirname(name),0700)
        lfc.lfc_unlink(name)
        lfc.lfc_rmdir(os.path.dirname(name))
 
    def test(self):
        ret = lfc.lfc_delete(name)
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=1
        retval.filesize=0L
        return (retval, errno.EACCES)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
        else:
            retval = False
        return retval

class test_EPERM(_ntest):
    def info(self):
        return "lfc_delete on directory (EPERM)"
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        lfc.lfc_mkdir(os.path.dirname(name),0755)
        ret = lfc.lfc_creatg(name,self.guid,0664)
        if (ret != 0):
            print "Error: cannot create testing file"
            sys.exit(1)
    def clean(self):
        lfc.lfc_chmod(os.path.dirname(name),0700)
        lfc.lfc_unlink(name)
        lfc.lfc_rmdir(os.path.dirname(name))

    def test(self):
        ret = lfc.lfc_delete(os.path.dirname(name))
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=1
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.EPERM)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval

class test_ENOENT(_ntest):
    def info(self):
        return "lfc_delete nonexisting file (ENOENT)"
    def prepare(self):
        lfc.lfc_mkdir(os.path.dirname(name),0755)
    def clean(self):
        lfc.lfc_rmdir(os.path.dirname(name))

    def test(self):
        ret = lfc.lfc_delete(name)
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=0
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.ENOENT)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval

class test_ENOTDIR(_ntest):
    def info(self):
        return "lfc_delete prefix not a DIR (ENOTDIR)"
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        lfc.lfc_creatg(os.path.dirname(name),self.guid,0664)
    def clean(self):
        lfc.lfc_unlink(os.path.dirname(name))

    def test(self):
        ret = lfc.lfc_delete(name)
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=0
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.ENOTDIR)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval

class lfc_unlink_test:
    def __init__(self):
	self.name = "lfc_unlink_test"
        self.tests=[test_ok,test_EACCES, test_EPERM, test_ENOENT, test_ENOTDIR]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
name = "/grid/dteam/python_delete_test/python_delete_test"
lfc_unlink_test().run()

