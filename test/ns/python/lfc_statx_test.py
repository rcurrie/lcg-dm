import os, lfc, sys, commands
from testClass import _test

global testHome 

class test_existing_file(_test):
    def info(self):
        return "Test root dir: "
    def test(self):
        stat=lfc.lfc_filestat()
        fileid=lfc.lfc_fileid()
        ret=lfc.lfc_statx("/",fileid, stat)
        return ((stat,fileid),ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.fileid=2L
        retval.filemode=16877
        retval.nlink=1
        retval.uid=0
        retval.gid=0
        retval.filesize=0L
        retval.atime=1184059742
        retval.mtime=1171381061
        retval.ctime=1171381061
        retval.fileclass=0
        retval.status='-'
        fileid=lfc.lfc_fileid()
        fileid.server=""
        fileid.fileid=2L
        return (retval,fileid)
    def compare(self, testVal, retVal):
        ((ret, retFileid), retRetVal) = retVal
        ((test, testFileid), testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
        else:
            retval = False
        return retval


class test_nonexisting_file(_test):
    def __init__(self):
        self.retVal = -1
    def info(self):
        return "Check for nonexistent file: "
    def test(self):
        stat=lfc.lfc_filestat()
        fileid=lfc.lfc_fileid()
        ret=lfc.lfc_statx("/nonexisting",fileid,stat)
        return ((stat,fileid),ret)
    def ret(self):
        retval=lfc.lfc_filestat()
        retval.fileid=0L
        retval.filemode=0
        retval.nlink=-1
        retval.uid=0
        retval.gid=0
        retval.filesize=0L
        retval.atime=0
        retval.mtime=0
        retval.ctime=0
        retval.fileclass=0
        retval.status=' '
        fileid=lfc.lfc_fileid()
        fileid.server=""
        fileid.fileid=0L
        return (retval,fileid)
    def compare(self, testVal, retVal):
        ((ret, retFileid), retRetVal) = retVal
        ((test, testFileid), testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
        else:
            retval = False
        return retval



class lfc_statx_test:
    def __init__(self):
	self.name = "lfc_statx_test"
        self.tests=[test_existing_file,test_nonexisting_file]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_statx_test().run()
