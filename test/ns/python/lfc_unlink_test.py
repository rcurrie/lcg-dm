####
#
#	NOTE: Removes also the file which is not writeable.
#	NOTE: Error only if the parent directory is not writeable
#
####


import os, time, lfc, sys, commands, errno
from testClass import _test, _ntest

global testHome 

class test_ok(_test):
    def info(self):
	return "lfc_unlink OK "
    def clean(self):
        lfc.lfc_unlink(self.name)
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_unlink_test"
        ret = lfc.lfc_creatg(self.name,self.guid,0664)
        if (ret != 0):
            print "Error: cannot create testing file"
            sys.exit(1)


    def test(self):
        ret = lfc.lfc_unlink(self.name) 
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(self.name,"", statg)
        return (statg,ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=0
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
	    retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
            retval = retval & ( test.guid == ret.guid )
        else:
            retval = False
        return retval

class test_EACCES(_ntest):
    def info(self):
	return "lfc_unlink in permission denied (EACCES)"
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_unlink_test/python_unlink_test"
        lfc.lfc_mkdir(os.path.dirname(self.name),0755)
        ret = lfc.lfc_creatg(self.name,self.guid,0664)
        if (ret != 0):
            print "Error: cannot create testing file"
            sys.exit(1)
        lfc.lfc_chmod(os.path.dirname(self.name),0500)
    def clean(self):
        lfc.lfc_chmod(os.path.dirname(self.name),0700)
        lfc.lfc_unlink(self.name)
        lfc.lfc_rmdir(os.path.dirname(self.name))
 
    def test(self):
        ret = lfc.lfc_unlink(self.name)
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(self.name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=1
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.EACCES)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval

class test_EPERM(_ntest):
    def info(self):
        return "lfc_unlink on directory (EPERM)"
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_unlink_test/python_unlink_test"
        lfc.lfc_mkdir(os.path.dirname(self.name),0755)
        ret = lfc.lfc_creatg(self.name,self.guid,0664)
        if (ret != 0):
            print "Error: cannot create testing file"
            sys.exit(1)
    def clean(self):
        lfc.lfc_chmod(os.path.dirname(self.name),0700)
        lfc.lfc_unlink(self.name)
        lfc.lfc_rmdir(os.path.dirname(self.name))

    def test(self):
        ret = lfc.lfc_unlink(os.path.dirname(self.name))
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(self.name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=1
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.EPERM)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval

class test_ENOENT(_ntest):
    def info(self):
        return "lfc_unlink nonexisting file (ENOENT)"
    def prepare(self):
        self.name = "/grid/dteam/python_unlink_test/python_unlink_test"
        lfc.lfc_mkdir(os.path.dirname(self.name),0755)
    def clean(self):
        lfc.lfc_rmdir(os.path.dirname(self.name))

    def test(self):
        ret = lfc.lfc_unlink(self.name)
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(self.name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=0
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.ENOENT)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval

class test_ENOTDIR(_ntest):
    def info(self):
        return "lfc_unlink prefix not a DIR (ENOTDIR)"
    def prepare(self):
        self.name = "/grid/dteam/python_unlink_test/python_unlink_test"
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        lfc.lfc_creatg(os.path.dirname(self.name),self.guid,0664)
    def clean(self):
        lfc.lfc_unlink(os.path.dirname(self.name))

    def test(self):
        ret = lfc.lfc_unlink(self.name)
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(self.name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=0
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.ENOTDIR)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval

class test_EEXIST(_ntest):
    def info(self):
        return "lfc_unlink file has replicas (EEXIST)"
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_unlink_test"
        ret = lfc.lfc_creatg(self.name,self.guid,0664)
        if (ret != 0):
            print "Error: cannot create testing file"
            sys.exit(1)
        self.replica = "sfn://my_se.cern.ch/castor/cern.ch/grid/something"
        lfc.lfc_addreplica(self.guid, None, os.getenv("LFC_HOST"), self.replica, "-", "D","","")
        # create replica
    def clean(self):
	# unlink replicas
        lfc.lfc_delreplica(self.guid,None,self.replica)
        lfc.lfc_unlink(self.name)

    def test(self):
        ret = lfc.lfc_unlink(self.name)
        err = lfc.cvar.serrno
        statg=lfc.lfc_filestatg()
        lfc.lfc_statg(self.name,"", statg)
        return ((statg, err), ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=1
        retval.filesize=0L
        retval.fileclass=0
        retval.status=' '
        return (retval, errno.EEXIST)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        ((test, testerr), testRetVal) = testVal
        retval = True
        if ((retRetVal == testRetVal) & (reterr == testerr)):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
        else:
            retval = False
        return retval


class lfc_unlink_test:
    def __init__(self):
	self.name = "lfc_unlink_test"
        self.tests=[test_ok,test_EACCES, test_EPERM, test_ENOENT, test_ENOTDIR, test_EEXIST]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_unlink_test().run()

