#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10
extern char *optarg;
extern int optind;

main(argc, argv)
int argc;
char **argv;
{
	char buf[4096];
	int c;
	FILE *df;
	char *dirfile = NULL;
	int errflg = 0;
	static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
	struct dpm_copyfilestatus *filestatuses = NULL;
	char *fromsurl;
	int i;
	int nbfiles = 0;
	int nbreplies;
	int r = 0;
	char r_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_copyfilereq *reqfiles;
	int status;
	char *tosurl;

	while ((c = getopt (argc, argv, "i:")) != EOF) {
		switch (c) {
		case 'i':
			dirfile = optarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if ((optind > argc - 2 && dirfile == NULL) ||
	    (optind < argc && dirfile != NULL)) {
		errflg++;
	}
	if (errflg) {
		fprintf (stderr, "usage: %s from_SURL to_SURL\n\t%s -i directive_file\n",
				argv[0], argv[0]);
		exit (1);
	}
	if (dirfile == NULL) 
		nbfiles = 1;
	else {
		if ((df = fopen (dirfile, "r")) == NULL) {
			perror ("dirfile open");
			exit (1);
		}
		while (fgets (buf, sizeof(buf), df)) {
			if (buf[strlen (buf)-1] != '\n') {
				fprintf (stderr, "directive %d too long\n", nbfiles + 1);
				exit (1);
			}
			nbfiles++;
		}
		rewind (df);
	}

	if ((reqfiles = calloc (nbfiles, sizeof(struct dpm_copyfilereq))) == NULL) {
		perror ("calloc");
		exit (1);
	}
	if (dirfile == NULL) {
		reqfiles[0].from_surl = argv[1];
		reqfiles[0].to_surl = argv[2];
	} else {
		i = 0;
		while (fgets (buf, sizeof(buf), df)) {
			buf[strlen (buf)-1] = '\0';
			if ((fromsurl = strtok (buf, " ")) == NULL ||
			    (tosurl = strtok (NULL, " ")) == NULL) {
				fprintf (stderr,
				    "directive %d does not contain a fromSURL and a toSURL\n",
				    nbfiles + 1);
				exit (1);
			}
			if ((reqfiles[i].from_surl = strdup (fromsurl)) == NULL ||
			    (reqfiles[i].to_surl = strdup (tosurl)) == NULL) {
				perror ("malloc");
				exit (1);
			}
			i++;
		}
		fclose (df);
	}

	r_token[0] = '\0';
	status = dpm_copy (nbfiles, reqfiles, NULL, 0, 0,
	    r_token, &nbreplies, &filestatuses);

	if (*r_token)
		printf ("dpm_copy returned r_token: %s\n", r_token);

	if (! filestatuses) {
		sperror ("dpm_copy");
		exit (1);
	}

	/* wait for request status "Done" or "Failed" */

	while (status == DPM_QUEUED4COPY || status == DPM_ACTIVE) {
		dpm_free_cfilest (nbreplies, filestatuses);
		printf("request state Pending\n");
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		filestatuses = NULL;
		if ((status = dpm_getstatus_copyreq (r_token, 0, NULL, NULL,
		    &nbreplies, &filestatuses)) < 0) {
			if (filestatuses) break;
			sperror ("dpm_getstatus_copyreq");
			exit (1);
		}
	}
	printf ("request state %s\n", f_stat[status >> 12]);

	for (i = 0; i < nbreplies; i++) {
		if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n",
			    i, f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : "");
		else {
			printf ("state[%d] = %s", i,
			    f_stat[(filestatuses+i)->status >> 12]);
			if ((filestatuses+i)->f_lifetime == 0x7FFFFFFF)
				printf (" f_lifetime = Inf\n");
			else
				printf (" f_lifetime = %d\n", (filestatuses+i)->f_lifetime);
		}
		if ((filestatuses+i)->from_surl)
			free ((filestatuses+i)->from_surl);
		if ((filestatuses+i)->to_surl)
			free ((filestatuses+i)->to_surl);
		if ((filestatuses+i)->errstring)
			free ((filestatuses+i)->errstring);
	}
	free (filestatuses);
	exit (0);
}
