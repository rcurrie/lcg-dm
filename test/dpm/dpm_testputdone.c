#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

main(argc, argv)
int argc;
char **argv;
{
	static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
	struct dpm_filestatus *filestatuses;
	int i;
	int nbfiles;
	int nbreplies;
	int r = 0;
	char *r_token;
	char **surls;
	int status;

	if (argc < 3) {
		fprintf (stderr, "usage: %s r_token SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 2;
	r_token = argv[1];
	surls = &argv[2];

	if ((status = dpm_putdone (r_token, nbfiles, surls, &nbreplies,
	    &filestatuses)) < 0) {
		sperror ("dpm_putdone");
		exit (1);
	}

	for (i = 0; i < nbreplies; i++) {
		if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : "");
		else
			printf ("state[%d] = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12]);
		if ((filestatuses+i)->surl)
			free ((filestatuses+i)->surl);
		if ((filestatuses+i)->errstring)
			free ((filestatuses+i)->errstring);
	}
	free (filestatuses);
	exit (0);
}
